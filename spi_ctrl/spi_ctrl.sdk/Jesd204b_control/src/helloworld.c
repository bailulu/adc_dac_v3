/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
/***************************** Include Files *********************************/

#include <stdio.h>
#include <string.h>
#include "platform.h"
#include "xil_printf.h"
#include "xuartlite_l.h"
#include "xparameters.h"

#include "xstatus.h"
#include "xspi.h"		/* SPI device driver */
#include "xspi_l.h"
#include "selfdefined.h"
/***************************** Include Files *********************************/

#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID
#define ADC_BUFFER_SIZE	    12
#define ADC_BUFFER_WIDTH    24
#define LMK_BUFFER_SIZE	    12
#define LMK_BUFFER_WIDTH    24
#define DAC_BUFFER_SIZE	    12
#define DAC_BUFFER_WIDTH    24

/**************************** Type Definitions *******************************/
typedef unsigned char u24;


typedef int AdcDataBuffer[ADC_BUFFER_SIZE];
u24 AdcReadBuffer[ADC_BUFFER_SIZE];
u24 AdcWriteBuffer[ADC_BUFFER_SIZE];

typedef u24 LmkDataBuffer[LMK_BUFFER_SIZE];
u24 LmkReadBuffer[LMK_BUFFER_SIZE];
u24 LmkWriteBuffer[LMK_BUFFER_SIZE];

typedef int DacDataBuffer[DAC_BUFFER_SIZE];
u24 DacReadBuffer[DAC_BUFFER_SIZE];
u24 DacWriteBuffer[DAC_BUFFER_SIZE];

/************************** Function Prototypes ******************************/

int SpiPolled(XSpi *SpiInstancePtr, u16 SpiDeviceId);
static XSpi  SpiInstance;	 /* The instance of the SPI device */

/*
int main()
{
    init_platform();

    print("Hello World\n\r");

    cleanup_platform();
    return 0;
}*/
/*****************************************************************************/
/**
 * spiPolledfunction
 */
int SpiPolledInitial(XSpi *SpiInstancePtr, u16 SpiDeviceId)
	{
		int Status;
		XSpi_Config *ConfigPtr;	/* Pointer to Configuration data */

		/*
		 * Initialize the SPI driver so that it is  ready to use.
		 */
		ConfigPtr = XSpi_LookupConfig(SpiDeviceId);
		if (ConfigPtr == NULL) {
			return XST_DEVICE_NOT_FOUND;
		}

		Status = XSpi_CfgInitialize(SpiInstancePtr, ConfigPtr,
					  ConfigPtr->BaseAddress);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}


		/*
		 * Set the Spi device as a master.
		 */
		Status = XSpi_SetOptions(SpiInstancePtr, XSP_MASTER_OPTION);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Start the SPI driver so that the device is enabled.
		 */
		XSpi_Start(SpiInstancePtr);

		/*
		 * Disable Global interrupt to use polled mode operation
		 */
		XSpi_IntrGlobalDisable(SpiInstancePtr);

		return XST_SUCCESS;
	}


int AdcPolled(XSpi *SpiInstancePtr, u16 SpiDeviceId)
	{
		u32 Count;

		/*
		 * Initialize the write buffer with pattern to write, initialize the
		 * read buffer to zero so it can be verified after the read, the
		 * Test value that is added to the unique value allows the value to be
		 * changed in a debug environment.
		 */
	   for (Count = 0; Count < ADC_BUFFER_SIZE; Count++) {
				AdcReadBuffer[Count] = 0;
			}

		    AdcWriteBuffer[1]=ADC_CONFIG_A_SR_MASK;
		    AdcWriteBuffer[2]=0x000001;
		    AdcWriteBuffer[3]=0x000001;
		    AdcWriteBuffer[4]=0x000001;
		    AdcWriteBuffer[5]=0x000001;
		    AdcWriteBuffer[6]=0x000001;

		/*
		 * Transmit the data.
		 */
		XSpi_Transfer(SpiInstancePtr, AdcWriteBuffer, NULL, ADC_BUFFER_SIZE);
		return XST_SUCCESS;


int DacPolled(XSpi *SpiInstancePtr, u16 SpiDeviceId)
			{
				u32 Count;

				/*
				 * Initialize the write buffer with pattern to write, initialize the
				 * read buffer to zero so it can be verified after the read, the
				 * Test value that is added to the unique value allows the value to be
				 * changed in a debug environment.
				 */
			   for (Count = 0; Count < ADC_BUFFER_SIZE; Count++) {
						AdcReadBuffer[Count] = 0;
					}

				    DacWriteBuffer[1]=DAC_Config0_MASK;
				    DacWriteBuffer[2]=0x000001;
				    DacWriteBuffer[3]=0x000001;
				    DacWriteBuffer[4]=0x000001;
				    DacWriteBuffer[5]=0x000001;
				    DacWriteBuffer[6]=0x000001;

				/*
				 * Transmit the data.
				 */
				XSpi_Transfer(SpiInstancePtr, AdcWriteBuffer, NULL, ADC_BUFFER_SIZE);
				return XST_SUCCESS;

int LmkPolled(XSpi *SpiInstancePtr, u16 SpiDeviceId)
					{
						u32 Count;


			for (Count = 0; Count < ADC_BUFFER_SIZE; Count++) {
								AdcReadBuffer[Count] = 0;
							}

						    LmkWriteBuffer[1]=ADC_CONFIG_A_SR_MASK;
						    LmkWriteBuffer[2]=0x000001;
						    LmkWriteBuffer[3]=0x000001;
						    LmkWriteBuffer[4]=0x000001;
						    LmkWriteBuffer[5]=0x000001;
						    LmkWriteBuffer[6]=0x000001;

						/*
						 * Transmit the data.
						 */
			XSpi_Transfer(SpiInstancePtr, AdcWriteBuffer, NULL, ADC_BUFFER_SIZE);
			return XST_SUCCESS;

/* a .h file can be created to list the offset_addr of jesd204*/
void Jesd204_Tx(u32 BaseAddress)
{
	   xil_printf("Initial Jesd204_Tx\n\r");
	   Xil_Out32((BaseAddress+OFFSET_Tx_RESET), 0x00000001);
	   Xil_Out32((BaseAddress+OFFSET_Tx_ILA_Multiframes), 0x03);
	   Xil_Out32((BaseAddress+OFFSET_Tx_OctetsPerFrame), 0x01);
	   Xil_Out32((BaseAddress+OFFSET_Tx_FramePerMultiframe), 0x1F);
	   Xil_Out32((BaseAddress+OFFSET_Tx_Lanes), 0x01);
	   Xil_Out32((BaseAddress+OFFSET_Tx_OctetsPerFrame), 0x01);
	   Xil_Out32((BaseAddress+OFFSET_Tx_FramePerMultiframe), 0x1F);

	   xil_printf("Tx_LaneID_Lane0: \t0x%08x\t \n\r", Xil_In32((BaseAddress+OFFSET_Tx_LaneID_Lane0)));

	   xil_printf("Initial Jesd204_Tx Complete\n\r");
}

void Jesd204_Rx(u32 BaseAddress)
{
	   xil_printf("Initial Jesd204_Rx\n\r");
	   Xil_Out32((BaseAddress+OFFSET_Rx_RESET), 0x00000001);
	   Xil_Out32((BaseAddress+OFFSET_Rx_OctetsPerFrame), 0x01);
	   Xil_Out32((BaseAddress+OFFSET_Rx_FramePerMultiframe), 0x1F);
	   Xil_Out32((BaseAddress+OFFSET_Rx_Lanes), 0x01);
	   Xil_Out32((BaseAddress+OFFSET_Rx_OctetsPerFrame), 0x01);
	   Xil_Out32((BaseAddress+OFFSET_Rx_FramePerMultiframe), 0x1F);
	   xil_printf("Jesd204 Version: \t0x%08x\t \n\r", Xil_In32((BaseAddress+OFFSET_Rx_ILA_Config_0)));
	   xil_printf("Jesd204 Version: \t0x%08x\t \n\r", Xil_In32((BaseAddress+OFFSET_Rx_ILA_Config_1)));
	   xil_printf("Jesd204 Version: \t0x%08x\t \n\r", Xil_In32((BaseAddress+OFFSET_Rx_ILA_Config_2)));

	   xil_printf("Initial Jesd204_Rx Complete\n\r");
}

void Jesd204_PHY(u32 BaseAddress)
{
	   xil_printf("Initial Jesd204_PHY\n\r");
	   xil_printf("Jesd204PHY IP Configuration: \t0x%08x\t \n\r", Xil_In32((BaseAddress+OFFSET_JESD204PHY_IP_Configuration)));
	   Xil_Out32((BaseAddress+OFFSET_JESD204PHY_CommonInterfaceSelector), Xil_In32((BaseAddress+OFFSET_JESD204PHY_NumCommonInterfaces)));
	   Xil_Out32((BaseAddress+OFFSET_JESD204PHY_GTInterfaceSelector), Xil_In32((BaseAddress+OFFSET_JESD204PHY_NumTRxInterfaces)));

	   /*setting DRP address and data*/
	   Xil_Out32((BaseAddress+OFFSET_JESD204PHY_DRP_WData), 0x01);
	   Xil_Out32((BaseAddress+OFFSET_JESD204PHY_DRP_Address), 0x80000000|OFFSET_GTH_DRP_TxOUT_DIV);


	   xil_printf("Initial Jesd204_PHY Complete\n\r");
}


void SPI_Initial(){
	    xil_printf("Initial SPI Driver\n\r");
	    SpiPolledInitial(&SpiInstance, SPI_DEVICE_ID);
	    xil_printf("Initial SPI Driver Complete\n\r");
}
void ADC1_Initial(){
	  xil_printf("Initial FMC142 ADC1\n\r");
	  AdcPolled(&SpiInstance, SPI_DEVICE_ID);

        xil_printf("Initial FMC142 ADC1 Complete\n\r");
}

void DAC_Initial(){
        xil_printf("Initial FMC142 DAC\n\r");
        DacPolled(&SpiInstance, SPI_DEVICE_ID);
        xil_printf("Initial FMC142 DAC Complete\n\r");
}

void LMK_Initial(){
        xil_printf("Initial FMC142 LMK\n\r");
        LmkPolled(&SpiInstance, SPI_DEVICE_ID);
        xil_printf("Initial FMC142 LMK Complete\n\r");
}



void FMC142_initial (void) {

	while (1){
		xil_printf("\n\r----Configure FMC142 Card----");
		xil_printf("c: Configure Jesd204 and Jesd204 PHY\n\r");
		xil_printf("s: Initial SPI driver\n\r");
		xil_printf("f: Initial FMC142 ALL\n\r");
		xil_printf("a: Initial FMC142 ADC1\n\r");
		xil_printf("d: Initial FMC142 DAC\n\r");
		xil_printf("l: Initial FMC142 LMK\n\r");
		xil_printf("b: Return to the previous menu\n\r");
		xil_printf("Select: \n\r");
		char cmd = XUartLite_RecvByte(XPAR_AXI_UARTLITE_0_BASEADDR);
		xil_printf("%c \n\r", cmd);
		xil_printf("\n\r");
		xil_printf("\n\r");
		switch (cmd){
		case'c':
			Jesd204_PHY(XPAR_JESD204_PHY_0_BASEADDR);
			Jesd204_Tx(XPAR_JESD204_TX_BASEADDR);
			Jesd204_Rx(XPAR_JESD204_RX_BASEADDR);
			xil_printf("\n\r");
			break;
		case's':
			SPI_Initial();
			xil_printf("\n\r");
			break;
		case'f':
			ADC1_Initial();
			DAC_Initial();
			LMK_Initial();
			xil_printf("\n\r");
			break;
		case'a':
			ADC1_Initial();
			xil_printf("\n\r");
			break;
		case'd':
			DAC_Initial();
			xil_printf("\n\r");
			break;
		case'l':
			LMK_Initial();
			xil_printf("\n\r");
			break;
		case'b':
			return;
		}
	}
}


int FMC142_JESD204B_Manual_Test(void){
	while (1){
		xil_printf("\n\r----NetFPGA_SUME FMC142 JESD204B TEST----");
		xil_printf("i: PHY Information\n\r");
		xil_printf("c: Configure Jesd204 Interface\n\r");
		xil_printf("b: Return to the previous menu\n\r");
		xil_printf("Select: \n\r");
		char cmd = XUartLite_RecvByte(XPAR_AXI_UARTLITE_0_BASEADDR);
		xil_printf("%c \n\r", cmd);
		xil_printf("\n\r");
		xil_printf("\n\r");
		switch (cmd){
			case'i':
				/* do some to test the uart is connected */

				xil_printf("Serial port connected successfully\n\r");
				xil_printf("\n\r");
			break;
			case'c':
				FMC142_initial ();
			    xil_printf("\n\r");
			break;
			case'b':
				return;
				}
	}
}

