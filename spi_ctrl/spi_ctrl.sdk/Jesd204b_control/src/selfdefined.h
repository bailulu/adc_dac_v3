/*
 * selfdefined.h
 *
 *  Created on: Apr 17, 2018
 *      Author: lu
 */

#ifndef SRC_SELFDEFINED_H_
#define SRC_SELFDEFINED_H_


/***********************************ADC***************************************/
#define ADC_CONFIG_A_SR_MASK       0x0000BC  /*set soft-reset to set all the registers to the default values.*/
#define ADC_SPI_CFG_logic_level    0x001001  /*SDO output logic level--3V*/
#define ADC_OM2_CLKDIV       0x001340  /*CLKDIV=1  divide by 1*/
#define ADC_OVR_TH			 0x003C00  /*full scale*/
#define ADC_SER_CFG 		 0x004700  /*0.580V  Serial-lane transmitter driver output differential peak-to-peak voltage amplitude.*/
									   /*De-emphasis 0[dB]*/
#define ADC_JESD_CTRL1       0x00607D  /*scrambler disabled, K=32, single-lane mode, JESD204B link enabled.*/
#define ADC_JESD_CTRL2       0x006100  /* test mode disabled*/

/* read */
#define ADC_JESD_STATUS      0x806C

/***********************************DAC***************************************/
#define DAC_Config0_MASK 0x0000BC

/*PLL setting: bypass mode*/
#define DAC_config49 0x311000 /*reset=1, enable=0*/
#define DAC_config26 0x1A0020 /*pll sleep=1/

/*SERDES setting*/
#define DAC_config59 0x3BC000/*serdes_clk_sel = 1  because the DAC PLL bypass mode is selected
 	 	 	 	 	 	 	   serdes_clk_div = 8  M=8 DACCLK/M=serdes_refclk */
#define DAC_config61 0x3D0080  /* TESTPATT disable, ENOC enable, loopback=00,
							   config61[6:0] control equalizer*/
#define DAC_config62 0x3D0400  /*TERM=001---AC coupling, RATE=00 --- full,
								config62[4:2]--bus width 20bit--did not find*/

/*SERDES lane setting*/
#define DAC_config63 0x3F0000 /*no polarity invert*/
/*config71 keep default*/
#define DAC_config73 0x490000 /*link assign: input multiplexer function---in this case, all the input is assigned to link0*/
#define DAC_config74 0x4A0100/*enable jesd204B 8 lanes--- only enable lane0
 	 	 	 	 	 	 	 jesd sequence test disable,
 	 	 	 	 	 	 	 config74[5]turn on DUAL DAC channel
 	 	 	 	 	 	 	 config74[4:0]--- jesd204 interface*/
/*config95 96 ----jesd 204*/

/*jesd clock  clkjesd_div*/
#define DAC_config37 0x25
/*sysref signal type---use only next sysref pulse=010*/
#define DAC_config36 0x240020
/*interpolation amount*/
#define DAC_config0 0x00

/*jesd setting*/


/***********************************LMK******************************
 24-bit register, 1 bit command, 2-bit multi-byte filed (W1, W2), 13-bit address,
 8-bit data, w1 w2 =0
 ********************************************************************/
#define LMK_reg000_reset        0x000080
#define LMK_reg000_4wires       0x000010 /*enable 4 wire spi mode,
								           CLKin_SEL0 is selected as SPI read pin--FMC(C18)*/

/*Setup output dividers and power down the unused clock*/
#define LMK_reg100_DCLK0_DIV    0x010010 /*FPGA:div=16---2500/16=156.25*/
#define LMK_reg101_DDLY         0x010155 /* the digital delay module is used for phase adjustment of the output clock,
											the default values are 5
											DCLK_DLY_CNTH, DCLK_DLY_CNTL*/
/*reg103: analog delay control registers set as default.*/
#define LMK_reg104   		    0x010463  /*device clock half step; SYSREF is the input of SDCLKoutY;
 	 	 	 	 	 	 	 	            SDCLKout digital delay: 2 clock cycles of VCO;
 	 	 	 	 	 	 	 	            sysref clock half step*/
/* reg105: sysref analog delay disabled*/
#define LMK_reg106_CLKoutX_Y_PD 0x010671  /*bit[0]? sclkout power down?*/
#define LMK_reg107_CLK_FMT      0x010711  /*SDCLK and DCLK format: LVDS; no polarity*/



#define LMK_reg108_DCLK2_DIV    0x010801  /*DAC:div=1 ---2500/1=2500*/
#define LMK_reg109_DDLY         0x010955 /* the digital delay module is used for phase adjustment of the output clock,
											the default values are 5
											DCLK_DLY_CNTH, DCLK_DLY_CNTL*/
#define LMK_reg10C              0x010C63  /*device clock half step; SYSREF is the input of SDCLKoutY;
 	 	 	 	 	 	 	         	    SDCLKout digital delay: 2 clock cycles of VCO;
 	 	 	 	 	 	 	 	            sysref clock half step*/
#define LMK_reg10E_CLKoutX_Y_PD 0x010E71  /*bit[0]? sclkout power down?*/
#define LMK_reg10F_CLK_FMT      0x010F11  /*SDCLK and DCLK format: LVDS; no polarity*/



#define LMK_reg110_DCLK4_DIV    0x011007 /*ADC:div=7 ---2500/7=357.14*/
#define LMK_reg111_DDLY         0x011155 /* the digital delay module is used for phase adjustment of the output clock,
											the default values are 5
											DCLK_DLY_CNTH, DCLK_DLY_CNTL*/
#define LMK_reg114              0x011463  /*device clock half step; SYSREF is the input of SDCLKoutY;
 	 	 	 	 	 	 	         	    SDCLKout digital delay: 2 clock cycles of VCO;
 	 	 	 	 	 	 	 	            sysref clock half step*/
#define LMK_reg116_CLKoutX_Y_PD 0x011671  /*bit[0]? sclkout power down?*/
#define LMK_reg117_CLK_FMT      0x011711  /*SDCLK and DCLK format: LVDS; no polarity*/


/*power down the clock group 6-7*/
#define LMK_reg11E_CLKoutX_Y_PD 0x011EF9
#define LMK_reg11F_CLK_FMT      0x011F00  /*SDCLK and DCLK format: LVDS; no polarity*/



#define LMK_reg120_DCLK8_DIV    0x0120   /*GBTCLK0:div=*/
#define LMK_reg121_DDLY         0x012155 /* the digital delay module is used for phase adjustment of the output clock,
											the default values are 5
											DCLK_DLY_CNTH, DCLK_DLY_CNTL*/
#define LMK_reg124              0x012463  /*device clock half step; SYSREF is the input of SDCLKoutY;
 	 	 	 	 	 	 	         	    SDCLKout digital delay: 2 clock cycles of VCO;
 	 	 	 	 	 	 	 	            sysref clock half step*/
#define LMK_reg126_CLKoutX_Y_PD 0x012671  /*bit[0]? sclkout power down?*/
#define LMK_reg127_CLK_FMT      0x012711  /*SDCLK and DCLK format: LVDS; no polarity*/



#define LMK_reg128_DCLK10_DIV   0x0120   /*GBTCLK1:div=*/
#define LMK_reg129_DDLY         0x012955 /* the digital delay module is used for phase adjustment of the output clock,
											the default values are 5
											DCLK_DLY_CNTH, DCLK_DLY_CNTL*/
#define LMK_reg12C              0x012C63  /*device clock half step; SYSREF is the input of SDCLKoutY;
 	 	 	 	 	 	 	         	    SDCLKout digital delay: 2 clock cycles of VCO;
 	 	 	 	 	 	 	 	            sysref clock half step*/
#define LMK_reg12E_CLKoutX_Y_PD 0x012E71  /*bit[0]? sclkout power down?*/
#define LMK_reg12F_CLK_FMT      0x012F11  /*SDCLK and DCLK format: LVDS; no polarity*/


/*power down the clock group 12-13*/
#define LMK_reg136_CLKoutX_Y_PD 0x0136F9
#define LMK_reg137_CLK_FMT      0x013700  /*SDCLK and DCLK format: power down; no polarity*/


#define LMK_reg138_clock        0x013800  /*vco0 is selected,
 	 	 	 	 	 	 	 	 	        power down the OSCout, it can be used as the third ref input for PLL1--CLKin2/CLKin2*
 	 	 	 	 	 	 	 	 	        but in this project, this pin is not needed*/
#define LMK_reg139_SYSREF_MUX   0x013900 /*SYSREF_MUX=0*/
/*setup sysref divider=10MHz:2500/250=10, div=250*/
#define LMK_reg13A_SYSREF_DIV   0x013A00
#define LMK_reg13B_SYSREF_DIV   0x013BFA   /*250=00FA*/
/*sysref digital delay*/
#define LMK_reg13C_SYSREF_DDLY  0x013C
#define LMK_reg13D_SYSREF_DDLY  0x013D


#define LMK_reg13E_SYSREF_PULSE_CNT 0x013E00 /*number of SYSREF pulse: 1 pulse---?*/
#define LMK_reg13F_FB_MUX			0x013F   /*feedback selection*/


/*Setup SYSREF*/
#define LMK_reg140_PowerDown        0x014080 /*power down PLL1,
											   SYSREF_PD = 0,
											   SYSREF_DDLY_PD = 0,
											   SYSREF_PLSR_PD = 0 */
#define LMK_reg141_Dynamic_DDLY     0x014100 /*disable dynamic digital delay*/
#define LMK_reg142_Dynamic_CNT      0x014200 /*disable dynamic digital delay*/

#define LMK_reg143_SYNC_POL         0x014300 /*SYNC_POL=0*/
#define LMK_reg143_CYNC_MODE        0x014301 /*SYNC_MODE=1*/
#define LMK_reg143_SYNC_EN          0x014310 /*SYNC_EN = 1*/
#define LMK_reg143_SYSREF_CLR       0x014380 /*sysref_clear = 1, Clear Local SYSREF DDLY: SYSREF_CLR = 1*/

#define LMK_reg144_SYNC_DIS         0x014400

/*CLKin control*/
#define LMK_reg146_Disable          0x014600 /*disable CLKin0,1,2*/

/*RESET control*/
#define LMK_reg14A_reset            0x014A02  /*reset pin set as pull-down resistor. reset=1*/
/*PLL1 Holdover*/
#define LMK_reg14B_HOLDOVER         0x014B04  /*disable holdover mode, as it hold the clock in of PLL1*/
#define LMK_reg150_HOLDOVER         0x015000  /*disable holdover mode, as it hold the clock in of PLL1*/

#define LMK_reg15F_STATUS_LD1       0x015F
/*PLL2*/
#define LMK_reg160_PLL2_RDiv        0x0160
#define LMK_reg161_PLL2_RDiv        0x0161
#define LMK_reg162_PLL2_P
#define LMK_reg162_PLL2_QSCin_Freq  0x016210  /*frequency > 255MHz select=4*/
#define LMK_reg162_PLL2_2xREF_en    0x016201

/*reg163-165 PLL2_N_divider value for cascaded 0-delay mode, when PLL2_NCLK_MUX = 1, so we do not use these regs*/
/*reg166-168 */
#define LMK_reg166                  0x016600  /*frequency calibration enabled*/
#define LMK_reg167_PLL2_N           0x0167
#define LMK_reg168_PLL2_N           0x0168

#define LMK_reg169_default          0x016959

#define LMK_reg16A          0x016A
#define LMK_reg16B          0x016B

#define LMK_reg16E_Status_LD2        0x016E  /*set the output value of status_LD2*/
#define LMK_reg173  0x017300 /*PLL2 prescaler and PLL2 power on*/
/*reg17C and 17D bypassed: because VCO0 is selected*/
#define LMK_reg183_PLL2_DLD_edge     0x0183
#define LMK_reg184_ReadBack          0x018400  /*not selected*/
#define LMK_reg185_ReadBack          0x018500  /*do not care*/
#define LMK_reg188_ReadBack			 0x018800

#define LMK_reg1FFD_Lock         0x1FFD00 /*registers unlocked*/
#define LMK_reg1FFE_Lock         0x1FFE00
#define LMK_reg1FFF_Lock         0x1FFF83



/***********************************AMC7823******************************
 * 16-bit command : 1-bit read/write(15), 2-bit memory page(13:12),
 * 5-bit starting register address(10:6), 5-bit ending register address[4:0]
 */
#define AMC_GPIO_reg_addr     0x028A /*page--00, Sadd--0A, Eadd--0A*/
#define AMC_GPIO_reg_data     0xF0C0
#define AMC_Status_reg_addr   0x128A /*page--01, Sadd--0A, Eadd--0A*/
#define AMC_Status_reg_data   0x0000
#define ADC_ADC_control_addr  0x12CB    /*page--01, Sadd--0B, Eadd--0B*/
#define ADC_ADC_control_data  0x8080
#define ADC_RESET_addr        0x130C    /*page--01, Sadd--0C, Eadd--0C*/
#define ADC_RESET_data        0xBB30
#define ADC_PowerDown_addr    0x134D    /*page--01, Sadd--0D, Eadd--0D*/
#define ADC_PowerDown_data    0x8000


/*******Jesd204_PHY***************************************/
/*read*/
#define OFFSET_JESD204PHY_IP_Configuration    0x004
#define OFFSET_JESD204PHY_NumCommonInterfaces 0x008
#define OFFSET_JESD204PHY_NumTRxInterfaces    0x00C
/*write*/
#define OFFSET_JESD204PHY_CommonInterfaceSelector 0x020
#define OFFSET_JESD204PHY_GTInterfaceSelector     0x024
#define OFFSET_JESD204PHY_DRP_Address		      0x204
#define OFFSET_JESD204PHY_DRP_WData			      0x208
#define OFFSET_JESD204PHY_DRP_RData			      0x20C /*read*/
/*reset data path and pll*/
#define OFFSET_JESD204PHY_TxSysReset	  		  0x420
#define OFFSET_JESD204PHY_RxSysReset      		  0x424

/**********************************7 serial GTH TX PG198Page49******************************/
#define OFFSET_GTH_DRP_TxOUT_DIV   0x0088 /*6:4*/
#define OFFSET_GTH_DRP_RxOUT_DIV   0x0088 /*2:0*/
#define OFFSET_GTH_DRP_CPLL_CFG    0x005C
#define OFFSET_GTH_DRP_CPLL_CFG1   0x005D
#define OFFSET_GTH_DRP_CPLL_REFCLK_DIV    0x005E /*12:8*/
#define OFFSET_GTH_DRP_CPLL_FB_DIV45      0x005E  /*7*/
#define OFFSET_GTH_DRP_CPLL_FB_DIV        0x005E /*6:0*/

#define OFFSET_GTH_DRP_RXSYNC_OVRD        0x0010  /*15*/
#define OFFSET_GTH_DRP_RXSYNC_SKIP_DA     0x0010 /*12*/
#define OFFSET_GTH_DRP_RXSYNC_MULTILANE   0x0010 /*10*/
#define OFFSET_GTH_DRP_RX_INT_DATAWIDTH   0x0011 /*14*/
#define OFFSET_GTH_DRP_RX_DATA_WIDTH      0x0011 /*13:11*/
#define OFFSET_GTH_DRP_RX_CM_SEL          0x0011 /*5:4*/
#define OFFSET_GTH_DRP_RXPRBS_ERR_LOOPBACK     0x0011 /*0*/
#define OFFSET_GTH_DRP_SATA_BURST_SEQ_LEN    0x0012 /*15:12*/
/*OUTREFCLK_SEL_INV8*/
#define OFFSET_GTH_DRP_PCS_PCIE_EN  0x001B   /*0: false=0*/
#define OFFSET_GTH_DRP_TXBUF_RESET_ON_RATE_CHANGE 0x001C /*15: false=0*/
#define OFFSET_GTH_DRP_TXBUF_EN    0x001C /*14*/
#define OFFSET_GTH_DRP_TXGEARBOX_EN   0x001C /*5*/
#define OFFSET_GTH_DRP_GEARBOX_MODE   0x001C /*2:0*/
#define OFFSET_GTH_DRP_ES_QUALIFIER0  0x002C
#define OFFSET_GTH_DRP_ES_QUALIFIER1  0x002D
#define OFFSET_GTH_DRP_ES_QUALIFIER2  0x002E
#define OFFSET_GTH_DRP_ES_QUALIFIER3  0x002F
#define OFFSET_GTH_DRP_ES_QUALIFIER4  0x0030
#define OFFSET_GTH_DRP_ES_QUAL_MASK0  0x0031
#define OFFSET_GTH_DRP_ES_QUAL_MASK1  0x0032
#define OFFSET_GTH_DRP_ES_QUAL_MASK2  0x0033
#define OFFSET_GTH_DRP_ES_QUAL_MASK3  0x0034
#define OFFSET_GTH_DRP_ES_QUAL_MASK4  0x0035
ES_SDATA_MASK0  0x0036
ES_SDATA_MASK1  0x0037
ES_SDATA_MASK2  0x0038
ES_SDATA_MASK3  0x0039
ES_SDATA_MASK4  0x003A
ES_PRESCALE     0x003B
ES_VERT_OFFSET 0x003B
ES_HORZ_OFFSET 0x003C
RX_DISPERR_SEQ_MATCH 0x003D
DEC_PCOMMA_DETECT 0x003D
DEC_MCOMMA_DETECT 0x003D
DEC_VALID_COMMA_ONLY 0x003D
ES_ERRDET_EN 0x003D
ES_EYE_SCAN_EN 0x003D
ES_CONTROL 0x003D
ALIGN_COMMA_ENABLE 0x003E
ALIGN_MCOMMA_VALUE 0x003F
RXSLIDE_MODE  0x0040
ALIGN_PCOMMA_VALUE  0x0040
ALIGN_COMMA_WORD 0x0041
RX_SIG_VALID_DLY 0x0041
ALIGN_PCOMMA_DET 0x0041
ALIGN_MCOMMA_DET 0x0041
SHOW_REALIGN_COMMA 0x0041
ALIGN_COMMA_DOUBLE 0x0041
RXSLIDE_AUTO_WAIT 0x0041
CLK_CORRECT_USE      0x0044
CLK_COR_SEQ_1_ENABLE 0x0044
CLK_COR_SEQ_1_1      0x0044
CLK_COR_SEQ_1_2      0x0045
CLK_COR_SEQ_1_3      0x0046
CLK_COR_REPEAT_WAIT  0x0047
CLK_COR_SEQ_1_4      0x0047
CLK_COR_SEQ_2_USE     0x0048
CLK_COR_SEQ_2_ENABLE  0x0048  /*enable or not, if not:*/

CLK_COR_KEEP_IDLE   0x0049
CLK_COR_PRECEDENCE  0x0049
CLK_COR_SEQ_LEN     0x0049
RXGEARBOX_EN   004B




/***********************************Jesd204_Tx***************************************/
#define OFFSET_Tx_RESET 0x004   /*self clean */
#define OFFSET_Tx_SYSREF_HANDING  0x010 /*default-no request */
#define OFFSET_Tx_ILA_Multiframes 0x014
#define OFFSET_Tx_OctetsPerFrame 0x020
#define OFFSET_Tx_FramePerMultiframe 0x024
#define OFFSET_Tx_Lanes 0x028


#define OFFSET_Tx_LaneID_Lane0 0x400 /*read*/

/* ila sequence can be written by ILA config registers, the default values are 0,
 * to write these registers, the related register in adc and dac jesd204b registers
 * should be checked, the values are the same*/
#define OFFSET_Tx_ILA_Config_3 0x80C
#define OFFSET_Tx_ILA_Config_4 0x810
#define OFFSET_Tx_ILA_Config_5 0x814
#define OFFSET_Tx_ILA_Config_6 0x818
#define OFFSET_Tx_ILA_Config_7 0x81C


/***********************************Jesd204_Rx***************************************/
#define OFFSET_Rx_RESET 0x004   /*self clean */
#define OFFSET_Rx_SYSREF_HANDING 0x010 /*default-no request */
#define OFFSET_Rx_OctetsPerFrame 0x020
#define OFFSET_Rx_FramePerMultiframe 0x024
#define OFFSET_Rx_Lanes 0x028

/*read*/
#define OFFSET_Rx_DebugStatus 0x03C
#define OFFSET_Rx_ILA_Config_0 0x800
#define OFFSET_Rx_ILA_Config_1 0x804
#define OFFSET_Rx_ILA_Config_2 0x808
#define OFFSET_Rx_ILA_Config_3 0x80C
#define OFFSET_Rx_ILA_Config_4 0x810
#define OFFSET_Rx_ILA_Config_5 0x814
#define OFFSET_Rx_ILA_Config_6 0x818
#define OFFSET_Rx_ILA_Config_7 0x81C


#endif /* SRC_SELFDEFINED_H_ */
