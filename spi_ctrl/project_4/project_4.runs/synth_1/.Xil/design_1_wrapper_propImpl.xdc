set_property SRC_FILE_INFO {cfile:/home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/project_4/project_4.srcs/constrs_1/new/constrain.xdc rfile:../../../project_4.srcs/constrs_1/new/constrain.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN H19 [get_ports diff_clock_rtl_clk_p]
set_property src_info {type:XDC file:1 line:3 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR13 [get_ports reset_rtl_0]
set_property src_info {type:XDC file:1 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR22 [get_ports {gpio_rtl_tri_io[1]}]
set_property src_info {type:XDC file:1 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR23 [get_ports {gpio_rtl_tri_io[0]}]
set_property src_info {type:XDC file:1 line:9 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AY19 [get_ports uart_rtl_rxd]
set_property src_info {type:XDC file:1 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN BA19 [get_ports uart_rtl_txd]
