#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim tb_all_behav -key {Behavioral:sim_1:Functional:tb_all} -tclbatch tb_all.tcl -view /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/tb_all_behav.wcfg -log simulate.log
