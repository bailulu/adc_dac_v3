-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
-- Date        : Wed Mar 21 19:59:53 2018
-- Host        : lu running 64-bit Ubuntu 14.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ Jesd204_microblaze_jesd204_phy_0_0_sim_netlist.vhdl
-- Design      : Jesd204_microblaze_jesd204_phy_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7vx690tffg1761-3
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_GT is
  port (
    gt0_cpllfbclklost_out : out STD_LOGIC;
    gt0_cplllock_out : out STD_LOGIC;
    gt0_drprdy_out : out STD_LOGIC;
    gt0_eyescandataerror_out : out STD_LOGIC;
    gt0_gthtxn_out : out STD_LOGIC;
    gt0_gthtxp_out : out STD_LOGIC;
    gt0_rxbyteisaligned_out : out STD_LOGIC;
    gt0_rxbyterealign_out : out STD_LOGIC;
    gt0_rxcommadet_out : out STD_LOGIC;
    gt0_rxoutclk_out : out STD_LOGIC;
    gt0_rxoutclkfabric_out : out STD_LOGIC;
    gt0_rxprbserr_out : out STD_LOGIC;
    gt0_rxresetdone_out : out STD_LOGIC;
    gt0_txoutclk_out : out STD_LOGIC;
    gt0_txoutclkfabric_out : out STD_LOGIC;
    gt0_txoutclkpcs_out : out STD_LOGIC;
    gt0_txresetdone_out : out STD_LOGIC;
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );
    gt0_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_txbufstatus_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxbufstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxdata_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );
    gt0_rxchariscomma_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxcharisk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxdisperr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxnotintable_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    data_in : out STD_LOGIC;
    data_sync_reg1 : out STD_LOGIC;
    gt0_cplllockdetclk_in : in STD_LOGIC;
    cpllpd_in : in STD_LOGIC;
    cpllreset_in : in STD_LOGIC;
    gt0_drpclk_in : in STD_LOGIC;
    gt0_drpen_in : in STD_LOGIC;
    gt0_drpwe_in : in STD_LOGIC;
    gt0_eyescanreset_in : in STD_LOGIC;
    gt0_eyescantrigger_in : in STD_LOGIC;
    gt0_gthrxn_in : in STD_LOGIC;
    gt0_gthrxp_in : in STD_LOGIC;
    gt0_gtnorthrefclk0_in : in STD_LOGIC;
    gt0_gtnorthrefclk1_in : in STD_LOGIC;
    gt0_gtrefclk0_in : in STD_LOGIC;
    gt0_gtrefclk1_in : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gtsouthrefclk0_in : in STD_LOGIC;
    gt0_gtsouthrefclk1_in : in STD_LOGIC;
    gt0_gttxreset_in1_out : in STD_LOGIC;
    GT0_QPLLOUTCLK_IN : in STD_LOGIC;
    GT0_QPLLOUTREFCLK_IN : in STD_LOGIC;
    gt0_rxbufreset_in : in STD_LOGIC;
    gt0_rxcdrhold_in : in STD_LOGIC;
    gt0_rxdfelpmreset_in : in STD_LOGIC;
    gt0_rxlpmen_in : in STD_LOGIC;
    gt0_rxmcommaalignen_in : in STD_LOGIC;
    gt0_rxpcommaalignen_in : in STD_LOGIC;
    gt0_rxpcsreset_in : in STD_LOGIC;
    gt0_rxpmareset_in : in STD_LOGIC;
    gt0_rxpolarity_in : in STD_LOGIC;
    gt0_rxprbscntreset_in : in STD_LOGIC;
    gt0_rxuserrdy_in3_out : in STD_LOGIC;
    gt0_rxusrclk_in : in STD_LOGIC;
    gt0_rxusrclk2_in : in STD_LOGIC;
    gt0_txinhibit_in : in STD_LOGIC;
    gt0_txpcsreset_in : in STD_LOGIC;
    gt0_txpmareset_in : in STD_LOGIC;
    gt0_txpolarity_in : in STD_LOGIC;
    gt0_txprbsforceerr_in : in STD_LOGIC;
    gt0_txuserrdy_in0_out : in STD_LOGIC;
    gt0_txusrclk_in : in STD_LOGIC;
    gt0_txusrclk2_in : in STD_LOGIC;
    gt0_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_loopback_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txdiffctrl_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_txpostcursor_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_txprecursor_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_txdata_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_txcharisk_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_GT;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_GT is
  signal gt0_rxpmaresetdone_i : STD_LOGIC;
  signal \^gt0_rxresetdone_out\ : STD_LOGIC;
  signal \^gt0_txresetdone_out\ : STD_LOGIC;
  signal gthe2_i_n_2 : STD_LOGIC;
  signal gthe2_i_n_50 : STD_LOGIC;
  signal NLW_gthe2_i_GTREFCLKMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_PHYSTATUS_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RSOSINTDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXCDRLOCK_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXCHANBONDSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXCHANISALIGNED_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXCHANREALIGN_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXCOMINITDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXCOMSASDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXCOMWAKEDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXDFESLIDETAPSTARTED_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXDFESLIDETAPSTROBEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXDFESLIDETAPSTROBESTARTED_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXDFESTADAPTDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXELECIDLE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXOSINTSTARTED_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXOSINTSTROBEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXOSINTSTROBESTARTED_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXOUTCLKPCS_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXSYNCDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXSYNCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_RXVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXCOMFINISH_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXGEARBOXREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXPHINITDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXSYNCDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_TXSYNCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_i_PCSRSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gthe2_i_RXCHARISCOMMA_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal NLW_gthe2_i_RXCHARISK_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal NLW_gthe2_i_RXCHBONDO_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gthe2_i_RXCLKCORCNT_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gthe2_i_RXDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 32 );
  signal NLW_gthe2_i_RXDATAVALID_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gthe2_i_RXDISPERR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal NLW_gthe2_i_RXHEADER_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_gthe2_i_RXHEADERVALID_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gthe2_i_RXNOTINTABLE_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal NLW_gthe2_i_RXPHMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gthe2_i_RXPHSLIPMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gthe2_i_RXSTARTOFSEQ_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of gthe2_i : label is "PRIMITIVE";
begin
  gt0_rxresetdone_out <= \^gt0_rxresetdone_out\;
  gt0_txresetdone_out <= \^gt0_txresetdone_out\;
data_sync1_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => gt0_txpd_in(1),
      I1 => gt0_txpd_in(0),
      I2 => \^gt0_txresetdone_out\,
      O => data_in
    );
\data_sync1_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => gt0_rxpd_in(1),
      I1 => gt0_rxpd_in(0),
      I2 => \^gt0_rxresetdone_out\,
      O => data_sync_reg1
    );
gthe2_i: unisim.vcomponents.GTHE2_CHANNEL
    generic map(
      ACJTAG_DEBUG_MODE => '0',
      ACJTAG_MODE => '0',
      ACJTAG_RESET => '0',
      ADAPT_CFG0 => X"00C10",
      ALIGN_COMMA_DOUBLE => "FALSE",
      ALIGN_COMMA_ENABLE => B"1111111111",
      ALIGN_COMMA_WORD => 1,
      ALIGN_MCOMMA_DET => "TRUE",
      ALIGN_MCOMMA_VALUE => B"1010000011",
      ALIGN_PCOMMA_DET => "TRUE",
      ALIGN_PCOMMA_VALUE => B"0101111100",
      A_RXOSCALRESET => '0',
      CBCC_DATA_SOURCE_SEL => "DECODED",
      CFOK_CFG => X"24800040E80",
      CFOK_CFG2 => B"100000",
      CFOK_CFG3 => B"100000",
      CHAN_BOND_KEEP_ALIGN => "FALSE",
      CHAN_BOND_MAX_SKEW => 1,
      CHAN_BOND_SEQ_1_1 => B"0000000000",
      CHAN_BOND_SEQ_1_2 => B"0000000000",
      CHAN_BOND_SEQ_1_3 => B"0000000000",
      CHAN_BOND_SEQ_1_4 => B"0000000000",
      CHAN_BOND_SEQ_1_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_1 => B"0000000000",
      CHAN_BOND_SEQ_2_2 => B"0000000000",
      CHAN_BOND_SEQ_2_3 => B"0000000000",
      CHAN_BOND_SEQ_2_4 => B"0000000000",
      CHAN_BOND_SEQ_2_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_USE => "FALSE",
      CHAN_BOND_SEQ_LEN => 1,
      CLK_CORRECT_USE => "FALSE",
      CLK_COR_KEEP_IDLE => "FALSE",
      CLK_COR_MAX_LAT => 12,
      CLK_COR_MIN_LAT => 8,
      CLK_COR_PRECEDENCE => "TRUE",
      CLK_COR_REPEAT_WAIT => 0,
      CLK_COR_SEQ_1_1 => B"0100000000",
      CLK_COR_SEQ_1_2 => B"0000000000",
      CLK_COR_SEQ_1_3 => B"0000000000",
      CLK_COR_SEQ_1_4 => B"0000000000",
      CLK_COR_SEQ_1_ENABLE => B"1111",
      CLK_COR_SEQ_2_1 => B"0100000000",
      CLK_COR_SEQ_2_2 => B"0000000000",
      CLK_COR_SEQ_2_3 => B"0000000000",
      CLK_COR_SEQ_2_4 => B"0000000000",
      CLK_COR_SEQ_2_ENABLE => B"1111",
      CLK_COR_SEQ_2_USE => "FALSE",
      CLK_COR_SEQ_LEN => 1,
      CPLL_CFG => X"00BC07DC",
      CPLL_FBDIV => 4,
      CPLL_FBDIV_45 => 5,
      CPLL_INIT_CFG => X"00001E",
      CPLL_LOCK_CFG => X"01E8",
      CPLL_REFCLK_DIV => 1,
      DEC_MCOMMA_DETECT => "TRUE",
      DEC_PCOMMA_DETECT => "TRUE",
      DEC_VALID_COMMA_ONLY => "FALSE",
      DMONITOR_CFG => X"000A00",
      ES_CLK_PHASE_SEL => '0',
      ES_CONTROL => B"000000",
      ES_ERRDET_EN => "FALSE",
      ES_EYE_SCAN_EN => "TRUE",
      ES_HORZ_OFFSET => X"000",
      ES_PMA_CFG => B"0000000000",
      ES_PRESCALE => B"00000",
      ES_QUALIFIER => X"00000000000000000000",
      ES_QUAL_MASK => X"00000000000000000000",
      ES_SDATA_MASK => X"00000000000000000000",
      ES_VERT_OFFSET => B"000000000",
      FTS_DESKEW_SEQ_ENABLE => B"1111",
      FTS_LANE_DESKEW_CFG => B"1111",
      FTS_LANE_DESKEW_EN => "FALSE",
      GEARBOX_MODE => B"000",
      IS_CLKRSVD0_INVERTED => '0',
      IS_CLKRSVD1_INVERTED => '0',
      IS_CPLLLOCKDETCLK_INVERTED => '0',
      IS_DMONITORCLK_INVERTED => '0',
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_RXUSRCLK2_INVERTED => '0',
      IS_RXUSRCLK_INVERTED => '0',
      IS_SIGVALIDCLK_INVERTED => '0',
      IS_TXPHDLYTSTCLK_INVERTED => '0',
      IS_TXUSRCLK2_INVERTED => '0',
      IS_TXUSRCLK_INVERTED => '0',
      LOOPBACK_CFG => '0',
      OUTREFCLK_SEL_INV => B"11",
      PCS_PCIE_EN => "FALSE",
      PCS_RSVD_ATTR => X"000000000000",
      PD_TRANS_TIME_FROM_P2 => X"03C",
      PD_TRANS_TIME_NONE_P2 => X"3C",
      PD_TRANS_TIME_TO_P2 => X"64",
      PMA_RSV => B"00000000000000000000000010000000",
      PMA_RSV2 => B"00011100000000000000000000001010",
      PMA_RSV3 => B"00",
      PMA_RSV4 => B"000000000001000",
      PMA_RSV5 => B"0000",
      RESET_POWERSAVE_DISABLE => '0',
      RXBUFRESET_TIME => B"00001",
      RXBUF_ADDR_MODE => "FAST",
      RXBUF_EIDLE_HI_CNT => B"1000",
      RXBUF_EIDLE_LO_CNT => B"0000",
      RXBUF_EN => "TRUE",
      RXBUF_RESET_ON_CB_CHANGE => "TRUE",
      RXBUF_RESET_ON_COMMAALIGN => "FALSE",
      RXBUF_RESET_ON_EIDLE => "FALSE",
      RXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      RXBUF_THRESH_OVFLW => 57,
      RXBUF_THRESH_OVRD => "TRUE",
      RXBUF_THRESH_UNDFLW => 3,
      RXCDRFREQRESET_TIME => B"00001",
      RXCDRPHRESET_TIME => B"00001",
      RXCDR_CFG => X"0002007FE2000C2080018",
      RXCDR_FR_RESET_ON_EIDLE => '0',
      RXCDR_HOLD_DURING_EIDLE => '0',
      RXCDR_LOCK_CFG => B"010101",
      RXCDR_PH_RESET_ON_EIDLE => '0',
      RXDFELPMRESET_TIME => B"0001111",
      RXDLY_CFG => X"001F",
      RXDLY_LCFG => X"030",
      RXDLY_TAP_CFG => X"0000",
      RXGEARBOX_EN => "FALSE",
      RXISCANRESET_TIME => B"00001",
      RXLPM_HF_CFG => B"00001000000000",
      RXLPM_LF_CFG => B"001001000000000000",
      RXOOB_CFG => B"0000110",
      RXOOB_CLK_CFG => "PMA",
      RXOSCALRESET_TIME => B"00011",
      RXOSCALRESET_TIMEOUT => B"00000",
      RXOUT_DIV => 1,
      RXPCSRESET_TIME => B"00001",
      RXPHDLY_CFG => X"084020",
      RXPH_CFG => X"C00002",
      RXPH_MONITOR_SEL => B"00000",
      RXPI_CFG0 => B"00",
      RXPI_CFG1 => B"00",
      RXPI_CFG2 => B"00",
      RXPI_CFG3 => B"11",
      RXPI_CFG4 => '1',
      RXPI_CFG5 => '1',
      RXPI_CFG6 => B"001",
      RXPMARESET_TIME => B"00011",
      RXPRBS_ERR_LOOPBACK => '0',
      RXSLIDE_AUTO_WAIT => 7,
      RXSLIDE_MODE => "OFF",
      RXSYNC_MULTILANE => '0',
      RXSYNC_OVRD => '0',
      RXSYNC_SKIP_DA => '0',
      RX_BIAS_CFG => B"000011000000000000010000",
      RX_BUFFER_CFG => B"000000",
      RX_CLK25_DIV => 7,
      RX_CLKMUX_PD => '1',
      RX_CM_SEL => B"11",
      RX_CM_TRIM => B"1010",
      RX_DATA_WIDTH => 40,
      RX_DDI_SEL => B"000000",
      RX_DEBUG_CFG => B"00000000000000",
      RX_DEFER_RESET_BUF_EN => "TRUE",
      RX_DFELPM_CFG0 => B"0110",
      RX_DFELPM_CFG1 => '0',
      RX_DFELPM_KLKH_AGC_STUP_EN => '1',
      RX_DFE_AGC_CFG0 => B"00",
      RX_DFE_AGC_CFG1 => B"010",
      RX_DFE_AGC_CFG2 => B"0000",
      RX_DFE_AGC_OVRDEN => '1',
      RX_DFE_GAIN_CFG => X"0020C0",
      RX_DFE_H2_CFG => B"000000000000",
      RX_DFE_H3_CFG => B"000001000000",
      RX_DFE_H4_CFG => B"00011100000",
      RX_DFE_H5_CFG => B"00011100000",
      RX_DFE_H6_CFG => B"00000100000",
      RX_DFE_H7_CFG => B"00000100000",
      RX_DFE_KL_CFG => B"001000001000000000000001100010000",
      RX_DFE_KL_LPM_KH_CFG0 => B"01",
      RX_DFE_KL_LPM_KH_CFG1 => B"010",
      RX_DFE_KL_LPM_KH_CFG2 => B"0010",
      RX_DFE_KL_LPM_KH_OVRDEN => '1',
      RX_DFE_KL_LPM_KL_CFG0 => B"01",
      RX_DFE_KL_LPM_KL_CFG1 => B"010",
      RX_DFE_KL_LPM_KL_CFG2 => B"0010",
      RX_DFE_KL_LPM_KL_OVRDEN => '1',
      RX_DFE_LPM_CFG => X"0080",
      RX_DFE_LPM_HOLD_DURING_EIDLE => '0',
      RX_DFE_ST_CFG => X"00E100000C003F",
      RX_DFE_UT_CFG => B"00011100000000000",
      RX_DFE_VP_CFG => B"00011101010100011",
      RX_DISPERR_SEQ_MATCH => "TRUE",
      RX_INT_DATAWIDTH => 1,
      RX_OS_CFG => B"0000010000000",
      RX_SIG_VALID_DLY => 10,
      RX_XCLK_SEL => "RXREC",
      SAS_MAX_COM => 64,
      SAS_MIN_COM => 36,
      SATA_BURST_SEQ_LEN => B"0101",
      SATA_BURST_VAL => B"111",
      SATA_CPLL_CFG => "VCO_3000MHZ",
      SATA_EIDLE_VAL => B"111",
      SATA_MAX_BURST => 8,
      SATA_MAX_INIT => 21,
      SATA_MAX_WAKE => 7,
      SATA_MIN_BURST => 4,
      SATA_MIN_INIT => 12,
      SATA_MIN_WAKE => 4,
      SHOW_REALIGN_COMMA => "TRUE",
      SIM_CPLLREFCLK_SEL => B"001",
      SIM_RECEIVER_DETECT_PASS => "TRUE",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_TX_EIDLE_DRIVE_LEVEL => "X",
      SIM_VERSION => "2.0",
      TERM_RCAL_CFG => B"100001000010000",
      TERM_RCAL_OVRD => B"000",
      TRANS_TIME_RATE => X"0E",
      TST_RSV => X"00000000",
      TXBUF_EN => "TRUE",
      TXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      TXDLY_CFG => X"001F",
      TXDLY_LCFG => X"030",
      TXDLY_TAP_CFG => X"0000",
      TXGEARBOX_EN => "FALSE",
      TXOOB_CFG => '0',
      TXOUT_DIV => 1,
      TXPCSRESET_TIME => B"00001",
      TXPHDLY_CFG => X"084020",
      TXPH_CFG => X"0780",
      TXPH_MONITOR_SEL => B"00000",
      TXPI_CFG0 => B"00",
      TXPI_CFG1 => B"00",
      TXPI_CFG2 => B"00",
      TXPI_CFG3 => '0',
      TXPI_CFG4 => '0',
      TXPI_CFG5 => B"100",
      TXPI_GREY_SEL => '0',
      TXPI_INVSTROBE_SEL => '0',
      TXPI_PPMCLK_SEL => "TXUSRCLK2",
      TXPI_PPM_CFG => B"00000000",
      TXPI_SYNFREQ_PPM => B"001",
      TXPMARESET_TIME => B"00001",
      TXSYNC_MULTILANE => '0',
      TXSYNC_OVRD => '0',
      TXSYNC_SKIP_DA => '0',
      TX_CLK25_DIV => 7,
      TX_CLKMUX_PD => '1',
      TX_DATA_WIDTH => 40,
      TX_DEEMPH0 => B"000000",
      TX_DEEMPH1 => B"000000",
      TX_DRIVE_MODE => "DIRECT",
      TX_EIDLE_ASSERT_DELAY => B"110",
      TX_EIDLE_DEASSERT_DELAY => B"100",
      TX_INT_DATAWIDTH => 1,
      TX_LOOPBACK_DRIVE_HIZ => "FALSE",
      TX_MAINCURSOR_SEL => '0',
      TX_MARGIN_FULL_0 => B"1001110",
      TX_MARGIN_FULL_1 => B"1001001",
      TX_MARGIN_FULL_2 => B"1000101",
      TX_MARGIN_FULL_3 => B"1000010",
      TX_MARGIN_FULL_4 => B"1000000",
      TX_MARGIN_LOW_0 => B"1000110",
      TX_MARGIN_LOW_1 => B"1000100",
      TX_MARGIN_LOW_2 => B"1000010",
      TX_MARGIN_LOW_3 => B"1000000",
      TX_MARGIN_LOW_4 => B"1000000",
      TX_QPI_STATUS_EN => '0',
      TX_RXDETECT_CFG => X"1832",
      TX_RXDETECT_PRECHARGE_TIME => X"155CC",
      TX_RXDETECT_REF => B"100",
      TX_XCLK_SEL => "TXOUT",
      UCODEER_CLR => '0',
      USE_PCS_CLK_PHASE_SEL => '0'
    )
        port map (
      CFGRESET => '0',
      CLKRSVD0 => '0',
      CLKRSVD1 => '0',
      CPLLFBCLKLOST => gt0_cpllfbclklost_out,
      CPLLLOCK => gt0_cplllock_out,
      CPLLLOCKDETCLK => gt0_cplllockdetclk_in,
      CPLLLOCKEN => '1',
      CPLLPD => cpllpd_in,
      CPLLREFCLKLOST => gthe2_i_n_2,
      CPLLREFCLKSEL(2 downto 0) => B"001",
      CPLLRESET => cpllreset_in,
      DMONFIFORESET => '0',
      DMONITORCLK => '0',
      DMONITOROUT(14 downto 0) => gt0_dmonitorout_out(14 downto 0),
      DRPADDR(8 downto 0) => gt0_drpaddr_in(8 downto 0),
      DRPCLK => gt0_drpclk_in,
      DRPDI(15 downto 0) => gt0_drpdi_in(15 downto 0),
      DRPDO(15 downto 0) => gt0_drpdo_out(15 downto 0),
      DRPEN => gt0_drpen_in,
      DRPRDY => gt0_drprdy_out,
      DRPWE => gt0_drpwe_in,
      EYESCANDATAERROR => gt0_eyescandataerror_out,
      EYESCANMODE => '0',
      EYESCANRESET => gt0_eyescanreset_in,
      EYESCANTRIGGER => gt0_eyescantrigger_in,
      GTGREFCLK => '0',
      GTHRXN => gt0_gthrxn_in,
      GTHRXP => gt0_gthrxp_in,
      GTHTXN => gt0_gthtxn_out,
      GTHTXP => gt0_gthtxp_out,
      GTNORTHREFCLK0 => gt0_gtnorthrefclk0_in,
      GTNORTHREFCLK1 => gt0_gtnorthrefclk1_in,
      GTREFCLK0 => gt0_gtrefclk0_in,
      GTREFCLK1 => gt0_gtrefclk1_in,
      GTREFCLKMONITOR => NLW_gthe2_i_GTREFCLKMONITOR_UNCONNECTED,
      GTRESETSEL => '0',
      GTRSVD(15 downto 0) => B"0000000000000000",
      GTRXRESET => SR(0),
      GTSOUTHREFCLK0 => gt0_gtsouthrefclk0_in,
      GTSOUTHREFCLK1 => gt0_gtsouthrefclk1_in,
      GTTXRESET => gt0_gttxreset_in1_out,
      LOOPBACK(2 downto 0) => gt0_loopback_in(2 downto 0),
      PCSRSVDIN(15 downto 0) => B"0000000000000000",
      PCSRSVDIN2(4 downto 0) => B"00000",
      PCSRSVDOUT(15 downto 0) => NLW_gthe2_i_PCSRSVDOUT_UNCONNECTED(15 downto 0),
      PHYSTATUS => NLW_gthe2_i_PHYSTATUS_UNCONNECTED,
      PMARSVDIN(4 downto 0) => B"00000",
      QPLLCLK => GT0_QPLLOUTCLK_IN,
      QPLLREFCLK => GT0_QPLLOUTREFCLK_IN,
      RESETOVRD => '0',
      RSOSINTDONE => NLW_gthe2_i_RSOSINTDONE_UNCONNECTED,
      RX8B10BEN => '1',
      RXADAPTSELTEST(13 downto 0) => B"00000000000000",
      RXBUFRESET => gt0_rxbufreset_in,
      RXBUFSTATUS(2 downto 0) => gt0_rxbufstatus_out(2 downto 0),
      RXBYTEISALIGNED => gt0_rxbyteisaligned_out,
      RXBYTEREALIGN => gt0_rxbyterealign_out,
      RXCDRFREQRESET => '0',
      RXCDRHOLD => gt0_rxcdrhold_in,
      RXCDRLOCK => NLW_gthe2_i_RXCDRLOCK_UNCONNECTED,
      RXCDROVRDEN => '0',
      RXCDRRESET => '0',
      RXCDRRESETRSV => '0',
      RXCHANBONDSEQ => NLW_gthe2_i_RXCHANBONDSEQ_UNCONNECTED,
      RXCHANISALIGNED => NLW_gthe2_i_RXCHANISALIGNED_UNCONNECTED,
      RXCHANREALIGN => NLW_gthe2_i_RXCHANREALIGN_UNCONNECTED,
      RXCHARISCOMMA(7 downto 4) => NLW_gthe2_i_RXCHARISCOMMA_UNCONNECTED(7 downto 4),
      RXCHARISCOMMA(3 downto 0) => gt0_rxchariscomma_out(3 downto 0),
      RXCHARISK(7 downto 4) => NLW_gthe2_i_RXCHARISK_UNCONNECTED(7 downto 4),
      RXCHARISK(3 downto 0) => gt0_rxcharisk_out(3 downto 0),
      RXCHBONDEN => '0',
      RXCHBONDI(4 downto 0) => B"00000",
      RXCHBONDLEVEL(2 downto 0) => B"000",
      RXCHBONDMASTER => '0',
      RXCHBONDO(4 downto 0) => NLW_gthe2_i_RXCHBONDO_UNCONNECTED(4 downto 0),
      RXCHBONDSLAVE => '0',
      RXCLKCORCNT(1 downto 0) => NLW_gthe2_i_RXCLKCORCNT_UNCONNECTED(1 downto 0),
      RXCOMINITDET => NLW_gthe2_i_RXCOMINITDET_UNCONNECTED,
      RXCOMMADET => gt0_rxcommadet_out,
      RXCOMMADETEN => '1',
      RXCOMSASDET => NLW_gthe2_i_RXCOMSASDET_UNCONNECTED,
      RXCOMWAKEDET => NLW_gthe2_i_RXCOMWAKEDET_UNCONNECTED,
      RXDATA(63 downto 32) => NLW_gthe2_i_RXDATA_UNCONNECTED(63 downto 32),
      RXDATA(31 downto 0) => gt0_rxdata_out(31 downto 0),
      RXDATAVALID(1 downto 0) => NLW_gthe2_i_RXDATAVALID_UNCONNECTED(1 downto 0),
      RXDDIEN => '0',
      RXDFEAGCHOLD => '0',
      RXDFEAGCOVRDEN => '1',
      RXDFEAGCTRL(4 downto 0) => B"10000",
      RXDFECM1EN => '0',
      RXDFELFHOLD => '0',
      RXDFELFOVRDEN => '0',
      RXDFELPMRESET => gt0_rxdfelpmreset_in,
      RXDFESLIDETAP(4 downto 0) => B"00000",
      RXDFESLIDETAPADAPTEN => '0',
      RXDFESLIDETAPHOLD => '0',
      RXDFESLIDETAPID(5 downto 0) => B"000000",
      RXDFESLIDETAPINITOVRDEN => '0',
      RXDFESLIDETAPONLYADAPTEN => '0',
      RXDFESLIDETAPOVRDEN => '0',
      RXDFESLIDETAPSTARTED => NLW_gthe2_i_RXDFESLIDETAPSTARTED_UNCONNECTED,
      RXDFESLIDETAPSTROBE => '0',
      RXDFESLIDETAPSTROBEDONE => NLW_gthe2_i_RXDFESLIDETAPSTROBEDONE_UNCONNECTED,
      RXDFESLIDETAPSTROBESTARTED => NLW_gthe2_i_RXDFESLIDETAPSTROBESTARTED_UNCONNECTED,
      RXDFESTADAPTDONE => NLW_gthe2_i_RXDFESTADAPTDONE_UNCONNECTED,
      RXDFETAP2HOLD => '0',
      RXDFETAP2OVRDEN => '0',
      RXDFETAP3HOLD => '0',
      RXDFETAP3OVRDEN => '0',
      RXDFETAP4HOLD => '0',
      RXDFETAP4OVRDEN => '0',
      RXDFETAP5HOLD => '0',
      RXDFETAP5OVRDEN => '0',
      RXDFETAP6HOLD => '0',
      RXDFETAP6OVRDEN => '0',
      RXDFETAP7HOLD => '0',
      RXDFETAP7OVRDEN => '0',
      RXDFEUTHOLD => '0',
      RXDFEUTOVRDEN => '0',
      RXDFEVPHOLD => '0',
      RXDFEVPOVRDEN => '0',
      RXDFEVSEN => '0',
      RXDFEXYDEN => '1',
      RXDISPERR(7 downto 4) => NLW_gthe2_i_RXDISPERR_UNCONNECTED(7 downto 4),
      RXDISPERR(3 downto 0) => gt0_rxdisperr_out(3 downto 0),
      RXDLYBYPASS => '1',
      RXDLYEN => '0',
      RXDLYOVRDEN => '0',
      RXDLYSRESET => '0',
      RXDLYSRESETDONE => NLW_gthe2_i_RXDLYSRESETDONE_UNCONNECTED,
      RXELECIDLE => NLW_gthe2_i_RXELECIDLE_UNCONNECTED,
      RXELECIDLEMODE(1 downto 0) => B"11",
      RXGEARBOXSLIP => '0',
      RXHEADER(5 downto 0) => NLW_gthe2_i_RXHEADER_UNCONNECTED(5 downto 0),
      RXHEADERVALID(1 downto 0) => NLW_gthe2_i_RXHEADERVALID_UNCONNECTED(1 downto 0),
      RXLPMEN => gt0_rxlpmen_in,
      RXLPMHFHOLD => '0',
      RXLPMHFOVRDEN => '0',
      RXLPMLFHOLD => '0',
      RXLPMLFKLOVRDEN => '0',
      RXMCOMMAALIGNEN => gt0_rxmcommaalignen_in,
      RXMONITOROUT(6 downto 0) => gt0_rxmonitorout_out(6 downto 0),
      RXMONITORSEL(1 downto 0) => gt0_rxmonitorsel_in(1 downto 0),
      RXNOTINTABLE(7 downto 4) => NLW_gthe2_i_RXNOTINTABLE_UNCONNECTED(7 downto 4),
      RXNOTINTABLE(3 downto 0) => gt0_rxnotintable_out(3 downto 0),
      RXOOBRESET => '0',
      RXOSCALRESET => '0',
      RXOSHOLD => '0',
      RXOSINTCFG(3 downto 0) => B"0110",
      RXOSINTEN => '1',
      RXOSINTHOLD => '0',
      RXOSINTID0(3 downto 0) => B"0000",
      RXOSINTNTRLEN => '0',
      RXOSINTOVRDEN => '0',
      RXOSINTSTARTED => NLW_gthe2_i_RXOSINTSTARTED_UNCONNECTED,
      RXOSINTSTROBE => '0',
      RXOSINTSTROBEDONE => NLW_gthe2_i_RXOSINTSTROBEDONE_UNCONNECTED,
      RXOSINTSTROBESTARTED => NLW_gthe2_i_RXOSINTSTROBESTARTED_UNCONNECTED,
      RXOSINTTESTOVRDEN => '0',
      RXOSOVRDEN => '0',
      RXOUTCLK => gt0_rxoutclk_out,
      RXOUTCLKFABRIC => gt0_rxoutclkfabric_out,
      RXOUTCLKPCS => NLW_gthe2_i_RXOUTCLKPCS_UNCONNECTED,
      RXOUTCLKSEL(2 downto 0) => B"010",
      RXPCOMMAALIGNEN => gt0_rxpcommaalignen_in,
      RXPCSRESET => gt0_rxpcsreset_in,
      RXPD(1 downto 0) => gt0_rxpd_in(1 downto 0),
      RXPHALIGN => '0',
      RXPHALIGNDONE => NLW_gthe2_i_RXPHALIGNDONE_UNCONNECTED,
      RXPHALIGNEN => '0',
      RXPHDLYPD => '0',
      RXPHDLYRESET => '0',
      RXPHMONITOR(4 downto 0) => NLW_gthe2_i_RXPHMONITOR_UNCONNECTED(4 downto 0),
      RXPHOVRDEN => '0',
      RXPHSLIPMONITOR(4 downto 0) => NLW_gthe2_i_RXPHSLIPMONITOR_UNCONNECTED(4 downto 0),
      RXPMARESET => gt0_rxpmareset_in,
      RXPMARESETDONE => gt0_rxpmaresetdone_i,
      RXPOLARITY => gt0_rxpolarity_in,
      RXPRBSCNTRESET => gt0_rxprbscntreset_in,
      RXPRBSERR => gt0_rxprbserr_out,
      RXPRBSSEL(2 downto 0) => gt0_rxprbssel_in(2 downto 0),
      RXQPIEN => '0',
      RXQPISENN => NLW_gthe2_i_RXQPISENN_UNCONNECTED,
      RXQPISENP => NLW_gthe2_i_RXQPISENP_UNCONNECTED,
      RXRATE(2 downto 0) => B"000",
      RXRATEDONE => NLW_gthe2_i_RXRATEDONE_UNCONNECTED,
      RXRATEMODE => '0',
      RXRESETDONE => \^gt0_rxresetdone_out\,
      RXSLIDE => '0',
      RXSTARTOFSEQ(1 downto 0) => NLW_gthe2_i_RXSTARTOFSEQ_UNCONNECTED(1 downto 0),
      RXSTATUS(2 downto 0) => gt0_rxstatus_out(2 downto 0),
      RXSYNCALLIN => '0',
      RXSYNCDONE => NLW_gthe2_i_RXSYNCDONE_UNCONNECTED,
      RXSYNCIN => '0',
      RXSYNCMODE => '0',
      RXSYNCOUT => NLW_gthe2_i_RXSYNCOUT_UNCONNECTED,
      RXSYSCLKSEL(1 downto 0) => gt0_rxsysclksel_in(1 downto 0),
      RXUSERRDY => gt0_rxuserrdy_in3_out,
      RXUSRCLK => gt0_rxusrclk_in,
      RXUSRCLK2 => gt0_rxusrclk2_in,
      RXVALID => NLW_gthe2_i_RXVALID_UNCONNECTED,
      SETERRSTATUS => '0',
      SIGVALIDCLK => '0',
      TSTIN(19 downto 0) => B"11111111111111111111",
      TX8B10BBYPASS(7 downto 0) => B"00000000",
      TX8B10BEN => '1',
      TXBUFDIFFCTRL(2 downto 0) => B"100",
      TXBUFSTATUS(1 downto 0) => gt0_txbufstatus_out(1 downto 0),
      TXCHARDISPMODE(7 downto 0) => B"00000000",
      TXCHARDISPVAL(7 downto 0) => B"00000000",
      TXCHARISK(7 downto 4) => B"0000",
      TXCHARISK(3 downto 0) => gt0_txcharisk_in(3 downto 0),
      TXCOMFINISH => NLW_gthe2_i_TXCOMFINISH_UNCONNECTED,
      TXCOMINIT => '0',
      TXCOMSAS => '0',
      TXCOMWAKE => '0',
      TXDATA(63 downto 32) => B"00000000000000000000000000000000",
      TXDATA(31 downto 0) => gt0_txdata_in(31 downto 0),
      TXDEEMPH => '0',
      TXDETECTRX => '0',
      TXDIFFCTRL(3 downto 0) => gt0_txdiffctrl_in(3 downto 0),
      TXDIFFPD => '0',
      TXDLYBYPASS => '1',
      TXDLYEN => '0',
      TXDLYHOLD => '0',
      TXDLYOVRDEN => '0',
      TXDLYSRESET => '0',
      TXDLYSRESETDONE => NLW_gthe2_i_TXDLYSRESETDONE_UNCONNECTED,
      TXDLYUPDOWN => '0',
      TXELECIDLE => '0',
      TXGEARBOXREADY => NLW_gthe2_i_TXGEARBOXREADY_UNCONNECTED,
      TXHEADER(2 downto 0) => B"000",
      TXINHIBIT => gt0_txinhibit_in,
      TXMAINCURSOR(6 downto 0) => B"0000000",
      TXMARGIN(2 downto 0) => B"000",
      TXOUTCLK => gt0_txoutclk_out,
      TXOUTCLKFABRIC => gt0_txoutclkfabric_out,
      TXOUTCLKPCS => gt0_txoutclkpcs_out,
      TXOUTCLKSEL(2 downto 0) => B"010",
      TXPCSRESET => gt0_txpcsreset_in,
      TXPD(1 downto 0) => gt0_txpd_in(1 downto 0),
      TXPDELECIDLEMODE => '0',
      TXPHALIGN => '0',
      TXPHALIGNDONE => NLW_gthe2_i_TXPHALIGNDONE_UNCONNECTED,
      TXPHALIGNEN => '0',
      TXPHDLYPD => '0',
      TXPHDLYRESET => '0',
      TXPHDLYTSTCLK => '0',
      TXPHINIT => '0',
      TXPHINITDONE => NLW_gthe2_i_TXPHINITDONE_UNCONNECTED,
      TXPHOVRDEN => '0',
      TXPIPPMEN => '0',
      TXPIPPMOVRDEN => '0',
      TXPIPPMPD => '0',
      TXPIPPMSEL => '1',
      TXPIPPMSTEPSIZE(4 downto 0) => B"00000",
      TXPISOPD => '0',
      TXPMARESET => gt0_txpmareset_in,
      TXPMARESETDONE => gthe2_i_n_50,
      TXPOLARITY => gt0_txpolarity_in,
      TXPOSTCURSOR(4 downto 0) => gt0_txpostcursor_in(4 downto 0),
      TXPOSTCURSORINV => '0',
      TXPRBSFORCEERR => gt0_txprbsforceerr_in,
      TXPRBSSEL(2 downto 0) => gt0_txprbssel_in(2 downto 0),
      TXPRECURSOR(4 downto 0) => gt0_txprecursor_in(4 downto 0),
      TXPRECURSORINV => '0',
      TXQPIBIASEN => '0',
      TXQPISENN => NLW_gthe2_i_TXQPISENN_UNCONNECTED,
      TXQPISENP => NLW_gthe2_i_TXQPISENP_UNCONNECTED,
      TXQPISTRONGPDOWN => '0',
      TXQPIWEAKPUP => '0',
      TXRATE(2 downto 0) => B"000",
      TXRATEDONE => NLW_gthe2_i_TXRATEDONE_UNCONNECTED,
      TXRATEMODE => '0',
      TXRESETDONE => \^gt0_txresetdone_out\,
      TXSEQUENCE(6 downto 0) => B"0000000",
      TXSTARTSEQ => '0',
      TXSWING => '0',
      TXSYNCALLIN => '0',
      TXSYNCDONE => NLW_gthe2_i_TXSYNCDONE_UNCONNECTED,
      TXSYNCIN => '0',
      TXSYNCMODE => '0',
      TXSYNCOUT => NLW_gthe2_i_TXSYNCOUT_UNCONNECTED,
      TXSYSCLKSEL(1 downto 0) => gt0_txsysclksel_in(1 downto 0),
      TXUSERRDY => gt0_txuserrdy_in0_out,
      TXUSRCLK => gt0_txusrclk_in,
      TXUSRCLK2 => gt0_txusrclk2_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_cpll_railing is
  port (
    cpllreset_in : out STD_LOGIC;
    cpllpd_in : out STD_LOGIC;
    gt0_gtrefclk0_in : in STD_LOGIC;
    p_5_in : in STD_LOGIC;
    gt0_cpllreset_in : in STD_LOGIC;
    p_4_in : in STD_LOGIC;
    gt0_cpllpd_in : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_cpll_railing;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_cpll_railing is
  signal cpll_pd_out : STD_LOGIC;
  signal cpll_reset_out : STD_LOGIC;
  signal \cpllpd_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[94]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[126]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[95]_srl32_n_1\ : STD_LOGIC;
  signal \use_bufh_cpll.refclk_buf_n_0\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \cpllpd_wait_reg[31]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name : string;
  attribute srl_name of \cpllpd_wait_reg[31]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[63]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[63]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[94]_srl31\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[94]_srl31\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 ";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \cpllpd_wait_reg[95]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[126]_srl31\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[126]_srl31\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 ";
  attribute equivalent_register_removal of \cpllreset_wait_reg[127]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[31]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[31]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[63]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[63]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[95]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[95]_srl32\ : label is "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 ";
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of \use_bufh_cpll.refclk_buf\ : label is "PRIMITIVE";
begin
\cpllpd_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => \use_bufh_cpll.refclk_buf_n_0\,
      D => '0',
      Q => \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[31]_srl32_n_1\
    );
\cpllpd_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => \use_bufh_cpll.refclk_buf_n_0\,
      D => \cpllpd_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[63]_srl32_n_1\
    );
\cpllpd_wait_reg[94]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => \use_bufh_cpll.refclk_buf_n_0\,
      D => \cpllpd_wait_reg[63]_srl32_n_1\,
      Q => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q31 => \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\
    );
\cpllpd_wait_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => \use_bufh_cpll.refclk_buf_n_0\,
      CE => '1',
      D => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q => cpll_pd_out,
      R => '0'
    );
\cpllreset_wait_reg[126]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => \use_bufh_cpll.refclk_buf_n_0\,
      D => \cpllreset_wait_reg[95]_srl32_n_1\,
      Q => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q31 => \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\
    );
\cpllreset_wait_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \use_bufh_cpll.refclk_buf_n_0\,
      CE => '1',
      D => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q => cpll_reset_out,
      R => '0'
    );
\cpllreset_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"000000FF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => \use_bufh_cpll.refclk_buf_n_0\,
      D => '0',
      Q => \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[31]_srl32_n_1\
    );
\cpllreset_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => \use_bufh_cpll.refclk_buf_n_0\,
      D => \cpllreset_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[63]_srl32_n_1\
    );
\cpllreset_wait_reg[95]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => \use_bufh_cpll.refclk_buf_n_0\,
      D => \cpllreset_wait_reg[63]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[95]_srl32_n_1\
    );
gthe2_i_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cpll_pd_out,
      I1 => gt0_cpllpd_in,
      O => cpllpd_in
    );
gthe2_i_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cpll_reset_out,
      I1 => p_5_in,
      I2 => gt0_cpllreset_in,
      I3 => p_4_in,
      O => cpllreset_in
    );
\use_bufh_cpll.refclk_buf\: unisim.vcomponents.BUFH
     port map (
      I => gt0_gtrefclk0_in,
      O => \use_bufh_cpll.refclk_buf_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block is
  port (
    data_out : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_sync_reg6_0 : in STD_LOGIC;
    time_out_2ms_reg : in STD_LOGIC;
    pll_reset_asserted_reg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \wait_time_cnt_reg[6]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_tx_state_reg[1]\ : in STD_LOGIC;
    init_wait_done_reg : in STD_LOGIC;
    reset_time_out_reg : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    gt0_cplllock_out : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block is
  signal \FSM_sequential_tx_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state_reg[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state_reg[3]_i_7_n_0\ : STD_LOGIC;
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal tx_state04_out : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
  data_out <= \^data_out\;
\FSM_sequential_tx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00338BBB00338B88"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[3]_i_3_n_0\,
      I1 => \out\(0),
      I2 => \wait_time_cnt_reg[6]\(0),
      I3 => \FSM_sequential_tx_state_reg[1]\,
      I4 => \out\(3),
      I5 => init_wait_done_reg,
      O => E(0)
    );
\FSM_sequential_tx_state[3]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFE2"
    )
        port map (
      I0 => \^data_out\,
      I1 => gt0_txsysclksel_in(0),
      I2 => data_sync_reg6_0,
      I3 => time_out_2ms_reg,
      O => \FSM_sequential_tx_state[3]_i_10_n_0\
    );
\FSM_sequential_tx_state[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => pll_reset_asserted_reg,
      I1 => \^data_out\,
      I2 => gt0_txsysclksel_in(0),
      I3 => data_sync_reg6_0,
      O => tx_state04_out
    );
\FSM_sequential_tx_state_reg[3]_i_3\: unisim.vcomponents.MUXF8
     port map (
      I0 => \FSM_sequential_tx_state_reg[3]_i_6_n_0\,
      I1 => \FSM_sequential_tx_state_reg[3]_i_7_n_0\,
      O => \FSM_sequential_tx_state_reg[3]_i_3_n_0\,
      S => \out\(1)
    );
\FSM_sequential_tx_state_reg[3]_i_6\: unisim.vcomponents.MUXF7
     port map (
      I0 => tx_state04_out,
      I1 => reset_time_out_reg,
      O => \FSM_sequential_tx_state_reg[3]_i_6_n_0\,
      S => \out\(2)
    );
\FSM_sequential_tx_state_reg[3]_i_7\: unisim.vcomponents.MUXF7
     port map (
      I0 => \FSM_sequential_tx_state[3]_i_10_n_0\,
      I1 => reset_time_out_reg_0,
      O => \FSM_sequential_tx_state_reg[3]_i_7_n_0\,
      S => \out\(2)
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => gt0_cplllock_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_0 is
  port (
    data_out : out STD_LOGIC;
    reset_time_out_reg : out STD_LOGIC;
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_sync_reg6_0 : in STD_LOGIC;
    reset_time_out : in STD_LOGIC;
    txresetdone_s3_reg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    init_wait_done_reg : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_0 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_0 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal reset_time_out_1 : STD_LOGIC;
  signal reset_time_out_i_3_n_0 : STD_LOGIC;
  signal tx_state0 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => '1',
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
reset_time_out_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => reset_time_out_1,
      I1 => reset_time_out_i_3_n_0,
      I2 => reset_time_out,
      O => reset_time_out_reg
    );
reset_time_out_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00CFC0EF00C0C0E0"
    )
        port map (
      I0 => tx_state0,
      I1 => txresetdone_s3_reg,
      I2 => \out\(0),
      I3 => \out\(3),
      I4 => \out\(2),
      I5 => init_wait_done_reg,
      O => reset_time_out_1
    );
reset_time_out_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"303030302020FFFC"
    )
        port map (
      I0 => tx_state0,
      I1 => \out\(3),
      I2 => \out\(0),
      I3 => init_wait_done_reg,
      I4 => \out\(1),
      I5 => \out\(2),
      O => reset_time_out_i_3_n_0
    );
\reset_time_out_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^data_out\,
      I1 => gt0_txsysclksel_in(0),
      I2 => data_sync_reg6_0,
      O => tx_state0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_1 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_1 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_1 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_10 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    mmcm_lock_reclocked_reg : out STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mmcm_lock_count_reg[4]\ : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_10 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_10;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_10 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal mmcm_lock_i : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_1__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \mmcm_lock_reclocked_i_1__0\ : label is "soft_lutpair2";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => '1',
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => mmcm_lock_i,
      R => '0'
    );
\mmcm_lock_count[7]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_i,
      O => SR(0)
    );
\mmcm_lock_reclocked_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \mmcm_lock_count_reg[4]\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_11 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    gt0_rxusrclk_in : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_11 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_11;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_11 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_12 is
  port (
    data_out : out STD_LOGIC;
    GT_RX_FSM_RESET_DONE_OUT : in STD_LOGIC;
    gt0_rxusrclk_in : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_12 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_12;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_12 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => GT_RX_FSM_RESET_DONE_OUT,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_13 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_13 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_13 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_2 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    mmcm_lock_reclocked_reg : out STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mmcm_lock_count_reg[4]\ : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_2 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_2 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal mmcm_lock_i : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of mmcm_lock_reclocked_i_1 : label is "soft_lutpair12";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => '1',
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => mmcm_lock_i,
      R => '0'
    );
\mmcm_lock_count[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_i,
      O => SR(0)
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \mmcm_lock_count_reg[4]\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_3 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    gt0_txusrclk_in : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_3 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_3 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_4 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_4 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_4 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_5 is
  port (
    data_out : out STD_LOGIC;
    GT_TX_FSM_RESET_DONE_OUT : in STD_LOGIC;
    gt0_txusrclk_in : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_5 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_5 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => GT_TX_FSM_RESET_DONE_OUT,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_6 is
  port (
    data_out : out STD_LOGIC;
    gt0_cplllock_out : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_6 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_6;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_6 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => gt0_cplllock_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_7 is
  port (
    rx_state01_out : out STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]\ : out STD_LOGIC;
    reset_time_out_reg : out STD_LOGIC;
    gt0_rxsysclksel_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : in STD_LOGIC;
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    pll_reset_asserted_reg : in STD_LOGIC;
    time_out_2ms_reg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rxresetdone_s3 : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    gt0_rx_cdrlocked_reg : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[3]\ : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC;
    gt0_rx_cdrlocked_reg_0 : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_7 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_7;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_7 is
  signal \FSM_sequential_rx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal qplllock_sync : STD_LOGIC;
  signal \reset_time_out_i_3__0_n_0\ : STD_LOGIC;
  signal \reset_time_out_i_5__0_n_0\ : STD_LOGIC;
  signal \^rx_state01_out\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_8\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_9\ : label is "soft_lutpair0";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
  rx_state01_out <= \^rx_state01_out\;
\FSM_sequential_rx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FE0EFEAE"
    )
        port map (
      I0 => time_out_2ms_reg,
      I1 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I2 => \out\(2),
      I3 => rxresetdone_s3,
      I4 => reset_time_out_reg_0,
      I5 => \out\(3),
      O => \FSM_sequential_rx_state_reg[0]\
    );
\FSM_sequential_rx_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => qplllock_sync,
      I1 => gt0_rxsysclksel_in(0),
      I2 => data_out,
      O => \FSM_sequential_rx_state[3]_i_8_n_0\
    );
\FSM_sequential_rx_state[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A7A5F7A5"
    )
        port map (
      I0 => gt0_rxsysclksel_in(0),
      I1 => qplllock_sync,
      I2 => gt0_txsysclksel_in(0),
      I3 => pll_reset_asserted_reg,
      I4 => data_out,
      O => \^rx_state01_out\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => '1',
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => qplllock_sync,
      R => '0'
    );
\reset_time_out_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEAEFFFFFEAE0000"
    )
        port map (
      I0 => gt0_rx_cdrlocked_reg,
      I1 => \reset_time_out_i_3__0_n_0\,
      I2 => \out\(2),
      I3 => \FSM_sequential_rx_state_reg[3]\,
      I4 => \reset_time_out_i_5__0_n_0\,
      I5 => reset_time_out_reg_0,
      O => reset_time_out_reg
    );
\reset_time_out_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"303FBBBF303F888C"
    )
        port map (
      I0 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I1 => \out\(1),
      I2 => data_sync_reg6_0,
      I3 => \out\(0),
      I4 => \out\(3),
      I5 => \^rx_state01_out\,
      O => \reset_time_out_i_3__0_n_0\
    );
\reset_time_out_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F303F380F303C38"
    )
        port map (
      I0 => gt0_rx_cdrlocked_reg_0,
      I1 => \out\(2),
      I2 => \out\(3),
      I3 => \out\(0),
      I4 => \out\(1),
      I5 => \^rx_state01_out\,
      O => \reset_time_out_i_5__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_8 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_8 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_8;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_8 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_9 is
  port (
    data_out : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    DONT_RESET_ON_DATA_ERROR_IN : in STD_LOGIC;
    time_out_100us_reg : in STD_LOGIC;
    reset_time_out_reg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    GT_RX_FSM_RESET_DONE_OUT : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[2]\ : in STD_LOGIC;
    time_out_2ms_reg : in STD_LOGIC;
    gt0_rx_cdrlocked_reg : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \wait_time_cnt_reg[4]\ : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    rx_state128_out : in STD_LOGIC;
    rx_state01_out : in STD_LOGIC;
    time_out_1us_reg : in STD_LOGIC;
    time_out_wait_bypass_s3 : in STD_LOGIC;
    time_out_2ms_reg_0 : in STD_LOGIC;
    GT0_DATA_VALID_IN : in STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_9 : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_9;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_9 is
  signal \FSM_sequential_rx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal rx_fsm_reset_done_int : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_3_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_4_n_0 : STD_LOGIC;
  signal rx_state126_out : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[1]_i_2\ : label is "soft_lutpair1";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg1 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg2 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg3 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg4 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg5 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute BOX_TYPE of data_sync_reg6 : label is "PRIMITIVE";
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_4 : label is "soft_lutpair1";
begin
  data_out <= \^data_out\;
\FSM_sequential_rx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C0C5DFD0C0C5D5D"
    )
        port map (
      I0 => \out\(0),
      I1 => \FSM_sequential_rx_state_reg[2]\,
      I2 => \out\(3),
      I3 => \out\(1),
      I4 => \out\(2),
      I5 => rx_state126_out,
      O => D(0)
    );
\FSM_sequential_rx_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000050000FF7700"
    )
        port map (
      I0 => \out\(2),
      I1 => rx_state128_out,
      I2 => rx_state126_out,
      I3 => \out\(0),
      I4 => \out\(1),
      I5 => \out\(3),
      O => D(1)
    );
\FSM_sequential_rx_state[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => DONT_RESET_ON_DATA_ERROR_IN,
      I1 => time_out_100us_reg,
      I2 => reset_time_out_reg,
      I3 => \^data_out\,
      O => rx_state126_out
    );
\FSM_sequential_rx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => time_out_2ms_reg,
      I1 => \FSM_sequential_rx_state[3]_i_4_n_0\,
      I2 => \out\(0),
      I3 => \FSM_sequential_rx_state[3]_i_5_n_0\,
      I4 => \out\(1),
      I5 => gt0_rx_cdrlocked_reg,
      O => E(0)
    );
\FSM_sequential_rx_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55AA00A2000000A2"
    )
        port map (
      I0 => \out\(3),
      I1 => time_out_wait_bypass_s3,
      I2 => \out\(1),
      I3 => \out\(2),
      I4 => \out\(0),
      I5 => \FSM_sequential_rx_state[3]_i_7_n_0\,
      O => D(2)
    );
\FSM_sequential_rx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F00EFEF0F00E0E0"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => rx_state128_out,
      I2 => \out\(2),
      I3 => rx_fsm_reset_done_int_i_4_n_0,
      I4 => \out\(3),
      I5 => rx_state01_out,
      O => \FSM_sequential_rx_state[3]_i_4_n_0\
    );
\FSM_sequential_rx_state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1010101F"
    )
        port map (
      I0 => \out\(2),
      I1 => \^data_out\,
      I2 => \out\(3),
      I3 => Q(0),
      I4 => \wait_time_cnt_reg[4]\,
      O => \FSM_sequential_rx_state[3]_i_5_n_0\
    );
\FSM_sequential_rx_state[3]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B0B0000F"
    )
        port map (
      I0 => reset_time_out_reg,
      I1 => time_out_2ms_reg_0,
      I2 => \out\(1),
      I3 => rx_state126_out,
      I4 => \out\(2),
      O => \FSM_sequential_rx_state[3]_i_7_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => GT0_DATA_VALID_IN,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
rx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF2000"
    )
        port map (
      I0 => rx_fsm_reset_done_int,
      I1 => \out\(2),
      I2 => \out\(3),
      I3 => rx_fsm_reset_done_int_i_3_n_0,
      I4 => GT_RX_FSM_RESET_DONE_OUT,
      O => rx_fsm_reset_done_int_reg
    );
rx_fsm_reset_done_int_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \^data_out\,
      I1 => time_out_1us_reg,
      I2 => reset_time_out_reg,
      I3 => \out\(0),
      I4 => \out\(2),
      O => rx_fsm_reset_done_int
    );
rx_fsm_reset_done_int_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0838083838380838"
    )
        port map (
      I0 => rx_fsm_reset_done_int_i_4_n_0,
      I1 => \out\(0),
      I2 => \out\(1),
      I3 => \^data_out\,
      I4 => time_out_1us_reg,
      I5 => reset_time_out_reg,
      O => rx_fsm_reset_done_int_i_3_n_0
    );
rx_fsm_reset_done_int_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF04"
    )
        port map (
      I0 => DONT_RESET_ON_DATA_ERROR_IN,
      I1 => time_out_100us_reg,
      I2 => reset_time_out_reg,
      I3 => \^data_out\,
      O => rx_fsm_reset_done_int_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gtwizard_0_common is
  port (
    common0_qpll_clk_in : out STD_LOGIC;
    common0_qpll_refclk_in : out STD_LOGIC;
    cpll_refclk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gtwizard_0_common;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gtwizard_0_common is
  signal gthe2_common_i_n_0 : STD_LOGIC;
  signal gthe2_common_i_n_10 : STD_LOGIC;
  signal gthe2_common_i_n_11 : STD_LOGIC;
  signal gthe2_common_i_n_12 : STD_LOGIC;
  signal gthe2_common_i_n_13 : STD_LOGIC;
  signal gthe2_common_i_n_14 : STD_LOGIC;
  signal gthe2_common_i_n_15 : STD_LOGIC;
  signal gthe2_common_i_n_16 : STD_LOGIC;
  signal gthe2_common_i_n_17 : STD_LOGIC;
  signal gthe2_common_i_n_18 : STD_LOGIC;
  signal gthe2_common_i_n_19 : STD_LOGIC;
  signal gthe2_common_i_n_2 : STD_LOGIC;
  signal gthe2_common_i_n_20 : STD_LOGIC;
  signal gthe2_common_i_n_21 : STD_LOGIC;
  signal gthe2_common_i_n_22 : STD_LOGIC;
  signal gthe2_common_i_n_5 : STD_LOGIC;
  signal gthe2_common_i_n_7 : STD_LOGIC;
  signal gthe2_common_i_n_8 : STD_LOGIC;
  signal gthe2_common_i_n_9 : STD_LOGIC;
  signal NLW_gthe2_common_i_QPLLFBCLKLOST_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_common_i_REFCLKOUTMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gthe2_common_i_PMARSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gthe2_common_i_QPLLDMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of gthe2_common_i : label is "PRIMITIVE";
begin
gthe2_common_i: unisim.vcomponents.GTHE2_COMMON
    generic map(
      BIAS_CFG => X"0000040000001050",
      COMMON_CFG => X"0000005C",
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_QPLLLOCKDETCLK_INVERTED => '0',
      QPLL_CFG => X"04801C7",
      QPLL_CLKOUT_CFG => B"1111",
      QPLL_COARSE_FREQ_OVRD => B"010000",
      QPLL_COARSE_FREQ_OVRD_EN => '0',
      QPLL_CP => B"0000011111",
      QPLL_CP_MONITOR_EN => '0',
      QPLL_DMONITOR_SEL => '0',
      QPLL_FBDIV => B"0010000000",
      QPLL_FBDIV_MONITOR_EN => '0',
      QPLL_FBDIV_RATIO => '1',
      QPLL_INIT_CFG => X"000006",
      QPLL_LOCK_CFG => X"05E8",
      QPLL_LPF => B"1111",
      QPLL_REFCLK_DIV => 1,
      QPLL_RP_COMP => '0',
      QPLL_VTRL_RESET => B"00",
      RCAL_CFG => B"00",
      RSVD_ATTR0 => X"0000",
      RSVD_ATTR1 => X"0000",
      SIM_QPLLREFCLK_SEL => B"001",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_VERSION => "2.0"
    )
        port map (
      BGBYPASSB => '1',
      BGMONITORENB => '1',
      BGPDB => '1',
      BGRCALOVRD(4 downto 0) => B"11111",
      BGRCALOVRDENB => '1',
      DRPADDR(7 downto 0) => B"00000000",
      DRPCLK => '0',
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15) => gthe2_common_i_n_7,
      DRPDO(14) => gthe2_common_i_n_8,
      DRPDO(13) => gthe2_common_i_n_9,
      DRPDO(12) => gthe2_common_i_n_10,
      DRPDO(11) => gthe2_common_i_n_11,
      DRPDO(10) => gthe2_common_i_n_12,
      DRPDO(9) => gthe2_common_i_n_13,
      DRPDO(8) => gthe2_common_i_n_14,
      DRPDO(7) => gthe2_common_i_n_15,
      DRPDO(6) => gthe2_common_i_n_16,
      DRPDO(5) => gthe2_common_i_n_17,
      DRPDO(4) => gthe2_common_i_n_18,
      DRPDO(3) => gthe2_common_i_n_19,
      DRPDO(2) => gthe2_common_i_n_20,
      DRPDO(1) => gthe2_common_i_n_21,
      DRPDO(0) => gthe2_common_i_n_22,
      DRPEN => '0',
      DRPRDY => gthe2_common_i_n_0,
      DRPWE => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => cpll_refclk,
      GTREFCLK1 => '0',
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      PMARSVD(7 downto 0) => B"00000000",
      PMARSVDOUT(15 downto 0) => NLW_gthe2_common_i_PMARSVDOUT_UNCONNECTED(15 downto 0),
      QPLLDMONITOR(7 downto 0) => NLW_gthe2_common_i_QPLLDMONITOR_UNCONNECTED(7 downto 0),
      QPLLFBCLKLOST => NLW_gthe2_common_i_QPLLFBCLKLOST_UNCONNECTED,
      QPLLLOCK => gthe2_common_i_n_2,
      QPLLLOCKDETCLK => '0',
      QPLLLOCKEN => '1',
      QPLLOUTCLK => common0_qpll_clk_in,
      QPLLOUTREFCLK => common0_qpll_refclk_in,
      QPLLOUTRESET => '0',
      QPLLPD => '1',
      QPLLREFCLKLOST => gthe2_common_i_n_5,
      QPLLREFCLKSEL(2 downto 0) => B"001",
      QPLLRESET => '0',
      QPLLRSVD1(15 downto 0) => B"0000000000000000",
      QPLLRSVD2(4 downto 0) => B"11111",
      RCALENB => '1',
      REFCLKOUTMONITOR => NLW_gthe2_common_i_REFCLKOUTMONITOR_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig is
  port (
    timeout_enable : out STD_LOGIC;
    \txpllclksel_reg[1]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \txpllclksel_reg[1]_0\ : out STD_LOGIC;
    \axi_rdata_reg[1]\ : out STD_LOGIC;
    \axi_rdata_reg[2]\ : out STD_LOGIC;
    \axi_rdata_reg[2]_0\ : out STD_LOGIC;
    \rx_pd_0_reg[1]\ : out STD_LOGIC;
    \loopback_0_reg[2]\ : out STD_LOGIC;
    \axi_rdata_reg[0]\ : out STD_LOGIC;
    \timeout_timer_count_reg[11]\ : out STD_LOGIC_VECTOR ( 11 downto 0 );
    \axi_rdata_reg[7]\ : out STD_LOGIC_VECTOR ( 6 downto 0 );
    p_0_in : in STD_LOGIC;
    \slv_addr_reg[2]\ : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    \slv_addr_reg[6]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_addr_reg[5]\ : in STD_LOGIC;
    \slv_rd_addr_reg[1]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 11 downto 0 );
    \slv_addr_reg[2]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_addr_reg[2]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig is
  signal \^q\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \^axi_rdata_reg[2]_0\ : STD_LOGIC;
  signal \cmm_interface_sel_reg_n_0_[0]\ : STD_LOGIC;
  signal \^timeout_enable\ : STD_LOGIC;
  signal \^txpllclksel_reg[1]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_rdata[4]_i_11\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \txpostcursor_0[4]_i_2\ : label is "soft_lutpair28";
begin
  Q(7 downto 0) <= \^q\(7 downto 0);
  \axi_rdata_reg[2]_0\ <= \^axi_rdata_reg[2]_0\;
  timeout_enable <= \^timeout_enable\;
  \txpllclksel_reg[1]_0\ <= \^txpllclksel_reg[1]_0\;
\axi_rdata[0]_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"50035F03"
    )
        port map (
      I0 => \^timeout_enable\,
      I1 => \cmm_interface_sel_reg_n_0_[0]\,
      I2 => \slv_addr_reg[6]\(1),
      I3 => \slv_addr_reg[6]\(0),
      I4 => \^q\(0),
      O => \axi_rdata_reg[0]\
    );
\axi_rdata[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \slv_rd_addr_reg[1]\(0),
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \slv_addr_reg[6]\(3),
      I5 => \^txpllclksel_reg[1]_0\,
      O => \axi_rdata_reg[1]\
    );
\axi_rdata[2]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => \^txpllclksel_reg[1]_0\,
      I1 => \^axi_rdata_reg[2]_0\,
      I2 => \slv_addr_reg[6]\(2),
      I3 => \slv_addr_reg[6]\(1),
      O => \axi_rdata_reg[2]\
    );
\axi_rdata[4]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      I2 => \^q\(6),
      I3 => \^q\(7),
      I4 => \^q\(5),
      O => \^txpllclksel_reg[1]_0\
    );
\axi_rdata[4]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \slv_addr_reg[6]\(3),
      O => \^axi_rdata_reg[2]_0\
    );
\cmm_interface_sel_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_0\(0),
      D => s_axi_wdata(0),
      Q => \cmm_interface_sel_reg_n_0_[0]\,
      R => p_0_in
    );
\cmm_interface_sel_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_0\(0),
      D => s_axi_wdata(1),
      Q => \axi_rdata_reg[7]\(0),
      R => p_0_in
    );
\cmm_interface_sel_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_0\(0),
      D => s_axi_wdata(2),
      Q => \axi_rdata_reg[7]\(1),
      R => p_0_in
    );
\cmm_interface_sel_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_0\(0),
      D => s_axi_wdata(3),
      Q => \axi_rdata_reg[7]\(2),
      R => p_0_in
    );
\cmm_interface_sel_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_0\(0),
      D => s_axi_wdata(4),
      Q => \axi_rdata_reg[7]\(3),
      R => p_0_in
    );
\cmm_interface_sel_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_0\(0),
      D => s_axi_wdata(5),
      Q => \axi_rdata_reg[7]\(4),
      R => p_0_in
    );
\cmm_interface_sel_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_0\(0),
      D => s_axi_wdata(6),
      Q => \axi_rdata_reg[7]\(5),
      R => p_0_in
    );
\cmm_interface_sel_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_0\(0),
      D => s_axi_wdata(7),
      Q => \axi_rdata_reg[7]\(6),
      R => p_0_in
    );
\gt_interface_sel_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_1\(0),
      D => s_axi_wdata(0),
      Q => \^q\(0),
      R => p_0_in
    );
\gt_interface_sel_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_1\(0),
      D => s_axi_wdata(1),
      Q => \^q\(1),
      R => p_0_in
    );
\gt_interface_sel_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_1\(0),
      D => s_axi_wdata(2),
      Q => \^q\(2),
      R => p_0_in
    );
\gt_interface_sel_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_1\(0),
      D => s_axi_wdata(3),
      Q => \^q\(3),
      R => p_0_in
    );
\gt_interface_sel_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_1\(0),
      D => s_axi_wdata(4),
      Q => \^q\(4),
      R => p_0_in
    );
\gt_interface_sel_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_1\(0),
      D => s_axi_wdata(5),
      Q => \^q\(5),
      R => p_0_in
    );
\gt_interface_sel_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_1\(0),
      D => s_axi_wdata(6),
      Q => \^q\(6),
      R => p_0_in
    );
\gt_interface_sel_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[2]_1\(0),
      D => s_axi_wdata(7),
      Q => \^q\(7),
      R => p_0_in
    );
\rx_pd_0[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \slv_addr_reg[6]\(2),
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \slv_addr_reg[6]\(3),
      I5 => \^txpllclksel_reg[1]_0\,
      O => \rx_pd_0_reg[1]\
    );
timeout_enable_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]\,
      Q => \^timeout_enable\,
      S => p_0_in
    );
\timeout_value_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(0),
      Q => \timeout_timer_count_reg[11]\(0),
      R => p_0_in
    );
\timeout_value_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(10),
      Q => \timeout_timer_count_reg[11]\(10),
      R => p_0_in
    );
\timeout_value_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(11),
      Q => \timeout_timer_count_reg[11]\(11),
      R => p_0_in
    );
\timeout_value_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(1),
      Q => \timeout_timer_count_reg[11]\(1),
      R => p_0_in
    );
\timeout_value_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(2),
      Q => \timeout_timer_count_reg[11]\(2),
      R => p_0_in
    );
\timeout_value_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(3),
      Q => \timeout_timer_count_reg[11]\(3),
      R => p_0_in
    );
\timeout_value_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(4),
      Q => \timeout_timer_count_reg[11]\(4),
      R => p_0_in
    );
\timeout_value_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(5),
      Q => \timeout_timer_count_reg[11]\(5),
      R => p_0_in
    );
\timeout_value_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(6),
      Q => \timeout_timer_count_reg[11]\(6),
      R => p_0_in
    );
\timeout_value_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(7),
      Q => \timeout_timer_count_reg[11]\(7),
      S => p_0_in
    );
\timeout_value_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(8),
      Q => \timeout_timer_count_reg[11]\(8),
      R => p_0_in
    );
\timeout_value_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(9),
      Q => \timeout_timer_count_reg[11]\(9),
      R => p_0_in
    );
\txpllclksel[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \slv_addr_reg[6]\(3),
      I4 => \^txpllclksel_reg[1]_0\,
      I5 => \slv_addr_reg[5]\,
      O => \txpllclksel_reg[1]\
    );
\txpostcursor_0[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^txpllclksel_reg[1]_0\,
      I1 => \slv_addr_reg[6]\(3),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(2),
      O => \loopback_0_reg[2]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_axi is
  port (
    chan_tx_axi_map_wready : out STD_LOGIC;
    p_0_in : out STD_LOGIC;
    chan_rx_axi_map_wready : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    gt_slv_rden : out STD_LOGIC;
    chan_tx_slv_rden : out STD_LOGIC;
    chan_rx_slv_rden : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \cmm_interface_sel_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \rx_pd_0_reg[1]\ : out STD_LOGIC;
    chan_async_axi_map_wready_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxpolarity_0_reg : out STD_LOGIC;
    timeout_enable_reg : out STD_LOGIC;
    drp_reset_reg : out STD_LOGIC;
    gt_slv_wren : out STD_LOGIC;
    wait_for_drp_reg : out STD_LOGIC;
    cpll_pd_0_reg : out STD_LOGIC;
    cpll_pd_0_reg_0 : out STD_LOGIC;
    tx_sys_reset_axi_reg : out STD_LOGIC;
    rx_sys_reset_axi_reg : out STD_LOGIC;
    \rx_pd_0_reg[1]_0\ : out STD_LOGIC;
    \rx_pd_0_reg[0]\ : out STD_LOGIC;
    \txpllclksel_reg[1]\ : out STD_LOGIC;
    \txpllclksel_reg[0]\ : out STD_LOGIC;
    \rxpllclksel_reg[1]\ : out STD_LOGIC;
    \rxpllclksel_reg[0]\ : out STD_LOGIC;
    \loopback_0_reg[2]\ : out STD_LOGIC;
    \loopback_0_reg[1]\ : out STD_LOGIC;
    \loopback_0_reg[0]\ : out STD_LOGIC;
    \timeout_timer_count_reg[12]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \timeout_timer_count_reg[11]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \timeout_timer_count_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    access_type5_out : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \timeout_length_reg[11]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \drp_write_data_reg[15]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \gt_interface_sel_reg[7]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \timeout_value_reg[11]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    txpolarity_0_reg : out STD_LOGIC;
    \txpostcursor_0_reg[4]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    txinihibit_0_reg : out STD_LOGIC;
    \slv_rdata_reg[0]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \txdiffctrl_0_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata_reg[7]_0\ : out STD_LOGIC;
    \txprecursor_0_reg[4]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_wdata_r_internal_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 24 downto 0 );
    s_axi_aclk : in STD_LOGIC;
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \timeout_value_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \timeout_timer_count_reg[11]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \timeout_timer_count_reg[12]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \timeout_value_reg[11]_0\ : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    \drp_int_addr_reg[1]\ : in STD_LOGIC;
    \timeout_length_reg[8]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \rx_pd_0_reg[1]_1\ : in STD_LOGIC;
    \loopback_0_reg[1]_0\ : in STD_LOGIC;
    \cmm_interface_sel_reg[7]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s_axi_rready : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 4 downto 0 );
    timeout_enable : in STD_LOGIC;
    drp_reset : in STD_LOGIC;
    wait_for_drp : in STD_LOGIC;
    gt0_cpllpd_in : in STD_LOGIC;
    \slv_addr_reg[6]_0\ : in STD_LOGIC;
    tx_sys_reset_axi : in STD_LOGIC;
    rx_sys_reset_axi : in STD_LOGIC;
    \slv_addr_reg[5]_0\ : in STD_LOGIC;
    gt0_rxpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \gt_interface_sel_reg[2]\ : in STD_LOGIC;
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_loopback_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 9 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 9 downto 0 );
    gt_slv_rd_done : in STD_LOGIC;
    slv_wren_done_pulse : in STD_LOGIC;
    slv_rden_r : in STD_LOGIC;
    \slv_rdata_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_rd_addr_reg[1]_0\ : in STD_LOGIC;
    slv_wren_done_pulse_0 : in STD_LOGIC;
    slv_rden_r_1 : in STD_LOGIC;
    chan_rx_slv_rdata : in STD_LOGIC_VECTOR ( 0 to 0 );
    wait_for_drp_reg_0 : in STD_LOGIC;
    \drp_read_data_reg[0]\ : in STD_LOGIC;
    \txpostcursor_0_reg[2]\ : in STD_LOGIC;
    \slv_addr_reg[5]_1\ : in STD_LOGIC;
    slv_rden_r_reg : in STD_LOGIC;
    clk1_ready_pulse_reg : in STD_LOGIC;
    \drp_write_data_reg[10]\ : in STD_LOGIC;
    \gt_interface_sel_reg[4]\ : in STD_LOGIC;
    \gt_interface_sel_reg[2]_0\ : in STD_LOGIC;
    \gt_interface_sel_reg[7]_0\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \timeout_length_reg[9]\ : in STD_LOGIC;
    \drp_int_addr_reg[3]\ : in STD_LOGIC;
    \timeout_length_reg[2]\ : in STD_LOGIC;
    \drp_read_data_reg[2]\ : in STD_LOGIC;
    \drp_write_data_reg[11]\ : in STD_LOGIC;
    \drp_int_addr_reg[4]\ : in STD_LOGIC;
    \drp_read_data_reg[7]\ : in STD_LOGIC;
    \drp_read_data_reg[6]\ : in STD_LOGIC;
    \drp_read_data_reg[5]\ : in STD_LOGIC;
    timeout_enable_reg_0 : in STD_LOGIC;
    data_sync_reg_gsr : in STD_LOGIC;
    \drp_write_data_reg[15]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \drp_read_data_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \txpllclksel_reg[0]_0\ : in STD_LOGIC;
    \loopback_0_reg[0]_0\ : in STD_LOGIC;
    \txprecursor_0_reg[4]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \txpostcursor_0_reg[4]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_sync_reg_gsr_0 : in STD_LOGIC;
    \txdiffctrl_0_reg[3]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    slv_wren_clk2 : in STD_LOGIC;
    \txprecursor_0_reg[3]\ : in STD_LOGIC;
    \drp_read_data_reg[8]\ : in STD_LOGIC;
    data_sync_reg_gsr_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_axi;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_axi is
  signal \^q\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_awready19_in : STD_LOGIC;
  signal axi_awready_i_2_n_0 : STD_LOGIC;
  signal \axi_bresp[1]_i_1_n_0\ : STD_LOGIC;
  signal \axi_bresp[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_bresp[1]_i_6_n_0\ : STD_LOGIC;
  signal \axi_bresp[1]_i_7_n_0\ : STD_LOGIC;
  signal axi_bvalid0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_14_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_16_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_14_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal \^axi_rdata_reg[7]_0\ : STD_LOGIC;
  signal \axi_rresp[1]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rresp[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rresp[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rresp[1]_i_4_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wr_access0 : STD_LOGIC;
  signal chan_async_axi_map_wready : STD_LOGIC;
  signal chan_async_axi_map_wready0 : STD_LOGIC;
  signal \^chan_async_axi_map_wready_reg_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal chan_async_slv_rden : STD_LOGIC;
  signal chan_async_slv_rden_i_1_n_0 : STD_LOGIC;
  signal \^chan_rx_axi_map_wready\ : STD_LOGIC;
  signal chan_rx_axi_map_wready0 : STD_LOGIC;
  signal chan_rx_slv_rden_i_1_n_0 : STD_LOGIC;
  signal \^chan_tx_axi_map_wready\ : STD_LOGIC;
  signal chan_tx_axi_map_wready0 : STD_LOGIC;
  signal chan_tx_axi_map_wready_i_2_n_0 : STD_LOGIC;
  signal \^chan_tx_slv_rden\ : STD_LOGIC;
  signal chan_tx_slv_rden_i_1_n_0 : STD_LOGIC;
  signal \cmm_interface_sel[7]_i_2_n_0\ : STD_LOGIC;
  signal \^cpll_pd_0_reg_0\ : STD_LOGIC;
  signal drp_access_in_progress_i_4_n_0 : STD_LOGIC;
  signal drp_reset_i_2_n_0 : STD_LOGIC;
  signal gt_axi_map_wready : STD_LOGIC;
  signal gt_axi_map_wready0 : STD_LOGIC;
  signal gt_slv_rden_i_1_n_0 : STD_LOGIC;
  signal \^gt_slv_wren\ : STD_LOGIC;
  signal load_timeout_timer0 : STD_LOGIC;
  signal \loopback_0[2]_i_2_n_0\ : STD_LOGIC;
  signal \^p_0_in\ : STD_LOGIC;
  signal phy1_axi_map_wready : STD_LOGIC;
  signal phy1_axi_map_wready0 : STD_LOGIC;
  signal phy1_slv_rden : STD_LOGIC;
  signal phy1_slv_rden_i_1_n_0 : STD_LOGIC;
  signal phy1_slv_rden_i_3_n_0 : STD_LOGIC;
  signal read_in_progress : STD_LOGIC;
  signal read_in_progress_i_1_n_0 : STD_LOGIC;
  signal read_in_progress_i_2_n_0 : STD_LOGIC;
  signal \rx_pd_0[1]_i_3_n_0\ : STD_LOGIC;
  signal \^rx_pd_0_reg[1]\ : STD_LOGIC;
  signal \rxpllclksel[1]_i_2_n_0\ : STD_LOGIC;
  signal \^rxpolarity_0_reg\ : STD_LOGIC;
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_bresp\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^s_axi_bvalid\ : STD_LOGIC;
  signal \^s_axi_rresp\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^s_axi_rvalid\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal slv_addr : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \slv_addr[2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_addr[3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_addr[4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_addr[5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_addr[6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_addr[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_rd_addr[0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_rd_addr[1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_rd_addr[2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_rd_addr[3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_rd_addr[3]_i_2_n_0\ : STD_LOGIC;
  signal \slv_rd_addr_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_rd_addr_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_rd_addr_reg_n_0_[3]\ : STD_LOGIC;
  signal slv_reg_rden0 : STD_LOGIC;
  signal slv_reg_rden_i_1_n_0 : STD_LOGIC;
  signal slv_reg_rden_reg_n_0 : STD_LOGIC;
  signal timeout : STD_LOGIC;
  signal timeout_enable_i_2_n_0 : STD_LOGIC;
  signal \timeout_timer_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \timeout_timer_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \timeout_timer_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \timeout_timer_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \timeout_timer_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \timeout_timer_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[0]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[10]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[11]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[1]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[2]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[3]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[4]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[5]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[6]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[7]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[8]\ : STD_LOGIC;
  signal \timeout_timer_count_reg_n_0_[9]\ : STD_LOGIC;
  signal \timeout_value[11]_i_2_n_0\ : STD_LOGIC;
  signal valid_waddr_i_1_n_0 : STD_LOGIC;
  signal valid_waddr_i_2_n_0 : STD_LOGIC;
  signal valid_waddr_i_4_n_0 : STD_LOGIC;
  signal valid_waddr_reg_n_0 : STD_LOGIC;
  signal wait_for_drp_i_2_n_0 : STD_LOGIC;
  signal write_in_progress : STD_LOGIC;
  signal write_in_progress_i_1_n_0 : STD_LOGIC;
  signal write_in_progress_i_2_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \axi_bresp[1]_i_3\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \axi_bresp[1]_i_7\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of axi_bvalid_i_1 : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_10\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_14\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_16\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_3\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_9\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \axi_rdata[10]_i_4\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_5\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_6\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_7\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \axi_rdata[13]_i_2\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \axi_rdata[13]_i_5\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \axi_rdata[13]_i_6\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \axi_rdata[14]_i_2\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_2\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_3\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_4\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_5\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \axi_rdata[17]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \axi_rdata[18]_i_2\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_2\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \axi_rdata[1]_i_2\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \axi_rdata[1]_i_5\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \axi_rdata[20]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \axi_rdata[21]_i_2\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \axi_rdata[24]_i_2\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \axi_rdata[25]_i_2\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \axi_rdata[25]_i_3\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \axi_rdata[2]_i_2\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \axi_rdata[2]_i_4\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \axi_rdata[2]_i_8\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_2\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_6\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_8\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \axi_rdata[4]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \axi_rdata[4]_i_4\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \axi_rdata[4]_i_7\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \axi_rdata[4]_i_9\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \axi_rdata[7]_i_6\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \axi_rdata[7]_i_7\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \axi_rdata[8]_i_3\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \axi_rdata[8]_i_4\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of chan_async_slv_rden_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of chan_tx_axi_map_wready_i_2 : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of chan_tx_slv_rden_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \cmm_interface_sel[7]_i_2\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of cpll_pd_0_i_1 : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of drp_access_in_progress_i_4 : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of drp_reset_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of gt_axi_map_wready_i_1 : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of gt_slv_rden_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \loopback_0[2]_i_2\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of phy1_slv_rden_i_3 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \rx_pd_0[1]_i_3\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of rxpolarity_0_i_2 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \slv_rdata[0]_i_2\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of slv_reg_rden_i_1 : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \slv_wdata_r_internal[3]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of timeout_enable_i_2 : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \timeout_timer_count[0]_i_7\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \timeout_value[11]_i_2\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of valid_waddr_i_3 : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of wr_req_reg_i_1 : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of write_in_progress_i_2 : label is "soft_lutpair64";
begin
  Q(4 downto 0) <= \^q\(4 downto 0);
  \axi_rdata_reg[7]_0\ <= \^axi_rdata_reg[7]_0\;
  chan_async_axi_map_wready_reg_0(0) <= \^chan_async_axi_map_wready_reg_0\(0);
  chan_rx_axi_map_wready <= \^chan_rx_axi_map_wready\;
  chan_tx_axi_map_wready <= \^chan_tx_axi_map_wready\;
  chan_tx_slv_rden <= \^chan_tx_slv_rden\;
  cpll_pd_0_reg_0 <= \^cpll_pd_0_reg_0\;
  gt_slv_wren <= \^gt_slv_wren\;
  p_0_in <= \^p_0_in\;
  \rx_pd_0_reg[1]\ <= \^rx_pd_0_reg[1]\;
  rxpolarity_0_reg <= \^rxpolarity_0_reg\;
  s_axi_arready <= \^s_axi_arready\;
  s_axi_awready <= \^s_axi_awready\;
  s_axi_bresp(0) <= \^s_axi_bresp\(0);
  s_axi_bvalid <= \^s_axi_bvalid\;
  s_axi_rresp(0) <= \^s_axi_rresp\(0);
  s_axi_rvalid <= \^s_axi_rvalid\;
  s_axi_wready <= \^s_axi_wready\;
axi_arready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => read_in_progress,
      I1 => s_axi_arvalid,
      I2 => write_in_progress,
      I3 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => \^p_0_in\
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_axi_aresetn,
      O => \^p_0_in\
    );
axi_awready_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF00002"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => valid_waddr_reg_n_0,
      I2 => read_in_progress,
      I3 => s_axi_arvalid,
      I4 => \^s_axi_awready\,
      O => axi_awready_i_2_n_0
    );
axi_awready_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_awready_i_2_n_0,
      Q => \^s_axi_awready\,
      R => \^p_0_in\
    );
\axi_bresp[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8BBB000088880000"
    )
        port map (
      I0 => timeout,
      I1 => axi_bvalid0,
      I2 => s_axi_bready,
      I3 => \^s_axi_bvalid\,
      I4 => s_axi_aresetn,
      I5 => \^s_axi_bresp\(0),
      O => \axi_bresp[1]_i_1_n_0\
    );
\axi_bresp[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF00FE"
    )
        port map (
      I0 => \axi_bresp[1]_i_3_n_0\,
      I1 => slv_rden_r_reg,
      I2 => clk1_ready_pulse_reg,
      I3 => \^s_axi_bvalid\,
      I4 => \axi_bresp[1]_i_6_n_0\,
      I5 => \axi_bresp[1]_i_7_n_0\,
      O => axi_bvalid0
    );
\axi_bresp[1]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => phy1_axi_map_wready,
      I1 => chan_async_axi_map_wready,
      I2 => s_axi_wvalid,
      O => \axi_bresp[1]_i_3_n_0\
    );
\axi_bresp[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => \^chan_rx_axi_map_wready\,
      I1 => \^chan_tx_axi_map_wready\,
      I2 => phy1_axi_map_wready,
      I3 => gt_axi_map_wready,
      I4 => chan_async_axi_map_wready,
      I5 => \^s_axi_wready\,
      O => \axi_bresp[1]_i_6_n_0\
    );
\axi_bresp[1]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => timeout,
      I1 => write_in_progress,
      O => \axi_bresp[1]_i_7_n_0\
    );
\axi_bresp_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \axi_bresp[1]_i_1_n_0\,
      Q => \^s_axi_bresp\(0),
      R => '0'
    );
axi_bvalid_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F2"
    )
        port map (
      I0 => \^s_axi_bvalid\,
      I1 => s_axi_bready,
      I2 => axi_bvalid0,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s_axi_bvalid\,
      R => \^p_0_in\
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAFFFFFFAB"
    )
        port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => \axi_rdata[0]_i_3_n_0\,
      I2 => \axi_rdata[4]_i_4_n_0\,
      I3 => \axi_rdata[0]_i_4_n_0\,
      I4 => \axi_rdata[0]_i_5_n_0\,
      I5 => \axi_rdata[4]_i_2_n_0\,
      O => \axi_rdata[0]_i_1_n_0\
    );
\axi_rdata[0]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[2]\,
      I1 => \^chan_async_axi_map_wready_reg_0\(0),
      I2 => slv_addr(7),
      I3 => \^q\(4),
      I4 => \^q\(3),
      O => \axi_rdata[0]_i_10_n_0\
    );
\axi_rdata[0]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0D00000000000000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(3),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => slv_addr(7),
      I5 => \^q\(4),
      O => \axi_rdata[0]_i_13_n_0\
    );
\axi_rdata[0]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3BBB"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \timeout_value_reg[11]_0\(0),
      O => \axi_rdata[0]_i_14_n_0\
    );
\axi_rdata[0]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFEFF"
    )
        port map (
      I0 => \^q\(4),
      I1 => slv_addr(7),
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \^q\(3),
      O => \axi_rdata[0]_i_16_n_0\
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => \slv_rdata_reg[3]\(0),
      I1 => \slv_rd_addr_reg_n_0_[3]\,
      I2 => \slv_rd_addr_reg_n_0_[0]\,
      I3 => \slv_rd_addr_reg_n_0_[2]\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FF35"
    )
        port map (
      I0 => \txpllclksel_reg[0]_0\,
      I1 => \loopback_0_reg[0]_0\,
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \axi_rdata[0]_i_8_n_0\,
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8F888F8F8F888888"
    )
        port map (
      I0 => \axi_rdata[0]_i_9_n_0\,
      I1 => chan_rx_slv_rdata(0),
      I2 => \axi_rdata[0]_i_10_n_0\,
      I3 => wait_for_drp_reg_0,
      I4 => \^q\(2),
      I5 => \drp_read_data_reg[0]\,
      O => \axi_rdata[0]_i_4_n_0\
    );
\axi_rdata[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A888A888A88AAAA"
    )
        port map (
      I0 => \axi_rdata[4]_i_7_n_0\,
      I1 => \axi_rdata[0]_i_13_n_0\,
      I2 => \axi_rdata[0]_i_14_n_0\,
      I3 => \axi_rdata[11]_i_5_n_0\,
      I4 => timeout_enable_reg_0,
      I5 => \axi_rdata[0]_i_16_n_0\,
      O => \axi_rdata[0]_i_5_n_0\
    );
\axi_rdata[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000B080000"
    )
        port map (
      I0 => rx_sys_reset_axi,
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => tx_sys_reset_axi,
      I4 => \^q\(3),
      I5 => \^q\(2),
      O => \axi_rdata[0]_i_8_n_0\
    );
\axi_rdata[0]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => \slv_rd_addr_reg_n_0_[2]\,
      O => \axi_rdata[0]_i_9_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000080AAAAAAAA"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \drp_write_data_reg[10]\,
      I2 => \^chan_async_axi_map_wready_reg_0\(0),
      I3 => \axi_rdata[25]_i_2_n_0\,
      I4 => \^q\(3),
      I5 => \axi_rdata[10]_i_3_n_0\,
      O => \axi_rdata[10]_i_1_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFFDEFFFDFFDEFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \axi_rdata[10]_i_4_n_0\,
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \^q\(4),
      I5 => \^q\(3),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[10]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAFFBF"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => \timeout_value_reg[11]_0\(10),
      I2 => \^q\(1),
      I3 => \^q\(3),
      I4 => slv_addr(7),
      O => \axi_rdata[10]_i_4_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"08AA080808080808"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[11]_i_2_n_0\,
      I2 => \axi_rdata[11]_i_3_n_0\,
      I3 => \drp_write_data_reg[11]\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      I5 => \axi_rdata[11]_i_5_n_0\,
      O => \axi_rdata[11]_i_1_n_0\
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5051004040514051"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(1),
      I4 => \^q\(3),
      I5 => \^q\(4),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0D0D0D0D0DFF0D0D"
    )
        port map (
      I0 => \timeout_value_reg[11]_0\(11),
      I1 => \axi_rdata[11]_i_6_n_0\,
      I2 => slv_addr(7),
      I3 => \axi_rdata[21]_i_2_n_0\,
      I4 => \axi_rdata[11]_i_7_n_0\,
      I5 => \^q\(4),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[11]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(4),
      I2 => slv_addr(7),
      O => \axi_rdata[11]_i_5_n_0\
    );
\axi_rdata[11]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      O => \axi_rdata[11]_i_6_n_0\
    );
\axi_rdata[11]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(2),
      O => \axi_rdata[11]_i_7_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A888A888888888A"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[12]_i_2_n_0\,
      I2 => \axi_rdata[15]_i_3_n_0\,
      I3 => \^q\(0),
      I4 => \^q\(4),
      I5 => \^q\(1),
      O => \axi_rdata[12]_i_1_n_0\
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFA8080000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \drp_write_data_reg[15]_0\(0),
      I2 => \^q\(0),
      I3 => \drp_read_data_reg[15]\(0),
      I4 => \axi_rdata[15]_i_5_n_0\,
      I5 => \axi_rdata[12]_i_3_n_0\,
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => slv_addr(7),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(1),
      I5 => \^q\(2),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AAA8A8A8AAA8AA"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[13]_i_2_n_0\,
      I2 => \axi_rdata[13]_i_3_n_0\,
      I3 => \axi_rdata[13]_i_4_n_0\,
      I4 => \^q\(4),
      I5 => \axi_rdata[13]_i_5_n_0\,
      O => \axi_rdata[13]_i_1_n_0\
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8A800000"
    )
        port map (
      I0 => \axi_rdata[15]_i_5_n_0\,
      I1 => \drp_read_data_reg[15]\(1),
      I2 => \^q\(0),
      I3 => \drp_write_data_reg[15]_0\(1),
      I4 => \^q\(1),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000800000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(4),
      I2 => \axi_rdata[13]_i_6_n_0\,
      I3 => \^q\(1),
      I4 => \^q\(2),
      I5 => \^q\(3),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFDDFFFFFFFFFFFF"
    )
        port map (
      I0 => slv_addr(7),
      I1 => \^chan_async_axi_map_wready_reg_0\(0),
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \^q\(2),
      I5 => \^rxpolarity_0_reg\,
      O => \axi_rdata[13]_i_4_n_0\
    );
\axi_rdata[13]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \axi_rdata[13]_i_5_n_0\
    );
\axi_rdata[13]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => slv_addr(7),
      I1 => \^chan_async_axi_map_wready_reg_0\(0),
      O => \axi_rdata[13]_i_6_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000A08AAAAAAAAA"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \^q\(1),
      I2 => \^q\(4),
      I3 => \^q\(0),
      I4 => \axi_rdata[15]_i_3_n_0\,
      I5 => \axi_rdata[14]_i_2_n_0\,
      O => \axi_rdata[14]_i_1_n_0\
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F777FFF"
    )
        port map (
      I0 => \axi_rdata[15]_i_5_n_0\,
      I1 => \^q\(1),
      I2 => \drp_read_data_reg[15]\(2),
      I3 => \^q\(0),
      I4 => \drp_write_data_reg[15]_0\(2),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000A088AAAAAAAA"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \^q\(4),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \axi_rdata[15]_i_3_n_0\,
      I5 => \axi_rdata[15]_i_4_n_0\,
      O => \axi_rdata[15]_i_1_n_0\
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[2]\,
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      I2 => \slv_rd_addr_reg_n_0_[3]\,
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF5DFFFF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^chan_async_axi_map_wready_reg_0\(0),
      I4 => slv_addr(7),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[15]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"757FFFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \drp_read_data_reg[15]\(3),
      I2 => \^q\(0),
      I3 => \drp_write_data_reg[15]_0\(3),
      I4 => \axi_rdata[15]_i_5_n_0\,
      O => \axi_rdata[15]_i_4_n_0\
    );
\axi_rdata[15]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => \^q\(3),
      I2 => \^q\(2),
      I3 => slv_addr(7),
      I4 => \^q\(4),
      O => \axi_rdata[15]_i_5_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8500000000000000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \axi_rdata[24]_i_2_n_0\,
      I4 => \^q\(2),
      I5 => slv_addr(7),
      O => \axi_rdata[16]_i_1_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[3]\,
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      I2 => \^chan_async_axi_map_wready_reg_0\(0),
      I3 => \slv_rd_addr_reg_n_0_[2]\,
      I4 => \axi_rdata[17]_i_2_n_0\,
      O => \axi_rdata[17]_i_1_n_0\
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2EEEFFFFFFFFFFFC"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(1),
      I4 => slv_addr(7),
      I5 => \^q\(2),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000444000000"
    )
        port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => \axi_rdata[25]_i_3_n_0\,
      I2 => \^q\(3),
      I3 => slv_addr(7),
      I4 => \^q\(2),
      I5 => \^q\(0),
      O => \axi_rdata[18]_i_1_n_0\
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(4),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"000D"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      I2 => \^q\(1),
      I3 => \axi_rdata[19]_i_2_n_0\,
      O => \axi_rdata[19]_i_1_n_0\
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFBDFFFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(2),
      I2 => slv_addr(7),
      I3 => \^q\(3),
      I4 => \axi_rdata[25]_i_3_n_0\,
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABABBBBAAAAAAAA"
    )
        port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => \axi_rdata[1]_i_3_n_0\,
      I2 => \slv_rd_addr_reg[1]_0\,
      I3 => \axi_rdata[1]_i_5_n_0\,
      I4 => \slv_rd_addr_reg_n_0_[2]\,
      I5 => \axi_rdata[1]_i_6_n_0\,
      O => \axi_rdata[1]_i_1_n_0\
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => \slv_rdata_reg[3]\(1),
      I1 => \slv_rd_addr_reg_n_0_[3]\,
      I2 => \slv_rd_addr_reg_n_0_[0]\,
      I3 => \slv_rd_addr_reg_n_0_[2]\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000800080008AAAA"
    )
        port map (
      I0 => \axi_rdata[4]_i_7_n_0\,
      I1 => \axi_rdata[1]_i_7_n_0\,
      I2 => \axi_rdata[1]_i_8_n_0\,
      I3 => \axi_rdata[3]_i_11_n_0\,
      I4 => \axi_rdata[3]_i_8_n_0\,
      I5 => \axi_rdata[1]_i_9_n_0\,
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \^q\(3),
      I1 => \rx_pd_0_reg[1]_1\,
      I2 => \^q\(2),
      I3 => \loopback_0_reg[1]_0\,
      O => \axi_rdata[1]_i_5_n_0\
    );
\axi_rdata[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4454000055555555"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => \drp_int_addr_reg[1]\,
      I2 => \timeout_length_reg[8]\(0),
      I3 => \axi_rdata[4]_i_9_n_0\,
      I4 => \axi_rdata[11]_i_5_n_0\,
      I5 => \^chan_async_axi_map_wready_reg_0\(0),
      O => \axi_rdata[1]_i_6_n_0\
    );
\axi_rdata[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF7FFFFFFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => slv_addr(7),
      I3 => \^q\(4),
      I4 => \^q\(3),
      I5 => \timeout_value_reg[11]_0\(1),
      O => \axi_rdata[1]_i_7_n_0\
    );
\axi_rdata[1]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001010001"
    )
        port map (
      I0 => slv_addr(7),
      I1 => \^q\(4),
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \gt_interface_sel_reg[7]_0\(0),
      I5 => \^q\(1),
      O => \axi_rdata[1]_i_8_n_0\
    );
\axi_rdata[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404000000000400"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \^q\(1),
      I3 => \cmm_interface_sel_reg[7]\(0),
      I4 => \^q\(4),
      I5 => slv_addr(7),
      O => \axi_rdata[1]_i_9_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[3]\,
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      I2 => \^chan_async_axi_map_wready_reg_0\(0),
      I3 => \slv_rd_addr_reg_n_0_[2]\,
      I4 => \axi_rdata[20]_i_2_n_0\,
      O => \axi_rdata[20]_i_1_n_0\
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE77F7FFF7"
    )
        port map (
      I0 => \^q\(2),
      I1 => slv_addr(7),
      I2 => \^q\(1),
      I3 => \^q\(4),
      I4 => \^q\(3),
      I5 => \^q\(0),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => \axi_rdata[25]_i_3_n_0\,
      I1 => slv_addr(7),
      I2 => \^q\(3),
      I3 => \^q\(0),
      I4 => \^q\(4),
      I5 => \axi_rdata[21]_i_2_n_0\,
      O => \axi_rdata[21]_i_1_n_0\
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => slv_addr(7),
      I1 => \^q\(2),
      I2 => \axi_rdata[25]_i_3_n_0\,
      I3 => \^q\(0),
      I4 => \^q\(4),
      I5 => \^q\(1),
      O => \axi_rdata[22]_i_1_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000002"
    )
        port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => slv_addr(7),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(1),
      I5 => \^q\(2),
      O => \axi_rdata[24]_i_1_n_0\
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[3]\,
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      I2 => \^chan_async_axi_map_wready_reg_0\(0),
      I3 => \slv_rd_addr_reg_n_0_[2]\,
      I4 => \^q\(0),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \axi_rdata[25]_i_2_n_0\,
      I4 => \^q\(0),
      I5 => \axi_rdata[25]_i_3_n_0\,
      O => \axi_rdata[25]_i_1_n_0\
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => slv_addr(7),
      I1 => \^q\(4),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[2]\,
      I1 => \^chan_async_axi_map_wready_reg_0\(0),
      I2 => \slv_rd_addr_reg_n_0_[0]\,
      I3 => \slv_rd_addr_reg_n_0_[3]\,
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000004440404"
    )
        port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => \axi_rdata[2]_i_3_n_0\,
      I2 => \axi_rdata[2]_i_4_n_0\,
      I3 => \txpostcursor_0_reg[2]\,
      I4 => \slv_addr_reg[5]_1\,
      I5 => \axi_rdata[2]_i_7_n_0\,
      O => \axi_rdata[2]_i_1_n_0\
    );
\axi_rdata[2]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => \timeout_value_reg[11]_0\(2),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => slv_addr(7),
      I4 => \^q\(4),
      I5 => \^q\(3),
      O => \axi_rdata[2]_i_10_n_0\
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFF4CC"
    )
        port map (
      I0 => \slv_rdata_reg[3]\(2),
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      I2 => \^chan_async_axi_map_wready_reg_0\(0),
      I3 => \slv_rd_addr_reg_n_0_[2]\,
      I4 => \slv_rd_addr_reg_n_0_[3]\,
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F1F1F101FFFFFFFF"
    )
        port map (
      I0 => \axi_rdata[2]_i_8_n_0\,
      I1 => \^rx_pd_0_reg[1]\,
      I2 => \^q\(0),
      I3 => \axi_rdata[2]_i_9_n_0\,
      I4 => \axi_rdata[2]_i_10_n_0\,
      I5 => \axi_rdata[4]_i_7_n_0\,
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[2]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[2]\,
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      O => \axi_rdata[2]_i_4_n_0\
    );
\axi_rdata[2]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AAA8A8A8AAAAAA"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => \axi_rdata[25]_i_2_n_0\,
      I2 => \^q\(3),
      I3 => \timeout_length_reg[2]\,
      I4 => \^q\(2),
      I5 => \drp_read_data_reg[2]\,
      O => \axi_rdata[2]_i_7_n_0\
    );
\axi_rdata[2]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFCFFF77"
    )
        port map (
      I0 => \cmm_interface_sel_reg[7]\(1),
      I1 => \^q\(3),
      I2 => data_sync_reg_gsr,
      I3 => \^q\(4),
      I4 => slv_addr(7),
      O => \axi_rdata[2]_i_8_n_0\
    );
\axi_rdata[2]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000C1C000000000"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(4),
      I2 => slv_addr(7),
      I3 => \gt_interface_sel_reg[7]_0\(1),
      I4 => \^q\(1),
      I5 => \^q\(3),
      O => \axi_rdata[2]_i_9_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAFEFEAAFE"
    )
        port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => \axi_rdata[15]_i_2_n_0\,
      I2 => \axi_rdata[3]_i_3_n_0\,
      I3 => \axi_rdata[4]_i_7_n_0\,
      I4 => \axi_rdata[3]_i_4_n_0\,
      I5 => \axi_rdata[3]_i_5_n_0\,
      O => \axi_rdata[3]_i_1_n_0\
    );
\axi_rdata[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => \gt_interface_sel_reg[7]_0\(2),
      I1 => \^q\(1),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => slv_addr(7),
      I5 => \^q\(4),
      O => \axi_rdata[3]_i_10_n_0\
    );
\axi_rdata[3]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80000000FFFFFFFF"
    )
        port map (
      I0 => \^q\(4),
      I1 => slv_addr(7),
      I2 => \^q\(3),
      I3 => \^q\(1),
      I4 => \^q\(2),
      I5 => \^q\(0),
      O => \axi_rdata[3]_i_11_n_0\
    );
\axi_rdata[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF7FFFFFFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => slv_addr(7),
      I3 => \^q\(4),
      I4 => \^q\(3),
      I5 => \timeout_value_reg[11]_0\(3),
      O => \axi_rdata[3]_i_12_n_0\
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => \slv_rdata_reg[3]\(3),
      I1 => \slv_rd_addr_reg_n_0_[3]\,
      I2 => \slv_rd_addr_reg_n_0_[0]\,
      I3 => \slv_rd_addr_reg_n_0_[2]\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \gt_interface_sel_reg[2]_0\,
      I3 => \gt_interface_sel_reg[4]\,
      I4 => \axi_rdata[3]_i_6_n_0\,
      I5 => \txprecursor_0_reg[3]\,
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABABAB00ABABABAB"
    )
        port map (
      I0 => \axi_rdata[3]_i_8_n_0\,
      I1 => \^q\(1),
      I2 => \axi_rdata[3]_i_9_n_0\,
      I3 => \axi_rdata[3]_i_10_n_0\,
      I4 => \axi_rdata[3]_i_11_n_0\,
      I5 => \axi_rdata[3]_i_12_n_0\,
      O => \axi_rdata[3]_i_4_n_0\
    );
\axi_rdata[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"22222222A2AAAAAA"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => \axi_rdata[11]_i_5_n_0\,
      I2 => \axi_rdata[13]_i_5_n_0\,
      I3 => \^q\(2),
      I4 => \timeout_length_reg[8]\(1),
      I5 => \drp_int_addr_reg[3]\,
      O => \axi_rdata[3]_i_5_n_0\
    );
\axi_rdata[3]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      I2 => \slv_rd_addr_reg_n_0_[3]\,
      O => \axi_rdata[3]_i_6_n_0\
    );
\axi_rdata[3]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEAAAAAA"
    )
        port map (
      I0 => \^q\(0),
      I1 => slv_addr(7),
      I2 => \^q\(4),
      I3 => \^q\(1),
      I4 => \^q\(2),
      O => \axi_rdata[3]_i_8_n_0\
    );
\axi_rdata[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0FF5F3FF"
    )
        port map (
      I0 => \cmm_interface_sel_reg[7]\(2),
      I1 => data_sync_reg_gsr_0,
      I2 => \^q\(4),
      I3 => slv_addr(7),
      I4 => \^q\(3),
      I5 => \^q\(2),
      O => \axi_rdata[3]_i_9_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4544555545444544"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => \axi_rdata[4]_i_3_n_0\,
      I2 => \axi_rdata[4]_i_4_n_0\,
      I3 => \axi_rdata[4]_i_5_n_0\,
      I4 => \axi_rdata[4]_i_6_n_0\,
      I5 => \axi_rdata[4]_i_7_n_0\,
      O => \axi_rdata[4]_i_1_n_0\
    );
\axi_rdata[4]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"66EEFF7F66FFFF7F"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \timeout_value_reg[11]_0\(4),
      I3 => \^q\(4),
      I4 => \^q\(3),
      I5 => \gt_interface_sel_reg[7]_0\(3),
      O => \axi_rdata[4]_i_12_n_0\
    );
\axi_rdata[4]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBFFFFFFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(3),
      I2 => \^q\(2),
      I3 => slv_addr(7),
      I4 => \^q\(4),
      I5 => \cmm_interface_sel_reg[7]\(3),
      O => \axi_rdata[4]_i_13_n_0\
    );
\axi_rdata[4]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444400000040"
    )
        port map (
      I0 => \^q\(4),
      I1 => slv_addr(7),
      I2 => data_sync_reg_gsr_1,
      I3 => \^q\(3),
      I4 => \^q\(1),
      I5 => \^q\(2),
      O => \axi_rdata[4]_i_14_n_0\
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[3]\,
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AE000000"
    )
        port map (
      I0 => \drp_int_addr_reg[4]\,
      I1 => \timeout_length_reg[8]\(2),
      I2 => \axi_rdata[4]_i_9_n_0\,
      I3 => \axi_rdata[11]_i_5_n_0\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      I5 => \slv_rd_addr_reg_n_0_[2]\,
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[4]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[2]\,
      I1 => \gt_interface_sel_reg[4]\,
      I2 => \gt_interface_sel_reg[2]_0\,
      I3 => \^chan_async_axi_map_wready_reg_0\(0),
      O => \axi_rdata[4]_i_4_n_0\
    );
\axi_rdata[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000064200000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \txprecursor_0_reg[4]_0\(0),
      I3 => \txpostcursor_0_reg[4]_0\(0),
      I4 => \^q\(2),
      I5 => \^q\(3),
      O => \axi_rdata[4]_i_5_n_0\
    );
\axi_rdata[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F600F600F6FFF600"
    )
        port map (
      I0 => \^q\(4),
      I1 => slv_addr(7),
      I2 => \axi_rdata[4]_i_12_n_0\,
      I3 => \^q\(0),
      I4 => \axi_rdata[4]_i_13_n_0\,
      I5 => \axi_rdata[4]_i_14_n_0\,
      O => \axi_rdata[4]_i_6_n_0\
    );
\axi_rdata[4]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^chan_async_axi_map_wready_reg_0\(0),
      I1 => \slv_rd_addr_reg_n_0_[2]\,
      O => \axi_rdata[4]_i_7_n_0\
    );
\axi_rdata[4]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => \axi_rdata[4]_i_9_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000002A2AAAA02A2"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[5]_i_2_n_0\,
      I2 => \^q\(0),
      I3 => \axi_rdata[5]_i_3_n_0\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      I5 => \axi_rdata[5]_i_4_n_0\,
      O => \axi_rdata[5]_i_1_n_0\
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"77FFFF67FFFFFFFF"
    )
        port map (
      I0 => \^q\(4),
      I1 => slv_addr(7),
      I2 => \cmm_interface_sel_reg[7]\(4),
      I3 => \^q\(1),
      I4 => \^q\(2),
      I5 => \^q\(3),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AAAAAAAAAAAAAA"
    )
        port map (
      I0 => \axi_rdata[5]_i_5_n_0\,
      I1 => \^q\(3),
      I2 => \axi_rdata[25]_i_2_n_0\,
      I3 => \^q\(2),
      I4 => \^q\(1),
      I5 => \timeout_value_reg[11]_0\(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F757F7F7F7F7F7F7"
    )
        port map (
      I0 => \axi_rdata[11]_i_5_n_0\,
      I1 => \drp_read_data_reg[5]\,
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \timeout_length_reg[8]\(3),
      I5 => \^q\(1),
      O => \axi_rdata[5]_i_4_n_0\
    );
\axi_rdata[5]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFBFFFFEFFFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \gt_interface_sel_reg[7]_0\(4),
      I5 => slv_addr(7),
      O => \axi_rdata[5]_i_5_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000101010001"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[3]\,
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      I2 => \slv_rd_addr_reg_n_0_[2]\,
      I3 => \axi_rdata[6]_i_2_n_0\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      I5 => \axi_rdata[6]_i_3_n_0\,
      O => \axi_rdata[6]_i_1_n_0\
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00BBBBFFFFAFAF"
    )
        port map (
      I0 => \axi_rdata[6]_i_4_n_0\,
      I1 => \^q\(0),
      I2 => \^q\(3),
      I3 => \axi_rdata[6]_i_5_n_0\,
      I4 => \^q\(1),
      I5 => \^q\(2),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F757F7F7F7F7F7F7"
    )
        port map (
      I0 => \axi_rdata[11]_i_5_n_0\,
      I1 => \drp_read_data_reg[6]\,
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \timeout_length_reg[8]\(4),
      I5 => \^q\(1),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3333EFEE3333EFFF"
    )
        port map (
      I0 => \^q\(2),
      I1 => slv_addr(7),
      I2 => \gt_interface_sel_reg[7]_0\(5),
      I3 => \^q\(0),
      I4 => \^q\(4),
      I5 => \cmm_interface_sel_reg[7]\(5),
      O => \axi_rdata[6]_i_4_n_0\
    );
\axi_rdata[6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F3F133F3"
    )
        port map (
      I0 => \timeout_value_reg[11]_0\(6),
      I1 => slv_addr(7),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(0),
      O => \axi_rdata[6]_i_5_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000A8AAAAAAA8AA"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[7]_i_2_n_0\,
      I2 => \axi_rdata[7]_i_3_n_0\,
      I3 => \axi_rdata[7]_i_4_n_0\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      I5 => \axi_rdata[7]_i_5_n_0\,
      O => \axi_rdata[7]_i_1_n_0\
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000040"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \gt_interface_sel_reg[7]_0\(6),
      I3 => \^rxpolarity_0_reg\,
      I4 => slv_addr(7),
      I5 => \^q\(4),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000C00505"
    )
        port map (
      I0 => \axi_rdata[7]_i_6_n_0\,
      I1 => slv_addr(7),
      I2 => \^q\(4),
      I3 => \^axi_rdata_reg[7]_0\,
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7FFFFFFFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \axi_rdata[25]_i_2_n_0\,
      I5 => \timeout_value_reg[11]_0\(7),
      O => \axi_rdata[7]_i_4_n_0\
    );
\axi_rdata[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F757F7F7F7F7F7F7"
    )
        port map (
      I0 => \axi_rdata[11]_i_5_n_0\,
      I1 => \drp_read_data_reg[7]\,
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \timeout_length_reg[8]\(5),
      I5 => \^q\(1),
      O => \axi_rdata[7]_i_5_n_0\
    );
\axi_rdata[7]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \^q\(3),
      I1 => slv_addr(7),
      I2 => \cmm_interface_sel_reg[7]\(6),
      I3 => \^q\(2),
      O => \axi_rdata[7]_i_6_n_0\
    );
\axi_rdata[7]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      O => \^axi_rdata_reg[7]_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A088A8000088A8"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[8]_i_2_n_0\,
      I2 => \axi_rdata[11]_i_5_n_0\,
      I3 => \^rx_pd_0_reg[1]\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      I5 => \axi_rdata[8]_i_4_n_0\,
      O => \axi_rdata[8]_i_1_n_0\
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000040A0000000"
    )
        port map (
      I0 => slv_addr(7),
      I1 => \timeout_value_reg[11]_0\(8),
      I2 => \axi_rdata[21]_i_2_n_0\,
      I3 => \^q\(4),
      I4 => \^q\(3),
      I5 => \^q\(0),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      O => \^rx_pd_0_reg[1]\
    );
\axi_rdata[8]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08FF0800"
    )
        port map (
      I0 => \^q\(1),
      I1 => \timeout_length_reg[8]\(6),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \drp_read_data_reg[8]\,
      O => \axi_rdata[8]_i_4_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"008AAA8A008A008A"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[9]_i_2_n_0\,
      I2 => \axi_rdata[9]_i_3_n_0\,
      I3 => \^chan_async_axi_map_wready_reg_0\(0),
      I4 => \timeout_length_reg[9]\,
      I5 => \axi_rdata[11]_i_5_n_0\,
      O => \axi_rdata[9]_i_1_n_0\
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0000015D000000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      I2 => \^q\(1),
      I3 => slv_addr(7),
      I4 => \^q\(2),
      I5 => \^q\(0),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7FFFFFFFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \axi_rdata[25]_i_2_n_0\,
      I5 => \timeout_value_reg[11]_0\(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[0]_i_1_n_0\,
      Q => s_axi_rdata(0),
      R => \^p_0_in\
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[10]_i_1_n_0\,
      Q => s_axi_rdata(10),
      R => \^p_0_in\
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[11]_i_1_n_0\,
      Q => s_axi_rdata(11),
      R => \^p_0_in\
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[12]_i_1_n_0\,
      Q => s_axi_rdata(12),
      R => \^p_0_in\
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[13]_i_1_n_0\,
      Q => s_axi_rdata(13),
      R => \^p_0_in\
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[14]_i_1_n_0\,
      Q => s_axi_rdata(14),
      R => \^p_0_in\
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[15]_i_1_n_0\,
      Q => s_axi_rdata(15),
      R => \^p_0_in\
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[16]_i_1_n_0\,
      Q => s_axi_rdata(16),
      R => \^p_0_in\
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[17]_i_1_n_0\,
      Q => s_axi_rdata(17),
      R => \^p_0_in\
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[18]_i_1_n_0\,
      Q => s_axi_rdata(18),
      R => \^p_0_in\
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[19]_i_1_n_0\,
      Q => s_axi_rdata(19),
      R => \^p_0_in\
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[1]_i_1_n_0\,
      Q => s_axi_rdata(1),
      R => \^p_0_in\
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[20]_i_1_n_0\,
      Q => s_axi_rdata(20),
      R => \^p_0_in\
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[21]_i_1_n_0\,
      Q => s_axi_rdata(21),
      R => \^p_0_in\
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[22]_i_1_n_0\,
      Q => s_axi_rdata(22),
      R => \^p_0_in\
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[24]_i_1_n_0\,
      Q => s_axi_rdata(23),
      R => \^p_0_in\
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[25]_i_1_n_0\,
      Q => s_axi_rdata(24),
      R => \^p_0_in\
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[2]_i_1_n_0\,
      Q => s_axi_rdata(2),
      R => \^p_0_in\
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[3]_i_1_n_0\,
      Q => s_axi_rdata(3),
      R => \^p_0_in\
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[4]_i_1_n_0\,
      Q => s_axi_rdata(4),
      R => \^p_0_in\
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[5]_i_1_n_0\,
      Q => s_axi_rdata(5),
      R => \^p_0_in\
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[6]_i_1_n_0\,
      Q => s_axi_rdata(6),
      R => \^p_0_in\
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[7]_i_1_n_0\,
      Q => s_axi_rdata(7),
      R => \^p_0_in\
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[8]_i_1_n_0\,
      Q => s_axi_rdata(8),
      R => \^p_0_in\
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => slv_reg_rden_reg_n_0,
      D => \axi_rdata[9]_i_1_n_0\,
      Q => s_axi_rdata(9),
      R => \^p_0_in\
    );
\axi_rresp[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFDF00C0"
    )
        port map (
      I0 => \axi_rresp[1]_i_2_n_0\,
      I1 => timeout,
      I2 => read_in_progress,
      I3 => \^s_axi_rvalid\,
      I4 => \^s_axi_rresp\(0),
      O => \axi_rresp[1]_i_1_n_0\
    );
\axi_rresp[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF55554450"
    )
        port map (
      I0 => \axi_rresp[1]_i_3_n_0\,
      I1 => gt_slv_rd_done,
      I2 => phy1_slv_rden,
      I3 => \^chan_async_axi_map_wready_reg_0\(0),
      I4 => \slv_rd_addr_reg_n_0_[2]\,
      I5 => \axi_rresp[1]_i_4_n_0\,
      O => \axi_rresp[1]_i_2_n_0\
    );
\axi_rresp[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF04C4C4C4"
    )
        port map (
      I0 => chan_async_slv_rden,
      I1 => \slv_rd_addr_reg_n_0_[2]\,
      I2 => \^chan_async_axi_map_wready_reg_0\(0),
      I3 => slv_wren_done_pulse_0,
      I4 => slv_rden_r_1,
      I5 => \slv_rd_addr_reg_n_0_[0]\,
      O => \axi_rresp[1]_i_3_n_0\
    );
\axi_rresp[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFEFEFAAAAAAAA"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[3]\,
      I1 => \^chan_async_axi_map_wready_reg_0\(0),
      I2 => \slv_rd_addr_reg_n_0_[2]\,
      I3 => slv_wren_done_pulse,
      I4 => slv_rden_r,
      I5 => \slv_rd_addr_reg_n_0_[0]\,
      O => \axi_rresp[1]_i_4_n_0\
    );
\axi_rresp_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \axi_rresp[1]_i_1_n_0\,
      Q => \^s_axi_rresp\(0),
      R => \^p_0_in\
    );
axi_rvalid_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"74747444"
    )
        port map (
      I0 => s_axi_rready,
      I1 => \^s_axi_rvalid\,
      I2 => read_in_progress,
      I3 => timeout,
      I4 => \axi_rresp[1]_i_2_n_0\,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s_axi_rvalid\,
      R => \^p_0_in\
    );
axi_wr_access_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_axi_arvalid,
      O => axi_awready19_in
    );
axi_wr_access_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => valid_waddr_reg_n_0,
      I2 => \^s_axi_wready\,
      O => axi_wr_access0
    );
axi_wr_access_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => axi_awready19_in,
      D => axi_wr_access0,
      Q => \^s_axi_wready\,
      R => \^p_0_in\
    );
chan_async_axi_map_wready_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => valid_waddr_reg_n_0,
      I2 => s_axi_wvalid,
      I3 => \slv_rd_addr_reg_n_0_[2]\,
      I4 => \^chan_async_axi_map_wready_reg_0\(0),
      I5 => chan_async_axi_map_wready,
      O => chan_async_axi_map_wready0
    );
chan_async_axi_map_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready19_in,
      D => chan_async_axi_map_wready0,
      Q => chan_async_axi_map_wready,
      R => \^p_0_in\
    );
chan_async_slv_rden_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => s_axi_araddr(7),
      I1 => s_axi_araddr(9),
      I2 => s_axi_araddr(8),
      I3 => s_axi_araddr(6),
      O => chan_async_slv_rden_i_1_n_0
    );
chan_async_slv_rden_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden0,
      D => chan_async_slv_rden_i_1_n_0,
      Q => chan_async_slv_rden,
      R => phy1_slv_rden_i_1_n_0
    );
chan_rx_axi_map_wready_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => valid_waddr_reg_n_0,
      I2 => s_axi_wvalid,
      I3 => \^chan_async_axi_map_wready_reg_0\(0),
      I4 => \slv_rd_addr_reg_n_0_[2]\,
      I5 => \^chan_rx_axi_map_wready\,
      O => chan_rx_axi_map_wready0
    );
chan_rx_axi_map_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready19_in,
      D => chan_rx_axi_map_wready0,
      Q => \^chan_rx_axi_map_wready\,
      R => \^p_0_in\
    );
chan_rx_slv_rden_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => s_axi_araddr(9),
      I1 => s_axi_araddr(7),
      I2 => s_axi_araddr(8),
      I3 => s_axi_araddr(6),
      O => chan_rx_slv_rden_i_1_n_0
    );
chan_rx_slv_rden_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden0,
      D => chan_rx_slv_rden_i_1_n_0,
      Q => chan_rx_slv_rden,
      R => phy1_slv_rden_i_1_n_0
    );
chan_tx_axi_map_wready_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000400000"
    )
        port map (
      I0 => \slv_rd_addr_reg_n_0_[3]\,
      I1 => \slv_rd_addr_reg_n_0_[0]\,
      I2 => \slv_rd_addr_reg_n_0_[2]\,
      I3 => \^chan_async_axi_map_wready_reg_0\(0),
      I4 => chan_tx_axi_map_wready_i_2_n_0,
      I5 => \^chan_tx_axi_map_wready\,
      O => chan_tx_axi_map_wready0
    );
chan_tx_axi_map_wready_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => valid_waddr_reg_n_0,
      I1 => s_axi_wvalid,
      O => chan_tx_axi_map_wready_i_2_n_0
    );
chan_tx_axi_map_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready19_in,
      D => chan_tx_axi_map_wready0,
      Q => \^chan_tx_axi_map_wready\,
      R => \^p_0_in\
    );
chan_tx_slv_rden_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => s_axi_araddr(7),
      I1 => s_axi_araddr(9),
      I2 => s_axi_araddr(6),
      I3 => s_axi_araddr(8),
      O => chan_tx_slv_rden_i_1_n_0
    );
chan_tx_slv_rden_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden0,
      D => chan_tx_slv_rden_i_1_n_0,
      Q => \^chan_tx_slv_rden\,
      R => phy1_slv_rden_i_1_n_0
    );
\cmm_interface_sel[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \cmm_interface_sel[7]_i_2_n_0\,
      I2 => \^q\(4),
      I3 => slv_addr(7),
      I4 => phy1_axi_map_wready,
      I5 => s_axi_wvalid,
      O => \cmm_interface_sel_reg[0]\(0)
    );
\cmm_interface_sel[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(3),
      I2 => \^q\(2),
      O => \cmm_interface_sel[7]_i_2_n_0\
    );
cpll_pd_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => chan_async_axi_map_wready,
      I2 => s_axi_wvalid,
      I3 => \^cpll_pd_0_reg_0\,
      I4 => gt0_cpllpd_in,
      O => cpll_pd_0_reg
    );
cpll_pd_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \gt_interface_sel_reg[4]\,
      I5 => \gt_interface_sel_reg[2]_0\,
      O => \^cpll_pd_0_reg_0\
    );
drp_access_in_progress_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800080000000"
    )
        port map (
      I0 => drp_access_in_progress_i_4_n_0,
      I1 => gt_axi_map_wready,
      I2 => s_axi_wvalid,
      I3 => \^q\(0),
      I4 => s_axi_wdata(4),
      I5 => s_axi_wdata(3),
      O => access_type5_out
    );
drp_access_in_progress_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => slv_addr(7),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(1),
      O => drp_access_in_progress_i_4_n_0
    );
\drp_int_addr[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => gt_axi_map_wready,
      I1 => s_axi_wvalid,
      I2 => \^q\(0),
      I3 => \axi_rdata[11]_i_5_n_0\,
      I4 => \^q\(1),
      I5 => \^q\(2),
      O => E(0)
    );
drp_reset_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8A000000000000"
    )
        port map (
      I0 => drp_reset,
      I1 => drp_reset_i_2_n_0,
      I2 => \axi_rdata[11]_i_5_n_0\,
      I3 => s_axi_wdata(0),
      I4 => \^gt_slv_wren\,
      I5 => s_axi_aresetn,
      O => drp_reset_reg
    );
drp_reset_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(2),
      I2 => \^q\(1),
      O => drp_reset_i_2_n_0
    );
\drp_write_data[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000040"
    )
        port map (
      I0 => \axi_rdata[13]_i_5_n_0\,
      I1 => s_axi_wvalid,
      I2 => gt_axi_map_wready,
      I3 => \^q\(3),
      I4 => \^q\(2),
      I5 => \axi_rdata[25]_i_2_n_0\,
      O => \drp_write_data_reg[15]\(0)
    );
gt_axi_map_wready_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \^chan_async_axi_map_wready_reg_0\(0),
      I2 => gt_axi_map_wready,
      I3 => valid_waddr_reg_n_0,
      I4 => s_axi_wvalid,
      O => gt_axi_map_wready0
    );
gt_axi_map_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready19_in,
      D => gt_axi_map_wready0,
      Q => gt_axi_map_wready,
      R => \^p_0_in\
    );
\gt_interface_sel[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \cmm_interface_sel[7]_i_2_n_0\,
      I2 => \^q\(4),
      I3 => slv_addr(7),
      I4 => phy1_axi_map_wready,
      I5 => s_axi_wvalid,
      O => \gt_interface_sel_reg[7]\(0)
    );
gt_slv_rden_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => s_axi_araddr(6),
      I1 => s_axi_araddr(8),
      I2 => s_axi_araddr(9),
      I3 => s_axi_araddr(7),
      O => gt_slv_rden_i_1_n_0
    );
gt_slv_rden_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden0,
      D => gt_slv_rden_i_1_n_0,
      Q => gt_slv_rden,
      R => phy1_slv_rden_i_1_n_0
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => \timeout_value_reg[11]_0\(7),
      I1 => \timeout_timer_count[0]_i_4_n_0\,
      I2 => \timeout_timer_count_reg_n_0_[7]\,
      O => \timeout_timer_count_reg[7]_0\(3)
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[6]\,
      I1 => \timeout_value_reg[11]_0\(6),
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => \timeout_timer_count_reg[7]_0\(2)
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[5]\,
      I1 => \timeout_value_reg[11]_0\(5),
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => \timeout_timer_count_reg[7]_0\(1)
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[4]\,
      I1 => \timeout_value_reg[11]_0\(4),
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => \timeout_timer_count_reg[7]_0\(0)
    );
\i__carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[11]\,
      I1 => \timeout_value_reg[11]_0\(11),
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => \timeout_timer_count_reg[11]_0\(3)
    );
\i__carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[10]\,
      I1 => \timeout_value_reg[11]_0\(10),
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => \timeout_timer_count_reg[11]_0\(2)
    );
\i__carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => \timeout_value_reg[11]_0\(9),
      I1 => \timeout_timer_count[0]_i_4_n_0\,
      I2 => \timeout_timer_count_reg_n_0_[9]\,
      O => \timeout_timer_count_reg[11]_0\(1)
    );
\i__carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[8]\,
      I1 => \timeout_value_reg[11]_0\(8),
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => \timeout_timer_count_reg[11]_0\(0)
    );
\i__carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => timeout,
      I1 => \timeout_timer_count[0]_i_4_n_0\,
      O => \timeout_timer_count_reg[12]_0\(0)
    );
\i__carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[0]\,
      I1 => \timeout_value_reg[11]_0\(0),
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => DI(0)
    );
\i__carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => \timeout_value_reg[11]_0\(3),
      I1 => \timeout_timer_count[0]_i_4_n_0\,
      I2 => \timeout_timer_count_reg_n_0_[3]\,
      O => S(3)
    );
\i__carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[2]\,
      I1 => \timeout_value_reg[11]_0\(2),
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => S(2)
    );
\i__carry_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => \timeout_value_reg[11]_0\(1),
      I1 => \timeout_timer_count[0]_i_4_n_0\,
      I2 => \timeout_timer_count_reg_n_0_[1]\,
      O => S(1)
    );
\i__carry_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"53"
    )
        port map (
      I0 => \timeout_value_reg[11]_0\(0),
      I1 => \timeout_timer_count_reg_n_0_[0]\,
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => S(0)
    );
\loopback_0[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFF00002000"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \loopback_0[2]_i_2_n_0\,
      I2 => s_axi_wvalid,
      I3 => chan_async_axi_map_wready,
      I4 => \slv_addr_reg[6]_0\,
      I5 => gt0_loopback_in(0),
      O => \loopback_0_reg[0]\
    );
\loopback_0[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFF00002000"
    )
        port map (
      I0 => s_axi_wdata(1),
      I1 => \loopback_0[2]_i_2_n_0\,
      I2 => s_axi_wvalid,
      I3 => chan_async_axi_map_wready,
      I4 => \slv_addr_reg[6]_0\,
      I5 => gt0_loopback_in(1),
      O => \loopback_0_reg[1]\
    );
\loopback_0[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFF00002000"
    )
        port map (
      I0 => s_axi_wdata(2),
      I1 => \loopback_0[2]_i_2_n_0\,
      I2 => s_axi_wvalid,
      I3 => chan_async_axi_map_wready,
      I4 => \slv_addr_reg[6]_0\,
      I5 => gt0_loopback_in(2),
      O => \loopback_0_reg[2]\
    );
\loopback_0[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      O => \loopback_0[2]_i_2_n_0\
    );
phy1_axi_map_wready_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000000"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => \^chan_async_axi_map_wready_reg_0\(0),
      I2 => \slv_rd_addr_reg_n_0_[2]\,
      I3 => valid_waddr_reg_n_0,
      I4 => s_axi_wvalid,
      I5 => phy1_axi_map_wready,
      O => phy1_axi_map_wready0
    );
phy1_axi_map_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready19_in,
      D => phy1_axi_map_wready0,
      Q => phy1_axi_map_wready,
      R => \^p_0_in\
    );
phy1_slv_rden_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F700FFFF"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s_axi_arvalid,
      I2 => \^s_axi_rvalid\,
      I3 => \axi_rresp[1]_i_2_n_0\,
      I4 => s_axi_aresetn,
      O => phy1_slv_rden_i_1_n_0
    );
phy1_slv_rden_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s_axi_arvalid,
      I2 => \^s_axi_rvalid\,
      O => slv_reg_rden0
    );
phy1_slv_rden_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => s_axi_araddr(7),
      I1 => s_axi_araddr(9),
      I2 => s_axi_araddr(6),
      I3 => s_axi_araddr(8),
      O => phy1_slv_rden_i_3_n_0
    );
phy1_slv_rden_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden0,
      D => phy1_slv_rden_i_3_n_0,
      Q => phy1_slv_rden,
      R => phy1_slv_rden_i_1_n_0
    );
read_in_progress_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000EA00EA00EA"
    )
        port map (
      I0 => read_in_progress,
      I1 => s_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => read_in_progress_i_2_n_0,
      I4 => \^s_axi_rvalid\,
      I5 => s_axi_rready,
      O => read_in_progress_i_1_n_0
    );
read_in_progress_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => timeout,
      I1 => s_axi_aresetn,
      O => read_in_progress_i_2_n_0
    );
read_in_progress_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => read_in_progress_i_1_n_0,
      Q => read_in_progress,
      R => '0'
    );
\rx_pd_0[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \slv_addr_reg[5]_0\,
      I2 => \^q\(0),
      I3 => \rx_pd_0[1]_i_3_n_0\,
      I4 => \^rx_pd_0_reg[1]\,
      I5 => gt0_rxpd_in(0),
      O => \rx_pd_0_reg[0]\
    );
\rx_pd_0[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s_axi_wdata(1),
      I1 => \slv_addr_reg[5]_0\,
      I2 => \^q\(0),
      I3 => \rx_pd_0[1]_i_3_n_0\,
      I4 => \^rx_pd_0_reg[1]\,
      I5 => gt0_rxpd_in(1),
      O => \rx_pd_0_reg[1]_0\
    );
\rx_pd_0[1]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => chan_async_axi_map_wready,
      O => \rx_pd_0[1]_i_3_n_0\
    );
rx_sys_reset_axi_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFBFF00000800"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \^q\(0),
      I2 => \cmm_interface_sel[7]_i_2_n_0\,
      I3 => \rx_pd_0[1]_i_3_n_0\,
      I4 => \slv_addr_reg[6]_0\,
      I5 => rx_sys_reset_axi,
      O => rx_sys_reset_axi_reg
    );
\rxpllclksel[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \rx_pd_0[1]_i_3_n_0\,
      I2 => \slv_addr_reg[5]_0\,
      I3 => \^q\(0),
      I4 => \rxpllclksel[1]_i_2_n_0\,
      I5 => gt0_rxsysclksel_in(0),
      O => \rxpllclksel_reg[0]\
    );
\rxpllclksel[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => s_axi_wdata(1),
      I1 => \rx_pd_0[1]_i_3_n_0\,
      I2 => \slv_addr_reg[5]_0\,
      I3 => \^q\(0),
      I4 => \rxpllclksel[1]_i_2_n_0\,
      I5 => gt0_rxsysclksel_in(1),
      O => \rxpllclksel_reg[1]\
    );
\rxpllclksel[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      O => \rxpllclksel[1]_i_2_n_0\
    );
rxpolarity_0_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \^rxpolarity_0_reg\
    );
\slv_addr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(0),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(0),
      O => \slv_addr[2]_i_1_n_0\
    );
\slv_addr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(1),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(1),
      O => \slv_addr[3]_i_1_n_0\
    );
\slv_addr[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(2),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(2),
      O => \slv_addr[4]_i_1_n_0\
    );
\slv_addr[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(3),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(3),
      O => \slv_addr[5]_i_1_n_0\
    );
\slv_addr[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(4),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(4),
      O => \slv_addr[6]_i_1_n_0\
    );
\slv_addr[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(5),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(5),
      O => \slv_addr[7]_i_1_n_0\
    );
\slv_addr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_addr[2]_i_1_n_0\,
      Q => \^q\(0),
      R => \^p_0_in\
    );
\slv_addr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_addr[3]_i_1_n_0\,
      Q => \^q\(1),
      R => \^p_0_in\
    );
\slv_addr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_addr[4]_i_1_n_0\,
      Q => \^q\(2),
      R => \^p_0_in\
    );
\slv_addr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_addr[5]_i_1_n_0\,
      Q => \^q\(3),
      R => \^p_0_in\
    );
\slv_addr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_addr[6]_i_1_n_0\,
      Q => \^q\(4),
      R => \^p_0_in\
    );
\slv_addr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_addr[7]_i_1_n_0\,
      Q => slv_addr(7),
      R => \^p_0_in\
    );
\slv_rd_addr[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(6),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(6),
      O => \slv_rd_addr[0]_i_1_n_0\
    );
\slv_rd_addr[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(7),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(7),
      O => \slv_rd_addr[1]_i_1_n_0\
    );
\slv_rd_addr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(8),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(8),
      O => \slv_rd_addr[2]_i_1_n_0\
    );
\slv_rd_addr[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02020F02"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => valid_waddr_reg_n_0,
      I3 => s_axi_arvalid,
      I4 => read_in_progress,
      O => \slv_rd_addr[3]_i_1_n_0\
    );
\slv_rd_addr[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(9),
      I1 => s_axi_arvalid,
      I2 => read_in_progress,
      I3 => s_axi_awaddr(9),
      O => \slv_rd_addr[3]_i_2_n_0\
    );
\slv_rd_addr_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_rd_addr[0]_i_1_n_0\,
      Q => \slv_rd_addr_reg_n_0_[0]\,
      R => \^p_0_in\
    );
\slv_rd_addr_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_rd_addr[1]_i_1_n_0\,
      Q => \^chan_async_axi_map_wready_reg_0\(0),
      R => \^p_0_in\
    );
\slv_rd_addr_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_rd_addr[2]_i_1_n_0\,
      Q => \slv_rd_addr_reg_n_0_[2]\,
      R => \^p_0_in\
    );
\slv_rd_addr_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_rd_addr[3]_i_1_n_0\,
      D => \slv_rd_addr[3]_i_2_n_0\,
      Q => \slv_rd_addr_reg_n_0_[3]\,
      R => \^p_0_in\
    );
\slv_rdata[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \slv_rdata_reg[0]\
    );
\slv_rdata[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \gt_interface_sel_reg[4]\,
      I4 => \gt_interface_sel_reg[2]_0\,
      I5 => \^q\(3),
      O => txpolarity_0_reg
    );
\slv_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \txdiffctrl_0_reg[3]_0\(0),
      I1 => \slv_addr_reg[6]_0\,
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => D(0)
    );
\slv_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \txdiffctrl_0_reg[3]_0\(1),
      I1 => \slv_addr_reg[6]_0\,
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => D(1)
    );
slv_reg_rden_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"75553000"
    )
        port map (
      I0 => \axi_rresp[1]_i_2_n_0\,
      I1 => \^s_axi_rvalid\,
      I2 => s_axi_arvalid,
      I3 => \^s_axi_arready\,
      I4 => slv_reg_rden_reg_n_0,
      O => slv_reg_rden_i_1_n_0
    );
slv_reg_rden_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => slv_reg_rden_i_1_n_0,
      Q => slv_reg_rden_reg_n_0,
      R => \^p_0_in\
    );
\slv_wdata_r_internal[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F444"
    )
        port map (
      I0 => slv_rden_r,
      I1 => \^chan_tx_slv_rden\,
      I2 => \^chan_tx_axi_map_wready\,
      I3 => s_axi_wvalid,
      O => \slv_wdata_r_internal_reg[0]\(0)
    );
timeout_enable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \axi_rdata[11]_i_5_n_0\,
      I2 => \^q\(0),
      I3 => timeout_enable_i_2_n_0,
      I4 => \rxpllclksel[1]_i_2_n_0\,
      I5 => timeout_enable,
      O => timeout_enable_reg
    );
timeout_enable_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => phy1_axi_map_wready,
      O => timeout_enable_i_2_n_0
    );
\timeout_length[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000400000"
    )
        port map (
      I0 => \axi_rdata[13]_i_5_n_0\,
      I1 => s_axi_wvalid,
      I2 => gt_axi_map_wready,
      I3 => \^q\(3),
      I4 => \^q\(2),
      I5 => \axi_rdata[25]_i_2_n_0\,
      O => \timeout_length_reg[11]\(0)
    );
\timeout_timer_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF8FFF8FFF8FF"
    )
        port map (
      I0 => \^s_axi_bvalid\,
      I1 => s_axi_bready,
      I2 => timeout,
      I3 => s_axi_aresetn,
      I4 => \^s_axi_rvalid\,
      I5 => s_axi_rready,
      O => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => \timeout_timer_count[0]_i_3_n_0\,
      I1 => timeout_enable,
      I2 => \timeout_timer_count[0]_i_4_n_0\,
      O => \timeout_timer_count[0]_i_2_n_0\
    );
\timeout_timer_count[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \timeout_timer_count[0]_i_5_n_0\,
      I1 => \timeout_timer_count_reg_n_0_[1]\,
      I2 => \timeout_timer_count_reg_n_0_[0]\,
      I3 => \timeout_timer_count_reg_n_0_[3]\,
      I4 => \timeout_timer_count_reg_n_0_[2]\,
      I5 => \timeout_timer_count[0]_i_6_n_0\,
      O => \timeout_timer_count[0]_i_3_n_0\
    );
\timeout_timer_count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAA888A888A888"
    )
        port map (
      I0 => timeout_enable,
      I1 => load_timeout_timer0,
      I2 => \^s_axi_awready\,
      I3 => s_axi_awvalid,
      I4 => \^s_axi_wready\,
      I5 => s_axi_wvalid,
      O => \timeout_timer_count[0]_i_4_n_0\
    );
\timeout_timer_count[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[5]\,
      I1 => \timeout_timer_count_reg_n_0_[4]\,
      I2 => \timeout_timer_count_reg_n_0_[7]\,
      I3 => \timeout_timer_count_reg_n_0_[6]\,
      O => \timeout_timer_count[0]_i_5_n_0\
    );
\timeout_timer_count[0]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \timeout_timer_count_reg_n_0_[11]\,
      I1 => \timeout_timer_count_reg_n_0_[8]\,
      I2 => \timeout_timer_count_reg_n_0_[9]\,
      I3 => timeout,
      I4 => \timeout_timer_count_reg_n_0_[10]\,
      O => \timeout_timer_count[0]_i_6_n_0\
    );
\timeout_timer_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => load_timeout_timer0
    );
\timeout_timer_count_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => O(0),
      Q => \timeout_timer_count_reg_n_0_[0]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_timer_count_reg[11]_1\(2),
      Q => \timeout_timer_count_reg_n_0_[10]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_timer_count_reg[11]_1\(3),
      Q => \timeout_timer_count_reg_n_0_[11]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_timer_count_reg[12]_1\(0),
      Q => timeout,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => O(1),
      Q => \timeout_timer_count_reg_n_0_[1]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => O(2),
      Q => \timeout_timer_count_reg_n_0_[2]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => O(3),
      Q => \timeout_timer_count_reg_n_0_[3]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_value_reg[7]\(0),
      Q => \timeout_timer_count_reg_n_0_[4]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_value_reg[7]\(1),
      Q => \timeout_timer_count_reg_n_0_[5]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_value_reg[7]\(2),
      Q => \timeout_timer_count_reg_n_0_[6]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_value_reg[7]\(3),
      Q => \timeout_timer_count_reg_n_0_[7]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_timer_count_reg[11]_1\(0),
      Q => \timeout_timer_count_reg_n_0_[8]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_timer_count_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \timeout_timer_count[0]_i_2_n_0\,
      D => \timeout_timer_count_reg[11]_1\(1),
      Q => \timeout_timer_count_reg_n_0_[9]\,
      R => \timeout_timer_count[0]_i_1_n_0\
    );
\timeout_value[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => \timeout_value[11]_i_2_n_0\,
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => s_axi_wvalid,
      I4 => phy1_axi_map_wready,
      I5 => \axi_rdata[25]_i_2_n_0\,
      O => \timeout_value_reg[11]\(0)
    );
\timeout_value[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      O => \timeout_value[11]_i_2_n_0\
    );
tx_sys_reset_axi_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000200"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \^q\(0),
      I2 => \cmm_interface_sel[7]_i_2_n_0\,
      I3 => \rx_pd_0[1]_i_3_n_0\,
      I4 => \slv_addr_reg[6]_0\,
      I5 => tx_sys_reset_axi,
      O => tx_sys_reset_axi_reg
    );
\txdiffctrl_0[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010000"
    )
        port map (
      I0 => \slv_addr_reg[6]_0\,
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \axi_rdata[13]_i_5_n_0\,
      I4 => slv_wren_clk2,
      I5 => slv_rden_r,
      O => \txdiffctrl_0_reg[3]\(0)
    );
txinihibit_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF7"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \gt_interface_sel_reg[4]\,
      I5 => \gt_interface_sel_reg[2]_0\,
      O => txinihibit_0_reg
    );
\txpllclksel[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \rx_pd_0[1]_i_3_n_0\,
      I2 => \gt_interface_sel_reg[2]\,
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => gt0_txsysclksel_in(0),
      O => \txpllclksel_reg[0]\
    );
\txpllclksel[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
        port map (
      I0 => s_axi_wdata(1),
      I1 => \rx_pd_0[1]_i_3_n_0\,
      I2 => \gt_interface_sel_reg[2]\,
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => gt0_txsysclksel_in(1),
      O => \txpllclksel_reg[1]\
    );
\txpostcursor_0[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => \^q\(0),
      I1 => \rx_pd_0[1]_i_3_n_0\,
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \^q\(3),
      I5 => \slv_addr_reg[6]_0\,
      O => \txpostcursor_0_reg[4]\(0)
    );
\txprecursor_0[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000002000000"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \slv_addr_reg[6]_0\,
      I3 => s_axi_wvalid,
      I4 => chan_async_axi_map_wready,
      I5 => \axi_rdata[13]_i_5_n_0\,
      O => \txprecursor_0_reg[4]\(0)
    );
valid_waddr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000A000000EA00"
    )
        port map (
      I0 => valid_waddr_reg_n_0,
      I1 => valid_waddr_i_2_n_0,
      I2 => axi_awready0,
      I3 => s_axi_aresetn,
      I4 => timeout,
      I5 => valid_waddr_i_4_n_0,
      O => valid_waddr_i_1_n_0
    );
valid_waddr_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => \^s_axi_awready\,
      O => valid_waddr_i_2_n_0
    );
valid_waddr_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_in_progress,
      I1 => s_axi_arvalid,
      O => axi_awready0
    );
valid_waddr_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axi_bready,
      I1 => \^s_axi_bvalid\,
      O => valid_waddr_i_4_n_0
    );
valid_waddr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => valid_waddr_i_1_n_0,
      Q => valid_waddr_reg_n_0,
      R => '0'
    );
wait_for_drp_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => gt_axi_map_wready,
      I2 => s_axi_wvalid,
      I3 => wait_for_drp_i_2_n_0,
      I4 => wait_for_drp,
      O => wait_for_drp_reg
    );
wait_for_drp_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^q\(4),
      I1 => slv_addr(7),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => wait_for_drp_i_2_n_0
    );
wr_req_reg_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => gt_axi_map_wready,
      O => \^gt_slv_wren\
    );
write_in_progress_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFFF888"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => s_axi_awvalid,
      I2 => \^s_axi_wready\,
      I3 => s_axi_wvalid,
      I4 => write_in_progress,
      I5 => write_in_progress_i_2_n_0,
      O => write_in_progress_i_1_n_0
    );
write_in_progress_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF8F"
    )
        port map (
      I0 => \^s_axi_bvalid\,
      I1 => s_axi_bready,
      I2 => s_axi_aresetn,
      I3 => timeout,
      O => write_in_progress_i_2_n_0
    );
write_in_progress_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => write_in_progress_i_1_n_0,
      Q => write_in_progress,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async is
  port (
    gt0_cpllpd_in : out STD_LOGIC;
    tx_sys_reset_axi : out STD_LOGIC;
    rx_sys_reset_axi : out STD_LOGIC;
    gt0_rxpd_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txsysclksel_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxsysclksel_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_loopback_in : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[1]\ : out STD_LOGIC;
    \axi_rdata_reg[3]\ : out STD_LOGIC;
    data_sync_reg1 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    data_sync_reg1_0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \axi_rdata_reg[2]\ : out STD_LOGIC;
    data_in : out STD_LOGIC;
    \arststages_ff_reg[4]\ : out STD_LOGIC;
    \axi_rdata_reg[1]_0\ : out STD_LOGIC;
    \axi_rdata_reg[0]\ : out STD_LOGIC;
    \axi_rdata_reg[0]_0\ : out STD_LOGIC;
    p_0_in : in STD_LOGIC;
    chan_async_axi_map_wready_reg : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    \slv_addr_reg[2]\ : in STD_LOGIC;
    \slv_addr_reg[2]_0\ : in STD_LOGIC;
    \slv_addr_reg[2]_1\ : in STD_LOGIC;
    \slv_addr_reg[2]_2\ : in STD_LOGIC;
    \slv_addr_reg[2]_3\ : in STD_LOGIC;
    \slv_addr_reg[2]_4\ : in STD_LOGIC;
    \slv_addr_reg[2]_5\ : in STD_LOGIC;
    \slv_addr_reg[2]_6\ : in STD_LOGIC;
    chan_async_axi_map_wready_reg_0 : in STD_LOGIC;
    chan_async_axi_map_wready_reg_1 : in STD_LOGIC;
    chan_async_axi_map_wready_reg_2 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    tx_sys_reset : in STD_LOGIC;
    rx_sys_reset : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \slv_addr_reg[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async is
  signal \^data_sync_reg1\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^data_sync_reg1_0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^gt0_cpllpd_in\ : STD_LOGIC;
  signal \^gt0_loopback_in\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^gt0_rxpd_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^gt0_rxsysclksel_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^gt0_txsysclksel_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^rx_sys_reset_axi\ : STD_LOGIC;
  signal \^tx_sys_reset_axi\ : STD_LOGIC;
begin
  data_sync_reg1(4 downto 0) <= \^data_sync_reg1\(4 downto 0);
  data_sync_reg1_0(4 downto 0) <= \^data_sync_reg1_0\(4 downto 0);
  gt0_cpllpd_in <= \^gt0_cpllpd_in\;
  gt0_loopback_in(2 downto 0) <= \^gt0_loopback_in\(2 downto 0);
  gt0_rxpd_in(1 downto 0) <= \^gt0_rxpd_in\(1 downto 0);
  gt0_rxsysclksel_in(1 downto 0) <= \^gt0_rxsysclksel_in\(1 downto 0);
  gt0_txsysclksel_in(1 downto 0) <= \^gt0_txsysclksel_in\(1 downto 0);
  rx_sys_reset_axi <= \^rx_sys_reset_axi\;
  tx_sys_reset_axi <= \^tx_sys_reset_axi\;
\axi_rdata[0]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^gt0_txsysclksel_in\(0),
      I1 => \^gt0_cpllpd_in\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => \^gt0_rxpd_in\(0),
      O => \axi_rdata_reg[0]_0\
    );
\axi_rdata[0]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^gt0_loopback_in\(0),
      I1 => \^data_sync_reg1\(0),
      I2 => Q(1),
      I3 => \^data_sync_reg1_0\(0),
      I4 => Q(0),
      I5 => \^gt0_rxsysclksel_in\(0),
      O => \axi_rdata_reg[0]\
    );
\axi_rdata[1]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C0A0"
    )
        port map (
      I0 => \^gt0_rxpd_in\(1),
      I1 => \^gt0_txsysclksel_in\(1),
      I2 => Q(0),
      I3 => Q(1),
      O => \axi_rdata_reg[1]\
    );
\axi_rdata[1]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^gt0_loopback_in\(1),
      I1 => \^data_sync_reg1\(1),
      I2 => Q(1),
      I3 => \^data_sync_reg1_0\(1),
      I4 => Q(0),
      I5 => \^gt0_rxsysclksel_in\(1),
      O => \axi_rdata_reg[1]_0\
    );
\axi_rdata[2]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1D331DFF"
    )
        port map (
      I0 => \^data_sync_reg1_0\(2),
      I1 => Q(1),
      I2 => \^gt0_loopback_in\(2),
      I3 => Q(0),
      I4 => \^data_sync_reg1\(2),
      O => \axi_rdata_reg[2]\
    );
\axi_rdata[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C7F7"
    )
        port map (
      I0 => \^data_sync_reg1\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \^data_sync_reg1_0\(3),
      O => \axi_rdata_reg[3]\
    );
cpll_pd_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => chan_async_axi_map_wready_reg,
      Q => \^gt0_cpllpd_in\,
      R => p_0_in
    );
\loopback_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => chan_async_axi_map_wready_reg_2,
      Q => \^gt0_loopback_in\(0),
      R => p_0_in
    );
\loopback_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => chan_async_axi_map_wready_reg_1,
      Q => \^gt0_loopback_in\(1),
      R => p_0_in
    );
\loopback_0_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => chan_async_axi_map_wready_reg_0,
      Q => \^gt0_loopback_in\(2),
      R => p_0_in
    );
\rx_pd_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]_2\,
      Q => \^gt0_rxpd_in\(0),
      R => p_0_in
    );
\rx_pd_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]_1\,
      Q => \^gt0_rxpd_in\(1),
      R => p_0_in
    );
rx_sys_reset_axi_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]_0\,
      Q => \^rx_sys_reset_axi\,
      R => p_0_in
    );
\rxpllclksel_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]_6\,
      Q => \^gt0_rxsysclksel_in\(0),
      R => p_0_in
    );
\rxpllclksel_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]_5\,
      Q => \^gt0_rxsysclksel_in\(1),
      R => p_0_in
    );
tx_sys_reset_axi_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]\,
      Q => \^tx_sys_reset_axi\,
      R => p_0_in
    );
\txpllclksel_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]_4\,
      Q => \^gt0_txsysclksel_in\(0),
      R => p_0_in
    );
\txpllclksel_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_addr_reg[2]_3\,
      Q => \^gt0_txsysclksel_in\(1),
      R => p_0_in
    );
\txpostcursor_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(0),
      Q => \^data_sync_reg1_0\(0),
      R => p_0_in
    );
\txpostcursor_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(1),
      Q => \^data_sync_reg1_0\(1),
      R => p_0_in
    );
\txpostcursor_0_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(2),
      Q => \^data_sync_reg1_0\(2),
      R => p_0_in
    );
\txpostcursor_0_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(3),
      Q => \^data_sync_reg1_0\(3),
      R => p_0_in
    );
\txpostcursor_0_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(4),
      Q => \^data_sync_reg1_0\(4),
      R => p_0_in
    );
\txprecursor_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[4]\(0),
      D => s_axi_wdata(0),
      Q => \^data_sync_reg1\(0),
      R => p_0_in
    );
\txprecursor_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[4]\(0),
      D => s_axi_wdata(1),
      Q => \^data_sync_reg1\(1),
      R => p_0_in
    );
\txprecursor_0_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[4]\(0),
      D => s_axi_wdata(2),
      Q => \^data_sync_reg1\(2),
      R => p_0_in
    );
\txprecursor_0_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[4]\(0),
      D => s_axi_wdata(3),
      Q => \^data_sync_reg1\(3),
      R => p_0_in
    );
\txprecursor_0_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => \slv_addr_reg[4]\(0),
      D => s_axi_wdata(4),
      Q => \^data_sync_reg1\(4),
      R => p_0_in
    );
xpm_cdc_async_rst_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^tx_sys_reset_axi\,
      I1 => tx_sys_reset,
      O => data_in
    );
\xpm_cdc_async_rst_inst_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^rx_sys_reset_axi\,
      I1 => rx_sys_reset,
      O => \arststages_ff_reg[4]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst is
  port (
    src_arst : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_arst : out STD_LOGIC
  );
  attribute DEF_VAL : string;
  attribute DEF_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst : entity is "1'b1";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst : entity is 5;
  attribute INIT : string;
  attribute INIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst : entity is "1";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst : entity is 1;
  attribute VERSION : integer;
  attribute VERSION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst : entity is "ASYNC_RST";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst is
  signal arststages_ff : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of arststages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of arststages_ff : signal is "true";
  attribute xpm_cdc of arststages_ff : signal is "ASYNC_RST";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \arststages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[0]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[1]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[2]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[3]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[4]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[4]\ : label is "ASYNC_RST";
begin
\arststages_ff_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => '1',
      Q => arststages_ff(0)
    );
\arststages_ff_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(0),
      Q => arststages_ff(1)
    );
\arststages_ff_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(1),
      Q => arststages_ff(2)
    );
\arststages_ff_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(2),
      Q => arststages_ff(3)
    );
\arststages_ff_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(3),
      Q => arststages_ff(4)
    );
dest_arst_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => arststages_ff(4),
      O => dest_arst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ is
  port (
    src_arst : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_arst : out STD_LOGIC
  );
  attribute DEF_VAL : string;
  attribute DEF_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ : entity is "1'b1";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ : entity is 5;
  attribute INIT : string;
  attribute INIT of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ : entity is "1";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ : entity is "xpm_cdc_async_rst";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ : entity is 1;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ : entity is "ASYNC_RST";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\ is
  signal arststages_ff : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of arststages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of arststages_ff : signal is "true";
  attribute xpm_cdc of arststages_ff : signal is "ASYNC_RST";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \arststages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[0]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[1]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[2]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[3]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[4]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[4]\ : label is "ASYNC_RST";
begin
\arststages_ff_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => '1',
      Q => arststages_ff(0)
    );
\arststages_ff_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(0),
      Q => arststages_ff(1)
    );
\arststages_ff_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(1),
      Q => arststages_ff(2)
    );
\arststages_ff_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(2),
      Q => arststages_ff(3)
    );
\arststages_ff_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(3),
      Q => arststages_ff(4)
    );
dest_arst_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => arststages_ff(4),
      O => dest_arst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ is
  port (
    src_arst : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_arst : out STD_LOGIC
  );
  attribute DEF_VAL : string;
  attribute DEF_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ : entity is "1'b1";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ : entity is 5;
  attribute INIT : string;
  attribute INIT of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ : entity is "1";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ : entity is "xpm_cdc_async_rst";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ : entity is 1;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ : entity is "ASYNC_RST";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\ is
  signal arststages_ff : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of arststages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of arststages_ff : signal is "true";
  attribute xpm_cdc of arststages_ff : signal is "ASYNC_RST";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \arststages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[0]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[1]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[2]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[3]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[4]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[4]\ : label is "ASYNC_RST";
begin
\arststages_ff_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => '1',
      Q => arststages_ff(0)
    );
\arststages_ff_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(0),
      Q => arststages_ff(1)
    );
\arststages_ff_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(1),
      Q => arststages_ff(2)
    );
\arststages_ff_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(2),
      Q => arststages_ff(3)
    );
\arststages_ff_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(3),
      Q => arststages_ff(4)
    );
dest_arst_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => arststages_ff(4),
      O => dest_arst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ is
  port (
    src_arst : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_arst : out STD_LOGIC
  );
  attribute DEF_VAL : string;
  attribute DEF_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ : entity is "1'b1";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ : entity is 5;
  attribute INIT : string;
  attribute INIT of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ : entity is "1";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ : entity is "xpm_cdc_async_rst";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ : entity is 1;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ : entity is "ASYNC_RST";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\ is
  signal arststages_ff : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of arststages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of arststages_ff : signal is "true";
  attribute xpm_cdc of arststages_ff : signal is "ASYNC_RST";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \arststages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[0]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[1]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[2]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[3]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[4]\ : label is std.standard.true;
  attribute XPM_CDC of \arststages_ff_reg[4]\ : label is "ASYNC_RST";
begin
\arststages_ff_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => '1',
      Q => arststages_ff(0)
    );
\arststages_ff_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(0),
      Q => arststages_ff(1)
    );
\arststages_ff_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(1),
      Q => arststages_ff(2)
    );
\arststages_ff_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(2),
      Q => arststages_ff(3)
    );
\arststages_ff_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '1'
    )
        port map (
      C => dest_clk,
      CE => '1',
      CLR => src_arst,
      D => arststages_ff(3),
      Q => arststages_ff(4)
    );
dest_arst_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => arststages_ff(4),
      O => dest_arst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single : entity is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single : entity is "SINGLE";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ : entity is 4;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ : entity is "SINGLE";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_RX_STARTUP_FSM is
  port (
    p_4_in : out STD_LOGIC;
    GT_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_rxuserrdy_in3_out : out STD_LOGIC;
    gt0_rx_cdrlocked_reg : out STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC;
    gt0_rxusrclk_in : in STD_LOGIC;
    SOFT_RESET_RX_IN : in STD_LOGIC;
    DONT_RESET_ON_DATA_ERROR_IN : in STD_LOGIC;
    gt0_rxsysclksel_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gtrxreset_in : in STD_LOGIC;
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_rxuserrdy_in : in STD_LOGIC;
    gt0_rx_cdrlocked_reg_0 : in STD_LOGIC;
    \gt0_rx_cdrlock_counter_reg[0]\ : in STD_LOGIC;
    \gt0_rx_cdrlock_counter_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \gt0_rx_cdrlock_counter_reg[7]\ : in STD_LOGIC;
    data_in : in STD_LOGIC;
    GT0_DATA_VALID_IN : in STD_LOGIC;
    gt0_cplllock_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_RX_STARTUP_FSM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_RX_STARTUP_FSM is
  signal \CPLL_RESET_i_1__0_n_0\ : STD_LOGIC;
  signal \CPLL_RESET_i_2__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \^gt_rx_fsm_reset_done_out\ : STD_LOGIC;
  signal RXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal check_tlock_max_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_reg_n_0 : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_valid_sync : STD_LOGIC;
  signal gtrxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count_reg__0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \init_wait_done_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_done_i_2__0_n_0\ : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_4__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count_reg__0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 5 downto 1 );
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_2_in : STD_LOGIC;
  signal p_3_in : STD_LOGIC;
  signal \^p_4_in\ : STD_LOGIC;
  signal \pll_reset_asserted_i_1__0_n_0\ : STD_LOGIC;
  signal pll_reset_asserted_i_2_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal \reset_time_out_i_2__0_n_0\ : STD_LOGIC;
  signal reset_time_out_i_4_n_0 : STD_LOGIC;
  signal reset_time_out_reg_n_0 : STD_LOGIC;
  signal \run_phase_alignment_int_i_1__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s2 : STD_LOGIC;
  signal run_phase_alignment_int_s3_reg_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal rx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of rx_state : signal is "yes";
  signal rx_state01_out : STD_LOGIC;
  signal rx_state128_out : STD_LOGIC;
  signal rxresetdone_s2 : STD_LOGIC;
  signal rxresetdone_s3 : STD_LOGIC;
  signal sync_QPLLLOCK_n_1 : STD_LOGIC;
  signal sync_QPLLLOCK_n_2 : STD_LOGIC;
  signal sync_data_valid_n_1 : STD_LOGIC;
  signal sync_data_valid_n_2 : STD_LOGIC;
  signal sync_data_valid_n_3 : STD_LOGIC;
  signal sync_data_valid_n_4 : STD_LOGIC;
  signal sync_data_valid_n_5 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_1 : STD_LOGIC;
  signal time_out_100us_i_1_n_0 : STD_LOGIC;
  signal time_out_100us_i_2_n_0 : STD_LOGIC;
  signal time_out_100us_i_3_n_0 : STD_LOGIC;
  signal time_out_100us_i_4_n_0 : STD_LOGIC;
  signal time_out_100us_reg_n_0 : STD_LOGIC;
  signal time_out_1us_i_1_n_0 : STD_LOGIC;
  signal time_out_1us_i_2_n_0 : STD_LOGIC;
  signal time_out_1us_reg_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_5__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_6__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_7__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_8_n_0\ : STD_LOGIC;
  signal \time_out_counter[12]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[12]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[12]_i_4__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[12]_i_5__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[16]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[16]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[4]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[4]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[4]_i_4__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[4]_i_5__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[8]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[8]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[8]_i_4__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[8]_i_5__0_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \time_out_counter_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_1__0_n_0\ : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max : STD_LOGIC;
  signal time_tlock_max1 : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_1\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_2\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_3\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal time_tlock_max1_carry_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_5_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_6_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_7_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_1 : STD_LOGIC;
  signal time_tlock_max1_carry_n_2 : STD_LOGIC;
  signal time_tlock_max1_carry_n_3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_10__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_5__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_6__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_7__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_8__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_9__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[12]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[4]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[4]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[4]_i_4__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[4]_i_5__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[8]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[8]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[8]_i_4__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[8]_i_5__0_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt_reg__0\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_time_tlock_max1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[2]_i_2\ : label is "soft_lutpair9";
  attribute KEEP : string;
  attribute KEEP of \FSM_sequential_rx_state_reg[0]\ : label is "yes";
  attribute KEEP of \FSM_sequential_rx_state_reg[1]\ : label is "yes";
  attribute KEEP of \FSM_sequential_rx_state_reg[2]\ : label is "yes";
  attribute KEEP of \FSM_sequential_rx_state_reg[3]\ : label is "yes";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of time_out_100us_i_4 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of time_out_1us_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of time_tlock_max_i_1 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1__0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1__0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1__0\ : label is "soft_lutpair3";
begin
  GT_RX_FSM_RESET_DONE_OUT <= \^gt_rx_fsm_reset_done_out\;
  SR(0) <= \^sr\(0);
  p_4_in <= \^p_4_in\;
\CPLL_RESET_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => \CPLL_RESET_i_2__0_n_0\,
      I2 => \^p_4_in\,
      O => \CPLL_RESET_i_1__0_n_0\
    );
\CPLL_RESET_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000200"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(2),
      I2 => gt0_rxsysclksel_in(0),
      I3 => gt0_txsysclksel_in(0),
      I4 => rx_state(3),
      I5 => rx_state(1),
      O => \CPLL_RESET_i_2__0_n_0\
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => \CPLL_RESET_i_1__0_n_0\,
      Q => \^p_4_in\,
      R => SOFT_RESET_RX_IN
    );
\FSM_sequential_rx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4E0AEE2A4E0ACE0A"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(1),
      I2 => rx_state(0),
      I3 => time_out_2ms_reg_n_0,
      I4 => reset_time_out_reg_n_0,
      I5 => time_tlock_max,
      O => \FSM_sequential_rx_state[0]_i_2_n_0\
    );
\FSM_sequential_rx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111004015150040"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => time_out_2ms_reg_n_0,
      I4 => rx_state(2),
      I5 => rx_state128_out,
      O => \FSM_sequential_rx_state[2]_i_1_n_0\
    );
\FSM_sequential_rx_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_tlock_max,
      I1 => reset_time_out_reg_n_0,
      O => rx_state128_out
    );
\FSM_sequential_rx_state[3]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3B38"
    )
        port map (
      I0 => gt0_rx_cdrlocked_reg_0,
      I1 => rx_state(2),
      I2 => rx_state(3),
      I3 => init_wait_done_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_6_n_0\
    );
\FSM_sequential_rx_state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sync_data_valid_n_5,
      D => sync_data_valid_n_4,
      Q => rx_state(0),
      R => SOFT_RESET_RX_IN
    );
\FSM_sequential_rx_state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sync_data_valid_n_5,
      D => sync_data_valid_n_3,
      Q => rx_state(1),
      R => SOFT_RESET_RX_IN
    );
\FSM_sequential_rx_state_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sync_data_valid_n_5,
      D => \FSM_sequential_rx_state[2]_i_1_n_0\,
      Q => rx_state(2),
      R => SOFT_RESET_RX_IN
    );
\FSM_sequential_rx_state_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sync_data_valid_n_5,
      D => sync_data_valid_n_2,
      Q => rx_state(3),
      R => SOFT_RESET_RX_IN
    );
RXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => p_3_in,
      O => RXUSERRDY_i_1_n_0
    );
RXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => RXUSERRDY_i_1_n_0,
      Q => p_3_in,
      R => SOFT_RESET_RX_IN
    );
check_tlock_max_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => check_tlock_max_reg_n_0,
      O => check_tlock_max_i_1_n_0
    );
check_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => check_tlock_max_i_1_n_0,
      Q => check_tlock_max_reg_n_0,
      R => SOFT_RESET_RX_IN
    );
gt0_rx_cdrlocked_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AABAAAAA"
    )
        port map (
      I0 => gt0_rx_cdrlocked_reg_0,
      I1 => \gt0_rx_cdrlock_counter_reg[0]\,
      I2 => \gt0_rx_cdrlock_counter_reg[3]\,
      I3 => Q(0),
      I4 => \gt0_rx_cdrlock_counter_reg[7]\,
      I5 => \^sr\(0),
      O => gt0_rx_cdrlocked_reg
    );
gthe2_i_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => gt0_gtrxreset_in,
      I1 => p_2_in,
      O => \^sr\(0)
    );
gthe2_i_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => gt0_rxuserrdy_in,
      I1 => p_3_in,
      O => gt0_rxuserrdy_in3_out
    );
gtrxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD0004"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => p_2_in,
      O => gtrxreset_i_i_1_n_0
    );
gtrxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => gtrxreset_i_i_1_n_0,
      Q => p_2_in,
      R => SOFT_RESET_RX_IN
    );
\init_wait_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \init_wait_count_reg__0\(0),
      O => \init_wait_count[0]_i_1__0_n_0\
    );
\init_wait_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \init_wait_count_reg__0\(0),
      I1 => \init_wait_count_reg__0\(1),
      O => p_0_in(1)
    );
\init_wait_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \init_wait_count_reg__0\(1),
      I1 => \init_wait_count_reg__0\(0),
      I2 => \init_wait_count_reg__0\(2),
      O => p_0_in(2)
    );
\init_wait_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \init_wait_count_reg__0\(2),
      I1 => \init_wait_count_reg__0\(0),
      I2 => \init_wait_count_reg__0\(1),
      I3 => \init_wait_count_reg__0\(3),
      O => p_0_in(3)
    );
\init_wait_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \init_wait_count_reg__0\(3),
      I1 => \init_wait_count_reg__0\(1),
      I2 => \init_wait_count_reg__0\(0),
      I3 => \init_wait_count_reg__0\(2),
      I4 => \init_wait_count_reg__0\(4),
      O => p_0_in(4)
    );
\init_wait_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF7FFF"
    )
        port map (
      I0 => \init_wait_count_reg__0\(4),
      I1 => \init_wait_count_reg__0\(5),
      I2 => \init_wait_count_reg__0\(2),
      I3 => \init_wait_count_reg__0\(3),
      I4 => \init_wait_count_reg__0\(1),
      I5 => \init_wait_count_reg__0\(0),
      O => \init_wait_count[5]_i_1__0_n_0\
    );
\init_wait_count[5]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \init_wait_count_reg__0\(4),
      I1 => \init_wait_count_reg__0\(2),
      I2 => \init_wait_count_reg__0\(0),
      I3 => \init_wait_count_reg__0\(1),
      I4 => \init_wait_count_reg__0\(3),
      I5 => \init_wait_count_reg__0\(5),
      O => p_0_in(5)
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1__0_n_0\,
      CLR => SOFT_RESET_RX_IN,
      D => \init_wait_count[0]_i_1__0_n_0\,
      Q => \init_wait_count_reg__0\(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1__0_n_0\,
      CLR => SOFT_RESET_RX_IN,
      D => p_0_in(1),
      Q => \init_wait_count_reg__0\(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1__0_n_0\,
      CLR => SOFT_RESET_RX_IN,
      D => p_0_in(2),
      Q => \init_wait_count_reg__0\(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1__0_n_0\,
      CLR => SOFT_RESET_RX_IN,
      D => p_0_in(3),
      Q => \init_wait_count_reg__0\(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1__0_n_0\,
      CLR => SOFT_RESET_RX_IN,
      D => p_0_in(4),
      Q => \init_wait_count_reg__0\(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1__0_n_0\,
      CLR => SOFT_RESET_RX_IN,
      D => p_0_in(5),
      Q => \init_wait_count_reg__0\(5)
    );
\init_wait_done_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF02000000"
    )
        port map (
      I0 => \init_wait_done_i_2__0_n_0\,
      I1 => \init_wait_count_reg__0\(1),
      I2 => \init_wait_count_reg__0\(0),
      I3 => \init_wait_count_reg__0\(3),
      I4 => \init_wait_count_reg__0\(2),
      I5 => init_wait_done_reg_n_0,
      O => \init_wait_done_i_1__0_n_0\
    );
\init_wait_done_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \init_wait_count_reg__0\(4),
      I1 => \init_wait_count_reg__0\(5),
      O => \init_wait_done_i_2__0_n_0\
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      CLR => SOFT_RESET_RX_IN,
      D => \init_wait_done_i_1__0_n_0\,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(0),
      O => \p_0_in__0\(0)
    );
\mmcm_lock_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(0),
      I1 => \mmcm_lock_count_reg__0\(1),
      O => \p_0_in__0\(1)
    );
\mmcm_lock_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(1),
      I1 => \mmcm_lock_count_reg__0\(0),
      I2 => \mmcm_lock_count_reg__0\(2),
      O => \p_0_in__0\(2)
    );
\mmcm_lock_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(2),
      I1 => \mmcm_lock_count_reg__0\(0),
      I2 => \mmcm_lock_count_reg__0\(1),
      I3 => \mmcm_lock_count_reg__0\(3),
      O => \p_0_in__0\(3)
    );
\mmcm_lock_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(3),
      I1 => \mmcm_lock_count_reg__0\(1),
      I2 => \mmcm_lock_count_reg__0\(0),
      I3 => \mmcm_lock_count_reg__0\(2),
      I4 => \mmcm_lock_count_reg__0\(4),
      O => \p_0_in__0\(4)
    );
\mmcm_lock_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(4),
      I1 => \mmcm_lock_count_reg__0\(2),
      I2 => \mmcm_lock_count_reg__0\(0),
      I3 => \mmcm_lock_count_reg__0\(1),
      I4 => \mmcm_lock_count_reg__0\(3),
      I5 => \mmcm_lock_count_reg__0\(5),
      O => \p_0_in__0\(5)
    );
\mmcm_lock_count[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_count[7]_i_4__0_n_0\,
      I1 => \mmcm_lock_count_reg__0\(6),
      O => \p_0_in__0\(6)
    );
\mmcm_lock_count[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_count[7]_i_4__0_n_0\,
      I1 => \mmcm_lock_count_reg__0\(6),
      I2 => \mmcm_lock_count_reg__0\(7),
      O => \mmcm_lock_count[7]_i_2__0_n_0\
    );
\mmcm_lock_count[7]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(6),
      I1 => \mmcm_lock_count[7]_i_4__0_n_0\,
      I2 => \mmcm_lock_count_reg__0\(7),
      O => \p_0_in__0\(7)
    );
\mmcm_lock_count[7]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(4),
      I1 => \mmcm_lock_count_reg__0\(2),
      I2 => \mmcm_lock_count_reg__0\(0),
      I3 => \mmcm_lock_count_reg__0\(1),
      I4 => \mmcm_lock_count_reg__0\(3),
      I5 => \mmcm_lock_count_reg__0\(5),
      O => \mmcm_lock_count[7]_i_4__0_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__0\(0),
      Q => \mmcm_lock_count_reg__0\(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__0\(1),
      Q => \mmcm_lock_count_reg__0\(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__0\(2),
      Q => \mmcm_lock_count_reg__0\(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__0\(3),
      Q => \mmcm_lock_count_reg__0\(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__0\(4),
      Q => \mmcm_lock_count_reg__0\(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__0\(5),
      Q => \mmcm_lock_count_reg__0\(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__0\(6),
      Q => \mmcm_lock_count_reg__0\(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__0\(7),
      Q => \mmcm_lock_count_reg__0\(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => sync_mmcm_lock_reclocked_n_1,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
\pll_reset_asserted_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFC0001"
    )
        port map (
      I0 => rx_state(1),
      I1 => rx_state(3),
      I2 => rx_state(2),
      I3 => pll_reset_asserted_i_2_n_0,
      I4 => pll_reset_asserted_reg_n_0,
      O => \pll_reset_asserted_i_1__0_n_0\
    );
pll_reset_asserted_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00EBFFFF"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => gt0_rxsysclksel_in(0),
      I2 => gt0_txsysclksel_in(0),
      I3 => rx_state(1),
      I4 => rx_state(0),
      O => pll_reset_asserted_i_2_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => \pll_reset_asserted_i_1__0_n_0\,
      Q => pll_reset_asserted_reg_n_0,
      R => SOFT_RESET_RX_IN
    );
\reset_time_out_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => gt0_rx_cdrlocked_reg_0,
      I1 => rx_state(0),
      I2 => rx_state(3),
      O => \reset_time_out_i_2__0_n_0\
    );
reset_time_out_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44400040"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => mmcm_lock_reclocked,
      I3 => rx_state(1),
      I4 => rxresetdone_s3,
      O => reset_time_out_i_4_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => sync_QPLLLOCK_n_2,
      Q => reset_time_out_reg_n_0,
      S => SOFT_RESET_RX_IN
    );
\run_phase_alignment_int_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => \run_phase_alignment_int_i_1__0_n_0\
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => \run_phase_alignment_int_i_1__0_n_0\,
      Q => run_phase_alignment_int_reg_n_0,
      R => SOFT_RESET_RX_IN
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => run_phase_alignment_int_s2,
      Q => run_phase_alignment_int_s3_reg_n_0,
      R => '0'
    );
rx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => sync_data_valid_n_1,
      Q => \^gt_rx_fsm_reset_done_out\,
      R => SOFT_RESET_RX_IN
    );
rx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => rx_fsm_reset_done_int_s2,
      Q => rx_fsm_reset_done_int_s3,
      R => '0'
    );
rxresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => rxresetdone_s2,
      Q => rxresetdone_s3,
      R => '0'
    );
sync_CPLLLOCK: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_6
     port map (
      SYSCLK_IN => SYSCLK_IN,
      data_out => cplllock_sync,
      gt0_cplllock_out => gt0_cplllock_out
    );
sync_QPLLLOCK: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_7
     port map (
      \FSM_sequential_rx_state_reg[0]\ => sync_QPLLLOCK_n_1,
      \FSM_sequential_rx_state_reg[3]\ => reset_time_out_i_4_n_0,
      SYSCLK_IN => SYSCLK_IN,
      data_out => cplllock_sync,
      data_sync_reg6_0 => data_valid_sync,
      gt0_rx_cdrlocked_reg => \reset_time_out_i_2__0_n_0\,
      gt0_rx_cdrlocked_reg_0 => gt0_rx_cdrlocked_reg_0,
      gt0_rxsysclksel_in(0) => gt0_rxsysclksel_in(0),
      gt0_txsysclksel_in(0) => gt0_txsysclksel_in(0),
      \out\(3 downto 0) => rx_state(3 downto 0),
      pll_reset_asserted_reg => pll_reset_asserted_reg_n_0,
      reset_time_out_reg => sync_QPLLLOCK_n_2,
      reset_time_out_reg_0 => reset_time_out_reg_n_0,
      rx_state01_out => rx_state01_out,
      rxresetdone_s3 => rxresetdone_s3,
      time_out_2ms_reg => time_out_2ms_reg_n_0
    );
sync_RXRESETDONE: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_8
     port map (
      SYSCLK_IN => SYSCLK_IN,
      data_in => data_in,
      data_out => rxresetdone_s2
    );
sync_data_valid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_9
     port map (
      D(2) => sync_data_valid_n_2,
      D(1) => sync_data_valid_n_3,
      D(0) => sync_data_valid_n_4,
      DONT_RESET_ON_DATA_ERROR_IN => DONT_RESET_ON_DATA_ERROR_IN,
      E(0) => sync_data_valid_n_5,
      \FSM_sequential_rx_state_reg[2]\ => \FSM_sequential_rx_state[0]_i_2_n_0\,
      GT0_DATA_VALID_IN => GT0_DATA_VALID_IN,
      GT_RX_FSM_RESET_DONE_OUT => \^gt_rx_fsm_reset_done_out\,
      Q(0) => \wait_time_cnt_reg__0\(6),
      SYSCLK_IN => SYSCLK_IN,
      data_out => data_valid_sync,
      gt0_rx_cdrlocked_reg => \FSM_sequential_rx_state[3]_i_6_n_0\,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      \out\(3 downto 0) => rx_state(3 downto 0),
      reset_time_out_reg => reset_time_out_reg_n_0,
      rx_fsm_reset_done_int_reg => sync_data_valid_n_1,
      rx_state01_out => rx_state01_out,
      rx_state128_out => rx_state128_out,
      time_out_100us_reg => time_out_100us_reg_n_0,
      time_out_1us_reg => time_out_1us_reg_n_0,
      time_out_2ms_reg => sync_QPLLLOCK_n_1,
      time_out_2ms_reg_0 => time_out_2ms_reg_n_0,
      time_out_wait_bypass_s3 => time_out_wait_bypass_s3,
      \wait_time_cnt_reg[4]\ => \wait_time_cnt[6]_i_4__0_n_0\
    );
sync_mmcm_lock_reclocked: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_10
     port map (
      Q(1 downto 0) => \mmcm_lock_count_reg__0\(7 downto 6),
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      SYSCLK_IN => SYSCLK_IN,
      \mmcm_lock_count_reg[4]\ => \mmcm_lock_count[7]_i_4__0_n_0\,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      mmcm_lock_reclocked_reg => sync_mmcm_lock_reclocked_n_1
    );
sync_run_phase_alignment_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_11
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => run_phase_alignment_int_s2,
      gt0_rxusrclk_in => gt0_rxusrclk_in
    );
sync_rx_fsm_reset_done_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_12
     port map (
      GT_RX_FSM_RESET_DONE_OUT => \^gt_rx_fsm_reset_done_out\,
      data_out => rx_fsm_reset_done_int_s2,
      gt0_rxusrclk_in => gt0_rxusrclk_in
    );
sync_time_out_wait_bypass: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_13
     port map (
      SYSCLK_IN => SYSCLK_IN,
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2
    );
time_out_100us_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF02"
    )
        port map (
      I0 => time_out_100us_i_2_n_0,
      I1 => time_out_100us_i_3_n_0,
      I2 => \time_out_counter[0]_i_3_n_0\,
      I3 => time_out_100us_reg_n_0,
      O => time_out_100us_i_1_n_0
    );
time_out_100us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000004000000"
    )
        port map (
      I0 => time_out_100us_i_4_n_0,
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(11),
      I3 => time_out_counter_reg(4),
      I4 => time_out_counter_reg(9),
      I5 => time_out_counter_reg(6),
      O => time_out_100us_i_2_n_0
    );
time_out_100us_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(0),
      I3 => time_out_counter_reg(1),
      I4 => time_out_counter_reg(12),
      I5 => time_out_counter_reg(7),
      O => time_out_100us_i_3_n_0
    );
time_out_100us_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => time_out_100us_i_4_n_0
    );
time_out_100us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => time_out_100us_i_1_n_0,
      Q => time_out_100us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_1us_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0004"
    )
        port map (
      I0 => \time_out_counter[0]_i_4_n_0\,
      I1 => time_out_1us_i_2_n_0,
      I2 => time_out_counter_reg(17),
      I3 => time_out_counter_reg(16),
      I4 => time_out_1us_reg_n_0,
      O => time_out_1us_i_1_n_0
    );
time_out_1us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(8),
      I2 => time_out_counter_reg(2),
      I3 => time_out_counter_reg(3),
      I4 => time_out_counter_reg(11),
      I5 => time_out_counter_reg(10),
      O => time_out_1us_i_2_n_0
    );
time_out_1us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => time_out_1us_i_1_n_0,
      Q => time_out_1us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_2ms_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00004000"
    )
        port map (
      I0 => \time_out_counter[0]_i_4_n_0\,
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(17),
      I3 => time_out_counter_reg(11),
      I4 => \time_out_counter[0]_i_3_n_0\,
      I5 => time_out_2ms_reg_n_0,
      O => time_out_2ms_i_1_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => reset_time_out_reg_n_0
    );
\time_out_counter[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFBFFF"
    )
        port map (
      I0 => \time_out_counter[0]_i_3_n_0\,
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(17),
      I3 => time_out_counter_reg(11),
      I4 => \time_out_counter[0]_i_4_n_0\,
      O => time_out_counter
    );
\time_out_counter[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFFFF"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(2),
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(10),
      I4 => time_out_counter_reg(8),
      O => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(4),
      I3 => time_out_counter_reg(6),
      I4 => time_out_100us_i_3_n_0,
      O => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter[0]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(3),
      O => \time_out_counter[0]_i_5__0_n_0\
    );
\time_out_counter[0]_i_6__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(2),
      O => \time_out_counter[0]_i_6__0_n_0\
    );
\time_out_counter[0]_i_7__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(1),
      O => \time_out_counter[0]_i_7__0_n_0\
    );
\time_out_counter[0]_i_8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_8_n_0\
    );
\time_out_counter[12]_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(15),
      O => \time_out_counter[12]_i_2__0_n_0\
    );
\time_out_counter[12]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      O => \time_out_counter[12]_i_3__0_n_0\
    );
\time_out_counter[12]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(13),
      O => \time_out_counter[12]_i_4__0_n_0\
    );
\time_out_counter[12]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(12),
      O => \time_out_counter[12]_i_5__0_n_0\
    );
\time_out_counter[16]_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(17),
      O => \time_out_counter[16]_i_2__0_n_0\
    );
\time_out_counter[16]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(16),
      O => \time_out_counter[16]_i_3__0_n_0\
    );
\time_out_counter[4]_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(7),
      O => \time_out_counter[4]_i_2__0_n_0\
    );
\time_out_counter[4]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(6),
      O => \time_out_counter[4]_i_3__0_n_0\
    );
\time_out_counter[4]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      O => \time_out_counter[4]_i_4__0_n_0\
    );
\time_out_counter[4]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(4),
      O => \time_out_counter[4]_i_5__0_n_0\
    );
\time_out_counter[8]_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(11),
      O => \time_out_counter[8]_i_2__0_n_0\
    );
\time_out_counter[8]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(10),
      O => \time_out_counter[8]_i_3__0_n_0\
    );
\time_out_counter[8]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      O => \time_out_counter[8]_i_4__0_n_0\
    );
\time_out_counter[8]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(8),
      O => \time_out_counter[8]_i_5__0_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2__0_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2__0_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2__0_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2__0_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2__0_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2__0_n_7\,
      S(3) => \time_out_counter[0]_i_5__0_n_0\,
      S(2) => \time_out_counter[0]_i_6__0_n_0\,
      S(1) => \time_out_counter[0]_i_7__0_n_0\,
      S(0) => \time_out_counter[0]_i_8_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1__0_n_7\,
      S(3) => \time_out_counter[12]_i_2__0_n_0\,
      S(2) => \time_out_counter[12]_i_3__0_n_0\,
      S(1) => \time_out_counter[12]_i_4__0_n_0\,
      S(0) => \time_out_counter[12]_i_5__0_n_0\
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(3 downto 1) => \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \time_out_counter_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\(3 downto 2),
      O(1) => \time_out_counter_reg[16]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1__0_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \time_out_counter[16]_i_2__0_n_0\,
      S(0) => \time_out_counter[16]_i_3__0_n_0\
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1__0_n_7\,
      S(3) => \time_out_counter[4]_i_2__0_n_0\,
      S(2) => \time_out_counter[4]_i_3__0_n_0\,
      S(1) => \time_out_counter[4]_i_4__0_n_0\,
      S(0) => \time_out_counter[4]_i_5__0_n_0\
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1__0_n_7\,
      S(3) => \time_out_counter[8]_i_2__0_n_0\,
      S(2) => \time_out_counter[8]_i_3__0_n_0\,
      S(1) => \time_out_counter[8]_i_4__0_n_0\,
      S(0) => \time_out_counter[8]_i_5__0_n_0\
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out_reg_n_0
    );
\time_out_wait_bypass_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => rx_fsm_reset_done_int_s3,
      I2 => \wait_bypass_count[0]_i_4__0_n_0\,
      I3 => run_phase_alignment_int_s3_reg_n_0,
      O => \time_out_wait_bypass_i_1__0_n_0\
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_rxusrclk_in,
      CE => '1',
      D => \time_out_wait_bypass_i_1__0_n_0\,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => time_tlock_max1_carry_n_0,
      CO(2) => time_tlock_max1_carry_n_1,
      CO(1) => time_tlock_max1_carry_n_2,
      CO(0) => time_tlock_max1_carry_n_3,
      CYINIT => '0',
      DI(3) => time_tlock_max1_carry_i_1_n_0,
      DI(2) => time_out_counter_reg(5),
      DI(1) => time_tlock_max1_carry_i_2_n_0,
      DI(0) => time_tlock_max1_carry_i_3_n_0,
      O(3 downto 0) => NLW_time_tlock_max1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => time_tlock_max1_carry_i_4_n_0,
      S(2) => time_tlock_max1_carry_i_5_n_0,
      S(1) => time_tlock_max1_carry_i_6_n_0,
      S(0) => time_tlock_max1_carry_i_7_n_0
    );
\time_tlock_max1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => time_tlock_max1_carry_n_0,
      CO(3) => \time_tlock_max1_carry__0_n_0\,
      CO(2) => \time_tlock_max1_carry__0_n_1\,
      CO(1) => \time_tlock_max1_carry__0_n_2\,
      CO(0) => \time_tlock_max1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \time_tlock_max1_carry__0_i_1_n_0\,
      DI(2) => \time_tlock_max1_carry__0_i_2_n_0\,
      DI(1) => time_out_counter_reg(11),
      DI(0) => '0',
      O(3 downto 0) => \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \time_tlock_max1_carry__0_i_3_n_0\,
      S(2) => \time_tlock_max1_carry__0_i_4_n_0\,
      S(1) => \time_tlock_max1_carry__0_i_5_n_0\,
      S(0) => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      O => \time_tlock_max1_carry__0_i_1_n_0\
    );
\time_tlock_max1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(13),
      O => \time_tlock_max1_carry__0_i_2_n_0\
    );
\time_tlock_max1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(14),
      O => \time_tlock_max1_carry__0_i_3_n_0\
    );
\time_tlock_max1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(12),
      O => \time_tlock_max1_carry__0_i_4_n_0\
    );
\time_tlock_max1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(11),
      O => \time_tlock_max1_carry__0_i_5_n_0\
    );
\time_tlock_max1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(8),
      I1 => time_out_counter_reg(9),
      O => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_tlock_max1_carry__0_n_0\,
      CO(3 downto 1) => \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\(3 downto 1),
      CO(0) => time_tlock_max1,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \time_tlock_max1_carry__1_i_1_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \time_tlock_max1_carry__1_i_2_n_0\
    );
\time_tlock_max1_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_tlock_max1_carry__1_i_1_n_0\
    );
\time_tlock_max1_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(16),
      O => \time_tlock_max1_carry__1_i_2_n_0\
    );
time_tlock_max1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_1_n_0
    );
time_tlock_max1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      O => time_tlock_max1_carry_i_2_n_0
    );
time_tlock_max1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(1),
      O => time_tlock_max1_carry_i_3_n_0
    );
time_tlock_max1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_4_n_0
    );
time_tlock_max1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(5),
      O => time_tlock_max1_carry_i_5_n_0
    );
time_tlock_max1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(3),
      I1 => time_out_counter_reg(2),
      O => time_tlock_max1_carry_i_6_n_0
    );
time_tlock_max1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      O => time_tlock_max1_carry_i_7_n_0
    );
time_tlock_max_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => check_tlock_max_reg_n_0,
      I1 => time_tlock_max1,
      I2 => time_tlock_max,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max,
      R => reset_time_out_reg_n_0
    );
\wait_bypass_count[0]_i_10__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => wait_bypass_count_reg(2),
      I1 => wait_bypass_count_reg(12),
      I2 => wait_bypass_count_reg(4),
      I3 => wait_bypass_count_reg(10),
      I4 => wait_bypass_count_reg(6),
      I5 => wait_bypass_count_reg(11),
      O => \wait_bypass_count[0]_i_10__0_n_0\
    );
\wait_bypass_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3_reg_n_0,
      O => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \wait_bypass_count[0]_i_4__0_n_0\,
      I1 => rx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2__0_n_0\
    );
\wait_bypass_count[0]_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFFFFFF"
    )
        port map (
      I0 => \wait_bypass_count[0]_i_9__0_n_0\,
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(8),
      I3 => wait_bypass_count_reg(0),
      I4 => \wait_bypass_count[0]_i_10__0_n_0\,
      O => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count[0]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(3),
      O => \wait_bypass_count[0]_i_5__0_n_0\
    );
\wait_bypass_count[0]_i_6__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(2),
      O => \wait_bypass_count[0]_i_6__0_n_0\
    );
\wait_bypass_count[0]_i_7__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(1),
      O => \wait_bypass_count[0]_i_7__0_n_0\
    );
\wait_bypass_count[0]_i_8__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_8__0_n_0\
    );
\wait_bypass_count[0]_i_9__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(3),
      I1 => wait_bypass_count_reg(5),
      I2 => wait_bypass_count_reg(9),
      I3 => wait_bypass_count_reg(7),
      O => \wait_bypass_count[0]_i_9__0_n_0\
    );
\wait_bypass_count[12]_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(12),
      O => \wait_bypass_count[12]_i_2__0_n_0\
    );
\wait_bypass_count[4]_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(7),
      O => \wait_bypass_count[4]_i_2__0_n_0\
    );
\wait_bypass_count[4]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(6),
      O => \wait_bypass_count[4]_i_3__0_n_0\
    );
\wait_bypass_count[4]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      O => \wait_bypass_count[4]_i_4__0_n_0\
    );
\wait_bypass_count[4]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      O => \wait_bypass_count[4]_i_5__0_n_0\
    );
\wait_bypass_count[8]_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(11),
      O => \wait_bypass_count[8]_i_2__0_n_0\
    );
\wait_bypass_count[8]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(10),
      O => \wait_bypass_count[8]_i_3__0_n_0\
    );
\wait_bypass_count[8]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      O => \wait_bypass_count[8]_i_4__0_n_0\
    );
\wait_bypass_count[8]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      O => \wait_bypass_count[8]_i_5__0_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      Q => wait_bypass_count_reg(0),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[0]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3__0_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3__0_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      S(3) => \wait_bypass_count[0]_i_5__0_n_0\,
      S(2) => \wait_bypass_count[0]_i_6__0_n_0\,
      S(1) => \wait_bypass_count[0]_i_7__0_n_0\,
      S(0) => \wait_bypass_count[0]_i_8__0_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(10),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(11),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(12),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \wait_bypass_count[12]_i_2__0_n_0\
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      Q => wait_bypass_count_reg(1),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      Q => wait_bypass_count_reg(2),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      Q => wait_bypass_count_reg(3),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(4),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      S(3) => \wait_bypass_count[4]_i_2__0_n_0\,
      S(2) => \wait_bypass_count[4]_i_3__0_n_0\,
      S(1) => \wait_bypass_count[4]_i_4__0_n_0\,
      S(0) => \wait_bypass_count[4]_i_5__0_n_0\
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(5),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(6),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(7),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(8),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      S(3) => \wait_bypass_count[8]_i_2__0_n_0\,
      S(2) => \wait_bypass_count[8]_i_3__0_n_0\,
      S(1) => \wait_bypass_count[8]_i_4__0_n_0\,
      S(0) => \wait_bypass_count[8]_i_5__0_n_0\
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_rxusrclk_in,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(9),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_time_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(1),
      I1 => \wait_time_cnt_reg__0\(0),
      O => wait_time_cnt0(1)
    );
\wait_time_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(2),
      I1 => \wait_time_cnt_reg__0\(0),
      I2 => \wait_time_cnt_reg__0\(1),
      O => wait_time_cnt0(2)
    );
\wait_time_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(3),
      I1 => \wait_time_cnt_reg__0\(1),
      I2 => \wait_time_cnt_reg__0\(0),
      I3 => \wait_time_cnt_reg__0\(2),
      O => wait_time_cnt0(3)
    );
\wait_time_cnt[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(4),
      I1 => \wait_time_cnt_reg__0\(2),
      I2 => \wait_time_cnt_reg__0\(0),
      I3 => \wait_time_cnt_reg__0\(1),
      I4 => \wait_time_cnt_reg__0\(3),
      O => wait_time_cnt0(4)
    );
\wait_time_cnt[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(5),
      I1 => \wait_time_cnt_reg__0\(3),
      I2 => \wait_time_cnt_reg__0\(1),
      I3 => \wait_time_cnt_reg__0\(0),
      I4 => \wait_time_cnt_reg__0\(2),
      I5 => \wait_time_cnt_reg__0\(4),
      O => wait_time_cnt0(5)
    );
\wait_time_cnt[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(3),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => \wait_time_cnt_reg__0\(6),
      O => \wait_time_cnt[6]_i_2__0_n_0\
    );
\wait_time_cnt[6]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(6),
      I1 => \wait_time_cnt[6]_i_4__0_n_0\,
      O => wait_time_cnt0(6)
    );
\wait_time_cnt[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(4),
      I1 => \wait_time_cnt_reg__0\(2),
      I2 => \wait_time_cnt_reg__0\(0),
      I3 => \wait_time_cnt_reg__0\(1),
      I4 => \wait_time_cnt_reg__0\(3),
      I5 => \wait_time_cnt_reg__0\(5),
      O => \wait_time_cnt[6]_i_4__0_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => wait_time_cnt0(0),
      Q => \wait_time_cnt_reg__0\(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => wait_time_cnt0(1),
      Q => \wait_time_cnt_reg__0\(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => SYSCLK_IN,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => wait_time_cnt0(2),
      Q => \wait_time_cnt_reg__0\(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => wait_time_cnt0(3),
      Q => \wait_time_cnt_reg__0\(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => wait_time_cnt0(4),
      Q => \wait_time_cnt_reg__0\(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => SYSCLK_IN,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => wait_time_cnt0(5),
      Q => \wait_time_cnt_reg__0\(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => SYSCLK_IN,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => wait_time_cnt0(6),
      Q => \wait_time_cnt_reg__0\(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_TX_STARTUP_FSM is
  port (
    p_5_in : out STD_LOGIC;
    GT_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;
    gt0_gttxreset_in1_out : out STD_LOGIC;
    gt0_txuserrdy_in0_out : out STD_LOGIC;
    SYSCLK_IN : in STD_LOGIC;
    gt0_txusrclk_in : in STD_LOGIC;
    SOFT_RESET_TX_IN : in STD_LOGIC;
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in : in STD_LOGIC;
    gt0_txuserrdy_in : in STD_LOGIC;
    data_in : in STD_LOGIC;
    gt0_cplllock_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_TX_STARTUP_FSM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_TX_STARTUP_FSM is
  signal CPLL_RESET_i_1_n_0 : STD_LOGIC;
  signal CPLL_RESET_i_2_n_0 : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_11_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_9_n_0\ : STD_LOGIC;
  signal \^gt_tx_fsm_reset_done_out\ : STD_LOGIC;
  signal TXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_out : STD_LOGIC;
  signal gttxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[5]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count_reg__0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal init_wait_done_i_1_n_0 : STD_LOGIC;
  signal init_wait_done_i_2_n_0 : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_4_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count_reg__0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 5 downto 1 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_1_in : STD_LOGIC;
  signal \^p_5_in\ : STD_LOGIC;
  signal pll_reset_asserted_i_1_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal qplllock_sync : STD_LOGIC;
  signal reset_time_out : STD_LOGIC;
  signal reset_time_out_i_5_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_i_1_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3 : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_CPLLLOCK_n_1 : STD_LOGIC;
  signal sync_QPLLLOCK_n_1 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_1 : STD_LOGIC;
  signal \time_out_2ms_i_1__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_2_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_500us_i_1_n_0 : STD_LOGIC;
  signal time_out_500us_i_2_n_0 : STD_LOGIC;
  signal time_out_500us_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_10_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_11_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_5_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_6_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_7_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_8__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_9_n_0\ : STD_LOGIC;
  signal \time_out_counter[12]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter[12]_i_3_n_0\ : STD_LOGIC;
  signal \time_out_counter[12]_i_4_n_0\ : STD_LOGIC;
  signal \time_out_counter[12]_i_5_n_0\ : STD_LOGIC;
  signal \time_out_counter[16]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter[16]_i_3_n_0\ : STD_LOGIC;
  signal \time_out_counter[4]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter[4]_i_3_n_0\ : STD_LOGIC;
  signal \time_out_counter[4]_i_4_n_0\ : STD_LOGIC;
  signal \time_out_counter[4]_i_5_n_0\ : STD_LOGIC;
  signal \time_out_counter[8]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter[8]_i_3_n_0\ : STD_LOGIC;
  signal \time_out_counter[8]_i_4_n_0\ : STD_LOGIC;
  signal \time_out_counter[8]_i_5_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \time_out_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal \time_tlock_max_i_1__0_n_0\ : STD_LOGIC;
  signal time_tlock_max_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max_reg_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_i_1_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal tx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of tx_state : signal is "yes";
  signal tx_state111_out : STD_LOGIC;
  signal txresetdone_s2 : STD_LOGIC;
  signal txresetdone_s3 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_10_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_11_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_12_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_8_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_9_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[12]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[12]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[12]_i_4_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[12]_i_5_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[4]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[4]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[4]_i_4_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[4]_i_5_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[8]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[8]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[8]_i_4_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[8]_i_5_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal \wait_time_cnt_reg__0\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_wait_bypass_count_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute KEEP : string;
  attribute KEEP of \FSM_sequential_tx_state_reg[0]\ : label is "yes";
  attribute KEEP of \FSM_sequential_tx_state_reg[1]\ : label is "yes";
  attribute KEEP of \FSM_sequential_tx_state_reg[2]\ : label is "yes";
  attribute KEEP of \FSM_sequential_tx_state_reg[3]\ : label is "yes";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \time_out_counter[0]_i_11\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of time_tlock_max_i_2 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1\ : label is "soft_lutpair16";
begin
  GT_TX_FSM_RESET_DONE_OUT <= \^gt_tx_fsm_reset_done_out\;
  p_5_in <= \^p_5_in\;
CPLL_RESET_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF700000004"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => gt0_txsysclksel_in(0),
      I4 => CPLL_RESET_i_2_n_0,
      I5 => \^p_5_in\,
      O => CPLL_RESET_i_1_n_0
    );
CPLL_RESET_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(1),
      O => CPLL_RESET_i_2_n_0
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => CPLL_RESET_i_1_n_0,
      Q => \^p_5_in\,
      R => SOFT_RESET_TX_IN
    );
\FSM_sequential_tx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222220222220A0A"
    )
        port map (
      I0 => \FSM_sequential_tx_state[0]_i_2_n_0\,
      I1 => tx_state(3),
      I2 => tx_state(0),
      I3 => time_out_2ms_reg_n_0,
      I4 => tx_state(2),
      I5 => tx_state(1),
      O => \FSM_sequential_tx_state[0]_i_1_n_0\
    );
\FSM_sequential_tx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3B33BBBBBBBBBBBB"
    )
        port map (
      I0 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I1 => tx_state(0),
      I2 => reset_time_out,
      I3 => time_out_500us_reg_n_0,
      I4 => tx_state(1),
      I5 => tx_state(2),
      O => \FSM_sequential_tx_state[0]_i_2_n_0\
    );
\FSM_sequential_tx_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11110444"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state111_out,
      I3 => tx_state(2),
      I4 => tx_state(1),
      O => \FSM_sequential_tx_state[1]_i_1_n_0\
    );
\FSM_sequential_tx_state[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => reset_time_out,
      I2 => time_tlock_max_reg_n_0,
      O => tx_state111_out
    );
\FSM_sequential_tx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111004055550040"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(1),
      I3 => time_out_2ms_reg_n_0,
      I4 => tx_state(2),
      I5 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      O => \FSM_sequential_tx_state[2]_i_1_n_0\
    );
\FSM_sequential_tx_state[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF02"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => reset_time_out,
      I2 => mmcm_lock_reclocked,
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[2]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_out_500us_reg_n_0,
      I2 => txresetdone_s3,
      O => \FSM_sequential_tx_state[3]_i_11_n_0\
    );
\FSM_sequential_tx_state[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00A00B00"
    )
        port map (
      I0 => \FSM_sequential_tx_state[3]_i_5_n_0\,
      I1 => time_out_wait_bypass_s3,
      I2 => tx_state(2),
      I3 => tx_state(3),
      I4 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      O => \FSM_sequential_tx_state[3]_i_4_n_0\
    );
\FSM_sequential_tx_state[3]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => tx_state(0),
      I1 => reset_time_out,
      I2 => time_out_500us_reg_n_0,
      O => \FSM_sequential_tx_state[3]_i_5_n_0\
    );
\FSM_sequential_tx_state[3]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_tlock_max_reg_n_0,
      I2 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[3]_i_9_n_0\
    );
\FSM_sequential_tx_state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sync_CPLLLOCK_n_1,
      D => \FSM_sequential_tx_state[0]_i_1_n_0\,
      Q => tx_state(0),
      R => SOFT_RESET_TX_IN
    );
\FSM_sequential_tx_state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sync_CPLLLOCK_n_1,
      D => \FSM_sequential_tx_state[1]_i_1_n_0\,
      Q => tx_state(1),
      R => SOFT_RESET_TX_IN
    );
\FSM_sequential_tx_state_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sync_CPLLLOCK_n_1,
      D => \FSM_sequential_tx_state[2]_i_1_n_0\,
      Q => tx_state(2),
      R => SOFT_RESET_TX_IN
    );
\FSM_sequential_tx_state_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sync_CPLLLOCK_n_1,
      D => \FSM_sequential_tx_state[3]_i_2_n_0\,
      Q => tx_state(3),
      R => SOFT_RESET_TX_IN
    );
TXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(1),
      I3 => tx_state(2),
      I4 => p_0_in,
      O => TXUSERRDY_i_1_n_0
    );
TXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => TXUSERRDY_i_1_n_0,
      Q => p_0_in,
      R => SOFT_RESET_TX_IN
    );
gthe2_i_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => gt0_gttxreset_in,
      I1 => p_1_in,
      O => gt0_gttxreset_in1_out
    );
gthe2_i_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => gt0_txuserrdy_in,
      I1 => p_0_in,
      O => gt0_txuserrdy_in0_out
    );
gttxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD0004"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(1),
      I3 => tx_state(3),
      I4 => p_1_in,
      O => gttxreset_i_i_1_n_0
    );
gttxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => gttxreset_i_i_1_n_0,
      Q => p_1_in,
      R => SOFT_RESET_TX_IN
    );
\init_wait_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \init_wait_count_reg__0\(0),
      O => \init_wait_count[0]_i_1_n_0\
    );
\init_wait_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \init_wait_count_reg__0\(0),
      I1 => \init_wait_count_reg__0\(1),
      O => \p_0_in__0\(1)
    );
\init_wait_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \init_wait_count_reg__0\(1),
      I1 => \init_wait_count_reg__0\(0),
      I2 => \init_wait_count_reg__0\(2),
      O => \p_0_in__0\(2)
    );
\init_wait_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \init_wait_count_reg__0\(2),
      I1 => \init_wait_count_reg__0\(0),
      I2 => \init_wait_count_reg__0\(1),
      I3 => \init_wait_count_reg__0\(3),
      O => \p_0_in__0\(3)
    );
\init_wait_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \init_wait_count_reg__0\(3),
      I1 => \init_wait_count_reg__0\(1),
      I2 => \init_wait_count_reg__0\(0),
      I3 => \init_wait_count_reg__0\(2),
      I4 => \init_wait_count_reg__0\(4),
      O => \p_0_in__0\(4)
    );
\init_wait_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF7FFF"
    )
        port map (
      I0 => \init_wait_count_reg__0\(4),
      I1 => \init_wait_count_reg__0\(5),
      I2 => \init_wait_count_reg__0\(2),
      I3 => \init_wait_count_reg__0\(3),
      I4 => \init_wait_count_reg__0\(1),
      I5 => \init_wait_count_reg__0\(0),
      O => \init_wait_count[5]_i_1_n_0\
    );
\init_wait_count[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \init_wait_count_reg__0\(4),
      I1 => \init_wait_count_reg__0\(2),
      I2 => \init_wait_count_reg__0\(0),
      I3 => \init_wait_count_reg__0\(1),
      I4 => \init_wait_count_reg__0\(3),
      I5 => \init_wait_count_reg__0\(5),
      O => \p_0_in__0\(5)
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1_n_0\,
      CLR => SOFT_RESET_TX_IN,
      D => \init_wait_count[0]_i_1_n_0\,
      Q => \init_wait_count_reg__0\(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1_n_0\,
      CLR => SOFT_RESET_TX_IN,
      D => \p_0_in__0\(1),
      Q => \init_wait_count_reg__0\(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1_n_0\,
      CLR => SOFT_RESET_TX_IN,
      D => \p_0_in__0\(2),
      Q => \init_wait_count_reg__0\(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1_n_0\,
      CLR => SOFT_RESET_TX_IN,
      D => \p_0_in__0\(3),
      Q => \init_wait_count_reg__0\(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1_n_0\,
      CLR => SOFT_RESET_TX_IN,
      D => \p_0_in__0\(4),
      Q => \init_wait_count_reg__0\(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \init_wait_count[5]_i_1_n_0\,
      CLR => SOFT_RESET_TX_IN,
      D => \p_0_in__0\(5),
      Q => \init_wait_count_reg__0\(5)
    );
init_wait_done_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF02000000"
    )
        port map (
      I0 => init_wait_done_i_2_n_0,
      I1 => \init_wait_count_reg__0\(1),
      I2 => \init_wait_count_reg__0\(0),
      I3 => \init_wait_count_reg__0\(3),
      I4 => \init_wait_count_reg__0\(2),
      I5 => init_wait_done_reg_n_0,
      O => init_wait_done_i_1_n_0
    );
init_wait_done_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \init_wait_count_reg__0\(4),
      I1 => \init_wait_count_reg__0\(5),
      O => init_wait_done_i_2_n_0
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      CLR => SOFT_RESET_TX_IN,
      D => init_wait_done_i_1_n_0,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(0),
      O => \p_0_in__1\(0)
    );
\mmcm_lock_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(0),
      I1 => \mmcm_lock_count_reg__0\(1),
      O => \p_0_in__1\(1)
    );
\mmcm_lock_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(1),
      I1 => \mmcm_lock_count_reg__0\(0),
      I2 => \mmcm_lock_count_reg__0\(2),
      O => \p_0_in__1\(2)
    );
\mmcm_lock_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(2),
      I1 => \mmcm_lock_count_reg__0\(0),
      I2 => \mmcm_lock_count_reg__0\(1),
      I3 => \mmcm_lock_count_reg__0\(3),
      O => \p_0_in__1\(3)
    );
\mmcm_lock_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(3),
      I1 => \mmcm_lock_count_reg__0\(1),
      I2 => \mmcm_lock_count_reg__0\(0),
      I3 => \mmcm_lock_count_reg__0\(2),
      I4 => \mmcm_lock_count_reg__0\(4),
      O => \p_0_in__1\(4)
    );
\mmcm_lock_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(4),
      I1 => \mmcm_lock_count_reg__0\(2),
      I2 => \mmcm_lock_count_reg__0\(0),
      I3 => \mmcm_lock_count_reg__0\(1),
      I4 => \mmcm_lock_count_reg__0\(3),
      I5 => \mmcm_lock_count_reg__0\(5),
      O => \p_0_in__1\(5)
    );
\mmcm_lock_count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_count[7]_i_4_n_0\,
      I1 => \mmcm_lock_count_reg__0\(6),
      O => \p_0_in__1\(6)
    );
\mmcm_lock_count[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_count[7]_i_4_n_0\,
      I1 => \mmcm_lock_count_reg__0\(6),
      I2 => \mmcm_lock_count_reg__0\(7),
      O => \mmcm_lock_count[7]_i_2_n_0\
    );
\mmcm_lock_count[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(6),
      I1 => \mmcm_lock_count[7]_i_4_n_0\,
      I2 => \mmcm_lock_count_reg__0\(7),
      O => \p_0_in__1\(7)
    );
\mmcm_lock_count[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \mmcm_lock_count_reg__0\(4),
      I1 => \mmcm_lock_count_reg__0\(2),
      I2 => \mmcm_lock_count_reg__0\(0),
      I3 => \mmcm_lock_count_reg__0\(1),
      I4 => \mmcm_lock_count_reg__0\(3),
      I5 => \mmcm_lock_count_reg__0\(5),
      O => \mmcm_lock_count[7]_i_4_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(0),
      Q => \mmcm_lock_count_reg__0\(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(1),
      Q => \mmcm_lock_count_reg__0\(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(2),
      Q => \mmcm_lock_count_reg__0\(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(3),
      Q => \mmcm_lock_count_reg__0\(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(4),
      Q => \mmcm_lock_count_reg__0\(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(5),
      Q => \mmcm_lock_count_reg__0\(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(6),
      Q => \mmcm_lock_count_reg__0\(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(7),
      Q => \mmcm_lock_count_reg__0\(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => sync_mmcm_lock_reclocked_n_1,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
pll_reset_asserted_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EF00FF10"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => pll_reset_asserted_reg_n_0,
      I4 => tx_state(1),
      O => pll_reset_asserted_i_1_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => pll_reset_asserted_i_1_n_0,
      Q => pll_reset_asserted_reg_n_0,
      R => SOFT_RESET_TX_IN
    );
reset_time_out_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B833"
    )
        port map (
      I0 => txresetdone_s3,
      I1 => tx_state(1),
      I2 => mmcm_lock_reclocked,
      I3 => tx_state(2),
      O => reset_time_out_i_5_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => sync_QPLLLOCK_n_1,
      Q => reset_time_out,
      R => SOFT_RESET_TX_IN
    );
run_phase_alignment_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => run_phase_alignment_int_i_1_n_0
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => run_phase_alignment_int_i_1_n_0,
      Q => run_phase_alignment_int_reg_n_0,
      R => SOFT_RESET_TX_IN
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => data_out,
      Q => run_phase_alignment_int_s3,
      R => '0'
    );
sync_CPLLLOCK: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block
     port map (
      E(0) => sync_CPLLLOCK_n_1,
      \FSM_sequential_tx_state_reg[1]\ => \FSM_sequential_tx_state[3]_i_4_n_0\,
      SYSCLK_IN => SYSCLK_IN,
      data_out => cplllock_sync,
      data_sync_reg6_0 => qplllock_sync,
      gt0_cplllock_out => gt0_cplllock_out,
      gt0_txsysclksel_in(0) => gt0_txsysclksel_in(0),
      init_wait_done_reg => init_wait_done_reg_n_0,
      \out\(3 downto 0) => tx_state(3 downto 0),
      pll_reset_asserted_reg => pll_reset_asserted_reg_n_0,
      reset_time_out_reg => \FSM_sequential_tx_state[3]_i_9_n_0\,
      reset_time_out_reg_0 => \FSM_sequential_tx_state[3]_i_11_n_0\,
      time_out_2ms_reg => time_out_2ms_reg_n_0,
      \wait_time_cnt_reg[6]\(0) => sel
    );
sync_QPLLLOCK: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_0
     port map (
      SYSCLK_IN => SYSCLK_IN,
      data_out => qplllock_sync,
      data_sync_reg6_0 => cplllock_sync,
      gt0_txsysclksel_in(0) => gt0_txsysclksel_in(0),
      init_wait_done_reg => init_wait_done_reg_n_0,
      \out\(3 downto 0) => tx_state(3 downto 0),
      reset_time_out => reset_time_out,
      reset_time_out_reg => sync_QPLLLOCK_n_1,
      txresetdone_s3_reg => reset_time_out_i_5_n_0
    );
sync_TXRESETDONE: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_1
     port map (
      SYSCLK_IN => SYSCLK_IN,
      data_in => data_in,
      data_out => txresetdone_s2
    );
sync_mmcm_lock_reclocked: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_2
     port map (
      Q(1 downto 0) => \mmcm_lock_count_reg__0\(7 downto 6),
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      SYSCLK_IN => SYSCLK_IN,
      \mmcm_lock_count_reg[4]\ => \mmcm_lock_count[7]_i_4_n_0\,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      mmcm_lock_reclocked_reg => sync_mmcm_lock_reclocked_n_1
    );
sync_run_phase_alignment_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_3
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out,
      gt0_txusrclk_in => gt0_txusrclk_in
    );
sync_time_out_wait_bypass: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_4
     port map (
      SYSCLK_IN => SYSCLK_IN,
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2
    );
sync_tx_fsm_reset_done_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_5
     port map (
      GT_TX_FSM_RESET_DONE_OUT => \^gt_tx_fsm_reset_done_out\,
      data_out => tx_fsm_reset_done_int_s2,
      gt0_txusrclk_in => gt0_txusrclk_in
    );
\time_out_2ms_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => time_out_2ms_i_2_n_0,
      I2 => \time_out_counter[0]_i_3__0_n_0\,
      I3 => reset_time_out,
      O => \time_out_2ms_i_1__0_n_0\
    );
time_out_2ms_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(11),
      I4 => time_out_counter_reg(4),
      I5 => \time_out_counter[0]_i_4__0_n_0\,
      O => time_out_2ms_i_2_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => \time_out_2ms_i_1__0_n_0\,
      Q => time_out_2ms_reg_n_0,
      R => '0'
    );
time_out_500us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEAAA"
    )
        port map (
      I0 => time_out_500us_reg_n_0,
      I1 => time_out_500us_i_2_n_0,
      I2 => time_out_counter_reg(4),
      I3 => time_out_counter_reg(9),
      I4 => \time_out_counter[0]_i_3__0_n_0\,
      I5 => reset_time_out,
      O => time_out_500us_i_1_n_0
    );
time_out_500us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(11),
      I4 => time_out_counter_reg(17),
      I5 => time_out_counter_reg(16),
      O => time_out_500us_i_2_n_0
    );
time_out_500us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => time_out_500us_i_1_n_0,
      Q => time_out_500us_reg_n_0,
      R => '0'
    );
\time_out_counter[0]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(5),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(12),
      O => \time_out_counter[0]_i_10_n_0\
    );
\time_out_counter[0]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(2),
      O => \time_out_counter[0]_i_11_n_0\
    );
\time_out_counter[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => \time_out_counter[0]_i_3__0_n_0\,
      I1 => time_out_counter_reg(11),
      I2 => \time_out_counter[0]_i_4__0_n_0\,
      I3 => time_out_counter_reg(9),
      I4 => \time_out_counter[0]_i_5_n_0\,
      I5 => time_out_counter_reg(4),
      O => time_out_counter
    );
\time_out_counter[0]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(13),
      I2 => \time_out_counter[0]_i_10_n_0\,
      I3 => \time_out_counter[0]_i_11_n_0\,
      O => \time_out_counter[0]_i_3__0_n_0\
    );
\time_out_counter[0]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(14),
      I2 => time_out_counter_reg(10),
      O => \time_out_counter[0]_i_4__0_n_0\
    );
\time_out_counter[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_out_counter[0]_i_5_n_0\
    );
\time_out_counter[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(3),
      O => \time_out_counter[0]_i_6_n_0\
    );
\time_out_counter[0]_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(2),
      O => \time_out_counter[0]_i_7_n_0\
    );
\time_out_counter[0]_i_8__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(1),
      O => \time_out_counter[0]_i_8__0_n_0\
    );
\time_out_counter[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_9_n_0\
    );
\time_out_counter[12]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(15),
      O => \time_out_counter[12]_i_2_n_0\
    );
\time_out_counter[12]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      O => \time_out_counter[12]_i_3_n_0\
    );
\time_out_counter[12]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(13),
      O => \time_out_counter[12]_i_4_n_0\
    );
\time_out_counter[12]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(12),
      O => \time_out_counter[12]_i_5_n_0\
    );
\time_out_counter[16]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(17),
      O => \time_out_counter[16]_i_2_n_0\
    );
\time_out_counter[16]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(16),
      O => \time_out_counter[16]_i_3_n_0\
    );
\time_out_counter[4]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(7),
      O => \time_out_counter[4]_i_2_n_0\
    );
\time_out_counter[4]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(6),
      O => \time_out_counter[4]_i_3_n_0\
    );
\time_out_counter[4]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      O => \time_out_counter[4]_i_4_n_0\
    );
\time_out_counter[4]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(4),
      O => \time_out_counter[4]_i_5_n_0\
    );
\time_out_counter[8]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(11),
      O => \time_out_counter[8]_i_2_n_0\
    );
\time_out_counter[8]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(10),
      O => \time_out_counter[8]_i_3_n_0\
    );
\time_out_counter[8]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      O => \time_out_counter[8]_i_4_n_0\
    );
\time_out_counter[8]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(8),
      O => \time_out_counter[8]_i_5_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out
    );
\time_out_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2_n_7\,
      S(3) => \time_out_counter[0]_i_6_n_0\,
      S(2) => \time_out_counter[0]_i_7_n_0\,
      S(1) => \time_out_counter[0]_i_8__0_n_0\,
      S(0) => \time_out_counter[0]_i_9_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out
    );
\time_out_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1_n_7\,
      S(3) => \time_out_counter[12]_i_2_n_0\,
      S(2) => \time_out_counter[12]_i_3_n_0\,
      S(1) => \time_out_counter[12]_i_4_n_0\,
      S(0) => \time_out_counter[12]_i_5_n_0\
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out
    );
\time_out_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1_n_0\,
      CO(3 downto 1) => \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \time_out_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\(3 downto 2),
      O(1) => \time_out_counter_reg[16]_i_1_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \time_out_counter[16]_i_2_n_0\,
      S(0) => \time_out_counter[16]_i_3_n_0\
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out
    );
\time_out_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1_n_7\,
      S(3) => \time_out_counter[4]_i_2_n_0\,
      S(2) => \time_out_counter[4]_i_3_n_0\,
      S(1) => \time_out_counter[4]_i_4_n_0\,
      S(0) => \time_out_counter[4]_i_5_n_0\
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out
    );
\time_out_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1_n_7\,
      S(3) => \time_out_counter[8]_i_2_n_0\,
      S(2) => \time_out_counter[8]_i_3_n_0\,
      S(1) => \time_out_counter[8]_i_4_n_0\,
      S(0) => \time_out_counter[8]_i_5_n_0\
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => \wait_bypass_count[0]_i_4_n_0\,
      I2 => tx_fsm_reset_done_int_s3,
      I3 => run_phase_alignment_int_s3,
      O => time_out_wait_bypass_i_1_n_0
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
\time_tlock_max_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AABAAAAA"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => time_tlock_max_i_2_n_0,
      I2 => time_out_counter_reg(4),
      I3 => \time_out_counter[0]_i_4__0_n_0\,
      I4 => time_tlock_max_i_3_n_0,
      I5 => reset_time_out,
      O => \time_tlock_max_i_1__0_n_0\
    );
time_tlock_max_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(0),
      I3 => time_out_counter_reg(1),
      I4 => \time_out_counter[0]_i_10_n_0\,
      O => time_tlock_max_i_2_n_0
    );
time_tlock_max_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(11),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(6),
      I4 => time_out_counter_reg(17),
      I5 => time_out_counter_reg(16),
      O => time_tlock_max_i_3_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => \time_tlock_max_i_1__0_n_0\,
      Q => time_tlock_max_reg_n_0,
      R => '0'
    );
tx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0008"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => \^gt_tx_fsm_reset_done_out\,
      O => tx_fsm_reset_done_int_i_1_n_0
    );
tx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => tx_fsm_reset_done_int_i_1_n_0,
      Q => \^gt_tx_fsm_reset_done_out\,
      R => SOFT_RESET_TX_IN
    );
tx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gt0_txusrclk_in,
      CE => '1',
      D => tx_fsm_reset_done_int_s2,
      Q => tx_fsm_reset_done_int_s3,
      R => '0'
    );
txresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => '1',
      D => txresetdone_s2,
      Q => txresetdone_s3,
      R => '0'
    );
\wait_bypass_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3,
      O => clear
    );
\wait_bypass_count[0]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => wait_bypass_count_reg(1),
      I1 => wait_bypass_count_reg(0),
      I2 => wait_bypass_count_reg(3),
      I3 => wait_bypass_count_reg(2),
      O => \wait_bypass_count[0]_i_10_n_0\
    );
\wait_bypass_count[0]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => wait_bypass_count_reg(13),
      I1 => wait_bypass_count_reg(12),
      I2 => wait_bypass_count_reg(15),
      I3 => wait_bypass_count_reg(14),
      O => \wait_bypass_count[0]_i_11_n_0\
    );
\wait_bypass_count[0]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      I1 => wait_bypass_count_reg(8),
      I2 => wait_bypass_count_reg(11),
      I3 => wait_bypass_count_reg(10),
      O => \wait_bypass_count[0]_i_12_n_0\
    );
\wait_bypass_count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \wait_bypass_count[0]_i_4_n_0\,
      I1 => tx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2_n_0\
    );
\wait_bypass_count[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \wait_bypass_count[0]_i_9_n_0\,
      I1 => \wait_bypass_count[0]_i_10_n_0\,
      I2 => \wait_bypass_count[0]_i_11_n_0\,
      I3 => \wait_bypass_count[0]_i_12_n_0\,
      O => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(3),
      O => \wait_bypass_count[0]_i_5_n_0\
    );
\wait_bypass_count[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(2),
      O => \wait_bypass_count[0]_i_6_n_0\
    );
\wait_bypass_count[0]_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(1),
      O => \wait_bypass_count[0]_i_7_n_0\
    );
\wait_bypass_count[0]_i_8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_8_n_0\
    );
\wait_bypass_count[0]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      I1 => wait_bypass_count_reg(4),
      I2 => wait_bypass_count_reg(7),
      I3 => wait_bypass_count_reg(6),
      O => \wait_bypass_count[0]_i_9_n_0\
    );
\wait_bypass_count[12]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(15),
      O => \wait_bypass_count[12]_i_2_n_0\
    );
\wait_bypass_count[12]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(14),
      O => \wait_bypass_count[12]_i_3_n_0\
    );
\wait_bypass_count[12]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(13),
      O => \wait_bypass_count[12]_i_4_n_0\
    );
\wait_bypass_count[12]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(12),
      O => \wait_bypass_count[12]_i_5_n_0\
    );
\wait_bypass_count[4]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(7),
      O => \wait_bypass_count[4]_i_2_n_0\
    );
\wait_bypass_count[4]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(6),
      O => \wait_bypass_count[4]_i_3_n_0\
    );
\wait_bypass_count[4]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      O => \wait_bypass_count[4]_i_4_n_0\
    );
\wait_bypass_count[4]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      O => \wait_bypass_count[4]_i_5_n_0\
    );
\wait_bypass_count[8]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(11),
      O => \wait_bypass_count[8]_i_2_n_0\
    );
\wait_bypass_count[8]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(10),
      O => \wait_bypass_count[8]_i_3_n_0\
    );
\wait_bypass_count[8]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      O => \wait_bypass_count[8]_i_4_n_0\
    );
\wait_bypass_count[8]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      O => \wait_bypass_count[8]_i_5_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_7\,
      Q => wait_bypass_count_reg(0),
      R => clear
    );
\wait_bypass_count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3_n_7\,
      S(3) => \wait_bypass_count[0]_i_5_n_0\,
      S(2) => \wait_bypass_count[0]_i_6_n_0\,
      S(1) => \wait_bypass_count[0]_i_7_n_0\,
      S(0) => \wait_bypass_count[0]_i_8_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_5\,
      Q => wait_bypass_count_reg(10),
      R => clear
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_4\,
      Q => wait_bypass_count_reg(11),
      R => clear
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_7\,
      Q => wait_bypass_count_reg(12),
      R => clear
    );
\wait_bypass_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(3) => \NLW_wait_bypass_count_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \wait_bypass_count_reg[12]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[12]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[12]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[12]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[12]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[12]_i_1_n_7\,
      S(3) => \wait_bypass_count[12]_i_2_n_0\,
      S(2) => \wait_bypass_count[12]_i_3_n_0\,
      S(1) => \wait_bypass_count[12]_i_4_n_0\,
      S(0) => \wait_bypass_count[12]_i_5_n_0\
    );
\wait_bypass_count_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_6\,
      Q => wait_bypass_count_reg(13),
      R => clear
    );
\wait_bypass_count_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_5\,
      Q => wait_bypass_count_reg(14),
      R => clear
    );
\wait_bypass_count_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_4\,
      Q => wait_bypass_count_reg(15),
      R => clear
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_6\,
      Q => wait_bypass_count_reg(1),
      R => clear
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_5\,
      Q => wait_bypass_count_reg(2),
      R => clear
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_4\,
      Q => wait_bypass_count_reg(3),
      R => clear
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_7\,
      Q => wait_bypass_count_reg(4),
      R => clear
    );
\wait_bypass_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1_n_7\,
      S(3) => \wait_bypass_count[4]_i_2_n_0\,
      S(2) => \wait_bypass_count[4]_i_3_n_0\,
      S(1) => \wait_bypass_count[4]_i_4_n_0\,
      S(0) => \wait_bypass_count[4]_i_5_n_0\
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_6\,
      Q => wait_bypass_count_reg(5),
      R => clear
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_5\,
      Q => wait_bypass_count_reg(6),
      R => clear
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_4\,
      Q => wait_bypass_count_reg(7),
      R => clear
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_7\,
      Q => wait_bypass_count_reg(8),
      R => clear
    );
\wait_bypass_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1_n_7\,
      S(3) => \wait_bypass_count[8]_i_2_n_0\,
      S(2) => \wait_bypass_count[8]_i_3_n_0\,
      S(1) => \wait_bypass_count[8]_i_4_n_0\,
      S(0) => \wait_bypass_count[8]_i_5_n_0\
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gt0_txusrclk_in,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_6\,
      Q => wait_bypass_count_reg(9),
      R => clear
    );
\wait_time_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(1),
      I1 => \wait_time_cnt_reg__0\(0),
      O => wait_time_cnt0(1)
    );
\wait_time_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(2),
      I1 => \wait_time_cnt_reg__0\(0),
      I2 => \wait_time_cnt_reg__0\(1),
      O => wait_time_cnt0(2)
    );
\wait_time_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(3),
      I1 => \wait_time_cnt_reg__0\(1),
      I2 => \wait_time_cnt_reg__0\(0),
      I3 => \wait_time_cnt_reg__0\(2),
      O => wait_time_cnt0(3)
    );
\wait_time_cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(4),
      I1 => \wait_time_cnt_reg__0\(2),
      I2 => \wait_time_cnt_reg__0\(0),
      I3 => \wait_time_cnt_reg__0\(1),
      I4 => \wait_time_cnt_reg__0\(3),
      O => wait_time_cnt0(4)
    );
\wait_time_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(5),
      I1 => \wait_time_cnt_reg__0\(3),
      I2 => \wait_time_cnt_reg__0\(1),
      I3 => \wait_time_cnt_reg__0\(0),
      I4 => \wait_time_cnt_reg__0\(2),
      I5 => \wait_time_cnt_reg__0\(4),
      O => wait_time_cnt0(5)
    );
\wait_time_cnt[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"004C"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(1),
      I3 => tx_state(3),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => \wait_time_cnt_reg__0\(6),
      O => sel
    );
\wait_time_cnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(6),
      I1 => \wait_time_cnt[6]_i_4_n_0\,
      O => wait_time_cnt0(6)
    );
\wait_time_cnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \wait_time_cnt_reg__0\(4),
      I1 => \wait_time_cnt_reg__0\(2),
      I2 => \wait_time_cnt_reg__0\(0),
      I3 => \wait_time_cnt_reg__0\(1),
      I4 => \wait_time_cnt_reg__0\(3),
      I5 => \wait_time_cnt_reg__0\(5),
      O => \wait_time_cnt[6]_i_4_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sel,
      D => wait_time_cnt0(0),
      Q => \wait_time_cnt_reg__0\(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sel,
      D => wait_time_cnt0(1),
      Q => \wait_time_cnt_reg__0\(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => SYSCLK_IN,
      CE => sel,
      D => wait_time_cnt0(2),
      Q => \wait_time_cnt_reg__0\(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sel,
      D => wait_time_cnt0(3),
      Q => \wait_time_cnt_reg__0\(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => sel,
      D => wait_time_cnt0(4),
      Q => \wait_time_cnt_reg__0\(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => SYSCLK_IN,
      CE => sel,
      D => wait_time_cnt0(5),
      Q => \wait_time_cnt_reg__0\(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => SYSCLK_IN,
      CE => sel,
      D => wait_time_cnt0(6),
      Q => \wait_time_cnt_reg__0\(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_common_wrapper is
  port (
    common0_qpll_clk_in : out STD_LOGIC;
    common0_qpll_refclk_in : out STD_LOGIC;
    cpll_refclk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_common_wrapper;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_common_wrapper is
begin
jesd204_0_common: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gtwizard_0_common
     port map (
      common0_qpll_clk_in => common0_qpll_clk_in,
      common0_qpll_refclk_in => common0_qpll_refclk_in,
      cpll_refclk => cpll_refclk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_multi_gt is
  port (
    gt0_cpllfbclklost_out : out STD_LOGIC;
    gt0_cplllock_out : out STD_LOGIC;
    gt0_drprdy_out : out STD_LOGIC;
    gt0_eyescandataerror_out : out STD_LOGIC;
    gt0_gthtxn_out : out STD_LOGIC;
    gt0_gthtxp_out : out STD_LOGIC;
    gt0_rxbyteisaligned_out : out STD_LOGIC;
    gt0_rxbyterealign_out : out STD_LOGIC;
    gt0_rxcommadet_out : out STD_LOGIC;
    gt0_rxoutclk_out : out STD_LOGIC;
    gt0_rxoutclkfabric_out : out STD_LOGIC;
    gt0_rxprbserr_out : out STD_LOGIC;
    gt0_rxresetdone_out : out STD_LOGIC;
    gt0_txoutclk_out : out STD_LOGIC;
    gt0_txoutclkfabric_out : out STD_LOGIC;
    gt0_txoutclkpcs_out : out STD_LOGIC;
    gt0_txresetdone_out : out STD_LOGIC;
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );
    gt0_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_txbufstatus_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxbufstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxdata_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );
    gt0_rxchariscomma_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxcharisk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxdisperr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxnotintable_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    data_in : out STD_LOGIC;
    data_sync_reg1 : out STD_LOGIC;
    gt0_cplllockdetclk_in : in STD_LOGIC;
    gt0_drpclk_in : in STD_LOGIC;
    gt0_drpen_in : in STD_LOGIC;
    gt0_drpwe_in : in STD_LOGIC;
    gt0_eyescanreset_in : in STD_LOGIC;
    gt0_eyescantrigger_in : in STD_LOGIC;
    gt0_gthrxn_in : in STD_LOGIC;
    gt0_gthrxp_in : in STD_LOGIC;
    gt0_gtnorthrefclk0_in : in STD_LOGIC;
    gt0_gtnorthrefclk1_in : in STD_LOGIC;
    gt0_gtrefclk0_in : in STD_LOGIC;
    gt0_gtrefclk1_in : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gtsouthrefclk0_in : in STD_LOGIC;
    gt0_gtsouthrefclk1_in : in STD_LOGIC;
    gt0_gttxreset_in1_out : in STD_LOGIC;
    GT0_QPLLOUTCLK_IN : in STD_LOGIC;
    GT0_QPLLOUTREFCLK_IN : in STD_LOGIC;
    gt0_rxbufreset_in : in STD_LOGIC;
    gt0_rxcdrhold_in : in STD_LOGIC;
    gt0_rxdfelpmreset_in : in STD_LOGIC;
    gt0_rxlpmen_in : in STD_LOGIC;
    gt0_rxmcommaalignen_in : in STD_LOGIC;
    gt0_rxpcommaalignen_in : in STD_LOGIC;
    gt0_rxpcsreset_in : in STD_LOGIC;
    gt0_rxpmareset_in : in STD_LOGIC;
    gt0_rxpolarity_in : in STD_LOGIC;
    gt0_rxprbscntreset_in : in STD_LOGIC;
    gt0_rxuserrdy_in3_out : in STD_LOGIC;
    gt0_rxusrclk_in : in STD_LOGIC;
    gt0_rxusrclk2_in : in STD_LOGIC;
    gt0_txinhibit_in : in STD_LOGIC;
    gt0_txpcsreset_in : in STD_LOGIC;
    gt0_txpmareset_in : in STD_LOGIC;
    gt0_txpolarity_in : in STD_LOGIC;
    gt0_txprbsforceerr_in : in STD_LOGIC;
    gt0_txuserrdy_in0_out : in STD_LOGIC;
    gt0_txusrclk_in : in STD_LOGIC;
    gt0_txusrclk2_in : in STD_LOGIC;
    gt0_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_loopback_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txdiffctrl_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_txpostcursor_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_txprecursor_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_txdata_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_txcharisk_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    p_5_in : in STD_LOGIC;
    gt0_cpllreset_in : in STD_LOGIC;
    p_4_in : in STD_LOGIC;
    gt0_cpllpd_in : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_multi_gt;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_multi_gt is
  signal cpllpd_in : STD_LOGIC;
  signal cpllreset_in : STD_LOGIC;
begin
cpll_railing0_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_cpll_railing
     port map (
      cpllpd_in => cpllpd_in,
      cpllreset_in => cpllreset_in,
      gt0_cpllpd_in => gt0_cpllpd_in,
      gt0_cpllreset_in => gt0_cpllreset_in,
      gt0_gtrefclk0_in => gt0_gtrefclk0_in,
      p_4_in => p_4_in,
      p_5_in => p_5_in
    );
gt0_Jesd204_microblaze_jesd204_phy_0_0_gt_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_GT
     port map (
      GT0_QPLLOUTCLK_IN => GT0_QPLLOUTCLK_IN,
      GT0_QPLLOUTREFCLK_IN => GT0_QPLLOUTREFCLK_IN,
      SR(0) => SR(0),
      cpllpd_in => cpllpd_in,
      cpllreset_in => cpllreset_in,
      data_in => data_in,
      data_sync_reg1 => data_sync_reg1,
      gt0_cpllfbclklost_out => gt0_cpllfbclklost_out,
      gt0_cplllock_out => gt0_cplllock_out,
      gt0_cplllockdetclk_in => gt0_cplllockdetclk_in,
      gt0_dmonitorout_out(14 downto 0) => gt0_dmonitorout_out(14 downto 0),
      gt0_drpaddr_in(8 downto 0) => gt0_drpaddr_in(8 downto 0),
      gt0_drpclk_in => gt0_drpclk_in,
      gt0_drpdi_in(15 downto 0) => gt0_drpdi_in(15 downto 0),
      gt0_drpdo_out(15 downto 0) => gt0_drpdo_out(15 downto 0),
      gt0_drpen_in => gt0_drpen_in,
      gt0_drprdy_out => gt0_drprdy_out,
      gt0_drpwe_in => gt0_drpwe_in,
      gt0_eyescandataerror_out => gt0_eyescandataerror_out,
      gt0_eyescanreset_in => gt0_eyescanreset_in,
      gt0_eyescantrigger_in => gt0_eyescantrigger_in,
      gt0_gthrxn_in => gt0_gthrxn_in,
      gt0_gthrxp_in => gt0_gthrxp_in,
      gt0_gthtxn_out => gt0_gthtxn_out,
      gt0_gthtxp_out => gt0_gthtxp_out,
      gt0_gtnorthrefclk0_in => gt0_gtnorthrefclk0_in,
      gt0_gtnorthrefclk1_in => gt0_gtnorthrefclk1_in,
      gt0_gtrefclk0_in => gt0_gtrefclk0_in,
      gt0_gtrefclk1_in => gt0_gtrefclk1_in,
      gt0_gtsouthrefclk0_in => gt0_gtsouthrefclk0_in,
      gt0_gtsouthrefclk1_in => gt0_gtsouthrefclk1_in,
      gt0_gttxreset_in1_out => gt0_gttxreset_in1_out,
      gt0_loopback_in(2 downto 0) => gt0_loopback_in(2 downto 0),
      gt0_rxbufreset_in => gt0_rxbufreset_in,
      gt0_rxbufstatus_out(2 downto 0) => gt0_rxbufstatus_out(2 downto 0),
      gt0_rxbyteisaligned_out => gt0_rxbyteisaligned_out,
      gt0_rxbyterealign_out => gt0_rxbyterealign_out,
      gt0_rxcdrhold_in => gt0_rxcdrhold_in,
      gt0_rxchariscomma_out(3 downto 0) => gt0_rxchariscomma_out(3 downto 0),
      gt0_rxcharisk_out(3 downto 0) => gt0_rxcharisk_out(3 downto 0),
      gt0_rxcommadet_out => gt0_rxcommadet_out,
      gt0_rxdata_out(31 downto 0) => gt0_rxdata_out(31 downto 0),
      gt0_rxdfelpmreset_in => gt0_rxdfelpmreset_in,
      gt0_rxdisperr_out(3 downto 0) => gt0_rxdisperr_out(3 downto 0),
      gt0_rxlpmen_in => gt0_rxlpmen_in,
      gt0_rxmcommaalignen_in => gt0_rxmcommaalignen_in,
      gt0_rxmonitorout_out(6 downto 0) => gt0_rxmonitorout_out(6 downto 0),
      gt0_rxmonitorsel_in(1 downto 0) => gt0_rxmonitorsel_in(1 downto 0),
      gt0_rxnotintable_out(3 downto 0) => gt0_rxnotintable_out(3 downto 0),
      gt0_rxoutclk_out => gt0_rxoutclk_out,
      gt0_rxoutclkfabric_out => gt0_rxoutclkfabric_out,
      gt0_rxpcommaalignen_in => gt0_rxpcommaalignen_in,
      gt0_rxpcsreset_in => gt0_rxpcsreset_in,
      gt0_rxpd_in(1 downto 0) => gt0_rxpd_in(1 downto 0),
      gt0_rxpmareset_in => gt0_rxpmareset_in,
      gt0_rxpolarity_in => gt0_rxpolarity_in,
      gt0_rxprbscntreset_in => gt0_rxprbscntreset_in,
      gt0_rxprbserr_out => gt0_rxprbserr_out,
      gt0_rxprbssel_in(2 downto 0) => gt0_rxprbssel_in(2 downto 0),
      gt0_rxresetdone_out => gt0_rxresetdone_out,
      gt0_rxstatus_out(2 downto 0) => gt0_rxstatus_out(2 downto 0),
      gt0_rxsysclksel_in(1 downto 0) => gt0_rxsysclksel_in(1 downto 0),
      gt0_rxuserrdy_in3_out => gt0_rxuserrdy_in3_out,
      gt0_rxusrclk2_in => gt0_rxusrclk2_in,
      gt0_rxusrclk_in => gt0_rxusrclk_in,
      gt0_txbufstatus_out(1 downto 0) => gt0_txbufstatus_out(1 downto 0),
      gt0_txcharisk_in(3 downto 0) => gt0_txcharisk_in(3 downto 0),
      gt0_txdata_in(31 downto 0) => gt0_txdata_in(31 downto 0),
      gt0_txdiffctrl_in(3 downto 0) => gt0_txdiffctrl_in(3 downto 0),
      gt0_txinhibit_in => gt0_txinhibit_in,
      gt0_txoutclk_out => gt0_txoutclk_out,
      gt0_txoutclkfabric_out => gt0_txoutclkfabric_out,
      gt0_txoutclkpcs_out => gt0_txoutclkpcs_out,
      gt0_txpcsreset_in => gt0_txpcsreset_in,
      gt0_txpd_in(1 downto 0) => gt0_txpd_in(1 downto 0),
      gt0_txpmareset_in => gt0_txpmareset_in,
      gt0_txpolarity_in => gt0_txpolarity_in,
      gt0_txpostcursor_in(4 downto 0) => gt0_txpostcursor_in(4 downto 0),
      gt0_txprbsforceerr_in => gt0_txprbsforceerr_in,
      gt0_txprbssel_in(2 downto 0) => gt0_txprbssel_in(2 downto 0),
      gt0_txprecursor_in(4 downto 0) => gt0_txprecursor_in(4 downto 0),
      gt0_txresetdone_out => gt0_txresetdone_out,
      gt0_txsysclksel_in(1 downto 0) => gt0_txsysclksel_in(1 downto 0),
      gt0_txuserrdy_in0_out => gt0_txuserrdy_in0_out,
      gt0_txusrclk2_in => gt0_txusrclk2_in,
      gt0_txusrclk_in => gt0_txusrclk_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync is
  port (
    data_out : out STD_LOGIC;
    tx_reset_done : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync is
  signal cdc_i_i_1_n_0 : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__23\
     port map (
      dest_clk => s_axi_aclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => cdc_i_i_1_n_0
    );
cdc_i_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tx_reset_done,
      O => cdc_i_i_1_n_0
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => data_tmp,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__1\ is
  port (
    clk2_ready_reg : out STD_LOGIC;
    clk2_valid_pulse_reg : out STD_LOGIC;
    clk2_valid_sync_r_reg : out STD_LOGIC;
    data_in : in STD_LOGIC;
    gt0_drprdy_out : in STD_LOGIC;
    drp_if_select : in STD_LOGIC;
    data_out : in STD_LOGIC;
    clk2_valid_sync_r : in STD_LOGIC;
    drp_access_valid_reg : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__1\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__1\ is
  signal data_sync_reg_gsr_n_0 : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of clk2_ready_i_1 : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of clk2_valid_sync_r_i_1 : label is "soft_lutpair26";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__28\
     port map (
      dest_clk => drpclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => drp_access_valid_reg
    );
clk2_ready_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88F80000"
    )
        port map (
      I0 => data_in,
      I1 => data_sync_reg_gsr_n_0,
      I2 => gt0_drprdy_out,
      I3 => drp_if_select,
      I4 => data_out,
      O => clk2_ready_reg
    );
\clk2_valid_pulse_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => clk2_valid_sync_r,
      I1 => data_sync_reg_gsr_n_0,
      I2 => data_out,
      O => clk2_valid_pulse_reg
    );
clk2_valid_sync_r_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => data_sync_reg_gsr_n_0,
      I1 => data_out,
      O => clk2_valid_sync_r_reg
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => data_sync_reg_gsr_n_0,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__10\ is
  port (
    data_out : out STD_LOGIC;
    rx_reset_done : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__10\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__10\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__10\ is
  signal \cdc_i_i_1__0_n_0\ : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__24\
     port map (
      dest_clk => s_axi_aclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => \cdc_i_i_1__0_n_0\
    );
\cdc_i_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rx_reset_done,
      O => \cdc_i_i_1__0_n_0\
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => data_tmp,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__11\ is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_aresetn : in STD_LOGIC;
    tx_core_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__11\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__11\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__11\ is
  signal data_tmp : STD_LOGIC;
  signal tx_core_reset : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__26\
     port map (
      dest_clk => tx_core_clk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => s_axi_aresetn
    );
\clk2_valid_sync_r_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tx_core_reset,
      O => SR(0)
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => '1',
      D => data_tmp,
      Q => tx_core_reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__2\ is
  port (
    drp_access_valid_reg : out STD_LOGIC;
    data_out : out STD_LOGIC;
    clk1_ready_pulse0 : out STD_LOGIC;
    data_in : in STD_LOGIC;
    access_type5_out : in STD_LOGIC;
    \s_axi_wdata[30]\ : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    gt_slv_wren : in STD_LOGIC;
    clk1_ready_sync_r : in STD_LOGIC;
    clk2_ready_reg : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__2\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__2\ is
  signal \^data_out\ : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
  data_out <= \^data_out\;
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__29\
     port map (
      dest_clk => s_axi_aclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => clk2_ready_reg
    );
clk1_ready_pulse_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk1_ready_sync_r,
      I1 => \^data_out\,
      O => clk1_ready_pulse0
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => data_tmp,
      Q => \^data_out\,
      R => '0'
    );
drp_access_valid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E200E2000000E200"
    )
        port map (
      I0 => data_in,
      I1 => access_type5_out,
      I2 => \s_axi_wdata[30]\,
      I3 => s_axi_aresetn,
      I4 => \^data_out\,
      I5 => gt_slv_wren,
      O => drp_access_valid_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__3\ is
  port (
    clk2_ready_reg : out STD_LOGIC;
    data_out : out STD_LOGIC;
    clk2_valid_pulse0 : out STD_LOGIC;
    slv_wren_clk2 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    clk2_valid_sync_r : in STD_LOGIC;
    slv_access_valid_hold_reg : in STD_LOGIC;
    rx_core_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__3\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__3\ is
  signal \^data_out\ : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \clk2_ready_i_1__1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \clk2_valid_pulse_i_1__0\ : label is "soft_lutpair29";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
  data_out <= \^data_out\;
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__32\
     port map (
      dest_clk => rx_core_clk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => slv_access_valid_hold_reg
    );
\clk2_ready_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => slv_wren_clk2,
      I1 => \^data_out\,
      I2 => data_in,
      O => clk2_ready_reg
    );
\clk2_valid_pulse_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^data_out\,
      I1 => clk2_valid_sync_r,
      O => clk2_valid_pulse0
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => rx_core_clk,
      CE => '1',
      D => data_tmp,
      Q => \^data_out\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__4\ is
  port (
    slv_access_valid_hold_reg : out STD_LOGIC;
    data_out : out STD_LOGIC;
    clk1_ready_pulse0 : out STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    chan_rx_axi_map_wready : in STD_LOGIC;
    chan_rx_slv_rden : in STD_LOGIC;
    slv_rden_r_reg : in STD_LOGIC;
    data_in : in STD_LOGIC;
    clk1_ready_sync_r : in STD_LOGIC;
    clk2_ready_reg : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__4\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__4\ is
  signal \^data_out\ : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
  data_out <= \^data_out\;
cdc_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single
     port map (
      dest_clk => s_axi_aclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => clk2_ready_reg
    );
\clk1_ready_pulse_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk1_ready_sync_r,
      I1 => \^data_out\,
      O => clk1_ready_pulse0
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => data_tmp,
      Q => \^data_out\,
      R => '0'
    );
\slv_access_valid_hold_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D5D5FFD5C0C0FFC0"
    )
        port map (
      I0 => \^data_out\,
      I1 => s_axi_wvalid,
      I2 => chan_rx_axi_map_wready,
      I3 => chan_rx_slv_rden,
      I4 => slv_rden_r_reg,
      I5 => data_in,
      O => slv_access_valid_hold_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__5\ is
  port (
    clk2_ready_reg : out STD_LOGIC;
    data_out : out STD_LOGIC;
    clk2_valid_pulse0 : out STD_LOGIC;
    clk2_valid_pulse_reg : in STD_LOGIC;
    data_in : in STD_LOGIC;
    clk2_valid_sync_r : in STD_LOGIC;
    slv_access_valid_hold_reg : in STD_LOGIC;
    tx_core_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__5\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__5\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__5\ is
  signal \^data_out\ : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \clk2_ready_i_1__0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of clk2_valid_pulse_i_1 : label is "soft_lutpair30";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
  data_out <= \^data_out\;
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__30\
     port map (
      dest_clk => tx_core_clk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => slv_access_valid_hold_reg
    );
\clk2_ready_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => clk2_valid_pulse_reg,
      I1 => \^data_out\,
      I2 => data_in,
      O => clk2_ready_reg
    );
clk2_valid_pulse_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^data_out\,
      I1 => clk2_valid_sync_r,
      O => clk2_valid_pulse0
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => '1',
      D => data_tmp,
      Q => \^data_out\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__6\ is
  port (
    slv_access_valid_hold_reg : out STD_LOGIC;
    data_out : out STD_LOGIC;
    clk1_ready_pulse0 : out STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    chan_tx_axi_map_wready : in STD_LOGIC;
    chan_tx_slv_rden : in STD_LOGIC;
    slv_rden_r_reg : in STD_LOGIC;
    data_in : in STD_LOGIC;
    clk1_ready_sync_r : in STD_LOGIC;
    clk2_ready_reg : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__6\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__6\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__6\ is
  signal \^data_out\ : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
  data_out <= \^data_out\;
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__31\
     port map (
      dest_clk => s_axi_aclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => clk2_ready_reg
    );
\clk1_ready_pulse_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk1_ready_sync_r,
      I1 => \^data_out\,
      O => clk1_ready_pulse0
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => data_tmp,
      Q => \^data_out\,
      R => '0'
    );
slv_access_valid_hold_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D5D5FFD5C0C0FFC0"
    )
        port map (
      I0 => \^data_out\,
      I1 => s_axi_wvalid,
      I2 => chan_tx_axi_map_wready,
      I3 => chan_tx_slv_rden,
      I4 => slv_rden_r_reg,
      I5 => data_in,
      O => slv_access_valid_hold_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__7\ is
  port (
    data_out : out STD_LOGIC;
    gt0_cplllock_out : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__7\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__7\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__7\ is
  signal \cdc_i_i_1__1_n_0\ : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__22\
     port map (
      dest_clk => s_axi_aclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => \cdc_i_i_1__1_n_0\
    );
\cdc_i_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => gt0_cplllock_out,
      O => \cdc_i_i_1__1_n_0\
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => data_tmp,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__8\ is
  port (
    \drp_read_data_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__8\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__8\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__8\ is
  signal \^data_out\ : STD_LOGIC;
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
  data_out <= \^data_out\;
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__25\
     port map (
      dest_clk => drpclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => s_axi_aresetn
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => \^data_out\,
      R => '0'
    );
\drp_read_data[15]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => \drp_read_data_reg[0]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__9\ is
  port (
    clk2_ready_reg : out STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    rx_core_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__9\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__9\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__9\ is
  signal data_tmp : STD_LOGIC;
  signal rx_core_reset : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__27\
     port map (
      dest_clk => rx_core_clk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => s_axi_aresetn
    );
\clk2_valid_sync_r_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rx_core_reset,
      O => clk2_ready_reg
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => rx_core_clk,
      CE => '1',
      D => data_tmp,
      Q => rx_core_reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block is
  port (
    data_out : out STD_LOGIC;
    tx_reset_gt : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block is
  signal data_tmp : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
  attribute DEF_VAL : string;
  attribute DEF_VAL of xpm_cdc_async_rst_inst : label is "1'b1";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of xpm_cdc_async_rst_inst : label is 5;
  attribute INIT : string;
  attribute INIT of xpm_cdc_async_rst_inst : label is "1";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of xpm_cdc_async_rst_inst : label is 1;
  attribute VERSION : integer;
  attribute VERSION of xpm_cdc_async_rst_inst : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of xpm_cdc_async_rst_inst : label is "ASYNC_RST";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of xpm_cdc_async_rst_inst : label is "TRUE";
begin
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => data_out,
      R => '0'
    );
xpm_cdc_async_rst_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__4\
     port map (
      dest_arst => data_tmp,
      dest_clk => drpclk,
      src_arst => tx_reset_gt
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized0\ is
  port (
    data_out : out STD_LOGIC;
    O54 : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized0\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_sync_block";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized0\ is
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__17\
     port map (
      dest_clk => s_axi_aclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => O54
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => data_tmp,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized1\ is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized1\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_sync_block";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized1\ is
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__18\
     port map (
      dest_clk => drpclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => data_in
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized2\ is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized2\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_sync_block";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized2\ is
  signal data_tmp : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__19\
     port map (
      dest_clk => drpclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => data_in
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized3\ is
  port (
    tx_reset_done_r0 : out STD_LOGIC;
    GT_TX_FSM_RESET_DONE_OUT : in STD_LOGIC;
    data_in : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized3\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_sync_block";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized3\ is
  signal data_tmp : STD_LOGIC;
  signal tx_chan_rst_done_sync : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__20\
     port map (
      dest_clk => drpclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => data_in
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => tx_chan_rst_done_sync,
      R => '0'
    );
tx_reset_done_r_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => GT_TX_FSM_RESET_DONE_OUT,
      I1 => tx_chan_rst_done_sync,
      O => tx_reset_done_r0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized4\ is
  port (
    rx_reset_done_r0 : out STD_LOGIC;
    GT_RX_FSM_RESET_DONE_OUT : in STD_LOGIC;
    data_in : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized4\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_sync_block";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized4\ is
  signal data_tmp : STD_LOGIC;
  signal rx_chan_rst_done_sync : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of cdc_i : label is 4;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of cdc_i : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of cdc_i : label is 0;
  attribute VERSION : integer;
  attribute VERSION of cdc_i : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of cdc_i : label is "SINGLE";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of cdc_i : label is "TRUE";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
begin
cdc_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_single__21\
     port map (
      dest_clk => drpclk,
      dest_out => data_tmp,
      src_clk => '0',
      src_in => data_in
    );
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => rx_chan_rst_done_sync,
      R => '0'
    );
rx_reset_done_r_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => GT_RX_FSM_RESET_DONE_OUT,
      I1 => rx_chan_rst_done_sync,
      O => rx_reset_done_r0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__1\ is
  port (
    gt_rxreset0 : out STD_LOGIC;
    data_out : in STD_LOGIC;
    GT_RX_FSM_RESET_DONE_OUT : in STD_LOGIC;
    data_in : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__1\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_sync_block";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__1\ is
  signal data_tmp : STD_LOGIC;
  signal rx_rst_all_sync : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
  attribute DEF_VAL : string;
  attribute DEF_VAL of xpm_cdc_async_rst_inst : label is "1'b1";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of xpm_cdc_async_rst_inst : label is 5;
  attribute INIT : string;
  attribute INIT of xpm_cdc_async_rst_inst : label is "1";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of xpm_cdc_async_rst_inst : label is 1;
  attribute VERSION : integer;
  attribute VERSION of xpm_cdc_async_rst_inst : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of xpm_cdc_async_rst_inst : label is "ASYNC_RST";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of xpm_cdc_async_rst_inst : label is "TRUE";
begin
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => rx_rst_all_sync,
      R => '0'
    );
gt_rxreset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => rx_rst_all_sync,
      I1 => data_out,
      I2 => GT_RX_FSM_RESET_DONE_OUT,
      O => gt_rxreset0
    );
xpm_cdc_async_rst_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst
     port map (
      dest_arst => data_tmp,
      dest_clk => drpclk,
      src_arst => data_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__2\ is
  port (
    data_out : out STD_LOGIC;
    rx_reset_gt : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__2\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_sync_block";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__2\ is
  signal data_tmp : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
  attribute DEF_VAL : string;
  attribute DEF_VAL of xpm_cdc_async_rst_inst : label is "1'b1";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of xpm_cdc_async_rst_inst : label is 5;
  attribute INIT : string;
  attribute INIT of xpm_cdc_async_rst_inst : label is "1";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of xpm_cdc_async_rst_inst : label is 1;
  attribute VERSION : integer;
  attribute VERSION of xpm_cdc_async_rst_inst : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of xpm_cdc_async_rst_inst : label is "ASYNC_RST";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of xpm_cdc_async_rst_inst : label is "TRUE";
begin
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => data_out,
      R => '0'
    );
xpm_cdc_async_rst_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__5\
     port map (
      dest_arst => data_tmp,
      dest_clk => drpclk,
      src_arst => rx_reset_gt
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__3\ is
  port (
    gt_txreset0 : out STD_LOGIC;
    data_out : in STD_LOGIC;
    GT_TX_FSM_RESET_DONE_OUT : in STD_LOGIC;
    data_in : in STD_LOGIC;
    drpclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__3\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_sync_block";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__3\ is
  signal data_tmp : STD_LOGIC;
  signal tx_rst_all_sync : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg_gsr : label is std.standard.true;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of data_sync_reg_gsr : label is "PRIMITIVE";
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg_gsr : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg_gsr : label is "FD";
  attribute DEF_VAL : string;
  attribute DEF_VAL of xpm_cdc_async_rst_inst : label is "1'b1";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of xpm_cdc_async_rst_inst : label is 5;
  attribute INIT : string;
  attribute INIT of xpm_cdc_async_rst_inst : label is "1";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of xpm_cdc_async_rst_inst : label is 1;
  attribute VERSION : integer;
  attribute VERSION of xpm_cdc_async_rst_inst : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of xpm_cdc_async_rst_inst : label is "ASYNC_RST";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of xpm_cdc_async_rst_inst : label is "TRUE";
begin
data_sync_reg_gsr: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => drpclk,
      CE => '1',
      D => data_tmp,
      Q => tx_rst_all_sync,
      R => '0'
    );
gt_txreset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => tx_rst_all_sync,
      I1 => data_out,
      I2 => GT_TX_FSM_RESET_DONE_OUT,
      O => gt_txreset0
    );
xpm_cdc_async_rst_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xpm_cdc_async_rst__6\
     port map (
      dest_arst => data_tmp,
      dest_clk => drpclk,
      src_arst => data_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init is
  port (
    SYSCLK_IN : in STD_LOGIC;
    SOFT_RESET_TX_IN : in STD_LOGIC;
    SOFT_RESET_RX_IN : in STD_LOGIC;
    DONT_RESET_ON_DATA_ERROR_IN : in STD_LOGIC;
    GT_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;
    GT_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;
    GT0_DATA_VALID_IN : in STD_LOGIC;
    gt0_cpllfbclklost_out : out STD_LOGIC;
    gt0_cplllock_out : out STD_LOGIC;
    gt0_cplllockdetclk_in : in STD_LOGIC;
    gt0_cpllpd_in : in STD_LOGIC;
    gt0_cpllreset_in : in STD_LOGIC;
    gt0_gtnorthrefclk0_in : in STD_LOGIC;
    gt0_gtnorthrefclk1_in : in STD_LOGIC;
    gt0_gtrefclk0_in : in STD_LOGIC;
    gt0_gtrefclk1_in : in STD_LOGIC;
    gt0_gtsouthrefclk0_in : in STD_LOGIC;
    gt0_gtsouthrefclk1_in : in STD_LOGIC;
    gt0_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    gt0_drpclk_in : in STD_LOGIC;
    gt0_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_drpen_in : in STD_LOGIC;
    gt0_drprdy_out : out STD_LOGIC;
    gt0_drpwe_in : in STD_LOGIC;
    gt0_rxsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_loopback_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_eyescanreset_in : in STD_LOGIC;
    gt0_rxuserrdy_in : in STD_LOGIC;
    gt0_eyescandataerror_out : out STD_LOGIC;
    gt0_eyescantrigger_in : in STD_LOGIC;
    gt0_rxcdrhold_in : in STD_LOGIC;
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );
    gt0_rxusrclk_in : in STD_LOGIC;
    gt0_rxusrclk2_in : in STD_LOGIC;
    gt0_rxdata_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_rxprbserr_out : out STD_LOGIC;
    gt0_rxprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxprbscntreset_in : in STD_LOGIC;
    gt0_rxdisperr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxnotintable_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_gthrxn_in : in STD_LOGIC;
    gt0_rxbufreset_in : in STD_LOGIC;
    gt0_rxbufstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxbyteisaligned_out : out STD_LOGIC;
    gt0_rxbyterealign_out : out STD_LOGIC;
    gt0_rxcommadet_out : out STD_LOGIC;
    gt0_rxmcommaalignen_in : in STD_LOGIC;
    gt0_rxpcommaalignen_in : in STD_LOGIC;
    gt0_rxdfelpmreset_in : in STD_LOGIC;
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxoutclk_out : out STD_LOGIC;
    gt0_rxoutclkfabric_out : out STD_LOGIC;
    gt0_gtrxreset_in : in STD_LOGIC;
    gt0_rxpcsreset_in : in STD_LOGIC;
    gt0_rxpmareset_in : in STD_LOGIC;
    gt0_rxlpmen_in : in STD_LOGIC;
    gt0_rxpolarity_in : in STD_LOGIC;
    gt0_rxchariscomma_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxcharisk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_gthrxp_in : in STD_LOGIC;
    gt0_rxresetdone_out : out STD_LOGIC;
    gt0_txpostcursor_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_txprecursor_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_gttxreset_in : in STD_LOGIC;
    gt0_txuserrdy_in : in STD_LOGIC;
    gt0_txusrclk_in : in STD_LOGIC;
    gt0_txusrclk2_in : in STD_LOGIC;
    gt0_txprbsforceerr_in : in STD_LOGIC;
    gt0_txbufstatus_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txdiffctrl_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_txinhibit_in : in STD_LOGIC;
    gt0_txdata_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_gthtxn_out : out STD_LOGIC;
    gt0_gthtxp_out : out STD_LOGIC;
    gt0_txoutclk_out : out STD_LOGIC;
    gt0_txoutclkfabric_out : out STD_LOGIC;
    gt0_txoutclkpcs_out : out STD_LOGIC;
    gt0_txpcsreset_in : in STD_LOGIC;
    gt0_txpmareset_in : in STD_LOGIC;
    gt0_txresetdone_out : out STD_LOGIC;
    gt0_txpolarity_in : in STD_LOGIC;
    gt0_txprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txcharisk_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    GT0_QPLLOUTCLK_IN : in STD_LOGIC;
    GT0_QPLLOUTREFCLK_IN : in STD_LOGIC
  );
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init : entity is "yes";
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init : entity is 0;
  attribute EXAMPLE_SIM_GTRESET_SPEEDUP : string;
  attribute EXAMPLE_SIM_GTRESET_SPEEDUP of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init : entity is "TRUE";
  attribute EXAMPLE_USE_CHIPSCOPE : integer;
  attribute EXAMPLE_USE_CHIPSCOPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init : entity is 1;
  attribute STABLE_CLOCK_PERIOD : integer;
  attribute STABLE_CLOCK_PERIOD of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init : entity is 10;
  attribute USE_BUFG : integer;
  attribute USE_BUFG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init : entity is 0;
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init is
  signal RXRESETDONE : STD_LOGIC;
  signal TXRESETDONE : STD_LOGIC;
  signal \^gt0_cplllock_out\ : STD_LOGIC;
  signal gt0_gtrxreset_in2_out : STD_LOGIC;
  signal gt0_gttxreset_in1_out : STD_LOGIC;
  signal gt0_rx_cdrlock_counter : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[10]_i_3_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[10]_i_4_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal gt0_rx_cdrlocked_i_2_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_i_3_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_i_4_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_reg_n_0 : STD_LOGIC;
  signal gt0_rxuserrdy_in3_out : STD_LOGIC;
  signal gt0_txuserrdy_in0_out : STD_LOGIC;
  signal gt_rxresetfsm_i_n_4 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal p_4_in : STD_LOGIC;
  signal p_5_in : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[0]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[1]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[2]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[3]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[4]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[6]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[7]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[8]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \gt0_rx_cdrlock_counter[9]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of gt0_rx_cdrlocked_i_2 : label is "soft_lutpair24";
begin
  gt0_cplllock_out <= \^gt0_cplllock_out\;
Jesd204_microblaze_jesd204_phy_0_0_gt_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_multi_gt
     port map (
      GT0_QPLLOUTCLK_IN => GT0_QPLLOUTCLK_IN,
      GT0_QPLLOUTREFCLK_IN => GT0_QPLLOUTREFCLK_IN,
      SR(0) => gt0_gtrxreset_in2_out,
      data_in => TXRESETDONE,
      data_sync_reg1 => RXRESETDONE,
      gt0_cpllfbclklost_out => gt0_cpllfbclklost_out,
      gt0_cplllock_out => \^gt0_cplllock_out\,
      gt0_cplllockdetclk_in => gt0_cplllockdetclk_in,
      gt0_cpllpd_in => gt0_cpllpd_in,
      gt0_cpllreset_in => gt0_cpllreset_in,
      gt0_dmonitorout_out(14 downto 0) => gt0_dmonitorout_out(14 downto 0),
      gt0_drpaddr_in(8 downto 0) => gt0_drpaddr_in(8 downto 0),
      gt0_drpclk_in => gt0_drpclk_in,
      gt0_drpdi_in(15 downto 0) => gt0_drpdi_in(15 downto 0),
      gt0_drpdo_out(15 downto 0) => gt0_drpdo_out(15 downto 0),
      gt0_drpen_in => gt0_drpen_in,
      gt0_drprdy_out => gt0_drprdy_out,
      gt0_drpwe_in => gt0_drpwe_in,
      gt0_eyescandataerror_out => gt0_eyescandataerror_out,
      gt0_eyescanreset_in => gt0_eyescanreset_in,
      gt0_eyescantrigger_in => gt0_eyescantrigger_in,
      gt0_gthrxn_in => gt0_gthrxn_in,
      gt0_gthrxp_in => gt0_gthrxp_in,
      gt0_gthtxn_out => gt0_gthtxn_out,
      gt0_gthtxp_out => gt0_gthtxp_out,
      gt0_gtnorthrefclk0_in => gt0_gtnorthrefclk0_in,
      gt0_gtnorthrefclk1_in => gt0_gtnorthrefclk1_in,
      gt0_gtrefclk0_in => gt0_gtrefclk0_in,
      gt0_gtrefclk1_in => gt0_gtrefclk1_in,
      gt0_gtsouthrefclk0_in => gt0_gtsouthrefclk0_in,
      gt0_gtsouthrefclk1_in => gt0_gtsouthrefclk1_in,
      gt0_gttxreset_in1_out => gt0_gttxreset_in1_out,
      gt0_loopback_in(2 downto 0) => gt0_loopback_in(2 downto 0),
      gt0_rxbufreset_in => gt0_rxbufreset_in,
      gt0_rxbufstatus_out(2 downto 0) => gt0_rxbufstatus_out(2 downto 0),
      gt0_rxbyteisaligned_out => gt0_rxbyteisaligned_out,
      gt0_rxbyterealign_out => gt0_rxbyterealign_out,
      gt0_rxcdrhold_in => gt0_rxcdrhold_in,
      gt0_rxchariscomma_out(3 downto 0) => gt0_rxchariscomma_out(3 downto 0),
      gt0_rxcharisk_out(3 downto 0) => gt0_rxcharisk_out(3 downto 0),
      gt0_rxcommadet_out => gt0_rxcommadet_out,
      gt0_rxdata_out(31 downto 0) => gt0_rxdata_out(31 downto 0),
      gt0_rxdfelpmreset_in => gt0_rxdfelpmreset_in,
      gt0_rxdisperr_out(3 downto 0) => gt0_rxdisperr_out(3 downto 0),
      gt0_rxlpmen_in => gt0_rxlpmen_in,
      gt0_rxmcommaalignen_in => gt0_rxmcommaalignen_in,
      gt0_rxmonitorout_out(6 downto 0) => gt0_rxmonitorout_out(6 downto 0),
      gt0_rxmonitorsel_in(1 downto 0) => gt0_rxmonitorsel_in(1 downto 0),
      gt0_rxnotintable_out(3 downto 0) => gt0_rxnotintable_out(3 downto 0),
      gt0_rxoutclk_out => gt0_rxoutclk_out,
      gt0_rxoutclkfabric_out => gt0_rxoutclkfabric_out,
      gt0_rxpcommaalignen_in => gt0_rxpcommaalignen_in,
      gt0_rxpcsreset_in => gt0_rxpcsreset_in,
      gt0_rxpd_in(1 downto 0) => gt0_rxpd_in(1 downto 0),
      gt0_rxpmareset_in => gt0_rxpmareset_in,
      gt0_rxpolarity_in => gt0_rxpolarity_in,
      gt0_rxprbscntreset_in => gt0_rxprbscntreset_in,
      gt0_rxprbserr_out => gt0_rxprbserr_out,
      gt0_rxprbssel_in(2 downto 0) => gt0_rxprbssel_in(2 downto 0),
      gt0_rxresetdone_out => gt0_rxresetdone_out,
      gt0_rxstatus_out(2 downto 0) => gt0_rxstatus_out(2 downto 0),
      gt0_rxsysclksel_in(1 downto 0) => gt0_rxsysclksel_in(1 downto 0),
      gt0_rxuserrdy_in3_out => gt0_rxuserrdy_in3_out,
      gt0_rxusrclk2_in => gt0_rxusrclk2_in,
      gt0_rxusrclk_in => gt0_rxusrclk_in,
      gt0_txbufstatus_out(1 downto 0) => gt0_txbufstatus_out(1 downto 0),
      gt0_txcharisk_in(3 downto 0) => gt0_txcharisk_in(3 downto 0),
      gt0_txdata_in(31 downto 0) => gt0_txdata_in(31 downto 0),
      gt0_txdiffctrl_in(3 downto 0) => gt0_txdiffctrl_in(3 downto 0),
      gt0_txinhibit_in => gt0_txinhibit_in,
      gt0_txoutclk_out => gt0_txoutclk_out,
      gt0_txoutclkfabric_out => gt0_txoutclkfabric_out,
      gt0_txoutclkpcs_out => gt0_txoutclkpcs_out,
      gt0_txpcsreset_in => gt0_txpcsreset_in,
      gt0_txpd_in(1 downto 0) => gt0_txpd_in(1 downto 0),
      gt0_txpmareset_in => gt0_txpmareset_in,
      gt0_txpolarity_in => gt0_txpolarity_in,
      gt0_txpostcursor_in(4 downto 0) => gt0_txpostcursor_in(4 downto 0),
      gt0_txprbsforceerr_in => gt0_txprbsforceerr_in,
      gt0_txprbssel_in(2 downto 0) => gt0_txprbssel_in(2 downto 0),
      gt0_txprecursor_in(4 downto 0) => gt0_txprecursor_in(4 downto 0),
      gt0_txresetdone_out => gt0_txresetdone_out,
      gt0_txsysclksel_in(1 downto 0) => gt0_txsysclksel_in(1 downto 0),
      gt0_txuserrdy_in0_out => gt0_txuserrdy_in0_out,
      gt0_txusrclk2_in => gt0_txusrclk2_in,
      gt0_txusrclk_in => gt0_txusrclk_in,
      p_4_in => p_4_in,
      p_5_in => p_5_in
    );
\gt0_rx_cdrlock_counter[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(0),
      O => p_0_in(0)
    );
\gt0_rx_cdrlock_counter[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(3),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(4),
      I2 => \gt0_rx_cdrlock_counter[10]_i_3_n_0\,
      I3 => \gt0_rx_cdrlock_counter_reg__0\(2),
      I4 => \gt0_rx_cdrlock_counter_reg__0\(0),
      I5 => \gt0_rx_cdrlock_counter_reg__0\(1),
      O => gt0_rx_cdrlock_counter
    );
\gt0_rx_cdrlock_counter[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFFFFF08000000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(9),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(7),
      I2 => \gt0_rx_cdrlock_counter[10]_i_4_n_0\,
      I3 => \gt0_rx_cdrlock_counter_reg__0\(6),
      I4 => \gt0_rx_cdrlock_counter_reg__0\(8),
      I5 => \gt0_rx_cdrlock_counter_reg__0\(10),
      O => p_0_in(10)
    );
\gt0_rx_cdrlock_counter[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFF7F"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(9),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(10),
      I2 => \gt0_rx_cdrlock_counter_reg__0\(7),
      I3 => \gt0_rx_cdrlock_counter_reg__0\(8),
      I4 => \gt0_rx_cdrlock_counter_reg__0\(6),
      I5 => \gt0_rx_cdrlock_counter_reg__0\(5),
      O => \gt0_rx_cdrlock_counter[10]_i_3_n_0\
    );
\gt0_rx_cdrlock_counter[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(4),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(2),
      I2 => \gt0_rx_cdrlock_counter_reg__0\(0),
      I3 => \gt0_rx_cdrlock_counter_reg__0\(1),
      I4 => \gt0_rx_cdrlock_counter_reg__0\(3),
      I5 => \gt0_rx_cdrlock_counter_reg__0\(5),
      O => \gt0_rx_cdrlock_counter[10]_i_4_n_0\
    );
\gt0_rx_cdrlock_counter[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(1),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(0),
      O => p_0_in(1)
    );
\gt0_rx_cdrlock_counter[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(1),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(0),
      I2 => \gt0_rx_cdrlock_counter_reg__0\(2),
      O => p_0_in(2)
    );
\gt0_rx_cdrlock_counter[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(2),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(0),
      I2 => \gt0_rx_cdrlock_counter_reg__0\(1),
      I3 => \gt0_rx_cdrlock_counter_reg__0\(3),
      O => p_0_in(3)
    );
\gt0_rx_cdrlock_counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(3),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(1),
      I2 => \gt0_rx_cdrlock_counter_reg__0\(0),
      I3 => \gt0_rx_cdrlock_counter_reg__0\(2),
      I4 => \gt0_rx_cdrlock_counter_reg__0\(4),
      O => p_0_in(4)
    );
\gt0_rx_cdrlock_counter[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(4),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(2),
      I2 => \gt0_rx_cdrlock_counter_reg__0\(0),
      I3 => \gt0_rx_cdrlock_counter_reg__0\(1),
      I4 => \gt0_rx_cdrlock_counter_reg__0\(3),
      I5 => \gt0_rx_cdrlock_counter_reg__0\(5),
      O => p_0_in(5)
    );
\gt0_rx_cdrlock_counter[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[10]_i_4_n_0\,
      I1 => \gt0_rx_cdrlock_counter_reg__0\(6),
      O => p_0_in(6)
    );
\gt0_rx_cdrlock_counter[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(6),
      I1 => \gt0_rx_cdrlock_counter[10]_i_4_n_0\,
      I2 => \gt0_rx_cdrlock_counter_reg__0\(7),
      O => p_0_in(7)
    );
\gt0_rx_cdrlock_counter[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DF20"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(7),
      I1 => \gt0_rx_cdrlock_counter[10]_i_4_n_0\,
      I2 => \gt0_rx_cdrlock_counter_reg__0\(6),
      I3 => \gt0_rx_cdrlock_counter_reg__0\(8),
      O => p_0_in(8)
    );
\gt0_rx_cdrlock_counter[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F7FF0800"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(8),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(6),
      I2 => \gt0_rx_cdrlock_counter[10]_i_4_n_0\,
      I3 => \gt0_rx_cdrlock_counter_reg__0\(7),
      I4 => \gt0_rx_cdrlock_counter_reg__0\(9),
      O => p_0_in(9)
    );
\gt0_rx_cdrlock_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(0),
      Q => \gt0_rx_cdrlock_counter_reg__0\(0),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(10),
      Q => \gt0_rx_cdrlock_counter_reg__0\(10),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(1),
      Q => \gt0_rx_cdrlock_counter_reg__0\(1),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(2),
      Q => \gt0_rx_cdrlock_counter_reg__0\(2),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(3),
      Q => \gt0_rx_cdrlock_counter_reg__0\(3),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(4),
      Q => \gt0_rx_cdrlock_counter_reg__0\(4),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(5),
      Q => \gt0_rx_cdrlock_counter_reg__0\(5),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(6),
      Q => \gt0_rx_cdrlock_counter_reg__0\(6),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(7),
      Q => \gt0_rx_cdrlock_counter_reg__0\(7),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(8),
      Q => \gt0_rx_cdrlock_counter_reg__0\(8),
      R => gt0_gtrxreset_in2_out
    );
\gt0_rx_cdrlock_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => SYSCLK_IN,
      CE => gt0_rx_cdrlock_counter,
      D => p_0_in(9),
      Q => \gt0_rx_cdrlock_counter_reg__0\(9),
      R => gt0_gtrxreset_in2_out
    );
gt0_rx_cdrlocked_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(0),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(1),
      O => gt0_rx_cdrlocked_i_2_n_0
    );
gt0_rx_cdrlocked_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(3),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(4),
      O => gt0_rx_cdrlocked_i_3_n_0
    );
gt0_rx_cdrlocked_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter_reg__0\(7),
      I1 => \gt0_rx_cdrlock_counter_reg__0\(8),
      I2 => \gt0_rx_cdrlock_counter_reg__0\(5),
      I3 => \gt0_rx_cdrlock_counter_reg__0\(6),
      I4 => \gt0_rx_cdrlock_counter_reg__0\(10),
      I5 => \gt0_rx_cdrlock_counter_reg__0\(9),
      O => gt0_rx_cdrlocked_i_4_n_0
    );
gt0_rx_cdrlocked_reg: unisim.vcomponents.FDRE
     port map (
      C => SYSCLK_IN,
      CE => '1',
      D => gt_rxresetfsm_i_n_4,
      Q => gt0_rx_cdrlocked_reg_n_0,
      R => '0'
    );
gt_rxresetfsm_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_RX_STARTUP_FSM
     port map (
      DONT_RESET_ON_DATA_ERROR_IN => DONT_RESET_ON_DATA_ERROR_IN,
      GT0_DATA_VALID_IN => GT0_DATA_VALID_IN,
      GT_RX_FSM_RESET_DONE_OUT => GT_RX_FSM_RESET_DONE_OUT,
      Q(0) => \gt0_rx_cdrlock_counter_reg__0\(2),
      SOFT_RESET_RX_IN => SOFT_RESET_RX_IN,
      SR(0) => gt0_gtrxreset_in2_out,
      SYSCLK_IN => SYSCLK_IN,
      data_in => RXRESETDONE,
      gt0_cplllock_out => \^gt0_cplllock_out\,
      gt0_gtrxreset_in => gt0_gtrxreset_in,
      \gt0_rx_cdrlock_counter_reg[0]\ => gt0_rx_cdrlocked_i_2_n_0,
      \gt0_rx_cdrlock_counter_reg[3]\ => gt0_rx_cdrlocked_i_3_n_0,
      \gt0_rx_cdrlock_counter_reg[7]\ => gt0_rx_cdrlocked_i_4_n_0,
      gt0_rx_cdrlocked_reg => gt_rxresetfsm_i_n_4,
      gt0_rx_cdrlocked_reg_0 => gt0_rx_cdrlocked_reg_n_0,
      gt0_rxsysclksel_in(0) => gt0_rxsysclksel_in(0),
      gt0_rxuserrdy_in => gt0_rxuserrdy_in,
      gt0_rxuserrdy_in3_out => gt0_rxuserrdy_in3_out,
      gt0_rxusrclk_in => gt0_rxusrclk_in,
      gt0_txsysclksel_in(0) => gt0_txsysclksel_in(0),
      p_4_in => p_4_in
    );
gt_txresetfsm_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_TX_STARTUP_FSM
     port map (
      GT_TX_FSM_RESET_DONE_OUT => GT_TX_FSM_RESET_DONE_OUT,
      SOFT_RESET_TX_IN => SOFT_RESET_TX_IN,
      SYSCLK_IN => SYSCLK_IN,
      data_in => TXRESETDONE,
      gt0_cplllock_out => \^gt0_cplllock_out\,
      gt0_gttxreset_in => gt0_gttxreset_in,
      gt0_gttxreset_in1_out => gt0_gttxreset_in1_out,
      gt0_txsysclksel_in(0) => gt0_txsysclksel_in(0),
      gt0_txuserrdy_in => gt0_txuserrdy_in,
      gt0_txuserrdy_in0_out => gt0_txuserrdy_in0_out,
      gt0_txusrclk_in => gt0_txusrclk_in,
      p_5_in => p_5_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen is
  port (
    slv_wren_done_pulse : out STD_LOGIC;
    clk2_ready_reg_0 : out STD_LOGIC;
    txinihibit_0_reg : out STD_LOGIC;
    txpolarity_0_reg : out STD_LOGIC;
    slv_access_valid_hold_reg : out STD_LOGIC;
    \tx_pd_0_reg[1]\ : out STD_LOGIC;
    \tx_pd_0_reg[0]\ : out STD_LOGIC;
    p_0_in : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_core_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \slv_addr_reg[3]\ : in STD_LOGIC;
    slv_rden_r_reg : in STD_LOGIC;
    gt0_txinhibit_in : in STD_LOGIC;
    \slv_addr_reg[3]_0\ : in STD_LOGIC;
    gt0_txpolarity_in : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    chan_tx_axi_map_wready : in STD_LOGIC;
    chan_tx_slv_rden : in STD_LOGIC;
    data_in : in STD_LOGIC;
    \slv_addr_reg[5]\ : in STD_LOGIC;
    \slv_addr_reg[2]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_addr_reg[3]_1\ : in STD_LOGIC;
    gt0_txpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen is
  signal axi_2_drp_valid_i_n_0 : STD_LOGIC;
  signal axi_2_drp_valid_i_n_1 : STD_LOGIC;
  signal clk1_ready_pulse0 : STD_LOGIC;
  signal clk1_ready_sync_r : STD_LOGIC;
  signal clk2_ready : STD_LOGIC;
  signal \^clk2_ready_reg_0\ : STD_LOGIC;
  signal clk2_valid_pulse0 : STD_LOGIC;
  signal clk2_valid_sync_r : STD_LOGIC;
  signal slv_wren_clear : STD_LOGIC;
  signal \tx_pd_0[1]_i_2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \tx_pd_0[1]_i_2\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of txinihibit_0_i_1 : label is "soft_lutpair31";
begin
  clk2_ready_reg_0 <= \^clk2_ready_reg_0\;
axi_2_drp_valid_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__5\
     port map (
      clk2_ready_reg => axi_2_drp_valid_i_n_0,
      clk2_valid_pulse0 => clk2_valid_pulse0,
      clk2_valid_pulse_reg => \^clk2_ready_reg_0\,
      clk2_valid_sync_r => clk2_valid_sync_r,
      data_in => clk2_ready,
      data_out => axi_2_drp_valid_i_n_1,
      slv_access_valid_hold_reg => data_in,
      tx_core_clk => tx_core_clk
    );
clk1_ready_pulse_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => clk1_ready_pulse0,
      Q => slv_wren_done_pulse,
      R => p_0_in
    );
clk1_ready_sync_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => slv_wren_clear,
      Q => clk1_ready_sync_r,
      R => p_0_in
    );
clk2_ready_reg: unisim.vcomponents.FDRE
     port map (
      C => tx_core_clk,
      CE => '1',
      D => axi_2_drp_valid_i_n_0,
      Q => clk2_ready,
      R => SR(0)
    );
clk2_valid_pulse_reg: unisim.vcomponents.FDRE
     port map (
      C => tx_core_clk,
      CE => '1',
      D => clk2_valid_pulse0,
      Q => \^clk2_ready_reg_0\,
      R => SR(0)
    );
clk2_valid_sync_r_reg: unisim.vcomponents.FDRE
     port map (
      C => tx_core_clk,
      CE => '1',
      D => axi_2_drp_valid_i_n_1,
      Q => clk2_valid_sync_r,
      R => SR(0)
    );
drp_2_axi_in_progress_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__6\
     port map (
      chan_tx_axi_map_wready => chan_tx_axi_map_wready,
      chan_tx_slv_rden => chan_tx_slv_rden,
      clk1_ready_pulse0 => clk1_ready_pulse0,
      clk1_ready_sync_r => clk1_ready_sync_r,
      clk2_ready_reg => clk2_ready,
      data_in => data_in,
      data_out => slv_wren_clear,
      s_axi_aclk => s_axi_aclk,
      s_axi_wvalid => s_axi_wvalid,
      slv_access_valid_hold_reg => slv_access_valid_hold_reg,
      slv_rden_r_reg => slv_rden_r_reg
    );
\tx_pd_0[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => Q(0),
      I1 => \slv_addr_reg[5]\,
      I2 => \slv_addr_reg[2]\(0),
      I3 => \slv_addr_reg[3]_1\,
      I4 => \tx_pd_0[1]_i_2_n_0\,
      I5 => gt0_txpd_in(0),
      O => \tx_pd_0_reg[0]\
    );
\tx_pd_0[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => Q(1),
      I1 => \slv_addr_reg[5]\,
      I2 => \slv_addr_reg[2]\(0),
      I3 => \slv_addr_reg[3]_1\,
      I4 => \tx_pd_0[1]_i_2_n_0\,
      I5 => gt0_txpd_in(1),
      O => \tx_pd_0_reg[1]\
    );
\tx_pd_0[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => slv_rden_r_reg,
      I1 => \^clk2_ready_reg_0\,
      O => \tx_pd_0[1]_i_2_n_0\
    );
txinihibit_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => Q(0),
      I1 => \slv_addr_reg[3]\,
      I2 => \^clk2_ready_reg_0\,
      I3 => slv_rden_r_reg,
      I4 => gt0_txinhibit_in,
      O => txinihibit_0_reg
    );
txpolarity_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => Q(0),
      I1 => \slv_addr_reg[3]_0\,
      I2 => \^clk2_ready_reg_0\,
      I3 => slv_rden_r_reg,
      I4 => gt0_txpolarity_in,
      O => txpolarity_0_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__1\ is
  port (
    drp_access_in_progress_reg : out STD_LOGIC;
    access_type_reg : out STD_LOGIC;
    drp_access_valid_reg : out STD_LOGIC;
    gt0_drpwe_in : out STD_LOGIC;
    gt0_drpen_in : out STD_LOGIC;
    \axi_bresp_reg[1]\ : out STD_LOGIC;
    p_0_in : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    drpclk : in STD_LOGIC;
    gt0_drprdy_out : in STD_LOGIC;
    drp_if_select : in STD_LOGIC;
    data_out : in STD_LOGIC;
    drp_access_in_progress_reg_0 : in STD_LOGIC;
    access_type5_out : in STD_LOGIC;
    \s_axi_wdata[30]\ : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    gt_slv_wren : in STD_LOGIC;
    access_type : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_in : in STD_LOGIC;
    wait_for_drp : in STD_LOGIC;
    wr_req_reg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__1\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__1\ is
  signal axi_2_drp_valid_i_n_0 : STD_LOGIC;
  signal axi_2_drp_valid_i_n_1 : STD_LOGIC;
  signal axi_2_drp_valid_i_n_2 : STD_LOGIC;
  signal clk1_ready_pulse : STD_LOGIC;
  signal clk1_ready_pulse0 : STD_LOGIC;
  signal clk1_ready_sync_r : STD_LOGIC;
  signal clk2_ready : STD_LOGIC;
  signal clk2_valid_pulse : STD_LOGIC;
  signal clk2_valid_sync_r : STD_LOGIC;
  signal selected_rdy_axi : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of Jesd204_microblaze_jesd204_phy_0_0_gt_i_1 : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of Jesd204_microblaze_jesd204_phy_0_0_gt_i_2 : label is "soft_lutpair27";
begin
Jesd204_microblaze_jesd204_phy_0_0_gt_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk2_valid_pulse,
      I1 => drp_if_select,
      O => gt0_drpen_in
    );
Jesd204_microblaze_jesd204_phy_0_0_gt_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => access_type,
      I1 => drp_if_select,
      I2 => clk2_valid_pulse,
      O => gt0_drpwe_in
    );
access_type_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E200E2000000E200"
    )
        port map (
      I0 => access_type,
      I1 => access_type5_out,
      I2 => s_axi_wdata(0),
      I3 => s_axi_aresetn,
      I4 => clk1_ready_pulse,
      I5 => gt_slv_wren,
      O => access_type_reg
    );
axi_2_drp_valid_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__1\
     port map (
      clk2_ready_reg => axi_2_drp_valid_i_n_0,
      clk2_valid_pulse_reg => axi_2_drp_valid_i_n_1,
      clk2_valid_sync_r => clk2_valid_sync_r,
      clk2_valid_sync_r_reg => axi_2_drp_valid_i_n_2,
      data_in => clk2_ready,
      data_out => data_out,
      drp_access_valid_reg => data_in,
      drp_if_select => drp_if_select,
      drpclk => drpclk,
      gt0_drprdy_out => gt0_drprdy_out
    );
\axi_bresp[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => clk1_ready_pulse,
      I1 => drp_access_in_progress_reg_0,
      I2 => wait_for_drp,
      I3 => wr_req_reg,
      O => \axi_bresp_reg[1]\
    );
clk1_ready_pulse_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => clk1_ready_pulse0,
      Q => clk1_ready_pulse,
      R => p_0_in
    );
clk1_ready_sync_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => selected_rdy_axi,
      Q => clk1_ready_sync_r,
      R => p_0_in
    );
clk2_ready_reg: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => '1',
      D => axi_2_drp_valid_i_n_0,
      Q => clk2_ready,
      R => '0'
    );
clk2_valid_pulse_reg: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => '1',
      D => axi_2_drp_valid_i_n_1,
      Q => clk2_valid_pulse,
      R => '0'
    );
clk2_valid_sync_r_reg: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => '1',
      D => axi_2_drp_valid_i_n_2,
      Q => clk2_valid_sync_r,
      R => '0'
    );
drp_2_axi_in_progress_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__2\
     port map (
      access_type5_out => access_type5_out,
      clk1_ready_pulse0 => clk1_ready_pulse0,
      clk1_ready_sync_r => clk1_ready_sync_r,
      clk2_ready_reg => clk2_ready,
      data_in => data_in,
      data_out => selected_rdy_axi,
      drp_access_valid_reg => drp_access_valid_reg,
      gt_slv_wren => gt_slv_wren,
      s_axi_aclk => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      \s_axi_wdata[30]\ => \s_axi_wdata[30]\
    );
drp_access_in_progress_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E200E2000000E200"
    )
        port map (
      I0 => drp_access_in_progress_reg_0,
      I1 => access_type5_out,
      I2 => \s_axi_wdata[30]\,
      I3 => s_axi_aresetn,
      I4 => clk1_ready_pulse,
      I5 => gt_slv_wren,
      O => drp_access_in_progress_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__2\ is
  port (
    slv_wren_done_pulse : out STD_LOGIC;
    rxpolarity_0_reg : out STD_LOGIC;
    rxlpmen_reg : out STD_LOGIC;
    rxdfelpmreset_reg : out STD_LOGIC;
    slv_access_valid_hold_reg : out STD_LOGIC;
    \axi_bresp_reg[1]\ : out STD_LOGIC;
    p_0_in : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    data_sync_reg_gsr : in STD_LOGIC;
    rx_core_clk : in STD_LOGIC;
    \slv_wdata_r_internal_reg[0]\ : in STD_LOGIC;
    slv_rden_r_reg : in STD_LOGIC;
    \slv_addr_reg[3]\ : in STD_LOGIC;
    \gt_interface_sel_reg[2]\ : in STD_LOGIC;
    gt0_rxpolarity_in : in STD_LOGIC;
    \slv_addr_reg[3]_0\ : in STD_LOGIC;
    gt0_rxlpmen_in : in STD_LOGIC;
    \slv_addr_reg[3]_1\ : in STD_LOGIC;
    gt0_rxdfelpmreset_in : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    chan_rx_axi_map_wready : in STD_LOGIC;
    chan_rx_slv_rden : in STD_LOGIC;
    data_in : in STD_LOGIC;
    slv_rden_r_0 : in STD_LOGIC;
    slv_wren_done_pulse_1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__2\ : entity is "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__2\ is
  signal axi_2_drp_valid_i_n_0 : STD_LOGIC;
  signal axi_2_drp_valid_i_n_1 : STD_LOGIC;
  signal clk1_ready_pulse0 : STD_LOGIC;
  signal clk1_ready_sync_r : STD_LOGIC;
  signal clk2_ready : STD_LOGIC;
  signal clk2_valid_pulse0 : STD_LOGIC;
  signal clk2_valid_sync_r : STD_LOGIC;
  signal slv_wren_clear : STD_LOGIC;
  signal slv_wren_clk2 : STD_LOGIC;
  signal \^slv_wren_done_pulse\ : STD_LOGIC;
begin
  slv_wren_done_pulse <= \^slv_wren_done_pulse\;
axi_2_drp_valid_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__3\
     port map (
      clk2_ready_reg => axi_2_drp_valid_i_n_0,
      clk2_valid_pulse0 => clk2_valid_pulse0,
      clk2_valid_sync_r => clk2_valid_sync_r,
      data_in => clk2_ready,
      data_out => axi_2_drp_valid_i_n_1,
      rx_core_clk => rx_core_clk,
      slv_access_valid_hold_reg => data_in,
      slv_wren_clk2 => slv_wren_clk2
    );
\axi_bresp[1]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => slv_rden_r_reg,
      I1 => \^slv_wren_done_pulse\,
      I2 => slv_rden_r_0,
      I3 => slv_wren_done_pulse_1,
      O => \axi_bresp_reg[1]\
    );
clk1_ready_pulse_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => clk1_ready_pulse0,
      Q => \^slv_wren_done_pulse\,
      R => p_0_in
    );
clk1_ready_sync_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => slv_wren_clear,
      Q => clk1_ready_sync_r,
      R => p_0_in
    );
clk2_ready_reg: unisim.vcomponents.FDRE
     port map (
      C => rx_core_clk,
      CE => '1',
      D => axi_2_drp_valid_i_n_0,
      Q => clk2_ready,
      R => data_sync_reg_gsr
    );
clk2_valid_pulse_reg: unisim.vcomponents.FDRE
     port map (
      C => rx_core_clk,
      CE => '1',
      D => clk2_valid_pulse0,
      Q => slv_wren_clk2,
      R => data_sync_reg_gsr
    );
clk2_valid_sync_r_reg: unisim.vcomponents.FDRE
     port map (
      C => rx_core_clk,
      CE => '1',
      D => axi_2_drp_valid_i_n_1,
      Q => clk2_valid_sync_r,
      R => data_sync_reg_gsr
    );
drp_2_axi_in_progress_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__4\
     port map (
      chan_rx_axi_map_wready => chan_rx_axi_map_wready,
      chan_rx_slv_rden => chan_rx_slv_rden,
      clk1_ready_pulse0 => clk1_ready_pulse0,
      clk1_ready_sync_r => clk1_ready_sync_r,
      clk2_ready_reg => clk2_ready,
      data_in => data_in,
      data_out => slv_wren_clear,
      s_axi_aclk => s_axi_aclk,
      s_axi_wvalid => s_axi_wvalid,
      slv_access_valid_hold_reg => slv_access_valid_hold_reg,
      slv_rden_r_reg => slv_rden_r_reg
    );
rxdfelpmreset_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => \slv_wdata_r_internal_reg[0]\,
      I1 => \slv_addr_reg[3]_1\,
      I2 => slv_wren_clk2,
      I3 => slv_rden_r_reg,
      I4 => gt0_rxdfelpmreset_in,
      O => rxdfelpmreset_reg
    );
rxlpmen_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => \slv_wdata_r_internal_reg[0]\,
      I1 => \slv_addr_reg[3]_0\,
      I2 => slv_wren_clk2,
      I3 => slv_rden_r_reg,
      I4 => gt0_rxlpmen_in,
      O => rxlpmen_reg
    );
rxpolarity_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFB00000008"
    )
        port map (
      I0 => \slv_wdata_r_internal_reg[0]\,
      I1 => slv_wren_clk2,
      I2 => slv_rden_r_reg,
      I3 => \slv_addr_reg[3]\,
      I4 => \gt_interface_sel_reg[2]\,
      I5 => gt0_rxpolarity_in,
      O => rxpolarity_0_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox is
  port (
    gt_slv_rd_done : out STD_LOGIC;
    drp_reset : out STD_LOGIC;
    wait_for_drp : out STD_LOGIC;
    \axi_rdata_reg[11]\ : out STD_LOGIC;
    data_sync_reg1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \axi_rdata_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[10]\ : out STD_LOGIC;
    \axi_rdata_reg[9]\ : out STD_LOGIC;
    \axi_rdata_reg[2]\ : out STD_LOGIC;
    gt0_drpwe_in : out STD_LOGIC;
    gt0_drpen_in : out STD_LOGIC;
    \axi_bresp_reg[1]\ : out STD_LOGIC;
    \axi_rdata_reg[8]\ : out STD_LOGIC;
    data_sync_reg1_0 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    \axi_rdata_reg[7]\ : out STD_LOGIC;
    \axi_rdata_reg[6]\ : out STD_LOGIC;
    \axi_rdata_reg[5]\ : out STD_LOGIC;
    \axi_rdata_reg[4]\ : out STD_LOGIC;
    \axi_rdata_reg[3]\ : out STD_LOGIC;
    \axi_rdata_reg[2]_0\ : out STD_LOGIC;
    \axi_rdata_reg[1]\ : out STD_LOGIC;
    \axi_rdata_reg[0]\ : out STD_LOGIC;
    \axi_rdata_reg[0]_0\ : out STD_LOGIC;
    \axi_rdata_reg[8]_0\ : out STD_LOGIC_VECTOR ( 6 downto 0 );
    p_0_in : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt_slv_rden : in STD_LOGIC;
    gt_slv_wren : in STD_LOGIC;
    drpclk : in STD_LOGIC;
    drp_reset_reg_0 : in STD_LOGIC;
    gt_axi_map_wready_reg : in STD_LOGIC;
    gt0_drprdy_out : in STD_LOGIC;
    data_out : in STD_LOGIC;
    access_type5_out : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 17 downto 0 );
    \slv_addr_reg[4]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt_axi_map_wready_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_sync_reg_gsr : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt_axi_map_wready_reg_1 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox is
  signal access_type : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_0 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_1 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_2 : STD_LOGIC;
  signal \^data_sync_reg1\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^data_sync_reg1_0\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal drp_access_in_progress_i_3_n_0 : STD_LOGIC;
  signal drp_access_in_progress_reg_n_0 : STD_LOGIC;
  signal drp_access_valid_reg_n_0 : STD_LOGIC;
  signal drp_if_select : STD_LOGIC;
  signal drp_read_data : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal drp_read_data0 : STD_LOGIC;
  signal \^drp_reset\ : STD_LOGIC;
  signal \timeout_length_reg_n_0_[0]\ : STD_LOGIC;
  signal \timeout_length_reg_n_0_[10]\ : STD_LOGIC;
  signal \timeout_length_reg_n_0_[11]\ : STD_LOGIC;
  signal \timeout_length_reg_n_0_[2]\ : STD_LOGIC;
  signal \timeout_length_reg_n_0_[9]\ : STD_LOGIC;
  signal \^wait_for_drp\ : STD_LOGIC;
  signal wr_req_reg : STD_LOGIC;
begin
  data_sync_reg1(15 downto 0) <= \^data_sync_reg1\(15 downto 0);
  data_sync_reg1_0(8 downto 0) <= \^data_sync_reg1_0\(8 downto 0);
  drp_reset <= \^drp_reset\;
  wait_for_drp <= \^wait_for_drp\;
access_type_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_1,
      Q => access_type,
      R => '0'
    );
\axi_rdata[0]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^wait_for_drp\,
      I1 => \timeout_length_reg_n_0_[0]\,
      I2 => \slv_addr_reg[4]\(1),
      I3 => drp_access_in_progress_reg_n_0,
      I4 => \slv_addr_reg[4]\(0),
      I5 => \^drp_reset\,
      O => \axi_rdata_reg[0]_0\
    );
\axi_rdata[0]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => drp_read_data(0),
      I1 => \^data_sync_reg1\(0),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \^data_sync_reg1_0\(0),
      I4 => \slv_addr_reg[4]\(0),
      I5 => drp_if_select,
      O => \axi_rdata_reg[0]\
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000F000CC00AA00"
    )
        port map (
      I0 => \^data_sync_reg1\(10),
      I1 => drp_read_data(10),
      I2 => \timeout_length_reg_n_0_[10]\,
      I3 => \slv_addr_reg[4]\(1),
      I4 => \slv_addr_reg[4]\(0),
      I5 => \slv_addr_reg[4]\(2),
      O => \axi_rdata_reg[10]\
    );
\axi_rdata[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF0F3F5FFFFF3F5F"
    )
        port map (
      I0 => \^data_sync_reg1\(11),
      I1 => drp_read_data(11),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \slv_addr_reg[4]\(0),
      I4 => \slv_addr_reg[4]\(2),
      I5 => \timeout_length_reg_n_0_[11]\,
      O => \axi_rdata_reg[11]\
    );
\axi_rdata[1]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F8C83808"
    )
        port map (
      I0 => \^data_sync_reg1_0\(1),
      I1 => \slv_addr_reg[4]\(0),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \^data_sync_reg1\(1),
      I4 => drp_read_data(1),
      I5 => \slv_addr_reg[4]\(2),
      O => \axi_rdata_reg[1]\
    );
\axi_rdata[2]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3808"
    )
        port map (
      I0 => \timeout_length_reg_n_0_[2]\,
      I1 => \slv_addr_reg[4]\(1),
      I2 => \slv_addr_reg[4]\(0),
      I3 => access_type,
      O => \axi_rdata_reg[2]\
    );
\axi_rdata[2]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => drp_read_data(2),
      I1 => \^data_sync_reg1\(2),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \slv_addr_reg[4]\(0),
      I4 => \^data_sync_reg1_0\(2),
      O => \axi_rdata_reg[2]_0\
    );
\axi_rdata[3]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F8C83808"
    )
        port map (
      I0 => \^data_sync_reg1_0\(3),
      I1 => \slv_addr_reg[4]\(0),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \^data_sync_reg1\(3),
      I4 => drp_read_data(3),
      I5 => \slv_addr_reg[4]\(2),
      O => \axi_rdata_reg[3]\
    );
\axi_rdata[4]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F8C83808"
    )
        port map (
      I0 => \^data_sync_reg1_0\(4),
      I1 => \slv_addr_reg[4]\(0),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \^data_sync_reg1\(4),
      I4 => drp_read_data(4),
      I5 => \slv_addr_reg[4]\(2),
      O => \axi_rdata_reg[4]\
    );
\axi_rdata[5]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => drp_read_data(5),
      I1 => \^data_sync_reg1\(5),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \slv_addr_reg[4]\(0),
      I4 => \^data_sync_reg1_0\(5),
      O => \axi_rdata_reg[5]\
    );
\axi_rdata[6]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => drp_read_data(6),
      I1 => \^data_sync_reg1\(6),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \slv_addr_reg[4]\(0),
      I4 => \^data_sync_reg1_0\(6),
      O => \axi_rdata_reg[6]\
    );
\axi_rdata[7]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => drp_read_data(7),
      I1 => \^data_sync_reg1\(7),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \slv_addr_reg[4]\(0),
      I4 => \^data_sync_reg1_0\(7),
      O => \axi_rdata_reg[7]\
    );
\axi_rdata[8]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => drp_read_data(8),
      I1 => \^data_sync_reg1\(8),
      I2 => \slv_addr_reg[4]\(1),
      I3 => \slv_addr_reg[4]\(0),
      I4 => \^data_sync_reg1_0\(8),
      O => \axi_rdata_reg[8]\
    );
\axi_rdata[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF77FF77333FFF3F"
    )
        port map (
      I0 => \timeout_length_reg_n_0_[9]\,
      I1 => \slv_addr_reg[4]\(1),
      I2 => \^data_sync_reg1\(9),
      I3 => \slv_addr_reg[4]\(0),
      I4 => drp_read_data(9),
      I5 => \slv_addr_reg[4]\(2),
      O => \axi_rdata_reg[9]\
    );
clk2clk_handshake_pulse_gen_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__1\
     port map (
      access_type => access_type,
      access_type5_out => access_type5_out,
      access_type_reg => clk2clk_handshake_pulse_gen_i_n_1,
      \axi_bresp_reg[1]\ => \axi_bresp_reg[1]\,
      data_in => drp_access_valid_reg_n_0,
      data_out => data_out,
      drp_access_in_progress_reg => clk2clk_handshake_pulse_gen_i_n_0,
      drp_access_in_progress_reg_0 => drp_access_in_progress_reg_n_0,
      drp_access_valid_reg => clk2clk_handshake_pulse_gen_i_n_2,
      drp_if_select => drp_if_select,
      drpclk => drpclk,
      gt0_drpen_in => gt0_drpen_in,
      gt0_drprdy_out => gt0_drprdy_out,
      gt0_drpwe_in => gt0_drpwe_in,
      gt_slv_wren => gt_slv_wren,
      p_0_in => p_0_in,
      s_axi_aclk => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      s_axi_wdata(0) => s_axi_wdata(17),
      \s_axi_wdata[30]\ => drp_access_in_progress_i_3_n_0,
      wait_for_drp => \^wait_for_drp\,
      wr_req_reg => wr_req_reg
    );
drp_access_in_progress_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axi_wdata(17),
      I1 => s_axi_wdata(16),
      O => drp_access_in_progress_i_3_n_0
    );
drp_access_in_progress_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_0,
      Q => drp_access_in_progress_reg_n_0,
      R => '0'
    );
drp_access_valid_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_2,
      Q => drp_access_valid_reg_n_0,
      R => '0'
    );
\drp_if_select_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => Q(0),
      Q => drp_if_select,
      R => p_0_in
    );
\drp_int_addr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(0),
      Q => \^data_sync_reg1_0\(0),
      R => p_0_in
    );
\drp_int_addr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(1),
      Q => \^data_sync_reg1_0\(1),
      R => p_0_in
    );
\drp_int_addr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(2),
      Q => \^data_sync_reg1_0\(2),
      R => p_0_in
    );
\drp_int_addr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(3),
      Q => \^data_sync_reg1_0\(3),
      R => p_0_in
    );
\drp_int_addr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(4),
      Q => \^data_sync_reg1_0\(4),
      R => p_0_in
    );
\drp_int_addr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(5),
      Q => \^data_sync_reg1_0\(5),
      R => p_0_in
    );
\drp_int_addr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(6),
      Q => \^data_sync_reg1_0\(6),
      R => p_0_in
    );
\drp_int_addr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(7),
      Q => \^data_sync_reg1_0\(7),
      R => p_0_in
    );
\drp_int_addr_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(8),
      Q => \^data_sync_reg1_0\(8),
      R => p_0_in
    );
\drp_read_data[15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => drp_if_select,
      I1 => gt0_drprdy_out,
      I2 => access_type,
      O => drp_read_data0
    );
\drp_read_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(0),
      Q => drp_read_data(0),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(10),
      Q => drp_read_data(10),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(11),
      Q => drp_read_data(11),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(12),
      Q => \axi_rdata_reg[15]\(0),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(13),
      Q => \axi_rdata_reg[15]\(1),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(14),
      Q => \axi_rdata_reg[15]\(2),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(15),
      Q => \axi_rdata_reg[15]\(3),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(1),
      Q => drp_read_data(1),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(2),
      Q => drp_read_data(2),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(3),
      Q => drp_read_data(3),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(4),
      Q => drp_read_data(4),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(5),
      Q => drp_read_data(5),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(6),
      Q => drp_read_data(6),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(7),
      Q => drp_read_data(7),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(8),
      Q => drp_read_data(8),
      R => data_sync_reg_gsr(0)
    );
\drp_read_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => drp_read_data0,
      D => D(9),
      Q => drp_read_data(9),
      R => data_sync_reg_gsr(0)
    );
drp_reset_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => drp_reset_reg_0,
      Q => \^drp_reset\,
      R => '0'
    );
\drp_write_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(0),
      Q => \^data_sync_reg1\(0),
      R => p_0_in
    );
\drp_write_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(10),
      Q => \^data_sync_reg1\(10),
      R => p_0_in
    );
\drp_write_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(11),
      Q => \^data_sync_reg1\(11),
      R => p_0_in
    );
\drp_write_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(12),
      Q => \^data_sync_reg1\(12),
      R => p_0_in
    );
\drp_write_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(13),
      Q => \^data_sync_reg1\(13),
      R => p_0_in
    );
\drp_write_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(14),
      Q => \^data_sync_reg1\(14),
      R => p_0_in
    );
\drp_write_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(15),
      Q => \^data_sync_reg1\(15),
      R => p_0_in
    );
\drp_write_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(1),
      Q => \^data_sync_reg1\(1),
      R => p_0_in
    );
\drp_write_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(2),
      Q => \^data_sync_reg1\(2),
      R => p_0_in
    );
\drp_write_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(3),
      Q => \^data_sync_reg1\(3),
      R => p_0_in
    );
\drp_write_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(4),
      Q => \^data_sync_reg1\(4),
      R => p_0_in
    );
\drp_write_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(5),
      Q => \^data_sync_reg1\(5),
      R => p_0_in
    );
\drp_write_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(6),
      Q => \^data_sync_reg1\(6),
      R => p_0_in
    );
\drp_write_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(7),
      Q => \^data_sync_reg1\(7),
      R => p_0_in
    );
\drp_write_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(8),
      Q => \^data_sync_reg1\(8),
      R => p_0_in
    );
\drp_write_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_0(0),
      D => s_axi_wdata(9),
      Q => \^data_sync_reg1\(9),
      R => p_0_in
    );
rd_req_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => gt_slv_rden,
      Q => gt_slv_rd_done,
      R => p_0_in
    );
\timeout_length_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(0),
      Q => \timeout_length_reg_n_0_[0]\,
      R => p_0_in
    );
\timeout_length_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(10),
      Q => \timeout_length_reg_n_0_[10]\,
      R => p_0_in
    );
\timeout_length_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(11),
      Q => \timeout_length_reg_n_0_[11]\,
      R => p_0_in
    );
\timeout_length_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(1),
      Q => \axi_rdata_reg[8]_0\(0),
      R => p_0_in
    );
\timeout_length_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(2),
      Q => \timeout_length_reg_n_0_[2]\,
      R => p_0_in
    );
\timeout_length_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(3),
      Q => \axi_rdata_reg[8]_0\(1),
      R => p_0_in
    );
\timeout_length_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(4),
      Q => \axi_rdata_reg[8]_0\(2),
      R => p_0_in
    );
\timeout_length_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(5),
      Q => \axi_rdata_reg[8]_0\(3),
      R => p_0_in
    );
\timeout_length_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(6),
      Q => \axi_rdata_reg[8]_0\(4),
      R => p_0_in
    );
\timeout_length_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(7),
      Q => \axi_rdata_reg[8]_0\(5),
      R => p_0_in
    );
\timeout_length_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(8),
      Q => \axi_rdata_reg[8]_0\(6),
      R => p_0_in
    );
\timeout_length_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => gt_axi_map_wready_reg_1(0),
      D => s_axi_wdata(9),
      Q => \timeout_length_reg_n_0_[9]\,
      R => p_0_in
    );
wait_for_drp_reg: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => gt_axi_map_wready_reg,
      Q => \^wait_for_drp\,
      S => p_0_in
    );
wr_req_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => gt_slv_wren,
      Q => wr_req_reg,
      R => p_0_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt is
  port (
    SYSCLK_IN : in STD_LOGIC;
    SOFT_RESET_TX_IN : in STD_LOGIC;
    SOFT_RESET_RX_IN : in STD_LOGIC;
    DONT_RESET_ON_DATA_ERROR_IN : in STD_LOGIC;
    GT0_DATA_VALID_IN : in STD_LOGIC;
    GT_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;
    GT_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;
    gt0_cpllfbclklost_out : out STD_LOGIC;
    gt0_cplllock_out : out STD_LOGIC;
    gt0_cplllockdetclk_in : in STD_LOGIC;
    gt0_cpllpd_in : in STD_LOGIC;
    gt0_cpllreset_in : in STD_LOGIC;
    gt0_gtnorthrefclk0_in : in STD_LOGIC;
    gt0_gtnorthrefclk1_in : in STD_LOGIC;
    gt0_gtrefclk0_in : in STD_LOGIC;
    gt0_gtrefclk1_in : in STD_LOGIC;
    gt0_gtsouthrefclk0_in : in STD_LOGIC;
    gt0_gtsouthrefclk1_in : in STD_LOGIC;
    gt0_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    gt0_drpclk_in : in STD_LOGIC;
    gt0_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_drpen_in : in STD_LOGIC;
    gt0_drprdy_out : out STD_LOGIC;
    gt0_drpwe_in : in STD_LOGIC;
    gt0_rxsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txsysclksel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_loopback_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txpd_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_eyescanreset_in : in STD_LOGIC;
    gt0_rxuserrdy_in : in STD_LOGIC;
    gt0_eyescandataerror_out : out STD_LOGIC;
    gt0_eyescantrigger_in : in STD_LOGIC;
    gt0_rxcdrhold_in : in STD_LOGIC;
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );
    gt0_rxusrclk_in : in STD_LOGIC;
    gt0_rxusrclk2_in : in STD_LOGIC;
    gt0_rxdata_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_rxprbserr_out : out STD_LOGIC;
    gt0_rxprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxprbscntreset_in : in STD_LOGIC;
    gt0_rxdisperr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxnotintable_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_gthrxn_in : in STD_LOGIC;
    gt0_rxbufreset_in : in STD_LOGIC;
    gt0_rxbufstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxbyteisaligned_out : out STD_LOGIC;
    gt0_rxbyterealign_out : out STD_LOGIC;
    gt0_rxcommadet_out : out STD_LOGIC;
    gt0_rxmcommaalignen_in : in STD_LOGIC;
    gt0_rxpcommaalignen_in : in STD_LOGIC;
    gt0_rxdfelpmreset_in : in STD_LOGIC;
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxoutclk_out : out STD_LOGIC;
    gt0_rxoutclkfabric_out : out STD_LOGIC;
    gt0_gtrxreset_in : in STD_LOGIC;
    gt0_rxpcsreset_in : in STD_LOGIC;
    gt0_rxpmareset_in : in STD_LOGIC;
    gt0_rxlpmen_in : in STD_LOGIC;
    gt0_rxpolarity_in : in STD_LOGIC;
    gt0_rxchariscomma_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxcharisk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_gthrxp_in : in STD_LOGIC;
    gt0_rxresetdone_out : out STD_LOGIC;
    gt0_txpostcursor_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_txprecursor_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_gttxreset_in : in STD_LOGIC;
    gt0_txuserrdy_in : in STD_LOGIC;
    gt0_txusrclk_in : in STD_LOGIC;
    gt0_txusrclk2_in : in STD_LOGIC;
    gt0_txprbsforceerr_in : in STD_LOGIC;
    gt0_txbufstatus_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txdiffctrl_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_txinhibit_in : in STD_LOGIC;
    gt0_txdata_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_gthtxn_out : out STD_LOGIC;
    gt0_gthtxp_out : out STD_LOGIC;
    gt0_txoutclk_out : out STD_LOGIC;
    gt0_txoutclkfabric_out : out STD_LOGIC;
    gt0_txoutclkpcs_out : out STD_LOGIC;
    gt0_txpcsreset_in : in STD_LOGIC;
    gt0_txpmareset_in : in STD_LOGIC;
    gt0_txresetdone_out : out STD_LOGIC;
    gt0_txpolarity_in : in STD_LOGIC;
    gt0_txprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txcharisk_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    GT0_QPLLOUTCLK_IN : in STD_LOGIC;
    GT0_QPLLOUTREFCLK_IN : in STD_LOGIC
  );
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt : entity is "Jesd204_microblaze_jesd204_phy_0_0_gt,gtwizard_v3_6_5,{protocol_file=JESD204}";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt is
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of U0 : label is 0;
  attribute EXAMPLE_SIM_GTRESET_SPEEDUP : string;
  attribute EXAMPLE_SIM_GTRESET_SPEEDUP of U0 : label is "TRUE";
  attribute EXAMPLE_USE_CHIPSCOPE : integer;
  attribute EXAMPLE_USE_CHIPSCOPE of U0 : label is 1;
  attribute STABLE_CLOCK_PERIOD : integer;
  attribute STABLE_CLOCK_PERIOD of U0 : label is 10;
  attribute USE_BUFG : integer;
  attribute USE_BUFG of U0 : label is 0;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_init
     port map (
      DONT_RESET_ON_DATA_ERROR_IN => DONT_RESET_ON_DATA_ERROR_IN,
      GT0_DATA_VALID_IN => GT0_DATA_VALID_IN,
      GT0_QPLLOUTCLK_IN => GT0_QPLLOUTCLK_IN,
      GT0_QPLLOUTREFCLK_IN => GT0_QPLLOUTREFCLK_IN,
      GT_RX_FSM_RESET_DONE_OUT => GT_RX_FSM_RESET_DONE_OUT,
      GT_TX_FSM_RESET_DONE_OUT => GT_TX_FSM_RESET_DONE_OUT,
      SOFT_RESET_RX_IN => SOFT_RESET_RX_IN,
      SOFT_RESET_TX_IN => SOFT_RESET_TX_IN,
      SYSCLK_IN => SYSCLK_IN,
      gt0_cpllfbclklost_out => gt0_cpllfbclklost_out,
      gt0_cplllock_out => gt0_cplllock_out,
      gt0_cplllockdetclk_in => gt0_cplllockdetclk_in,
      gt0_cpllpd_in => gt0_cpllpd_in,
      gt0_cpllreset_in => gt0_cpllreset_in,
      gt0_dmonitorout_out(14 downto 0) => gt0_dmonitorout_out(14 downto 0),
      gt0_drpaddr_in(8 downto 0) => gt0_drpaddr_in(8 downto 0),
      gt0_drpclk_in => gt0_drpclk_in,
      gt0_drpdi_in(15 downto 0) => gt0_drpdi_in(15 downto 0),
      gt0_drpdo_out(15 downto 0) => gt0_drpdo_out(15 downto 0),
      gt0_drpen_in => gt0_drpen_in,
      gt0_drprdy_out => gt0_drprdy_out,
      gt0_drpwe_in => gt0_drpwe_in,
      gt0_eyescandataerror_out => gt0_eyescandataerror_out,
      gt0_eyescanreset_in => gt0_eyescanreset_in,
      gt0_eyescantrigger_in => gt0_eyescantrigger_in,
      gt0_gthrxn_in => gt0_gthrxn_in,
      gt0_gthrxp_in => gt0_gthrxp_in,
      gt0_gthtxn_out => gt0_gthtxn_out,
      gt0_gthtxp_out => gt0_gthtxp_out,
      gt0_gtnorthrefclk0_in => gt0_gtnorthrefclk0_in,
      gt0_gtnorthrefclk1_in => gt0_gtnorthrefclk1_in,
      gt0_gtrefclk0_in => gt0_gtrefclk0_in,
      gt0_gtrefclk1_in => gt0_gtrefclk1_in,
      gt0_gtrxreset_in => gt0_gtrxreset_in,
      gt0_gtsouthrefclk0_in => gt0_gtsouthrefclk0_in,
      gt0_gtsouthrefclk1_in => gt0_gtsouthrefclk1_in,
      gt0_gttxreset_in => gt0_gttxreset_in,
      gt0_loopback_in(2 downto 0) => gt0_loopback_in(2 downto 0),
      gt0_rxbufreset_in => gt0_rxbufreset_in,
      gt0_rxbufstatus_out(2 downto 0) => gt0_rxbufstatus_out(2 downto 0),
      gt0_rxbyteisaligned_out => gt0_rxbyteisaligned_out,
      gt0_rxbyterealign_out => gt0_rxbyterealign_out,
      gt0_rxcdrhold_in => gt0_rxcdrhold_in,
      gt0_rxchariscomma_out(3 downto 0) => gt0_rxchariscomma_out(3 downto 0),
      gt0_rxcharisk_out(3 downto 0) => gt0_rxcharisk_out(3 downto 0),
      gt0_rxcommadet_out => gt0_rxcommadet_out,
      gt0_rxdata_out(31 downto 0) => gt0_rxdata_out(31 downto 0),
      gt0_rxdfelpmreset_in => gt0_rxdfelpmreset_in,
      gt0_rxdisperr_out(3 downto 0) => gt0_rxdisperr_out(3 downto 0),
      gt0_rxlpmen_in => gt0_rxlpmen_in,
      gt0_rxmcommaalignen_in => gt0_rxmcommaalignen_in,
      gt0_rxmonitorout_out(6 downto 0) => gt0_rxmonitorout_out(6 downto 0),
      gt0_rxmonitorsel_in(1 downto 0) => gt0_rxmonitorsel_in(1 downto 0),
      gt0_rxnotintable_out(3 downto 0) => gt0_rxnotintable_out(3 downto 0),
      gt0_rxoutclk_out => gt0_rxoutclk_out,
      gt0_rxoutclkfabric_out => gt0_rxoutclkfabric_out,
      gt0_rxpcommaalignen_in => gt0_rxpcommaalignen_in,
      gt0_rxpcsreset_in => gt0_rxpcsreset_in,
      gt0_rxpd_in(1 downto 0) => gt0_rxpd_in(1 downto 0),
      gt0_rxpmareset_in => gt0_rxpmareset_in,
      gt0_rxpolarity_in => gt0_rxpolarity_in,
      gt0_rxprbscntreset_in => gt0_rxprbscntreset_in,
      gt0_rxprbserr_out => gt0_rxprbserr_out,
      gt0_rxprbssel_in(2 downto 0) => gt0_rxprbssel_in(2 downto 0),
      gt0_rxresetdone_out => gt0_rxresetdone_out,
      gt0_rxstatus_out(2 downto 0) => gt0_rxstatus_out(2 downto 0),
      gt0_rxsysclksel_in(1 downto 0) => gt0_rxsysclksel_in(1 downto 0),
      gt0_rxuserrdy_in => gt0_rxuserrdy_in,
      gt0_rxusrclk2_in => gt0_rxusrclk2_in,
      gt0_rxusrclk_in => gt0_rxusrclk_in,
      gt0_txbufstatus_out(1 downto 0) => gt0_txbufstatus_out(1 downto 0),
      gt0_txcharisk_in(3 downto 0) => gt0_txcharisk_in(3 downto 0),
      gt0_txdata_in(31 downto 0) => gt0_txdata_in(31 downto 0),
      gt0_txdiffctrl_in(3 downto 0) => gt0_txdiffctrl_in(3 downto 0),
      gt0_txinhibit_in => gt0_txinhibit_in,
      gt0_txoutclk_out => gt0_txoutclk_out,
      gt0_txoutclkfabric_out => gt0_txoutclkfabric_out,
      gt0_txoutclkpcs_out => gt0_txoutclkpcs_out,
      gt0_txpcsreset_in => gt0_txpcsreset_in,
      gt0_txpd_in(1 downto 0) => gt0_txpd_in(1 downto 0),
      gt0_txpmareset_in => gt0_txpmareset_in,
      gt0_txpolarity_in => gt0_txpolarity_in,
      gt0_txpostcursor_in(4 downto 0) => gt0_txpostcursor_in(4 downto 0),
      gt0_txprbsforceerr_in => gt0_txprbsforceerr_in,
      gt0_txprbssel_in(2 downto 0) => gt0_txprbssel_in(2 downto 0),
      gt0_txprecursor_in(4 downto 0) => gt0_txprecursor_in(4 downto 0),
      gt0_txresetdone_out => gt0_txresetdone_out,
      gt0_txsysclksel_in(1 downto 0) => gt0_txsysclksel_in(1 downto 0),
      gt0_txuserrdy_in => gt0_txuserrdy_in,
      gt0_txusrclk2_in => gt0_txusrclk2_in,
      gt0_txusrclk_in => gt0_txusrclk_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx is
  port (
    slv_wren_done_pulse : out STD_LOGIC;
    slv_rden_r : out STD_LOGIC;
    chan_rx_slv_rdata : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_rxpolarity_in : out STD_LOGIC;
    gt0_rxlpmen_in : out STD_LOGIC;
    gt0_rxdfelpmreset_in : out STD_LOGIC;
    \axi_bresp_reg[1]\ : out STD_LOGIC;
    p_0_in : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    data_sync_reg_gsr : in STD_LOGIC;
    rx_core_clk : in STD_LOGIC;
    chan_rx_slv_rden : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 0 to 0 );
    chan_rx_axi_map_wready : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    \slv_addr_reg[3]\ : in STD_LOGIC;
    \gt_interface_sel_reg[2]\ : in STD_LOGIC;
    \slv_addr_reg[3]_0\ : in STD_LOGIC;
    \slv_addr_reg[3]_1\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    slv_rden_r_0 : in STD_LOGIC;
    slv_wren_done_pulse_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx is
  signal clk2clk_handshake_pulse_gen_i_n_1 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_2 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_3 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_4 : STD_LOGIC;
  signal \^gt0_rxdfelpmreset_in\ : STD_LOGIC;
  signal \^gt0_rxlpmen_in\ : STD_LOGIC;
  signal \^gt0_rxpolarity_in\ : STD_LOGIC;
  signal slv_access_valid_hold : STD_LOGIC;
  signal \slv_rdata[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \^slv_rden_r\ : STD_LOGIC;
  signal \slv_wdata_r_internal[0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_wdata_r_internal_reg_n_0_[0]\ : STD_LOGIC;
begin
  gt0_rxdfelpmreset_in <= \^gt0_rxdfelpmreset_in\;
  gt0_rxlpmen_in <= \^gt0_rxlpmen_in\;
  gt0_rxpolarity_in <= \^gt0_rxpolarity_in\;
  slv_rden_r <= \^slv_rden_r\;
clk2clk_handshake_pulse_gen_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__2\
     port map (
      \axi_bresp_reg[1]\ => \axi_bresp_reg[1]\,
      chan_rx_axi_map_wready => chan_rx_axi_map_wready,
      chan_rx_slv_rden => chan_rx_slv_rden,
      data_in => slv_access_valid_hold,
      data_sync_reg_gsr => data_sync_reg_gsr,
      gt0_rxdfelpmreset_in => \^gt0_rxdfelpmreset_in\,
      gt0_rxlpmen_in => \^gt0_rxlpmen_in\,
      gt0_rxpolarity_in => \^gt0_rxpolarity_in\,
      \gt_interface_sel_reg[2]\ => \gt_interface_sel_reg[2]\,
      p_0_in => p_0_in,
      rx_core_clk => rx_core_clk,
      rxdfelpmreset_reg => clk2clk_handshake_pulse_gen_i_n_3,
      rxlpmen_reg => clk2clk_handshake_pulse_gen_i_n_2,
      rxpolarity_0_reg => clk2clk_handshake_pulse_gen_i_n_1,
      s_axi_aclk => s_axi_aclk,
      s_axi_wvalid => s_axi_wvalid,
      slv_access_valid_hold_reg => clk2clk_handshake_pulse_gen_i_n_4,
      \slv_addr_reg[3]\ => \slv_addr_reg[3]\,
      \slv_addr_reg[3]_0\ => \slv_addr_reg[3]_0\,
      \slv_addr_reg[3]_1\ => \slv_addr_reg[3]_1\,
      slv_rden_r_0 => slv_rden_r_0,
      slv_rden_r_reg => \^slv_rden_r\,
      \slv_wdata_r_internal_reg[0]\ => \slv_wdata_r_internal_reg_n_0_[0]\,
      slv_wren_done_pulse => slv_wren_done_pulse,
      slv_wren_done_pulse_1 => slv_wren_done_pulse_1
    );
rxdfelpmreset_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => rx_core_clk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_3,
      Q => \^gt0_rxdfelpmreset_in\,
      R => data_sync_reg_gsr
    );
rxlpmen_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => rx_core_clk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_2,
      Q => \^gt0_rxlpmen_in\,
      S => data_sync_reg_gsr
    );
rxpolarity_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => rx_core_clk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_1,
      Q => \^gt0_rxpolarity_in\,
      R => data_sync_reg_gsr
    );
slv_access_valid_hold_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_4,
      Q => slv_access_valid_hold,
      R => p_0_in
    );
\slv_rdata[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FAC000000AC0"
    )
        port map (
      I0 => \^gt0_rxpolarity_in\,
      I1 => \^gt0_rxlpmen_in\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => \gt_interface_sel_reg[2]\,
      I5 => \^gt0_rxdfelpmreset_in\,
      O => \slv_rdata[0]_i_1__0_n_0\
    );
\slv_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_aresetn,
      D => \slv_rdata[0]_i_1__0_n_0\,
      Q => chan_rx_slv_rdata(0),
      R => '0'
    );
slv_rden_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => chan_rx_slv_rden,
      Q => \^slv_rden_r\,
      R => p_0_in
    );
\slv_wdata_r_internal[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAEFEFEFAA202020"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \^slv_rden_r\,
      I2 => chan_rx_slv_rden,
      I3 => chan_rx_axi_map_wready,
      I4 => s_axi_wvalid,
      I5 => \slv_wdata_r_internal_reg_n_0_[0]\,
      O => \slv_wdata_r_internal[0]_i_1_n_0\
    );
\slv_wdata_r_internal_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_wdata_r_internal[0]_i_1_n_0\,
      Q => \slv_wdata_r_internal_reg_n_0_[0]\,
      R => p_0_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_tx is
  port (
    slv_wren_done_pulse : out STD_LOGIC;
    slv_wren_clk2 : out STD_LOGIC;
    slv_rden_r : out STD_LOGIC;
    gt0_txinhibit_in : out STD_LOGIC;
    gt0_txpolarity_in : out STD_LOGIC;
    gt0_txpd_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \slv_rdata_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    p_0_in : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_core_clk : in STD_LOGIC;
    chan_tx_slv_rden : in STD_LOGIC;
    \slv_addr_reg[3]\ : in STD_LOGIC;
    \slv_addr_reg[3]_0\ : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    chan_tx_axi_map_wready : in STD_LOGIC;
    \slv_addr_reg[2]\ : in STD_LOGIC;
    \gt_interface_sel_reg[2]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_addr_reg[5]\ : in STD_LOGIC;
    \slv_addr_reg[3]_1\ : in STD_LOGIC;
    \slv_addr_reg[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_aresetn : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_tx;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_tx is
  signal clk2clk_handshake_pulse_gen_i_n_2 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_3 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_4 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_5 : STD_LOGIC;
  signal clk2clk_handshake_pulse_gen_i_n_6 : STD_LOGIC;
  signal \^gt0_txinhibit_in\ : STD_LOGIC;
  signal \^gt0_txpd_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^gt0_txpolarity_in\ : STD_LOGIC;
  signal slv_access_valid_hold : STD_LOGIC;
  signal \slv_rdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \slv_rdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_rdata_reg[3]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^slv_rden_r\ : STD_LOGIC;
  signal \slv_wdata_r_internal_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_wdata_r_internal_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_wdata_r_internal_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_wdata_r_internal_reg_n_0_[3]\ : STD_LOGIC;
begin
  gt0_txinhibit_in <= \^gt0_txinhibit_in\;
  gt0_txpd_in(1 downto 0) <= \^gt0_txpd_in\(1 downto 0);
  gt0_txpolarity_in <= \^gt0_txpolarity_in\;
  \slv_rdata_reg[3]_0\(3 downto 0) <= \^slv_rdata_reg[3]_0\(3 downto 0);
  slv_rden_r <= \^slv_rden_r\;
clk2clk_handshake_pulse_gen_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen
     port map (
      Q(1) => \slv_wdata_r_internal_reg_n_0_[1]\,
      Q(0) => \slv_wdata_r_internal_reg_n_0_[0]\,
      SR(0) => SR(0),
      chan_tx_axi_map_wready => chan_tx_axi_map_wready,
      chan_tx_slv_rden => chan_tx_slv_rden,
      clk2_ready_reg_0 => slv_wren_clk2,
      data_in => slv_access_valid_hold,
      gt0_txinhibit_in => \^gt0_txinhibit_in\,
      gt0_txpd_in(1 downto 0) => \^gt0_txpd_in\(1 downto 0),
      gt0_txpolarity_in => \^gt0_txpolarity_in\,
      p_0_in => p_0_in,
      s_axi_aclk => s_axi_aclk,
      s_axi_wvalid => s_axi_wvalid,
      slv_access_valid_hold_reg => clk2clk_handshake_pulse_gen_i_n_4,
      \slv_addr_reg[2]\(0) => Q(0),
      \slv_addr_reg[3]\ => \slv_addr_reg[3]\,
      \slv_addr_reg[3]_0\ => \slv_addr_reg[3]_0\,
      \slv_addr_reg[3]_1\ => \slv_addr_reg[3]_1\,
      \slv_addr_reg[5]\ => \slv_addr_reg[5]\,
      slv_rden_r_reg => \^slv_rden_r\,
      slv_wren_done_pulse => slv_wren_done_pulse,
      tx_core_clk => tx_core_clk,
      \tx_pd_0_reg[0]\ => clk2clk_handshake_pulse_gen_i_n_6,
      \tx_pd_0_reg[1]\ => clk2clk_handshake_pulse_gen_i_n_5,
      txinihibit_0_reg => clk2clk_handshake_pulse_gen_i_n_2,
      txpolarity_0_reg => clk2clk_handshake_pulse_gen_i_n_3
    );
slv_access_valid_hold_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_4,
      Q => slv_access_valid_hold,
      R => p_0_in
    );
\slv_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"040F040FFFFF040F"
    )
        port map (
      I0 => \slv_addr_reg[2]\,
      I1 => \^gt0_txinhibit_in\,
      I2 => \gt_interface_sel_reg[2]\,
      I3 => \slv_rdata[0]_i_3_n_0\,
      I4 => \^gt0_txpolarity_in\,
      I5 => \slv_addr_reg[3]_0\,
      O => \slv_rdata[0]_i_1_n_0\
    );
\slv_rdata[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C7F7"
    )
        port map (
      I0 => \^gt0_txpd_in\(0),
      I1 => Q(0),
      I2 => Q(1),
      I3 => \^slv_rdata_reg[3]_0\(0),
      O => \slv_rdata[0]_i_3_n_0\
    );
\slv_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"002C0020"
    )
        port map (
      I0 => \^gt0_txpd_in\(1),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \gt_interface_sel_reg[2]\,
      I4 => \^slv_rdata_reg[3]_0\(1),
      O => \slv_rdata[1]_i_1_n_0\
    );
\slv_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_aresetn,
      D => \slv_rdata[0]_i_1_n_0\,
      Q => \axi_rdata_reg[3]\(0),
      R => '0'
    );
\slv_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_aresetn,
      D => \slv_rdata[1]_i_1_n_0\,
      Q => \axi_rdata_reg[3]\(1),
      R => '0'
    );
\slv_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_aresetn,
      D => D(0),
      Q => \axi_rdata_reg[3]\(2),
      R => '0'
    );
\slv_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_aresetn,
      D => D(1),
      Q => \axi_rdata_reg[3]\(3),
      R => '0'
    );
slv_rden_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => chan_tx_slv_rden,
      Q => \^slv_rden_r\,
      R => p_0_in
    );
\slv_wdata_r_internal_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(0),
      Q => \slv_wdata_r_internal_reg_n_0_[0]\,
      R => p_0_in
    );
\slv_wdata_r_internal_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(1),
      Q => \slv_wdata_r_internal_reg_n_0_[1]\,
      R => p_0_in
    );
\slv_wdata_r_internal_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(2),
      Q => \slv_wdata_r_internal_reg_n_0_[2]\,
      R => p_0_in
    );
\slv_wdata_r_internal_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(3),
      Q => \slv_wdata_r_internal_reg_n_0_[3]\,
      R => p_0_in
    );
\tx_pd_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_6,
      Q => \^gt0_txpd_in\(0),
      R => SR(0)
    );
\tx_pd_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_5,
      Q => \^gt0_txpd_in\(1),
      R => SR(0)
    );
\txdiffctrl_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => \slv_addr_reg[4]\(0),
      D => \slv_wdata_r_internal_reg_n_0_[0]\,
      Q => \^slv_rdata_reg[3]_0\(0),
      R => SR(0)
    );
\txdiffctrl_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => \slv_addr_reg[4]\(0),
      D => \slv_wdata_r_internal_reg_n_0_[1]\,
      Q => \^slv_rdata_reg[3]_0\(1),
      R => SR(0)
    );
\txdiffctrl_0_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => \slv_addr_reg[4]\(0),
      D => \slv_wdata_r_internal_reg_n_0_[2]\,
      Q => \^slv_rdata_reg[3]_0\(2),
      R => SR(0)
    );
\txdiffctrl_0_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => tx_core_clk,
      CE => \slv_addr_reg[4]\(0),
      D => \slv_wdata_r_internal_reg_n_0_[3]\,
      Q => \^slv_rdata_reg[3]_0\(3),
      S => SR(0)
    );
txinihibit_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_2,
      Q => \^gt0_txinhibit_in\,
      R => SR(0)
    );
txpolarity_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tx_core_clk,
      CE => '1',
      D => clk2clk_handshake_pulse_gen_i_n_3,
      Q => \^gt0_txpolarity_in\,
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface is
  port (
    s_axi_wready : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_cpllpd_in : out STD_LOGIC;
    gt0_txinhibit_in : out STD_LOGIC;
    gt0_txpolarity_in : out STD_LOGIC;
    gt0_rxpolarity_in : out STD_LOGIC;
    gt0_rxlpmen_in : out STD_LOGIC;
    gt0_rxdfelpmreset_in : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 24 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 8 downto 0 );
    data_sync_reg1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_rxpd_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_txsysclksel_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxsysclksel_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    data_sync_reg1_0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    data_sync_reg1_1 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    gt0_loopback_in : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txpd_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \slv_rdata_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_drpwe_in : out STD_LOGIC;
    gt0_drpen_in : out STD_LOGIC;
    data_in : out STD_LOGIC;
    \arststages_ff_reg[4]\ : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_core_clk : in STD_LOGIC;
    data_sync_reg_gsr : in STD_LOGIC;
    rx_core_clk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    drpclk : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 17 downto 0 );
    gt0_drprdy_out : in STD_LOGIC;
    data_out : in STD_LOGIC;
    data_sync_reg_gsr_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 9 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 9 downto 0 );
    data_sync_reg_gsr_1 : in STD_LOGIC;
    data_sync_reg_gsr_2 : in STD_LOGIC;
    tx_sys_reset : in STD_LOGIC;
    rx_sys_reset : in STD_LOGIC;
    data_sync_reg_gsr_3 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface is
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_24 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_25 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_26 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_29 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_3 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_30 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_40 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_41 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_42 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_43 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_44 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_45 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_46 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_47 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_48 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_49 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_50 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_51 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_52 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_53 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_54 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_55 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_10 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_11 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_12 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_13 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_14 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_15 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_16 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_29 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_30 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_31 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_32 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_33 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_34 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_35 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_12 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_13 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_24 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_27 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_28 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_29 : STD_LOGIC;
  signal Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx_i_n_6 : STD_LOGIC;
  signal access_type5_out : STD_LOGIC;
  signal axi_register_if_i_n_13 : STD_LOGIC;
  signal axi_register_if_i_n_14 : STD_LOGIC;
  signal axi_register_if_i_n_15 : STD_LOGIC;
  signal axi_register_if_i_n_16 : STD_LOGIC;
  signal axi_register_if_i_n_17 : STD_LOGIC;
  signal axi_register_if_i_n_23 : STD_LOGIC;
  signal axi_register_if_i_n_24 : STD_LOGIC;
  signal axi_register_if_i_n_25 : STD_LOGIC;
  signal axi_register_if_i_n_26 : STD_LOGIC;
  signal axi_register_if_i_n_27 : STD_LOGIC;
  signal axi_register_if_i_n_29 : STD_LOGIC;
  signal axi_register_if_i_n_30 : STD_LOGIC;
  signal axi_register_if_i_n_31 : STD_LOGIC;
  signal axi_register_if_i_n_32 : STD_LOGIC;
  signal axi_register_if_i_n_33 : STD_LOGIC;
  signal axi_register_if_i_n_34 : STD_LOGIC;
  signal axi_register_if_i_n_35 : STD_LOGIC;
  signal axi_register_if_i_n_36 : STD_LOGIC;
  signal axi_register_if_i_n_37 : STD_LOGIC;
  signal axi_register_if_i_n_38 : STD_LOGIC;
  signal axi_register_if_i_n_39 : STD_LOGIC;
  signal axi_register_if_i_n_40 : STD_LOGIC;
  signal axi_register_if_i_n_41 : STD_LOGIC;
  signal axi_register_if_i_n_42 : STD_LOGIC;
  signal axi_register_if_i_n_43 : STD_LOGIC;
  signal axi_register_if_i_n_44 : STD_LOGIC;
  signal axi_register_if_i_n_45 : STD_LOGIC;
  signal axi_register_if_i_n_46 : STD_LOGIC;
  signal axi_register_if_i_n_47 : STD_LOGIC;
  signal axi_register_if_i_n_48 : STD_LOGIC;
  signal axi_register_if_i_n_49 : STD_LOGIC;
  signal axi_register_if_i_n_50 : STD_LOGIC;
  signal axi_register_if_i_n_51 : STD_LOGIC;
  signal axi_register_if_i_n_52 : STD_LOGIC;
  signal axi_register_if_i_n_57 : STD_LOGIC;
  signal axi_register_if_i_n_58 : STD_LOGIC;
  signal axi_register_if_i_n_59 : STD_LOGIC;
  signal axi_register_if_i_n_60 : STD_LOGIC;
  signal axi_register_if_i_n_61 : STD_LOGIC;
  signal axi_register_if_i_n_62 : STD_LOGIC;
  signal axi_register_if_i_n_63 : STD_LOGIC;
  signal axi_register_if_i_n_64 : STD_LOGIC;
  signal axi_register_if_i_n_65 : STD_LOGIC;
  signal axi_register_if_i_n_66 : STD_LOGIC;
  signal axi_register_if_i_n_67 : STD_LOGIC;
  signal axi_register_if_i_n_68 : STD_LOGIC;
  signal chan_rx_axi_map_wready : STD_LOGIC;
  signal chan_rx_slv_rdata : STD_LOGIC_VECTOR ( 0 to 0 );
  signal chan_rx_slv_rden : STD_LOGIC;
  signal chan_tx_axi_map_wready : STD_LOGIC;
  signal chan_tx_slv_rdata : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal chan_tx_slv_rden : STD_LOGIC;
  signal \^data_sync_reg1\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^data_sync_reg1_0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^data_sync_reg1_1\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal drp_int_addr : STD_LOGIC;
  signal drp_read_data : STD_LOGIC_VECTOR ( 15 downto 12 );
  signal drp_reset : STD_LOGIC;
  signal drp_write_data : STD_LOGIC;
  signal \^gt0_cpllpd_in\ : STD_LOGIC;
  signal \^gt0_loopback_in\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^gt0_rxpd_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^gt0_rxsysclksel_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^gt0_txsysclksel_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal gt_interface_sel : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal gt_slv_rd_done : STD_LOGIC;
  signal gt_slv_rden : STD_LOGIC;
  signal gt_slv_wren : STD_LOGIC;
  signal \i_/i_/i__carry__0_n_0\ : STD_LOGIC;
  signal \i_/i_/i__carry__0_n_1\ : STD_LOGIC;
  signal \i_/i_/i__carry__0_n_2\ : STD_LOGIC;
  signal \i_/i_/i__carry__0_n_3\ : STD_LOGIC;
  signal \i_/i_/i__carry__0_n_4\ : STD_LOGIC;
  signal \i_/i_/i__carry__0_n_5\ : STD_LOGIC;
  signal \i_/i_/i__carry__0_n_6\ : STD_LOGIC;
  signal \i_/i_/i__carry__0_n_7\ : STD_LOGIC;
  signal \i_/i_/i__carry__1_n_0\ : STD_LOGIC;
  signal \i_/i_/i__carry__1_n_1\ : STD_LOGIC;
  signal \i_/i_/i__carry__1_n_2\ : STD_LOGIC;
  signal \i_/i_/i__carry__1_n_3\ : STD_LOGIC;
  signal \i_/i_/i__carry__1_n_4\ : STD_LOGIC;
  signal \i_/i_/i__carry__1_n_5\ : STD_LOGIC;
  signal \i_/i_/i__carry__1_n_6\ : STD_LOGIC;
  signal \i_/i_/i__carry__1_n_7\ : STD_LOGIC;
  signal \i_/i_/i__carry__2_n_7\ : STD_LOGIC;
  signal \i_/i_/i__carry_n_0\ : STD_LOGIC;
  signal \i_/i_/i__carry_n_1\ : STD_LOGIC;
  signal \i_/i_/i__carry_n_2\ : STD_LOGIC;
  signal \i_/i_/i__carry_n_3\ : STD_LOGIC;
  signal \i_/i_/i__carry_n_4\ : STD_LOGIC;
  signal \i_/i_/i__carry_n_5\ : STD_LOGIC;
  signal \i_/i_/i__carry_n_6\ : STD_LOGIC;
  signal \i_/i_/i__carry_n_7\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal rx_sys_reset_axi : STD_LOGIC;
  signal slv_addr : STD_LOGIC_VECTOR ( 6 downto 2 );
  signal \^slv_rdata_reg[3]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal slv_rden_r : STD_LOGIC;
  signal slv_rden_r_0 : STD_LOGIC;
  signal slv_wren_clk2 : STD_LOGIC;
  signal slv_wren_done_pulse : STD_LOGIC;
  signal slv_wren_done_pulse_1 : STD_LOGIC;
  signal timeout_enable : STD_LOGIC;
  signal timeout_length : STD_LOGIC;
  signal timeout_value : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal tx_sys_reset_axi : STD_LOGIC;
  signal wait_for_drp : STD_LOGIC;
  signal \NLW_i_/i_/i__carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_i_/i_/i__carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
begin
  data_sync_reg1(15 downto 0) <= \^data_sync_reg1\(15 downto 0);
  data_sync_reg1_0(4 downto 0) <= \^data_sync_reg1_0\(4 downto 0);
  data_sync_reg1_1(4 downto 0) <= \^data_sync_reg1_1\(4 downto 0);
  gt0_cpllpd_in <= \^gt0_cpllpd_in\;
  gt0_loopback_in(2 downto 0) <= \^gt0_loopback_in\(2 downto 0);
  gt0_rxpd_in(1 downto 0) <= \^gt0_rxpd_in\(1 downto 0);
  gt0_rxsysclksel_in(1 downto 0) <= \^gt0_rxsysclksel_in\(1 downto 0);
  gt0_txsysclksel_in(1 downto 0) <= \^gt0_txsysclksel_in\(1 downto 0);
  \slv_rdata_reg[3]\(3 downto 0) <= \^slv_rdata_reg[3]\(3 downto 0);
Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox
     port map (
      D(15 downto 0) => D(15 downto 0),
      E(0) => drp_int_addr,
      Q(0) => gt_interface_sel(0),
      access_type5_out => access_type5_out,
      \axi_bresp_reg[1]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_29,
      \axi_rdata_reg[0]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_47,
      \axi_rdata_reg[0]_0\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_48,
      \axi_rdata_reg[10]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_24,
      \axi_rdata_reg[11]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_3,
      \axi_rdata_reg[15]\(3 downto 0) => drp_read_data(15 downto 12),
      \axi_rdata_reg[1]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_46,
      \axi_rdata_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_26,
      \axi_rdata_reg[2]_0\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_45,
      \axi_rdata_reg[3]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_44,
      \axi_rdata_reg[4]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_43,
      \axi_rdata_reg[5]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_42,
      \axi_rdata_reg[6]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_41,
      \axi_rdata_reg[7]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_40,
      \axi_rdata_reg[8]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_30,
      \axi_rdata_reg[8]_0\(6) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_49,
      \axi_rdata_reg[8]_0\(5) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_50,
      \axi_rdata_reg[8]_0\(4) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_51,
      \axi_rdata_reg[8]_0\(3) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_52,
      \axi_rdata_reg[8]_0\(2) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_53,
      \axi_rdata_reg[8]_0\(1) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_54,
      \axi_rdata_reg[8]_0\(0) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_55,
      \axi_rdata_reg[9]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_25,
      data_out => data_out,
      data_sync_reg1(15 downto 0) => \^data_sync_reg1\(15 downto 0),
      data_sync_reg1_0(8 downto 0) => Q(8 downto 0),
      data_sync_reg_gsr(0) => data_sync_reg_gsr_0(0),
      drp_reset => drp_reset,
      drp_reset_reg_0 => axi_register_if_i_n_27,
      drpclk => drpclk,
      gt0_drpen_in => gt0_drpen_in,
      gt0_drprdy_out => gt0_drprdy_out,
      gt0_drpwe_in => gt0_drpwe_in,
      gt_axi_map_wready_reg => axi_register_if_i_n_29,
      gt_axi_map_wready_reg_0(0) => drp_write_data,
      gt_axi_map_wready_reg_1(0) => timeout_length,
      gt_slv_rd_done => gt_slv_rd_done,
      gt_slv_rden => gt_slv_rden,
      gt_slv_wren => gt_slv_wren,
      p_0_in => p_0_in,
      s_axi_aclk => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      s_axi_wdata(17 downto 0) => s_axi_wdata(17 downto 0),
      \slv_addr_reg[4]\(2 downto 0) => slv_addr(4 downto 2),
      wait_for_drp => wait_for_drp
    );
Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig
     port map (
      E(0) => axi_register_if_i_n_58,
      Q(7 downto 0) => gt_interface_sel(7 downto 0),
      \axi_rdata_reg[0]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_16,
      \axi_rdata_reg[1]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_11,
      \axi_rdata_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_12,
      \axi_rdata_reg[2]_0\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_13,
      \axi_rdata_reg[7]\(6) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_29,
      \axi_rdata_reg[7]\(5) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_30,
      \axi_rdata_reg[7]\(4) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_31,
      \axi_rdata_reg[7]\(3) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_32,
      \axi_rdata_reg[7]\(2) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_33,
      \axi_rdata_reg[7]\(1) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_34,
      \axi_rdata_reg[7]\(0) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_35,
      \loopback_0_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_15,
      p_0_in => p_0_in,
      \rx_pd_0_reg[1]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_14,
      s_axi_aclk => s_axi_aclk,
      s_axi_wdata(11 downto 0) => s_axi_wdata(11 downto 0),
      \slv_addr_reg[2]\ => axi_register_if_i_n_26,
      \slv_addr_reg[2]_0\(0) => axi_register_if_i_n_17,
      \slv_addr_reg[2]_1\(0) => axi_register_if_i_n_57,
      \slv_addr_reg[5]\ => axi_register_if_i_n_66,
      \slv_addr_reg[6]\(3 downto 1) => slv_addr(6 downto 4),
      \slv_addr_reg[6]\(0) => slv_addr(2),
      \slv_rd_addr_reg[1]\(0) => axi_register_if_i_n_24,
      timeout_enable => timeout_enable,
      \timeout_timer_count_reg[11]\(11 downto 0) => timeout_value(11 downto 0),
      \txpllclksel_reg[1]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1,
      \txpllclksel_reg[1]_0\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_10
    );
Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async
     port map (
      E(0) => axi_register_if_i_n_60,
      Q(1 downto 0) => slv_addr(3 downto 2),
      \arststages_ff_reg[4]\ => \arststages_ff_reg[4]\,
      \axi_rdata_reg[0]\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_28,
      \axi_rdata_reg[0]_0\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_29,
      \axi_rdata_reg[1]\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_12,
      \axi_rdata_reg[1]_0\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_27,
      \axi_rdata_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_24,
      \axi_rdata_reg[3]\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_13,
      chan_async_axi_map_wready_reg => axi_register_if_i_n_30,
      chan_async_axi_map_wready_reg_0 => axi_register_if_i_n_40,
      chan_async_axi_map_wready_reg_1 => axi_register_if_i_n_41,
      chan_async_axi_map_wready_reg_2 => axi_register_if_i_n_42,
      data_in => data_in,
      data_sync_reg1(4 downto 0) => \^data_sync_reg1_1\(4 downto 0),
      data_sync_reg1_0(4 downto 0) => \^data_sync_reg1_0\(4 downto 0),
      gt0_cpllpd_in => \^gt0_cpllpd_in\,
      gt0_loopback_in(2 downto 0) => \^gt0_loopback_in\(2 downto 0),
      gt0_rxpd_in(1 downto 0) => \^gt0_rxpd_in\(1 downto 0),
      gt0_rxsysclksel_in(1 downto 0) => \^gt0_rxsysclksel_in\(1 downto 0),
      gt0_txsysclksel_in(1 downto 0) => \^gt0_txsysclksel_in\(1 downto 0),
      p_0_in => p_0_in,
      rx_sys_reset => rx_sys_reset,
      rx_sys_reset_axi => rx_sys_reset_axi,
      s_axi_aclk => s_axi_aclk,
      s_axi_wdata(4 downto 0) => s_axi_wdata(4 downto 0),
      \slv_addr_reg[2]\ => axi_register_if_i_n_32,
      \slv_addr_reg[2]_0\ => axi_register_if_i_n_33,
      \slv_addr_reg[2]_1\ => axi_register_if_i_n_34,
      \slv_addr_reg[2]_2\ => axi_register_if_i_n_35,
      \slv_addr_reg[2]_3\ => axi_register_if_i_n_36,
      \slv_addr_reg[2]_4\ => axi_register_if_i_n_37,
      \slv_addr_reg[2]_5\ => axi_register_if_i_n_38,
      \slv_addr_reg[2]_6\ => axi_register_if_i_n_39,
      \slv_addr_reg[4]\(0) => axi_register_if_i_n_67,
      tx_sys_reset => tx_sys_reset,
      tx_sys_reset_axi => tx_sys_reset_axi
    );
Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx
     port map (
      Q(1 downto 0) => slv_addr(3 downto 2),
      \axi_bresp_reg[1]\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx_i_n_6,
      chan_rx_axi_map_wready => chan_rx_axi_map_wready,
      chan_rx_slv_rdata(0) => chan_rx_slv_rdata(0),
      chan_rx_slv_rden => chan_rx_slv_rden,
      data_sync_reg_gsr => data_sync_reg_gsr,
      gt0_rxdfelpmreset_in => gt0_rxdfelpmreset_in,
      gt0_rxlpmen_in => gt0_rxlpmen_in,
      gt0_rxpolarity_in => gt0_rxpolarity_in,
      \gt_interface_sel_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1,
      p_0_in => p_0_in,
      rx_core_clk => rx_core_clk,
      s_axi_aclk => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      s_axi_wdata(0) => s_axi_wdata(0),
      s_axi_wvalid => s_axi_wvalid,
      \slv_addr_reg[3]\ => axi_register_if_i_n_25,
      \slv_addr_reg[3]_0\ => axi_register_if_i_n_31,
      \slv_addr_reg[3]_1\ => axi_register_if_i_n_61,
      slv_rden_r => slv_rden_r,
      slv_rden_r_0 => slv_rden_r_0,
      slv_wren_done_pulse => slv_wren_done_pulse,
      slv_wren_done_pulse_1 => slv_wren_done_pulse_1
    );
Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_tx_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_tx
     port map (
      D(1) => axi_register_if_i_n_63,
      D(0) => axi_register_if_i_n_64,
      E(0) => axi_register_if_i_n_68,
      Q(1 downto 0) => slv_addr(3 downto 2),
      SR(0) => SR(0),
      \axi_rdata_reg[3]\(3 downto 0) => chan_tx_slv_rdata(3 downto 0),
      chan_tx_axi_map_wready => chan_tx_axi_map_wready,
      chan_tx_slv_rden => chan_tx_slv_rden,
      gt0_txinhibit_in => gt0_txinhibit_in,
      gt0_txpd_in(1 downto 0) => gt0_txpd_in(1 downto 0),
      gt0_txpolarity_in => gt0_txpolarity_in,
      \gt_interface_sel_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1,
      p_0_in => p_0_in,
      s_axi_aclk => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      s_axi_wdata(3 downto 0) => s_axi_wdata(3 downto 0),
      s_axi_wvalid => s_axi_wvalid,
      \slv_addr_reg[2]\ => axi_register_if_i_n_62,
      \slv_addr_reg[3]\ => axi_register_if_i_n_61,
      \slv_addr_reg[3]_0\ => axi_register_if_i_n_59,
      \slv_addr_reg[3]_1\ => axi_register_if_i_n_23,
      \slv_addr_reg[4]\(0) => axi_register_if_i_n_65,
      \slv_addr_reg[5]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_14,
      \slv_rdata_reg[3]_0\(3 downto 0) => \^slv_rdata_reg[3]\(3 downto 0),
      slv_rden_r => slv_rden_r_0,
      slv_wren_clk2 => slv_wren_clk2,
      slv_wren_done_pulse => slv_wren_done_pulse_1,
      tx_core_clk => tx_core_clk
    );
axi_register_if_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_axi
     port map (
      D(1) => axi_register_if_i_n_63,
      D(0) => axi_register_if_i_n_64,
      DI(0) => axi_register_if_i_n_52,
      E(0) => drp_int_addr,
      O(3) => \i_/i_/i__carry_n_4\,
      O(2) => \i_/i_/i__carry_n_5\,
      O(1) => \i_/i_/i__carry_n_6\,
      O(0) => \i_/i_/i__carry_n_7\,
      Q(4 downto 0) => slv_addr(6 downto 2),
      S(3) => axi_register_if_i_n_13,
      S(2) => axi_register_if_i_n_14,
      S(1) => axi_register_if_i_n_15,
      S(0) => axi_register_if_i_n_16,
      access_type5_out => access_type5_out,
      \axi_rdata_reg[7]_0\ => axi_register_if_i_n_66,
      chan_async_axi_map_wready_reg_0(0) => axi_register_if_i_n_24,
      chan_rx_axi_map_wready => chan_rx_axi_map_wready,
      chan_rx_slv_rdata(0) => chan_rx_slv_rdata(0),
      chan_rx_slv_rden => chan_rx_slv_rden,
      chan_tx_axi_map_wready => chan_tx_axi_map_wready,
      chan_tx_slv_rden => chan_tx_slv_rden,
      clk1_ready_pulse_reg => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_29,
      \cmm_interface_sel_reg[0]\(0) => axi_register_if_i_n_17,
      \cmm_interface_sel_reg[7]\(6) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_29,
      \cmm_interface_sel_reg[7]\(5) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_30,
      \cmm_interface_sel_reg[7]\(4) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_31,
      \cmm_interface_sel_reg[7]\(3) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_32,
      \cmm_interface_sel_reg[7]\(2) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_33,
      \cmm_interface_sel_reg[7]\(1) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_34,
      \cmm_interface_sel_reg[7]\(0) => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_35,
      cpll_pd_0_reg => axi_register_if_i_n_30,
      cpll_pd_0_reg_0 => axi_register_if_i_n_31,
      data_sync_reg_gsr => data_sync_reg_gsr_1,
      data_sync_reg_gsr_0 => data_sync_reg_gsr_2,
      data_sync_reg_gsr_1 => data_sync_reg_gsr_3,
      \drp_int_addr_reg[1]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_46,
      \drp_int_addr_reg[3]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_44,
      \drp_int_addr_reg[4]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_43,
      \drp_read_data_reg[0]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_47,
      \drp_read_data_reg[15]\(3 downto 0) => drp_read_data(15 downto 12),
      \drp_read_data_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_45,
      \drp_read_data_reg[5]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_42,
      \drp_read_data_reg[6]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_41,
      \drp_read_data_reg[7]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_40,
      \drp_read_data_reg[8]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_30,
      drp_reset => drp_reset,
      drp_reset_reg => axi_register_if_i_n_27,
      \drp_write_data_reg[10]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_24,
      \drp_write_data_reg[11]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_3,
      \drp_write_data_reg[15]\(0) => drp_write_data,
      \drp_write_data_reg[15]_0\(3 downto 0) => \^data_sync_reg1\(15 downto 12),
      gt0_cpllpd_in => \^gt0_cpllpd_in\,
      gt0_loopback_in(2 downto 0) => \^gt0_loopback_in\(2 downto 0),
      gt0_rxpd_in(1 downto 0) => \^gt0_rxpd_in\(1 downto 0),
      gt0_rxsysclksel_in(1 downto 0) => \^gt0_rxsysclksel_in\(1 downto 0),
      gt0_txsysclksel_in(1 downto 0) => \^gt0_txsysclksel_in\(1 downto 0),
      \gt_interface_sel_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1,
      \gt_interface_sel_reg[2]_0\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_13,
      \gt_interface_sel_reg[4]\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_10,
      \gt_interface_sel_reg[7]\(0) => axi_register_if_i_n_57,
      \gt_interface_sel_reg[7]_0\(6 downto 0) => gt_interface_sel(7 downto 1),
      gt_slv_rd_done => gt_slv_rd_done,
      gt_slv_rden => gt_slv_rden,
      gt_slv_wren => gt_slv_wren,
      \loopback_0_reg[0]\ => axi_register_if_i_n_42,
      \loopback_0_reg[0]_0\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_28,
      \loopback_0_reg[1]\ => axi_register_if_i_n_41,
      \loopback_0_reg[1]_0\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_27,
      \loopback_0_reg[2]\ => axi_register_if_i_n_40,
      p_0_in => p_0_in,
      \rx_pd_0_reg[0]\ => axi_register_if_i_n_35,
      \rx_pd_0_reg[1]\ => axi_register_if_i_n_23,
      \rx_pd_0_reg[1]_0\ => axi_register_if_i_n_34,
      \rx_pd_0_reg[1]_1\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_12,
      rx_sys_reset_axi => rx_sys_reset_axi,
      rx_sys_reset_axi_reg => axi_register_if_i_n_33,
      \rxpllclksel_reg[0]\ => axi_register_if_i_n_39,
      \rxpllclksel_reg[1]\ => axi_register_if_i_n_38,
      rxpolarity_0_reg => axi_register_if_i_n_25,
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(9 downto 0) => s_axi_araddr(9 downto 0),
      s_axi_aresetn => s_axi_aresetn,
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(9 downto 0) => s_axi_awaddr(9 downto 0),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bresp(0) => s_axi_bresp(0),
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(24 downto 0) => s_axi_rdata(24 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rresp(0) => s_axi_rresp(0),
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(4 downto 3) => s_axi_wdata(17 downto 16),
      s_axi_wdata(2 downto 0) => s_axi_wdata(2 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wvalid => s_axi_wvalid,
      \slv_addr_reg[5]_0\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_14,
      \slv_addr_reg[5]_1\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_12,
      \slv_addr_reg[6]_0\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_15,
      \slv_rd_addr_reg[1]_0\ => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_11,
      \slv_rdata_reg[0]\ => axi_register_if_i_n_62,
      \slv_rdata_reg[3]\(3 downto 0) => chan_tx_slv_rdata(3 downto 0),
      slv_rden_r => slv_rden_r_0,
      slv_rden_r_1 => slv_rden_r,
      slv_rden_r_reg => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx_i_n_6,
      \slv_wdata_r_internal_reg[0]\(0) => axi_register_if_i_n_68,
      slv_wren_clk2 => slv_wren_clk2,
      slv_wren_done_pulse => slv_wren_done_pulse_1,
      slv_wren_done_pulse_0 => slv_wren_done_pulse,
      timeout_enable => timeout_enable,
      timeout_enable_reg => axi_register_if_i_n_26,
      timeout_enable_reg_0 => Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_16,
      \timeout_length_reg[11]\(0) => timeout_length,
      \timeout_length_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_26,
      \timeout_length_reg[8]\(6) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_49,
      \timeout_length_reg[8]\(5) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_50,
      \timeout_length_reg[8]\(4) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_51,
      \timeout_length_reg[8]\(3) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_52,
      \timeout_length_reg[8]\(2) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_53,
      \timeout_length_reg[8]\(1) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_54,
      \timeout_length_reg[8]\(0) => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_55,
      \timeout_length_reg[9]\ => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_25,
      \timeout_timer_count_reg[11]_0\(3) => axi_register_if_i_n_44,
      \timeout_timer_count_reg[11]_0\(2) => axi_register_if_i_n_45,
      \timeout_timer_count_reg[11]_0\(1) => axi_register_if_i_n_46,
      \timeout_timer_count_reg[11]_0\(0) => axi_register_if_i_n_47,
      \timeout_timer_count_reg[11]_1\(3) => \i_/i_/i__carry__1_n_4\,
      \timeout_timer_count_reg[11]_1\(2) => \i_/i_/i__carry__1_n_5\,
      \timeout_timer_count_reg[11]_1\(1) => \i_/i_/i__carry__1_n_6\,
      \timeout_timer_count_reg[11]_1\(0) => \i_/i_/i__carry__1_n_7\,
      \timeout_timer_count_reg[12]_0\(0) => axi_register_if_i_n_43,
      \timeout_timer_count_reg[12]_1\(0) => \i_/i_/i__carry__2_n_7\,
      \timeout_timer_count_reg[7]_0\(3) => axi_register_if_i_n_48,
      \timeout_timer_count_reg[7]_0\(2) => axi_register_if_i_n_49,
      \timeout_timer_count_reg[7]_0\(1) => axi_register_if_i_n_50,
      \timeout_timer_count_reg[7]_0\(0) => axi_register_if_i_n_51,
      \timeout_value_reg[11]\(0) => axi_register_if_i_n_58,
      \timeout_value_reg[11]_0\(11 downto 0) => timeout_value(11 downto 0),
      \timeout_value_reg[7]\(3) => \i_/i_/i__carry__0_n_4\,
      \timeout_value_reg[7]\(2) => \i_/i_/i__carry__0_n_5\,
      \timeout_value_reg[7]\(1) => \i_/i_/i__carry__0_n_6\,
      \timeout_value_reg[7]\(0) => \i_/i_/i__carry__0_n_7\,
      tx_sys_reset_axi => tx_sys_reset_axi,
      tx_sys_reset_axi_reg => axi_register_if_i_n_32,
      \txdiffctrl_0_reg[3]\(0) => axi_register_if_i_n_65,
      \txdiffctrl_0_reg[3]_0\(1 downto 0) => \^slv_rdata_reg[3]\(3 downto 2),
      txinihibit_0_reg => axi_register_if_i_n_61,
      \txpllclksel_reg[0]\ => axi_register_if_i_n_37,
      \txpllclksel_reg[0]_0\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_29,
      \txpllclksel_reg[1]\ => axi_register_if_i_n_36,
      txpolarity_0_reg => axi_register_if_i_n_59,
      \txpostcursor_0_reg[2]\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_24,
      \txpostcursor_0_reg[4]\(0) => axi_register_if_i_n_60,
      \txpostcursor_0_reg[4]_0\(0) => \^data_sync_reg1_0\(4),
      \txprecursor_0_reg[3]\ => Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_13,
      \txprecursor_0_reg[4]\(0) => axi_register_if_i_n_67,
      \txprecursor_0_reg[4]_0\(0) => \^data_sync_reg1_1\(4),
      wait_for_drp => wait_for_drp,
      wait_for_drp_reg => axi_register_if_i_n_29,
      wait_for_drp_reg_0 => Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_48
    );
\i_/i_/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \i_/i_/i__carry_n_0\,
      CO(2) => \i_/i_/i__carry_n_1\,
      CO(1) => \i_/i_/i__carry_n_2\,
      CO(0) => \i_/i_/i__carry_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => axi_register_if_i_n_52,
      O(3) => \i_/i_/i__carry_n_4\,
      O(2) => \i_/i_/i__carry_n_5\,
      O(1) => \i_/i_/i__carry_n_6\,
      O(0) => \i_/i_/i__carry_n_7\,
      S(3) => axi_register_if_i_n_13,
      S(2) => axi_register_if_i_n_14,
      S(1) => axi_register_if_i_n_15,
      S(0) => axi_register_if_i_n_16
    );
\i_/i_/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \i_/i_/i__carry_n_0\,
      CO(3) => \i_/i_/i__carry__0_n_0\,
      CO(2) => \i_/i_/i__carry__0_n_1\,
      CO(1) => \i_/i_/i__carry__0_n_2\,
      CO(0) => \i_/i_/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \i_/i_/i__carry__0_n_4\,
      O(2) => \i_/i_/i__carry__0_n_5\,
      O(1) => \i_/i_/i__carry__0_n_6\,
      O(0) => \i_/i_/i__carry__0_n_7\,
      S(3) => axi_register_if_i_n_48,
      S(2) => axi_register_if_i_n_49,
      S(1) => axi_register_if_i_n_50,
      S(0) => axi_register_if_i_n_51
    );
\i_/i_/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \i_/i_/i__carry__0_n_0\,
      CO(3) => \i_/i_/i__carry__1_n_0\,
      CO(2) => \i_/i_/i__carry__1_n_1\,
      CO(1) => \i_/i_/i__carry__1_n_2\,
      CO(0) => \i_/i_/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \i_/i_/i__carry__1_n_4\,
      O(2) => \i_/i_/i__carry__1_n_5\,
      O(1) => \i_/i_/i__carry__1_n_6\,
      O(0) => \i_/i_/i__carry__1_n_7\,
      S(3) => axi_register_if_i_n_44,
      S(2) => axi_register_if_i_n_45,
      S(1) => axi_register_if_i_n_46,
      S(0) => axi_register_if_i_n_47
    );
\i_/i_/i__carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \i_/i_/i__carry__1_n_0\,
      CO(3 downto 0) => \NLW_i_/i_/i__carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_i_/i_/i__carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => \i_/i_/i__carry__2_n_7\,
      S(3 downto 1) => B"000",
      S(0) => axi_register_if_i_n_43
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_block is
  port (
    s_axi_arready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    gt0_cplllock_out : out STD_LOGIC;
    gt0_eyescandataerror_out : out STD_LOGIC;
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );
    gt0_rxdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_rxprbserr_out : out STD_LOGIC;
    gt0_rxdisperr : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxnotintable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxbufstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxbyteisaligned_out : out STD_LOGIC;
    gt0_rxbyterealign_out : out STD_LOGIC;
    gt0_rxcommadet_out : out STD_LOGIC;
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );
    rxoutclk : out STD_LOGIC;
    gt0_rxcharisk : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxresetdone_out : out STD_LOGIC;
    gt0_txbufstatus_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    txn_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    txp_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    txoutclk : out STD_LOGIC;
    gt0_txresetdone_out : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 24 downto 0 );
    tx_reset_done : out STD_LOGIC;
    rx_reset_done : out STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    tx_reset_gt : in STD_LOGIC;
    drpclk : in STD_LOGIC;
    rx_reset_gt : in STD_LOGIC;
    cpll_refclk : in STD_LOGIC;
    gt0_eyescanreset_in : in STD_LOGIC;
    gt0_eyescantrigger_in : in STD_LOGIC;
    gt0_rxcdrhold_in : in STD_LOGIC;
    rx_core_clk : in STD_LOGIC;
    gt0_rxprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxprbscntreset_in : in STD_LOGIC;
    rxn_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_rxbufreset_in : in STD_LOGIC;
    rxencommaalign : in STD_LOGIC;
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxpcsreset_in : in STD_LOGIC;
    gt0_rxpmareset_in : in STD_LOGIC;
    rxp_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_core_clk : in STD_LOGIC;
    gt0_txprbsforceerr_in : in STD_LOGIC;
    gt0_txdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_txpcsreset_in : in STD_LOGIC;
    gt0_txpmareset_in : in STD_LOGIC;
    gt_prbssel : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txcharisk : in STD_LOGIC_VECTOR ( 3 downto 0 );
    common0_qpll_clk_in : in STD_LOGIC;
    common0_qpll_refclk_in : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 17 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 9 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 9 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    tx_sys_reset : in STD_LOGIC;
    rx_sys_reset : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_block is
  signal cpll_lock_axi : STD_LOGIC;
  signal cpll_lock_r : STD_LOGIC;
  signal cpll_lock_sync : STD_LOGIC;
  signal cpllpd_i : STD_LOGIC;
  signal data_in0 : STD_LOGIC;
  signal data_in00_out : STD_LOGIC;
  signal \^gt0_cplllock_out\ : STD_LOGIC;
  signal gt0_drpaddr : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal gt0_drpdi : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal gt0_drpdo : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal gt0_drpen : STD_LOGIC;
  signal gt0_drprdy : STD_LOGIC;
  signal gt0_drpwe : STD_LOGIC;
  signal gt0_loopback_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal gt0_rxdfelpmreset_in : STD_LOGIC;
  signal gt0_rxlpmen_in : STD_LOGIC;
  signal gt0_rxpd_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal gt0_rxpolarity_in : STD_LOGIC;
  signal \^gt0_rxresetdone_out\ : STD_LOGIC;
  signal gt0_rxsysclksel_i : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal gt0_txdiffctrl_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal gt0_txinhibit_in : STD_LOGIC;
  signal gt0_txpd_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal gt0_txpolarity_in : STD_LOGIC;
  signal gt0_txpostcursor_in : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal gt0_txprecursor_in : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^gt0_txresetdone_out\ : STD_LOGIC;
  signal gt0_txsysclksel_i : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal gt_rxfsmdone : STD_LOGIC;
  signal gt_rxreset : STD_LOGIC;
  signal gt_rxreset0 : STD_LOGIC;
  signal gt_txfsmdone : STD_LOGIC;
  signal gt_txreset : STD_LOGIC;
  signal gt_txreset0 : STD_LOGIC;
  signal rx_chan_rst_done_r : STD_LOGIC;
  signal rx_pll_lock_i_reg_n_0 : STD_LOGIC;
  signal rx_pll_lock_sync : STD_LOGIC;
  signal \^rx_reset_done\ : STD_LOGIC;
  signal rx_reset_done_axi : STD_LOGIC;
  signal rx_reset_done_r0 : STD_LOGIC;
  signal rx_rst_gt_data_sync : STD_LOGIC;
  signal s_drp_reset : STD_LOGIC;
  signal sync_drp_reset_i_n_0 : STD_LOGIC;
  signal sync_rx_core_reset_i_n_0 : STD_LOGIC;
  signal sync_tx_core_reset_i_n_0 : STD_LOGIC;
  signal tx_chan_rst_done_r : STD_LOGIC;
  signal tx_pll_lock_sync : STD_LOGIC;
  signal \^tx_reset_done\ : STD_LOGIC;
  signal tx_reset_done_axi : STD_LOGIC;
  signal tx_reset_done_r0 : STD_LOGIC;
  signal tx_rst_gt_data_sync : STD_LOGIC;
  signal NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_cpllfbclklost_out_UNCONNECTED : STD_LOGIC;
  signal NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_rxoutclkfabric_out_UNCONNECTED : STD_LOGIC;
  signal NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_txoutclkfabric_out_UNCONNECTED : STD_LOGIC;
  signal NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_txoutclkpcs_out_UNCONNECTED : STD_LOGIC;
  signal NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_rxchariscomma_out_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Jesd204_microblaze_jesd204_phy_0_0_gt : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Jesd204_microblaze_jesd204_phy_0_0_gt : label is "Jesd204_microblaze_jesd204_phy_0_0_gt,gtwizard_v3_6_5,{protocol_file=JESD204}";
begin
  gt0_cplllock_out <= \^gt0_cplllock_out\;
  gt0_rxresetdone_out <= \^gt0_rxresetdone_out\;
  gt0_txresetdone_out <= \^gt0_txresetdone_out\;
  rx_reset_done <= \^rx_reset_done\;
  tx_reset_done <= \^tx_reset_done\;
Jesd204_microblaze_jesd204_phy_0_0_gt: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt
     port map (
      DONT_RESET_ON_DATA_ERROR_IN => '0',
      GT0_DATA_VALID_IN => '1',
      GT0_QPLLOUTCLK_IN => common0_qpll_clk_in,
      GT0_QPLLOUTREFCLK_IN => common0_qpll_refclk_in,
      GT_RX_FSM_RESET_DONE_OUT => gt_rxfsmdone,
      GT_TX_FSM_RESET_DONE_OUT => gt_txfsmdone,
      SOFT_RESET_RX_IN => gt_rxreset,
      SOFT_RESET_TX_IN => gt_txreset,
      SYSCLK_IN => drpclk,
      gt0_cpllfbclklost_out => NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_cpllfbclklost_out_UNCONNECTED,
      gt0_cplllock_out => \^gt0_cplllock_out\,
      gt0_cplllockdetclk_in => '0',
      gt0_cpllpd_in => cpllpd_i,
      gt0_cpllreset_in => '0',
      gt0_dmonitorout_out(14 downto 0) => gt0_dmonitorout_out(14 downto 0),
      gt0_drpaddr_in(8 downto 0) => gt0_drpaddr(8 downto 0),
      gt0_drpclk_in => drpclk,
      gt0_drpdi_in(15 downto 0) => gt0_drpdi(15 downto 0),
      gt0_drpdo_out(15 downto 0) => gt0_drpdo(15 downto 0),
      gt0_drpen_in => gt0_drpen,
      gt0_drprdy_out => gt0_drprdy,
      gt0_drpwe_in => gt0_drpwe,
      gt0_eyescandataerror_out => gt0_eyescandataerror_out,
      gt0_eyescanreset_in => gt0_eyescanreset_in,
      gt0_eyescantrigger_in => gt0_eyescantrigger_in,
      gt0_gthrxn_in => rxn_in(0),
      gt0_gthrxp_in => rxp_in(0),
      gt0_gthtxn_out => txn_out(0),
      gt0_gthtxp_out => txp_out(0),
      gt0_gtnorthrefclk0_in => '0',
      gt0_gtnorthrefclk1_in => '0',
      gt0_gtrefclk0_in => cpll_refclk,
      gt0_gtrefclk1_in => '0',
      gt0_gtrxreset_in => rx_rst_gt_data_sync,
      gt0_gtsouthrefclk0_in => '0',
      gt0_gtsouthrefclk1_in => '0',
      gt0_gttxreset_in => tx_rst_gt_data_sync,
      gt0_loopback_in(2 downto 0) => gt0_loopback_in(2 downto 0),
      gt0_rxbufreset_in => gt0_rxbufreset_in,
      gt0_rxbufstatus_out(2 downto 0) => gt0_rxbufstatus_out(2 downto 0),
      gt0_rxbyteisaligned_out => gt0_rxbyteisaligned_out,
      gt0_rxbyterealign_out => gt0_rxbyterealign_out,
      gt0_rxcdrhold_in => gt0_rxcdrhold_in,
      gt0_rxchariscomma_out(3 downto 0) => NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_rxchariscomma_out_UNCONNECTED(3 downto 0),
      gt0_rxcharisk_out(3 downto 0) => gt0_rxcharisk(3 downto 0),
      gt0_rxcommadet_out => gt0_rxcommadet_out,
      gt0_rxdata_out(31 downto 0) => gt0_rxdata(31 downto 0),
      gt0_rxdfelpmreset_in => gt0_rxdfelpmreset_in,
      gt0_rxdisperr_out(3 downto 0) => gt0_rxdisperr(3 downto 0),
      gt0_rxlpmen_in => gt0_rxlpmen_in,
      gt0_rxmcommaalignen_in => rxencommaalign,
      gt0_rxmonitorout_out(6 downto 0) => gt0_rxmonitorout_out(6 downto 0),
      gt0_rxmonitorsel_in(1 downto 0) => gt0_rxmonitorsel_in(1 downto 0),
      gt0_rxnotintable_out(3 downto 0) => gt0_rxnotintable(3 downto 0),
      gt0_rxoutclk_out => rxoutclk,
      gt0_rxoutclkfabric_out => NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_rxoutclkfabric_out_UNCONNECTED,
      gt0_rxpcommaalignen_in => rxencommaalign,
      gt0_rxpcsreset_in => gt0_rxpcsreset_in,
      gt0_rxpd_in(1 downto 0) => gt0_rxpd_in(1 downto 0),
      gt0_rxpmareset_in => gt0_rxpmareset_in,
      gt0_rxpolarity_in => gt0_rxpolarity_in,
      gt0_rxprbscntreset_in => gt0_rxprbscntreset_in,
      gt0_rxprbserr_out => gt0_rxprbserr_out,
      gt0_rxprbssel_in(2 downto 0) => gt0_rxprbssel_in(2 downto 0),
      gt0_rxresetdone_out => \^gt0_rxresetdone_out\,
      gt0_rxstatus_out(2 downto 0) => gt0_rxstatus_out(2 downto 0),
      gt0_rxsysclksel_in(1 downto 0) => gt0_rxsysclksel_i(1 downto 0),
      gt0_rxuserrdy_in => '1',
      gt0_rxusrclk2_in => rx_core_clk,
      gt0_rxusrclk_in => rx_core_clk,
      gt0_txbufstatus_out(1 downto 0) => gt0_txbufstatus_out(1 downto 0),
      gt0_txcharisk_in(3 downto 0) => gt0_txcharisk(3 downto 0),
      gt0_txdata_in(31 downto 0) => gt0_txdata(31 downto 0),
      gt0_txdiffctrl_in(3 downto 0) => gt0_txdiffctrl_in(3 downto 0),
      gt0_txinhibit_in => gt0_txinhibit_in,
      gt0_txoutclk_out => txoutclk,
      gt0_txoutclkfabric_out => NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_txoutclkfabric_out_UNCONNECTED,
      gt0_txoutclkpcs_out => NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_txoutclkpcs_out_UNCONNECTED,
      gt0_txpcsreset_in => gt0_txpcsreset_in,
      gt0_txpd_in(1 downto 0) => gt0_txpd_in(1 downto 0),
      gt0_txpmareset_in => gt0_txpmareset_in,
      gt0_txpolarity_in => gt0_txpolarity_in,
      gt0_txpostcursor_in(4 downto 0) => gt0_txpostcursor_in(4 downto 0),
      gt0_txprbsforceerr_in => gt0_txprbsforceerr_in,
      gt0_txprbssel_in(2 downto 0) => gt_prbssel(2 downto 0),
      gt0_txprecursor_in(4 downto 0) => gt0_txprecursor_in(4 downto 0),
      gt0_txresetdone_out => \^gt0_txresetdone_out\,
      gt0_txsysclksel_in(1 downto 0) => gt0_txsysclksel_i(1 downto 0),
      gt0_txuserrdy_in => '1',
      gt0_txusrclk2_in => tx_core_clk,
      gt0_txusrclk_in => tx_core_clk
    );
cpll_lock_r_reg: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => '1',
      D => \^gt0_cplllock_out\,
      Q => cpll_lock_r,
      R => '0'
    );
gt_rxreset_reg: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => '1',
      D => gt_rxreset0,
      Q => gt_rxreset,
      R => '0'
    );
gt_txreset_reg: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => '1',
      D => gt_txreset0,
      Q => gt_txreset,
      R => '0'
    );
phyCoreCtrlInterface_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface
     port map (
      D(15 downto 0) => gt0_drpdo(15 downto 0),
      Q(8 downto 0) => gt0_drpaddr(8 downto 0),
      SR(0) => sync_tx_core_reset_i_n_0,
      \arststages_ff_reg[4]\ => data_in0,
      data_in => data_in00_out,
      data_out => s_drp_reset,
      data_sync_reg1(15 downto 0) => gt0_drpdi(15 downto 0),
      data_sync_reg1_0(4 downto 0) => gt0_txpostcursor_in(4 downto 0),
      data_sync_reg1_1(4 downto 0) => gt0_txprecursor_in(4 downto 0),
      data_sync_reg_gsr => sync_rx_core_reset_i_n_0,
      data_sync_reg_gsr_0(0) => sync_drp_reset_i_n_0,
      data_sync_reg_gsr_1 => cpll_lock_axi,
      data_sync_reg_gsr_2 => rx_reset_done_axi,
      data_sync_reg_gsr_3 => tx_reset_done_axi,
      drpclk => drpclk,
      gt0_cpllpd_in => cpllpd_i,
      gt0_drpen_in => gt0_drpen,
      gt0_drprdy_out => gt0_drprdy,
      gt0_drpwe_in => gt0_drpwe,
      gt0_loopback_in(2 downto 0) => gt0_loopback_in(2 downto 0),
      gt0_rxdfelpmreset_in => gt0_rxdfelpmreset_in,
      gt0_rxlpmen_in => gt0_rxlpmen_in,
      gt0_rxpd_in(1 downto 0) => gt0_rxpd_in(1 downto 0),
      gt0_rxpolarity_in => gt0_rxpolarity_in,
      gt0_rxsysclksel_in(1 downto 0) => gt0_rxsysclksel_i(1 downto 0),
      gt0_txinhibit_in => gt0_txinhibit_in,
      gt0_txpd_in(1 downto 0) => gt0_txpd_in(1 downto 0),
      gt0_txpolarity_in => gt0_txpolarity_in,
      gt0_txsysclksel_in(1 downto 0) => gt0_txsysclksel_i(1 downto 0),
      rx_core_clk => rx_core_clk,
      rx_sys_reset => rx_sys_reset,
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(9 downto 0) => s_axi_araddr(9 downto 0),
      s_axi_aresetn => s_axi_aresetn,
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(9 downto 0) => s_axi_awaddr(9 downto 0),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bresp(0) => s_axi_bresp(0),
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(24 downto 0) => s_axi_rdata(24 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rresp(0) => s_axi_rresp(0),
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(17 downto 0) => s_axi_wdata(17 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wvalid => s_axi_wvalid,
      \slv_rdata_reg[3]\(3 downto 0) => gt0_txdiffctrl_in(3 downto 0),
      tx_core_clk => tx_core_clk,
      tx_sys_reset => tx_sys_reset
    );
rx_chan_rst_done_r_reg: unisim.vcomponents.FDRE
     port map (
      C => rx_core_clk,
      CE => '1',
      D => \^gt0_rxresetdone_out\,
      Q => rx_chan_rst_done_r,
      R => '0'
    );
rx_pll_lock_i_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => cpll_lock_sync,
      Q => rx_pll_lock_i_reg_n_0,
      R => '0'
    );
rx_reset_done_r_reg: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => '1',
      D => rx_reset_done_r0,
      Q => \^rx_reset_done\,
      R => '0'
    );
sync_cpll_lock: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized0\
     port map (
      O54 => cpll_lock_r,
      data_out => cpll_lock_sync,
      s_axi_aclk => s_axi_aclk
    );
sync_cpll_lock_axi_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__7\
     port map (
      data_out => cpll_lock_axi,
      gt0_cplllock_out => \^gt0_cplllock_out\,
      s_axi_aclk => s_axi_aclk
    );
sync_drp_reset_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__8\
     port map (
      data_out => s_drp_reset,
      \drp_read_data_reg[0]\(0) => sync_drp_reset_i_n_0,
      drpclk => drpclk,
      s_axi_aresetn => s_axi_aresetn
    );
sync_rx_chan_rst_done: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized4\
     port map (
      GT_RX_FSM_RESET_DONE_OUT => gt_rxfsmdone,
      data_in => rx_chan_rst_done_r,
      drpclk => drpclk,
      rx_reset_done_r0 => rx_reset_done_r0
    );
sync_rx_core_reset_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__9\
     port map (
      clk2_ready_reg => sync_rx_core_reset_i_n_0,
      rx_core_clk => rx_core_clk,
      s_axi_aresetn => s_axi_aresetn
    );
sync_rx_pll_lock: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized2\
     port map (
      data_in => rx_pll_lock_i_reg_n_0,
      data_out => rx_pll_lock_sync,
      drpclk => drpclk
    );
sync_rx_reset_all: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__1\
     port map (
      GT_RX_FSM_RESET_DONE_OUT => gt_rxfsmdone,
      data_in => data_in0,
      data_out => rx_pll_lock_sync,
      drpclk => drpclk,
      gt_rxreset0 => gt_rxreset0
    );
sync_rx_reset_data: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__2\
     port map (
      data_out => rx_rst_gt_data_sync,
      drpclk => drpclk,
      rx_reset_gt => rx_reset_gt
    );
sync_rx_reset_done_axi_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__10\
     port map (
      data_out => rx_reset_done_axi,
      rx_reset_done => \^rx_reset_done\,
      s_axi_aclk => s_axi_aclk
    );
sync_tx_chan_rst_done: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized3\
     port map (
      GT_TX_FSM_RESET_DONE_OUT => gt_txfsmdone,
      data_in => tx_chan_rst_done_r,
      drpclk => drpclk,
      tx_reset_done_r0 => tx_reset_done_r0
    );
sync_tx_core_reset_i: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__11\
     port map (
      SR(0) => sync_tx_core_reset_i_n_0,
      s_axi_aresetn => s_axi_aresetn,
      tx_core_clk => tx_core_clk
    );
sync_tx_pll_lock: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized1\
     port map (
      data_in => rx_pll_lock_i_reg_n_0,
      data_out => tx_pll_lock_sync,
      drpclk => drpclk
    );
sync_tx_reset_all: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__3\
     port map (
      GT_TX_FSM_RESET_DONE_OUT => gt_txfsmdone,
      data_in => data_in00_out,
      data_out => tx_pll_lock_sync,
      drpclk => drpclk,
      gt_txreset0 => gt_txreset0
    );
sync_tx_reset_data: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_sync_block
     port map (
      data_out => tx_rst_gt_data_sync,
      drpclk => drpclk,
      tx_reset_gt => tx_reset_gt
    );
sync_tx_reset_done_axi_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync
     port map (
      data_out => tx_reset_done_axi,
      s_axi_aclk => s_axi_aclk,
      tx_reset_done => \^tx_reset_done\
    );
tx_chan_rst_done_r_reg: unisim.vcomponents.FDRE
     port map (
      C => tx_core_clk,
      CE => '1',
      D => \^gt0_txresetdone_out\,
      Q => tx_chan_rst_done_r,
      R => '0'
    );
tx_reset_done_r_reg: unisim.vcomponents.FDRE
     port map (
      C => drpclk,
      CE => '1',
      D => tx_reset_done_r0,
      Q => \^tx_reset_done\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_support is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    gt0_txresetdone_out : out STD_LOGIC;
    gt0_rxresetdone_out : out STD_LOGIC;
    gt0_cplllock_out : out STD_LOGIC;
    gt0_eyescandataerror_out : out STD_LOGIC;
    gt0_eyescantrigger_in : in STD_LOGIC;
    gt0_eyescanreset_in : in STD_LOGIC;
    gt0_txprbsforceerr_in : in STD_LOGIC;
    gt0_txpcsreset_in : in STD_LOGIC;
    gt0_txpmareset_in : in STD_LOGIC;
    gt0_txbufstatus_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxcdrhold_in : in STD_LOGIC;
    gt0_rxprbserr_out : out STD_LOGIC;
    gt0_rxprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxprbscntreset_in : in STD_LOGIC;
    gt0_rxbufreset_in : in STD_LOGIC;
    gt0_rxbufstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxbyteisaligned_out : out STD_LOGIC;
    gt0_rxbyterealign_out : out STD_LOGIC;
    gt0_rxcommadet_out : out STD_LOGIC;
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );
    gt0_rxpcsreset_in : in STD_LOGIC;
    gt0_rxpmareset_in : in STD_LOGIC;
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    tx_sys_reset : in STD_LOGIC;
    rx_sys_reset : in STD_LOGIC;
    tx_reset_gt : in STD_LOGIC;
    rx_reset_gt : in STD_LOGIC;
    tx_reset_done : out STD_LOGIC;
    rx_reset_done : out STD_LOGIC;
    cpll_refclk : in STD_LOGIC;
    rxencommaalign : in STD_LOGIC;
    tx_core_clk : in STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rx_core_clk : in STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    drpclk : in STD_LOGIC;
    gt_prbssel : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_txcharisk : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_rxcharisk : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxdisperr : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxnotintable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rxn_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    rxp_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    txn_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    txp_out : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_support : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_support;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_support is
  signal \<const0>\ : STD_LOGIC;
  signal common0_qpll_clk_i : STD_LOGIC;
  signal common0_qpll_refclk_i : STD_LOGIC;
  signal \^s_axi_bresp\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \^s_axi_rdata\ : STD_LOGIC_VECTOR ( 25 downto 0 );
  signal \^s_axi_rresp\ : STD_LOGIC_VECTOR ( 1 to 1 );
begin
  s_axi_bresp(1) <= \^s_axi_bresp\(1);
  s_axi_bresp(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25 downto 24) <= \^s_axi_rdata\(25 downto 24);
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22 downto 0) <= \^s_axi_rdata\(22 downto 0);
  s_axi_rresp(1) <= \^s_axi_rresp\(1);
  s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
jesd204_phy_block_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_block
     port map (
      common0_qpll_clk_in => common0_qpll_clk_i,
      common0_qpll_refclk_in => common0_qpll_refclk_i,
      cpll_refclk => cpll_refclk,
      drpclk => drpclk,
      gt0_cplllock_out => gt0_cplllock_out,
      gt0_dmonitorout_out(14 downto 0) => gt0_dmonitorout_out(14 downto 0),
      gt0_eyescandataerror_out => gt0_eyescandataerror_out,
      gt0_eyescanreset_in => gt0_eyescanreset_in,
      gt0_eyescantrigger_in => gt0_eyescantrigger_in,
      gt0_rxbufreset_in => gt0_rxbufreset_in,
      gt0_rxbufstatus_out(2 downto 0) => gt0_rxbufstatus_out(2 downto 0),
      gt0_rxbyteisaligned_out => gt0_rxbyteisaligned_out,
      gt0_rxbyterealign_out => gt0_rxbyterealign_out,
      gt0_rxcdrhold_in => gt0_rxcdrhold_in,
      gt0_rxcharisk(3 downto 0) => gt0_rxcharisk(3 downto 0),
      gt0_rxcommadet_out => gt0_rxcommadet_out,
      gt0_rxdata(31 downto 0) => gt0_rxdata(31 downto 0),
      gt0_rxdisperr(3 downto 0) => gt0_rxdisperr(3 downto 0),
      gt0_rxmonitorout_out(6 downto 0) => gt0_rxmonitorout_out(6 downto 0),
      gt0_rxmonitorsel_in(1 downto 0) => gt0_rxmonitorsel_in(1 downto 0),
      gt0_rxnotintable(3 downto 0) => gt0_rxnotintable(3 downto 0),
      gt0_rxpcsreset_in => gt0_rxpcsreset_in,
      gt0_rxpmareset_in => gt0_rxpmareset_in,
      gt0_rxprbscntreset_in => gt0_rxprbscntreset_in,
      gt0_rxprbserr_out => gt0_rxprbserr_out,
      gt0_rxprbssel_in(2 downto 0) => gt0_rxprbssel_in(2 downto 0),
      gt0_rxresetdone_out => gt0_rxresetdone_out,
      gt0_rxstatus_out(2 downto 0) => gt0_rxstatus_out(2 downto 0),
      gt0_txbufstatus_out(1 downto 0) => gt0_txbufstatus_out(1 downto 0),
      gt0_txcharisk(3 downto 0) => gt0_txcharisk(3 downto 0),
      gt0_txdata(31 downto 0) => gt0_txdata(31 downto 0),
      gt0_txpcsreset_in => gt0_txpcsreset_in,
      gt0_txpmareset_in => gt0_txpmareset_in,
      gt0_txprbsforceerr_in => gt0_txprbsforceerr_in,
      gt0_txresetdone_out => gt0_txresetdone_out,
      gt_prbssel(2 downto 0) => gt_prbssel(2 downto 0),
      rx_core_clk => rx_core_clk,
      rx_reset_done => rx_reset_done,
      rx_reset_gt => rx_reset_gt,
      rx_sys_reset => rx_sys_reset,
      rxencommaalign => rxencommaalign,
      rxn_in(0) => rxn_in(0),
      rxoutclk => rxoutclk,
      rxp_in(0) => rxp_in(0),
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(9 downto 0) => s_axi_araddr(11 downto 2),
      s_axi_aresetn => s_axi_aresetn,
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(9 downto 0) => s_axi_awaddr(11 downto 2),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bresp(0) => \^s_axi_bresp\(1),
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(24 downto 23) => \^s_axi_rdata\(25 downto 24),
      s_axi_rdata(22 downto 0) => \^s_axi_rdata\(22 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rresp(0) => \^s_axi_rresp\(1),
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(17 downto 16) => s_axi_wdata(31 downto 30),
      s_axi_wdata(15 downto 0) => s_axi_wdata(15 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wvalid => s_axi_wvalid,
      tx_core_clk => tx_core_clk,
      tx_reset_done => tx_reset_done,
      tx_reset_gt => tx_reset_gt,
      tx_sys_reset => tx_sys_reset,
      txn_out(0) => txn_out(0),
      txoutclk => txoutclk,
      txp_out(0) => txp_out(0)
    );
jesd204_phy_gt_common_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_gt_common_wrapper
     port map (
      common0_qpll_clk_in => common0_qpll_clk_i,
      common0_qpll_refclk_in => common0_qpll_refclk_i,
      cpll_refclk => cpll_refclk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    gt0_txresetdone_out : out STD_LOGIC;
    gt0_rxresetdone_out : out STD_LOGIC;
    gt0_cplllock_out : out STD_LOGIC;
    gt0_eyescandataerror_out : out STD_LOGIC;
    gt0_eyescantrigger_in : in STD_LOGIC;
    gt0_eyescanreset_in : in STD_LOGIC;
    gt0_txprbsforceerr_in : in STD_LOGIC;
    gt0_txpcsreset_in : in STD_LOGIC;
    gt0_txpmareset_in : in STD_LOGIC;
    gt0_txbufstatus_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_rxcdrhold_in : in STD_LOGIC;
    gt0_rxprbserr_out : out STD_LOGIC;
    gt0_rxprbssel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxprbscntreset_in : in STD_LOGIC;
    gt0_rxbufreset_in : in STD_LOGIC;
    gt0_rxbufstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxstatus_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_rxbyteisaligned_out : out STD_LOGIC;
    gt0_rxbyterealign_out : out STD_LOGIC;
    gt0_rxcommadet_out : out STD_LOGIC;
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );
    gt0_rxpcsreset_in : in STD_LOGIC;
    gt0_rxpmareset_in : in STD_LOGIC;
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    tx_sys_reset : in STD_LOGIC;
    rx_sys_reset : in STD_LOGIC;
    tx_reset_gt : in STD_LOGIC;
    rx_reset_gt : in STD_LOGIC;
    tx_reset_done : out STD_LOGIC;
    rx_reset_done : out STD_LOGIC;
    cpll_refclk : in STD_LOGIC;
    rxencommaalign : in STD_LOGIC;
    tx_core_clk : in STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rx_core_clk : in STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    drpclk : in STD_LOGIC;
    gt_prbssel : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gt0_txdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_txcharisk : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gt0_rxcharisk : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxdisperr : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_rxnotintable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rxn_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    rxp_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    txn_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    txp_out : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "jesd204_phy_v3_2_1,Vivado 2016.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute downgradeipidentifiedwarnings of inst : label is "yes";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Jesd204_microblaze_jesd204_phy_0_0_support
     port map (
      cpll_refclk => cpll_refclk,
      drpclk => drpclk,
      gt0_cplllock_out => gt0_cplllock_out,
      gt0_dmonitorout_out(14 downto 0) => gt0_dmonitorout_out(14 downto 0),
      gt0_eyescandataerror_out => gt0_eyescandataerror_out,
      gt0_eyescanreset_in => gt0_eyescanreset_in,
      gt0_eyescantrigger_in => gt0_eyescantrigger_in,
      gt0_rxbufreset_in => gt0_rxbufreset_in,
      gt0_rxbufstatus_out(2 downto 0) => gt0_rxbufstatus_out(2 downto 0),
      gt0_rxbyteisaligned_out => gt0_rxbyteisaligned_out,
      gt0_rxbyterealign_out => gt0_rxbyterealign_out,
      gt0_rxcdrhold_in => gt0_rxcdrhold_in,
      gt0_rxcharisk(3 downto 0) => gt0_rxcharisk(3 downto 0),
      gt0_rxcommadet_out => gt0_rxcommadet_out,
      gt0_rxdata(31 downto 0) => gt0_rxdata(31 downto 0),
      gt0_rxdisperr(3 downto 0) => gt0_rxdisperr(3 downto 0),
      gt0_rxmonitorout_out(6 downto 0) => gt0_rxmonitorout_out(6 downto 0),
      gt0_rxmonitorsel_in(1 downto 0) => gt0_rxmonitorsel_in(1 downto 0),
      gt0_rxnotintable(3 downto 0) => gt0_rxnotintable(3 downto 0),
      gt0_rxpcsreset_in => gt0_rxpcsreset_in,
      gt0_rxpmareset_in => gt0_rxpmareset_in,
      gt0_rxprbscntreset_in => gt0_rxprbscntreset_in,
      gt0_rxprbserr_out => gt0_rxprbserr_out,
      gt0_rxprbssel_in(2 downto 0) => gt0_rxprbssel_in(2 downto 0),
      gt0_rxresetdone_out => gt0_rxresetdone_out,
      gt0_rxstatus_out(2 downto 0) => gt0_rxstatus_out(2 downto 0),
      gt0_txbufstatus_out(1 downto 0) => gt0_txbufstatus_out(1 downto 0),
      gt0_txcharisk(3 downto 0) => gt0_txcharisk(3 downto 0),
      gt0_txdata(31 downto 0) => gt0_txdata(31 downto 0),
      gt0_txpcsreset_in => gt0_txpcsreset_in,
      gt0_txpmareset_in => gt0_txpmareset_in,
      gt0_txprbsforceerr_in => gt0_txprbsforceerr_in,
      gt0_txresetdone_out => gt0_txresetdone_out,
      gt_prbssel(2 downto 0) => gt_prbssel(2 downto 0),
      rx_core_clk => rx_core_clk,
      rx_reset_done => rx_reset_done,
      rx_reset_gt => rx_reset_gt,
      rx_sys_reset => rx_sys_reset,
      rxencommaalign => rxencommaalign,
      rxn_in(0) => rxn_in(0),
      rxoutclk => rxoutclk,
      rxp_in(0) => rxp_in(0),
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(11 downto 0) => s_axi_araddr(11 downto 0),
      s_axi_aresetn => s_axi_aresetn,
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(11 downto 0) => s_axi_awaddr(11 downto 0),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bresp(1 downto 0) => s_axi_bresp(1 downto 0),
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rresp(1 downto 0) => s_axi_rresp(1 downto 0),
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wvalid => s_axi_wvalid,
      tx_core_clk => tx_core_clk,
      tx_reset_done => tx_reset_done,
      tx_reset_gt => tx_reset_gt,
      tx_sys_reset => tx_sys_reset,
      txn_out(0) => txn_out(0),
      txoutclk => txoutclk,
      txp_out(0) => txp_out(0)
    );
end STRUCTURE;
