----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.01.2018 20:20:10
-- Design Name: 
-- Module Name: tb_adc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_all is
--  Port ( );
end tb_all;

architecture Behavioral of tb_all is

-- component declaration for the unit under test (uut)


component spi_wrapper is
generic (
  START_ADDR      : std_logic_vector(27 downto 0) := x"0000000";
  STOP_ADDR       : std_logic_vector(27 downto 0) := x"00000FF"
);
port (
   rst           : in std_logic;
   clk           : in std_logic;
   in_cmd_val    : in std_logic;
   in_cmd        : in std_logic_vector(63 downto 0);
   out_cmd_val   : out std_logic;
   out_cmd       : out std_logic_vector(63 downto 0);
  -- spi_clk_out   : out std_logic;
   -- External Connections
   spi_sclk_phy  : out std_logic;
   spi_sdo_phy   : out std_logic; 
   spi_sdi_phy   : in  std_logic_vector(2 downto 0);
   spi_csn_phy   : out std_logic_vector(5 downto 0);
   spi_reset_phy : out std_logic_vector(3 downto 0)
);
end component;



--declare inputs and initialize them to zero.
signal clk            : std_logic := '0';
--signal sclk           : std_logic := '0';
--signal clk_reg        : std_logic := '0';
signal reset          : std_logic := '0';
--signal ena            : std_logic := '0';
--signal trig_clksel0   : std_logic := '0'; 
signal in_cmd_val  : std_logic := '0';
signal in_cmd     : std_logic_vector(64-1 downto 0):= (others => '0');

--declare outputs
signal out_cmd_val : std_logic := '0';
signal out_cmd     : std_logic_vector(64-1 downto 0):= (others => '0');
signal spi_sdi     : std_logic_vector(2 downto 0):= (others => '0');
--signal trig_n_cs   : std_logic := '0';
--signal trig_sclk   : std_logic := '0';
--signal trig_sdo    : std_logic := '0';
--signal mon_n_reset : std_logic := '0';
--signal spi_n_oe    : std_logic := '0';
--signal spi_sdi     : std_logic := '0';

-- define the period of clock here.
-- It's recommended to use CAPITAL letters to define constants.
constant CLK_PERIOD : time := 6.4 ns;   --156.25MHz is 6.4ns 
constant SCLK_PERIOD : time := 12.8 ns;   --370MHz is 2.7001ns
--constant CLK_REG_PERIOD : time := 6.4 ns;


begin

    -- instantiate the unit under test (uut)
   uut : spi_wrapper port map (
       rst            =>reset,
      clk             => clk,
     --serial_clk       => sclk,
    -- sclk_ext         => sclk0,
 
    -- clk_cmd         => clk,
     in_cmd_val      =>in_cmd_val, 
     in_cmd          =>in_cmd,
     out_cmd_val     =>out_cmd_val,
     out_cmd         =>out_cmd,
   
     -- SPI control
    
     spi_csn_phy        => open,
     spi_sclk_phy        => open,
     spi_sdo_phy         => open,
     spi_sdi_phy          => spi_sdi,
     spi_reset_phy      => open
       
     
        );      

   -- Clock process definitions( clock with 50% duty cycle is generated here.
   Clk_process :process
   begin
        clk <= '0';
        wait for CLK_PERIOD/2;  --for half of clock period clk stays at '0'.
        clk <= '1';
        wait for CLK_PERIOD/2;  --for next half of clock period clk stays at '1'.
   end process;
   

  
   -- Stimulus process, Apply inputs here.
  simulation: process
   begin 
        wait for CLK_PERIOD*10;       
        reset <= '1';    --wait for 10 clock cycles.
        wait for CLK_PERIOD*10;
        reset <= '0'; 
      --  ena <='1';
        wait;
  end process;


end Behavioral;