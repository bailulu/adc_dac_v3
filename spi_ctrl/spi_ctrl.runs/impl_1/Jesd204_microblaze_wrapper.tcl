proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set ACTIVE_STEP init_design
set rc [catch {
  create_msg_db init_design.pb
  set_property design_mode GateLvl [current_fileset]
  set_param project.singleFileAddWarning.threshold 0
  set_property webtalk.parent_dir /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.cache/wt [current_project]
  set_property parent.project_path /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.xpr [current_project]
  set_property ip_output_repo /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.cache/ip [current_project]
  set_property ip_cache_permissions {read write} [current_project]
  set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.runs/synth_1/Jesd204_microblaze_wrapper.dcp
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_0/Jesd204_microblaze_microblaze_0_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_0/Jesd204_microblaze_microblaze_0_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_dlmb_v10_0/Jesd204_microblaze_dlmb_v10_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_dlmb_v10_0/Jesd204_microblaze_dlmb_v10_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ilmb_v10_0/Jesd204_microblaze_ilmb_v10_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ilmb_v10_0/Jesd204_microblaze_ilmb_v10_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_dlmb_bram_if_cntlr_0/Jesd204_microblaze_dlmb_bram_if_cntlr_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_dlmb_bram_if_cntlr_0/Jesd204_microblaze_dlmb_bram_if_cntlr_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ilmb_bram_if_cntlr_0/Jesd204_microblaze_ilmb_bram_if_cntlr_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ilmb_bram_if_cntlr_0/Jesd204_microblaze_ilmb_bram_if_cntlr_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmb_bram_0/Jesd204_microblaze_lmb_bram_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmb_bram_0/Jesd204_microblaze_lmb_bram_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_axi_intc_0/Jesd204_microblaze_microblaze_0_axi_intc_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_axi_intc_0/Jesd204_microblaze_microblaze_0_axi_intc_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_xlconcat_0/Jesd204_microblaze_microblaze_0_xlconcat_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_xlconcat_0/Jesd204_microblaze_microblaze_0_xlconcat_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_mdm_1_0/Jesd204_microblaze_mdm_1_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_mdm_1_0/Jesd204_microblaze_mdm_1_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_clk_wiz_1_0/Jesd204_microblaze_clk_wiz_1_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_clk_wiz_1_0/Jesd204_microblaze_clk_wiz_1_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rst_clk_wiz_1_100M_0/Jesd204_microblaze_rst_clk_wiz_1_100M_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rst_clk_wiz_1_100M_0/Jesd204_microblaze_rst_clk_wiz_1_100M_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_phy_0_0/Jesd204_microblaze_jesd204_phy_0_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_phy_0_0/Jesd204_microblaze_jesd204_phy_0_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmk_sysref_0/Jesd204_microblaze_lmk_sysref_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmk_sysref_0/Jesd204_microblaze_lmk_sysref_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmk_sysref_buf_0/Jesd204_microblaze_lmk_sysref_buf_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmk_sysref_buf_0/Jesd204_microblaze_lmk_sysref_buf_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ref_clk_0/Jesd204_microblaze_ref_clk_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ref_clk_0/Jesd204_microblaze_ref_clk_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rx_sync_ds_0/Jesd204_microblaze_rx_sync_ds_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rx_sync_ds_0/Jesd204_microblaze_rx_sync_ds_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_tx_sync_ds_0/Jesd204_microblaze_tx_sync_ds_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_tx_sync_ds_0/Jesd204_microblaze_tx_sync_ds_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_xbar_0/Jesd204_microblaze_xbar_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_xbar_0/Jesd204_microblaze_xbar_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_xlconstant_1_0/Jesd204_microblaze_xlconstant_1_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_xlconstant_1_0/Jesd204_microblaze_xlconstant_1_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_uartlite_0_0/Jesd204_microblaze_axi_uartlite_0_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_uartlite_0_0/Jesd204_microblaze_axi_uartlite_0_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_gpio_0_0/Jesd204_microblaze_axi_gpio_0_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_gpio_0_0/Jesd204_microblaze_axi_gpio_0_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_quad_spi_0_0_1/Jesd204_microblaze_axi_quad_spi_0_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_quad_spi_0_0_1/Jesd204_microblaze_axi_quad_spi_0_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_1_0/Jesd204_microblaze_jesd204_1_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_1_0/Jesd204_microblaze_jesd204_1_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_2_0/Jesd204_microblaze_jesd204_2_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_2_0/Jesd204_microblaze_jesd204_2_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ila_0_0/Jesd204_microblaze_ila_0_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ila_0_0/Jesd204_microblaze_ila_0_0.dcp]
  add_files -quiet /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_auto_pc_0/Jesd204_microblaze_auto_pc_0.dcp
  set_property netlist_only true [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_auto_pc_0/Jesd204_microblaze_auto_pc_0.dcp]
  add_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/Jesd204_microblaze.bmm
  set_property SCOPED_TO_REF Jesd204_microblaze [get_files -all /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/Jesd204_microblaze.bmm]
  add_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_0/data/mb_bootloop_le.elf
  set_property SCOPED_TO_REF Jesd204_microblaze [get_files -all /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_0/data/mb_bootloop_le.elf]
  set_property SCOPED_TO_CELLS microblaze_0 [get_files -all /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_0/data/mb_bootloop_le.elf]
  read_xdc -ref Jesd204_microblaze_microblaze_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_0/Jesd204_microblaze_microblaze_0_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_0/Jesd204_microblaze_microblaze_0_0.xdc]
  read_xdc -ref Jesd204_microblaze_dlmb_v10_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_dlmb_v10_0/Jesd204_microblaze_dlmb_v10_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_dlmb_v10_0/Jesd204_microblaze_dlmb_v10_0.xdc]
  read_xdc -ref Jesd204_microblaze_ilmb_v10_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ilmb_v10_0/Jesd204_microblaze_ilmb_v10_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ilmb_v10_0/Jesd204_microblaze_ilmb_v10_0.xdc]
  read_xdc -ref Jesd204_microblaze_microblaze_0_axi_intc_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_axi_intc_0/Jesd204_microblaze_microblaze_0_axi_intc_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_axi_intc_0/Jesd204_microblaze_microblaze_0_axi_intc_0.xdc]
  read_xdc -ref Jesd204_microblaze_mdm_1_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_mdm_1_0/Jesd204_microblaze_mdm_1_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_mdm_1_0/Jesd204_microblaze_mdm_1_0.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_clk_wiz_1_0 -cells inst /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_clk_wiz_1_0/Jesd204_microblaze_clk_wiz_1_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_clk_wiz_1_0/Jesd204_microblaze_clk_wiz_1_0_board.xdc]
  read_xdc -ref Jesd204_microblaze_clk_wiz_1_0 -cells inst /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_clk_wiz_1_0/Jesd204_microblaze_clk_wiz_1_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_clk_wiz_1_0/Jesd204_microblaze_clk_wiz_1_0.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_rst_clk_wiz_1_100M_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rst_clk_wiz_1_100M_0/Jesd204_microblaze_rst_clk_wiz_1_100M_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rst_clk_wiz_1_100M_0/Jesd204_microblaze_rst_clk_wiz_1_100M_0_board.xdc]
  read_xdc -ref Jesd204_microblaze_rst_clk_wiz_1_100M_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rst_clk_wiz_1_100M_0/Jesd204_microblaze_rst_clk_wiz_1_100M_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rst_clk_wiz_1_100M_0/Jesd204_microblaze_rst_clk_wiz_1_100M_0.xdc]
  read_xdc -ref Jesd204_microblaze_jesd204_phy_0_0_gt -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_phy_0_0/ip_0/Jesd204_microblaze_jesd204_phy_0_0_gt.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_phy_0_0/ip_0/Jesd204_microblaze_jesd204_phy_0_0_gt.xdc]
  read_xdc -ref Jesd204_microblaze_jesd204_phy_0_0 -cells inst /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_phy_0_0/synth/Jesd204_microblaze_jesd204_phy_0_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_phy_0_0/synth/Jesd204_microblaze_jesd204_phy_0_0.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_lmk_sysref_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmk_sysref_0/Jesd204_microblaze_lmk_sysref_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmk_sysref_0/Jesd204_microblaze_lmk_sysref_0_board.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_lmk_sysref_buf_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmk_sysref_buf_0/Jesd204_microblaze_lmk_sysref_buf_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_lmk_sysref_buf_0/Jesd204_microblaze_lmk_sysref_buf_0_board.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_ref_clk_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ref_clk_0/Jesd204_microblaze_ref_clk_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ref_clk_0/Jesd204_microblaze_ref_clk_0_board.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_rx_sync_ds_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rx_sync_ds_0/Jesd204_microblaze_rx_sync_ds_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_rx_sync_ds_0/Jesd204_microblaze_rx_sync_ds_0_board.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_tx_sync_ds_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_tx_sync_ds_0/Jesd204_microblaze_tx_sync_ds_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_tx_sync_ds_0/Jesd204_microblaze_tx_sync_ds_0_board.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_axi_uartlite_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_uartlite_0_0/Jesd204_microblaze_axi_uartlite_0_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_uartlite_0_0/Jesd204_microblaze_axi_uartlite_0_0_board.xdc]
  read_xdc -ref Jesd204_microblaze_axi_uartlite_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_uartlite_0_0/Jesd204_microblaze_axi_uartlite_0_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_uartlite_0_0/Jesd204_microblaze_axi_uartlite_0_0.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_axi_gpio_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_gpio_0_0/Jesd204_microblaze_axi_gpio_0_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_gpio_0_0/Jesd204_microblaze_axi_gpio_0_0_board.xdc]
  read_xdc -ref Jesd204_microblaze_axi_gpio_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_gpio_0_0/Jesd204_microblaze_axi_gpio_0_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_gpio_0_0/Jesd204_microblaze_axi_gpio_0_0.xdc]
  read_xdc -prop_thru_buffers -ref Jesd204_microblaze_axi_quad_spi_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_quad_spi_0_0_1/Jesd204_microblaze_axi_quad_spi_0_0_board.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_quad_spi_0_0_1/Jesd204_microblaze_axi_quad_spi_0_0_board.xdc]
  read_xdc -ref Jesd204_microblaze_axi_quad_spi_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_quad_spi_0_0_1/Jesd204_microblaze_axi_quad_spi_0_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_quad_spi_0_0_1/Jesd204_microblaze_axi_quad_spi_0_0.xdc]
  read_xdc -ref Jesd204_microblaze_jesd204_1_0 -cells inst /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_1_0/synth/Jesd204_microblaze_jesd204_1_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_1_0/synth/Jesd204_microblaze_jesd204_1_0.xdc]
  read_xdc -ref Jesd204_microblaze_jesd204_2_0 -cells inst /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_2_0/synth/Jesd204_microblaze_jesd204_2_0.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_2_0/synth/Jesd204_microblaze_jesd204_2_0.xdc]
  read_xdc -ref Jesd204_microblaze_ila_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ila_0_0/ila_v6_2/constraints/ila.xdc
  set_property processing_order EARLY [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_ila_0_0/ila_v6_2/constraints/ila.xdc]
  read_xdc /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/constrs_1/new/pin_allocated_constrains.xdc
  read_xdc -ref Jesd204_microblaze_microblaze_0_axi_intc_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_axi_intc_0/Jesd204_microblaze_microblaze_0_axi_intc_0_clocks.xdc
  set_property processing_order LATE [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_microblaze_0_axi_intc_0/Jesd204_microblaze_microblaze_0_axi_intc_0_clocks.xdc]
  read_xdc -ref Jesd204_microblaze_jesd204_phy_0_0 -cells inst /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_phy_0_0/synth/Jesd204_microblaze_jesd204_phy_0_0_clocks.xdc
  set_property processing_order LATE [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_jesd204_phy_0_0/synth/Jesd204_microblaze_jesd204_phy_0_0_clocks.xdc]
  read_xdc -ref Jesd204_microblaze_axi_quad_spi_0_0 -cells U0 /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_quad_spi_0_0_1/Jesd204_microblaze_axi_quad_spi_0_0_clocks.xdc
  set_property processing_order LATE [get_files /home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/bd/Jesd204_microblaze/ip/Jesd204_microblaze_axi_quad_spi_0_0_1/Jesd204_microblaze_axi_quad_spi_0_0_clocks.xdc]
  link_design -top Jesd204_microblaze_wrapper -part xc7vx690tffg1761-3
  write_hwdef -file Jesd204_microblaze_wrapper.hwdef
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
  unset ACTIVE_STEP 
}

start_step opt_design
set ACTIVE_STEP opt_design
set rc [catch {
  create_msg_db opt_design.pb
  opt_design 
  write_checkpoint -force Jesd204_microblaze_wrapper_opt.dcp
  catch { report_drc -file Jesd204_microblaze_wrapper_drc_opted.rpt }
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
  unset ACTIVE_STEP 
}

start_step place_design
set ACTIVE_STEP place_design
set rc [catch {
  create_msg_db place_design.pb
  implement_debug_core 
  place_design 
  write_checkpoint -force Jesd204_microblaze_wrapper_placed.dcp
  catch { report_io -file Jesd204_microblaze_wrapper_io_placed.rpt }
  catch { report_utilization -file Jesd204_microblaze_wrapper_utilization_placed.rpt -pb Jesd204_microblaze_wrapper_utilization_placed.pb }
  catch { report_control_sets -verbose -file Jesd204_microblaze_wrapper_control_sets_placed.rpt }
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
  unset ACTIVE_STEP 
}

start_step route_design
set ACTIVE_STEP route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force Jesd204_microblaze_wrapper_routed.dcp
  catch { report_drc -file Jesd204_microblaze_wrapper_drc_routed.rpt -pb Jesd204_microblaze_wrapper_drc_routed.pb -rpx Jesd204_microblaze_wrapper_drc_routed.rpx }
  catch { report_methodology -file Jesd204_microblaze_wrapper_methodology_drc_routed.rpt -rpx Jesd204_microblaze_wrapper_methodology_drc_routed.rpx }
  catch { report_timing_summary -warn_on_violation -max_paths 10 -file Jesd204_microblaze_wrapper_timing_summary_routed.rpt -rpx Jesd204_microblaze_wrapper_timing_summary_routed.rpx }
  catch { report_power -file Jesd204_microblaze_wrapper_power_routed.rpt -pb Jesd204_microblaze_wrapper_power_summary_routed.pb -rpx Jesd204_microblaze_wrapper_power_routed.rpx }
  catch { report_route_status -file Jesd204_microblaze_wrapper_route_status.rpt -pb Jesd204_microblaze_wrapper_route_status.pb }
  catch { report_clock_utilization -file Jesd204_microblaze_wrapper_clock_utilization_routed.rpt }
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  write_checkpoint -force Jesd204_microblaze_wrapper_routed_error.dcp
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
  unset ACTIVE_STEP 
}

start_step write_bitstream
set ACTIVE_STEP write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
  catch { write_mem_info -force Jesd204_microblaze_wrapper.mmi }
  catch { write_bmm -force Jesd204_microblaze_wrapper_bd.bmm }
  write_bitstream -force -no_partial_bitfile Jesd204_microblaze_wrapper.bit 
  catch { write_sysdef -hwdef Jesd204_microblaze_wrapper.hwdef -bitfile Jesd204_microblaze_wrapper.bit -meminfo Jesd204_microblaze_wrapper.mmi -file Jesd204_microblaze_wrapper.sysdef }
  catch {write_debug_probes -quiet -force debug_nets}
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
  unset ACTIVE_STEP 
}

