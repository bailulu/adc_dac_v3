-------------------------------------------------------------------------------------
-- DESCRIPTION
-- ===========
-- 
--- Conncetions to FMC144 SPI Devices:
--
-- ADC1  ADC16DX370     (20 MHz MAX SPI)
-- ADC2  ADC16DX370     (20 MHz MAX SPI)
-- DAC   DAC38J84  
-- PLL   LMK04828       (20 MHz MAX SPI)
-- MON   AMC7828
--
--  FPGA    FMC144
--  -------------
--  SCLK     ->   SCLK : ADC1, ADC2, LMK, DAC, MON  
--  SDO      ->   SDI  : ADC1, ADC2, LMK, DAC, MON
--  SDI(0)   <-   SDO  : ADC1, ADC2, MON
--  SDI(1)   <-   SDO  : DAC
--  SDI(2)   <-   SDO  : LMK
--
--  CS_N(0)  --> CS_N  : ADC1
--  CS_N(1)  --> CS_N  : ADC2
--  CS_N(2)  --> CS_N  : DAC0 
--  CS_N(3)  --> CS_N  : LMK 
--  CS_N(4)  --> CS_N  : MON 
--
--  RESET(0)    --> RESET   : LMK
--  RESET(1)    --> RESETn  : DAC
--  RESET(1)    --> RESETn  : MON 

-------------------------------------------------------------------------------------
 
-------------------------------------------------------------------------------------
-- LIBRARIES
-------------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    -- IEEE  
    --use ieee.numeric_std.all; 
    -- non-IEEE 
    use ieee.std_logic_unsigned.all;
    use ieee.std_logic_misc.all;
    use ieee.std_logic_arith.all; 

library unisim;
  use unisim.vcomponents.all;
-------------------------------------------------------------------------------------
-- ENTITY
-------------------------------------------------------------------------------------
entity spi_wrapper is
generic (
  START_ADDR       : std_logic_vector(27 downto 0) := x"0000000";
  STOP_ADDR        : std_logic_vector(27 downto 0) := x"00000FF"
);
port (
   rst           : in std_logic;
   clk           : in std_logic;
   in_cmd_val    : in std_logic;
   in_cmd        : in std_logic_vector(63 downto 0);
   out_cmd_val   : out std_logic;
   out_cmd       : out std_logic_vector(63 downto 0);
  -- spi_clk_out   : out std_logic;
   -- External Connections
   spi_sclk_phy  : out std_logic;
   spi_sdo_phy   : out std_logic; 
   spi_sdi_phy   : in  std_logic_vector(2 downto 0);
   spi_csn_phy   : out std_logic_vector(5 downto 0);
   spi_reset_phy : out std_logic_vector(3 downto 0)
);
end spi_wrapper;

-------------------------------------------------------------------------------------
-- ARCHITECTURE
-------------------------------------------------------------------------------------
architecture Behavioral of spi_wrapper is

----------------------------------------------------------------------------------------------------
-- Constant declaration
----------------------------------------------------------------------------------------------------
constant START_ADDR_ADC16DX370_CTRL0    : std_logic_vector(27 downto 0) := START_ADDR + x"0000000";
constant STOP_ADDR_ADC16DX370_CTRL0     : std_logic_vector(27 downto 0) := START_ADDR + x"00001FF"; 

--constant START_ADDR_ADC16DX370_CTRL1    : std_logic_vector(27 downto 0) := START_ADDR + x"0000200";
--constant STOP_ADDR_ADC16DX370_CTRL1     : std_logic_vector(27 downto 0) := START_ADDR + x"00002FF"; 

constant START_ADDR_DAC38J84_CTRL0      : std_logic_vector(27 downto 0) := START_ADDR + x"0000300";
constant STOP_ADDR_DAC38J84_CTRL0       : std_logic_vector(27 downto 0) := START_ADDR + x"00003FF";

constant START_ADDR_AMC7823_CTRL0       : std_logic_vector(27 downto 0) := START_ADDR + x"0000400";
constant STOP_ADDR_AMC7823_CTRL0        : std_logic_vector(27 downto 0) := START_ADDR + x"00005FF";  

constant START_ADDR_LMK04828_CTRL       : std_logic_vector(27 downto 0) := START_ADDR + x"0000600";
constant STOP_ADDR_LMK04828_CTRL        : std_logic_vector(27 downto 0) := START_ADDR + x"0000BFF"; 

--constant START_ADDR_AD9912_CTRL         : std_logic_vector(27 downto 0) := START_ADDR + x"0000C00";
--constant STOP_ADDR_AD9912_CTRL          : std_logic_vector(27 downto 0) := START_ADDR + x"00011FF"; 

----------------------------------------------------------------------------------------------------
--Signal declaration
----------------------------------------------------------------------------------------------------
-- spi interface
signal spi_sclk     :  std_logic_vector(5 downto 0);
signal spi_sdo      :  std_logic_vector(5 downto 0); 
signal spi_sdi      :  std_logic_vector(5 downto 0);
signal spi_csn      :  std_logic_vector(5 downto 0);
signal spi_reset    :  std_logic_vector(4 downto 0);
signal spi_n_oe     :  std_logic;
-- Serial clock
--signal sclk_prebuf  : std_logic;
--signal serial_clk0  : std_logic;
--signal serial_clk   : std_logic;
signal sclk_ext     : std_logic;
-- ADC1
signal out_cmd_val0 : std_logic;                          
signal out_cmd0     : std_logic_vector(63 downto 0);
-- ADC2
--signal out_cmd_val1 : std_logic;                          
--signal out_cmd1     : std_logic_vector(63 downto 0);
-- DAC1 
signal out_cmd_val2 : std_logic;                          
signal out_cmd2     : std_logic_vector(63 downto 0);
-- LMK (CLOCK) 
signal out_cmd_val3 : std_logic;                          
signal out_cmd3     : std_logic_vector(63 downto 0);
-- MON 
signal out_cmd_val4 : std_logic;                          
signal out_cmd4     : std_logic_vector(63 downto 0);
-- DDS 
--signal out_cmd_val5 : std_logic;                          
--signal out_cmd5     : std_logic_vector(63 downto 0);
-- debug
signal probe0       : std_logic_vector(127 downto 0);

--***********************************************************************************
begin
--***********************************************************************************

----------------------------------------------------------------------------------------------------
-- Command Output. Don't register because it will be combined with other output commands
----------------------------------------------------------------------------------------------------
--out_cmd_val    <= out_cmd_val5 or out_cmd_val4 or out_cmd_val3 or out_cmd_val2 or out_cmd_val1 or out_cmd_val0;
--out_cmd        <=     out_cmd5 or     out_cmd4 or     out_cmd3 or     out_cmd2 or     out_cmd1 or     out_cmd0; 

out_cmd_val    <= out_cmd_val4 or out_cmd_val3 or out_cmd_val2 or out_cmd_val0;
out_cmd        <= out_cmd4 or     out_cmd3 or     out_cmd2 or     out_cmd0; 


----------------------------------------------------------------------------------------------------
-- SPI interface controlling the FMC ADC0 
----------------------------------------------------------------------------------------------------

adc0: entity work.adc_ctrol_dataread
generic map
(
 -- START_ADDR     => START_ADDR_ADC16DX370_CTRL0,
 -- STOP_ADDR      => STOP_ADDR_ADC16DX370_CTRL0
  
    START_ADDR     => open,
    STOP_ADDR      => open
  
)
port map(
 
    reset             => rst,
    clk               => clk,
  -- sclk             : in  std_logic;
    clk_reg           => clk,
 --  ena              : in  std_logic;     -- external enable signal        
  
    out_cmd_val           => out_cmd_val0,
    out_cmd               => open,
  
  -- Serial Interface
    trig_n_cs       => spi_csn(0), 
    trig_sclk       => spi_sclk(0), 
    trig_sdo        => spi_sdo(0), 
    trig_clksel0    => spi_sdi(0) 
);


----------------------------------------------------------------------------------------------------
-- SPI interface controlling the  FMC144 DAC0
----------------------------------------------------------------------------------------------------

dac: entity work.dac_ctrol_dataread
generic map
(
  --START_ADDR      => START_ADDR_DAC38J84_CTRL0,
 -- STOP_ADDR       => STOP_ADDR_DAC38J84_CTRL0
  
  START_ADDR      => open,
    STOP_ADDR       => open
)
port map(
 
   reset            => rst,
   clk              => clk,
 -- sclk             : in  std_logic;
   clk_reg          => clk,
--  ena              : in  std_logic;     -- external enable signal        
 
   out_cmd_val           => out_cmd_val2,
   out_cmd               => open,
 
   trig_n_cs       => spi_csn(2), 
   trig_sclk       => spi_sclk(2), 
   trig_sdo        => spi_sdo(2), 
   trig_clksel0    => spi_sdi(2) 
);

----------------------------------------------------------------------------------------------------
-- SPI interface controlling the LMK04828
----------------------------------------------------------------------------------------------------
lmk04828: entity work.lmk04828_ctrol_dataread
generic map
(
   --START_ADDR            => START_ADDR_LMK04828_CTRL,
   --STOP_ADDR             => STOP_ADDR_LMK04828_CTRL
   
    START_ADDR            => open,
     STOP_ADDR             => open
)
port map   (
 
     reset            => rst,
     clk              => clk,
 -- sclk             : in  std_logic;
     clk_reg          => clk,
 --  ena              : in  std_logic;     -- external enable signal        
 
     out_cmd_val           => out_cmd_val3,
     out_cmd               => open,
 
  -- Serial Interface
     trig_n_cs              => spi_csn(3), 
     trig_sclk              => spi_sclk(3), 
     trig_sdo               => spi_sdo(3), 
     trig_clksel0           => spi_sdi(3) 
);

----------------------------------------------------------------------------------------------------
-- SPI interface controlling the FMC144 MONITOR
----------------------------------------------------------------------------------------------------
amc7823: entity work.amc7823_ctrl
generic map
(
 -- START_ADDR      => START_ADDR_AMC7823_CTRL0,
--  STOP_ADDR       => STOP_ADDR_AMC7823_CTRL0
  
  START_ADDR      => open,
    STOP_ADDR       => open
)
port map
(
   rst             => rst,
   clk             => clk,
   serial_clk      => sclk_ext,
   -- sclk_ext        : in  std_logic;
   -- Sequence interface
   init_ena        => '1',  --in
   init_done       => open, -- out
  
   -- Command Interface
   clk_cmd         => clk,
   in_cmd_val      => in_cmd_val,
   in_cmd          => in_cmd,
   out_cmd_val     => out_cmd_val4,
   out_cmd         => out_cmd4,
   in_cmd_busy     => open, -- out
  
  -- Direct control
   mon_n_reset     => spi_reset_phy(1), --out
   mon_n_int       => '1',
  
 -- Serial Interface
    spi_n_oe       => spi_n_oe,
    spi_n_cs       => spi_csn(4), 
    spi_sclk       => spi_sclk(4), 
    spi_sdo        => spi_sdo(4), 
    spi_sdi        => spi_sdi(4) 
);

----------------------------------------------------------------------------------------------------
-- SPI interface controlling the FMC144 DDS CHIP 
----------------------------------------------------------------------------------------------------
--spi_dds_ctrl0: 
--entity work.ad9912_ctrl
--generic map (
--  START_ADDR      => START_ADDR_AD9912_CTRL,
 -- STOP_ADDR       => STOP_ADDR_AD9912_CTRL
--)
--port map (
 -- rst             => rst,
--  clk             => serial_clk,
  -- Command Interface
 -- clk_cmd         => clk_cmd,
 -- in_cmd_val      => in_cmd_val,
 -- in_cmd          => in_cmd,
 -- out_cmd_val     => out_cmd_val5,
 -- out_cmd         => out_cmd5,
  -- Serial Interface
 -- trig_n_cs       => spi_csn(5), 
 -- trig_sclk       => spi_sclk(5), 
 -- trig_sdo        => spi_sdo(5), 
 -- trig_clksel0    => spi_sdi(5) 
--);


----------------------------------------------------------------------------------------------------
-- Generate serial clocks for SPI (max 6.66MHz, due to ...)
-- Known problem running LMK serial clock faster than about 10 MHz
-- 125 / 8  = 15.625 MHz 
-- 125/ 16 = 7.8125      
----------------------------------------------------------------------------------------------------
process (clk)
  -- Divide by 2^5 = 32, CLKmax = 32 x 6.66MHz
  variable clk_div : std_logic_vector(3 downto 0) := (others => '0');
begin
  if (rising_edge(clk)) then
    clk_div    := clk_div + '1';
    -- The slave samples the data on the rising edge of SCLK.
    -- therefore we make sure the external clock is slightly
    -- after the internal clock.
    sclk_ext <= clk_div(clk_div'length-1);
   -- sclk_prebuf <= sclk_ext;
  end if;
end process;

--bufg_sclk : bufg
--port map (
 -- i => sclk_prebuf,
 -- o => serial_clk
--);

--bufg_sclk0 : bufg
--port map (
--  i => sclk_ext,
 -- o => serial_clk0
--);

--spi_clk_out <= serial_clk;

----------------------------------------------------------------------
-- Connections to PINS
----------------------------------------------------------------------
-- SCLK
spi_sclk_phy <= spi_sclk(0) when spi_csn(0)  = '0' else 
               -- spi_sclk(1) when spi_csn(1)  = '0' else
                spi_sclk(2) when spi_csn(2)  = '0' else
                spi_sclk(3) when spi_csn(3)  = '0' else
                spi_sclk(4) when spi_csn(4)  = '0' else '0';
              --  spi_sclk(5) when spi_csn(5)  = '0' else '0'
                

-- SDO 
spi_sdo_phy <= spi_sdo(0) when spi_csn(0)  = '0' else 
               --spi_sdo(1) when spi_csn(1)  = '0' else
               spi_sdo(2) when spi_csn(2)  = '0' else
               spi_sdo(3) when spi_csn(3)  = '0' else
               spi_sdo(4) when spi_csn(4)  = '0' else '1';
              -- spi_sdo(5) when spi_csn(5)  = '0' else '1';

-- SDI
spi_sdi(0)    <= spi_sdi_phy(0);
--spi_sdi(1)    <= spi_sdi_phy(0);
spi_sdi(2)    <= spi_sdi_phy(1);
spi_sdi(3)    <= spi_sdi_phy(2);
spi_sdi(4)    <= spi_sdi_phy(0);
--spi_sdi(5)    <= spi_sdi_phy(0);

-- CS_N
spi_csn_phy(0) <= spi_csn(0); -- ADC1
--spi_csn_phy(1) <= spi_csn(1); -- ADC2
spi_csn_phy(2) <= spi_csn(2); -- DAC1
spi_csn_phy(3) <= spi_csn(3); -- LMK
spi_csn_phy(4) <= spi_csn(4); -- MON
--spi_csn_phy(5) <= spi_csn(5); -- DDS 

-- RESET
spi_reset_phy(0)   <= '0'; -- LMK RESET
--spi_reset_phy(1)   <= '1'; -- MON RESETn
spi_reset_phy(2)   <= '1'; -- DAC RESETn
spi_reset_phy(3)   <= '0'; -- DDS RESET

--***********************************************************************************
end architecture Behavioral;
--***********************************************************************************
