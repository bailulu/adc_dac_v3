----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/28/2018 11:11:54 PM
-- Design Name: 
-- Module Name: axi_master - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity axi_master is
generic (
  --  parameter  C_M_START_DATA_VALUE = 32'hAA000000,  -- // The master will start generating data from the C_M_START_DATA_VALUE value  

 --   parameter  C_M_TARGET_SLAVE_BASE_ADDR   = 32'h40000000,  --    // The master requires a target slave base address.  
 --// The master will initiate read and write transactions on the slave with base address specified here as a parameter.  
 
    C_M_AXI_ADDR_WIDTH   : integer := 32; 
    C_M_AXI_DATA_WIDTH   : integer := 32;   
    C_M_TRANSACTIONS_NUM : integer := 4    -- // Transaction number is the number of write   
  -- // and read transactions the master will perform as a part of this example memory test.  
);
Port ( 
  --user ports
  DATA_IN              : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
  ADDR_IN              : in  std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
  
  M_AXI_ACLK           : in  std_logic;
  M_AXI_ARESETN        : in  std_logic;

  M_AXI_AWADDR         : out std_logic_vector (C_M_AXI_ADDR_WIDTH-1 downto 0); 
  M_AXI_AWVALID        : out std_logic; -- Write address valid. This signal indicates that the master signaling valid write address and control information.  
  M_AXI_AWREADY        : in  std_logic; --Write address ready. This signal indicates that the slave is ready to accept an address and associated control signals.  
  M_AXI_WDATA          : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);  --Master Interface Write Data Channel ports. Write data (issued by master)  
 
  M_AXI_WSTRB          : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);   -- Write strobes. This signal indicates which byte lanes hold valid data. There is one write strobe bit for each eight bits of the write data bus.  
  M_AXI_WVALID         : out std_logic; -- Write valid. This signal indicates that valid write data and strobes are available.
  M_AXI_WREADY         : in  std_logic; -- Write ready. This signal indicates that the slave can accept the write data
 
  M_AXI_BRESP          : in  std_logic_vector (1 downto 0);       --Master Interface Write Response Channel ports. This signal indicates the status of the write transaction    
  M_AXI_BVALID         : in  std_logic; --Write response valid. This signal indicates that the channel is signaling a valid write response 
  M_AXI_BREADY         : out std_logic; -- Response ready. This signal indicates that the master can accept a write response.  
  M_AXI_ARADDR         : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0); --Interface Read Address Channel ports. Read address (issued by master)      
  M_AXI_ARVALID        : out std_logic; -- Read address valid. This signal indicates that the channel is signaling valid read address and control information.    
  M_AXI_ARREADY        : in  std_logic;  -- Read address ready.
  M_AXI_RDATA          : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);--Master Interface Read Data Channel ports. Read data (issued by slave) 
  M_AXI_RRESP          : in  std_logic_vector (1 downto 0);  --Read response. This signal indicates the status of the read transfer.     
  M_AXI_RVALID         : in  std_logic;    --Read valid. This signal indicates that the channel is signaling the required read data.
  M_AXI_RREADY         : out std_logic    --Read ready. This signal indicates that the master can accept the read data and response information.
);  


end axi_master;

architecture Behavioral of axi_master is



    signal     axi_awvalid  : std_logic;  
    signal     axi_wvalid   : std_logic;
    signal     axi_arvalid  : std_logic;  
    signal     axi_rready   : std_logic; 
    signal     axi_bready   : std_logic; 
   
    signal     axi_awaddr   : std_logic_vector (C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal     axi_wdata    : std_logic_vector (C_M_AXI_DATA_WIDTH-1 downto 0);
    signal     axi_araddr   : std_logic_vector (C_M_AXI_ADDR_WIDTH-1 downto 0);
 
    signal     axi_wstrb    : std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0) := (others => '0');

    signal    start_single_write : std_logic;   --A pulse to initiate a write transaction 
    signal    start_single_read  : std_logic;   --A pulse to initiate a read transaction  
 
   
begin
  -----------------------------------------------------------------------------------------
  --  write adress valid
  ----------------------------------------------------------------------------------------
	process(M_AXI_ACLK, M_AXI_ARESETN)
    begin    
        if M_AXI_ARESETN = '0' then
               axi_awvalid      <= '0';
          
        elsif rising_edge (M_AXI_ACLK) then
              if start_single_write='1' then
			     axi_awvalid      <= '1';
	          elsif M_AXI_AWREADY = '1' and axi_awvalid = '1' then
			     axi_awvalid      <= '0';
			  end if;
        end if;
    end process;
    
-----------------------------------------------------------------------------------------
--  write data valid
-----------------------------------------------------------------------------------------
	process(M_AXI_ACLK, M_AXI_ARESETN)
    begin    
        if M_AXI_ARESETN = '0' then
               axi_wvalid      <= '0';
          
        elsif rising_edge (M_AXI_ACLK) then
              if start_single_write='1' then
			     axi_wvalid      <= '1';
	          elsif M_AXI_WREADY = '1' and axi_wvalid = '1' then
			     axi_wvalid      <= '0';
			  end if;
        end if;
    end process;
    
    
-----------------------------------------------------------------------------------------
-- write response 
-----------------------------------------------------------------------------------------

   process(M_AXI_ACLK, M_AXI_ARESETN)
        begin    
            if M_AXI_ARESETN = '0' then
                   axi_bready      <= '0';
              
            elsif rising_edge (M_AXI_ACLK) then
                  if M_AXI_BVALID = '1' and axi_bready = '0' then   --accept/acknowledge bresp with axi_bready by the master when M_AXI_BVALID is asserted by slave  
                     axi_bready      <= '1';   
                  elsif axi_bready = '1' then
                     axi_bready      <= '0';   --deassert after one clock cycle   
                  else                                                                 
                     axi_bready <= axi_bready;   --retain the previous value                                            
                  end if;
            end if;
    end process;

 -----------------------------------------------------------------------------------------
 -- adress read valid
 ----------------------------------------------------------------------------------------

   process(M_AXI_ACLK, M_AXI_ARESETN)
        begin    
            if M_AXI_ARESETN = '0' then
                   axi_arvalid      <= '0';
              
            elsif rising_edge (M_AXI_ACLK) then
                  if start_single_read= '1' then   --RAddress accepted by interconnect/slave (issue of M_AXI_ARREADY by slave)   
                     axi_arvalid      <= '1';   
                  elsif M_AXI_ARREADY = '1' and axi_arvalid='1' then
                     axi_arvalid      <= '0';                                               
                  end if;
            end if;
    end process;

-----------------------------------------------------------------------------------------
 -- data read ready
 ----------------------------------------------------------------------------------------

   process(M_AXI_ACLK, M_AXI_ARESETN)
        begin    
            if M_AXI_ARESETN = '0' then
                   axi_rready      <= '0';
              
            elsif rising_edge (M_AXI_ACLK) then
                   if M_AXI_RVALID = '1' and axi_rready = '0' then   --accept/acknowledge bresp with axi_bready by the master when M_AXI_BVALID is asserted by slave  
                      axi_rready      <= '1';   
                   elsif axi_rready = '1' then
                      axi_rready      <= '0';   --deassert after one clock cycle   
                   else                                                                 
                      axi_rready <= axi_rready;   --retain the previous value                                            
                   end if;
            end if;
    end process;
    
    
         M_AXI_AWADDR         <= axi_awaddr;
         M_AXI_AWVALID        <= axi_awvalid;  
         M_AXI_WDATA          <= axi_wdata;    
         M_AXI_WSTRB          <= axi_wstrb;
         M_AXI_WVALID         <= axi_wvalid;       
         M_AXI_BREADY         <= axi_bready;
         M_AXI_ARADDR         <= axi_araddr;     
         M_AXI_ARVALID        <= axi_arvalid;
         M_AXI_RREADY         <= axi_rready;
        
    
  end Behavioral;
         