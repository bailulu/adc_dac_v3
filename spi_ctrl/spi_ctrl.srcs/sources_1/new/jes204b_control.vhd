----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/23/2018 11:00:53 PM
-- Design Name: 
-- Module Name: jes204b_control - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity jes204b_control is
generic (
 
    C_M_AXI_ADDR_WIDTH   : integer := 32; 
    C_M_AXI_DATA_WIDTH   : integer := 32;   
    C_M_TRANSACTIONS_NUM : integer := 4    -- // Transaction number is the number of write   
  
);
Port ( 
      clk : in std_logic;
      resetn: in std_logic
);

end jes204b_control;


architecture Behavioral of jes204b_control is



    signal     M_AXI_AWVALID  : std_logic;  
    signal     M_AXI_WVALID   : std_logic;
    signal     M_AXI_ARVALID  : std_logic;  
    signal     M_AXI_RREADY   : std_logic; 
    signal     M_AXI_BREADY   : std_logic; 
   
    signal     M_AXI_AWADDR   : std_logic_vector (C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal     M_AXI_WDATA    : std_logic_vector (C_M_AXI_DATA_WIDTH-1 downto 0);
    signal     M_AXI_ARADDR   : std_logic_vector (C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal     M_AXI_WSTRB    : std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0) := (others => '0');

    signal     M_AXI_AWREADY  :   std_logic; 
    signal     M_AXI_WREADY   :   std_logic; 
   
    signal     M_AXI_BRESP    :   std_logic_vector (1 downto 0);      
    signal     M_AXI_BVALID   :   std_logic; 
    signal     M_AXI_ARREADY  :   std_logic;  
    signal     M_AXI_RDATA    :   std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal     M_AXI_RRESP    :   std_logic_vector (1 downto 0);       
    signal     M_AXI_RVALID   :   std_logic;    
  
    signal    start_single_write : std_logic;   
    signal    start_single_read  : std_logic;   
     
   
begin

aximaster: entity work.axi_master
generic map(
    C_M_AXI_ADDR_WIDTH => C_M_AXI_ADDR_WIDTH, 
    C_M_AXI_DATA_WIDTH => C_M_AXI_DATA_WIDTH,   
    C_M_TRANSACTIONS_NUM =>C_M_TRANSACTIONS_NUM 
 
)
Port map( 
  --user ports
  DATA_IN             => master_data,
  ADDR_IN             => master_addr,
  
  M_AXI_ACLK          => clk,
  M_AXI_ARESETN       => resetn,

  M_AXI_AWADDR        => M_AXI_AWADDR,
  M_AXI_AWVALID       => M_AXI_AWVALID,  
  M_AXI_AWREADY       => M_AXI_AWREADY,
  M_AXI_WDATA         => M_AXI_WDATA,
 
  M_AXI_WSTRB         => M_AXI_WSTRB,
  M_AXI_WVALID        => M_AXI_WVALID,
  M_AXI_WREADY        => M_AXI_WREADY,
 
  M_AXI_BRESP         => M_AXI_BRESP,
  M_AXI_BVALID        => M_AXI_BVALID,
  M_AXI_BREADY        => M_AXI_BREADY,
  M_AXI_ARADDR        => M_AXI_ARADDR,     
  M_AXI_ARVALID       => M_AXI_ARVALID,
  M_AXI_ARREADY       => M_AXI_ARREADY,
  M_AXI_RDATA         => M_AXI_RDATA,
  M_AXI_RRESP         => M_AXI_RRESP,   
  M_AXI_RVALID        => M_AXI_RVALID,
  M_AXI_RREADY        => M_AXI_RREADY
);          
    

jesd204b:  entity work.jesd204_adc
port map
(
  refclk_p => refclk_p,
  refclk_n => refclk_n,
  glblclk_p => glblclk_p,
  glblclk_n => glblclk_n,
  rx_core_clk_out => rx_core_clk_out,
 
  s_axi_aclk => s_axi_aclk,
  s_axi_aresetn => s_axi_aresetn,
 
  s_axi_awaddr => M_AXI_AWADDR,
  s_axi_awvalid => M_AXI_AWVALID,
  s_axi_awready => M_AXI_AWREADY,
  s_axi_wdata => M_AXI_WDATA,
  s_axi_wstrb => M_AXI_WSTRB,
  s_axi_wvalid => M_AXI_WVALID,
  s_axi_wready => M_AXI_WREADY,
  s_axi_bresp => M_AXI_BRESP,
  s_axi_bvalid => M_AXI_BVALID,
  s_axi_bready => M_AXI_BREADY,
  s_axi_araddr => M_AXI_ARADDR,
  s_axi_arvalid => M_AXI_ARVALID,
  s_axi_arready => M_AXI_ARREADY,
  s_axi_rdata => M_AXI_RDATA,
  s_axi_rresp => M_AXI_RRESP,
  s_axi_rvalid => M_AXI_RVALID,
  s_axi_rready => M_AXI_RREADY,
 
  rx_reset => rx_reset,
  rxp => rxp,
  rxn => rxn,
  rx_aresetn => rx_aresetn,
  rx_tdata => rx_tdata,
  rx_tvalid => rx_tvalid,
  rx_start_of_frame => rx_start_of_frame,
  rx_end_of_frame => rx_end_of_frame,
  rx_start_of_multiframe => rx_start_of_multiframe,
  rx_end_of_multiframe => rx_end_of_multiframe,
  rx_frame_error => rx_frame_error,
  rx_sysref => rx_sysref,
  rx_sync => rx_sync
);


end Behavioral;


  