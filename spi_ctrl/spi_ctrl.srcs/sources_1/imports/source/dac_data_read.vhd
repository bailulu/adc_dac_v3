library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
--use STD.textio.all;
use ieee.std_logic_textio.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity dac_data_read is
generic (
   COM_ALL      : integer
);
port (
   reset            : in  std_logic;
   clk              : in  std_logic;
   data_end         : in  std_logic;
   --ena              : in  std_logic;     -- external enable signal        
   out_data_read    : out std_logic_vector(COM_ALL-1 downto 0); 
   out_data_valid   : out std_logic
);
end dac_data_read;

architecture Behavioral of dac_data_read is

signal ena            : std_logic := '0';
signal out_data_valid_reg:   std_logic := '0';
signal out_data_valid_sig:   std_logic := '0';


constant NUM_ROW                : integer := 4;   -- number of column of file
    type T_DATA is array (0 to NUM_ROW-1 ) of std_logic_vector (COM_ALL-1 downto 0);
    constant DATA: T_DATA :=
            (
             "000000000000000001000000010",
             "000000000000000001100000011",
             "000000000000000010000000100",
             "000000000000000000100000001"        
            );
    signal DATA_reg: T_DATA;

    signal n: integer :=0;

begin

process(reset, clk)
begin 
    if(reset= '1') then
        ena                <= '1';
        out_data_valid_reg <= '0';
        
    elsif (rising_edge(clk)) then
        if (ena = '1') then
            out_data_valid_reg <= '1';
            
            for I in DATA'range loop  
                DATA_reg(I) <= DATA(I);
            end loop;         
        end if;    
    end if;  
end process;



process (reset,clk)
begin
     if(reset= '1') then
         n <= 0;
         out_data_valid_sig <= '0'; 
     elsif (rising_edge(clk)) then 
         if (out_data_valid_reg='1') and (n=0) then
             out_data_valid_sig <= '1';
         end if;
         
         if (out_data_valid_reg='1') and (data_end='1')then
                if (n< NUM_ROW-1) then
                    n <= n+1;
 
                elsif (n=NUM_ROW-1) then
                    out_data_valid_sig <= '0';
                end if;
               
      
          end if;
     end if;
     
end process;

out_data_valid <= out_data_valid_sig;
out_data_read  <=  DATA_reg(n);
end Behavioral;

