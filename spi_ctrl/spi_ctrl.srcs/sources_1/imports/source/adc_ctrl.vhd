
  -------------------------------------------------------------------------------------
  -- DESCRIPTION
  -- ===========
  --  1-bit R/W, 2-bit multi-byte field (W1, W0), 13-bit address field (A12-A0), 8-bit data
  --  Clocked in MSB first (R/W), and LSB (D0) last
  --  Serial data is clocked in on the rising edge of SCK

    -------------------------------------------------------------------------------------


  -- Library declarations
  library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;
  library unisim;
    use unisim.vcomponents.all;

  entity adc_ctrl is
    generic
   (
      START_ADDR   : std_logic_vector;
      STOP_ADDR    : std_logic_vector;
      
      COM_ALL      : integer;
      COM_NUM      : integer;
      ADDR_NUM     : integer;
      DATA_NUM     : integer;
      CLK_DIV      : integer
    );
    port (
      rst                    : in  std_logic;
   --   clk                    : in  std_logic;

      -- Command Interface
      clk_cmd                : in    std_logic;
      in_cmd_val             : in    std_logic;
      in_cmd                 : in    std_logic_vector(COM_ALL-1 downto 0);
      out_cmd_val            : out   std_logic;
      out_cmd                : out   std_logic_vector(COM_ALL-1 downto 0);
      
      clk_reg                : in    std_logic;
      

      -- Spi interface
      trig_n_cs              : out   std_logic;
      trig_sclk              : out   std_logic;
      trig_sdo               : out   std_logic;
      trig_clksel0           : in    std_logic;
      
      o_tx_end               : out   std_logic
   );
  end adc_ctrl;

  architecture Behavioural of adc_ctrl is

  ----------------------------------------------------------------------------------------------------
  -- Constants
  ----------------------------------------------------------------------------------------------------
  constant ADDR_MAX_WR  : std_logic_vector(14 downto 0) := b"111111111111111";
  constant ADDR_MAX_RD  : std_logic_vector(14 downto 0) := b"111111111111111";


  ----------------------------------------------------------------------------------------------------
  -- Signals
  ----------------------------------------------------------------------------------------------------
  type states is (idle, instruct, data_wait, data_io, data_valid, data_end);
  signal state       : states;
  signal state_next  : states;

  signal wr_reg_val          : std_logic;
  signal wr_reg_val_ack      : std_logic;
  signal wr_reg_addr         : std_logic_vector(ADDR_NUM-1 downto 0);
  signal wr_reg_data         : std_logic_vector(DATA_NUM-1 downto 0);

  signal rd_reg_req           : std_logic;
  signal rd_reg_addr          : std_logic_vector(ADDR_NUM-1 downto 0);
  signal rd_reg_val           : std_logic;
  signal rd_reg_data          : std_logic_vector(DATA_NUM-1 downto 0);

  signal wr_ack_s             : std_logic;
  signal wr_ack               : std_logic;
  signal wr_ack_i             : std_logic;
  signal wr_ack_ena           : std_logic;

  signal sclk_prebuf          : std_logic;
  signal serial_clk           : std_logic;
  signal sclk_ext             : std_logic;

  signal trig_sdi             : std_logic;
  signal trig_sdi_in          : std_logic;
  signal inst_val             : std_logic;
  signal inst_reg_val         : std_logic;
  signal inst_rw              : std_logic;
  signal inst_reg             : std_logic_vector(ADDR_NUM-1 downto 0); --IMPORTANT
  signal data_reg             : std_logic_vector(DATA_NUM-1 downto 0); -- IMPORTANT

 -- signal shifting             : std_logic;
  signal shift_reg            : std_logic_vector(COM_ALL-1 downto 0); -- IMPORTANT
  signal read_byte_val        : std_logic;
  signal data_read_val        : std_logic;
  signal data_write_val       : std_logic;
  signal data_read            : std_logic_vector(DATA_NUM-1 downto 0);

  signal sh_counter     : integer;
  signal read_n_write   : std_logic;
  signal ncs_int        : std_logic;
  signal ncs_trig       : std_logic;
  --signal ncs_int_w      : std_logic;
  signal busy           : std_logic;
  
  signal out_mailbox_data_sig : std_logic_vector(DATA_NUM-1 downto 0);

   signal o_tx_end_sig        : std_logic;
   signal sclk_sig              : std_logic;
   signal sclk_rise  : std_logic;
   signal sclk_fall:   std_logic;
   signal r_counter_clock        : integer range 0 to CLK_DIV*2;
   signal r_counter_clock_ena    : std_logic;

   signal r_counter_data         : integer range 0 to COM_ALL;
   signal trig_sdo_sig           : std_logic;
  -- signal com_counter_data      : std_logic;
   --signal _counter_data      : std_logic;
   

--*****************************************************************************************************
  begin
--*****************************************************************************************************

-- w_tc_counter_data  <= '0' when(r_counter_data>0) else '1';
  ----------------------------------------------------------------------------------------------------
  -- Command Interface: seperate the coming command into R/W, address bits, data bits and generate read
  -- request and write request/acknowledge pulse.
  ----------------------------------------------------------------------------------------------------
  spi_cmd_inst: entity work.command
  generic map
  (
    START_ADDR   => START_ADDR,
    STOP_ADDR    => STOP_ADDR,

    COM_ALL      => COM_ALL,
    COM_NUM      => COM_NUM,
    ADDR_NUM     => ADDR_NUM,
    DATA_NUM     => DATA_NUM
  )
  port map
  (
    reset           => rst,
    
    clk_cmd         => clk_cmd,
    in_cmd_val      => in_cmd_val,
    in_cmd          => in_cmd,
    out_cmd_val     => out_cmd_val,
    out_cmd         => out_cmd,
    
    clk_reg         => clk_reg, --LM clk,
    wr_reg_val      => wr_reg_val,
    wr_reg_val_ack  => wr_reg_val_ack,
    wr_reg_addr     => wr_reg_addr,
    wr_reg_data     => wr_reg_data,
    
    rd_reg_req      => rd_reg_req,
    rd_reg_addr     => rd_reg_addr,
    rd_reg_val      => rd_reg_val,
    rd_reg_data     => rd_reg_data,
    wr_ack          => wr_ack,
    mbx_in_reg      => (others=>'0'),
    mbx_in_val      => '0'
  );



  ----------------------------------------------------------------------------------------------------
  -- Shoot commands to the state machine
  ----------------------------------------------------------------------------------------------------
  p_state : process(clk_cmd,rst)
  begin
    if(rst='1') then
      state            <= idle;
    elsif(rising_edge(clk_cmd)) then
      state            <= state_next;
    end if;
  end process p_state;
  
  p_comb : process(
                   state                       ,
                   inst_reg_val                         ,
                   sclk_rise                        ,
                   sclk_fall                         )
  begin
    case state is
       when  idle            =>  -- ST_RESET
        if(inst_reg_val='1')  then   state_next  <= instruct;
        else                                            state_next  <= idle ;
        end if;
    
        when  instruct    => 
          if       (sh_counter= 0) and (sclk_fall='1') then  state_next  <= data_io;
          else                                               state_next  <= instruct;
          end if;
    
        
        when  data_io      => 
          if       (sh_counter= 0) and (sclk_fall='1') then  state_next  <= data_end;
          else                                                 state_next  <= data_io;
          end if;
   
        when  data_end    => 
          if(sclk_fall='1') then
            -- if (read_n_write = '1') then  
             --    state_next <= data_valid; -- read   
             state_next <= idle;
             else 
                --state_next <= data_wait; --write
            -- end if;
             state_next  <= data_end ;    
         end if;
         

     --   when data_valid    =>       state_next <= idle;
            
      --  when data_wait     =>       state_next <= idle;
    
  
        when  others       =>  
           if(inst_reg_val='1') then   state_next  <= instruct ;
           else                        state_next  <= idle ;
           end if;
       end case;
  end process p_comb;
  

  
  state_out: process (rst, clk_cmd)
   begin
     if (rst = '1') then
 
        sh_counter   <= 0;
        read_n_write <= '0';
        
      --  shifting     <= '0';
        wr_ack_s     <= '0'; 
        
        r_counter_data       <= shift_reg'length-data_reg'length-1;
        r_counter_clock_ena  <= '0';
        o_tx_end_sig <= '0';
        
        shift_reg      <= (others => '0');
        read_byte_val  <= '0';
        data_read      <= (others => '0');
        
        sclk_sig      <= '1';
        trig_sdo_sig  <= '1';
        ncs_int       <= '1';
        
        
     elsif (rising_edge(clk_cmd)) then
       -- Main state machine
       inst_reg_val<=inst_val;
       case state is
        
         when idle =>
           sh_counter    <= shift_reg'length-data_reg'length-1; --total length minus data bytes;
           read_byte_val <= '0';
           data_read     <= data_read;
           r_counter_clock_ena  <= '0';
           sclk_sig   <= '1';
           ncs_int      <= '1';
         --  if (inst_reg_val = '1') then
          --    shifting     <= '1';
           read_n_write <= inst_rw; -- 0 = write, 1 = read
             -- ncs_int      <= '0';
           o_tx_end_sig <= '0';
             -- r_counter_clock_ena  <= '1';
             -- sclk_sig   <= '0';
           shift_reg      <= (others => '0');
          -- else
         --  shifting     <= '0';
            --  ncs_int      <= '1';
           --   o_tx_end_sig <= '0';
             
              
         --  end if;
              wr_ack_s <= '0';
      
   
         when instruct =>
            --  shifting             <= '1';
              o_tx_end_sig         <= '0';
              r_counter_clock_ena  <= '1';
             -- ncs_int              <= '0';
              if (sh_counter =shift_reg'length-data_reg'length-1)then
              shift_reg <= "000" & inst_rw & inst_reg & data_reg;
              end if;
            
           if (sclk_rise='1') then
               sclk_sig   <= '1';
              -- shift_reg  <= shift_reg(shift_reg'length-2 downto 0) & trig_sdi_in;         
               
              
               
           elsif (sclk_fall='1') then
                 ncs_int            <= '0';
                 sclk_sig   <= '0';
                 trig_sdo_sig <= shift_reg(shift_reg'length - 1);
                 shift_reg  <= shift_reg(shift_reg'length-2 downto 0) & '1'; 
                 
                  if (sh_counter>0)then
                      sh_counter <= sh_counter - 1;
                  else
                      sh_counter <= data_reg'length-1;               
                  end if;
           end if;
 
 
 
         when data_io =>
           --   shifting             <= '1';
              o_tx_end_sig         <= '0';
              r_counter_clock_ena  <= '1';
             -- ncs_int              <= '0';
              
           if (sclk_rise='1') then
               sclk_sig   <= '1';
              -- shift_reg  <= shift_reg(shift_reg'length-2 downto 0) & trig_sdi_in;         
                  
           --    if (sh_counter>0)then
             --      sh_counter <= sh_counter - 1;
                   
             --  end if;
                  
           elsif (sclk_fall='1') then             
                   sclk_sig   <= '0';
                    ncs_int              <= '0';
                   trig_sdo_sig <= shift_reg(shift_reg'length - 1);
                   shift_reg  <= shift_reg(shift_reg'length-2 downto 0) & '1'; 
                   
                   if (sh_counter>0)then
                       sh_counter <= sh_counter - 1;
                   elsif (sh_counter = 0) then
                      o_tx_end_sig <= sclk_fall;
                 --      sh_counter <= data_reg'length-1;               
                   end if;
     
           end if;     
           
 
         when data_end =>
            -- if (sh_counter = 0) then
                 --sh_counter <= shift_reg'length-data_reg'length-1; 
              --   shifting   <= '0';
                 ncs_int    <= '0';
                 o_tx_end_sig <= '0';
                 r_counter_clock_ena  <= '1';
                if (sclk_rise='1') then
                              sclk_sig   <= '1';
        
                elsif (sclk_fall='1') then             
                       sclk_sig   <= '1';
                       ncs_int    <= '1';
                end if;
                  
      --   when data_valid =>  -- read
        --     o_tx_end_sig <= '0';
          --   sclk_sig   <= '1';
            -- trig_sdo_sig <= '1';
        --     r_counter_clock_ena  <= '0';
          --   read_byte_val <= '1';
            -- data_read     <= shift_reg(DATA_NUM-1 downto 0);
         
      --   when data_wait =>  -- write       the function of this state is to generate wr_ack
        --     o_tx_end_sig <= '0';
          --   sclk_sig   <= '1';
            -- trig_sdo_sig <= '1';
      --       r_counter_clock_ena  <= '0';
        --     if (wr_ack_ena = '1') then
          --     wr_ack_s <= '1';
            -- else
             --  wr_ack_s <= '0';
            -- end if;            
        
 
         when others =>
              wr_ack_s              <= '0';
              o_tx_end_sig          <= '0';
              r_counter_clock_ena   <= '0'; 
            --  shifting              <= '0';
              ncs_int               <= '1';
              sclk_sig              <= '1';
              trig_sdo_sig          <= '1';
                         
       end case;
 
     end if;
   end process;


  
  state_in: process (rst, clk_cmd) --LM clk
  begin
    if (rst = '1') then

      rd_reg_val     <= '0';
      rd_reg_data    <= (others => '0');
      inst_val       <= '0';
      inst_rw        <= '0';
      inst_reg       <= (others=> '0');
      data_reg       <= (others=> '0');
		--data_read      <= (others=> '0');
       
      wr_ack         <= '0';
      wr_ack_ena     <= '0';
   
      
    elsif (rising_edge(clk_cmd)) then -- LM clk

      -- read from serial if when address is within device range
      -- generate rd_reg_val 
      if (rd_reg_addr <= ADDR_MAX_RD) and (sclk_rise='1')then   
        rd_reg_val  <= data_read_val;
       -- rd_reg_data <= conv_std_logic_vector(0, 24) & data_read;
        rd_reg_data <= data_read;
      else
        rd_reg_val  <= '0';
        rd_reg_data <= rd_reg_data;
      end if;

      -- Write instruction, only when address is within device range
      if (wr_reg_val = '1' and wr_reg_addr <= ADDR_MAX_WR) then
        inst_val <= '1'; -- instruction valid, means there is an instruction
        inst_rw  <= '0'; -- write
        inst_reg <= wr_reg_addr(ADDR_NUM-1 downto 0);
        data_reg <= wr_reg_data(DATA_NUM-1 downto 0);
      -- Read instruction, only when address is within LMK04828 range
      elsif (rd_reg_req = '1' and rd_reg_addr <= ADDR_MAX_RD) then
        inst_val <= '1'; -- instruction valid, means there is an instruction
        inst_rw  <= '1'; -- read
        inst_reg <= rd_reg_addr(ADDR_NUM-1 downto 0);
        data_reg <= data_reg;
      -- No instruction
      else
        inst_val <= '0';
        inst_rw  <= inst_rw;
        inst_reg <= inst_reg;
        data_reg <= data_reg;
      end if;
  
   
    ----------------------------------------------------------------------------------------------------
     ----------------------------------------------------------------------------------------------------
      ----------------------------------------------------------------------------------------------------   
      -- Write acknowledge
      if (wr_reg_val_ack = '1') and (wr_reg_addr > ADDR_MAX_WR) then
        wr_ack     <= '1';
        wr_ack_ena <= '0';
      elsif (wr_reg_val_ack = '1') and (wr_reg_addr <= ADDR_MAX_WR) then
        wr_ack     <= '0';
        wr_ack_ena <= '1';
      elsif (wr_ack_i = '1') then
        wr_ack     <= '1';
        wr_ack_ena <= '0';
      else
        wr_ack     <= '0';
      end if;
      ----------------------------------------------------------------------------------------------------
       ----------------------------------------------------------------------------------------------------
        ----------------------------------------------------------------------------------------------------
        
        
    end if;
  end process;

  ----------------------------------------------------------------------------------------------------
  -- Serial interface state-machine
  -- The state machine decided how many clocks each states will be stayed. which finally defined the output of
  -- spi_sdo. 
  ----------------------------------------------------------------------------------------------------
 
  busy <= '0' when (state = idle) else '1';


data_read_val <= read_byte_val;

wr_ack_i <= wr_ack_s;



sclk_counter:   process (rst, clk_cmd)
begin
  if(rst='1') then
    r_counter_clock            <= 0;
    sclk_rise                <= '0';
    sclk_fall                <= '0';
  elsif(rising_edge(clk_cmd)) then
    if(r_counter_clock_ena='1') then  -- sclk = '1' by default 
      if(r_counter_clock=CLK_DIV-1) then  -- firse edge = fall
        r_counter_clock          <= r_counter_clock + 1;
        sclk_rise                <= '0';
        sclk_fall                <= '1';
      elsif(r_counter_clock=(CLK_DIV*2)-1) then
        r_counter_clock            <= 0;
        sclk_rise                <= '1';
        sclk_fall                <= '0';
      else
        r_counter_clock            <= r_counter_clock + 1;
        sclk_rise                <= '0';
        sclk_fall                <= '0';
      end if;
    else
      r_counter_clock            <= 0;
      sclk_rise                <= '0';
      sclk_fall                <= '0';
    end if;
  end if;
end process sclk_counter;

--------------------------------------------------------------------------------
-- Outputs
--------------------------------------------------------------------------------
    --spi_io_t  <= '1' when (sh_state = data_io and read_n_write = '1') else '0'; -- 0 = output, 1 = input
   -- trig_sdo <= 'Z' when (state = data_io and read_n_write = '1') else shift_reg(shift_reg'length - 1);--shift_reg(shift_reg'length - 1) when ncs_int = '0' else 'Z';
    trig_sdo  <= trig_sdo_sig;
    trig_n_cs <= ncs_int;
    trig_sclk <= sclk_sig;
--  ncs_trig <= ncs_int when read_n_write = '0' else ncs_int_w;
    o_tx_end <= o_tx_end_sig;

  ----------------------------------------------------------------------------------------------------
  -- End
  ----------------------------------------------------------------------------------------------------
  end Behavioural;

