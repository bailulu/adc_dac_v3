--Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
--Date        : Thu Apr  5 22:22:46 2018
--Host        : lu running 64-bit Ubuntu 14.04.3 LTS
--Command     : generate_target Jesd204_microblaze_wrapper.bd
--Design      : Jesd204_microblaze_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Jesd204_microblaze_wrapper is
  port (
    clk_in0_n : in STD_LOGIC;
    clk_in0_p : in STD_LOGIC;
    gpio_rtl_tri_io : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    gt_ref_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt_ref_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    lmk_clk_sysref_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    lmk_clk_sysref_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_rtl_0 : in STD_LOGIC;
    rx_sync_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_sync_p : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxn_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    rxp_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    spi_rtl_io0_io : inout STD_LOGIC;
    spi_rtl_io1_io : inout STD_LOGIC;
    spi_rtl_sck_io : inout STD_LOGIC;
    spi_rtl_ss_io : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_sync_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_sync_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    txn_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    txp_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    uart_rtl_rxd : in STD_LOGIC;
    uart_rtl_txd : out STD_LOGIC
  );
end Jesd204_microblaze_wrapper;

architecture STRUCTURE of Jesd204_microblaze_wrapper is
  component Jesd204_microblaze is
  port (
    uart_rtl_rxd : in STD_LOGIC;
    uart_rtl_txd : out STD_LOGIC;
    gpio_rtl_tri_i : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gpio_rtl_tri_o : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gpio_rtl_tri_t : out STD_LOGIC_VECTOR ( 2 downto 0 );
    spi_rtl_io0_i : in STD_LOGIC;
    spi_rtl_io0_o : out STD_LOGIC;
    spi_rtl_io0_t : out STD_LOGIC;
    spi_rtl_io1_i : in STD_LOGIC;
    spi_rtl_io1_o : out STD_LOGIC;
    spi_rtl_io1_t : out STD_LOGIC;
    spi_rtl_sck_i : in STD_LOGIC;
    spi_rtl_sck_o : out STD_LOGIC;
    spi_rtl_sck_t : out STD_LOGIC;
    spi_rtl_ss_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    spi_rtl_ss_o : out STD_LOGIC_VECTOR ( 3 downto 0 );
    spi_rtl_ss_t : out STD_LOGIC;
    lmk_clk_sysref_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    lmk_clk_sysref_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt_ref_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt_ref_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    rx_sync_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_sync_p : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxn_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    rxp_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_sync_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_sync_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_rtl_0 : in STD_LOGIC;
    clk_in0_n : in STD_LOGIC;
    clk_in0_p : in STD_LOGIC;
    txp_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    txn_out : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component Jesd204_microblaze;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal gpio_rtl_tri_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal gpio_rtl_tri_i_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal gpio_rtl_tri_i_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal gpio_rtl_tri_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal gpio_rtl_tri_io_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal gpio_rtl_tri_io_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal gpio_rtl_tri_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal gpio_rtl_tri_o_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal gpio_rtl_tri_o_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal gpio_rtl_tri_t_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal gpio_rtl_tri_t_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal gpio_rtl_tri_t_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal spi_rtl_io0_i : STD_LOGIC;
  signal spi_rtl_io0_o : STD_LOGIC;
  signal spi_rtl_io0_t : STD_LOGIC;
  signal spi_rtl_io1_i : STD_LOGIC;
  signal spi_rtl_io1_o : STD_LOGIC;
  signal spi_rtl_io1_t : STD_LOGIC;
  signal spi_rtl_sck_i : STD_LOGIC;
  signal spi_rtl_sck_o : STD_LOGIC;
  signal spi_rtl_sck_t : STD_LOGIC;
  signal spi_rtl_ss_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal spi_rtl_ss_i_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal spi_rtl_ss_i_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal spi_rtl_ss_i_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal spi_rtl_ss_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal spi_rtl_ss_io_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal spi_rtl_ss_io_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal spi_rtl_ss_io_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal spi_rtl_ss_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal spi_rtl_ss_o_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal spi_rtl_ss_o_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal spi_rtl_ss_o_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal spi_rtl_ss_t : STD_LOGIC;
begin
Jesd204_microblaze_i: component Jesd204_microblaze
     port map (
      clk_in0_n => clk_in0_n,
      clk_in0_p => clk_in0_p,
      gpio_rtl_tri_i(2) => gpio_rtl_tri_i_2(2),
      gpio_rtl_tri_i(1) => gpio_rtl_tri_i_1(1),
      gpio_rtl_tri_i(0) => gpio_rtl_tri_i_0(0),
      gpio_rtl_tri_o(2) => gpio_rtl_tri_o_2(2),
      gpio_rtl_tri_o(1) => gpio_rtl_tri_o_1(1),
      gpio_rtl_tri_o(0) => gpio_rtl_tri_o_0(0),
      gpio_rtl_tri_t(2) => gpio_rtl_tri_t_2(2),
      gpio_rtl_tri_t(1) => gpio_rtl_tri_t_1(1),
      gpio_rtl_tri_t(0) => gpio_rtl_tri_t_0(0),
      gt_ref_clk_n(0) => gt_ref_clk_n(0),
      gt_ref_clk_p(0) => gt_ref_clk_p(0),
      lmk_clk_sysref_n(0) => lmk_clk_sysref_n(0),
      lmk_clk_sysref_p(0) => lmk_clk_sysref_p(0),
      reset_rtl_0 => reset_rtl_0,
      rx_sync_n(0) => rx_sync_n(0),
      rx_sync_p(0) => rx_sync_p(0),
      rxn_in(0) => rxn_in(0),
      rxp_in(0) => rxp_in(0),
      spi_rtl_io0_i => spi_rtl_io0_i,
      spi_rtl_io0_o => spi_rtl_io0_o,
      spi_rtl_io0_t => spi_rtl_io0_t,
      spi_rtl_io1_i => spi_rtl_io1_i,
      spi_rtl_io1_o => spi_rtl_io1_o,
      spi_rtl_io1_t => spi_rtl_io1_t,
      spi_rtl_sck_i => spi_rtl_sck_i,
      spi_rtl_sck_o => spi_rtl_sck_o,
      spi_rtl_sck_t => spi_rtl_sck_t,
      spi_rtl_ss_i(3) => spi_rtl_ss_i_3(3),
      spi_rtl_ss_i(2) => spi_rtl_ss_i_2(2),
      spi_rtl_ss_i(1) => spi_rtl_ss_i_1(1),
      spi_rtl_ss_i(0) => spi_rtl_ss_i_0(0),
      spi_rtl_ss_o(3) => spi_rtl_ss_o_3(3),
      spi_rtl_ss_o(2) => spi_rtl_ss_o_2(2),
      spi_rtl_ss_o(1) => spi_rtl_ss_o_1(1),
      spi_rtl_ss_o(0) => spi_rtl_ss_o_0(0),
      spi_rtl_ss_t => spi_rtl_ss_t,
      tx_sync_n(0) => tx_sync_n(0),
      tx_sync_p(0) => tx_sync_p(0),
      txn_out(0) => txn_out(0),
      txp_out(0) => txp_out(0),
      uart_rtl_rxd => uart_rtl_rxd,
      uart_rtl_txd => uart_rtl_txd
    );
gpio_rtl_tri_iobuf_0: component IOBUF
     port map (
      I => gpio_rtl_tri_o_0(0),
      IO => gpio_rtl_tri_io(0),
      O => gpio_rtl_tri_i_0(0),
      T => gpio_rtl_tri_t_0(0)
    );
gpio_rtl_tri_iobuf_1: component IOBUF
     port map (
      I => gpio_rtl_tri_o_1(1),
      IO => gpio_rtl_tri_io(1),
      O => gpio_rtl_tri_i_1(1),
      T => gpio_rtl_tri_t_1(1)
    );
gpio_rtl_tri_iobuf_2: component IOBUF
     port map (
      I => gpio_rtl_tri_o_2(2),
      IO => gpio_rtl_tri_io(2),
      O => gpio_rtl_tri_i_2(2),
      T => gpio_rtl_tri_t_2(2)
    );
spi_rtl_io0_iobuf: component IOBUF
     port map (
      I => spi_rtl_io0_o,
      IO => spi_rtl_io0_io,
      O => spi_rtl_io0_i,
      T => spi_rtl_io0_t
    );
spi_rtl_io1_iobuf: component IOBUF
     port map (
      I => spi_rtl_io1_o,
      IO => spi_rtl_io1_io,
      O => spi_rtl_io1_i,
      T => spi_rtl_io1_t
    );
spi_rtl_sck_iobuf: component IOBUF
     port map (
      I => spi_rtl_sck_o,
      IO => spi_rtl_sck_io,
      O => spi_rtl_sck_i,
      T => spi_rtl_sck_t
    );
spi_rtl_ss_iobuf_0: component IOBUF
     port map (
      I => spi_rtl_ss_o_0(0),
      IO => spi_rtl_ss_io(0),
      O => spi_rtl_ss_i_0(0),
      T => spi_rtl_ss_t
    );
spi_rtl_ss_iobuf_1: component IOBUF
     port map (
      I => spi_rtl_ss_o_1(1),
      IO => spi_rtl_ss_io(1),
      O => spi_rtl_ss_i_1(1),
      T => spi_rtl_ss_t
    );
spi_rtl_ss_iobuf_2: component IOBUF
     port map (
      I => spi_rtl_ss_o_2(2),
      IO => spi_rtl_ss_io(2),
      O => spi_rtl_ss_i_2(2),
      T => spi_rtl_ss_t
    );
spi_rtl_ss_iobuf_3: component IOBUF
     port map (
      I => spi_rtl_ss_o_3(3),
      IO => spi_rtl_ss_io(3),
      O => spi_rtl_ss_i_3(3),
      T => spi_rtl_ss_t
    );
end STRUCTURE;
