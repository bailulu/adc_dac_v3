
################################################################
# This is a generated script based on design: Jesd204_microblaze
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2016.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source Jesd204_microblaze_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7vx690tffg1761-3
}


# CHANGE DESIGN NAME HERE
set design_name Jesd204_microblaze

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: microblaze_0_local_memory
proc create_hier_cell_microblaze_0_local_memory { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" create_hier_cell_microblaze_0_local_memory() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I -type clk LMB_Clk
  create_bd_pin -dir I -type rst SYS_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list \
CONFIG.C_ECC {0} \
 ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list \
CONFIG.C_ECC {0} \
 ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.3 lmb_bram ]
  set_property -dict [ list \
CONFIG.Memory_Type {True_Dual_Port_RAM} \
CONFIG.use_bram_block {BRAM_Controller} \
 ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_0_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_0_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net SYS_Rst_1 [get_bd_pins SYS_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set gpio_rtl [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 gpio_rtl ]
  set spi_rtl [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:spi_rtl:1.0 spi_rtl ]
  set uart_rtl [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 uart_rtl ]

  # Create ports
  set clk_in0_n [ create_bd_port -dir I -type clk clk_in0_n ]
  set clk_in0_p [ create_bd_port -dir I -type clk clk_in0_p ]
  set gt_ref_clk_n [ create_bd_port -dir I -from 0 -to 0 -type clk gt_ref_clk_n ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {156250000} \
 ] $gt_ref_clk_n
  set gt_ref_clk_p [ create_bd_port -dir I -from 0 -to 0 -type clk gt_ref_clk_p ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {156250000} \
 ] $gt_ref_clk_p
  set lmk_clk_sysref_n [ create_bd_port -dir I -from 0 -to 0 lmk_clk_sysref_n ]
  set lmk_clk_sysref_p [ create_bd_port -dir I -from 0 -to 0 lmk_clk_sysref_p ]
  set reset_rtl_0 [ create_bd_port -dir I -type rst reset_rtl_0 ]
  set_property -dict [ list \
CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $reset_rtl_0
  set rx_sync_n [ create_bd_port -dir O -from 0 -to 0 rx_sync_n ]
  set rx_sync_p [ create_bd_port -dir O -from 0 -to 0 rx_sync_p ]
  set rxn_in [ create_bd_port -dir I -from 0 -to 0 rxn_in ]
  set rxp_in [ create_bd_port -dir I -from 0 -to 0 rxp_in ]
  set tx_sync_n [ create_bd_port -dir I -from 0 -to 0 tx_sync_n ]
  set tx_sync_p [ create_bd_port -dir I -from 0 -to 0 tx_sync_p ]
  set txn_out [ create_bd_port -dir O -from 0 -to 0 txn_out ]
  set txp_out [ create_bd_port -dir O -from 0 -to 0 txp_out ]

  # Create instance: axi_gpio_0, and set properties
  set axi_gpio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0 ]
  set_property -dict [ list \
CONFIG.C_ALL_INPUTS {0} \
CONFIG.C_GPIO_WIDTH {3} \
CONFIG.C_INTERRUPT_PRESENT {0} \
 ] $axi_gpio_0

  # Create instance: axi_quad_spi_0, and set properties
  set axi_quad_spi_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 axi_quad_spi_0 ]
  set_property -dict [ list \
CONFIG.C_FIFO_DEPTH {256} \
CONFIG.C_NUM_SS_BITS {4} \
CONFIG.C_SCK_RATIO {2} \
CONFIG.C_TYPE_OF_AXI4_INTERFACE {1} \
CONFIG.C_USE_STARTUP {0} \
CONFIG.C_USE_STARTUP_INT {0} \
 ] $axi_quad_spi_0

  # Create instance: axi_uartlite_0, and set properties
  set axi_uartlite_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uartlite:2.0 axi_uartlite_0 ]
  set_property -dict [ list \
CONFIG.C_BAUDRATE {115200} \
 ] $axi_uartlite_0

  # Create instance: clk_wiz_1, and set properties
  set clk_wiz_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.3 clk_wiz_1 ]
  set_property -dict [ list \
CONFIG.CLKOUT1_JITTER {130.958} \
CONFIG.CLKOUT1_PHASE_ERROR {98.575} \
CONFIG.CLKOUT2_JITTER {130.958} \
CONFIG.CLKOUT2_PHASE_ERROR {98.575} \
CONFIG.CLKOUT2_USED {true} \
CONFIG.CLKOUT3_JITTER {130.958} \
CONFIG.CLKOUT3_PHASE_ERROR {98.575} \
CONFIG.CLKOUT3_USED {true} \
CONFIG.CLKOUT4_JITTER {130.958} \
CONFIG.CLKOUT4_PHASE_ERROR {98.575} \
CONFIG.CLKOUT4_USED {true} \
CONFIG.MMCM_CLKFBOUT_MULT_F {10.000} \
CONFIG.MMCM_CLKIN1_PERIOD {10.0} \
CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
CONFIG.MMCM_CLKOUT0_DIVIDE_F {10.000} \
CONFIG.MMCM_CLKOUT1_DIVIDE {10} \
CONFIG.MMCM_CLKOUT2_DIVIDE {10} \
CONFIG.MMCM_CLKOUT3_DIVIDE {10} \
CONFIG.MMCM_COMPENSATION {ZHOLD} \
CONFIG.MMCM_DIVCLK_DIVIDE {1} \
CONFIG.NUM_OUT_CLKS {4} \
CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} \
 ] $clk_wiz_1

  # Need to retain value_src of defaults
  set_property -dict [ list \
CONFIG.CLKOUT1_JITTER.VALUE_SRC {DEFAULT} \
CONFIG.CLKOUT1_PHASE_ERROR.VALUE_SRC {DEFAULT} \
CONFIG.MMCM_CLKFBOUT_MULT_F.VALUE_SRC {DEFAULT} \
CONFIG.MMCM_CLKIN1_PERIOD.VALUE_SRC {DEFAULT} \
CONFIG.MMCM_CLKIN2_PERIOD.VALUE_SRC {DEFAULT} \
CONFIG.MMCM_CLKOUT0_DIVIDE_F.VALUE_SRC {DEFAULT} \
CONFIG.MMCM_COMPENSATION.VALUE_SRC {DEFAULT} \
 ] $clk_wiz_1

  # Create instance: ila_0, and set properties
  set ila_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:ila:6.2 ila_0 ]
  set_property -dict [ list \
CONFIG.C_ADV_TRIGGER {true} \
CONFIG.C_ENABLE_ILA_AXI_MON {false} \
CONFIG.C_INPUT_PIPE_STAGES {0} \
CONFIG.C_MONITOR_TYPE {Native} \
CONFIG.C_NUM_OF_PROBES {1} \
CONFIG.C_PROBE0_WIDTH {4} \
CONFIG.C_PROBE1_WIDTH {1} \
 ] $ila_0

  # Create instance: jesd204_phy_0, and set properties
  set jesd204_phy_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:jesd204_phy:3.2 jesd204_phy_0 ]
  set_property -dict [ list \
CONFIG.Axi_Lite {true} \
CONFIG.C_LANES {1} \
CONFIG.Config_Type {1} \
CONFIG.Max_Line_Rate {12.5} \
CONFIG.Min_Line_Rate {6.25} \
CONFIG.TransceiverControl {true} \
 ] $jesd204_phy_0

  # Create instance: jesd204_rx, and set properties
  set jesd204_rx [ create_bd_cell -type ip -vlnv xilinx.com:ip:jesd204:7.1 jesd204_rx ]
  set_property -dict [ list \
CONFIG.C_LANES {1} \
CONFIG.C_NODE_IS_TRANSMIT {0} \
 ] $jesd204_rx

  # Create instance: jesd204_tx, and set properties
  set jesd204_tx [ create_bd_cell -type ip -vlnv xilinx.com:ip:jesd204:7.1 jesd204_tx ]
  set_property -dict [ list \
CONFIG.C_LANES {1} \
 ] $jesd204_tx

  # Create instance: lmk_sysref, and set properties
  set lmk_sysref [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 lmk_sysref ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {IBUFDS} \
 ] $lmk_sysref

  # Create instance: lmk_sysref_buf, and set properties
  set lmk_sysref_buf [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 lmk_sysref_buf ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {BUFG} \
 ] $lmk_sysref_buf

  # Create instance: mdm_1, and set properties
  set mdm_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:mdm:3.2 mdm_1 ]

  # Create instance: microblaze_0, and set properties
  set microblaze_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:10.0 microblaze_0 ]
  set_property -dict [ list \
CONFIG.C_DEBUG_ENABLED {1} \
CONFIG.C_D_AXI {1} \
CONFIG.C_D_LMB {1} \
CONFIG.C_I_LMB {1} \
 ] $microblaze_0

  # Create instance: microblaze_0_axi_intc, and set properties
  set microblaze_0_axi_intc [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 microblaze_0_axi_intc ]
  set_property -dict [ list \
CONFIG.C_HAS_FAST {1} \
 ] $microblaze_0_axi_intc

  # Create instance: microblaze_0_axi_periph, and set properties
  set microblaze_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 microblaze_0_axi_periph ]
  set_property -dict [ list \
CONFIG.NUM_MI {7} \
 ] $microblaze_0_axi_periph

  # Create instance: microblaze_0_local_memory
  create_hier_cell_microblaze_0_local_memory [current_bd_instance .] microblaze_0_local_memory

  # Create instance: microblaze_0_xlconcat, and set properties
  set microblaze_0_xlconcat [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 microblaze_0_xlconcat ]
  set_property -dict [ list \
CONFIG.NUM_PORTS {2} \
 ] $microblaze_0_xlconcat

  # Create instance: ref_clk, and set properties
  set ref_clk [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 ref_clk ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {IBUFDSGTE} \
 ] $ref_clk

  # Create instance: rst_clk_wiz_1_100M, and set properties
  set rst_clk_wiz_1_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_clk_wiz_1_100M ]
  set_property -dict [ list \
CONFIG.C_AUX_RESET_HIGH {1} \
 ] $rst_clk_wiz_1_100M

  # Create instance: rx_sync_ds, and set properties
  set rx_sync_ds [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 rx_sync_ds ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
 ] $rx_sync_ds

  # Create instance: tx_sync_ds, and set properties
  set tx_sync_ds [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 tx_sync_ds ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {IBUFDS} \
 ] $tx_sync_ds

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
 ] $xlconstant_1

  # Create interface connections
  connect_bd_intf_net -intf_net axi_gpio_0_GPIO [get_bd_intf_ports gpio_rtl] [get_bd_intf_pins axi_gpio_0/GPIO]
  connect_bd_intf_net -intf_net axi_quad_spi_0_SPI_0 [get_bd_intf_ports spi_rtl] [get_bd_intf_pins axi_quad_spi_0/SPI_0]
  connect_bd_intf_net -intf_net axi_uartlite_0_UART [get_bd_intf_ports uart_rtl] [get_bd_intf_pins axi_uartlite_0/UART]
  connect_bd_intf_net -intf_net jesd204_1_m_axis_rx [get_bd_intf_pins jesd204_rx/m_axis_rx] [get_bd_intf_pins jesd204_tx/s_axis_tx]
  connect_bd_intf_net -intf_net jesd204_2_gt0_tx [get_bd_intf_pins jesd204_phy_0/gt0_tx] [get_bd_intf_pins jesd204_tx/gt0_tx]
  connect_bd_intf_net -intf_net jesd204_phy_0_gt0_rx [get_bd_intf_pins jesd204_phy_0/gt0_rx] [get_bd_intf_pins jesd204_rx/gt0_rx]
  connect_bd_intf_net -intf_net microblaze_0_axi_dp [get_bd_intf_pins microblaze_0/M_AXI_DP] [get_bd_intf_pins microblaze_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M01_AXI [get_bd_intf_pins jesd204_phy_0/s_axi] [get_bd_intf_pins microblaze_0_axi_periph/M01_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M02_AXI [get_bd_intf_pins jesd204_rx/s_axi] [get_bd_intf_pins microblaze_0_axi_periph/M02_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M03_AXI [get_bd_intf_pins jesd204_tx/s_axi] [get_bd_intf_pins microblaze_0_axi_periph/M03_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M04_AXI [get_bd_intf_pins axi_uartlite_0/S_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M04_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M05_AXI [get_bd_intf_pins axi_gpio_0/S_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M05_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M06_AXI [get_bd_intf_pins axi_quad_spi_0/AXI_FULL] [get_bd_intf_pins microblaze_0_axi_periph/M06_AXI]
  connect_bd_intf_net -intf_net microblaze_0_debug [get_bd_intf_pins mdm_1/MBDEBUG_0] [get_bd_intf_pins microblaze_0/DEBUG]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_1 [get_bd_intf_pins microblaze_0/DLMB] [get_bd_intf_pins microblaze_0_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_1 [get_bd_intf_pins microblaze_0/ILMB] [get_bd_intf_pins microblaze_0_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_0_intc_axi [get_bd_intf_pins microblaze_0_axi_intc/s_axi] [get_bd_intf_pins microblaze_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net microblaze_0_interrupt [get_bd_intf_pins microblaze_0/INTERRUPT] [get_bd_intf_pins microblaze_0_axi_intc/interrupt]

  # Create port connections
  connect_bd_net -net Net [get_bd_pins clk_wiz_1/clk_out2] [get_bd_pins ila_0/clk] [get_bd_pins jesd204_phy_0/tx_core_clk] [get_bd_pins jesd204_tx/tx_core_clk]
  connect_bd_net -net Net1 [get_bd_pins clk_wiz_1/clk_out3] [get_bd_pins jesd204_phy_0/rx_core_clk] [get_bd_pins jesd204_rx/rx_core_clk]
  connect_bd_net -net axi_uartlite_0_interrupt [get_bd_pins axi_uartlite_0/interrupt] [get_bd_pins microblaze_0_xlconcat/In0]
  connect_bd_net -net clk_in0_n_1 [get_bd_ports clk_in0_n] [get_bd_pins clk_wiz_1/clk_in1_n]
  connect_bd_net -net clk_in0_p_1 [get_bd_ports clk_in0_p] [get_bd_pins clk_wiz_1/clk_in1_p]
  connect_bd_net -net clk_wiz_1_clk_out4 [get_bd_pins axi_quad_spi_0/ext_spi_clk] [get_bd_pins clk_wiz_1/clk_out4]
  connect_bd_net -net clk_wiz_1_locked [get_bd_pins clk_wiz_1/locked] [get_bd_pins rst_clk_wiz_1_100M/dcm_locked]
  connect_bd_net -net jesd204_1_rx_reset_gt [get_bd_pins jesd204_phy_0/rx_reset_gt] [get_bd_pins jesd204_rx/rx_reset_gt]
  connect_bd_net -net jesd204_1_rx_sync [get_bd_pins jesd204_rx/rx_sync] [get_bd_pins rx_sync_ds/OBUF_IN]
  connect_bd_net -net jesd204_1_rxencommaalign_out [get_bd_pins jesd204_phy_0/rxencommaalign] [get_bd_pins jesd204_rx/rxencommaalign_out]
  connect_bd_net -net jesd204_2_gt_prbssel_out [get_bd_pins jesd204_phy_0/gt_prbssel] [get_bd_pins jesd204_tx/gt_prbssel_out]
  connect_bd_net -net jesd204_2_tx_reset_gt [get_bd_pins jesd204_phy_0/tx_reset_gt] [get_bd_pins jesd204_tx/tx_reset_gt]
  connect_bd_net -net jesd204_phy_0_rx_reset_done [get_bd_pins jesd204_phy_0/rx_reset_done] [get_bd_pins jesd204_rx/rx_reset_done]
  connect_bd_net -net jesd204_phy_0_tx_reset_done [get_bd_pins jesd204_phy_0/tx_reset_done] [get_bd_pins jesd204_tx/tx_reset_done]
  connect_bd_net -net jesd204_phy_0_txn_out [get_bd_ports txn_out] [get_bd_pins jesd204_phy_0/txn_out]
  connect_bd_net -net jesd204_phy_0_txp_out [get_bd_ports txp_out] [get_bd_pins jesd204_phy_0/txp_out]
  connect_bd_net -net jesd204_tx_tx_start_of_frame [get_bd_pins ila_0/probe0] [get_bd_pins jesd204_tx/tx_start_of_frame]
  connect_bd_net -net lmk_clk_sysref_n_1 [get_bd_ports lmk_clk_sysref_n] [get_bd_pins lmk_sysref/IBUF_DS_N]
  connect_bd_net -net lmk_clk_sysref_p_1 [get_bd_ports lmk_clk_sysref_p] [get_bd_pins lmk_sysref/IBUF_DS_P]
  connect_bd_net -net lmk_sysref_IBUF_OUT [get_bd_pins lmk_sysref/IBUF_OUT] [get_bd_pins lmk_sysref_buf/BUFG_I]
  connect_bd_net -net lmk_sysref_buf_BUFG_O [get_bd_pins jesd204_rx/rx_sysref] [get_bd_pins jesd204_tx/tx_sysref] [get_bd_pins lmk_sysref_buf/BUFG_O]
  connect_bd_net -net mdm_1_debug_sys_rst [get_bd_pins mdm_1/Debug_SYS_Rst] [get_bd_pins rst_clk_wiz_1_100M/mb_debug_sys_rst]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins axi_gpio_0/s_axi_aclk] [get_bd_pins axi_quad_spi_0/s_axi4_aclk] [get_bd_pins axi_uartlite_0/s_axi_aclk] [get_bd_pins clk_wiz_1/clk_out1] [get_bd_pins jesd204_phy_0/drpclk] [get_bd_pins jesd204_phy_0/s_axi_aclk] [get_bd_pins jesd204_rx/s_axi_aclk] [get_bd_pins jesd204_tx/s_axi_aclk] [get_bd_pins microblaze_0/Clk] [get_bd_pins microblaze_0_axi_intc/processor_clk] [get_bd_pins microblaze_0_axi_intc/s_axi_aclk] [get_bd_pins microblaze_0_axi_periph/ACLK] [get_bd_pins microblaze_0_axi_periph/M00_ACLK] [get_bd_pins microblaze_0_axi_periph/M01_ACLK] [get_bd_pins microblaze_0_axi_periph/M02_ACLK] [get_bd_pins microblaze_0_axi_periph/M03_ACLK] [get_bd_pins microblaze_0_axi_periph/M04_ACLK] [get_bd_pins microblaze_0_axi_periph/M05_ACLK] [get_bd_pins microblaze_0_axi_periph/M06_ACLK] [get_bd_pins microblaze_0_axi_periph/S00_ACLK] [get_bd_pins microblaze_0_local_memory/LMB_Clk] [get_bd_pins rst_clk_wiz_1_100M/slowest_sync_clk]
  connect_bd_net -net microblaze_0_intr [get_bd_pins microblaze_0_axi_intc/intr] [get_bd_pins microblaze_0_xlconcat/dout]
  connect_bd_net -net ref_clk_IBUF_OUT [get_bd_pins jesd204_phy_0/cpll_refclk] [get_bd_pins ref_clk/IBUF_OUT]
  connect_bd_net -net ref_clk_n_1 [get_bd_ports gt_ref_clk_n] [get_bd_pins ref_clk/IBUF_DS_N]
  connect_bd_net -net ref_clk_p_1 [get_bd_ports gt_ref_clk_p] [get_bd_pins ref_clk/IBUF_DS_P]
  connect_bd_net -net reset_rtl_0_1 [get_bd_ports reset_rtl_0] [get_bd_pins clk_wiz_1/reset] [get_bd_pins jesd204_phy_0/rx_sys_reset] [get_bd_pins jesd204_phy_0/tx_sys_reset] [get_bd_pins jesd204_rx/rx_reset] [get_bd_pins jesd204_tx/tx_reset] [get_bd_pins rst_clk_wiz_1_100M/ext_reset_in]
  connect_bd_net -net rst_clk_wiz_1_100M_bus_struct_reset [get_bd_pins microblaze_0_local_memory/SYS_Rst] [get_bd_pins rst_clk_wiz_1_100M/bus_struct_reset]
  connect_bd_net -net rst_clk_wiz_1_100M_interconnect_aresetn [get_bd_pins microblaze_0_axi_periph/ARESETN] [get_bd_pins rst_clk_wiz_1_100M/interconnect_aresetn]
  connect_bd_net -net rst_clk_wiz_1_100M_mb_reset [get_bd_pins microblaze_0/Reset] [get_bd_pins microblaze_0_axi_intc/processor_rst] [get_bd_pins rst_clk_wiz_1_100M/mb_reset]
  connect_bd_net -net rst_clk_wiz_1_100M_peripheral_aresetn [get_bd_pins axi_gpio_0/s_axi_aresetn] [get_bd_pins axi_quad_spi_0/s_axi4_aresetn] [get_bd_pins axi_uartlite_0/s_axi_aresetn] [get_bd_pins jesd204_phy_0/s_axi_aresetn] [get_bd_pins jesd204_rx/s_axi_aresetn] [get_bd_pins jesd204_tx/s_axi_aresetn] [get_bd_pins microblaze_0_axi_intc/s_axi_aresetn] [get_bd_pins microblaze_0_axi_periph/M00_ARESETN] [get_bd_pins microblaze_0_axi_periph/M01_ARESETN] [get_bd_pins microblaze_0_axi_periph/M02_ARESETN] [get_bd_pins microblaze_0_axi_periph/M03_ARESETN] [get_bd_pins microblaze_0_axi_periph/M04_ARESETN] [get_bd_pins microblaze_0_axi_periph/M05_ARESETN] [get_bd_pins microblaze_0_axi_periph/M06_ARESETN] [get_bd_pins microblaze_0_axi_periph/S00_ARESETN] [get_bd_pins rst_clk_wiz_1_100M/peripheral_aresetn]
  connect_bd_net -net rx_sync_ds_OBUF_DS_N [get_bd_ports rx_sync_n] [get_bd_pins rx_sync_ds/OBUF_DS_N]
  connect_bd_net -net rx_sync_ds_OBUF_DS_P [get_bd_ports rx_sync_p] [get_bd_pins rx_sync_ds/OBUF_DS_P]
  connect_bd_net -net rxn_in_1 [get_bd_ports rxn_in] [get_bd_pins jesd204_phy_0/rxn_in]
  connect_bd_net -net rxp_in_1 [get_bd_ports rxp_in] [get_bd_pins jesd204_phy_0/rxp_in]
  connect_bd_net -net tx_core_clk1_IBUF_OUT [get_bd_pins jesd204_tx/tx_sync] [get_bd_pins tx_sync_ds/IBUF_OUT]
  connect_bd_net -net tx_sync_n_1 [get_bd_ports tx_sync_n] [get_bd_pins tx_sync_ds/IBUF_DS_N]
  connect_bd_net -net tx_sync_p_1 [get_bd_ports tx_sync_p] [get_bd_pins tx_sync_ds/IBUF_DS_P]
  connect_bd_net -net xlconstant_1_dout [get_bd_pins microblaze_0_xlconcat/In1] [get_bd_pins xlconstant_1/dout]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x40000000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_gpio_0/S_AXI/Reg] SEG_axi_gpio_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A30000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_quad_spi_0/aximm/MEM0] SEG_axi_quad_spi_0_MEM0
  create_bd_addr_seg -range 0x00010000 -offset 0x40600000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_uartlite_0/S_AXI/Reg] SEG_axi_uartlite_0_Reg
  create_bd_addr_seg -range 0x00020000 -offset 0x00000000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs microblaze_0_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x00020000 -offset 0x00000000 [get_bd_addr_spaces microblaze_0/Instruction] [get_bd_addr_segs microblaze_0_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x00010000 -offset 0x44A10000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs jesd204_rx/s_axi/Reg] SEG_jesd204_1_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A20000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs jesd204_tx/s_axi/Reg] SEG_jesd204_2_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A00000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs jesd204_phy_0/s_axi/Reg] SEG_jesd204_phy_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x41200000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs microblaze_0_axi_intc/S_AXI/Reg] SEG_microblaze_0_axi_intc_Reg

  # Perform GUI Layout
  regenerate_bd_layout -layout_string {
   guistr: "# # String gsaved with Nlview 6.6.5b  2016-09-06 bk=1.3687 VDI=39 GEI=35 GUI=JA:1.6
#  -string -flagsOSRD
preplace port uart_rtl -pg 1 -y 1010 -defaultsOSRD
preplace port gpio_rtl -pg 1 -y 760 -defaultsOSRD
preplace port clk_in0_n -pg 1 -y 910 -defaultsOSRD
preplace port reset_rtl_0 -pg 1 -y 170 -defaultsOSRD
preplace port spi_rtl -pg 1 -y 880 -defaultsOSRD
preplace port clk_in0_p -pg 1 -y 930 -defaultsOSRD
preplace portBus tx_sync_p -pg 1 -y 1330 -defaultsOSRD
preplace portBus rx_sync_p -pg 1 -y 1140 -defaultsOSRD
preplace portBus lmk_clk_sysref_p -pg 1 -y 430 -defaultsOSRD
preplace portBus gt_ref_clk_n -pg 1 -y 1420 -defaultsOSRD
preplace portBus txn_out -pg 1 -y 190 -defaultsOSRD
preplace portBus rxn_in -pg 1 -y 230 -defaultsOSRD
preplace portBus gt_ref_clk_p -pg 1 -y 1400 -defaultsOSRD
preplace portBus txp_out -pg 1 -y 170 -defaultsOSRD
preplace portBus tx_sync_n -pg 1 -y 1350 -defaultsOSRD
preplace portBus rxp_in -pg 1 -y 210 -defaultsOSRD
preplace portBus rx_sync_n -pg 1 -y 1160 -defaultsOSRD
preplace portBus lmk_clk_sysref_n -pg 1 -y 450 -defaultsOSRD
preplace inst jesd204_phy_0 -pg 1 -lvl 6 -y 340 -defaultsOSRD
preplace inst ref_clk -pg 1 -lvl 5 -y 1400 -defaultsOSRD
preplace inst rx_sync_ds -pg 1 -lvl 7 -y 1170 -defaultsOSRD
preplace inst jesd204_tx -pg 1 -lvl 5 -y -10 -defaultsOSRD
preplace inst xlconstant_1 -pg 1 -lvl 1 -y 1270 -defaultsOSRD
preplace inst microblaze_0_axi_periph -pg 1 -lvl 5 -y 680 -defaultsOSRD
preplace inst axi_gpio_0 -pg 1 -lvl 7 -y 770 -defaultsOSRD
preplace inst microblaze_0_xlconcat -pg 1 -lvl 2 -y 1260 -defaultsOSRD
preplace inst lmk_sysref_buf -pg 1 -lvl 4 -y 430 -defaultsOSRD
preplace inst lmk_sysref -pg 1 -lvl 3 -y 430 -defaultsOSRD
preplace inst mdm_1 -pg 1 -lvl 3 -y 1260 -defaultsOSRD
preplace inst microblaze_0_axi_intc -pg 1 -lvl 3 -y 1080 -defaultsOSRD
preplace inst ila_0 -pg 1 -lvl 5 -y 300 -defaultsOSRD
preplace inst jesd204_rx -pg 1 -lvl 7 -y 60 -defaultsOSRD
preplace inst axi_uartlite_0 -pg 1 -lvl 7 -y 1040 -defaultsOSRD
preplace inst tx_sync_ds -pg 1 -lvl 4 -y 1330 -defaultsOSRD
preplace inst microblaze_0 -pg 1 -lvl 4 -y 1110 -defaultsOSRD
preplace inst rst_clk_wiz_1_100M -pg 1 -lvl 2 -y 1030 -defaultsOSRD
preplace inst clk_wiz_1 -pg 1 -lvl 1 -y 920 -defaultsOSRD
preplace inst microblaze_0_local_memory -pg 1 -lvl 5 -y 1120 -defaultsOSRD
preplace inst axi_quad_spi_0 -pg 1 -lvl 7 -y 910 -defaultsOSRD
preplace netloc axi_quad_spi_0_SPI_0 1 7 1 3140J
preplace netloc xlconstant_1_dout 1 1 1 NJ
preplace netloc lmk_sysref_buf_BUFG_O 1 4 3 1510J 130 NJ 130 NJ
preplace netloc jesd204_1_m_axis_rx 1 4 4 1600 -140 NJ -140 NJ -140 3140
preplace netloc microblaze_0_axi_periph_M04_AXI 1 5 2 NJ 700 2670J
preplace netloc jesd204_1_rxencommaalign_out 1 5 3 2080 -80 NJ -80 3130
preplace netloc tx_sync_p_1 1 0 4 NJ 1330 NJ 1330 NJ 1330 NJ
preplace netloc rx_sync_ds_OBUF_DS_N 1 7 1 3150J
preplace netloc lmk_clk_sysref_p_1 1 0 3 NJ 430 NJ 430 NJ
preplace netloc jesd204_1_rx_sync 1 6 2 2710 220 3130
preplace netloc axi_uartlite_0_interrupt 1 1 7 250 1410 NJ 1410 NJ 1410 1600J 1290 NJ 1290 NJ 1290 3130
preplace netloc clk_in0_p_1 1 0 1 NJ
preplace netloc microblaze_0_intr 1 2 1 710
preplace netloc rx_sync_ds_OBUF_DS_P 1 7 1 3140J
preplace netloc ref_clk_n_1 1 0 5 NJ 1420 NJ 1420 NJ 1420 NJ 1420 NJ
preplace netloc ref_clk_IBUF_OUT 1 5 1 2010J
preplace netloc microblaze_0_Clk 1 1 6 240 940 720 940 1010 940 1530 940 2030 940 2690
preplace netloc microblaze_0_axi_periph_M03_AXI 1 4 2 1590 150 1950
preplace netloc microblaze_0_axi_periph_M06_AXI 1 5 2 NJ 740 2650J
preplace netloc microblaze_0_intc_axi 1 2 4 730 500 NJ 500 1510J 440 1940
preplace netloc microblaze_0_interrupt 1 3 1 N
preplace netloc clk_in0_n_1 1 0 1 NJ
preplace netloc rxp_in_1 1 0 6 NJ 210 NJ 210 NJ 210 NJ 210 NJ 210 2000J
preplace netloc microblaze_0_ilmb_1 1 4 1 N
preplace netloc tx_sync_n_1 1 0 4 NJ 1350 NJ 1350 NJ 1350 NJ
preplace netloc jesd204_phy_0_gt0_rx 1 6 1 2650
preplace netloc microblaze_0_axi_periph_M05_AXI 1 5 2 NJ 720 2680J
preplace netloc microblaze_0_axi_dp 1 4 1 1520
preplace netloc jesd204_2_tx_reset_gt 1 5 1 2040J
preplace netloc rxn_in_1 1 0 6 NJ 230 NJ 230 NJ 230 NJ 230 NJ 230 1970J
preplace netloc rst_clk_wiz_1_100M_interconnect_aresetn 1 2 3 690J 540 NJ 540 NJ
preplace netloc rst_clk_wiz_1_100M_bus_struct_reset 1 2 3 680J 1200 NJ 1200 1600J
preplace netloc microblaze_0_axi_periph_M01_AXI 1 5 1 1990J
preplace netloc jesd204_tx_tx_start_of_frame 1 4 2 1570 -150 1950
preplace netloc rst_clk_wiz_1_100M_peripheral_aresetn 1 2 5 710 860 NJ 860 1540 430 1960 540 2700
preplace netloc rst_clk_wiz_1_100M_mb_reset 1 2 2 700 1190 1030J
preplace netloc clk_wiz_1_locked 1 1 1 220
preplace netloc axi_gpio_0_GPIO 1 7 1 3140J
preplace netloc axi_uartlite_0_UART 1 7 1 3140J
preplace netloc reset_rtl_0_1 1 0 7 -70 120 250 120 NJ 120 NJ 120 1550J 120 2050 120 2680J
preplace netloc tx_core_clk1_IBUF_OUT 1 4 1 1560J
preplace netloc ref_clk_p_1 1 0 5 NJ 1400 NJ 1400 NJ 1400 NJ 1400 NJ
preplace netloc lmk_clk_sysref_n_1 1 0 3 NJ 450 NJ 450 NJ
preplace netloc microblaze_0_axi_periph_M02_AXI 1 5 2 NJ 660 2660J
preplace netloc microblaze_0_dlmb_1 1 4 1 N
preplace netloc jesd204_2_gt_prbssel_out 1 5 1 1980J
preplace netloc jesd204_phy_0_tx_reset_done 1 4 3 1580 -160 NJ -160 2640
preplace netloc jesd204_phy_0_rx_reset_done 1 6 1 2670
preplace netloc Net1 1 1 6 220J 360 NJ 360 NJ 360 NJ 360 2020J 50 NJ
preplace netloc Net 1 1 5 NJ 900 NJ 900 NJ 900 1500J 370 1960
preplace netloc microblaze_0_debug 1 3 1 1020
preplace netloc jesd204_phy_0_txp_out 1 6 2 N 310 3140J
preplace netloc clk_wiz_1_clk_out4 1 1 6 230J 920 NJ 920 NJ 920 NJ 920 NJ 920 2660
preplace netloc lmk_sysref_IBUF_OUT 1 3 1 NJ
preplace netloc mdm_1_debug_sys_rst 1 1 3 230 1320 NJ 1320 1020
preplace netloc jesd204_2_gt0_tx 1 5 1 2060J
preplace netloc jesd204_phy_0_txn_out 1 6 2 N 330 3150J
preplace netloc jesd204_1_rx_reset_gt 1 5 3 2070 -90 NJ -90 3150
levelinfo -pg 1 -90 120 510 870 1270 1780 2480 2970 3180 -top -170 -bot 2170
",
}

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


