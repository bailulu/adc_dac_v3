-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
-- Date        : Wed Mar 21 19:58:39 2018
-- Host        : lu running 64-bit Ubuntu 14.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top Jesd204_microblaze_microblaze_0_xlconcat_0 -prefix
--               Jesd204_microblaze_microblaze_0_xlconcat_0_ Jesd204_microblaze_microblaze_0_xlconcat_0_sim_netlist.vhdl
-- Design      : Jesd204_microblaze_microblaze_0_xlconcat_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7vx690tffg1761-3
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Jesd204_microblaze_microblaze_0_xlconcat_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    dout : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of Jesd204_microblaze_microblaze_0_xlconcat_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Jesd204_microblaze_microblaze_0_xlconcat_0 : entity is "Jesd204_microblaze_microblaze_0_xlconcat_0,xlconcat,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Jesd204_microblaze_microblaze_0_xlconcat_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Jesd204_microblaze_microblaze_0_xlconcat_0 : entity is "xlconcat,Vivado 2016.4";
end Jesd204_microblaze_microblaze_0_xlconcat_0;

architecture STRUCTURE of Jesd204_microblaze_microblaze_0_xlconcat_0 is
  signal \^in0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^in1\ : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  \^in0\(0) <= In0(0);
  \^in1\(0) <= In1(0);
  dout(1) <= \^in1\(0);
  dout(0) <= \^in0\(0);
end STRUCTURE;
