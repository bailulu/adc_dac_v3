// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
// Date        : Wed Mar 21 19:59:53 2018
// Host        : lu running 64-bit Ubuntu 14.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top Jesd204_microblaze_jesd204_phy_0_0 -prefix
//               Jesd204_microblaze_jesd204_phy_0_0_ Jesd204_microblaze_jesd204_phy_0_0_sim_netlist.v
// Design      : Jesd204_microblaze_jesd204_phy_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7vx690tffg1761-3
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "jesd204_phy_v3_2_1,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module Jesd204_microblaze_jesd204_phy_0_0
   (s_axi_aclk,
    s_axi_aresetn,
    s_axi_awaddr,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready,
    gt0_txresetdone_out,
    gt0_rxresetdone_out,
    gt0_cplllock_out,
    gt0_eyescandataerror_out,
    gt0_eyescantrigger_in,
    gt0_eyescanreset_in,
    gt0_txprbsforceerr_in,
    gt0_txpcsreset_in,
    gt0_txpmareset_in,
    gt0_txbufstatus_out,
    gt0_rxcdrhold_in,
    gt0_rxprbserr_out,
    gt0_rxprbssel_in,
    gt0_rxprbscntreset_in,
    gt0_rxbufreset_in,
    gt0_rxbufstatus_out,
    gt0_rxstatus_out,
    gt0_rxbyteisaligned_out,
    gt0_rxbyterealign_out,
    gt0_rxcommadet_out,
    gt0_dmonitorout_out,
    gt0_rxpcsreset_in,
    gt0_rxpmareset_in,
    gt0_rxmonitorout_out,
    gt0_rxmonitorsel_in,
    tx_sys_reset,
    rx_sys_reset,
    tx_reset_gt,
    rx_reset_gt,
    tx_reset_done,
    rx_reset_done,
    cpll_refclk,
    rxencommaalign,
    tx_core_clk,
    txoutclk,
    rx_core_clk,
    rxoutclk,
    drpclk,
    gt_prbssel,
    gt0_txdata,
    gt0_txcharisk,
    gt0_rxdata,
    gt0_rxcharisk,
    gt0_rxdisperr,
    gt0_rxnotintable,
    rxn_in,
    rxp_in,
    txn_out,
    txp_out);
  input s_axi_aclk;
  input s_axi_aresetn;
  input [11:0]s_axi_awaddr;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input s_axi_wvalid;
  output s_axi_wready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [11:0]s_axi_araddr;
  input s_axi_arvalid;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input s_axi_rready;
  output gt0_txresetdone_out;
  output gt0_rxresetdone_out;
  output gt0_cplllock_out;
  output gt0_eyescandataerror_out;
  input gt0_eyescantrigger_in;
  input gt0_eyescanreset_in;
  input gt0_txprbsforceerr_in;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  output [1:0]gt0_txbufstatus_out;
  input gt0_rxcdrhold_in;
  output gt0_rxprbserr_out;
  input [2:0]gt0_rxprbssel_in;
  input gt0_rxprbscntreset_in;
  input gt0_rxbufreset_in;
  output [2:0]gt0_rxbufstatus_out;
  output [2:0]gt0_rxstatus_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  output [14:0]gt0_dmonitorout_out;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  output [6:0]gt0_rxmonitorout_out;
  input [1:0]gt0_rxmonitorsel_in;
  input tx_sys_reset;
  input rx_sys_reset;
  input tx_reset_gt;
  input rx_reset_gt;
  output tx_reset_done;
  output rx_reset_done;
  input cpll_refclk;
  input rxencommaalign;
  input tx_core_clk;
  output txoutclk;
  input rx_core_clk;
  output rxoutclk;
  input drpclk;
  input [2:0]gt_prbssel;
  input [31:0]gt0_txdata;
  input [3:0]gt0_txcharisk;
  output [31:0]gt0_rxdata;
  output [3:0]gt0_rxcharisk;
  output [3:0]gt0_rxdisperr;
  output [3:0]gt0_rxnotintable;
  input [0:0]rxn_in;
  input [0:0]rxp_in;
  output [0:0]txn_out;
  output [0:0]txp_out;

  wire cpll_refclk;
  wire drpclk;
  wire gt0_cplllock_out;
  wire [14:0]gt0_dmonitorout_out;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_rxbufreset_in;
  wire [2:0]gt0_rxbufstatus_out;
  wire gt0_rxbyteisaligned_out;
  wire gt0_rxbyterealign_out;
  wire gt0_rxcdrhold_in;
  wire [3:0]gt0_rxcharisk;
  wire gt0_rxcommadet_out;
  wire [31:0]gt0_rxdata;
  wire [3:0]gt0_rxdisperr;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire [3:0]gt0_rxnotintable;
  wire gt0_rxpcsreset_in;
  wire gt0_rxpmareset_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_txbufstatus_out;
  wire [3:0]gt0_txcharisk;
  wire [31:0]gt0_txdata;
  wire gt0_txpcsreset_in;
  wire gt0_txpmareset_in;
  wire gt0_txprbsforceerr_in;
  wire gt0_txresetdone_out;
  wire [2:0]gt_prbssel;
  wire rx_core_clk;
  wire rx_reset_done;
  wire rx_reset_gt;
  wire rx_sys_reset;
  wire rxencommaalign;
  wire [0:0]rxn_in;
  wire rxoutclk;
  wire [0:0]rxp_in;
  wire s_axi_aclk;
  wire [11:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [11:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [1:0]s_axi_bresp;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire [1:0]s_axi_rresp;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire tx_core_clk;
  wire tx_reset_done;
  wire tx_reset_gt;
  wire tx_sys_reset;
  wire [0:0]txn_out;
  wire txoutclk;
  wire [0:0]txp_out;

  (* downgradeipidentifiedwarnings = "yes" *) 
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_support inst
       (.cpll_refclk(cpll_refclk),
        .drpclk(drpclk),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_rxbufreset_in(gt0_rxbufreset_in),
        .gt0_rxbufstatus_out(gt0_rxbufstatus_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxbyterealign_out(gt0_rxbyterealign_out),
        .gt0_rxcdrhold_in(gt0_rxcdrhold_in),
        .gt0_rxcharisk(gt0_rxcharisk),
        .gt0_rxcommadet_out(gt0_rxcommadet_out),
        .gt0_rxdata(gt0_rxdata),
        .gt0_rxdisperr(gt0_rxdisperr),
        .gt0_rxmonitorout_out(gt0_rxmonitorout_out),
        .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),
        .gt0_rxnotintable(gt0_rxnotintable),
        .gt0_rxpcsreset_in(gt0_rxpcsreset_in),
        .gt0_rxpmareset_in(gt0_rxpmareset_in),
        .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),
        .gt0_rxprbserr_out(gt0_rxprbserr_out),
        .gt0_rxprbssel_in(gt0_rxprbssel_in),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_txbufstatus_out(gt0_txbufstatus_out),
        .gt0_txcharisk(gt0_txcharisk),
        .gt0_txdata(gt0_txdata),
        .gt0_txpcsreset_in(gt0_txpcsreset_in),
        .gt0_txpmareset_in(gt0_txpmareset_in),
        .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),
        .gt0_txresetdone_out(gt0_txresetdone_out),
        .gt_prbssel(gt_prbssel),
        .rx_core_clk(rx_core_clk),
        .rx_reset_done(rx_reset_done),
        .rx_reset_gt(rx_reset_gt),
        .rx_sys_reset(rx_sys_reset),
        .rxencommaalign(rxencommaalign),
        .rxn_in(rxn_in),
        .rxoutclk(rxoutclk),
        .rxp_in(rxp_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bresp(s_axi_bresp),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rresp(s_axi_rresp),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wvalid(s_axi_wvalid),
        .tx_core_clk(tx_core_clk),
        .tx_reset_done(tx_reset_done),
        .tx_reset_gt(tx_reset_gt),
        .tx_sys_reset(tx_sys_reset),
        .txn_out(txn_out),
        .txoutclk(txoutclk),
        .txp_out(txp_out));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_block
   (s_axi_arready,
    s_axi_rvalid,
    gt0_cplllock_out,
    gt0_eyescandataerror_out,
    gt0_dmonitorout_out,
    gt0_rxdata,
    gt0_rxprbserr_out,
    gt0_rxdisperr,
    gt0_rxnotintable,
    gt0_rxbufstatus_out,
    gt0_rxstatus_out,
    gt0_rxbyteisaligned_out,
    gt0_rxbyterealign_out,
    gt0_rxcommadet_out,
    gt0_rxmonitorout_out,
    rxoutclk,
    gt0_rxcharisk,
    gt0_rxresetdone_out,
    gt0_txbufstatus_out,
    txn_out,
    txp_out,
    txoutclk,
    gt0_txresetdone_out,
    s_axi_wready,
    s_axi_rdata,
    tx_reset_done,
    rx_reset_done,
    s_axi_awready,
    s_axi_bvalid,
    s_axi_bresp,
    s_axi_rresp,
    s_axi_arvalid,
    s_axi_aresetn,
    s_axi_wvalid,
    tx_reset_gt,
    drpclk,
    rx_reset_gt,
    cpll_refclk,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    gt0_rxcdrhold_in,
    rx_core_clk,
    gt0_rxprbssel_in,
    gt0_rxprbscntreset_in,
    rxn_in,
    gt0_rxbufreset_in,
    rxencommaalign,
    gt0_rxmonitorsel_in,
    gt0_rxpcsreset_in,
    gt0_rxpmareset_in,
    rxp_in,
    tx_core_clk,
    gt0_txprbsforceerr_in,
    gt0_txdata,
    gt0_txpcsreset_in,
    gt0_txpmareset_in,
    gt_prbssel,
    gt0_txcharisk,
    common0_qpll_clk_in,
    common0_qpll_refclk_in,
    s_axi_aclk,
    s_axi_wdata,
    s_axi_awvalid,
    s_axi_araddr,
    s_axi_awaddr,
    s_axi_bready,
    s_axi_rready,
    tx_sys_reset,
    rx_sys_reset);
  output s_axi_arready;
  output s_axi_rvalid;
  output gt0_cplllock_out;
  output gt0_eyescandataerror_out;
  output [14:0]gt0_dmonitorout_out;
  output [31:0]gt0_rxdata;
  output gt0_rxprbserr_out;
  output [3:0]gt0_rxdisperr;
  output [3:0]gt0_rxnotintable;
  output [2:0]gt0_rxbufstatus_out;
  output [2:0]gt0_rxstatus_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  output [6:0]gt0_rxmonitorout_out;
  output rxoutclk;
  output [3:0]gt0_rxcharisk;
  output gt0_rxresetdone_out;
  output [1:0]gt0_txbufstatus_out;
  output [0:0]txn_out;
  output [0:0]txp_out;
  output txoutclk;
  output gt0_txresetdone_out;
  output s_axi_wready;
  output [24:0]s_axi_rdata;
  output tx_reset_done;
  output rx_reset_done;
  output s_axi_awready;
  output s_axi_bvalid;
  output [0:0]s_axi_bresp;
  output [0:0]s_axi_rresp;
  input s_axi_arvalid;
  input s_axi_aresetn;
  input s_axi_wvalid;
  input tx_reset_gt;
  input drpclk;
  input rx_reset_gt;
  input cpll_refclk;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input gt0_rxcdrhold_in;
  input rx_core_clk;
  input [2:0]gt0_rxprbssel_in;
  input gt0_rxprbscntreset_in;
  input [0:0]rxn_in;
  input gt0_rxbufreset_in;
  input rxencommaalign;
  input [1:0]gt0_rxmonitorsel_in;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  input [0:0]rxp_in;
  input tx_core_clk;
  input gt0_txprbsforceerr_in;
  input [31:0]gt0_txdata;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  input [2:0]gt_prbssel;
  input [3:0]gt0_txcharisk;
  input common0_qpll_clk_in;
  input common0_qpll_refclk_in;
  input s_axi_aclk;
  input [17:0]s_axi_wdata;
  input s_axi_awvalid;
  input [9:0]s_axi_araddr;
  input [9:0]s_axi_awaddr;
  input s_axi_bready;
  input s_axi_rready;
  input tx_sys_reset;
  input rx_sys_reset;

  wire common0_qpll_clk_in;
  wire common0_qpll_refclk_in;
  wire cpll_lock_axi;
  wire cpll_lock_r;
  wire cpll_lock_sync;
  wire cpll_refclk;
  wire cpllpd_i;
  wire data_in0;
  wire data_in00_out;
  wire drpclk;
  wire gt0_cplllock_out;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr;
  wire [15:0]gt0_drpdi;
  wire [15:0]gt0_drpdo;
  wire gt0_drpen;
  wire gt0_drprdy;
  wire gt0_drpwe;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire [2:0]gt0_loopback_in;
  wire gt0_rxbufreset_in;
  wire [2:0]gt0_rxbufstatus_out;
  wire gt0_rxbyteisaligned_out;
  wire gt0_rxbyterealign_out;
  wire gt0_rxcdrhold_in;
  wire [3:0]gt0_rxcharisk;
  wire gt0_rxcommadet_out;
  wire [31:0]gt0_rxdata;
  wire gt0_rxdfelpmreset_in;
  wire [3:0]gt0_rxdisperr;
  wire gt0_rxlpmen_in;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire [3:0]gt0_rxnotintable;
  wire gt0_rxpcsreset_in;
  wire [1:0]gt0_rxpd_in;
  wire gt0_rxpmareset_in;
  wire gt0_rxpolarity_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_rxsysclksel_i;
  wire [1:0]gt0_txbufstatus_out;
  wire [3:0]gt0_txcharisk;
  wire [31:0]gt0_txdata;
  wire [3:0]gt0_txdiffctrl_in;
  wire gt0_txinhibit_in;
  wire gt0_txpcsreset_in;
  wire [1:0]gt0_txpd_in;
  wire gt0_txpmareset_in;
  wire gt0_txpolarity_in;
  wire [4:0]gt0_txpostcursor_in;
  wire gt0_txprbsforceerr_in;
  wire [4:0]gt0_txprecursor_in;
  wire gt0_txresetdone_out;
  wire [1:0]gt0_txsysclksel_i;
  wire [2:0]gt_prbssel;
  wire gt_rxfsmdone;
  wire gt_rxreset;
  wire gt_rxreset0;
  wire gt_txfsmdone;
  wire gt_txreset;
  wire gt_txreset0;
  wire rx_chan_rst_done_r;
  wire rx_core_clk;
  wire rx_pll_lock_i_reg_n_0;
  wire rx_pll_lock_sync;
  wire rx_reset_done;
  wire rx_reset_done_axi;
  wire rx_reset_done_r0;
  wire rx_reset_gt;
  wire rx_rst_gt_data_sync;
  wire rx_sys_reset;
  wire rxencommaalign;
  wire [0:0]rxn_in;
  wire rxoutclk;
  wire [0:0]rxp_in;
  wire s_axi_aclk;
  wire [9:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [9:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [0:0]s_axi_bresp;
  wire s_axi_bvalid;
  wire [24:0]s_axi_rdata;
  wire s_axi_rready;
  wire [0:0]s_axi_rresp;
  wire s_axi_rvalid;
  wire [17:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire s_drp_reset;
  wire sync_drp_reset_i_n_0;
  wire sync_rx_core_reset_i_n_0;
  wire sync_tx_core_reset_i_n_0;
  wire tx_chan_rst_done_r;
  wire tx_core_clk;
  wire tx_pll_lock_sync;
  wire tx_reset_done;
  wire tx_reset_done_axi;
  wire tx_reset_done_r0;
  wire tx_reset_gt;
  wire tx_rst_gt_data_sync;
  wire tx_sys_reset;
  wire [0:0]txn_out;
  wire txoutclk;
  wire [0:0]txp_out;
  wire NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_cpllfbclklost_out_UNCONNECTED;
  wire NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_rxoutclkfabric_out_UNCONNECTED;
  wire NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_txoutclkfabric_out_UNCONNECTED;
  wire NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_txoutclkpcs_out_UNCONNECTED;
  wire [3:0]NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_rxchariscomma_out_UNCONNECTED;

  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "Jesd204_microblaze_jesd204_phy_0_0_gt,gtwizard_v3_6_5,{protocol_file=JESD204}" *) 
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt Jesd204_microblaze_jesd204_phy_0_0_gt
       (.DONT_RESET_ON_DATA_ERROR_IN(1'b0),
        .GT0_DATA_VALID_IN(1'b1),
        .GT0_QPLLOUTCLK_IN(common0_qpll_clk_in),
        .GT0_QPLLOUTREFCLK_IN(common0_qpll_refclk_in),
        .GT_RX_FSM_RESET_DONE_OUT(gt_rxfsmdone),
        .GT_TX_FSM_RESET_DONE_OUT(gt_txfsmdone),
        .SOFT_RESET_RX_IN(gt_rxreset),
        .SOFT_RESET_TX_IN(gt_txreset),
        .SYSCLK_IN(drpclk),
        .gt0_cpllfbclklost_out(NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_cpllfbclklost_out_UNCONNECTED),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_cplllockdetclk_in(1'b0),
        .gt0_cpllpd_in(cpllpd_i),
        .gt0_cpllreset_in(1'b0),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr_in(gt0_drpaddr),
        .gt0_drpclk_in(drpclk),
        .gt0_drpdi_in(gt0_drpdi),
        .gt0_drpdo_out(gt0_drpdo),
        .gt0_drpen_in(gt0_drpen),
        .gt0_drprdy_out(gt0_drprdy),
        .gt0_drpwe_in(gt0_drpwe),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_gthrxn_in(rxn_in),
        .gt0_gthrxp_in(rxp_in),
        .gt0_gthtxn_out(txn_out),
        .gt0_gthtxp_out(txp_out),
        .gt0_gtnorthrefclk0_in(1'b0),
        .gt0_gtnorthrefclk1_in(1'b0),
        .gt0_gtrefclk0_in(cpll_refclk),
        .gt0_gtrefclk1_in(1'b0),
        .gt0_gtrxreset_in(rx_rst_gt_data_sync),
        .gt0_gtsouthrefclk0_in(1'b0),
        .gt0_gtsouthrefclk1_in(1'b0),
        .gt0_gttxreset_in(tx_rst_gt_data_sync),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxbufreset_in(gt0_rxbufreset_in),
        .gt0_rxbufstatus_out(gt0_rxbufstatus_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxbyterealign_out(gt0_rxbyterealign_out),
        .gt0_rxcdrhold_in(gt0_rxcdrhold_in),
        .gt0_rxchariscomma_out(NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_rxchariscomma_out_UNCONNECTED[3:0]),
        .gt0_rxcharisk_out(gt0_rxcharisk),
        .gt0_rxcommadet_out(gt0_rxcommadet_out),
        .gt0_rxdata_out(gt0_rxdata),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxdisperr_out(gt0_rxdisperr),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxmcommaalignen_in(rxencommaalign),
        .gt0_rxmonitorout_out(gt0_rxmonitorout_out),
        .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),
        .gt0_rxnotintable_out(gt0_rxnotintable),
        .gt0_rxoutclk_out(rxoutclk),
        .gt0_rxoutclkfabric_out(NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_rxoutclkfabric_out_UNCONNECTED),
        .gt0_rxpcommaalignen_in(rxencommaalign),
        .gt0_rxpcsreset_in(gt0_rxpcsreset_in),
        .gt0_rxpd_in(gt0_rxpd_in),
        .gt0_rxpmareset_in(gt0_rxpmareset_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),
        .gt0_rxprbserr_out(gt0_rxprbserr_out),
        .gt0_rxprbssel_in(gt0_rxprbssel_in),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_i),
        .gt0_rxuserrdy_in(1'b1),
        .gt0_rxusrclk2_in(rx_core_clk),
        .gt0_rxusrclk_in(rx_core_clk),
        .gt0_txbufstatus_out(gt0_txbufstatus_out),
        .gt0_txcharisk_in(gt0_txcharisk),
        .gt0_txdata_in(gt0_txdata),
        .gt0_txdiffctrl_in(gt0_txdiffctrl_in),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txoutclk_out(txoutclk),
        .gt0_txoutclkfabric_out(NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_txoutclkfabric_out_UNCONNECTED),
        .gt0_txoutclkpcs_out(NLW_Jesd204_microblaze_jesd204_phy_0_0_gt_gt0_txoutclkpcs_out_UNCONNECTED),
        .gt0_txpcsreset_in(gt0_txpcsreset_in),
        .gt0_txpd_in(gt0_txpd_in),
        .gt0_txpmareset_in(gt0_txpmareset_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .gt0_txpostcursor_in(gt0_txpostcursor_in),
        .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),
        .gt0_txprbssel_in(gt_prbssel),
        .gt0_txprecursor_in(gt0_txprecursor_in),
        .gt0_txresetdone_out(gt0_txresetdone_out),
        .gt0_txsysclksel_in(gt0_txsysclksel_i),
        .gt0_txuserrdy_in(1'b1),
        .gt0_txusrclk2_in(tx_core_clk),
        .gt0_txusrclk_in(tx_core_clk));
  FDRE cpll_lock_r_reg
       (.C(drpclk),
        .CE(1'b1),
        .D(gt0_cplllock_out),
        .Q(cpll_lock_r),
        .R(1'b0));
  FDRE gt_rxreset_reg
       (.C(drpclk),
        .CE(1'b1),
        .D(gt_rxreset0),
        .Q(gt_rxreset),
        .R(1'b0));
  FDRE gt_txreset_reg
       (.C(drpclk),
        .CE(1'b1),
        .D(gt_txreset0),
        .Q(gt_txreset),
        .R(1'b0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface phyCoreCtrlInterface_i
       (.D(gt0_drpdo),
        .Q(gt0_drpaddr),
        .SR(sync_tx_core_reset_i_n_0),
        .\arststages_ff_reg[4] (data_in0),
        .data_in(data_in00_out),
        .data_out(s_drp_reset),
        .data_sync_reg1(gt0_drpdi),
        .data_sync_reg1_0(gt0_txpostcursor_in),
        .data_sync_reg1_1(gt0_txprecursor_in),
        .data_sync_reg_gsr(sync_rx_core_reset_i_n_0),
        .data_sync_reg_gsr_0(sync_drp_reset_i_n_0),
        .data_sync_reg_gsr_1(cpll_lock_axi),
        .data_sync_reg_gsr_2(rx_reset_done_axi),
        .data_sync_reg_gsr_3(tx_reset_done_axi),
        .drpclk(drpclk),
        .gt0_cpllpd_in(cpllpd_i),
        .gt0_drpen_in(gt0_drpen),
        .gt0_drprdy_out(gt0_drprdy),
        .gt0_drpwe_in(gt0_drpwe),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxpd_in(gt0_rxpd_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_i),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txpd_in(gt0_txpd_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .gt0_txsysclksel_in(gt0_txsysclksel_i),
        .rx_core_clk(rx_core_clk),
        .rx_sys_reset(rx_sys_reset),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bresp(s_axi_bresp),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rresp(s_axi_rresp),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wvalid(s_axi_wvalid),
        .\slv_rdata_reg[3] (gt0_txdiffctrl_in),
        .tx_core_clk(tx_core_clk),
        .tx_sys_reset(tx_sys_reset));
  FDRE rx_chan_rst_done_r_reg
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(gt0_rxresetdone_out),
        .Q(rx_chan_rst_done_r),
        .R(1'b0));
  FDRE rx_pll_lock_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(cpll_lock_sync),
        .Q(rx_pll_lock_i_reg_n_0),
        .R(1'b0));
  FDRE rx_reset_done_r_reg
       (.C(drpclk),
        .CE(1'b1),
        .D(rx_reset_done_r0),
        .Q(rx_reset_done),
        .R(1'b0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized0 sync_cpll_lock
       (.O54(cpll_lock_r),
        .data_out(cpll_lock_sync),
        .s_axi_aclk(s_axi_aclk));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__7 sync_cpll_lock_axi_i
       (.data_out(cpll_lock_axi),
        .gt0_cplllock_out(gt0_cplllock_out),
        .s_axi_aclk(s_axi_aclk));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__8 sync_drp_reset_i
       (.data_out(s_drp_reset),
        .\drp_read_data_reg[0] (sync_drp_reset_i_n_0),
        .drpclk(drpclk),
        .s_axi_aresetn(s_axi_aresetn));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized4 sync_rx_chan_rst_done
       (.GT_RX_FSM_RESET_DONE_OUT(gt_rxfsmdone),
        .data_in(rx_chan_rst_done_r),
        .drpclk(drpclk),
        .rx_reset_done_r0(rx_reset_done_r0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__9 sync_rx_core_reset_i
       (.clk2_ready_reg(sync_rx_core_reset_i_n_0),
        .rx_core_clk(rx_core_clk),
        .s_axi_aresetn(s_axi_aresetn));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized2 sync_rx_pll_lock
       (.data_in(rx_pll_lock_i_reg_n_0),
        .data_out(rx_pll_lock_sync),
        .drpclk(drpclk));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__1 sync_rx_reset_all
       (.GT_RX_FSM_RESET_DONE_OUT(gt_rxfsmdone),
        .data_in(data_in0),
        .data_out(rx_pll_lock_sync),
        .drpclk(drpclk),
        .gt_rxreset0(gt_rxreset0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__2 sync_rx_reset_data
       (.data_out(rx_rst_gt_data_sync),
        .drpclk(drpclk),
        .rx_reset_gt(rx_reset_gt));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__10 sync_rx_reset_done_axi_i
       (.data_out(rx_reset_done_axi),
        .rx_reset_done(rx_reset_done),
        .s_axi_aclk(s_axi_aclk));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized3 sync_tx_chan_rst_done
       (.GT_TX_FSM_RESET_DONE_OUT(gt_txfsmdone),
        .data_in(tx_chan_rst_done_r),
        .drpclk(drpclk),
        .tx_reset_done_r0(tx_reset_done_r0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__11 sync_tx_core_reset_i
       (.SR(sync_tx_core_reset_i_n_0),
        .s_axi_aresetn(s_axi_aresetn),
        .tx_core_clk(tx_core_clk));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized1 sync_tx_pll_lock
       (.data_in(rx_pll_lock_i_reg_n_0),
        .data_out(tx_pll_lock_sync),
        .drpclk(drpclk));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__3 sync_tx_reset_all
       (.GT_TX_FSM_RESET_DONE_OUT(gt_txfsmdone),
        .data_in(data_in00_out),
        .data_out(tx_pll_lock_sync),
        .drpclk(drpclk),
        .gt_txreset0(gt_txreset0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block sync_tx_reset_data
       (.data_out(tx_rst_gt_data_sync),
        .drpclk(drpclk),
        .tx_reset_gt(tx_reset_gt));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync sync_tx_reset_done_axi_i
       (.data_out(tx_reset_done_axi),
        .s_axi_aclk(s_axi_aclk),
        .tx_reset_done(tx_reset_done));
  FDRE tx_chan_rst_done_r_reg
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(gt0_txresetdone_out),
        .Q(tx_chan_rst_done_r),
        .R(1'b0));
  FDRE tx_reset_done_r_reg
       (.C(drpclk),
        .CE(1'b1),
        .D(tx_reset_done_r0),
        .Q(tx_reset_done),
        .R(1'b0));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox
   (gt_slv_rd_done,
    drp_reset,
    wait_for_drp,
    \axi_rdata_reg[11] ,
    data_sync_reg1,
    \axi_rdata_reg[15] ,
    \axi_rdata_reg[10] ,
    \axi_rdata_reg[9] ,
    \axi_rdata_reg[2] ,
    gt0_drpwe_in,
    gt0_drpen_in,
    \axi_bresp_reg[1] ,
    \axi_rdata_reg[8] ,
    data_sync_reg1_0,
    \axi_rdata_reg[7] ,
    \axi_rdata_reg[6] ,
    \axi_rdata_reg[5] ,
    \axi_rdata_reg[4] ,
    \axi_rdata_reg[3] ,
    \axi_rdata_reg[2]_0 ,
    \axi_rdata_reg[1] ,
    \axi_rdata_reg[0] ,
    \axi_rdata_reg[0]_0 ,
    \axi_rdata_reg[8]_0 ,
    p_0_in,
    s_axi_aclk,
    Q,
    gt_slv_rden,
    gt_slv_wren,
    drpclk,
    drp_reset_reg_0,
    gt_axi_map_wready_reg,
    gt0_drprdy_out,
    data_out,
    access_type5_out,
    s_axi_aresetn,
    s_axi_wdata,
    \slv_addr_reg[4] ,
    E,
    gt_axi_map_wready_reg_0,
    data_sync_reg_gsr,
    D,
    gt_axi_map_wready_reg_1);
  output gt_slv_rd_done;
  output drp_reset;
  output wait_for_drp;
  output \axi_rdata_reg[11] ;
  output [15:0]data_sync_reg1;
  output [3:0]\axi_rdata_reg[15] ;
  output \axi_rdata_reg[10] ;
  output \axi_rdata_reg[9] ;
  output \axi_rdata_reg[2] ;
  output gt0_drpwe_in;
  output gt0_drpen_in;
  output \axi_bresp_reg[1] ;
  output \axi_rdata_reg[8] ;
  output [8:0]data_sync_reg1_0;
  output \axi_rdata_reg[7] ;
  output \axi_rdata_reg[6] ;
  output \axi_rdata_reg[5] ;
  output \axi_rdata_reg[4] ;
  output \axi_rdata_reg[3] ;
  output \axi_rdata_reg[2]_0 ;
  output \axi_rdata_reg[1] ;
  output \axi_rdata_reg[0] ;
  output \axi_rdata_reg[0]_0 ;
  output [6:0]\axi_rdata_reg[8]_0 ;
  input p_0_in;
  input s_axi_aclk;
  input [0:0]Q;
  input gt_slv_rden;
  input gt_slv_wren;
  input drpclk;
  input drp_reset_reg_0;
  input gt_axi_map_wready_reg;
  input gt0_drprdy_out;
  input data_out;
  input access_type5_out;
  input s_axi_aresetn;
  input [17:0]s_axi_wdata;
  input [2:0]\slv_addr_reg[4] ;
  input [0:0]E;
  input [0:0]gt_axi_map_wready_reg_0;
  input [0:0]data_sync_reg_gsr;
  input [15:0]D;
  input [0:0]gt_axi_map_wready_reg_1;

  wire [15:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire access_type;
  wire access_type5_out;
  wire \axi_bresp_reg[1] ;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire \axi_rdata_reg[10] ;
  wire \axi_rdata_reg[11] ;
  wire [3:0]\axi_rdata_reg[15] ;
  wire \axi_rdata_reg[1] ;
  wire \axi_rdata_reg[2] ;
  wire \axi_rdata_reg[2]_0 ;
  wire \axi_rdata_reg[3] ;
  wire \axi_rdata_reg[4] ;
  wire \axi_rdata_reg[5] ;
  wire \axi_rdata_reg[6] ;
  wire \axi_rdata_reg[7] ;
  wire \axi_rdata_reg[8] ;
  wire [6:0]\axi_rdata_reg[8]_0 ;
  wire \axi_rdata_reg[9] ;
  wire clk2clk_handshake_pulse_gen_i_n_0;
  wire clk2clk_handshake_pulse_gen_i_n_1;
  wire clk2clk_handshake_pulse_gen_i_n_2;
  wire data_out;
  wire [15:0]data_sync_reg1;
  wire [8:0]data_sync_reg1_0;
  wire [0:0]data_sync_reg_gsr;
  wire drp_access_in_progress_i_3_n_0;
  wire drp_access_in_progress_reg_n_0;
  wire drp_access_valid_reg_n_0;
  wire drp_if_select;
  wire [11:0]drp_read_data;
  wire drp_read_data0;
  wire drp_reset;
  wire drp_reset_reg_0;
  wire drpclk;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire gt_axi_map_wready_reg;
  wire [0:0]gt_axi_map_wready_reg_0;
  wire [0:0]gt_axi_map_wready_reg_1;
  wire gt_slv_rd_done;
  wire gt_slv_rden;
  wire gt_slv_wren;
  wire p_0_in;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [17:0]s_axi_wdata;
  wire [2:0]\slv_addr_reg[4] ;
  wire \timeout_length_reg_n_0_[0] ;
  wire \timeout_length_reg_n_0_[10] ;
  wire \timeout_length_reg_n_0_[11] ;
  wire \timeout_length_reg_n_0_[2] ;
  wire \timeout_length_reg_n_0_[9] ;
  wire wait_for_drp;
  wire wr_req_reg;

  FDRE access_type_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_1),
        .Q(access_type),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_11 
       (.I0(wait_for_drp),
        .I1(\timeout_length_reg_n_0_[0] ),
        .I2(\slv_addr_reg[4] [1]),
        .I3(drp_access_in_progress_reg_n_0),
        .I4(\slv_addr_reg[4] [0]),
        .I5(drp_reset),
        .O(\axi_rdata_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_12 
       (.I0(drp_read_data[0]),
        .I1(data_sync_reg1[0]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(data_sync_reg1_0[0]),
        .I4(\slv_addr_reg[4] [0]),
        .I5(drp_if_select),
        .O(\axi_rdata_reg[0] ));
  LUT6 #(
    .INIT(64'h0000F000CC00AA00)) 
    \axi_rdata[10]_i_2 
       (.I0(data_sync_reg1[10]),
        .I1(drp_read_data[10]),
        .I2(\timeout_length_reg_n_0_[10] ),
        .I3(\slv_addr_reg[4] [1]),
        .I4(\slv_addr_reg[4] [0]),
        .I5(\slv_addr_reg[4] [2]),
        .O(\axi_rdata_reg[10] ));
  LUT6 #(
    .INIT(64'hFF0F3F5FFFFF3F5F)) 
    \axi_rdata[11]_i_4 
       (.I0(data_sync_reg1[11]),
        .I1(drp_read_data[11]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(\slv_addr_reg[4] [0]),
        .I4(\slv_addr_reg[4] [2]),
        .I5(\timeout_length_reg_n_0_[11] ),
        .O(\axi_rdata_reg[11] ));
  LUT6 #(
    .INIT(64'h00000000F8C83808)) 
    \axi_rdata[1]_i_12 
       (.I0(data_sync_reg1_0[1]),
        .I1(\slv_addr_reg[4] [0]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(data_sync_reg1[1]),
        .I4(drp_read_data[1]),
        .I5(\slv_addr_reg[4] [2]),
        .O(\axi_rdata_reg[1] ));
  LUT4 #(
    .INIT(16'h3808)) 
    \axi_rdata[2]_i_11 
       (.I0(\timeout_length_reg_n_0_[2] ),
        .I1(\slv_addr_reg[4] [1]),
        .I2(\slv_addr_reg[4] [0]),
        .I3(access_type),
        .O(\axi_rdata_reg[2] ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[2]_i_12 
       (.I0(drp_read_data[2]),
        .I1(data_sync_reg1[2]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(\slv_addr_reg[4] [0]),
        .I4(data_sync_reg1_0[2]),
        .O(\axi_rdata_reg[2]_0 ));
  LUT6 #(
    .INIT(64'h00000000F8C83808)) 
    \axi_rdata[3]_i_13 
       (.I0(data_sync_reg1_0[3]),
        .I1(\slv_addr_reg[4] [0]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(data_sync_reg1[3]),
        .I4(drp_read_data[3]),
        .I5(\slv_addr_reg[4] [2]),
        .O(\axi_rdata_reg[3] ));
  LUT6 #(
    .INIT(64'h00000000F8C83808)) 
    \axi_rdata[4]_i_8 
       (.I0(data_sync_reg1_0[4]),
        .I1(\slv_addr_reg[4] [0]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(data_sync_reg1[4]),
        .I4(drp_read_data[4]),
        .I5(\slv_addr_reg[4] [2]),
        .O(\axi_rdata_reg[4] ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(drp_read_data[5]),
        .I1(data_sync_reg1[5]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(\slv_addr_reg[4] [0]),
        .I4(data_sync_reg1_0[5]),
        .O(\axi_rdata_reg[5] ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(drp_read_data[6]),
        .I1(data_sync_reg1[6]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(\slv_addr_reg[4] [0]),
        .I4(data_sync_reg1_0[6]),
        .O(\axi_rdata_reg[6] ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[7]_i_8 
       (.I0(drp_read_data[7]),
        .I1(data_sync_reg1[7]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(\slv_addr_reg[4] [0]),
        .I4(data_sync_reg1_0[7]),
        .O(\axi_rdata_reg[7] ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[8]_i_5 
       (.I0(drp_read_data[8]),
        .I1(data_sync_reg1[8]),
        .I2(\slv_addr_reg[4] [1]),
        .I3(\slv_addr_reg[4] [0]),
        .I4(data_sync_reg1_0[8]),
        .O(\axi_rdata_reg[8] ));
  LUT6 #(
    .INIT(64'hFF77FF77333FFF3F)) 
    \axi_rdata[9]_i_4 
       (.I0(\timeout_length_reg_n_0_[9] ),
        .I1(\slv_addr_reg[4] [1]),
        .I2(data_sync_reg1[9]),
        .I3(\slv_addr_reg[4] [0]),
        .I4(drp_read_data[9]),
        .I5(\slv_addr_reg[4] [2]),
        .O(\axi_rdata_reg[9] ));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__1 clk2clk_handshake_pulse_gen_i
       (.access_type(access_type),
        .access_type5_out(access_type5_out),
        .access_type_reg(clk2clk_handshake_pulse_gen_i_n_1),
        .\axi_bresp_reg[1] (\axi_bresp_reg[1] ),
        .data_in(drp_access_valid_reg_n_0),
        .data_out(data_out),
        .drp_access_in_progress_reg(clk2clk_handshake_pulse_gen_i_n_0),
        .drp_access_in_progress_reg_0(drp_access_in_progress_reg_n_0),
        .drp_access_valid_reg(clk2clk_handshake_pulse_gen_i_n_2),
        .drp_if_select(drp_if_select),
        .drpclk(drpclk),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drprdy_out(gt0_drprdy_out),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt_slv_wren(gt_slv_wren),
        .p_0_in(p_0_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_wdata(s_axi_wdata[17]),
        .\s_axi_wdata[30] (drp_access_in_progress_i_3_n_0),
        .wait_for_drp(wait_for_drp),
        .wr_req_reg(wr_req_reg));
  LUT2 #(
    .INIT(4'h6)) 
    drp_access_in_progress_i_3
       (.I0(s_axi_wdata[17]),
        .I1(s_axi_wdata[16]),
        .O(drp_access_in_progress_i_3_n_0));
  FDRE drp_access_in_progress_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_0),
        .Q(drp_access_in_progress_reg_n_0),
        .R(1'b0));
  FDRE drp_access_valid_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_2),
        .Q(drp_access_valid_reg_n_0),
        .R(1'b0));
  FDRE \drp_if_select_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(Q),
        .Q(drp_if_select),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[0]),
        .Q(data_sync_reg1_0[0]),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[1]),
        .Q(data_sync_reg1_0[1]),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[2]),
        .Q(data_sync_reg1_0[2]),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[3]),
        .Q(data_sync_reg1_0[3]),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[4]),
        .Q(data_sync_reg1_0[4]),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[5]),
        .Q(data_sync_reg1_0[5]),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[6]),
        .Q(data_sync_reg1_0[6]),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[7]),
        .Q(data_sync_reg1_0[7]),
        .R(p_0_in));
  FDRE \drp_int_addr_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[8]),
        .Q(data_sync_reg1_0[8]),
        .R(p_0_in));
  LUT3 #(
    .INIT(8'h04)) 
    \drp_read_data[15]_i_2 
       (.I0(drp_if_select),
        .I1(gt0_drprdy_out),
        .I2(access_type),
        .O(drp_read_data0));
  FDRE \drp_read_data_reg[0] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[0]),
        .Q(drp_read_data[0]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[10] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[10]),
        .Q(drp_read_data[10]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[11] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[11]),
        .Q(drp_read_data[11]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[12] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[12]),
        .Q(\axi_rdata_reg[15] [0]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[13] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[13]),
        .Q(\axi_rdata_reg[15] [1]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[14] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[14]),
        .Q(\axi_rdata_reg[15] [2]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[15] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[15]),
        .Q(\axi_rdata_reg[15] [3]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[1] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[1]),
        .Q(drp_read_data[1]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[2] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[2]),
        .Q(drp_read_data[2]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[3] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[3]),
        .Q(drp_read_data[3]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[4] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[4]),
        .Q(drp_read_data[4]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[5] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[5]),
        .Q(drp_read_data[5]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[6] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[6]),
        .Q(drp_read_data[6]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[7] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[7]),
        .Q(drp_read_data[7]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[8] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[8]),
        .Q(drp_read_data[8]),
        .R(data_sync_reg_gsr));
  FDRE \drp_read_data_reg[9] 
       (.C(drpclk),
        .CE(drp_read_data0),
        .D(D[9]),
        .Q(drp_read_data[9]),
        .R(data_sync_reg_gsr));
  FDRE drp_reset_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(drp_reset_reg_0),
        .Q(drp_reset),
        .R(1'b0));
  FDRE \drp_write_data_reg[0] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[0]),
        .Q(data_sync_reg1[0]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[10] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[10]),
        .Q(data_sync_reg1[10]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[11] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[11]),
        .Q(data_sync_reg1[11]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[12] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[12]),
        .Q(data_sync_reg1[12]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[13] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[13]),
        .Q(data_sync_reg1[13]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[14] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[14]),
        .Q(data_sync_reg1[14]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[15] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[15]),
        .Q(data_sync_reg1[15]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[1] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[1]),
        .Q(data_sync_reg1[1]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[2] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[2]),
        .Q(data_sync_reg1[2]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[3] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[3]),
        .Q(data_sync_reg1[3]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[4] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[4]),
        .Q(data_sync_reg1[4]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[5] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[5]),
        .Q(data_sync_reg1[5]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[6] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[6]),
        .Q(data_sync_reg1[6]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[7] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[7]),
        .Q(data_sync_reg1[7]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[8] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[8]),
        .Q(data_sync_reg1[8]),
        .R(p_0_in));
  FDRE \drp_write_data_reg[9] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_0),
        .D(s_axi_wdata[9]),
        .Q(data_sync_reg1[9]),
        .R(p_0_in));
  FDRE rd_req_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(gt_slv_rden),
        .Q(gt_slv_rd_done),
        .R(p_0_in));
  FDRE \timeout_length_reg[0] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[0]),
        .Q(\timeout_length_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \timeout_length_reg[10] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[10]),
        .Q(\timeout_length_reg_n_0_[10] ),
        .R(p_0_in));
  FDRE \timeout_length_reg[11] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[11]),
        .Q(\timeout_length_reg_n_0_[11] ),
        .R(p_0_in));
  FDRE \timeout_length_reg[1] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[1]),
        .Q(\axi_rdata_reg[8]_0 [0]),
        .R(p_0_in));
  FDRE \timeout_length_reg[2] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[2]),
        .Q(\timeout_length_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE \timeout_length_reg[3] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[3]),
        .Q(\axi_rdata_reg[8]_0 [1]),
        .R(p_0_in));
  FDRE \timeout_length_reg[4] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[4]),
        .Q(\axi_rdata_reg[8]_0 [2]),
        .R(p_0_in));
  FDRE \timeout_length_reg[5] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[5]),
        .Q(\axi_rdata_reg[8]_0 [3]),
        .R(p_0_in));
  FDRE \timeout_length_reg[6] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[6]),
        .Q(\axi_rdata_reg[8]_0 [4]),
        .R(p_0_in));
  FDRE \timeout_length_reg[7] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[7]),
        .Q(\axi_rdata_reg[8]_0 [5]),
        .R(p_0_in));
  FDRE \timeout_length_reg[8] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[8]),
        .Q(\axi_rdata_reg[8]_0 [6]),
        .R(p_0_in));
  FDRE \timeout_length_reg[9] 
       (.C(s_axi_aclk),
        .CE(gt_axi_map_wready_reg_1),
        .D(s_axi_wdata[9]),
        .Q(\timeout_length_reg_n_0_[9] ),
        .R(p_0_in));
  FDSE wait_for_drp_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(gt_axi_map_wready_reg),
        .Q(wait_for_drp),
        .S(p_0_in));
  FDRE wr_req_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(gt_slv_wren),
        .Q(wr_req_reg),
        .R(p_0_in));
endmodule

(* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "Jesd204_microblaze_jesd204_phy_0_0_gt,gtwizard_v3_6_5,{protocol_file=JESD204}" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt
   (SYSCLK_IN,
    SOFT_RESET_TX_IN,
    SOFT_RESET_RX_IN,
    DONT_RESET_ON_DATA_ERROR_IN,
    GT0_DATA_VALID_IN,
    GT_TX_FSM_RESET_DONE_OUT,
    GT_RX_FSM_RESET_DONE_OUT,
    gt0_cpllfbclklost_out,
    gt0_cplllock_out,
    gt0_cplllockdetclk_in,
    gt0_cpllpd_in,
    gt0_cpllreset_in,
    gt0_gtnorthrefclk0_in,
    gt0_gtnorthrefclk1_in,
    gt0_gtrefclk0_in,
    gt0_gtrefclk1_in,
    gt0_gtsouthrefclk0_in,
    gt0_gtsouthrefclk1_in,
    gt0_drpaddr_in,
    gt0_drpclk_in,
    gt0_drpdi_in,
    gt0_drpdo_out,
    gt0_drpen_in,
    gt0_drprdy_out,
    gt0_drpwe_in,
    gt0_rxsysclksel_in,
    gt0_txsysclksel_in,
    gt0_loopback_in,
    gt0_rxpd_in,
    gt0_txpd_in,
    gt0_eyescanreset_in,
    gt0_rxuserrdy_in,
    gt0_eyescandataerror_out,
    gt0_eyescantrigger_in,
    gt0_rxcdrhold_in,
    gt0_dmonitorout_out,
    gt0_rxusrclk_in,
    gt0_rxusrclk2_in,
    gt0_rxdata_out,
    gt0_rxprbserr_out,
    gt0_rxprbssel_in,
    gt0_rxprbscntreset_in,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    gt0_gthrxn_in,
    gt0_rxbufreset_in,
    gt0_rxbufstatus_out,
    gt0_rxstatus_out,
    gt0_rxbyteisaligned_out,
    gt0_rxbyterealign_out,
    gt0_rxcommadet_out,
    gt0_rxmcommaalignen_in,
    gt0_rxpcommaalignen_in,
    gt0_rxdfelpmreset_in,
    gt0_rxmonitorout_out,
    gt0_rxmonitorsel_in,
    gt0_rxoutclk_out,
    gt0_rxoutclkfabric_out,
    gt0_gtrxreset_in,
    gt0_rxpcsreset_in,
    gt0_rxpmareset_in,
    gt0_rxlpmen_in,
    gt0_rxpolarity_in,
    gt0_rxchariscomma_out,
    gt0_rxcharisk_out,
    gt0_gthrxp_in,
    gt0_rxresetdone_out,
    gt0_txpostcursor_in,
    gt0_txprecursor_in,
    gt0_gttxreset_in,
    gt0_txuserrdy_in,
    gt0_txusrclk_in,
    gt0_txusrclk2_in,
    gt0_txprbsforceerr_in,
    gt0_txbufstatus_out,
    gt0_txdiffctrl_in,
    gt0_txinhibit_in,
    gt0_txdata_in,
    gt0_gthtxn_out,
    gt0_gthtxp_out,
    gt0_txoutclk_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txpcsreset_in,
    gt0_txpmareset_in,
    gt0_txresetdone_out,
    gt0_txpolarity_in,
    gt0_txprbssel_in,
    gt0_txcharisk_in,
    GT0_QPLLOUTCLK_IN,
    GT0_QPLLOUTREFCLK_IN);
  input SYSCLK_IN;
  input SOFT_RESET_TX_IN;
  input SOFT_RESET_RX_IN;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input GT0_DATA_VALID_IN;
  output GT_TX_FSM_RESET_DONE_OUT;
  output GT_RX_FSM_RESET_DONE_OUT;
  output gt0_cpllfbclklost_out;
  output gt0_cplllock_out;
  input gt0_cplllockdetclk_in;
  input gt0_cpllpd_in;
  input gt0_cpllreset_in;
  input gt0_gtnorthrefclk0_in;
  input gt0_gtnorthrefclk1_in;
  input gt0_gtrefclk0_in;
  input gt0_gtrefclk1_in;
  input gt0_gtsouthrefclk0_in;
  input gt0_gtsouthrefclk1_in;
  input [8:0]gt0_drpaddr_in;
  input gt0_drpclk_in;
  input [15:0]gt0_drpdi_in;
  output [15:0]gt0_drpdo_out;
  input gt0_drpen_in;
  output gt0_drprdy_out;
  input gt0_drpwe_in;
  input [1:0]gt0_rxsysclksel_in;
  input [1:0]gt0_txsysclksel_in;
  input [2:0]gt0_loopback_in;
  input [1:0]gt0_rxpd_in;
  input [1:0]gt0_txpd_in;
  input gt0_eyescanreset_in;
  input gt0_rxuserrdy_in;
  output gt0_eyescandataerror_out;
  input gt0_eyescantrigger_in;
  input gt0_rxcdrhold_in;
  output [14:0]gt0_dmonitorout_out;
  input gt0_rxusrclk_in;
  input gt0_rxusrclk2_in;
  output [31:0]gt0_rxdata_out;
  output gt0_rxprbserr_out;
  input [2:0]gt0_rxprbssel_in;
  input gt0_rxprbscntreset_in;
  output [3:0]gt0_rxdisperr_out;
  output [3:0]gt0_rxnotintable_out;
  input gt0_gthrxn_in;
  input gt0_rxbufreset_in;
  output [2:0]gt0_rxbufstatus_out;
  output [2:0]gt0_rxstatus_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  input gt0_rxmcommaalignen_in;
  input gt0_rxpcommaalignen_in;
  input gt0_rxdfelpmreset_in;
  output [6:0]gt0_rxmonitorout_out;
  input [1:0]gt0_rxmonitorsel_in;
  output gt0_rxoutclk_out;
  output gt0_rxoutclkfabric_out;
  input gt0_gtrxreset_in;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  input gt0_rxlpmen_in;
  input gt0_rxpolarity_in;
  output [3:0]gt0_rxchariscomma_out;
  output [3:0]gt0_rxcharisk_out;
  input gt0_gthrxp_in;
  output gt0_rxresetdone_out;
  input [4:0]gt0_txpostcursor_in;
  input [4:0]gt0_txprecursor_in;
  input gt0_gttxreset_in;
  input gt0_txuserrdy_in;
  input gt0_txusrclk_in;
  input gt0_txusrclk2_in;
  input gt0_txprbsforceerr_in;
  output [1:0]gt0_txbufstatus_out;
  input [3:0]gt0_txdiffctrl_in;
  input gt0_txinhibit_in;
  input [31:0]gt0_txdata_in;
  output gt0_gthtxn_out;
  output gt0_gthtxp_out;
  output gt0_txoutclk_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  output gt0_txresetdone_out;
  input gt0_txpolarity_in;
  input [2:0]gt0_txprbssel_in;
  input [3:0]gt0_txcharisk_in;
  input GT0_QPLLOUTCLK_IN;
  input GT0_QPLLOUTREFCLK_IN;

  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire GT0_DATA_VALID_IN;
  wire GT0_QPLLOUTCLK_IN;
  wire GT0_QPLLOUTREFCLK_IN;
  wire GT_RX_FSM_RESET_DONE_OUT;
  wire GT_TX_FSM_RESET_DONE_OUT;
  wire SOFT_RESET_RX_IN;
  wire SOFT_RESET_TX_IN;
  wire SYSCLK_IN;
  wire gt0_cpllfbclklost_out;
  wire gt0_cplllock_out;
  wire gt0_cplllockdetclk_in;
  wire gt0_cpllpd_in;
  wire gt0_cpllreset_in;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire gt0_drpclk_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gthrxn_in;
  wire gt0_gthrxp_in;
  wire gt0_gthtxn_out;
  wire gt0_gthtxp_out;
  wire gt0_gtnorthrefclk0_in;
  wire gt0_gtnorthrefclk1_in;
  wire gt0_gtrefclk0_in;
  wire gt0_gtrefclk1_in;
  wire gt0_gtrxreset_in;
  wire gt0_gtsouthrefclk0_in;
  wire gt0_gtsouthrefclk1_in;
  wire gt0_gttxreset_in;
  wire [2:0]gt0_loopback_in;
  wire gt0_rxbufreset_in;
  wire [2:0]gt0_rxbufstatus_out;
  wire gt0_rxbyteisaligned_out;
  wire gt0_rxbyterealign_out;
  wire gt0_rxcdrhold_in;
  wire [3:0]gt0_rxchariscomma_out;
  wire [3:0]gt0_rxcharisk_out;
  wire gt0_rxcommadet_out;
  wire [31:0]gt0_rxdata_out;
  wire gt0_rxdfelpmreset_in;
  wire [3:0]gt0_rxdisperr_out;
  wire gt0_rxlpmen_in;
  wire gt0_rxmcommaalignen_in;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire [3:0]gt0_rxnotintable_out;
  wire gt0_rxoutclk_out;
  wire gt0_rxoutclkfabric_out;
  wire gt0_rxpcommaalignen_in;
  wire gt0_rxpcsreset_in;
  wire [1:0]gt0_rxpd_in;
  wire gt0_rxpmareset_in;
  wire gt0_rxpolarity_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_rxsysclksel_in;
  wire gt0_rxuserrdy_in;
  wire gt0_rxusrclk2_in;
  wire gt0_rxusrclk_in;
  wire [1:0]gt0_txbufstatus_out;
  wire [3:0]gt0_txcharisk_in;
  wire [31:0]gt0_txdata_in;
  wire [3:0]gt0_txdiffctrl_in;
  wire gt0_txinhibit_in;
  wire gt0_txoutclk_out;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txpcsreset_in;
  wire [1:0]gt0_txpd_in;
  wire gt0_txpmareset_in;
  wire gt0_txpolarity_in;
  wire [4:0]gt0_txpostcursor_in;
  wire gt0_txprbsforceerr_in;
  wire [2:0]gt0_txprbssel_in;
  wire [4:0]gt0_txprecursor_in;
  wire gt0_txresetdone_out;
  wire [1:0]gt0_txsysclksel_in;
  wire gt0_txuserrdy_in;
  wire gt0_txusrclk2_in;
  wire gt0_txusrclk_in;

  (* EXAMPLE_SIMULATION = "0" *) 
  (* EXAMPLE_SIM_GTRESET_SPEEDUP = "TRUE" *) 
  (* EXAMPLE_USE_CHIPSCOPE = "1" *) 
  (* STABLE_CLOCK_PERIOD = "10" *) 
  (* USE_BUFG = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_init U0
       (.DONT_RESET_ON_DATA_ERROR_IN(DONT_RESET_ON_DATA_ERROR_IN),
        .GT0_DATA_VALID_IN(GT0_DATA_VALID_IN),
        .GT0_QPLLOUTCLK_IN(GT0_QPLLOUTCLK_IN),
        .GT0_QPLLOUTREFCLK_IN(GT0_QPLLOUTREFCLK_IN),
        .GT_RX_FSM_RESET_DONE_OUT(GT_RX_FSM_RESET_DONE_OUT),
        .GT_TX_FSM_RESET_DONE_OUT(GT_TX_FSM_RESET_DONE_OUT),
        .SOFT_RESET_RX_IN(SOFT_RESET_RX_IN),
        .SOFT_RESET_TX_IN(SOFT_RESET_TX_IN),
        .SYSCLK_IN(SYSCLK_IN),
        .gt0_cpllfbclklost_out(gt0_cpllfbclklost_out),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_cplllockdetclk_in(gt0_cplllockdetclk_in),
        .gt0_cpllpd_in(gt0_cpllpd_in),
        .gt0_cpllreset_in(gt0_cpllreset_in),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr_in(gt0_drpaddr_in),
        .gt0_drpclk_in(gt0_drpclk_in),
        .gt0_drpdi_in(gt0_drpdi_in),
        .gt0_drpdo_out(gt0_drpdo_out),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drprdy_out(gt0_drprdy_out),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_gthrxn_in(gt0_gthrxn_in),
        .gt0_gthrxp_in(gt0_gthrxp_in),
        .gt0_gthtxn_out(gt0_gthtxn_out),
        .gt0_gthtxp_out(gt0_gthtxp_out),
        .gt0_gtnorthrefclk0_in(gt0_gtnorthrefclk0_in),
        .gt0_gtnorthrefclk1_in(gt0_gtnorthrefclk1_in),
        .gt0_gtrefclk0_in(gt0_gtrefclk0_in),
        .gt0_gtrefclk1_in(gt0_gtrefclk1_in),
        .gt0_gtrxreset_in(gt0_gtrxreset_in),
        .gt0_gtsouthrefclk0_in(gt0_gtsouthrefclk0_in),
        .gt0_gtsouthrefclk1_in(gt0_gtsouthrefclk1_in),
        .gt0_gttxreset_in(gt0_gttxreset_in),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxbufreset_in(gt0_rxbufreset_in),
        .gt0_rxbufstatus_out(gt0_rxbufstatus_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxbyterealign_out(gt0_rxbyterealign_out),
        .gt0_rxcdrhold_in(gt0_rxcdrhold_in),
        .gt0_rxchariscomma_out(gt0_rxchariscomma_out),
        .gt0_rxcharisk_out(gt0_rxcharisk_out),
        .gt0_rxcommadet_out(gt0_rxcommadet_out),
        .gt0_rxdata_out(gt0_rxdata_out),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxdisperr_out(gt0_rxdisperr_out),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxmcommaalignen_in(gt0_rxmcommaalignen_in),
        .gt0_rxmonitorout_out(gt0_rxmonitorout_out),
        .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),
        .gt0_rxnotintable_out(gt0_rxnotintable_out),
        .gt0_rxoutclk_out(gt0_rxoutclk_out),
        .gt0_rxoutclkfabric_out(gt0_rxoutclkfabric_out),
        .gt0_rxpcommaalignen_in(gt0_rxpcommaalignen_in),
        .gt0_rxpcsreset_in(gt0_rxpcsreset_in),
        .gt0_rxpd_in(gt0_rxpd_in),
        .gt0_rxpmareset_in(gt0_rxpmareset_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),
        .gt0_rxprbserr_out(gt0_rxprbserr_out),
        .gt0_rxprbssel_in(gt0_rxprbssel_in),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_in),
        .gt0_rxuserrdy_in(gt0_rxuserrdy_in),
        .gt0_rxusrclk2_in(gt0_rxusrclk2_in),
        .gt0_rxusrclk_in(gt0_rxusrclk_in),
        .gt0_txbufstatus_out(gt0_txbufstatus_out),
        .gt0_txcharisk_in(gt0_txcharisk_in),
        .gt0_txdata_in(gt0_txdata_in),
        .gt0_txdiffctrl_in(gt0_txdiffctrl_in),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txoutclk_out(gt0_txoutclk_out),
        .gt0_txoutclkfabric_out(gt0_txoutclkfabric_out),
        .gt0_txoutclkpcs_out(gt0_txoutclkpcs_out),
        .gt0_txpcsreset_in(gt0_txpcsreset_in),
        .gt0_txpd_in(gt0_txpd_in),
        .gt0_txpmareset_in(gt0_txpmareset_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .gt0_txpostcursor_in(gt0_txpostcursor_in),
        .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),
        .gt0_txprbssel_in(gt0_txprbssel_in),
        .gt0_txprecursor_in(gt0_txprecursor_in),
        .gt0_txresetdone_out(gt0_txresetdone_out),
        .gt0_txsysclksel_in(gt0_txsysclksel_in),
        .gt0_txuserrdy_in(gt0_txuserrdy_in),
        .gt0_txusrclk2_in(gt0_txusrclk2_in),
        .gt0_txusrclk_in(gt0_txusrclk_in));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_GT
   (gt0_cpllfbclklost_out,
    gt0_cplllock_out,
    gt0_drprdy_out,
    gt0_eyescandataerror_out,
    gt0_gthtxn_out,
    gt0_gthtxp_out,
    gt0_rxbyteisaligned_out,
    gt0_rxbyterealign_out,
    gt0_rxcommadet_out,
    gt0_rxoutclk_out,
    gt0_rxoutclkfabric_out,
    gt0_rxprbserr_out,
    gt0_rxresetdone_out,
    gt0_txoutclk_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txresetdone_out,
    gt0_dmonitorout_out,
    gt0_drpdo_out,
    gt0_txbufstatus_out,
    gt0_rxbufstatus_out,
    gt0_rxstatus_out,
    gt0_rxdata_out,
    gt0_rxmonitorout_out,
    gt0_rxchariscomma_out,
    gt0_rxcharisk_out,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    data_in,
    data_sync_reg1,
    gt0_cplllockdetclk_in,
    cpllpd_in,
    cpllreset_in,
    gt0_drpclk_in,
    gt0_drpen_in,
    gt0_drpwe_in,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    gt0_gthrxn_in,
    gt0_gthrxp_in,
    gt0_gtnorthrefclk0_in,
    gt0_gtnorthrefclk1_in,
    gt0_gtrefclk0_in,
    gt0_gtrefclk1_in,
    SR,
    gt0_gtsouthrefclk0_in,
    gt0_gtsouthrefclk1_in,
    gt0_gttxreset_in1_out,
    GT0_QPLLOUTCLK_IN,
    GT0_QPLLOUTREFCLK_IN,
    gt0_rxbufreset_in,
    gt0_rxcdrhold_in,
    gt0_rxdfelpmreset_in,
    gt0_rxlpmen_in,
    gt0_rxmcommaalignen_in,
    gt0_rxpcommaalignen_in,
    gt0_rxpcsreset_in,
    gt0_rxpmareset_in,
    gt0_rxpolarity_in,
    gt0_rxprbscntreset_in,
    gt0_rxuserrdy_in3_out,
    gt0_rxusrclk_in,
    gt0_rxusrclk2_in,
    gt0_txinhibit_in,
    gt0_txpcsreset_in,
    gt0_txpmareset_in,
    gt0_txpolarity_in,
    gt0_txprbsforceerr_in,
    gt0_txuserrdy_in0_out,
    gt0_txusrclk_in,
    gt0_txusrclk2_in,
    gt0_drpdi_in,
    gt0_rxmonitorsel_in,
    gt0_rxpd_in,
    gt0_rxsysclksel_in,
    gt0_txpd_in,
    gt0_txsysclksel_in,
    gt0_loopback_in,
    gt0_rxprbssel_in,
    gt0_txprbssel_in,
    gt0_txdiffctrl_in,
    gt0_txpostcursor_in,
    gt0_txprecursor_in,
    gt0_txdata_in,
    gt0_txcharisk_in,
    gt0_drpaddr_in);
  output gt0_cpllfbclklost_out;
  output gt0_cplllock_out;
  output gt0_drprdy_out;
  output gt0_eyescandataerror_out;
  output gt0_gthtxn_out;
  output gt0_gthtxp_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  output gt0_rxoutclk_out;
  output gt0_rxoutclkfabric_out;
  output gt0_rxprbserr_out;
  output gt0_rxresetdone_out;
  output gt0_txoutclk_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  output gt0_txresetdone_out;
  output [14:0]gt0_dmonitorout_out;
  output [15:0]gt0_drpdo_out;
  output [1:0]gt0_txbufstatus_out;
  output [2:0]gt0_rxbufstatus_out;
  output [2:0]gt0_rxstatus_out;
  output [31:0]gt0_rxdata_out;
  output [6:0]gt0_rxmonitorout_out;
  output [3:0]gt0_rxchariscomma_out;
  output [3:0]gt0_rxcharisk_out;
  output [3:0]gt0_rxdisperr_out;
  output [3:0]gt0_rxnotintable_out;
  output data_in;
  output data_sync_reg1;
  input gt0_cplllockdetclk_in;
  input cpllpd_in;
  input cpllreset_in;
  input gt0_drpclk_in;
  input gt0_drpen_in;
  input gt0_drpwe_in;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input gt0_gthrxn_in;
  input gt0_gthrxp_in;
  input gt0_gtnorthrefclk0_in;
  input gt0_gtnorthrefclk1_in;
  input gt0_gtrefclk0_in;
  input gt0_gtrefclk1_in;
  input [0:0]SR;
  input gt0_gtsouthrefclk0_in;
  input gt0_gtsouthrefclk1_in;
  input gt0_gttxreset_in1_out;
  input GT0_QPLLOUTCLK_IN;
  input GT0_QPLLOUTREFCLK_IN;
  input gt0_rxbufreset_in;
  input gt0_rxcdrhold_in;
  input gt0_rxdfelpmreset_in;
  input gt0_rxlpmen_in;
  input gt0_rxmcommaalignen_in;
  input gt0_rxpcommaalignen_in;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  input gt0_rxpolarity_in;
  input gt0_rxprbscntreset_in;
  input gt0_rxuserrdy_in3_out;
  input gt0_rxusrclk_in;
  input gt0_rxusrclk2_in;
  input gt0_txinhibit_in;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  input gt0_txpolarity_in;
  input gt0_txprbsforceerr_in;
  input gt0_txuserrdy_in0_out;
  input gt0_txusrclk_in;
  input gt0_txusrclk2_in;
  input [15:0]gt0_drpdi_in;
  input [1:0]gt0_rxmonitorsel_in;
  input [1:0]gt0_rxpd_in;
  input [1:0]gt0_rxsysclksel_in;
  input [1:0]gt0_txpd_in;
  input [1:0]gt0_txsysclksel_in;
  input [2:0]gt0_loopback_in;
  input [2:0]gt0_rxprbssel_in;
  input [2:0]gt0_txprbssel_in;
  input [3:0]gt0_txdiffctrl_in;
  input [4:0]gt0_txpostcursor_in;
  input [4:0]gt0_txprecursor_in;
  input [31:0]gt0_txdata_in;
  input [3:0]gt0_txcharisk_in;
  input [8:0]gt0_drpaddr_in;

  wire GT0_QPLLOUTCLK_IN;
  wire GT0_QPLLOUTREFCLK_IN;
  wire [0:0]SR;
  wire cpllpd_in;
  wire cpllreset_in;
  wire data_in;
  wire data_sync_reg1;
  wire gt0_cpllfbclklost_out;
  wire gt0_cplllock_out;
  wire gt0_cplllockdetclk_in;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire gt0_drpclk_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gthrxn_in;
  wire gt0_gthrxp_in;
  wire gt0_gthtxn_out;
  wire gt0_gthtxp_out;
  wire gt0_gtnorthrefclk0_in;
  wire gt0_gtnorthrefclk1_in;
  wire gt0_gtrefclk0_in;
  wire gt0_gtrefclk1_in;
  wire gt0_gtsouthrefclk0_in;
  wire gt0_gtsouthrefclk1_in;
  wire gt0_gttxreset_in1_out;
  wire [2:0]gt0_loopback_in;
  wire gt0_rxbufreset_in;
  wire [2:0]gt0_rxbufstatus_out;
  wire gt0_rxbyteisaligned_out;
  wire gt0_rxbyterealign_out;
  wire gt0_rxcdrhold_in;
  wire [3:0]gt0_rxchariscomma_out;
  wire [3:0]gt0_rxcharisk_out;
  wire gt0_rxcommadet_out;
  wire [31:0]gt0_rxdata_out;
  wire gt0_rxdfelpmreset_in;
  wire [3:0]gt0_rxdisperr_out;
  wire gt0_rxlpmen_in;
  wire gt0_rxmcommaalignen_in;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire [3:0]gt0_rxnotintable_out;
  wire gt0_rxoutclk_out;
  wire gt0_rxoutclkfabric_out;
  wire gt0_rxpcommaalignen_in;
  wire gt0_rxpcsreset_in;
  wire [1:0]gt0_rxpd_in;
  wire gt0_rxpmareset_in;
  wire gt0_rxpmaresetdone_i;
  wire gt0_rxpolarity_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_rxsysclksel_in;
  wire gt0_rxuserrdy_in3_out;
  wire gt0_rxusrclk2_in;
  wire gt0_rxusrclk_in;
  wire [1:0]gt0_txbufstatus_out;
  wire [3:0]gt0_txcharisk_in;
  wire [31:0]gt0_txdata_in;
  wire [3:0]gt0_txdiffctrl_in;
  wire gt0_txinhibit_in;
  wire gt0_txoutclk_out;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txpcsreset_in;
  wire [1:0]gt0_txpd_in;
  wire gt0_txpmareset_in;
  wire gt0_txpolarity_in;
  wire [4:0]gt0_txpostcursor_in;
  wire gt0_txprbsforceerr_in;
  wire [2:0]gt0_txprbssel_in;
  wire [4:0]gt0_txprecursor_in;
  wire gt0_txresetdone_out;
  wire [1:0]gt0_txsysclksel_in;
  wire gt0_txuserrdy_in0_out;
  wire gt0_txusrclk2_in;
  wire gt0_txusrclk_in;
  wire gthe2_i_n_2;
  wire gthe2_i_n_50;
  wire NLW_gthe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gthe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gthe2_i_RSOSINTDONE_UNCONNECTED;
  wire NLW_gthe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gthe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gthe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gthe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gthe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gthe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gthe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gthe2_i_RXDFESLIDETAPSTARTED_UNCONNECTED;
  wire NLW_gthe2_i_RXDFESLIDETAPSTROBEDONE_UNCONNECTED;
  wire NLW_gthe2_i_RXDFESLIDETAPSTROBESTARTED_UNCONNECTED;
  wire NLW_gthe2_i_RXDFESTADAPTDONE_UNCONNECTED;
  wire NLW_gthe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gthe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gthe2_i_RXOSINTSTARTED_UNCONNECTED;
  wire NLW_gthe2_i_RXOSINTSTROBEDONE_UNCONNECTED;
  wire NLW_gthe2_i_RXOSINTSTROBESTARTED_UNCONNECTED;
  wire NLW_gthe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gthe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gthe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gthe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gthe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gthe2_i_RXSYNCDONE_UNCONNECTED;
  wire NLW_gthe2_i_RXSYNCOUT_UNCONNECTED;
  wire NLW_gthe2_i_RXVALID_UNCONNECTED;
  wire NLW_gthe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gthe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gthe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gthe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gthe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gthe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gthe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gthe2_i_TXRATEDONE_UNCONNECTED;
  wire NLW_gthe2_i_TXSYNCDONE_UNCONNECTED;
  wire NLW_gthe2_i_TXSYNCOUT_UNCONNECTED;
  wire [15:0]NLW_gthe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:4]NLW_gthe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:4]NLW_gthe2_i_RXCHARISK_UNCONNECTED;
  wire [4:0]NLW_gthe2_i_RXCHBONDO_UNCONNECTED;
  wire [1:0]NLW_gthe2_i_RXCLKCORCNT_UNCONNECTED;
  wire [63:32]NLW_gthe2_i_RXDATA_UNCONNECTED;
  wire [1:0]NLW_gthe2_i_RXDATAVALID_UNCONNECTED;
  wire [7:4]NLW_gthe2_i_RXDISPERR_UNCONNECTED;
  wire [5:0]NLW_gthe2_i_RXHEADER_UNCONNECTED;
  wire [1:0]NLW_gthe2_i_RXHEADERVALID_UNCONNECTED;
  wire [7:4]NLW_gthe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gthe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gthe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [1:0]NLW_gthe2_i_RXSTARTOFSEQ_UNCONNECTED;

  LUT3 #(
    .INIT(8'hF8)) 
    data_sync1_i_1
       (.I0(gt0_txpd_in[1]),
        .I1(gt0_txpd_in[0]),
        .I2(gt0_txresetdone_out),
        .O(data_in));
  LUT3 #(
    .INIT(8'hF8)) 
    data_sync1_i_1__0
       (.I0(gt0_rxpd_in[1]),
        .I1(gt0_rxpd_in[0]),
        .I2(gt0_rxresetdone_out),
        .O(data_sync_reg1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  GTHE2_CHANNEL #(
    .ACJTAG_DEBUG_MODE(1'b0),
    .ACJTAG_MODE(1'b0),
    .ACJTAG_RESET(1'b0),
    .ADAPT_CFG0(20'h00C10),
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b1111111111),
    .ALIGN_COMMA_WORD(1),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .A_RXOSCALRESET(1'b0),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CFOK_CFG(42'h24800040E80),
    .CFOK_CFG2(6'b100000),
    .CFOK_CFG3(6'b100000),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("FALSE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(12),
    .CLK_COR_MIN_LAT(8),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0100000000),
    .CLK_COR_SEQ_1_2(10'b0000000000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0100000000),
    .CLK_COR_SEQ_2_2(10'b0000000000),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("FALSE"),
    .CLK_COR_SEQ_LEN(1),
    .CPLL_CFG(29'h00BC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CLK_PHASE_SEL(1'b0),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CLKRSVD0_INVERTED(1'b0),
    .IS_CLKRSVD1_INVERTED(1'b0),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DMONITORCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_SIGVALIDCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .LOOPBACK_CFG(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h3C),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'b00000000000000000000000010000000),
    .PMA_RSV2(32'b00011100000000000000000000001010),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(15'b000000000001000),
    .PMA_RSV5(4'b0000),
    .RESET_POWERSAVE_DISABLE(1'b0),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FAST"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(57),
    .RXBUF_THRESH_OVRD("TRUE"),
    .RXBUF_THRESH_UNDFLW(3),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(83'h0002007FE2000C2080018),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00001000000000),
    .RXLPM_LF_CFG(18'b001001000000000000),
    .RXOOB_CFG(7'b0000110),
    .RXOOB_CLK_CFG("PMA"),
    .RXOSCALRESET_TIME(5'b00011),
    .RXOSCALRESET_TIMEOUT(5'b00000),
    .RXOUT_DIV(1),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'hC00002),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPI_CFG0(2'b00),
    .RXPI_CFG1(2'b00),
    .RXPI_CFG2(2'b00),
    .RXPI_CFG3(2'b11),
    .RXPI_CFG4(1'b1),
    .RXPI_CFG5(1'b1),
    .RXPI_CFG6(3'b001),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RXSYNC_MULTILANE(1'b0),
    .RXSYNC_OVRD(1'b0),
    .RXSYNC_SKIP_DA(1'b0),
    .RX_BIAS_CFG(24'b000011000000000000010000),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(7),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(4'b1010),
    .RX_DATA_WIDTH(40),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(14'b00000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFELPM_CFG0(4'b0110),
    .RX_DFELPM_CFG1(1'b0),
    .RX_DFELPM_KLKH_AGC_STUP_EN(1'b1),
    .RX_DFE_AGC_CFG0(2'b00),
    .RX_DFE_AGC_CFG1(3'b010),
    .RX_DFE_AGC_CFG2(4'b0000),
    .RX_DFE_AGC_OVRDEN(1'b1),
    .RX_DFE_GAIN_CFG(23'h0020C0),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011100000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_H6_CFG(11'b00000100000),
    .RX_DFE_H7_CFG(11'b00000100000),
    .RX_DFE_KL_CFG(33'b001000001000000000000001100010000),
    .RX_DFE_KL_LPM_KH_CFG0(2'b01),
    .RX_DFE_KL_LPM_KH_CFG1(3'b010),
    .RX_DFE_KL_LPM_KH_CFG2(4'b0010),
    .RX_DFE_KL_LPM_KH_OVRDEN(1'b1),
    .RX_DFE_KL_LPM_KL_CFG0(2'b01),
    .RX_DFE_KL_LPM_KL_CFG1(3'b010),
    .RX_DFE_KL_LPM_KL_CFG2(4'b0010),
    .RX_DFE_KL_LPM_KL_OVRDEN(1'b1),
    .RX_DFE_LPM_CFG(16'h0080),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_ST_CFG(54'h00E100000C003F),
    .RX_DFE_UT_CFG(17'b00011100000000000),
    .RX_DFE_VP_CFG(17'b00011101010100011),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(1),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b111),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b111),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("2.0"),
    .TERM_RCAL_CFG(15'b100001000010000),
    .TERM_RCAL_OVRD(3'b000),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOOB_CFG(1'b0),
    .TXOUT_DIV(1),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPI_CFG0(2'b00),
    .TXPI_CFG1(2'b00),
    .TXPI_CFG2(2'b00),
    .TXPI_CFG3(1'b0),
    .TXPI_CFG4(1'b0),
    .TXPI_CFG5(3'b100),
    .TXPI_GREY_SEL(1'b0),
    .TXPI_INVSTROBE_SEL(1'b0),
    .TXPI_PPMCLK_SEL("TXUSRCLK2"),
    .TXPI_PPM_CFG(8'b00000000),
    .TXPI_SYNFREQ_PPM(3'b001),
    .TXPMARESET_TIME(5'b00001),
    .TXSYNC_MULTILANE(1'b0),
    .TXSYNC_OVRD(1'b0),
    .TXSYNC_SKIP_DA(1'b0),
    .TX_CLK25_DIV(7),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(40),
    .TX_DEEMPH0(6'b000000),
    .TX_DEEMPH1(6'b000000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(1),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_PRECHARGE_TIME(17'h155CC),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0),
    .USE_PCS_CLK_PHASE_SEL(1'b0)) 
    gthe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD0(1'b0),
        .CLKRSVD1(1'b0),
        .CPLLFBCLKLOST(gt0_cpllfbclklost_out),
        .CPLLLOCK(gt0_cplllock_out),
        .CPLLLOCKDETCLK(gt0_cplllockdetclk_in),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(cpllpd_in),
        .CPLLREFCLKLOST(gthe2_i_n_2),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(cpllreset_in),
        .DMONFIFORESET(1'b0),
        .DMONITORCLK(1'b0),
        .DMONITOROUT(gt0_dmonitorout_out),
        .DRPADDR(gt0_drpaddr_in),
        .DRPCLK(gt0_drpclk_in),
        .DRPDI(gt0_drpdi_in),
        .DRPDO(gt0_drpdo_out),
        .DRPEN(gt0_drpen_in),
        .DRPRDY(gt0_drprdy_out),
        .DRPWE(gt0_drpwe_in),
        .EYESCANDATAERROR(gt0_eyescandataerror_out),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(gt0_eyescanreset_in),
        .EYESCANTRIGGER(gt0_eyescantrigger_in),
        .GTGREFCLK(1'b0),
        .GTHRXN(gt0_gthrxn_in),
        .GTHRXP(gt0_gthrxp_in),
        .GTHTXN(gt0_gthtxn_out),
        .GTHTXP(gt0_gthtxp_out),
        .GTNORTHREFCLK0(gt0_gtnorthrefclk0_in),
        .GTNORTHREFCLK1(gt0_gtnorthrefclk1_in),
        .GTREFCLK0(gt0_gtrefclk0_in),
        .GTREFCLK1(gt0_gtrefclk1_in),
        .GTREFCLKMONITOR(NLW_gthe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(SR),
        .GTSOUTHREFCLK0(gt0_gtsouthrefclk0_in),
        .GTSOUTHREFCLK1(gt0_gtsouthrefclk1_in),
        .GTTXRESET(gt0_gttxreset_in1_out),
        .LOOPBACK(gt0_loopback_in),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gthe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gthe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(GT0_QPLLOUTCLK_IN),
        .QPLLREFCLK(GT0_QPLLOUTREFCLK_IN),
        .RESETOVRD(1'b0),
        .RSOSINTDONE(NLW_gthe2_i_RSOSINTDONE_UNCONNECTED),
        .RX8B10BEN(1'b1),
        .RXADAPTSELTEST({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXBUFRESET(gt0_rxbufreset_in),
        .RXBUFSTATUS(gt0_rxbufstatus_out),
        .RXBYTEISALIGNED(gt0_rxbyteisaligned_out),
        .RXBYTEREALIGN(gt0_rxbyterealign_out),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(gt0_rxcdrhold_in),
        .RXCDRLOCK(NLW_gthe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gthe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gthe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gthe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA({NLW_gthe2_i_RXCHARISCOMMA_UNCONNECTED[7:4],gt0_rxchariscomma_out}),
        .RXCHARISK({NLW_gthe2_i_RXCHARISK_UNCONNECTED[7:4],gt0_rxcharisk_out}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gthe2_i_RXCHBONDO_UNCONNECTED[4:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(NLW_gthe2_i_RXCLKCORCNT_UNCONNECTED[1:0]),
        .RXCOMINITDET(NLW_gthe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gt0_rxcommadet_out),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gthe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gthe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gthe2_i_RXDATA_UNCONNECTED[63:32],gt0_rxdata_out}),
        .RXDATAVALID(NLW_gthe2_i_RXDATAVALID_UNCONNECTED[1:0]),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b1),
        .RXDFEAGCTRL({1'b1,1'b0,1'b0,1'b0,1'b0}),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(gt0_rxdfelpmreset_in),
        .RXDFESLIDETAP({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXDFESLIDETAPADAPTEN(1'b0),
        .RXDFESLIDETAPHOLD(1'b0),
        .RXDFESLIDETAPID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXDFESLIDETAPINITOVRDEN(1'b0),
        .RXDFESLIDETAPONLYADAPTEN(1'b0),
        .RXDFESLIDETAPOVRDEN(1'b0),
        .RXDFESLIDETAPSTARTED(NLW_gthe2_i_RXDFESLIDETAPSTARTED_UNCONNECTED),
        .RXDFESLIDETAPSTROBE(1'b0),
        .RXDFESLIDETAPSTROBEDONE(NLW_gthe2_i_RXDFESLIDETAPSTROBEDONE_UNCONNECTED),
        .RXDFESLIDETAPSTROBESTARTED(NLW_gthe2_i_RXDFESLIDETAPSTROBESTARTED_UNCONNECTED),
        .RXDFESTADAPTDONE(NLW_gthe2_i_RXDFESTADAPTDONE_UNCONNECTED),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFETAP6HOLD(1'b0),
        .RXDFETAP6OVRDEN(1'b0),
        .RXDFETAP7HOLD(1'b0),
        .RXDFETAP7OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDISPERR({NLW_gthe2_i_RXDISPERR_UNCONNECTED[7:4],gt0_rxdisperr_out}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gthe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gthe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gthe2_i_RXHEADER_UNCONNECTED[5:0]),
        .RXHEADERVALID(NLW_gthe2_i_RXHEADERVALID_UNCONNECTED[1:0]),
        .RXLPMEN(gt0_rxlpmen_in),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(gt0_rxmcommaalignen_in),
        .RXMONITOROUT(gt0_rxmonitorout_out),
        .RXMONITORSEL(gt0_rxmonitorsel_in),
        .RXNOTINTABLE({NLW_gthe2_i_RXNOTINTABLE_UNCONNECTED[7:4],gt0_rxnotintable_out}),
        .RXOOBRESET(1'b0),
        .RXOSCALRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSINTCFG({1'b0,1'b1,1'b1,1'b0}),
        .RXOSINTEN(1'b1),
        .RXOSINTHOLD(1'b0),
        .RXOSINTID0({1'b0,1'b0,1'b0,1'b0}),
        .RXOSINTNTRLEN(1'b0),
        .RXOSINTOVRDEN(1'b0),
        .RXOSINTSTARTED(NLW_gthe2_i_RXOSINTSTARTED_UNCONNECTED),
        .RXOSINTSTROBE(1'b0),
        .RXOSINTSTROBEDONE(NLW_gthe2_i_RXOSINTSTROBEDONE_UNCONNECTED),
        .RXOSINTSTROBESTARTED(NLW_gthe2_i_RXOSINTSTROBESTARTED_UNCONNECTED),
        .RXOSINTTESTOVRDEN(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(gt0_rxoutclk_out),
        .RXOUTCLKFABRIC(gt0_rxoutclkfabric_out),
        .RXOUTCLKPCS(NLW_gthe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(gt0_rxpcommaalignen_in),
        .RXPCSRESET(gt0_rxpcsreset_in),
        .RXPD(gt0_rxpd_in),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gthe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gthe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gthe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(gt0_rxpmareset_in),
        .RXPMARESETDONE(gt0_rxpmaresetdone_i),
        .RXPOLARITY(gt0_rxpolarity_in),
        .RXPRBSCNTRESET(gt0_rxprbscntreset_in),
        .RXPRBSERR(gt0_rxprbserr_out),
        .RXPRBSSEL(gt0_rxprbssel_in),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gthe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gthe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gthe2_i_RXRATEDONE_UNCONNECTED),
        .RXRATEMODE(1'b0),
        .RXRESETDONE(gt0_rxresetdone_out),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gthe2_i_RXSTARTOFSEQ_UNCONNECTED[1:0]),
        .RXSTATUS(gt0_rxstatus_out),
        .RXSYNCALLIN(1'b0),
        .RXSYNCDONE(NLW_gthe2_i_RXSYNCDONE_UNCONNECTED),
        .RXSYNCIN(1'b0),
        .RXSYNCMODE(1'b0),
        .RXSYNCOUT(NLW_gthe2_i_RXSYNCOUT_UNCONNECTED),
        .RXSYSCLKSEL(gt0_rxsysclksel_in),
        .RXUSERRDY(gt0_rxuserrdy_in3_out),
        .RXUSRCLK(gt0_rxusrclk_in),
        .RXUSRCLK2(gt0_rxusrclk2_in),
        .RXVALID(NLW_gthe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .SIGVALIDCLK(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS(gt0_txbufstatus_out),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,gt0_txcharisk_in}),
        .TXCOMFINISH(NLW_gthe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gt0_txdata_in}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL(gt0_txdiffctrl_in),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gthe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(1'b0),
        .TXGEARBOXREADY(NLW_gthe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(gt0_txinhibit_in),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(gt0_txoutclk_out),
        .TXOUTCLKFABRIC(gt0_txoutclkfabric_out),
        .TXOUTCLKPCS(gt0_txoutclkpcs_out),
        .TXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .TXPCSRESET(gt0_txpcsreset_in),
        .TXPD(gt0_txpd_in),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gthe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gthe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPIPPMEN(1'b0),
        .TXPIPPMOVRDEN(1'b0),
        .TXPIPPMPD(1'b0),
        .TXPIPPMSEL(1'b1),
        .TXPIPPMSTEPSIZE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPISOPD(1'b0),
        .TXPMARESET(gt0_txpmareset_in),
        .TXPMARESETDONE(gthe2_i_n_50),
        .TXPOLARITY(gt0_txpolarity_in),
        .TXPOSTCURSOR(gt0_txpostcursor_in),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(gt0_txprbsforceerr_in),
        .TXPRBSSEL(gt0_txprbssel_in),
        .TXPRECURSOR(gt0_txprecursor_in),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gthe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gthe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gthe2_i_TXRATEDONE_UNCONNECTED),
        .TXRATEMODE(1'b0),
        .TXRESETDONE(gt0_txresetdone_out),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYNCALLIN(1'b0),
        .TXSYNCDONE(NLW_gthe2_i_TXSYNCDONE_UNCONNECTED),
        .TXSYNCIN(1'b0),
        .TXSYNCMODE(1'b0),
        .TXSYNCOUT(NLW_gthe2_i_TXSYNCOUT_UNCONNECTED),
        .TXSYSCLKSEL(gt0_txsysclksel_in),
        .TXUSERRDY(gt0_txuserrdy_in0_out),
        .TXUSRCLK(gt0_txusrclk_in),
        .TXUSRCLK2(gt0_txusrclk2_in));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_RX_STARTUP_FSM
   (p_4_in,
    GT_RX_FSM_RESET_DONE_OUT,
    SR,
    gt0_rxuserrdy_in3_out,
    gt0_rx_cdrlocked_reg,
    SYSCLK_IN,
    gt0_rxusrclk_in,
    SOFT_RESET_RX_IN,
    DONT_RESET_ON_DATA_ERROR_IN,
    gt0_rxsysclksel_in,
    gt0_gtrxreset_in,
    gt0_txsysclksel_in,
    gt0_rxuserrdy_in,
    gt0_rx_cdrlocked_reg_0,
    \gt0_rx_cdrlock_counter_reg[0] ,
    \gt0_rx_cdrlock_counter_reg[3] ,
    Q,
    \gt0_rx_cdrlock_counter_reg[7] ,
    data_in,
    GT0_DATA_VALID_IN,
    gt0_cplllock_out);
  output p_4_in;
  output GT_RX_FSM_RESET_DONE_OUT;
  output [0:0]SR;
  output gt0_rxuserrdy_in3_out;
  output gt0_rx_cdrlocked_reg;
  input SYSCLK_IN;
  input gt0_rxusrclk_in;
  input SOFT_RESET_RX_IN;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input [0:0]gt0_rxsysclksel_in;
  input gt0_gtrxreset_in;
  input [0:0]gt0_txsysclksel_in;
  input gt0_rxuserrdy_in;
  input gt0_rx_cdrlocked_reg_0;
  input \gt0_rx_cdrlock_counter_reg[0] ;
  input \gt0_rx_cdrlock_counter_reg[3] ;
  input [0:0]Q;
  input \gt0_rx_cdrlock_counter_reg[7] ;
  input data_in;
  input GT0_DATA_VALID_IN;
  input gt0_cplllock_out;

  wire CPLL_RESET_i_1__0_n_0;
  wire CPLL_RESET_i_2__0_n_0;
  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire \FSM_sequential_rx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[2]_i_1_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_6_n_0 ;
  wire GT0_DATA_VALID_IN;
  wire GT_RX_FSM_RESET_DONE_OUT;
  wire [0:0]Q;
  wire RXUSERRDY_i_1_n_0;
  wire SOFT_RESET_RX_IN;
  wire [0:0]SR;
  wire SYSCLK_IN;
  wire check_tlock_max_i_1_n_0;
  wire check_tlock_max_reg_n_0;
  wire cplllock_sync;
  wire data_in;
  wire data_valid_sync;
  wire gt0_cplllock_out;
  wire gt0_gtrxreset_in;
  wire \gt0_rx_cdrlock_counter_reg[0] ;
  wire \gt0_rx_cdrlock_counter_reg[3] ;
  wire \gt0_rx_cdrlock_counter_reg[7] ;
  wire gt0_rx_cdrlocked_reg;
  wire gt0_rx_cdrlocked_reg_0;
  wire [0:0]gt0_rxsysclksel_in;
  wire gt0_rxuserrdy_in;
  wire gt0_rxuserrdy_in3_out;
  wire gt0_rxusrclk_in;
  wire [0:0]gt0_txsysclksel_in;
  wire gtrxreset_i_i_1_n_0;
  wire \init_wait_count[0]_i_1__0_n_0 ;
  wire \init_wait_count[5]_i_1__0_n_0 ;
  wire [5:0]init_wait_count_reg__0;
  wire init_wait_done_i_1__0_n_0;
  wire init_wait_done_i_2__0_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[7]_i_2__0_n_0 ;
  wire \mmcm_lock_count[7]_i_4__0_n_0 ;
  wire [7:0]mmcm_lock_count_reg__0;
  wire mmcm_lock_reclocked;
  wire [5:1]p_0_in;
  wire [7:0]p_0_in__0;
  wire p_2_in;
  wire p_3_in;
  wire p_4_in;
  wire pll_reset_asserted_i_1__0_n_0;
  wire pll_reset_asserted_i_2_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire reset_time_out_i_2__0_n_0;
  wire reset_time_out_i_4_n_0;
  wire reset_time_out_reg_n_0;
  wire run_phase_alignment_int_i_1__0_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s2;
  wire run_phase_alignment_int_s3_reg_n_0;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
  (* RTL_KEEP = "yes" *) wire [3:0]rx_state;
  wire rx_state01_out;
  wire rx_state128_out;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire sync_QPLLLOCK_n_1;
  wire sync_QPLLLOCK_n_2;
  wire sync_data_valid_n_1;
  wire sync_data_valid_n_2;
  wire sync_data_valid_n_3;
  wire sync_data_valid_n_4;
  wire sync_data_valid_n_5;
  wire sync_mmcm_lock_reclocked_n_0;
  wire sync_mmcm_lock_reclocked_n_1;
  wire time_out_100us_i_1_n_0;
  wire time_out_100us_i_2_n_0;
  wire time_out_100us_i_3_n_0;
  wire time_out_100us_i_4_n_0;
  wire time_out_100us_reg_n_0;
  wire time_out_1us_i_1_n_0;
  wire time_out_1us_i_2_n_0;
  wire time_out_1us_reg_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3_n_0 ;
  wire \time_out_counter[0]_i_4_n_0 ;
  wire \time_out_counter[0]_i_5__0_n_0 ;
  wire \time_out_counter[0]_i_6__0_n_0 ;
  wire \time_out_counter[0]_i_7__0_n_0 ;
  wire \time_out_counter[0]_i_8_n_0 ;
  wire \time_out_counter[12]_i_2__0_n_0 ;
  wire \time_out_counter[12]_i_3__0_n_0 ;
  wire \time_out_counter[12]_i_4__0_n_0 ;
  wire \time_out_counter[12]_i_5__0_n_0 ;
  wire \time_out_counter[16]_i_2__0_n_0 ;
  wire \time_out_counter[16]_i_3__0_n_0 ;
  wire \time_out_counter[4]_i_2__0_n_0 ;
  wire \time_out_counter[4]_i_3__0_n_0 ;
  wire \time_out_counter[4]_i_4__0_n_0 ;
  wire \time_out_counter[4]_i_5__0_n_0 ;
  wire \time_out_counter[8]_i_2__0_n_0 ;
  wire \time_out_counter[8]_i_3__0_n_0 ;
  wire \time_out_counter[8]_i_4__0_n_0 ;
  wire \time_out_counter[8]_i_5__0_n_0 ;
  wire [17:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2__0_n_0 ;
  wire \time_out_counter_reg[0]_i_2__0_n_1 ;
  wire \time_out_counter_reg[0]_i_2__0_n_2 ;
  wire \time_out_counter_reg[0]_i_2__0_n_3 ;
  wire \time_out_counter_reg[0]_i_2__0_n_4 ;
  wire \time_out_counter_reg[0]_i_2__0_n_5 ;
  wire \time_out_counter_reg[0]_i_2__0_n_6 ;
  wire \time_out_counter_reg[0]_i_2__0_n_7 ;
  wire \time_out_counter_reg[12]_i_1__0_n_0 ;
  wire \time_out_counter_reg[12]_i_1__0_n_1 ;
  wire \time_out_counter_reg[12]_i_1__0_n_2 ;
  wire \time_out_counter_reg[12]_i_1__0_n_3 ;
  wire \time_out_counter_reg[12]_i_1__0_n_4 ;
  wire \time_out_counter_reg[12]_i_1__0_n_5 ;
  wire \time_out_counter_reg[12]_i_1__0_n_6 ;
  wire \time_out_counter_reg[12]_i_1__0_n_7 ;
  wire \time_out_counter_reg[16]_i_1__0_n_3 ;
  wire \time_out_counter_reg[16]_i_1__0_n_6 ;
  wire \time_out_counter_reg[16]_i_1__0_n_7 ;
  wire \time_out_counter_reg[4]_i_1__0_n_0 ;
  wire \time_out_counter_reg[4]_i_1__0_n_1 ;
  wire \time_out_counter_reg[4]_i_1__0_n_2 ;
  wire \time_out_counter_reg[4]_i_1__0_n_3 ;
  wire \time_out_counter_reg[4]_i_1__0_n_4 ;
  wire \time_out_counter_reg[4]_i_1__0_n_5 ;
  wire \time_out_counter_reg[4]_i_1__0_n_6 ;
  wire \time_out_counter_reg[4]_i_1__0_n_7 ;
  wire \time_out_counter_reg[8]_i_1__0_n_0 ;
  wire \time_out_counter_reg[8]_i_1__0_n_1 ;
  wire \time_out_counter_reg[8]_i_1__0_n_2 ;
  wire \time_out_counter_reg[8]_i_1__0_n_3 ;
  wire \time_out_counter_reg[8]_i_1__0_n_4 ;
  wire \time_out_counter_reg[8]_i_1__0_n_5 ;
  wire \time_out_counter_reg[8]_i_1__0_n_6 ;
  wire \time_out_counter_reg[8]_i_1__0_n_7 ;
  wire time_out_wait_bypass_i_1__0_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire time_tlock_max1_carry__0_i_1_n_0;
  wire time_tlock_max1_carry__0_i_2_n_0;
  wire time_tlock_max1_carry__0_i_3_n_0;
  wire time_tlock_max1_carry__0_i_4_n_0;
  wire time_tlock_max1_carry__0_i_5_n_0;
  wire time_tlock_max1_carry__0_i_6_n_0;
  wire time_tlock_max1_carry__0_n_0;
  wire time_tlock_max1_carry__0_n_1;
  wire time_tlock_max1_carry__0_n_2;
  wire time_tlock_max1_carry__0_n_3;
  wire time_tlock_max1_carry__1_i_1_n_0;
  wire time_tlock_max1_carry__1_i_2_n_0;
  wire time_tlock_max1_carry_i_1_n_0;
  wire time_tlock_max1_carry_i_2_n_0;
  wire time_tlock_max1_carry_i_3_n_0;
  wire time_tlock_max1_carry_i_4_n_0;
  wire time_tlock_max1_carry_i_5_n_0;
  wire time_tlock_max1_carry_i_6_n_0;
  wire time_tlock_max1_carry_i_7_n_0;
  wire time_tlock_max1_carry_n_0;
  wire time_tlock_max1_carry_n_1;
  wire time_tlock_max1_carry_n_2;
  wire time_tlock_max1_carry_n_3;
  wire time_tlock_max_i_1_n_0;
  wire \wait_bypass_count[0]_i_10__0_n_0 ;
  wire \wait_bypass_count[0]_i_1__0_n_0 ;
  wire \wait_bypass_count[0]_i_2__0_n_0 ;
  wire \wait_bypass_count[0]_i_4__0_n_0 ;
  wire \wait_bypass_count[0]_i_5__0_n_0 ;
  wire \wait_bypass_count[0]_i_6__0_n_0 ;
  wire \wait_bypass_count[0]_i_7__0_n_0 ;
  wire \wait_bypass_count[0]_i_8__0_n_0 ;
  wire \wait_bypass_count[0]_i_9__0_n_0 ;
  wire \wait_bypass_count[12]_i_2__0_n_0 ;
  wire \wait_bypass_count[4]_i_2__0_n_0 ;
  wire \wait_bypass_count[4]_i_3__0_n_0 ;
  wire \wait_bypass_count[4]_i_4__0_n_0 ;
  wire \wait_bypass_count[4]_i_5__0_n_0 ;
  wire \wait_bypass_count[8]_i_2__0_n_0 ;
  wire \wait_bypass_count[8]_i_3__0_n_0 ;
  wire \wait_bypass_count[8]_i_4__0_n_0 ;
  wire \wait_bypass_count[8]_i_5__0_n_0 ;
  wire [12:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3__0_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_7 ;
  wire [6:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[6]_i_2__0_n_0 ;
  wire \wait_time_cnt[6]_i_4__0_n_0 ;
  wire [6:0]wait_time_cnt_reg__0;
  wire [3:1]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:0]NLW_time_tlock_max1_carry_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__0_O_UNCONNECTED;
  wire [3:1]NLW_time_tlock_max1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

  LUT3 #(
    .INIT(8'h74)) 
    CPLL_RESET_i_1__0
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(CPLL_RESET_i_2__0_n_0),
        .I2(p_4_in),
        .O(CPLL_RESET_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h0000000000000200)) 
    CPLL_RESET_i_2__0
       (.I0(rx_state[0]),
        .I1(rx_state[2]),
        .I2(gt0_rxsysclksel_in),
        .I3(gt0_txsysclksel_in),
        .I4(rx_state[3]),
        .I5(rx_state[1]),
        .O(CPLL_RESET_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(CPLL_RESET_i_1__0_n_0),
        .Q(p_4_in),
        .R(SOFT_RESET_RX_IN));
  LUT6 #(
    .INIT(64'h4E0AEE2A4E0ACE0A)) 
    \FSM_sequential_rx_state[0]_i_2 
       (.I0(rx_state[2]),
        .I1(rx_state[1]),
        .I2(rx_state[0]),
        .I3(time_out_2ms_reg_n_0),
        .I4(reset_time_out_reg_n_0),
        .I5(time_tlock_max),
        .O(\FSM_sequential_rx_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1111004015150040)) 
    \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(time_out_2ms_reg_n_0),
        .I4(rx_state[2]),
        .I5(rx_state128_out),
        .O(\FSM_sequential_rx_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_rx_state[2]_i_2 
       (.I0(time_tlock_max),
        .I1(reset_time_out_reg_n_0),
        .O(rx_state128_out));
  LUT4 #(
    .INIT(16'h3B38)) 
    \FSM_sequential_rx_state[3]_i_6 
       (.I0(gt0_rx_cdrlocked_reg_0),
        .I1(rx_state[2]),
        .I2(rx_state[3]),
        .I3(init_wait_done_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_6_n_0 ));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_rx_state_reg[0] 
       (.C(SYSCLK_IN),
        .CE(sync_data_valid_n_5),
        .D(sync_data_valid_n_4),
        .Q(rx_state[0]),
        .R(SOFT_RESET_RX_IN));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_rx_state_reg[1] 
       (.C(SYSCLK_IN),
        .CE(sync_data_valid_n_5),
        .D(sync_data_valid_n_3),
        .Q(rx_state[1]),
        .R(SOFT_RESET_RX_IN));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_rx_state_reg[2] 
       (.C(SYSCLK_IN),
        .CE(sync_data_valid_n_5),
        .D(\FSM_sequential_rx_state[2]_i_1_n_0 ),
        .Q(rx_state[2]),
        .R(SOFT_RESET_RX_IN));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_rx_state_reg[3] 
       (.C(SYSCLK_IN),
        .CE(sync_data_valid_n_5),
        .D(sync_data_valid_n_2),
        .Q(rx_state[3]),
        .R(SOFT_RESET_RX_IN));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(p_3_in),
        .O(RXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    RXUSERRDY_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(RXUSERRDY_i_1_n_0),
        .Q(p_3_in),
        .R(SOFT_RESET_RX_IN));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(check_tlock_max_reg_n_0),
        .O(check_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    check_tlock_max_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(check_tlock_max_i_1_n_0),
        .Q(check_tlock_max_reg_n_0),
        .R(SOFT_RESET_RX_IN));
  LUT6 #(
    .INIT(64'h00000000AABAAAAA)) 
    gt0_rx_cdrlocked_i_1
       (.I0(gt0_rx_cdrlocked_reg_0),
        .I1(\gt0_rx_cdrlock_counter_reg[0] ),
        .I2(\gt0_rx_cdrlock_counter_reg[3] ),
        .I3(Q),
        .I4(\gt0_rx_cdrlock_counter_reg[7] ),
        .I5(SR),
        .O(gt0_rx_cdrlocked_reg));
  LUT2 #(
    .INIT(4'hE)) 
    gthe2_i_i_3
       (.I0(gt0_gtrxreset_in),
        .I1(p_2_in),
        .O(SR));
  LUT2 #(
    .INIT(4'hE)) 
    gthe2_i_i_5
       (.I0(gt0_rxuserrdy_in),
        .I1(p_3_in),
        .O(gt0_rxuserrdy_in3_out));
  LUT5 #(
    .INIT(32'hFFFD0004)) 
    gtrxreset_i_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(p_2_in),
        .O(gtrxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtrxreset_i_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(gtrxreset_i_i_1_n_0),
        .Q(p_2_in),
        .R(SOFT_RESET_RX_IN));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg__0[0]),
        .O(\init_wait_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg__0[0]),
        .I1(init_wait_count_reg__0[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg__0[1]),
        .I1(init_wait_count_reg__0[0]),
        .I2(init_wait_count_reg__0[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg__0[2]),
        .I1(init_wait_count_reg__0[0]),
        .I2(init_wait_count_reg__0[1]),
        .I3(init_wait_count_reg__0[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg__0[3]),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[2]),
        .I4(init_wait_count_reg__0[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF7FFF)) 
    \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[5]),
        .I2(init_wait_count_reg__0[2]),
        .I3(init_wait_count_reg__0[3]),
        .I4(init_wait_count_reg__0[1]),
        .I5(init_wait_count_reg__0[0]),
        .O(\init_wait_count[5]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_2__0 
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[2]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[1]),
        .I4(init_wait_count_reg__0[3]),
        .I5(init_wait_count_reg__0[5]),
        .O(p_0_in[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1__0_n_0 ),
        .CLR(SOFT_RESET_RX_IN),
        .D(\init_wait_count[0]_i_1__0_n_0 ),
        .Q(init_wait_count_reg__0[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1__0_n_0 ),
        .CLR(SOFT_RESET_RX_IN),
        .D(p_0_in[1]),
        .Q(init_wait_count_reg__0[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1__0_n_0 ),
        .CLR(SOFT_RESET_RX_IN),
        .D(p_0_in[2]),
        .Q(init_wait_count_reg__0[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1__0_n_0 ),
        .CLR(SOFT_RESET_RX_IN),
        .D(p_0_in[3]),
        .Q(init_wait_count_reg__0[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1__0_n_0 ),
        .CLR(SOFT_RESET_RX_IN),
        .D(p_0_in[4]),
        .Q(init_wait_count_reg__0[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1__0_n_0 ),
        .CLR(SOFT_RESET_RX_IN),
        .D(p_0_in[5]),
        .Q(init_wait_count_reg__0[5]));
  LUT6 #(
    .INIT(64'hFFFFFFFF02000000)) 
    init_wait_done_i_1__0
       (.I0(init_wait_done_i_2__0_n_0),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[3]),
        .I4(init_wait_count_reg__0[2]),
        .I5(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    init_wait_done_i_2__0
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[5]),
        .O(init_wait_done_i_2__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .CLR(SOFT_RESET_RX_IN),
        .D(init_wait_done_i_1__0_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[0]),
        .I1(mmcm_lock_count_reg__0[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[1]),
        .I1(mmcm_lock_count_reg__0[0]),
        .I2(mmcm_lock_count_reg__0[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[2]),
        .I1(mmcm_lock_count_reg__0[0]),
        .I2(mmcm_lock_count_reg__0[1]),
        .I3(mmcm_lock_count_reg__0[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[3]),
        .I1(mmcm_lock_count_reg__0[1]),
        .I2(mmcm_lock_count_reg__0[0]),
        .I3(mmcm_lock_count_reg__0[2]),
        .I4(mmcm_lock_count_reg__0[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[4]),
        .I1(mmcm_lock_count_reg__0[2]),
        .I2(mmcm_lock_count_reg__0[0]),
        .I3(mmcm_lock_count_reg__0[1]),
        .I4(mmcm_lock_count_reg__0[3]),
        .I5(mmcm_lock_count_reg__0[5]),
        .O(p_0_in__0[5]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1__0 
       (.I0(\mmcm_lock_count[7]_i_4__0_n_0 ),
        .I1(mmcm_lock_count_reg__0[6]),
        .O(p_0_in__0[6]));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2__0 
       (.I0(\mmcm_lock_count[7]_i_4__0_n_0 ),
        .I1(mmcm_lock_count_reg__0[6]),
        .I2(mmcm_lock_count_reg__0[7]),
        .O(\mmcm_lock_count[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3__0 
       (.I0(mmcm_lock_count_reg__0[6]),
        .I1(\mmcm_lock_count[7]_i_4__0_n_0 ),
        .I2(mmcm_lock_count_reg__0[7]),
        .O(p_0_in__0[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \mmcm_lock_count[7]_i_4__0 
       (.I0(mmcm_lock_count_reg__0[4]),
        .I1(mmcm_lock_count_reg__0[2]),
        .I2(mmcm_lock_count_reg__0[0]),
        .I3(mmcm_lock_count_reg__0[1]),
        .I4(mmcm_lock_count_reg__0[3]),
        .I5(mmcm_lock_count_reg__0[5]),
        .O(\mmcm_lock_count[7]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__0[0]),
        .Q(mmcm_lock_count_reg__0[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__0[1]),
        .Q(mmcm_lock_count_reg__0[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__0[2]),
        .Q(mmcm_lock_count_reg__0[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__0[3]),
        .Q(mmcm_lock_count_reg__0[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__0[4]),
        .Q(mmcm_lock_count_reg__0[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__0[5]),
        .Q(mmcm_lock_count_reg__0[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__0[6]),
        .Q(mmcm_lock_count_reg__0[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__0[7]),
        .Q(mmcm_lock_count_reg__0[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(sync_mmcm_lock_reclocked_n_1),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFC0001)) 
    pll_reset_asserted_i_1__0
       (.I0(rx_state[1]),
        .I1(rx_state[3]),
        .I2(rx_state[2]),
        .I3(pll_reset_asserted_i_2_n_0),
        .I4(pll_reset_asserted_reg_n_0),
        .O(pll_reset_asserted_i_1__0_n_0));
  LUT5 #(
    .INIT(32'h00EBFFFF)) 
    pll_reset_asserted_i_2
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(gt0_rxsysclksel_in),
        .I2(gt0_txsysclksel_in),
        .I3(rx_state[1]),
        .I4(rx_state[0]),
        .O(pll_reset_asserted_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1__0_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(SOFT_RESET_RX_IN));
  LUT3 #(
    .INIT(8'h02)) 
    reset_time_out_i_2__0
       (.I0(gt0_rx_cdrlocked_reg_0),
        .I1(rx_state[0]),
        .I2(rx_state[3]),
        .O(reset_time_out_i_2__0_n_0));
  LUT5 #(
    .INIT(32'h44400040)) 
    reset_time_out_i_4
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(mmcm_lock_reclocked),
        .I3(rx_state[1]),
        .I4(rxresetdone_s3),
        .O(reset_time_out_i_4_n_0));
  FDSE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(sync_QPLLLOCK_n_2),
        .Q(reset_time_out_reg_n_0),
        .S(SOFT_RESET_RX_IN));
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1__0
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1__0_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(SOFT_RESET_RX_IN));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(run_phase_alignment_int_s2),
        .Q(run_phase_alignment_int_s3_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(sync_data_valid_n_1),
        .Q(GT_RX_FSM_RESET_DONE_OUT),
        .R(SOFT_RESET_RX_IN));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_s3_reg
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxresetdone_s3_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_6 sync_CPLLLOCK
       (.SYSCLK_IN(SYSCLK_IN),
        .data_out(cplllock_sync),
        .gt0_cplllock_out(gt0_cplllock_out));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_7 sync_QPLLLOCK
       (.\FSM_sequential_rx_state_reg[0] (sync_QPLLLOCK_n_1),
        .\FSM_sequential_rx_state_reg[3] (reset_time_out_i_4_n_0),
        .SYSCLK_IN(SYSCLK_IN),
        .data_out(cplllock_sync),
        .data_sync_reg6_0(data_valid_sync),
        .gt0_rx_cdrlocked_reg(reset_time_out_i_2__0_n_0),
        .gt0_rx_cdrlocked_reg_0(gt0_rx_cdrlocked_reg_0),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_in),
        .gt0_txsysclksel_in(gt0_txsysclksel_in),
        .out(rx_state),
        .pll_reset_asserted_reg(pll_reset_asserted_reg_n_0),
        .reset_time_out_reg(sync_QPLLLOCK_n_2),
        .reset_time_out_reg_0(reset_time_out_reg_n_0),
        .rx_state01_out(rx_state01_out),
        .rxresetdone_s3(rxresetdone_s3),
        .time_out_2ms_reg(time_out_2ms_reg_n_0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_8 sync_RXRESETDONE
       (.SYSCLK_IN(SYSCLK_IN),
        .data_in(data_in),
        .data_out(rxresetdone_s2));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_9 sync_data_valid
       (.D({sync_data_valid_n_2,sync_data_valid_n_3,sync_data_valid_n_4}),
        .DONT_RESET_ON_DATA_ERROR_IN(DONT_RESET_ON_DATA_ERROR_IN),
        .E(sync_data_valid_n_5),
        .\FSM_sequential_rx_state_reg[2] (\FSM_sequential_rx_state[0]_i_2_n_0 ),
        .GT0_DATA_VALID_IN(GT0_DATA_VALID_IN),
        .GT_RX_FSM_RESET_DONE_OUT(GT_RX_FSM_RESET_DONE_OUT),
        .Q(wait_time_cnt_reg__0[6]),
        .SYSCLK_IN(SYSCLK_IN),
        .data_out(data_valid_sync),
        .gt0_rx_cdrlocked_reg(\FSM_sequential_rx_state[3]_i_6_n_0 ),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .out(rx_state),
        .reset_time_out_reg(reset_time_out_reg_n_0),
        .rx_fsm_reset_done_int_reg(sync_data_valid_n_1),
        .rx_state01_out(rx_state01_out),
        .rx_state128_out(rx_state128_out),
        .time_out_100us_reg(time_out_100us_reg_n_0),
        .time_out_1us_reg(time_out_1us_reg_n_0),
        .time_out_2ms_reg(sync_QPLLLOCK_n_1),
        .time_out_2ms_reg_0(time_out_2ms_reg_n_0),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3),
        .\wait_time_cnt_reg[4] (\wait_time_cnt[6]_i_4__0_n_0 ));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_10 sync_mmcm_lock_reclocked
       (.Q(mmcm_lock_count_reg__0[7:6]),
        .SR(sync_mmcm_lock_reclocked_n_0),
        .SYSCLK_IN(SYSCLK_IN),
        .\mmcm_lock_count_reg[4] (\mmcm_lock_count[7]_i_4__0_n_0 ),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .mmcm_lock_reclocked_reg(sync_mmcm_lock_reclocked_n_1));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_11 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(run_phase_alignment_int_s2),
        .gt0_rxusrclk_in(gt0_rxusrclk_in));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_12 sync_rx_fsm_reset_done_int
       (.GT_RX_FSM_RESET_DONE_OUT(GT_RX_FSM_RESET_DONE_OUT),
        .data_out(rx_fsm_reset_done_int_s2),
        .gt0_rxusrclk_in(gt0_rxusrclk_in));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_13 sync_time_out_wait_bypass
       (.SYSCLK_IN(SYSCLK_IN),
        .data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2));
  LUT4 #(
    .INIT(16'hFF02)) 
    time_out_100us_i_1
       (.I0(time_out_100us_i_2_n_0),
        .I1(time_out_100us_i_3_n_0),
        .I2(\time_out_counter[0]_i_3_n_0 ),
        .I3(time_out_100us_reg_n_0),
        .O(time_out_100us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000004000000)) 
    time_out_100us_i_2
       (.I0(time_out_100us_i_4_n_0),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[4]),
        .I4(time_out_counter_reg[9]),
        .I5(time_out_counter_reg[6]),
        .O(time_out_100us_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    time_out_100us_i_3
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[0]),
        .I3(time_out_counter_reg[1]),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[7]),
        .O(time_out_100us_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'hE)) 
    time_out_100us_i_4
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_out_100us_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_100us_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_out_100us_i_1_n_0),
        .Q(time_out_100us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFFFF0004)) 
    time_out_1us_i_1
       (.I0(\time_out_counter[0]_i_4_n_0 ),
        .I1(time_out_1us_i_2_n_0),
        .I2(time_out_counter_reg[17]),
        .I3(time_out_counter_reg[16]),
        .I4(time_out_1us_reg_n_0),
        .O(time_out_1us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_out_1us_i_2
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[8]),
        .I2(time_out_counter_reg[2]),
        .I3(time_out_counter_reg[3]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[10]),
        .O(time_out_1us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_1us_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_out_1us_i_1_n_0),
        .Q(time_out_1us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00004000)) 
    time_out_2ms_i_1
       (.I0(\time_out_counter[0]_i_4_n_0 ),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[17]),
        .I3(time_out_counter_reg[11]),
        .I4(\time_out_counter[0]_i_3_n_0 ),
        .I5(time_out_2ms_reg_n_0),
        .O(time_out_2ms_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT5 #(
    .INIT(32'hFFFFBFFF)) 
    \time_out_counter[0]_i_1 
       (.I0(\time_out_counter[0]_i_3_n_0 ),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[17]),
        .I3(time_out_counter_reg[11]),
        .I4(\time_out_counter[0]_i_4_n_0 ),
        .O(time_out_counter));
  LUT5 #(
    .INIT(32'hFEFFFFFF)) 
    \time_out_counter[0]_i_3 
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[2]),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[8]),
        .O(\time_out_counter[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[4]),
        .I3(time_out_counter_reg[6]),
        .I4(time_out_100us_i_3_n_0),
        .O(\time_out_counter[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[0]_i_5__0 
       (.I0(time_out_counter_reg[3]),
        .O(\time_out_counter[0]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[0]_i_6__0 
       (.I0(time_out_counter_reg[2]),
        .O(\time_out_counter[0]_i_6__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[0]_i_7__0 
       (.I0(time_out_counter_reg[1]),
        .O(\time_out_counter[0]_i_7__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_8 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[12]_i_2__0 
       (.I0(time_out_counter_reg[15]),
        .O(\time_out_counter[12]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[12]_i_3__0 
       (.I0(time_out_counter_reg[14]),
        .O(\time_out_counter[12]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[12]_i_4__0 
       (.I0(time_out_counter_reg[13]),
        .O(\time_out_counter[12]_i_4__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[12]_i_5__0 
       (.I0(time_out_counter_reg[12]),
        .O(\time_out_counter[12]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[16]_i_2__0 
       (.I0(time_out_counter_reg[17]),
        .O(\time_out_counter[16]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[16]_i_3__0 
       (.I0(time_out_counter_reg[16]),
        .O(\time_out_counter[16]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[4]_i_2__0 
       (.I0(time_out_counter_reg[7]),
        .O(\time_out_counter[4]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[4]_i_3__0 
       (.I0(time_out_counter_reg[6]),
        .O(\time_out_counter[4]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[4]_i_4__0 
       (.I0(time_out_counter_reg[5]),
        .O(\time_out_counter[4]_i_4__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[4]_i_5__0 
       (.I0(time_out_counter_reg[4]),
        .O(\time_out_counter[4]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[8]_i_2__0 
       (.I0(time_out_counter_reg[11]),
        .O(\time_out_counter[8]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[8]_i_3__0 
       (.I0(time_out_counter_reg[10]),
        .O(\time_out_counter[8]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[8]_i_4__0 
       (.I0(time_out_counter_reg[9]),
        .O(\time_out_counter[8]_i_4__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[8]_i_5__0 
       (.I0(time_out_counter_reg[8]),
        .O(\time_out_counter[8]_i_5__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out_reg_n_0));
  CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2__0_n_0 ,\time_out_counter_reg[0]_i_2__0_n_1 ,\time_out_counter_reg[0]_i_2__0_n_2 ,\time_out_counter_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2__0_n_4 ,\time_out_counter_reg[0]_i_2__0_n_5 ,\time_out_counter_reg[0]_i_2__0_n_6 ,\time_out_counter_reg[0]_i_2__0_n_7 }),
        .S({\time_out_counter[0]_i_5__0_n_0 ,\time_out_counter[0]_i_6__0_n_0 ,\time_out_counter[0]_i_7__0_n_0 ,\time_out_counter[0]_i_8_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out_reg_n_0));
  CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\time_out_counter_reg[8]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1__0_n_0 ,\time_out_counter_reg[12]_i_1__0_n_1 ,\time_out_counter_reg[12]_i_1__0_n_2 ,\time_out_counter_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1__0_n_4 ,\time_out_counter_reg[12]_i_1__0_n_5 ,\time_out_counter_reg[12]_i_1__0_n_6 ,\time_out_counter_reg[12]_i_1__0_n_7 }),
        .S({\time_out_counter[12]_i_2__0_n_0 ,\time_out_counter[12]_i_3__0_n_0 ,\time_out_counter[12]_i_4__0_n_0 ,\time_out_counter[12]_i_5__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out_reg_n_0));
  CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\time_out_counter_reg[12]_i_1__0_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:1],\time_out_counter_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1__0_n_6 ,\time_out_counter_reg[16]_i_1__0_n_7 }),
        .S({1'b0,1'b0,\time_out_counter[16]_i_2__0_n_0 ,\time_out_counter[16]_i_3__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out_reg_n_0));
  CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\time_out_counter_reg[0]_i_2__0_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1__0_n_0 ,\time_out_counter_reg[4]_i_1__0_n_1 ,\time_out_counter_reg[4]_i_1__0_n_2 ,\time_out_counter_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1__0_n_4 ,\time_out_counter_reg[4]_i_1__0_n_5 ,\time_out_counter_reg[4]_i_1__0_n_6 ,\time_out_counter_reg[4]_i_1__0_n_7 }),
        .S({\time_out_counter[4]_i_2__0_n_0 ,\time_out_counter[4]_i_3__0_n_0 ,\time_out_counter[4]_i_4__0_n_0 ,\time_out_counter[4]_i_5__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out_reg_n_0));
  CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\time_out_counter_reg[4]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1__0_n_0 ,\time_out_counter_reg[8]_i_1__0_n_1 ,\time_out_counter_reg[8]_i_1__0_n_2 ,\time_out_counter_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1__0_n_4 ,\time_out_counter_reg[8]_i_1__0_n_5 ,\time_out_counter_reg[8]_i_1__0_n_6 ,\time_out_counter_reg[8]_i_1__0_n_7 }),
        .S({\time_out_counter[8]_i_2__0_n_0 ,\time_out_counter[8]_i_3__0_n_0 ,\time_out_counter[8]_i_4__0_n_0 ,\time_out_counter[8]_i_5__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out_reg_n_0));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1__0
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .I2(\wait_bypass_count[0]_i_4__0_n_0 ),
        .I3(run_phase_alignment_int_s3_reg_n_0),
        .O(time_out_wait_bypass_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1__0_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  CARRY4 time_tlock_max1_carry
       (.CI(1'b0),
        .CO({time_tlock_max1_carry_n_0,time_tlock_max1_carry_n_1,time_tlock_max1_carry_n_2,time_tlock_max1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry_i_1_n_0,time_out_counter_reg[5],time_tlock_max1_carry_i_2_n_0,time_tlock_max1_carry_i_3_n_0}),
        .O(NLW_time_tlock_max1_carry_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry_i_4_n_0,time_tlock_max1_carry_i_5_n_0,time_tlock_max1_carry_i_6_n_0,time_tlock_max1_carry_i_7_n_0}));
  CARRY4 time_tlock_max1_carry__0
       (.CI(time_tlock_max1_carry_n_0),
        .CO({time_tlock_max1_carry__0_n_0,time_tlock_max1_carry__0_n_1,time_tlock_max1_carry__0_n_2,time_tlock_max1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry__0_i_1_n_0,time_tlock_max1_carry__0_i_2_n_0,time_out_counter_reg[11],1'b0}),
        .O(NLW_time_tlock_max1_carry__0_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry__0_i_3_n_0,time_tlock_max1_carry__0_i_4_n_0,time_tlock_max1_carry__0_i_5_n_0,time_tlock_max1_carry__0_i_6_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__0_i_1
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(time_tlock_max1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_2
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(time_tlock_max1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__0_i_3
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[14]),
        .O(time_tlock_max1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_4
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .O(time_tlock_max1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_5
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(time_tlock_max1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_6
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(time_tlock_max1_carry__0_i_6_n_0));
  CARRY4 time_tlock_max1_carry__1
       (.CI(time_tlock_max1_carry__0_n_0),
        .CO({NLW_time_tlock_max1_carry__1_CO_UNCONNECTED[3:1],time_tlock_max1}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,time_tlock_max1_carry__1_i_1_n_0}),
        .O(NLW_time_tlock_max1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,time_tlock_max1_carry__1_i_2_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__1_i_1
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_tlock_max1_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__1_i_2
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(time_tlock_max1_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_1
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_2
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(time_tlock_max1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_3
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .O(time_tlock_max1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_4
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry_i_5
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(time_tlock_max1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_6
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(time_tlock_max1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_7
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(time_tlock_max1_carry_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    time_tlock_max_i_1
       (.I0(check_tlock_max_reg_n_0),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(time_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \wait_bypass_count[0]_i_10__0 
       (.I0(wait_bypass_count_reg[2]),
        .I1(wait_bypass_count_reg[12]),
        .I2(wait_bypass_count_reg[4]),
        .I3(wait_bypass_count_reg[10]),
        .I4(wait_bypass_count_reg[6]),
        .I5(wait_bypass_count_reg[11]),
        .O(\wait_bypass_count[0]_i_10__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1__0 
       (.I0(run_phase_alignment_int_s3_reg_n_0),
        .O(\wait_bypass_count[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2__0 
       (.I0(\wait_bypass_count[0]_i_4__0_n_0 ),
        .I1(rx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2__0_n_0 ));
  LUT5 #(
    .INIT(32'hBFFFFFFF)) 
    \wait_bypass_count[0]_i_4__0 
       (.I0(\wait_bypass_count[0]_i_9__0_n_0 ),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[8]),
        .I3(wait_bypass_count_reg[0]),
        .I4(\wait_bypass_count[0]_i_10__0_n_0 ),
        .O(\wait_bypass_count[0]_i_4__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[0]_i_5__0 
       (.I0(wait_bypass_count_reg[3]),
        .O(\wait_bypass_count[0]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[0]_i_6__0 
       (.I0(wait_bypass_count_reg[2]),
        .O(\wait_bypass_count[0]_i_6__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[0]_i_7__0 
       (.I0(wait_bypass_count_reg[1]),
        .O(\wait_bypass_count[0]_i_7__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_8__0 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_8__0_n_0 ));
  LUT4 #(
    .INIT(16'hEFFF)) 
    \wait_bypass_count[0]_i_9__0 
       (.I0(wait_bypass_count_reg[3]),
        .I1(wait_bypass_count_reg[5]),
        .I2(wait_bypass_count_reg[9]),
        .I3(wait_bypass_count_reg[7]),
        .O(\wait_bypass_count[0]_i_9__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[12]_i_2__0 
       (.I0(wait_bypass_count_reg[12]),
        .O(\wait_bypass_count[12]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[4]_i_2__0 
       (.I0(wait_bypass_count_reg[7]),
        .O(\wait_bypass_count[4]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[4]_i_3__0 
       (.I0(wait_bypass_count_reg[6]),
        .O(\wait_bypass_count[4]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[4]_i_4__0 
       (.I0(wait_bypass_count_reg[5]),
        .O(\wait_bypass_count[4]_i_4__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[4]_i_5__0 
       (.I0(wait_bypass_count_reg[4]),
        .O(\wait_bypass_count[4]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[8]_i_2__0 
       (.I0(wait_bypass_count_reg[11]),
        .O(\wait_bypass_count[8]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[8]_i_3__0 
       (.I0(wait_bypass_count_reg[10]),
        .O(\wait_bypass_count[8]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[8]_i_4__0 
       (.I0(wait_bypass_count_reg[9]),
        .O(\wait_bypass_count[8]_i_4__0_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[8]_i_5__0 
       (.I0(wait_bypass_count_reg[8]),
        .O(\wait_bypass_count[8]_i_5__0_n_0 ));
  FDRE \wait_bypass_count_reg[0] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3__0_n_0 ,\wait_bypass_count_reg[0]_i_3__0_n_1 ,\wait_bypass_count_reg[0]_i_3__0_n_2 ,\wait_bypass_count_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3__0_n_4 ,\wait_bypass_count_reg[0]_i_3__0_n_5 ,\wait_bypass_count_reg[0]_i_3__0_n_6 ,\wait_bypass_count_reg[0]_i_3__0_n_7 }),
        .S({\wait_bypass_count[0]_i_5__0_n_0 ,\wait_bypass_count[0]_i_6__0_n_0 ,\wait_bypass_count[0]_i_7__0_n_0 ,\wait_bypass_count[0]_i_8__0_n_0 }));
  FDRE \wait_bypass_count_reg[10] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE \wait_bypass_count_reg[11] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE \wait_bypass_count_reg[12] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\wait_bypass_count_reg[8]_i_1__0_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\wait_bypass_count_reg[12]_i_1__0_n_7 }),
        .S({1'b0,1'b0,1'b0,\wait_bypass_count[12]_i_2__0_n_0 }));
  FDRE \wait_bypass_count_reg[1] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE \wait_bypass_count_reg[2] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE \wait_bypass_count_reg[3] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE \wait_bypass_count_reg[4] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\wait_bypass_count_reg[0]_i_3__0_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1__0_n_0 ,\wait_bypass_count_reg[4]_i_1__0_n_1 ,\wait_bypass_count_reg[4]_i_1__0_n_2 ,\wait_bypass_count_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1__0_n_4 ,\wait_bypass_count_reg[4]_i_1__0_n_5 ,\wait_bypass_count_reg[4]_i_1__0_n_6 ,\wait_bypass_count_reg[4]_i_1__0_n_7 }),
        .S({\wait_bypass_count[4]_i_2__0_n_0 ,\wait_bypass_count[4]_i_3__0_n_0 ,\wait_bypass_count[4]_i_4__0_n_0 ,\wait_bypass_count[4]_i_5__0_n_0 }));
  FDRE \wait_bypass_count_reg[5] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE \wait_bypass_count_reg[6] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE \wait_bypass_count_reg[7] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE \wait_bypass_count_reg[8] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\wait_bypass_count_reg[4]_i_1__0_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1__0_n_0 ,\wait_bypass_count_reg[8]_i_1__0_n_1 ,\wait_bypass_count_reg[8]_i_1__0_n_2 ,\wait_bypass_count_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1__0_n_4 ,\wait_bypass_count_reg[8]_i_1__0_n_5 ,\wait_bypass_count_reg[8]_i_1__0_n_6 ,\wait_bypass_count_reg[8]_i_1__0_n_7 }),
        .S({\wait_bypass_count[8]_i_2__0_n_0 ,\wait_bypass_count[8]_i_3__0_n_0 ,\wait_bypass_count[8]_i_4__0_n_0 ,\wait_bypass_count[8]_i_5__0_n_0 }));
  FDRE \wait_bypass_count_reg[9] 
       (.C(gt0_rxusrclk_in),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg__0[0]),
        .O(wait_time_cnt0[0]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg__0[1]),
        .I1(wait_time_cnt_reg__0[0]),
        .O(wait_time_cnt0[1]));
  LUT3 #(
    .INIT(8'hA9)) 
    \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg__0[2]),
        .I1(wait_time_cnt_reg__0[0]),
        .I2(wait_time_cnt_reg__0[1]),
        .O(wait_time_cnt0[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg__0[3]),
        .I1(wait_time_cnt_reg__0[1]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[2]),
        .O(wait_time_cnt0[3]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg__0[4]),
        .I1(wait_time_cnt_reg__0[2]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[1]),
        .I4(wait_time_cnt_reg__0[3]),
        .O(wait_time_cnt0[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg__0[5]),
        .I1(wait_time_cnt_reg__0[3]),
        .I2(wait_time_cnt_reg__0[1]),
        .I3(wait_time_cnt_reg__0[0]),
        .I4(wait_time_cnt_reg__0[2]),
        .I5(wait_time_cnt_reg__0[4]),
        .O(wait_time_cnt0[5]));
  LUT3 #(
    .INIT(8'h02)) 
    \wait_time_cnt[6]_i_1__0 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg__0[6]),
        .O(\wait_time_cnt[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3__0 
       (.I0(wait_time_cnt_reg__0[6]),
        .I1(\wait_time_cnt[6]_i_4__0_n_0 ),
        .O(wait_time_cnt0[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg__0[4]),
        .I1(wait_time_cnt_reg__0[2]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[1]),
        .I4(wait_time_cnt_reg__0[3]),
        .I5(wait_time_cnt_reg__0[5]),
        .O(\wait_time_cnt[6]_i_4__0_n_0 ));
  FDRE \wait_time_cnt_reg[0] 
       (.C(SYSCLK_IN),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0[0]),
        .Q(wait_time_cnt_reg__0[0]),
        .R(wait_time_cnt0_0));
  FDRE \wait_time_cnt_reg[1] 
       (.C(SYSCLK_IN),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0[1]),
        .Q(wait_time_cnt_reg__0[1]),
        .R(wait_time_cnt0_0));
  FDSE \wait_time_cnt_reg[2] 
       (.C(SYSCLK_IN),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0[2]),
        .Q(wait_time_cnt_reg__0[2]),
        .S(wait_time_cnt0_0));
  FDRE \wait_time_cnt_reg[3] 
       (.C(SYSCLK_IN),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0[3]),
        .Q(wait_time_cnt_reg__0[3]),
        .R(wait_time_cnt0_0));
  FDRE \wait_time_cnt_reg[4] 
       (.C(SYSCLK_IN),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0[4]),
        .Q(wait_time_cnt_reg__0[4]),
        .R(wait_time_cnt0_0));
  FDSE \wait_time_cnt_reg[5] 
       (.C(SYSCLK_IN),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0[5]),
        .Q(wait_time_cnt_reg__0[5]),
        .S(wait_time_cnt0_0));
  FDSE \wait_time_cnt_reg[6] 
       (.C(SYSCLK_IN),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0[6]),
        .Q(wait_time_cnt_reg__0[6]),
        .S(wait_time_cnt0_0));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_TX_STARTUP_FSM
   (p_5_in,
    GT_TX_FSM_RESET_DONE_OUT,
    gt0_gttxreset_in1_out,
    gt0_txuserrdy_in0_out,
    SYSCLK_IN,
    gt0_txusrclk_in,
    SOFT_RESET_TX_IN,
    gt0_txsysclksel_in,
    gt0_gttxreset_in,
    gt0_txuserrdy_in,
    data_in,
    gt0_cplllock_out);
  output p_5_in;
  output GT_TX_FSM_RESET_DONE_OUT;
  output gt0_gttxreset_in1_out;
  output gt0_txuserrdy_in0_out;
  input SYSCLK_IN;
  input gt0_txusrclk_in;
  input SOFT_RESET_TX_IN;
  input [0:0]gt0_txsysclksel_in;
  input gt0_gttxreset_in;
  input gt0_txuserrdy_in;
  input data_in;
  input gt0_cplllock_out;

  wire CPLL_RESET_i_1_n_0;
  wire CPLL_RESET_i_2_n_0;
  wire \FSM_sequential_tx_state[0]_i_1_n_0 ;
  wire \FSM_sequential_tx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[1]_i_1_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_1_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_11_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_9_n_0 ;
  wire GT_TX_FSM_RESET_DONE_OUT;
  wire SOFT_RESET_TX_IN;
  wire SYSCLK_IN;
  wire TXUSERRDY_i_1_n_0;
  wire clear;
  wire cplllock_sync;
  wire data_in;
  wire data_out;
  wire gt0_cplllock_out;
  wire gt0_gttxreset_in;
  wire gt0_gttxreset_in1_out;
  wire [0:0]gt0_txsysclksel_in;
  wire gt0_txuserrdy_in;
  wire gt0_txuserrdy_in0_out;
  wire gt0_txusrclk_in;
  wire gttxreset_i_i_1_n_0;
  wire \init_wait_count[0]_i_1_n_0 ;
  wire \init_wait_count[5]_i_1_n_0 ;
  wire [5:0]init_wait_count_reg__0;
  wire init_wait_done_i_1_n_0;
  wire init_wait_done_i_2_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[7]_i_2_n_0 ;
  wire \mmcm_lock_count[7]_i_4_n_0 ;
  wire [7:0]mmcm_lock_count_reg__0;
  wire mmcm_lock_reclocked;
  wire p_0_in;
  wire [5:1]p_0_in__0;
  wire [7:0]p_0_in__1;
  wire p_1_in;
  wire p_5_in;
  wire pll_reset_asserted_i_1_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire qplllock_sync;
  wire reset_time_out;
  wire reset_time_out_i_5_n_0;
  wire run_phase_alignment_int_i_1_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3;
  wire sel;
  wire sync_CPLLLOCK_n_1;
  wire sync_QPLLLOCK_n_1;
  wire sync_mmcm_lock_reclocked_n_0;
  wire sync_mmcm_lock_reclocked_n_1;
  wire time_out_2ms_i_1__0_n_0;
  wire time_out_2ms_i_2_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_500us_i_1_n_0;
  wire time_out_500us_i_2_n_0;
  wire time_out_500us_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_10_n_0 ;
  wire \time_out_counter[0]_i_11_n_0 ;
  wire \time_out_counter[0]_i_3__0_n_0 ;
  wire \time_out_counter[0]_i_4__0_n_0 ;
  wire \time_out_counter[0]_i_5_n_0 ;
  wire \time_out_counter[0]_i_6_n_0 ;
  wire \time_out_counter[0]_i_7_n_0 ;
  wire \time_out_counter[0]_i_8__0_n_0 ;
  wire \time_out_counter[0]_i_9_n_0 ;
  wire \time_out_counter[12]_i_2_n_0 ;
  wire \time_out_counter[12]_i_3_n_0 ;
  wire \time_out_counter[12]_i_4_n_0 ;
  wire \time_out_counter[12]_i_5_n_0 ;
  wire \time_out_counter[16]_i_2_n_0 ;
  wire \time_out_counter[16]_i_3_n_0 ;
  wire \time_out_counter[4]_i_2_n_0 ;
  wire \time_out_counter[4]_i_3_n_0 ;
  wire \time_out_counter[4]_i_4_n_0 ;
  wire \time_out_counter[4]_i_5_n_0 ;
  wire \time_out_counter[8]_i_2_n_0 ;
  wire \time_out_counter[8]_i_3_n_0 ;
  wire \time_out_counter[8]_i_4_n_0 ;
  wire \time_out_counter[8]_i_5_n_0 ;
  wire [17:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2_n_0 ;
  wire \time_out_counter_reg[0]_i_2_n_1 ;
  wire \time_out_counter_reg[0]_i_2_n_2 ;
  wire \time_out_counter_reg[0]_i_2_n_3 ;
  wire \time_out_counter_reg[0]_i_2_n_4 ;
  wire \time_out_counter_reg[0]_i_2_n_5 ;
  wire \time_out_counter_reg[0]_i_2_n_6 ;
  wire \time_out_counter_reg[0]_i_2_n_7 ;
  wire \time_out_counter_reg[12]_i_1_n_0 ;
  wire \time_out_counter_reg[12]_i_1_n_1 ;
  wire \time_out_counter_reg[12]_i_1_n_2 ;
  wire \time_out_counter_reg[12]_i_1_n_3 ;
  wire \time_out_counter_reg[12]_i_1_n_4 ;
  wire \time_out_counter_reg[12]_i_1_n_5 ;
  wire \time_out_counter_reg[12]_i_1_n_6 ;
  wire \time_out_counter_reg[12]_i_1_n_7 ;
  wire \time_out_counter_reg[16]_i_1_n_3 ;
  wire \time_out_counter_reg[16]_i_1_n_6 ;
  wire \time_out_counter_reg[16]_i_1_n_7 ;
  wire \time_out_counter_reg[4]_i_1_n_0 ;
  wire \time_out_counter_reg[4]_i_1_n_1 ;
  wire \time_out_counter_reg[4]_i_1_n_2 ;
  wire \time_out_counter_reg[4]_i_1_n_3 ;
  wire \time_out_counter_reg[4]_i_1_n_4 ;
  wire \time_out_counter_reg[4]_i_1_n_5 ;
  wire \time_out_counter_reg[4]_i_1_n_6 ;
  wire \time_out_counter_reg[4]_i_1_n_7 ;
  wire \time_out_counter_reg[8]_i_1_n_0 ;
  wire \time_out_counter_reg[8]_i_1_n_1 ;
  wire \time_out_counter_reg[8]_i_1_n_2 ;
  wire \time_out_counter_reg[8]_i_1_n_3 ;
  wire \time_out_counter_reg[8]_i_1_n_4 ;
  wire \time_out_counter_reg[8]_i_1_n_5 ;
  wire \time_out_counter_reg[8]_i_1_n_6 ;
  wire \time_out_counter_reg[8]_i_1_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max_i_1__0_n_0;
  wire time_tlock_max_i_2_n_0;
  wire time_tlock_max_i_3_n_0;
  wire time_tlock_max_reg_n_0;
  wire tx_fsm_reset_done_int_i_1_n_0;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
  (* RTL_KEEP = "yes" *) wire [3:0]tx_state;
  wire tx_state111_out;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire \wait_bypass_count[0]_i_10_n_0 ;
  wire \wait_bypass_count[0]_i_11_n_0 ;
  wire \wait_bypass_count[0]_i_12_n_0 ;
  wire \wait_bypass_count[0]_i_2_n_0 ;
  wire \wait_bypass_count[0]_i_4_n_0 ;
  wire \wait_bypass_count[0]_i_5_n_0 ;
  wire \wait_bypass_count[0]_i_6_n_0 ;
  wire \wait_bypass_count[0]_i_7_n_0 ;
  wire \wait_bypass_count[0]_i_8_n_0 ;
  wire \wait_bypass_count[0]_i_9_n_0 ;
  wire \wait_bypass_count[12]_i_2_n_0 ;
  wire \wait_bypass_count[12]_i_3_n_0 ;
  wire \wait_bypass_count[12]_i_4_n_0 ;
  wire \wait_bypass_count[12]_i_5_n_0 ;
  wire \wait_bypass_count[4]_i_2_n_0 ;
  wire \wait_bypass_count[4]_i_3_n_0 ;
  wire \wait_bypass_count[4]_i_4_n_0 ;
  wire \wait_bypass_count[4]_i_5_n_0 ;
  wire \wait_bypass_count[8]_i_2_n_0 ;
  wire \wait_bypass_count[8]_i_3_n_0 ;
  wire \wait_bypass_count[8]_i_4_n_0 ;
  wire \wait_bypass_count[8]_i_5_n_0 ;
  wire [15:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1_n_1 ;
  wire \wait_bypass_count_reg[12]_i_1_n_2 ;
  wire \wait_bypass_count_reg[12]_i_1_n_3 ;
  wire \wait_bypass_count_reg[12]_i_1_n_4 ;
  wire \wait_bypass_count_reg[12]_i_1_n_5 ;
  wire \wait_bypass_count_reg[12]_i_1_n_6 ;
  wire \wait_bypass_count_reg[12]_i_1_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1_n_7 ;
  wire [6:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[6]_i_4_n_0 ;
  wire [6:0]wait_time_cnt_reg__0;
  wire [3:1]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:3]\NLW_wait_bypass_count_reg[12]_i_1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFFF700000004)) 
    CPLL_RESET_i_1
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(gt0_txsysclksel_in),
        .I4(CPLL_RESET_i_2_n_0),
        .I5(p_5_in),
        .O(CPLL_RESET_i_1_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    CPLL_RESET_i_2
       (.I0(tx_state[3]),
        .I1(tx_state[1]),
        .O(CPLL_RESET_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(CPLL_RESET_i_1_n_0),
        .Q(p_5_in),
        .R(SOFT_RESET_TX_IN));
  LUT6 #(
    .INIT(64'h2222220222220A0A)) 
    \FSM_sequential_tx_state[0]_i_1 
       (.I0(\FSM_sequential_tx_state[0]_i_2_n_0 ),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(time_out_2ms_reg_n_0),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\FSM_sequential_tx_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h3B33BBBBBBBBBBBB)) 
    \FSM_sequential_tx_state[0]_i_2 
       (.I0(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I1(tx_state[0]),
        .I2(reset_time_out),
        .I3(time_out_500us_reg_n_0),
        .I4(tx_state[1]),
        .I5(tx_state[2]),
        .O(\FSM_sequential_tx_state[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h11110444)) 
    \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state111_out),
        .I3(tx_state[2]),
        .I4(tx_state[1]),
        .O(\FSM_sequential_tx_state[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h10)) 
    \FSM_sequential_tx_state[1]_i_2 
       (.I0(mmcm_lock_reclocked),
        .I1(reset_time_out),
        .I2(time_tlock_max_reg_n_0),
        .O(tx_state111_out));
  LUT6 #(
    .INIT(64'h1111004055550040)) 
    \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[1]),
        .I3(time_out_2ms_reg_n_0),
        .I4(tx_state[2]),
        .I5(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .O(\FSM_sequential_tx_state[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF02)) 
    \FSM_sequential_tx_state[2]_i_2 
       (.I0(time_tlock_max_reg_n_0),
        .I1(reset_time_out),
        .I2(mmcm_lock_reclocked),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[2]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hF4)) 
    \FSM_sequential_tx_state[3]_i_11 
       (.I0(reset_time_out),
        .I1(time_out_500us_reg_n_0),
        .I2(txresetdone_s3),
        .O(\FSM_sequential_tx_state[3]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'h00A00B00)) 
    \FSM_sequential_tx_state[3]_i_2 
       (.I0(\FSM_sequential_tx_state[3]_i_5_n_0 ),
        .I1(time_out_wait_bypass_s3),
        .I2(tx_state[2]),
        .I3(tx_state[3]),
        .I4(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_tx_state[3]_i_4 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(\FSM_sequential_tx_state[3]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h8A)) 
    \FSM_sequential_tx_state[3]_i_5 
       (.I0(tx_state[0]),
        .I1(reset_time_out),
        .I2(time_out_500us_reg_n_0),
        .O(\FSM_sequential_tx_state[3]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hF4)) 
    \FSM_sequential_tx_state[3]_i_9 
       (.I0(reset_time_out),
        .I1(time_tlock_max_reg_n_0),
        .I2(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[3]_i_9_n_0 ));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_tx_state_reg[0] 
       (.C(SYSCLK_IN),
        .CE(sync_CPLLLOCK_n_1),
        .D(\FSM_sequential_tx_state[0]_i_1_n_0 ),
        .Q(tx_state[0]),
        .R(SOFT_RESET_TX_IN));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_tx_state_reg[1] 
       (.C(SYSCLK_IN),
        .CE(sync_CPLLLOCK_n_1),
        .D(\FSM_sequential_tx_state[1]_i_1_n_0 ),
        .Q(tx_state[1]),
        .R(SOFT_RESET_TX_IN));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_tx_state_reg[2] 
       (.C(SYSCLK_IN),
        .CE(sync_CPLLLOCK_n_1),
        .D(\FSM_sequential_tx_state[2]_i_1_n_0 ),
        .Q(tx_state[2]),
        .R(SOFT_RESET_TX_IN));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_tx_state_reg[3] 
       (.C(SYSCLK_IN),
        .CE(sync_CPLLLOCK_n_1),
        .D(\FSM_sequential_tx_state[3]_i_2_n_0 ),
        .Q(tx_state[3]),
        .R(SOFT_RESET_TX_IN));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    TXUSERRDY_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[1]),
        .I3(tx_state[2]),
        .I4(p_0_in),
        .O(TXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    TXUSERRDY_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(TXUSERRDY_i_1_n_0),
        .Q(p_0_in),
        .R(SOFT_RESET_TX_IN));
  LUT2 #(
    .INIT(4'hE)) 
    gthe2_i_i_4
       (.I0(gt0_gttxreset_in),
        .I1(p_1_in),
        .O(gt0_gttxreset_in1_out));
  LUT2 #(
    .INIT(4'hE)) 
    gthe2_i_i_6
       (.I0(gt0_txuserrdy_in),
        .I1(p_0_in),
        .O(gt0_txuserrdy_in0_out));
  LUT5 #(
    .INIT(32'hFFFD0004)) 
    gttxreset_i_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[1]),
        .I3(tx_state[3]),
        .I4(p_1_in),
        .O(gttxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gttxreset_i_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(gttxreset_i_i_1_n_0),
        .Q(p_1_in),
        .R(SOFT_RESET_TX_IN));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg__0[0]),
        .O(\init_wait_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg__0[0]),
        .I1(init_wait_count_reg__0[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg__0[1]),
        .I1(init_wait_count_reg__0[0]),
        .I2(init_wait_count_reg__0[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg__0[2]),
        .I1(init_wait_count_reg__0[0]),
        .I2(init_wait_count_reg__0[1]),
        .I3(init_wait_count_reg__0[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg__0[3]),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[2]),
        .I4(init_wait_count_reg__0[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF7FFF)) 
    \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[5]),
        .I2(init_wait_count_reg__0[2]),
        .I3(init_wait_count_reg__0[3]),
        .I4(init_wait_count_reg__0[1]),
        .I5(init_wait_count_reg__0[0]),
        .O(\init_wait_count[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_2 
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[2]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[1]),
        .I4(init_wait_count_reg__0[3]),
        .I5(init_wait_count_reg__0[5]),
        .O(p_0_in__0[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1_n_0 ),
        .CLR(SOFT_RESET_TX_IN),
        .D(\init_wait_count[0]_i_1_n_0 ),
        .Q(init_wait_count_reg__0[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1_n_0 ),
        .CLR(SOFT_RESET_TX_IN),
        .D(p_0_in__0[1]),
        .Q(init_wait_count_reg__0[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1_n_0 ),
        .CLR(SOFT_RESET_TX_IN),
        .D(p_0_in__0[2]),
        .Q(init_wait_count_reg__0[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1_n_0 ),
        .CLR(SOFT_RESET_TX_IN),
        .D(p_0_in__0[3]),
        .Q(init_wait_count_reg__0[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1_n_0 ),
        .CLR(SOFT_RESET_TX_IN),
        .D(p_0_in__0[4]),
        .Q(init_wait_count_reg__0[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(SYSCLK_IN),
        .CE(\init_wait_count[5]_i_1_n_0 ),
        .CLR(SOFT_RESET_TX_IN),
        .D(p_0_in__0[5]),
        .Q(init_wait_count_reg__0[5]));
  LUT6 #(
    .INIT(64'hFFFFFFFF02000000)) 
    init_wait_done_i_1
       (.I0(init_wait_done_i_2_n_0),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[3]),
        .I4(init_wait_count_reg__0[2]),
        .I5(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    init_wait_done_i_2
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[5]),
        .O(init_wait_done_i_2_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .CLR(SOFT_RESET_TX_IN),
        .D(init_wait_done_i_1_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg__0[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg__0[0]),
        .I1(mmcm_lock_count_reg__0[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg__0[1]),
        .I1(mmcm_lock_count_reg__0[0]),
        .I2(mmcm_lock_count_reg__0[2]),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg__0[2]),
        .I1(mmcm_lock_count_reg__0[0]),
        .I2(mmcm_lock_count_reg__0[1]),
        .I3(mmcm_lock_count_reg__0[3]),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg__0[3]),
        .I1(mmcm_lock_count_reg__0[1]),
        .I2(mmcm_lock_count_reg__0[0]),
        .I3(mmcm_lock_count_reg__0[2]),
        .I4(mmcm_lock_count_reg__0[4]),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg__0[4]),
        .I1(mmcm_lock_count_reg__0[2]),
        .I2(mmcm_lock_count_reg__0[0]),
        .I3(mmcm_lock_count_reg__0[1]),
        .I4(mmcm_lock_count_reg__0[3]),
        .I5(mmcm_lock_count_reg__0[5]),
        .O(p_0_in__1[5]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1 
       (.I0(\mmcm_lock_count[7]_i_4_n_0 ),
        .I1(mmcm_lock_count_reg__0[6]),
        .O(p_0_in__1[6]));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2 
       (.I0(\mmcm_lock_count[7]_i_4_n_0 ),
        .I1(mmcm_lock_count_reg__0[6]),
        .I2(mmcm_lock_count_reg__0[7]),
        .O(\mmcm_lock_count[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3 
       (.I0(mmcm_lock_count_reg__0[6]),
        .I1(\mmcm_lock_count[7]_i_4_n_0 ),
        .I2(mmcm_lock_count_reg__0[7]),
        .O(p_0_in__1[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \mmcm_lock_count[7]_i_4 
       (.I0(mmcm_lock_count_reg__0[4]),
        .I1(mmcm_lock_count_reg__0[2]),
        .I2(mmcm_lock_count_reg__0[0]),
        .I3(mmcm_lock_count_reg__0[1]),
        .I4(mmcm_lock_count_reg__0[3]),
        .I5(mmcm_lock_count_reg__0[5]),
        .O(\mmcm_lock_count[7]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[0]),
        .Q(mmcm_lock_count_reg__0[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[1]),
        .Q(mmcm_lock_count_reg__0[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[2]),
        .Q(mmcm_lock_count_reg__0[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[3]),
        .Q(mmcm_lock_count_reg__0[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[4]),
        .Q(mmcm_lock_count_reg__0[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[5]),
        .Q(mmcm_lock_count_reg__0[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[6]),
        .Q(mmcm_lock_count_reg__0[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(SYSCLK_IN),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[7]),
        .Q(mmcm_lock_count_reg__0[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(sync_mmcm_lock_reclocked_n_1),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEF00FF10)) 
    pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(pll_reset_asserted_reg_n_0),
        .I4(tx_state[1]),
        .O(pll_reset_asserted_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(SOFT_RESET_TX_IN));
  LUT4 #(
    .INIT(16'hB833)) 
    reset_time_out_i_5
       (.I0(txresetdone_s3),
        .I1(tx_state[1]),
        .I2(mmcm_lock_reclocked),
        .I3(tx_state[2]),
        .O(reset_time_out_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(sync_QPLLLOCK_n_1),
        .Q(reset_time_out),
        .R(SOFT_RESET_TX_IN));
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(SOFT_RESET_TX_IN));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block sync_CPLLLOCK
       (.E(sync_CPLLLOCK_n_1),
        .\FSM_sequential_tx_state_reg[1] (\FSM_sequential_tx_state[3]_i_4_n_0 ),
        .SYSCLK_IN(SYSCLK_IN),
        .data_out(cplllock_sync),
        .data_sync_reg6_0(qplllock_sync),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_txsysclksel_in(gt0_txsysclksel_in),
        .init_wait_done_reg(init_wait_done_reg_n_0),
        .out(tx_state),
        .pll_reset_asserted_reg(pll_reset_asserted_reg_n_0),
        .reset_time_out_reg(\FSM_sequential_tx_state[3]_i_9_n_0 ),
        .reset_time_out_reg_0(\FSM_sequential_tx_state[3]_i_11_n_0 ),
        .time_out_2ms_reg(time_out_2ms_reg_n_0),
        .\wait_time_cnt_reg[6] (sel));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_0 sync_QPLLLOCK
       (.SYSCLK_IN(SYSCLK_IN),
        .data_out(qplllock_sync),
        .data_sync_reg6_0(cplllock_sync),
        .gt0_txsysclksel_in(gt0_txsysclksel_in),
        .init_wait_done_reg(init_wait_done_reg_n_0),
        .out(tx_state),
        .reset_time_out(reset_time_out),
        .reset_time_out_reg(sync_QPLLLOCK_n_1),
        .txresetdone_s3_reg(reset_time_out_i_5_n_0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_1 sync_TXRESETDONE
       (.SYSCLK_IN(SYSCLK_IN),
        .data_in(data_in),
        .data_out(txresetdone_s2));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_2 sync_mmcm_lock_reclocked
       (.Q(mmcm_lock_count_reg__0[7:6]),
        .SR(sync_mmcm_lock_reclocked_n_0),
        .SYSCLK_IN(SYSCLK_IN),
        .\mmcm_lock_count_reg[4] (\mmcm_lock_count[7]_i_4_n_0 ),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .mmcm_lock_reclocked_reg(sync_mmcm_lock_reclocked_n_1));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_3 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out),
        .gt0_txusrclk_in(gt0_txusrclk_in));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_4 sync_time_out_wait_bypass
       (.SYSCLK_IN(SYSCLK_IN),
        .data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_5 sync_tx_fsm_reset_done_int
       (.GT_TX_FSM_RESET_DONE_OUT(GT_TX_FSM_RESET_DONE_OUT),
        .data_out(tx_fsm_reset_done_int_s2),
        .gt0_txusrclk_in(gt0_txusrclk_in));
  LUT4 #(
    .INIT(16'h00AE)) 
    time_out_2ms_i_1__0
       (.I0(time_out_2ms_reg_n_0),
        .I1(time_out_2ms_i_2_n_0),
        .I2(\time_out_counter[0]_i_3__0_n_0 ),
        .I3(reset_time_out),
        .O(time_out_2ms_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    time_out_2ms_i_2
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[11]),
        .I4(time_out_counter_reg[4]),
        .I5(\time_out_counter[0]_i_4__0_n_0 ),
        .O(time_out_2ms_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_out_2ms_i_1__0_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAEAAA)) 
    time_out_500us_i_1
       (.I0(time_out_500us_reg_n_0),
        .I1(time_out_500us_i_2_n_0),
        .I2(time_out_counter_reg[4]),
        .I3(time_out_counter_reg[9]),
        .I4(\time_out_counter[0]_i_3__0_n_0 ),
        .I5(reset_time_out),
        .O(time_out_500us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    time_out_500us_i_2
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[11]),
        .I4(time_out_counter_reg[17]),
        .I5(time_out_counter_reg[16]),
        .O(time_out_500us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_500us_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_out_500us_i_1_n_0),
        .Q(time_out_500us_reg_n_0),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \time_out_counter[0]_i_10 
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[5]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[12]),
        .O(\time_out_counter[0]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \time_out_counter[0]_i_11 
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[2]),
        .O(\time_out_counter[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \time_out_counter[0]_i_1__0 
       (.I0(\time_out_counter[0]_i_3__0_n_0 ),
        .I1(time_out_counter_reg[11]),
        .I2(\time_out_counter[0]_i_4__0_n_0 ),
        .I3(time_out_counter_reg[9]),
        .I4(\time_out_counter[0]_i_5_n_0 ),
        .I5(time_out_counter_reg[4]),
        .O(time_out_counter));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \time_out_counter[0]_i_3__0 
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[13]),
        .I2(\time_out_counter[0]_i_10_n_0 ),
        .I3(\time_out_counter[0]_i_11_n_0 ),
        .O(\time_out_counter[0]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'hEF)) 
    \time_out_counter[0]_i_4__0 
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[10]),
        .O(\time_out_counter[0]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \time_out_counter[0]_i_5 
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(\time_out_counter[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[0]_i_6 
       (.I0(time_out_counter_reg[3]),
        .O(\time_out_counter[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[0]_i_7 
       (.I0(time_out_counter_reg[2]),
        .O(\time_out_counter[0]_i_7_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[0]_i_8__0 
       (.I0(time_out_counter_reg[1]),
        .O(\time_out_counter[0]_i_8__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_9 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_9_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[12]_i_2 
       (.I0(time_out_counter_reg[15]),
        .O(\time_out_counter[12]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[12]_i_3 
       (.I0(time_out_counter_reg[14]),
        .O(\time_out_counter[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[12]_i_4 
       (.I0(time_out_counter_reg[13]),
        .O(\time_out_counter[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[12]_i_5 
       (.I0(time_out_counter_reg[12]),
        .O(\time_out_counter[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[16]_i_2 
       (.I0(time_out_counter_reg[17]),
        .O(\time_out_counter[16]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[16]_i_3 
       (.I0(time_out_counter_reg[16]),
        .O(\time_out_counter[16]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[4]_i_2 
       (.I0(time_out_counter_reg[7]),
        .O(\time_out_counter[4]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[4]_i_3 
       (.I0(time_out_counter_reg[6]),
        .O(\time_out_counter[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[4]_i_4 
       (.I0(time_out_counter_reg[5]),
        .O(\time_out_counter[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[4]_i_5 
       (.I0(time_out_counter_reg[4]),
        .O(\time_out_counter[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[8]_i_2 
       (.I0(time_out_counter_reg[11]),
        .O(\time_out_counter[8]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[8]_i_3 
       (.I0(time_out_counter_reg[10]),
        .O(\time_out_counter[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[8]_i_4 
       (.I0(time_out_counter_reg[9]),
        .O(\time_out_counter[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \time_out_counter[8]_i_5 
       (.I0(time_out_counter_reg[8]),
        .O(\time_out_counter[8]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
  CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2_n_0 ,\time_out_counter_reg[0]_i_2_n_1 ,\time_out_counter_reg[0]_i_2_n_2 ,\time_out_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2_n_4 ,\time_out_counter_reg[0]_i_2_n_5 ,\time_out_counter_reg[0]_i_2_n_6 ,\time_out_counter_reg[0]_i_2_n_7 }),
        .S({\time_out_counter[0]_i_6_n_0 ,\time_out_counter[0]_i_7_n_0 ,\time_out_counter[0]_i_8__0_n_0 ,\time_out_counter[0]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
  CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\time_out_counter_reg[8]_i_1_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1_n_0 ,\time_out_counter_reg[12]_i_1_n_1 ,\time_out_counter_reg[12]_i_1_n_2 ,\time_out_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1_n_4 ,\time_out_counter_reg[12]_i_1_n_5 ,\time_out_counter_reg[12]_i_1_n_6 ,\time_out_counter_reg[12]_i_1_n_7 }),
        .S({\time_out_counter[12]_i_2_n_0 ,\time_out_counter[12]_i_3_n_0 ,\time_out_counter[12]_i_4_n_0 ,\time_out_counter[12]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
  CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\time_out_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:1],\time_out_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1_n_6 ,\time_out_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,\time_out_counter[16]_i_2_n_0 ,\time_out_counter[16]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
  CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\time_out_counter_reg[0]_i_2_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1_n_0 ,\time_out_counter_reg[4]_i_1_n_1 ,\time_out_counter_reg[4]_i_1_n_2 ,\time_out_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1_n_4 ,\time_out_counter_reg[4]_i_1_n_5 ,\time_out_counter_reg[4]_i_1_n_6 ,\time_out_counter_reg[4]_i_1_n_7 }),
        .S({\time_out_counter[4]_i_2_n_0 ,\time_out_counter[4]_i_3_n_0 ,\time_out_counter[4]_i_4_n_0 ,\time_out_counter[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
  CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\time_out_counter_reg[4]_i_1_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1_n_0 ,\time_out_counter_reg[8]_i_1_n_1 ,\time_out_counter_reg[8]_i_1_n_2 ,\time_out_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1_n_4 ,\time_out_counter_reg[8]_i_1_n_5 ,\time_out_counter_reg[8]_i_1_n_6 ,\time_out_counter_reg[8]_i_1_n_7 }),
        .S({\time_out_counter[8]_i_2_n_0 ,\time_out_counter[8]_i_3_n_0 ,\time_out_counter[8]_i_4_n_0 ,\time_out_counter[8]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(SYSCLK_IN),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(\wait_bypass_count[0]_i_4_n_0 ),
        .I2(tx_fsm_reset_done_int_s3),
        .I3(run_phase_alignment_int_s3),
        .O(time_out_wait_bypass_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AABAAAAA)) 
    time_tlock_max_i_1__0
       (.I0(time_tlock_max_reg_n_0),
        .I1(time_tlock_max_i_2_n_0),
        .I2(time_out_counter_reg[4]),
        .I3(\time_out_counter[0]_i_4__0_n_0 ),
        .I4(time_tlock_max_i_3_n_0),
        .I5(reset_time_out),
        .O(time_tlock_max_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_tlock_max_i_2
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[0]),
        .I3(time_out_counter_reg[1]),
        .I4(\time_out_counter[0]_i_10_n_0 ),
        .O(time_tlock_max_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_tlock_max_i_3
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[6]),
        .I4(time_out_counter_reg[17]),
        .I5(time_out_counter_reg[16]),
        .O(time_tlock_max_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(time_tlock_max_i_1__0_n_0),
        .Q(time_tlock_max_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF0008)) 
    tx_fsm_reset_done_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(GT_TX_FSM_RESET_DONE_OUT),
        .O(tx_fsm_reset_done_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_i_1_n_0),
        .Q(GT_TX_FSM_RESET_DONE_OUT),
        .R(SOFT_RESET_TX_IN));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_s3_reg
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txresetdone_s3_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(clear));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \wait_bypass_count[0]_i_10 
       (.I0(wait_bypass_count_reg[1]),
        .I1(wait_bypass_count_reg[0]),
        .I2(wait_bypass_count_reg[3]),
        .I3(wait_bypass_count_reg[2]),
        .O(\wait_bypass_count[0]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \wait_bypass_count[0]_i_11 
       (.I0(wait_bypass_count_reg[13]),
        .I1(wait_bypass_count_reg[12]),
        .I2(wait_bypass_count_reg[15]),
        .I3(wait_bypass_count_reg[14]),
        .O(\wait_bypass_count[0]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \wait_bypass_count[0]_i_12 
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[8]),
        .I2(wait_bypass_count_reg[11]),
        .I3(wait_bypass_count_reg[10]),
        .O(\wait_bypass_count[0]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2 
       (.I0(\wait_bypass_count[0]_i_4_n_0 ),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \wait_bypass_count[0]_i_4 
       (.I0(\wait_bypass_count[0]_i_9_n_0 ),
        .I1(\wait_bypass_count[0]_i_10_n_0 ),
        .I2(\wait_bypass_count[0]_i_11_n_0 ),
        .I3(\wait_bypass_count[0]_i_12_n_0 ),
        .O(\wait_bypass_count[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[0]_i_5 
       (.I0(wait_bypass_count_reg[3]),
        .O(\wait_bypass_count[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[0]_i_6 
       (.I0(wait_bypass_count_reg[2]),
        .O(\wait_bypass_count[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[0]_i_7 
       (.I0(wait_bypass_count_reg[1]),
        .O(\wait_bypass_count[0]_i_7_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_8 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \wait_bypass_count[0]_i_9 
       (.I0(wait_bypass_count_reg[5]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[7]),
        .I3(wait_bypass_count_reg[6]),
        .O(\wait_bypass_count[0]_i_9_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[12]_i_2 
       (.I0(wait_bypass_count_reg[15]),
        .O(\wait_bypass_count[12]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[12]_i_3 
       (.I0(wait_bypass_count_reg[14]),
        .O(\wait_bypass_count[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[12]_i_4 
       (.I0(wait_bypass_count_reg[13]),
        .O(\wait_bypass_count[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[12]_i_5 
       (.I0(wait_bypass_count_reg[12]),
        .O(\wait_bypass_count[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[4]_i_2 
       (.I0(wait_bypass_count_reg[7]),
        .O(\wait_bypass_count[4]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[4]_i_3 
       (.I0(wait_bypass_count_reg[6]),
        .O(\wait_bypass_count[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[4]_i_4 
       (.I0(wait_bypass_count_reg[5]),
        .O(\wait_bypass_count[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[4]_i_5 
       (.I0(wait_bypass_count_reg[4]),
        .O(\wait_bypass_count[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[8]_i_2 
       (.I0(wait_bypass_count_reg[11]),
        .O(\wait_bypass_count[8]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[8]_i_3 
       (.I0(wait_bypass_count_reg[10]),
        .O(\wait_bypass_count[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[8]_i_4 
       (.I0(wait_bypass_count_reg[9]),
        .O(\wait_bypass_count[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \wait_bypass_count[8]_i_5 
       (.I0(wait_bypass_count_reg[8]),
        .O(\wait_bypass_count[8]_i_5_n_0 ));
  FDRE \wait_bypass_count_reg[0] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(clear));
  CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3_n_0 ,\wait_bypass_count_reg[0]_i_3_n_1 ,\wait_bypass_count_reg[0]_i_3_n_2 ,\wait_bypass_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3_n_4 ,\wait_bypass_count_reg[0]_i_3_n_5 ,\wait_bypass_count_reg[0]_i_3_n_6 ,\wait_bypass_count_reg[0]_i_3_n_7 }),
        .S({\wait_bypass_count[0]_i_5_n_0 ,\wait_bypass_count[0]_i_6_n_0 ,\wait_bypass_count[0]_i_7_n_0 ,\wait_bypass_count[0]_i_8_n_0 }));
  FDRE \wait_bypass_count_reg[10] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(clear));
  FDRE \wait_bypass_count_reg[11] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(clear));
  FDRE \wait_bypass_count_reg[12] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(clear));
  CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\wait_bypass_count_reg[8]_i_1_n_0 ),
        .CO({\NLW_wait_bypass_count_reg[12]_i_1_CO_UNCONNECTED [3],\wait_bypass_count_reg[12]_i_1_n_1 ,\wait_bypass_count_reg[12]_i_1_n_2 ,\wait_bypass_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[12]_i_1_n_4 ,\wait_bypass_count_reg[12]_i_1_n_5 ,\wait_bypass_count_reg[12]_i_1_n_6 ,\wait_bypass_count_reg[12]_i_1_n_7 }),
        .S({\wait_bypass_count[12]_i_2_n_0 ,\wait_bypass_count[12]_i_3_n_0 ,\wait_bypass_count[12]_i_4_n_0 ,\wait_bypass_count[12]_i_5_n_0 }));
  FDRE \wait_bypass_count_reg[13] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[13]),
        .R(clear));
  FDRE \wait_bypass_count_reg[14] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[14]),
        .R(clear));
  FDRE \wait_bypass_count_reg[15] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[15]),
        .R(clear));
  FDRE \wait_bypass_count_reg[1] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(clear));
  FDRE \wait_bypass_count_reg[2] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(clear));
  FDRE \wait_bypass_count_reg[3] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(clear));
  FDRE \wait_bypass_count_reg[4] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(clear));
  CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\wait_bypass_count_reg[0]_i_3_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1_n_0 ,\wait_bypass_count_reg[4]_i_1_n_1 ,\wait_bypass_count_reg[4]_i_1_n_2 ,\wait_bypass_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1_n_4 ,\wait_bypass_count_reg[4]_i_1_n_5 ,\wait_bypass_count_reg[4]_i_1_n_6 ,\wait_bypass_count_reg[4]_i_1_n_7 }),
        .S({\wait_bypass_count[4]_i_2_n_0 ,\wait_bypass_count[4]_i_3_n_0 ,\wait_bypass_count[4]_i_4_n_0 ,\wait_bypass_count[4]_i_5_n_0 }));
  FDRE \wait_bypass_count_reg[5] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(clear));
  FDRE \wait_bypass_count_reg[6] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(clear));
  FDRE \wait_bypass_count_reg[7] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(clear));
  FDRE \wait_bypass_count_reg[8] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(clear));
  CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\wait_bypass_count_reg[4]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1_n_0 ,\wait_bypass_count_reg[8]_i_1_n_1 ,\wait_bypass_count_reg[8]_i_1_n_2 ,\wait_bypass_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1_n_4 ,\wait_bypass_count_reg[8]_i_1_n_5 ,\wait_bypass_count_reg[8]_i_1_n_6 ,\wait_bypass_count_reg[8]_i_1_n_7 }),
        .S({\wait_bypass_count[8]_i_2_n_0 ,\wait_bypass_count[8]_i_3_n_0 ,\wait_bypass_count[8]_i_4_n_0 ,\wait_bypass_count[8]_i_5_n_0 }));
  FDRE \wait_bypass_count_reg[9] 
       (.C(gt0_txusrclk_in),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg__0[0]),
        .O(wait_time_cnt0[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg__0[1]),
        .I1(wait_time_cnt_reg__0[0]),
        .O(wait_time_cnt0[1]));
  LUT3 #(
    .INIT(8'hA9)) 
    \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg__0[2]),
        .I1(wait_time_cnt_reg__0[0]),
        .I2(wait_time_cnt_reg__0[1]),
        .O(wait_time_cnt0[2]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg__0[3]),
        .I1(wait_time_cnt_reg__0[1]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[2]),
        .O(wait_time_cnt0[3]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg__0[4]),
        .I1(wait_time_cnt_reg__0[2]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[1]),
        .I4(wait_time_cnt_reg__0[3]),
        .O(wait_time_cnt0[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg__0[5]),
        .I1(wait_time_cnt_reg__0[3]),
        .I2(wait_time_cnt_reg__0[1]),
        .I3(wait_time_cnt_reg__0[0]),
        .I4(wait_time_cnt_reg__0[2]),
        .I5(wait_time_cnt_reg__0[4]),
        .O(wait_time_cnt0[5]));
  LUT4 #(
    .INIT(16'h004C)) 
    \wait_time_cnt[6]_i_1 
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[1]),
        .I3(tx_state[3]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg__0[6]),
        .O(sel));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3 
       (.I0(wait_time_cnt_reg__0[6]),
        .I1(\wait_time_cnt[6]_i_4_n_0 ),
        .O(wait_time_cnt0[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg__0[4]),
        .I1(wait_time_cnt_reg__0[2]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[1]),
        .I4(wait_time_cnt_reg__0[3]),
        .I5(wait_time_cnt_reg__0[5]),
        .O(\wait_time_cnt[6]_i_4_n_0 ));
  FDRE \wait_time_cnt_reg[0] 
       (.C(SYSCLK_IN),
        .CE(sel),
        .D(wait_time_cnt0[0]),
        .Q(wait_time_cnt_reg__0[0]),
        .R(wait_time_cnt0_0));
  FDRE \wait_time_cnt_reg[1] 
       (.C(SYSCLK_IN),
        .CE(sel),
        .D(wait_time_cnt0[1]),
        .Q(wait_time_cnt_reg__0[1]),
        .R(wait_time_cnt0_0));
  FDSE \wait_time_cnt_reg[2] 
       (.C(SYSCLK_IN),
        .CE(sel),
        .D(wait_time_cnt0[2]),
        .Q(wait_time_cnt_reg__0[2]),
        .S(wait_time_cnt0_0));
  FDRE \wait_time_cnt_reg[3] 
       (.C(SYSCLK_IN),
        .CE(sel),
        .D(wait_time_cnt0[3]),
        .Q(wait_time_cnt_reg__0[3]),
        .R(wait_time_cnt0_0));
  FDRE \wait_time_cnt_reg[4] 
       (.C(SYSCLK_IN),
        .CE(sel),
        .D(wait_time_cnt0[4]),
        .Q(wait_time_cnt_reg__0[4]),
        .R(wait_time_cnt0_0));
  FDSE \wait_time_cnt_reg[5] 
       (.C(SYSCLK_IN),
        .CE(sel),
        .D(wait_time_cnt0[5]),
        .Q(wait_time_cnt_reg__0[5]),
        .S(wait_time_cnt0_0));
  FDSE \wait_time_cnt_reg[6] 
       (.C(SYSCLK_IN),
        .CE(sel),
        .D(wait_time_cnt0[6]),
        .Q(wait_time_cnt_reg__0[6]),
        .S(wait_time_cnt0_0));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_common_wrapper
   (common0_qpll_clk_in,
    common0_qpll_refclk_in,
    cpll_refclk);
  output common0_qpll_clk_in;
  output common0_qpll_refclk_in;
  input cpll_refclk;

  wire common0_qpll_clk_in;
  wire common0_qpll_refclk_in;
  wire cpll_refclk;

  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gtwizard_0_common jesd204_0_common
       (.common0_qpll_clk_in(common0_qpll_clk_in),
        .common0_qpll_refclk_in(common0_qpll_refclk_in),
        .cpll_refclk(cpll_refclk));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_cpll_railing
   (cpllreset_in,
    cpllpd_in,
    gt0_gtrefclk0_in,
    p_5_in,
    gt0_cpllreset_in,
    p_4_in,
    gt0_cpllpd_in);
  output cpllreset_in;
  output cpllpd_in;
  input gt0_gtrefclk0_in;
  input p_5_in;
  input gt0_cpllreset_in;
  input p_4_in;
  input gt0_cpllpd_in;

  wire cpll_pd_out;
  wire cpll_reset_out;
  wire cpllpd_in;
  wire \cpllpd_wait_reg[31]_srl32_n_1 ;
  wire \cpllpd_wait_reg[63]_srl32_n_1 ;
  wire \cpllpd_wait_reg[94]_srl31_n_0 ;
  wire cpllreset_in;
  wire \cpllreset_wait_reg[126]_srl31_n_0 ;
  wire \cpllreset_wait_reg[31]_srl32_n_1 ;
  wire \cpllreset_wait_reg[63]_srl32_n_1 ;
  wire \cpllreset_wait_reg[95]_srl32_n_1 ;
  wire gt0_cpllpd_in;
  wire gt0_cpllreset_in;
  wire gt0_gtrefclk0_in;
  wire p_4_in;
  wire p_5_in;
  wire \use_bufh_cpll.refclk_buf_n_0 ;
  wire \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ;

  (* srl_bus_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(\use_bufh_cpll.refclk_buf_n_0 ),
        .D(1'b0),
        .Q(\NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(\use_bufh_cpll.refclk_buf_n_0 ),
        .D(\cpllpd_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h7FFFFFFF)) 
    \cpllpd_wait_reg[94]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(\use_bufh_cpll.refclk_buf_n_0 ),
        .D(\cpllpd_wait_reg[63]_srl32_n_1 ),
        .Q(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q31(\NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \cpllpd_wait_reg[95] 
       (.C(\use_bufh_cpll.refclk_buf_n_0 ),
        .CE(1'b1),
        .D(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q(cpll_pd_out),
        .R(1'b0));
  (* srl_bus_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[126]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(\use_bufh_cpll.refclk_buf_n_0 ),
        .D(\cpllreset_wait_reg[95]_srl32_n_1 ),
        .Q(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q31(\NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \cpllreset_wait_reg[127] 
       (.C(\use_bufh_cpll.refclk_buf_n_0 ),
        .CE(1'b1),
        .D(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q(cpll_reset_out),
        .R(1'b0));
  (* srl_bus_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h000000FF)) 
    \cpllreset_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(\use_bufh_cpll.refclk_buf_n_0 ),
        .D(1'b0),
        .Q(\NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(\use_bufh_cpll.refclk_buf_n_0 ),
        .D(\cpllreset_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "inst/\jesd204_phy_block_i/Jesd204_microblaze_jesd204_phy_0_0_gt /U0/\Jesd204_microblaze_jesd204_phy_0_0_gt_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[95]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(\use_bufh_cpll.refclk_buf_n_0 ),
        .D(\cpllreset_wait_reg[63]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[95]_srl32_n_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gthe2_i_i_1
       (.I0(cpll_pd_out),
        .I1(gt0_cpllpd_in),
        .O(cpllpd_in));
  LUT4 #(
    .INIT(16'hFFFE)) 
    gthe2_i_i_2
       (.I0(cpll_reset_out),
        .I1(p_5_in),
        .I2(gt0_cpllreset_in),
        .I3(p_4_in),
        .O(cpllreset_in));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFH \use_bufh_cpll.refclk_buf 
       (.I(gt0_gtrefclk0_in),
        .O(\use_bufh_cpll.refclk_buf_n_0 ));
endmodule

(* DowngradeIPIdentifiedWarnings = "yes" *) (* EXAMPLE_SIMULATION = "0" *) (* EXAMPLE_SIM_GTRESET_SPEEDUP = "TRUE" *) 
(* EXAMPLE_USE_CHIPSCOPE = "1" *) (* STABLE_CLOCK_PERIOD = "10" *) (* USE_BUFG = "0" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_init
   (SYSCLK_IN,
    SOFT_RESET_TX_IN,
    SOFT_RESET_RX_IN,
    DONT_RESET_ON_DATA_ERROR_IN,
    GT_TX_FSM_RESET_DONE_OUT,
    GT_RX_FSM_RESET_DONE_OUT,
    GT0_DATA_VALID_IN,
    gt0_cpllfbclklost_out,
    gt0_cplllock_out,
    gt0_cplllockdetclk_in,
    gt0_cpllpd_in,
    gt0_cpllreset_in,
    gt0_gtnorthrefclk0_in,
    gt0_gtnorthrefclk1_in,
    gt0_gtrefclk0_in,
    gt0_gtrefclk1_in,
    gt0_gtsouthrefclk0_in,
    gt0_gtsouthrefclk1_in,
    gt0_drpaddr_in,
    gt0_drpclk_in,
    gt0_drpdi_in,
    gt0_drpdo_out,
    gt0_drpen_in,
    gt0_drprdy_out,
    gt0_drpwe_in,
    gt0_rxsysclksel_in,
    gt0_txsysclksel_in,
    gt0_loopback_in,
    gt0_rxpd_in,
    gt0_txpd_in,
    gt0_eyescanreset_in,
    gt0_rxuserrdy_in,
    gt0_eyescandataerror_out,
    gt0_eyescantrigger_in,
    gt0_rxcdrhold_in,
    gt0_dmonitorout_out,
    gt0_rxusrclk_in,
    gt0_rxusrclk2_in,
    gt0_rxdata_out,
    gt0_rxprbserr_out,
    gt0_rxprbssel_in,
    gt0_rxprbscntreset_in,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    gt0_gthrxn_in,
    gt0_rxbufreset_in,
    gt0_rxbufstatus_out,
    gt0_rxstatus_out,
    gt0_rxbyteisaligned_out,
    gt0_rxbyterealign_out,
    gt0_rxcommadet_out,
    gt0_rxmcommaalignen_in,
    gt0_rxpcommaalignen_in,
    gt0_rxdfelpmreset_in,
    gt0_rxmonitorout_out,
    gt0_rxmonitorsel_in,
    gt0_rxoutclk_out,
    gt0_rxoutclkfabric_out,
    gt0_gtrxreset_in,
    gt0_rxpcsreset_in,
    gt0_rxpmareset_in,
    gt0_rxlpmen_in,
    gt0_rxpolarity_in,
    gt0_rxchariscomma_out,
    gt0_rxcharisk_out,
    gt0_gthrxp_in,
    gt0_rxresetdone_out,
    gt0_txpostcursor_in,
    gt0_txprecursor_in,
    gt0_gttxreset_in,
    gt0_txuserrdy_in,
    gt0_txusrclk_in,
    gt0_txusrclk2_in,
    gt0_txprbsforceerr_in,
    gt0_txbufstatus_out,
    gt0_txdiffctrl_in,
    gt0_txinhibit_in,
    gt0_txdata_in,
    gt0_gthtxn_out,
    gt0_gthtxp_out,
    gt0_txoutclk_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txpcsreset_in,
    gt0_txpmareset_in,
    gt0_txresetdone_out,
    gt0_txpolarity_in,
    gt0_txprbssel_in,
    gt0_txcharisk_in,
    GT0_QPLLOUTCLK_IN,
    GT0_QPLLOUTREFCLK_IN);
  input SYSCLK_IN;
  input SOFT_RESET_TX_IN;
  input SOFT_RESET_RX_IN;
  input DONT_RESET_ON_DATA_ERROR_IN;
  output GT_TX_FSM_RESET_DONE_OUT;
  output GT_RX_FSM_RESET_DONE_OUT;
  input GT0_DATA_VALID_IN;
  output gt0_cpllfbclklost_out;
  output gt0_cplllock_out;
  input gt0_cplllockdetclk_in;
  input gt0_cpllpd_in;
  input gt0_cpllreset_in;
  input gt0_gtnorthrefclk0_in;
  input gt0_gtnorthrefclk1_in;
  input gt0_gtrefclk0_in;
  input gt0_gtrefclk1_in;
  input gt0_gtsouthrefclk0_in;
  input gt0_gtsouthrefclk1_in;
  input [8:0]gt0_drpaddr_in;
  input gt0_drpclk_in;
  input [15:0]gt0_drpdi_in;
  output [15:0]gt0_drpdo_out;
  input gt0_drpen_in;
  output gt0_drprdy_out;
  input gt0_drpwe_in;
  input [1:0]gt0_rxsysclksel_in;
  input [1:0]gt0_txsysclksel_in;
  input [2:0]gt0_loopback_in;
  input [1:0]gt0_rxpd_in;
  input [1:0]gt0_txpd_in;
  input gt0_eyescanreset_in;
  input gt0_rxuserrdy_in;
  output gt0_eyescandataerror_out;
  input gt0_eyescantrigger_in;
  input gt0_rxcdrhold_in;
  output [14:0]gt0_dmonitorout_out;
  input gt0_rxusrclk_in;
  input gt0_rxusrclk2_in;
  output [31:0]gt0_rxdata_out;
  output gt0_rxprbserr_out;
  input [2:0]gt0_rxprbssel_in;
  input gt0_rxprbscntreset_in;
  output [3:0]gt0_rxdisperr_out;
  output [3:0]gt0_rxnotintable_out;
  input gt0_gthrxn_in;
  input gt0_rxbufreset_in;
  output [2:0]gt0_rxbufstatus_out;
  output [2:0]gt0_rxstatus_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  input gt0_rxmcommaalignen_in;
  input gt0_rxpcommaalignen_in;
  input gt0_rxdfelpmreset_in;
  output [6:0]gt0_rxmonitorout_out;
  input [1:0]gt0_rxmonitorsel_in;
  output gt0_rxoutclk_out;
  output gt0_rxoutclkfabric_out;
  input gt0_gtrxreset_in;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  input gt0_rxlpmen_in;
  input gt0_rxpolarity_in;
  output [3:0]gt0_rxchariscomma_out;
  output [3:0]gt0_rxcharisk_out;
  input gt0_gthrxp_in;
  output gt0_rxresetdone_out;
  input [4:0]gt0_txpostcursor_in;
  input [4:0]gt0_txprecursor_in;
  input gt0_gttxreset_in;
  input gt0_txuserrdy_in;
  input gt0_txusrclk_in;
  input gt0_txusrclk2_in;
  input gt0_txprbsforceerr_in;
  output [1:0]gt0_txbufstatus_out;
  input [3:0]gt0_txdiffctrl_in;
  input gt0_txinhibit_in;
  input [31:0]gt0_txdata_in;
  output gt0_gthtxn_out;
  output gt0_gthtxp_out;
  output gt0_txoutclk_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  output gt0_txresetdone_out;
  input gt0_txpolarity_in;
  input [2:0]gt0_txprbssel_in;
  input [3:0]gt0_txcharisk_in;
  input GT0_QPLLOUTCLK_IN;
  input GT0_QPLLOUTREFCLK_IN;

  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire GT0_DATA_VALID_IN;
  wire GT0_QPLLOUTCLK_IN;
  wire GT0_QPLLOUTREFCLK_IN;
  wire GT_RX_FSM_RESET_DONE_OUT;
  wire GT_TX_FSM_RESET_DONE_OUT;
  wire RXRESETDONE;
  wire SOFT_RESET_RX_IN;
  wire SOFT_RESET_TX_IN;
  wire SYSCLK_IN;
  wire TXRESETDONE;
  wire gt0_cpllfbclklost_out;
  wire gt0_cplllock_out;
  wire gt0_cplllockdetclk_in;
  wire gt0_cpllpd_in;
  wire gt0_cpllreset_in;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire gt0_drpclk_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gthrxn_in;
  wire gt0_gthrxp_in;
  wire gt0_gthtxn_out;
  wire gt0_gthtxp_out;
  wire gt0_gtnorthrefclk0_in;
  wire gt0_gtnorthrefclk1_in;
  wire gt0_gtrefclk0_in;
  wire gt0_gtrefclk1_in;
  wire gt0_gtrxreset_in;
  wire gt0_gtrxreset_in2_out;
  wire gt0_gtsouthrefclk0_in;
  wire gt0_gtsouthrefclk1_in;
  wire gt0_gttxreset_in;
  wire gt0_gttxreset_in1_out;
  wire [2:0]gt0_loopback_in;
  wire gt0_rx_cdrlock_counter;
  wire \gt0_rx_cdrlock_counter[10]_i_3_n_0 ;
  wire \gt0_rx_cdrlock_counter[10]_i_4_n_0 ;
  wire [10:0]gt0_rx_cdrlock_counter_reg__0;
  wire gt0_rx_cdrlocked_i_2_n_0;
  wire gt0_rx_cdrlocked_i_3_n_0;
  wire gt0_rx_cdrlocked_i_4_n_0;
  wire gt0_rx_cdrlocked_reg_n_0;
  wire gt0_rxbufreset_in;
  wire [2:0]gt0_rxbufstatus_out;
  wire gt0_rxbyteisaligned_out;
  wire gt0_rxbyterealign_out;
  wire gt0_rxcdrhold_in;
  wire [3:0]gt0_rxchariscomma_out;
  wire [3:0]gt0_rxcharisk_out;
  wire gt0_rxcommadet_out;
  wire [31:0]gt0_rxdata_out;
  wire gt0_rxdfelpmreset_in;
  wire [3:0]gt0_rxdisperr_out;
  wire gt0_rxlpmen_in;
  wire gt0_rxmcommaalignen_in;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire [3:0]gt0_rxnotintable_out;
  wire gt0_rxoutclk_out;
  wire gt0_rxoutclkfabric_out;
  wire gt0_rxpcommaalignen_in;
  wire gt0_rxpcsreset_in;
  wire [1:0]gt0_rxpd_in;
  wire gt0_rxpmareset_in;
  wire gt0_rxpolarity_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_rxsysclksel_in;
  wire gt0_rxuserrdy_in;
  wire gt0_rxuserrdy_in3_out;
  wire gt0_rxusrclk2_in;
  wire gt0_rxusrclk_in;
  wire [1:0]gt0_txbufstatus_out;
  wire [3:0]gt0_txcharisk_in;
  wire [31:0]gt0_txdata_in;
  wire [3:0]gt0_txdiffctrl_in;
  wire gt0_txinhibit_in;
  wire gt0_txoutclk_out;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txpcsreset_in;
  wire [1:0]gt0_txpd_in;
  wire gt0_txpmareset_in;
  wire gt0_txpolarity_in;
  wire [4:0]gt0_txpostcursor_in;
  wire gt0_txprbsforceerr_in;
  wire [2:0]gt0_txprbssel_in;
  wire [4:0]gt0_txprecursor_in;
  wire gt0_txresetdone_out;
  wire [1:0]gt0_txsysclksel_in;
  wire gt0_txuserrdy_in;
  wire gt0_txuserrdy_in0_out;
  wire gt0_txusrclk2_in;
  wire gt0_txusrclk_in;
  wire gt_rxresetfsm_i_n_4;
  wire [10:0]p_0_in;
  wire p_4_in;
  wire p_5_in;

  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_multi_gt Jesd204_microblaze_jesd204_phy_0_0_gt_i
       (.GT0_QPLLOUTCLK_IN(GT0_QPLLOUTCLK_IN),
        .GT0_QPLLOUTREFCLK_IN(GT0_QPLLOUTREFCLK_IN),
        .SR(gt0_gtrxreset_in2_out),
        .data_in(TXRESETDONE),
        .data_sync_reg1(RXRESETDONE),
        .gt0_cpllfbclklost_out(gt0_cpllfbclklost_out),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_cplllockdetclk_in(gt0_cplllockdetclk_in),
        .gt0_cpllpd_in(gt0_cpllpd_in),
        .gt0_cpllreset_in(gt0_cpllreset_in),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr_in(gt0_drpaddr_in),
        .gt0_drpclk_in(gt0_drpclk_in),
        .gt0_drpdi_in(gt0_drpdi_in),
        .gt0_drpdo_out(gt0_drpdo_out),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drprdy_out(gt0_drprdy_out),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_gthrxn_in(gt0_gthrxn_in),
        .gt0_gthrxp_in(gt0_gthrxp_in),
        .gt0_gthtxn_out(gt0_gthtxn_out),
        .gt0_gthtxp_out(gt0_gthtxp_out),
        .gt0_gtnorthrefclk0_in(gt0_gtnorthrefclk0_in),
        .gt0_gtnorthrefclk1_in(gt0_gtnorthrefclk1_in),
        .gt0_gtrefclk0_in(gt0_gtrefclk0_in),
        .gt0_gtrefclk1_in(gt0_gtrefclk1_in),
        .gt0_gtsouthrefclk0_in(gt0_gtsouthrefclk0_in),
        .gt0_gtsouthrefclk1_in(gt0_gtsouthrefclk1_in),
        .gt0_gttxreset_in1_out(gt0_gttxreset_in1_out),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxbufreset_in(gt0_rxbufreset_in),
        .gt0_rxbufstatus_out(gt0_rxbufstatus_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxbyterealign_out(gt0_rxbyterealign_out),
        .gt0_rxcdrhold_in(gt0_rxcdrhold_in),
        .gt0_rxchariscomma_out(gt0_rxchariscomma_out),
        .gt0_rxcharisk_out(gt0_rxcharisk_out),
        .gt0_rxcommadet_out(gt0_rxcommadet_out),
        .gt0_rxdata_out(gt0_rxdata_out),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxdisperr_out(gt0_rxdisperr_out),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxmcommaalignen_in(gt0_rxmcommaalignen_in),
        .gt0_rxmonitorout_out(gt0_rxmonitorout_out),
        .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),
        .gt0_rxnotintable_out(gt0_rxnotintable_out),
        .gt0_rxoutclk_out(gt0_rxoutclk_out),
        .gt0_rxoutclkfabric_out(gt0_rxoutclkfabric_out),
        .gt0_rxpcommaalignen_in(gt0_rxpcommaalignen_in),
        .gt0_rxpcsreset_in(gt0_rxpcsreset_in),
        .gt0_rxpd_in(gt0_rxpd_in),
        .gt0_rxpmareset_in(gt0_rxpmareset_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),
        .gt0_rxprbserr_out(gt0_rxprbserr_out),
        .gt0_rxprbssel_in(gt0_rxprbssel_in),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_in),
        .gt0_rxuserrdy_in3_out(gt0_rxuserrdy_in3_out),
        .gt0_rxusrclk2_in(gt0_rxusrclk2_in),
        .gt0_rxusrclk_in(gt0_rxusrclk_in),
        .gt0_txbufstatus_out(gt0_txbufstatus_out),
        .gt0_txcharisk_in(gt0_txcharisk_in),
        .gt0_txdata_in(gt0_txdata_in),
        .gt0_txdiffctrl_in(gt0_txdiffctrl_in),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txoutclk_out(gt0_txoutclk_out),
        .gt0_txoutclkfabric_out(gt0_txoutclkfabric_out),
        .gt0_txoutclkpcs_out(gt0_txoutclkpcs_out),
        .gt0_txpcsreset_in(gt0_txpcsreset_in),
        .gt0_txpd_in(gt0_txpd_in),
        .gt0_txpmareset_in(gt0_txpmareset_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .gt0_txpostcursor_in(gt0_txpostcursor_in),
        .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),
        .gt0_txprbssel_in(gt0_txprbssel_in),
        .gt0_txprecursor_in(gt0_txprecursor_in),
        .gt0_txresetdone_out(gt0_txresetdone_out),
        .gt0_txsysclksel_in(gt0_txsysclksel_in),
        .gt0_txuserrdy_in0_out(gt0_txuserrdy_in0_out),
        .gt0_txusrclk2_in(gt0_txusrclk2_in),
        .gt0_txusrclk_in(gt0_txusrclk_in),
        .p_4_in(p_4_in),
        .p_5_in(p_5_in));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[0]),
        .O(p_0_in[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[3]),
        .I1(gt0_rx_cdrlock_counter_reg__0[4]),
        .I2(\gt0_rx_cdrlock_counter[10]_i_3_n_0 ),
        .I3(gt0_rx_cdrlock_counter_reg__0[2]),
        .I4(gt0_rx_cdrlock_counter_reg__0[0]),
        .I5(gt0_rx_cdrlock_counter_reg__0[1]),
        .O(gt0_rx_cdrlock_counter));
  LUT6 #(
    .INIT(64'hF7FFFFFF08000000)) 
    \gt0_rx_cdrlock_counter[10]_i_2 
       (.I0(gt0_rx_cdrlock_counter_reg__0[9]),
        .I1(gt0_rx_cdrlock_counter_reg__0[7]),
        .I2(\gt0_rx_cdrlock_counter[10]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter_reg__0[6]),
        .I4(gt0_rx_cdrlock_counter_reg__0[8]),
        .I5(gt0_rx_cdrlock_counter_reg__0[10]),
        .O(p_0_in[10]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF7F)) 
    \gt0_rx_cdrlock_counter[10]_i_3 
       (.I0(gt0_rx_cdrlock_counter_reg__0[9]),
        .I1(gt0_rx_cdrlock_counter_reg__0[10]),
        .I2(gt0_rx_cdrlock_counter_reg__0[7]),
        .I3(gt0_rx_cdrlock_counter_reg__0[8]),
        .I4(gt0_rx_cdrlock_counter_reg__0[6]),
        .I5(gt0_rx_cdrlock_counter_reg__0[5]),
        .O(\gt0_rx_cdrlock_counter[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \gt0_rx_cdrlock_counter[10]_i_4 
       (.I0(gt0_rx_cdrlock_counter_reg__0[4]),
        .I1(gt0_rx_cdrlock_counter_reg__0[2]),
        .I2(gt0_rx_cdrlock_counter_reg__0[0]),
        .I3(gt0_rx_cdrlock_counter_reg__0[1]),
        .I4(gt0_rx_cdrlock_counter_reg__0[3]),
        .I5(gt0_rx_cdrlock_counter_reg__0[5]),
        .O(\gt0_rx_cdrlock_counter[10]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[1]),
        .I1(gt0_rx_cdrlock_counter_reg__0[0]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[1]),
        .I1(gt0_rx_cdrlock_counter_reg__0[0]),
        .I2(gt0_rx_cdrlock_counter_reg__0[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[2]),
        .I1(gt0_rx_cdrlock_counter_reg__0[0]),
        .I2(gt0_rx_cdrlock_counter_reg__0[1]),
        .I3(gt0_rx_cdrlock_counter_reg__0[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[3]),
        .I1(gt0_rx_cdrlock_counter_reg__0[1]),
        .I2(gt0_rx_cdrlock_counter_reg__0[0]),
        .I3(gt0_rx_cdrlock_counter_reg__0[2]),
        .I4(gt0_rx_cdrlock_counter_reg__0[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[4]),
        .I1(gt0_rx_cdrlock_counter_reg__0[2]),
        .I2(gt0_rx_cdrlock_counter_reg__0[0]),
        .I3(gt0_rx_cdrlock_counter_reg__0[1]),
        .I4(gt0_rx_cdrlock_counter_reg__0[3]),
        .I5(gt0_rx_cdrlock_counter_reg__0[5]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[10]_i_4_n_0 ),
        .I1(gt0_rx_cdrlock_counter_reg__0[6]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[6]),
        .I1(\gt0_rx_cdrlock_counter[10]_i_4_n_0 ),
        .I2(gt0_rx_cdrlock_counter_reg__0[7]),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[7]),
        .I1(\gt0_rx_cdrlock_counter[10]_i_4_n_0 ),
        .I2(gt0_rx_cdrlock_counter_reg__0[6]),
        .I3(gt0_rx_cdrlock_counter_reg__0[8]),
        .O(p_0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(gt0_rx_cdrlock_counter_reg__0[8]),
        .I1(gt0_rx_cdrlock_counter_reg__0[6]),
        .I2(\gt0_rx_cdrlock_counter[10]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter_reg__0[7]),
        .I4(gt0_rx_cdrlock_counter_reg__0[9]),
        .O(p_0_in[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[0] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[0]),
        .Q(gt0_rx_cdrlock_counter_reg__0[0]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[10] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[10]),
        .Q(gt0_rx_cdrlock_counter_reg__0[10]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[1] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[1]),
        .Q(gt0_rx_cdrlock_counter_reg__0[1]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[2] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[2]),
        .Q(gt0_rx_cdrlock_counter_reg__0[2]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[3] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[3]),
        .Q(gt0_rx_cdrlock_counter_reg__0[3]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[4] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[4]),
        .Q(gt0_rx_cdrlock_counter_reg__0[4]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[5] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[5]),
        .Q(gt0_rx_cdrlock_counter_reg__0[5]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[6] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[6]),
        .Q(gt0_rx_cdrlock_counter_reg__0[6]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[7] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[7]),
        .Q(gt0_rx_cdrlock_counter_reg__0[7]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[8] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[8]),
        .Q(gt0_rx_cdrlock_counter_reg__0[8]),
        .R(gt0_gtrxreset_in2_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[9] 
       (.C(SYSCLK_IN),
        .CE(gt0_rx_cdrlock_counter),
        .D(p_0_in[9]),
        .Q(gt0_rx_cdrlock_counter_reg__0[9]),
        .R(gt0_gtrxreset_in2_out));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'hB)) 
    gt0_rx_cdrlocked_i_2
       (.I0(gt0_rx_cdrlock_counter_reg__0[0]),
        .I1(gt0_rx_cdrlock_counter_reg__0[1]),
        .O(gt0_rx_cdrlocked_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    gt0_rx_cdrlocked_i_3
       (.I0(gt0_rx_cdrlock_counter_reg__0[3]),
        .I1(gt0_rx_cdrlock_counter_reg__0[4]),
        .O(gt0_rx_cdrlocked_i_3_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    gt0_rx_cdrlocked_i_4
       (.I0(gt0_rx_cdrlock_counter_reg__0[7]),
        .I1(gt0_rx_cdrlock_counter_reg__0[8]),
        .I2(gt0_rx_cdrlock_counter_reg__0[5]),
        .I3(gt0_rx_cdrlock_counter_reg__0[6]),
        .I4(gt0_rx_cdrlock_counter_reg__0[10]),
        .I5(gt0_rx_cdrlock_counter_reg__0[9]),
        .O(gt0_rx_cdrlocked_i_4_n_0));
  FDRE gt0_rx_cdrlocked_reg
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(gt_rxresetfsm_i_n_4),
        .Q(gt0_rx_cdrlocked_reg_n_0),
        .R(1'b0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_RX_STARTUP_FSM gt_rxresetfsm_i
       (.DONT_RESET_ON_DATA_ERROR_IN(DONT_RESET_ON_DATA_ERROR_IN),
        .GT0_DATA_VALID_IN(GT0_DATA_VALID_IN),
        .GT_RX_FSM_RESET_DONE_OUT(GT_RX_FSM_RESET_DONE_OUT),
        .Q(gt0_rx_cdrlock_counter_reg__0[2]),
        .SOFT_RESET_RX_IN(SOFT_RESET_RX_IN),
        .SR(gt0_gtrxreset_in2_out),
        .SYSCLK_IN(SYSCLK_IN),
        .data_in(RXRESETDONE),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_gtrxreset_in(gt0_gtrxreset_in),
        .\gt0_rx_cdrlock_counter_reg[0] (gt0_rx_cdrlocked_i_2_n_0),
        .\gt0_rx_cdrlock_counter_reg[3] (gt0_rx_cdrlocked_i_3_n_0),
        .\gt0_rx_cdrlock_counter_reg[7] (gt0_rx_cdrlocked_i_4_n_0),
        .gt0_rx_cdrlocked_reg(gt_rxresetfsm_i_n_4),
        .gt0_rx_cdrlocked_reg_0(gt0_rx_cdrlocked_reg_n_0),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_in[0]),
        .gt0_rxuserrdy_in(gt0_rxuserrdy_in),
        .gt0_rxuserrdy_in3_out(gt0_rxuserrdy_in3_out),
        .gt0_rxusrclk_in(gt0_rxusrclk_in),
        .gt0_txsysclksel_in(gt0_txsysclksel_in[0]),
        .p_4_in(p_4_in));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_TX_STARTUP_FSM gt_txresetfsm_i
       (.GT_TX_FSM_RESET_DONE_OUT(GT_TX_FSM_RESET_DONE_OUT),
        .SOFT_RESET_TX_IN(SOFT_RESET_TX_IN),
        .SYSCLK_IN(SYSCLK_IN),
        .data_in(TXRESETDONE),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_gttxreset_in(gt0_gttxreset_in),
        .gt0_gttxreset_in1_out(gt0_gttxreset_in1_out),
        .gt0_txsysclksel_in(gt0_txsysclksel_in[0]),
        .gt0_txuserrdy_in(gt0_txuserrdy_in),
        .gt0_txuserrdy_in0_out(gt0_txuserrdy_in0_out),
        .gt0_txusrclk_in(gt0_txusrclk_in),
        .p_5_in(p_5_in));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_multi_gt
   (gt0_cpllfbclklost_out,
    gt0_cplllock_out,
    gt0_drprdy_out,
    gt0_eyescandataerror_out,
    gt0_gthtxn_out,
    gt0_gthtxp_out,
    gt0_rxbyteisaligned_out,
    gt0_rxbyterealign_out,
    gt0_rxcommadet_out,
    gt0_rxoutclk_out,
    gt0_rxoutclkfabric_out,
    gt0_rxprbserr_out,
    gt0_rxresetdone_out,
    gt0_txoutclk_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txresetdone_out,
    gt0_dmonitorout_out,
    gt0_drpdo_out,
    gt0_txbufstatus_out,
    gt0_rxbufstatus_out,
    gt0_rxstatus_out,
    gt0_rxdata_out,
    gt0_rxmonitorout_out,
    gt0_rxchariscomma_out,
    gt0_rxcharisk_out,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    data_in,
    data_sync_reg1,
    gt0_cplllockdetclk_in,
    gt0_drpclk_in,
    gt0_drpen_in,
    gt0_drpwe_in,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    gt0_gthrxn_in,
    gt0_gthrxp_in,
    gt0_gtnorthrefclk0_in,
    gt0_gtnorthrefclk1_in,
    gt0_gtrefclk0_in,
    gt0_gtrefclk1_in,
    SR,
    gt0_gtsouthrefclk0_in,
    gt0_gtsouthrefclk1_in,
    gt0_gttxreset_in1_out,
    GT0_QPLLOUTCLK_IN,
    GT0_QPLLOUTREFCLK_IN,
    gt0_rxbufreset_in,
    gt0_rxcdrhold_in,
    gt0_rxdfelpmreset_in,
    gt0_rxlpmen_in,
    gt0_rxmcommaalignen_in,
    gt0_rxpcommaalignen_in,
    gt0_rxpcsreset_in,
    gt0_rxpmareset_in,
    gt0_rxpolarity_in,
    gt0_rxprbscntreset_in,
    gt0_rxuserrdy_in3_out,
    gt0_rxusrclk_in,
    gt0_rxusrclk2_in,
    gt0_txinhibit_in,
    gt0_txpcsreset_in,
    gt0_txpmareset_in,
    gt0_txpolarity_in,
    gt0_txprbsforceerr_in,
    gt0_txuserrdy_in0_out,
    gt0_txusrclk_in,
    gt0_txusrclk2_in,
    gt0_drpdi_in,
    gt0_rxmonitorsel_in,
    gt0_rxpd_in,
    gt0_rxsysclksel_in,
    gt0_txpd_in,
    gt0_txsysclksel_in,
    gt0_loopback_in,
    gt0_rxprbssel_in,
    gt0_txprbssel_in,
    gt0_txdiffctrl_in,
    gt0_txpostcursor_in,
    gt0_txprecursor_in,
    gt0_txdata_in,
    gt0_txcharisk_in,
    gt0_drpaddr_in,
    p_5_in,
    gt0_cpllreset_in,
    p_4_in,
    gt0_cpllpd_in);
  output gt0_cpllfbclklost_out;
  output gt0_cplllock_out;
  output gt0_drprdy_out;
  output gt0_eyescandataerror_out;
  output gt0_gthtxn_out;
  output gt0_gthtxp_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  output gt0_rxoutclk_out;
  output gt0_rxoutclkfabric_out;
  output gt0_rxprbserr_out;
  output gt0_rxresetdone_out;
  output gt0_txoutclk_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  output gt0_txresetdone_out;
  output [14:0]gt0_dmonitorout_out;
  output [15:0]gt0_drpdo_out;
  output [1:0]gt0_txbufstatus_out;
  output [2:0]gt0_rxbufstatus_out;
  output [2:0]gt0_rxstatus_out;
  output [31:0]gt0_rxdata_out;
  output [6:0]gt0_rxmonitorout_out;
  output [3:0]gt0_rxchariscomma_out;
  output [3:0]gt0_rxcharisk_out;
  output [3:0]gt0_rxdisperr_out;
  output [3:0]gt0_rxnotintable_out;
  output data_in;
  output data_sync_reg1;
  input gt0_cplllockdetclk_in;
  input gt0_drpclk_in;
  input gt0_drpen_in;
  input gt0_drpwe_in;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input gt0_gthrxn_in;
  input gt0_gthrxp_in;
  input gt0_gtnorthrefclk0_in;
  input gt0_gtnorthrefclk1_in;
  input gt0_gtrefclk0_in;
  input gt0_gtrefclk1_in;
  input [0:0]SR;
  input gt0_gtsouthrefclk0_in;
  input gt0_gtsouthrefclk1_in;
  input gt0_gttxreset_in1_out;
  input GT0_QPLLOUTCLK_IN;
  input GT0_QPLLOUTREFCLK_IN;
  input gt0_rxbufreset_in;
  input gt0_rxcdrhold_in;
  input gt0_rxdfelpmreset_in;
  input gt0_rxlpmen_in;
  input gt0_rxmcommaalignen_in;
  input gt0_rxpcommaalignen_in;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  input gt0_rxpolarity_in;
  input gt0_rxprbscntreset_in;
  input gt0_rxuserrdy_in3_out;
  input gt0_rxusrclk_in;
  input gt0_rxusrclk2_in;
  input gt0_txinhibit_in;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  input gt0_txpolarity_in;
  input gt0_txprbsforceerr_in;
  input gt0_txuserrdy_in0_out;
  input gt0_txusrclk_in;
  input gt0_txusrclk2_in;
  input [15:0]gt0_drpdi_in;
  input [1:0]gt0_rxmonitorsel_in;
  input [1:0]gt0_rxpd_in;
  input [1:0]gt0_rxsysclksel_in;
  input [1:0]gt0_txpd_in;
  input [1:0]gt0_txsysclksel_in;
  input [2:0]gt0_loopback_in;
  input [2:0]gt0_rxprbssel_in;
  input [2:0]gt0_txprbssel_in;
  input [3:0]gt0_txdiffctrl_in;
  input [4:0]gt0_txpostcursor_in;
  input [4:0]gt0_txprecursor_in;
  input [31:0]gt0_txdata_in;
  input [3:0]gt0_txcharisk_in;
  input [8:0]gt0_drpaddr_in;
  input p_5_in;
  input gt0_cpllreset_in;
  input p_4_in;
  input gt0_cpllpd_in;

  wire GT0_QPLLOUTCLK_IN;
  wire GT0_QPLLOUTREFCLK_IN;
  wire [0:0]SR;
  wire cpllpd_in;
  wire cpllreset_in;
  wire data_in;
  wire data_sync_reg1;
  wire gt0_cpllfbclklost_out;
  wire gt0_cplllock_out;
  wire gt0_cplllockdetclk_in;
  wire gt0_cpllpd_in;
  wire gt0_cpllreset_in;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire gt0_drpclk_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gthrxn_in;
  wire gt0_gthrxp_in;
  wire gt0_gthtxn_out;
  wire gt0_gthtxp_out;
  wire gt0_gtnorthrefclk0_in;
  wire gt0_gtnorthrefclk1_in;
  wire gt0_gtrefclk0_in;
  wire gt0_gtrefclk1_in;
  wire gt0_gtsouthrefclk0_in;
  wire gt0_gtsouthrefclk1_in;
  wire gt0_gttxreset_in1_out;
  wire [2:0]gt0_loopback_in;
  wire gt0_rxbufreset_in;
  wire [2:0]gt0_rxbufstatus_out;
  wire gt0_rxbyteisaligned_out;
  wire gt0_rxbyterealign_out;
  wire gt0_rxcdrhold_in;
  wire [3:0]gt0_rxchariscomma_out;
  wire [3:0]gt0_rxcharisk_out;
  wire gt0_rxcommadet_out;
  wire [31:0]gt0_rxdata_out;
  wire gt0_rxdfelpmreset_in;
  wire [3:0]gt0_rxdisperr_out;
  wire gt0_rxlpmen_in;
  wire gt0_rxmcommaalignen_in;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire [3:0]gt0_rxnotintable_out;
  wire gt0_rxoutclk_out;
  wire gt0_rxoutclkfabric_out;
  wire gt0_rxpcommaalignen_in;
  wire gt0_rxpcsreset_in;
  wire [1:0]gt0_rxpd_in;
  wire gt0_rxpmareset_in;
  wire gt0_rxpolarity_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_rxsysclksel_in;
  wire gt0_rxuserrdy_in3_out;
  wire gt0_rxusrclk2_in;
  wire gt0_rxusrclk_in;
  wire [1:0]gt0_txbufstatus_out;
  wire [3:0]gt0_txcharisk_in;
  wire [31:0]gt0_txdata_in;
  wire [3:0]gt0_txdiffctrl_in;
  wire gt0_txinhibit_in;
  wire gt0_txoutclk_out;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txpcsreset_in;
  wire [1:0]gt0_txpd_in;
  wire gt0_txpmareset_in;
  wire gt0_txpolarity_in;
  wire [4:0]gt0_txpostcursor_in;
  wire gt0_txprbsforceerr_in;
  wire [2:0]gt0_txprbssel_in;
  wire [4:0]gt0_txprecursor_in;
  wire gt0_txresetdone_out;
  wire [1:0]gt0_txsysclksel_in;
  wire gt0_txuserrdy_in0_out;
  wire gt0_txusrclk2_in;
  wire gt0_txusrclk_in;
  wire p_4_in;
  wire p_5_in;

  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_cpll_railing cpll_railing0_i
       (.cpllpd_in(cpllpd_in),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllpd_in(gt0_cpllpd_in),
        .gt0_cpllreset_in(gt0_cpllreset_in),
        .gt0_gtrefclk0_in(gt0_gtrefclk0_in),
        .p_4_in(p_4_in),
        .p_5_in(p_5_in));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_GT gt0_Jesd204_microblaze_jesd204_phy_0_0_gt_i
       (.GT0_QPLLOUTCLK_IN(GT0_QPLLOUTCLK_IN),
        .GT0_QPLLOUTREFCLK_IN(GT0_QPLLOUTREFCLK_IN),
        .SR(SR),
        .cpllpd_in(cpllpd_in),
        .cpllreset_in(cpllreset_in),
        .data_in(data_in),
        .data_sync_reg1(data_sync_reg1),
        .gt0_cpllfbclklost_out(gt0_cpllfbclklost_out),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_cplllockdetclk_in(gt0_cplllockdetclk_in),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr_in(gt0_drpaddr_in),
        .gt0_drpclk_in(gt0_drpclk_in),
        .gt0_drpdi_in(gt0_drpdi_in),
        .gt0_drpdo_out(gt0_drpdo_out),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drprdy_out(gt0_drprdy_out),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_gthrxn_in(gt0_gthrxn_in),
        .gt0_gthrxp_in(gt0_gthrxp_in),
        .gt0_gthtxn_out(gt0_gthtxn_out),
        .gt0_gthtxp_out(gt0_gthtxp_out),
        .gt0_gtnorthrefclk0_in(gt0_gtnorthrefclk0_in),
        .gt0_gtnorthrefclk1_in(gt0_gtnorthrefclk1_in),
        .gt0_gtrefclk0_in(gt0_gtrefclk0_in),
        .gt0_gtrefclk1_in(gt0_gtrefclk1_in),
        .gt0_gtsouthrefclk0_in(gt0_gtsouthrefclk0_in),
        .gt0_gtsouthrefclk1_in(gt0_gtsouthrefclk1_in),
        .gt0_gttxreset_in1_out(gt0_gttxreset_in1_out),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxbufreset_in(gt0_rxbufreset_in),
        .gt0_rxbufstatus_out(gt0_rxbufstatus_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxbyterealign_out(gt0_rxbyterealign_out),
        .gt0_rxcdrhold_in(gt0_rxcdrhold_in),
        .gt0_rxchariscomma_out(gt0_rxchariscomma_out),
        .gt0_rxcharisk_out(gt0_rxcharisk_out),
        .gt0_rxcommadet_out(gt0_rxcommadet_out),
        .gt0_rxdata_out(gt0_rxdata_out),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxdisperr_out(gt0_rxdisperr_out),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxmcommaalignen_in(gt0_rxmcommaalignen_in),
        .gt0_rxmonitorout_out(gt0_rxmonitorout_out),
        .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),
        .gt0_rxnotintable_out(gt0_rxnotintable_out),
        .gt0_rxoutclk_out(gt0_rxoutclk_out),
        .gt0_rxoutclkfabric_out(gt0_rxoutclkfabric_out),
        .gt0_rxpcommaalignen_in(gt0_rxpcommaalignen_in),
        .gt0_rxpcsreset_in(gt0_rxpcsreset_in),
        .gt0_rxpd_in(gt0_rxpd_in),
        .gt0_rxpmareset_in(gt0_rxpmareset_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),
        .gt0_rxprbserr_out(gt0_rxprbserr_out),
        .gt0_rxprbssel_in(gt0_rxprbssel_in),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_in),
        .gt0_rxuserrdy_in3_out(gt0_rxuserrdy_in3_out),
        .gt0_rxusrclk2_in(gt0_rxusrclk2_in),
        .gt0_rxusrclk_in(gt0_rxusrclk_in),
        .gt0_txbufstatus_out(gt0_txbufstatus_out),
        .gt0_txcharisk_in(gt0_txcharisk_in),
        .gt0_txdata_in(gt0_txdata_in),
        .gt0_txdiffctrl_in(gt0_txdiffctrl_in),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txoutclk_out(gt0_txoutclk_out),
        .gt0_txoutclkfabric_out(gt0_txoutclkfabric_out),
        .gt0_txoutclkpcs_out(gt0_txoutclkpcs_out),
        .gt0_txpcsreset_in(gt0_txpcsreset_in),
        .gt0_txpd_in(gt0_txpd_in),
        .gt0_txpmareset_in(gt0_txpmareset_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .gt0_txpostcursor_in(gt0_txpostcursor_in),
        .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),
        .gt0_txprbssel_in(gt0_txprbssel_in),
        .gt0_txprecursor_in(gt0_txprecursor_in),
        .gt0_txresetdone_out(gt0_txresetdone_out),
        .gt0_txsysclksel_in(gt0_txsysclksel_in),
        .gt0_txuserrdy_in0_out(gt0_txuserrdy_in0_out),
        .gt0_txusrclk2_in(gt0_txusrclk2_in),
        .gt0_txusrclk_in(gt0_txusrclk_in));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block
   (data_out,
    E,
    gt0_txsysclksel_in,
    data_sync_reg6_0,
    time_out_2ms_reg,
    pll_reset_asserted_reg,
    out,
    \wait_time_cnt_reg[6] ,
    \FSM_sequential_tx_state_reg[1] ,
    init_wait_done_reg,
    reset_time_out_reg,
    reset_time_out_reg_0,
    gt0_cplllock_out,
    SYSCLK_IN);
  output data_out;
  output [0:0]E;
  input [0:0]gt0_txsysclksel_in;
  input data_sync_reg6_0;
  input time_out_2ms_reg;
  input pll_reset_asserted_reg;
  input [3:0]out;
  input [0:0]\wait_time_cnt_reg[6] ;
  input \FSM_sequential_tx_state_reg[1] ;
  input init_wait_done_reg;
  input reset_time_out_reg;
  input reset_time_out_reg_0;
  input gt0_cplllock_out;
  input SYSCLK_IN;

  wire [0:0]E;
  wire \FSM_sequential_tx_state[3]_i_10_n_0 ;
  wire \FSM_sequential_tx_state_reg[1] ;
  wire \FSM_sequential_tx_state_reg[3]_i_3_n_0 ;
  wire \FSM_sequential_tx_state_reg[3]_i_6_n_0 ;
  wire \FSM_sequential_tx_state_reg[3]_i_7_n_0 ;
  wire SYSCLK_IN;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;
  wire gt0_cplllock_out;
  wire [0:0]gt0_txsysclksel_in;
  wire init_wait_done_reg;
  wire [3:0]out;
  wire pll_reset_asserted_reg;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire time_out_2ms_reg;
  wire tx_state04_out;
  wire [0:0]\wait_time_cnt_reg[6] ;

  LUT6 #(
    .INIT(64'h00338BBB00338B88)) 
    \FSM_sequential_tx_state[3]_i_1 
       (.I0(\FSM_sequential_tx_state_reg[3]_i_3_n_0 ),
        .I1(out[0]),
        .I2(\wait_time_cnt_reg[6] ),
        .I3(\FSM_sequential_tx_state_reg[1] ),
        .I4(out[3]),
        .I5(init_wait_done_reg),
        .O(E));
  LUT4 #(
    .INIT(16'hFFE2)) 
    \FSM_sequential_tx_state[3]_i_10 
       (.I0(data_out),
        .I1(gt0_txsysclksel_in),
        .I2(data_sync_reg6_0),
        .I3(time_out_2ms_reg),
        .O(\FSM_sequential_tx_state[3]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h02A2)) 
    \FSM_sequential_tx_state[3]_i_8 
       (.I0(pll_reset_asserted_reg),
        .I1(data_out),
        .I2(gt0_txsysclksel_in),
        .I3(data_sync_reg6_0),
        .O(tx_state04_out));
  MUXF8 \FSM_sequential_tx_state_reg[3]_i_3 
       (.I0(\FSM_sequential_tx_state_reg[3]_i_6_n_0 ),
        .I1(\FSM_sequential_tx_state_reg[3]_i_7_n_0 ),
        .O(\FSM_sequential_tx_state_reg[3]_i_3_n_0 ),
        .S(out[1]));
  MUXF7 \FSM_sequential_tx_state_reg[3]_i_6 
       (.I0(tx_state04_out),
        .I1(reset_time_out_reg),
        .O(\FSM_sequential_tx_state_reg[3]_i_6_n_0 ),
        .S(out[2]));
  MUXF7 \FSM_sequential_tx_state_reg[3]_i_7 
       (.I0(\FSM_sequential_tx_state[3]_i_10_n_0 ),
        .I1(reset_time_out_reg_0),
        .O(\FSM_sequential_tx_state_reg[3]_i_7_n_0 ),
        .S(out[2]));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(gt0_cplllock_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_0
   (data_out,
    reset_time_out_reg,
    gt0_txsysclksel_in,
    data_sync_reg6_0,
    reset_time_out,
    txresetdone_s3_reg,
    out,
    init_wait_done_reg,
    SYSCLK_IN);
  output data_out;
  output reset_time_out_reg;
  input [0:0]gt0_txsysclksel_in;
  input data_sync_reg6_0;
  input reset_time_out;
  input txresetdone_s3_reg;
  input [3:0]out;
  input init_wait_done_reg;
  input SYSCLK_IN;

  wire SYSCLK_IN;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;
  wire [0:0]gt0_txsysclksel_in;
  wire init_wait_done_reg;
  wire [3:0]out;
  wire reset_time_out;
  wire reset_time_out_1;
  wire reset_time_out_i_3_n_0;
  wire reset_time_out_reg;
  wire tx_state0;
  wire txresetdone_s3_reg;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(1'b1),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    reset_time_out_i_1
       (.I0(reset_time_out_1),
        .I1(reset_time_out_i_3_n_0),
        .I2(reset_time_out),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h00CFC0EF00C0C0E0)) 
    reset_time_out_i_2
       (.I0(tx_state0),
        .I1(txresetdone_s3_reg),
        .I2(out[0]),
        .I3(out[3]),
        .I4(out[2]),
        .I5(init_wait_done_reg),
        .O(reset_time_out_1));
  LUT6 #(
    .INIT(64'h303030302020FFFC)) 
    reset_time_out_i_3
       (.I0(tx_state0),
        .I1(out[3]),
        .I2(out[0]),
        .I3(init_wait_done_reg),
        .I4(out[1]),
        .I5(out[2]),
        .O(reset_time_out_i_3_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    reset_time_out_i_4__0
       (.I0(data_out),
        .I1(gt0_txsysclksel_in),
        .I2(data_sync_reg6_0),
        .O(tx_state0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_1
   (data_out,
    data_in,
    SYSCLK_IN);
  output data_out;
  input data_in;
  input SYSCLK_IN;

  wire SYSCLK_IN;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_10
   (SR,
    mmcm_lock_reclocked_reg,
    mmcm_lock_reclocked,
    Q,
    \mmcm_lock_count_reg[4] ,
    SYSCLK_IN);
  output [0:0]SR;
  output mmcm_lock_reclocked_reg;
  input mmcm_lock_reclocked;
  input [1:0]Q;
  input \mmcm_lock_count_reg[4] ;
  input SYSCLK_IN;

  wire [1:0]Q;
  wire [0:0]SR;
  wire SYSCLK_IN;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire \mmcm_lock_count_reg[4] ;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_reg;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(1'b1),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(mmcm_lock_i),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1__0 
       (.I0(mmcm_lock_i),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1__0
       (.I0(mmcm_lock_reclocked),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\mmcm_lock_count_reg[4] ),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_reg));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_11
   (data_out,
    data_in,
    gt0_rxusrclk_in);
  output data_out;
  input data_in;
  input gt0_rxusrclk_in;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_rxusrclk_in;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_12
   (data_out,
    GT_RX_FSM_RESET_DONE_OUT,
    gt0_rxusrclk_in);
  output data_out;
  input GT_RX_FSM_RESET_DONE_OUT;
  input gt0_rxusrclk_in;

  wire GT_RX_FSM_RESET_DONE_OUT;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_rxusrclk_in;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(GT_RX_FSM_RESET_DONE_OUT),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(gt0_rxusrclk_in),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_13
   (data_out,
    data_in,
    SYSCLK_IN);
  output data_out;
  input data_in;
  input SYSCLK_IN;

  wire SYSCLK_IN;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_2
   (SR,
    mmcm_lock_reclocked_reg,
    mmcm_lock_reclocked,
    Q,
    \mmcm_lock_count_reg[4] ,
    SYSCLK_IN);
  output [0:0]SR;
  output mmcm_lock_reclocked_reg;
  input mmcm_lock_reclocked;
  input [1:0]Q;
  input \mmcm_lock_count_reg[4] ;
  input SYSCLK_IN;

  wire [1:0]Q;
  wire [0:0]SR;
  wire SYSCLK_IN;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire \mmcm_lock_count_reg[4] ;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_reg;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(1'b1),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(mmcm_lock_i),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1 
       (.I0(mmcm_lock_i),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\mmcm_lock_count_reg[4] ),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_reg));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_3
   (data_out,
    data_in,
    gt0_txusrclk_in);
  output data_out;
  input data_in;
  input gt0_txusrclk_in;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_txusrclk_in;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_4
   (data_out,
    data_in,
    SYSCLK_IN);
  output data_out;
  input data_in;
  input SYSCLK_IN;

  wire SYSCLK_IN;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_5
   (data_out,
    GT_TX_FSM_RESET_DONE_OUT,
    gt0_txusrclk_in);
  output data_out;
  input GT_TX_FSM_RESET_DONE_OUT;
  input gt0_txusrclk_in;

  wire GT_TX_FSM_RESET_DONE_OUT;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_txusrclk_in;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(GT_TX_FSM_RESET_DONE_OUT),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(gt0_txusrclk_in),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_6
   (data_out,
    gt0_cplllock_out,
    SYSCLK_IN);
  output data_out;
  input gt0_cplllock_out;
  input SYSCLK_IN;

  wire SYSCLK_IN;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_cplllock_out;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(gt0_cplllock_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_7
   (rx_state01_out,
    \FSM_sequential_rx_state_reg[0] ,
    reset_time_out_reg,
    gt0_rxsysclksel_in,
    data_out,
    gt0_txsysclksel_in,
    pll_reset_asserted_reg,
    time_out_2ms_reg,
    out,
    rxresetdone_s3,
    reset_time_out_reg_0,
    gt0_rx_cdrlocked_reg,
    \FSM_sequential_rx_state_reg[3] ,
    data_sync_reg6_0,
    gt0_rx_cdrlocked_reg_0,
    SYSCLK_IN);
  output rx_state01_out;
  output \FSM_sequential_rx_state_reg[0] ;
  output reset_time_out_reg;
  input [0:0]gt0_rxsysclksel_in;
  input data_out;
  input [0:0]gt0_txsysclksel_in;
  input pll_reset_asserted_reg;
  input time_out_2ms_reg;
  input [3:0]out;
  input rxresetdone_s3;
  input reset_time_out_reg_0;
  input gt0_rx_cdrlocked_reg;
  input \FSM_sequential_rx_state_reg[3] ;
  input data_sync_reg6_0;
  input gt0_rx_cdrlocked_reg_0;
  input SYSCLK_IN;

  wire \FSM_sequential_rx_state[3]_i_8_n_0 ;
  wire \FSM_sequential_rx_state_reg[0] ;
  wire \FSM_sequential_rx_state_reg[3] ;
  wire SYSCLK_IN;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;
  wire gt0_rx_cdrlocked_reg;
  wire gt0_rx_cdrlocked_reg_0;
  wire [0:0]gt0_rxsysclksel_in;
  wire [0:0]gt0_txsysclksel_in;
  wire [3:0]out;
  wire pll_reset_asserted_reg;
  wire qplllock_sync;
  wire reset_time_out_i_3__0_n_0;
  wire reset_time_out_i_5__0_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire rx_state01_out;
  wire rxresetdone_s3;
  wire time_out_2ms_reg;

  LUT6 #(
    .INIT(64'h00000000FE0EFEAE)) 
    \FSM_sequential_rx_state[3]_i_3 
       (.I0(time_out_2ms_reg),
        .I1(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I2(out[2]),
        .I3(rxresetdone_s3),
        .I4(reset_time_out_reg_0),
        .I5(out[3]),
        .O(\FSM_sequential_rx_state_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \FSM_sequential_rx_state[3]_i_8 
       (.I0(qplllock_sync),
        .I1(gt0_rxsysclksel_in),
        .I2(data_out),
        .O(\FSM_sequential_rx_state[3]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hA7A5F7A5)) 
    \FSM_sequential_rx_state[3]_i_9 
       (.I0(gt0_rxsysclksel_in),
        .I1(qplllock_sync),
        .I2(gt0_txsysclksel_in),
        .I3(pll_reset_asserted_reg),
        .I4(data_out),
        .O(rx_state01_out));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(1'b1),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(qplllock_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFEAEFFFFFEAE0000)) 
    reset_time_out_i_1__0
       (.I0(gt0_rx_cdrlocked_reg),
        .I1(reset_time_out_i_3__0_n_0),
        .I2(out[2]),
        .I3(\FSM_sequential_rx_state_reg[3] ),
        .I4(reset_time_out_i_5__0_n_0),
        .I5(reset_time_out_reg_0),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h303FBBBF303F888C)) 
    reset_time_out_i_3__0
       (.I0(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I1(out[1]),
        .I2(data_sync_reg6_0),
        .I3(out[0]),
        .I4(out[3]),
        .I5(rx_state01_out),
        .O(reset_time_out_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0F303F380F303C38)) 
    reset_time_out_i_5__0
       (.I0(gt0_rx_cdrlocked_reg_0),
        .I1(out[2]),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[1]),
        .I5(rx_state01_out),
        .O(reset_time_out_i_5__0_n_0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_8
   (data_out,
    data_in,
    SYSCLK_IN);
  output data_out;
  input data_in;
  input SYSCLK_IN;

  wire SYSCLK_IN;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_sync_block_9
   (data_out,
    rx_fsm_reset_done_int_reg,
    D,
    E,
    DONT_RESET_ON_DATA_ERROR_IN,
    time_out_100us_reg,
    reset_time_out_reg,
    out,
    GT_RX_FSM_RESET_DONE_OUT,
    \FSM_sequential_rx_state_reg[2] ,
    time_out_2ms_reg,
    gt0_rx_cdrlocked_reg,
    Q,
    \wait_time_cnt_reg[4] ,
    mmcm_lock_reclocked,
    rx_state128_out,
    rx_state01_out,
    time_out_1us_reg,
    time_out_wait_bypass_s3,
    time_out_2ms_reg_0,
    GT0_DATA_VALID_IN,
    SYSCLK_IN);
  output data_out;
  output rx_fsm_reset_done_int_reg;
  output [2:0]D;
  output [0:0]E;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input time_out_100us_reg;
  input reset_time_out_reg;
  input [3:0]out;
  input GT_RX_FSM_RESET_DONE_OUT;
  input \FSM_sequential_rx_state_reg[2] ;
  input time_out_2ms_reg;
  input gt0_rx_cdrlocked_reg;
  input [0:0]Q;
  input \wait_time_cnt_reg[4] ;
  input mmcm_lock_reclocked;
  input rx_state128_out;
  input rx_state01_out;
  input time_out_1us_reg;
  input time_out_wait_bypass_s3;
  input time_out_2ms_reg_0;
  input GT0_DATA_VALID_IN;
  input SYSCLK_IN;

  wire [2:0]D;
  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire [0:0]E;
  wire \FSM_sequential_rx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_rx_state_reg[2] ;
  wire GT0_DATA_VALID_IN;
  wire GT_RX_FSM_RESET_DONE_OUT;
  wire [0:0]Q;
  wire SYSCLK_IN;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_rx_cdrlocked_reg;
  wire mmcm_lock_reclocked;
  wire [3:0]out;
  wire reset_time_out_reg;
  wire rx_fsm_reset_done_int;
  wire rx_fsm_reset_done_int_i_3_n_0;
  wire rx_fsm_reset_done_int_i_4_n_0;
  wire rx_fsm_reset_done_int_reg;
  wire rx_state01_out;
  wire rx_state126_out;
  wire rx_state128_out;
  wire time_out_100us_reg;
  wire time_out_1us_reg;
  wire time_out_2ms_reg;
  wire time_out_2ms_reg_0;
  wire time_out_wait_bypass_s3;
  wire \wait_time_cnt_reg[4] ;

  LUT6 #(
    .INIT(64'h0C0C5DFD0C0C5D5D)) 
    \FSM_sequential_rx_state[0]_i_1 
       (.I0(out[0]),
        .I1(\FSM_sequential_rx_state_reg[2] ),
        .I2(out[3]),
        .I3(out[1]),
        .I4(out[2]),
        .I5(rx_state126_out),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0000050000FF7700)) 
    \FSM_sequential_rx_state[1]_i_1 
       (.I0(out[2]),
        .I1(rx_state128_out),
        .I2(rx_state126_out),
        .I3(out[0]),
        .I4(out[1]),
        .I5(out[3]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \FSM_sequential_rx_state[1]_i_2 
       (.I0(DONT_RESET_ON_DATA_ERROR_IN),
        .I1(time_out_100us_reg),
        .I2(reset_time_out_reg),
        .I3(data_out),
        .O(rx_state126_out));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \FSM_sequential_rx_state[3]_i_1 
       (.I0(time_out_2ms_reg),
        .I1(\FSM_sequential_rx_state[3]_i_4_n_0 ),
        .I2(out[0]),
        .I3(\FSM_sequential_rx_state[3]_i_5_n_0 ),
        .I4(out[1]),
        .I5(gt0_rx_cdrlocked_reg),
        .O(E));
  LUT6 #(
    .INIT(64'h55AA00A2000000A2)) 
    \FSM_sequential_rx_state[3]_i_2 
       (.I0(out[3]),
        .I1(time_out_wait_bypass_s3),
        .I2(out[1]),
        .I3(out[2]),
        .I4(out[0]),
        .I5(\FSM_sequential_rx_state[3]_i_7_n_0 ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h0F00EFEF0F00E0E0)) 
    \FSM_sequential_rx_state[3]_i_4 
       (.I0(mmcm_lock_reclocked),
        .I1(rx_state128_out),
        .I2(out[2]),
        .I3(rx_fsm_reset_done_int_i_4_n_0),
        .I4(out[3]),
        .I5(rx_state01_out),
        .O(\FSM_sequential_rx_state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h1010101F)) 
    \FSM_sequential_rx_state[3]_i_5 
       (.I0(out[2]),
        .I1(data_out),
        .I2(out[3]),
        .I3(Q),
        .I4(\wait_time_cnt_reg[4] ),
        .O(\FSM_sequential_rx_state[3]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hB0B0000F)) 
    \FSM_sequential_rx_state[3]_i_7 
       (.I0(reset_time_out_reg),
        .I1(time_out_2ms_reg_0),
        .I2(out[1]),
        .I3(rx_state126_out),
        .I4(out[2]),
        .O(\FSM_sequential_rx_state[3]_i_7_n_0 ));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(GT0_DATA_VALID_IN),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(SYSCLK_IN),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    rx_fsm_reset_done_int_i_1
       (.I0(rx_fsm_reset_done_int),
        .I1(out[2]),
        .I2(out[3]),
        .I3(rx_fsm_reset_done_int_i_3_n_0),
        .I4(GT_RX_FSM_RESET_DONE_OUT),
        .O(rx_fsm_reset_done_int_reg));
  LUT5 #(
    .INIT(32'h00000008)) 
    rx_fsm_reset_done_int_i_2
       (.I0(data_out),
        .I1(time_out_1us_reg),
        .I2(reset_time_out_reg),
        .I3(out[0]),
        .I4(out[2]),
        .O(rx_fsm_reset_done_int));
  LUT6 #(
    .INIT(64'h0838083838380838)) 
    rx_fsm_reset_done_int_i_3
       (.I0(rx_fsm_reset_done_int_i_4_n_0),
        .I1(out[0]),
        .I2(out[1]),
        .I3(data_out),
        .I4(time_out_1us_reg),
        .I5(reset_time_out_reg),
        .O(rx_fsm_reset_done_int_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFF04)) 
    rx_fsm_reset_done_int_i_4
       (.I0(DONT_RESET_ON_DATA_ERROR_IN),
        .I1(time_out_100us_reg),
        .I2(reset_time_out_reg),
        .I3(data_out),
        .O(rx_fsm_reset_done_int_i_4_n_0));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gtwizard_0_common
   (common0_qpll_clk_in,
    common0_qpll_refclk_in,
    cpll_refclk);
  output common0_qpll_clk_in;
  output common0_qpll_refclk_in;
  input cpll_refclk;

  wire common0_qpll_clk_in;
  wire common0_qpll_refclk_in;
  wire cpll_refclk;
  wire gthe2_common_i_n_0;
  wire gthe2_common_i_n_10;
  wire gthe2_common_i_n_11;
  wire gthe2_common_i_n_12;
  wire gthe2_common_i_n_13;
  wire gthe2_common_i_n_14;
  wire gthe2_common_i_n_15;
  wire gthe2_common_i_n_16;
  wire gthe2_common_i_n_17;
  wire gthe2_common_i_n_18;
  wire gthe2_common_i_n_19;
  wire gthe2_common_i_n_2;
  wire gthe2_common_i_n_20;
  wire gthe2_common_i_n_21;
  wire gthe2_common_i_n_22;
  wire gthe2_common_i_n_5;
  wire gthe2_common_i_n_7;
  wire gthe2_common_i_n_8;
  wire gthe2_common_i_n_9;
  wire NLW_gthe2_common_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gthe2_common_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gthe2_common_i_PMARSVDOUT_UNCONNECTED;
  wire [7:0]NLW_gthe2_common_i_QPLLDMONITOR_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  GTHE2_COMMON #(
    .BIAS_CFG(64'h0000040000001050),
    .COMMON_CFG(32'h0000005C),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h04801C7),
    .QPLL_CLKOUT_CFG(4'b1111),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0010000000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h05E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .QPLL_RP_COMP(1'b0),
    .QPLL_VTRL_RESET(2'b00),
    .RCAL_CFG(2'b00),
    .RSVD_ATTR0(16'h0000),
    .RSVD_ATTR1(16'h0000),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_VERSION("2.0")) 
    gthe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .BGRCALOVRDENB(1'b1),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({gthe2_common_i_n_7,gthe2_common_i_n_8,gthe2_common_i_n_9,gthe2_common_i_n_10,gthe2_common_i_n_11,gthe2_common_i_n_12,gthe2_common_i_n_13,gthe2_common_i_n_14,gthe2_common_i_n_15,gthe2_common_i_n_16,gthe2_common_i_n_17,gthe2_common_i_n_18,gthe2_common_i_n_19,gthe2_common_i_n_20,gthe2_common_i_n_21,gthe2_common_i_n_22}),
        .DRPEN(1'b0),
        .DRPRDY(gthe2_common_i_n_0),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(cpll_refclk),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDOUT(NLW_gthe2_common_i_PMARSVDOUT_UNCONNECTED[15:0]),
        .QPLLDMONITOR(NLW_gthe2_common_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gthe2_common_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(gthe2_common_i_n_2),
        .QPLLLOCKDETCLK(1'b0),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(common0_qpll_clk_in),
        .QPLLOUTREFCLK(common0_qpll_refclk_in),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b1),
        .QPLLREFCLKLOST(gthe2_common_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(1'b0),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gthe2_common_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig
   (timeout_enable,
    \txpllclksel_reg[1] ,
    Q,
    \txpllclksel_reg[1]_0 ,
    \axi_rdata_reg[1] ,
    \axi_rdata_reg[2] ,
    \axi_rdata_reg[2]_0 ,
    \rx_pd_0_reg[1] ,
    \loopback_0_reg[2] ,
    \axi_rdata_reg[0] ,
    \timeout_timer_count_reg[11] ,
    \axi_rdata_reg[7] ,
    p_0_in,
    \slv_addr_reg[2] ,
    s_axi_aclk,
    \slv_addr_reg[6] ,
    \slv_addr_reg[5] ,
    \slv_rd_addr_reg[1] ,
    E,
    s_axi_wdata,
    \slv_addr_reg[2]_0 ,
    \slv_addr_reg[2]_1 );
  output timeout_enable;
  output \txpllclksel_reg[1] ;
  output [7:0]Q;
  output \txpllclksel_reg[1]_0 ;
  output \axi_rdata_reg[1] ;
  output \axi_rdata_reg[2] ;
  output \axi_rdata_reg[2]_0 ;
  output \rx_pd_0_reg[1] ;
  output \loopback_0_reg[2] ;
  output \axi_rdata_reg[0] ;
  output [11:0]\timeout_timer_count_reg[11] ;
  output [6:0]\axi_rdata_reg[7] ;
  input p_0_in;
  input \slv_addr_reg[2] ;
  input s_axi_aclk;
  input [3:0]\slv_addr_reg[6] ;
  input \slv_addr_reg[5] ;
  input [0:0]\slv_rd_addr_reg[1] ;
  input [0:0]E;
  input [11:0]s_axi_wdata;
  input [0:0]\slv_addr_reg[2]_0 ;
  input [0:0]\slv_addr_reg[2]_1 ;

  wire [0:0]E;
  wire [7:0]Q;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[1] ;
  wire \axi_rdata_reg[2] ;
  wire \axi_rdata_reg[2]_0 ;
  wire [6:0]\axi_rdata_reg[7] ;
  wire \cmm_interface_sel_reg_n_0_[0] ;
  wire \loopback_0_reg[2] ;
  wire p_0_in;
  wire \rx_pd_0_reg[1] ;
  wire s_axi_aclk;
  wire [11:0]s_axi_wdata;
  wire \slv_addr_reg[2] ;
  wire [0:0]\slv_addr_reg[2]_0 ;
  wire [0:0]\slv_addr_reg[2]_1 ;
  wire \slv_addr_reg[5] ;
  wire [3:0]\slv_addr_reg[6] ;
  wire [0:0]\slv_rd_addr_reg[1] ;
  wire timeout_enable;
  wire [11:0]\timeout_timer_count_reg[11] ;
  wire \txpllclksel_reg[1] ;
  wire \txpllclksel_reg[1]_0 ;

  LUT5 #(
    .INIT(32'h50035F03)) 
    \axi_rdata[0]_i_15 
       (.I0(timeout_enable),
        .I1(\cmm_interface_sel_reg_n_0_[0] ),
        .I2(\slv_addr_reg[6] [1]),
        .I3(\slv_addr_reg[6] [0]),
        .I4(Q[0]),
        .O(\axi_rdata_reg[0] ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \axi_rdata[1]_i_4 
       (.I0(\slv_rd_addr_reg[1] ),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\slv_addr_reg[6] [3]),
        .I5(\txpllclksel_reg[1]_0 ),
        .O(\axi_rdata_reg[1] ));
  LUT4 #(
    .INIT(16'h0100)) 
    \axi_rdata[2]_i_6 
       (.I0(\txpllclksel_reg[1]_0 ),
        .I1(\axi_rdata_reg[2]_0 ),
        .I2(\slv_addr_reg[6] [2]),
        .I3(\slv_addr_reg[6] [1]),
        .O(\axi_rdata_reg[2] ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \axi_rdata[4]_i_10 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[6]),
        .I3(Q[7]),
        .I4(Q[5]),
        .O(\txpllclksel_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \axi_rdata[4]_i_11 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\slv_addr_reg[6] [3]),
        .O(\axi_rdata_reg[2]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cmm_interface_sel_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_0 ),
        .D(s_axi_wdata[0]),
        .Q(\cmm_interface_sel_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \cmm_interface_sel_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_0 ),
        .D(s_axi_wdata[1]),
        .Q(\axi_rdata_reg[7] [0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \cmm_interface_sel_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_0 ),
        .D(s_axi_wdata[2]),
        .Q(\axi_rdata_reg[7] [1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \cmm_interface_sel_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_0 ),
        .D(s_axi_wdata[3]),
        .Q(\axi_rdata_reg[7] [2]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \cmm_interface_sel_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_0 ),
        .D(s_axi_wdata[4]),
        .Q(\axi_rdata_reg[7] [3]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \cmm_interface_sel_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_0 ),
        .D(s_axi_wdata[5]),
        .Q(\axi_rdata_reg[7] [4]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \cmm_interface_sel_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_0 ),
        .D(s_axi_wdata[6]),
        .Q(\axi_rdata_reg[7] [5]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \cmm_interface_sel_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_0 ),
        .D(s_axi_wdata[7]),
        .Q(\axi_rdata_reg[7] [6]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \gt_interface_sel_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_1 ),
        .D(s_axi_wdata[0]),
        .Q(Q[0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \gt_interface_sel_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_1 ),
        .D(s_axi_wdata[1]),
        .Q(Q[1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \gt_interface_sel_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_1 ),
        .D(s_axi_wdata[2]),
        .Q(Q[2]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \gt_interface_sel_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_1 ),
        .D(s_axi_wdata[3]),
        .Q(Q[3]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \gt_interface_sel_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_1 ),
        .D(s_axi_wdata[4]),
        .Q(Q[4]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \gt_interface_sel_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_1 ),
        .D(s_axi_wdata[5]),
        .Q(Q[5]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \gt_interface_sel_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_1 ),
        .D(s_axi_wdata[6]),
        .Q(Q[6]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \gt_interface_sel_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[2]_1 ),
        .D(s_axi_wdata[7]),
        .Q(Q[7]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \rx_pd_0[1]_i_2 
       (.I0(\slv_addr_reg[6] [2]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\slv_addr_reg[6] [3]),
        .I5(\txpllclksel_reg[1]_0 ),
        .O(\rx_pd_0_reg[1] ));
  FDSE #(
    .INIT(1'b1)) 
    timeout_enable_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2] ),
        .Q(timeout_enable),
        .S(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[0]),
        .Q(\timeout_timer_count_reg[11] [0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[10] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[10]),
        .Q(\timeout_timer_count_reg[11] [10]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[11] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[11]),
        .Q(\timeout_timer_count_reg[11] [11]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[1]),
        .Q(\timeout_timer_count_reg[11] [1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[2]),
        .Q(\timeout_timer_count_reg[11] [2]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[3]),
        .Q(\timeout_timer_count_reg[11] [3]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[4]),
        .Q(\timeout_timer_count_reg[11] [4]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[5]),
        .Q(\timeout_timer_count_reg[11] [5]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[6]),
        .Q(\timeout_timer_count_reg[11] [6]),
        .R(p_0_in));
  FDSE #(
    .INIT(1'b1)) 
    \timeout_value_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[7]),
        .Q(\timeout_timer_count_reg[11] [7]),
        .S(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[8]),
        .Q(\timeout_timer_count_reg[11] [8]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_value_reg[9] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[9]),
        .Q(\timeout_timer_count_reg[11] [9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \txpllclksel[1]_i_2 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\slv_addr_reg[6] [3]),
        .I4(\txpllclksel_reg[1]_0 ),
        .I5(\slv_addr_reg[5] ),
        .O(\txpllclksel_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \txpostcursor_0[4]_i_2 
       (.I0(\txpllclksel_reg[1]_0 ),
        .I1(\slv_addr_reg[6] [3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .O(\loopback_0_reg[2] ));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface
   (s_axi_wready,
    s_axi_arready,
    s_axi_rvalid,
    s_axi_bvalid,
    s_axi_awready,
    s_axi_bresp,
    s_axi_rresp,
    gt0_cpllpd_in,
    gt0_txinhibit_in,
    gt0_txpolarity_in,
    gt0_rxpolarity_in,
    gt0_rxlpmen_in,
    gt0_rxdfelpmreset_in,
    s_axi_rdata,
    Q,
    data_sync_reg1,
    gt0_rxpd_in,
    gt0_txsysclksel_in,
    gt0_rxsysclksel_in,
    data_sync_reg1_0,
    data_sync_reg1_1,
    gt0_loopback_in,
    gt0_txpd_in,
    \slv_rdata_reg[3] ,
    gt0_drpwe_in,
    gt0_drpen_in,
    data_in,
    \arststages_ff_reg[4] ,
    s_axi_aclk,
    SR,
    tx_core_clk,
    data_sync_reg_gsr,
    rx_core_clk,
    s_axi_aresetn,
    drpclk,
    s_axi_arvalid,
    s_axi_wvalid,
    s_axi_rready,
    s_axi_bready,
    s_axi_awvalid,
    s_axi_wdata,
    gt0_drprdy_out,
    data_out,
    data_sync_reg_gsr_0,
    D,
    s_axi_araddr,
    s_axi_awaddr,
    data_sync_reg_gsr_1,
    data_sync_reg_gsr_2,
    tx_sys_reset,
    rx_sys_reset,
    data_sync_reg_gsr_3);
  output s_axi_wready;
  output s_axi_arready;
  output s_axi_rvalid;
  output s_axi_bvalid;
  output s_axi_awready;
  output [0:0]s_axi_bresp;
  output [0:0]s_axi_rresp;
  output gt0_cpllpd_in;
  output gt0_txinhibit_in;
  output gt0_txpolarity_in;
  output gt0_rxpolarity_in;
  output gt0_rxlpmen_in;
  output gt0_rxdfelpmreset_in;
  output [24:0]s_axi_rdata;
  output [8:0]Q;
  output [15:0]data_sync_reg1;
  output [1:0]gt0_rxpd_in;
  output [1:0]gt0_txsysclksel_in;
  output [1:0]gt0_rxsysclksel_in;
  output [4:0]data_sync_reg1_0;
  output [4:0]data_sync_reg1_1;
  output [2:0]gt0_loopback_in;
  output [1:0]gt0_txpd_in;
  output [3:0]\slv_rdata_reg[3] ;
  output gt0_drpwe_in;
  output gt0_drpen_in;
  output data_in;
  output \arststages_ff_reg[4] ;
  input s_axi_aclk;
  input [0:0]SR;
  input tx_core_clk;
  input data_sync_reg_gsr;
  input rx_core_clk;
  input s_axi_aresetn;
  input drpclk;
  input s_axi_arvalid;
  input s_axi_wvalid;
  input s_axi_rready;
  input s_axi_bready;
  input s_axi_awvalid;
  input [17:0]s_axi_wdata;
  input gt0_drprdy_out;
  input data_out;
  input [0:0]data_sync_reg_gsr_0;
  input [15:0]D;
  input [9:0]s_axi_araddr;
  input [9:0]s_axi_awaddr;
  input data_sync_reg_gsr_1;
  input data_sync_reg_gsr_2;
  input tx_sys_reset;
  input rx_sys_reset;
  input data_sync_reg_gsr_3;

  wire [15:0]D;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_24;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_25;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_26;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_29;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_3;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_30;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_40;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_41;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_42;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_43;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_44;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_45;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_46;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_47;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_48;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_49;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_50;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_51;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_52;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_53;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_54;
  wire Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_55;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_10;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_11;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_12;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_13;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_14;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_15;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_16;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_29;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_30;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_31;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_32;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_33;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_34;
  wire Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_35;
  wire Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_12;
  wire Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_13;
  wire Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_24;
  wire Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_27;
  wire Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_28;
  wire Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_29;
  wire Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx_i_n_6;
  wire [8:0]Q;
  wire [0:0]SR;
  wire access_type5_out;
  wire \arststages_ff_reg[4] ;
  wire axi_register_if_i_n_13;
  wire axi_register_if_i_n_14;
  wire axi_register_if_i_n_15;
  wire axi_register_if_i_n_16;
  wire axi_register_if_i_n_17;
  wire axi_register_if_i_n_23;
  wire axi_register_if_i_n_24;
  wire axi_register_if_i_n_25;
  wire axi_register_if_i_n_26;
  wire axi_register_if_i_n_27;
  wire axi_register_if_i_n_29;
  wire axi_register_if_i_n_30;
  wire axi_register_if_i_n_31;
  wire axi_register_if_i_n_32;
  wire axi_register_if_i_n_33;
  wire axi_register_if_i_n_34;
  wire axi_register_if_i_n_35;
  wire axi_register_if_i_n_36;
  wire axi_register_if_i_n_37;
  wire axi_register_if_i_n_38;
  wire axi_register_if_i_n_39;
  wire axi_register_if_i_n_40;
  wire axi_register_if_i_n_41;
  wire axi_register_if_i_n_42;
  wire axi_register_if_i_n_43;
  wire axi_register_if_i_n_44;
  wire axi_register_if_i_n_45;
  wire axi_register_if_i_n_46;
  wire axi_register_if_i_n_47;
  wire axi_register_if_i_n_48;
  wire axi_register_if_i_n_49;
  wire axi_register_if_i_n_50;
  wire axi_register_if_i_n_51;
  wire axi_register_if_i_n_52;
  wire axi_register_if_i_n_57;
  wire axi_register_if_i_n_58;
  wire axi_register_if_i_n_59;
  wire axi_register_if_i_n_60;
  wire axi_register_if_i_n_61;
  wire axi_register_if_i_n_62;
  wire axi_register_if_i_n_63;
  wire axi_register_if_i_n_64;
  wire axi_register_if_i_n_65;
  wire axi_register_if_i_n_66;
  wire axi_register_if_i_n_67;
  wire axi_register_if_i_n_68;
  wire chan_rx_axi_map_wready;
  wire [0:0]chan_rx_slv_rdata;
  wire chan_rx_slv_rden;
  wire chan_tx_axi_map_wready;
  wire [3:0]chan_tx_slv_rdata;
  wire chan_tx_slv_rden;
  wire data_in;
  wire data_out;
  wire [15:0]data_sync_reg1;
  wire [4:0]data_sync_reg1_0;
  wire [4:0]data_sync_reg1_1;
  wire data_sync_reg_gsr;
  wire [0:0]data_sync_reg_gsr_0;
  wire data_sync_reg_gsr_1;
  wire data_sync_reg_gsr_2;
  wire data_sync_reg_gsr_3;
  wire drp_int_addr;
  wire [15:12]drp_read_data;
  wire drp_reset;
  wire drp_write_data;
  wire drpclk;
  wire gt0_cpllpd_in;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire [2:0]gt0_loopback_in;
  wire gt0_rxdfelpmreset_in;
  wire gt0_rxlpmen_in;
  wire [1:0]gt0_rxpd_in;
  wire gt0_rxpolarity_in;
  wire [1:0]gt0_rxsysclksel_in;
  wire gt0_txinhibit_in;
  wire [1:0]gt0_txpd_in;
  wire gt0_txpolarity_in;
  wire [1:0]gt0_txsysclksel_in;
  wire [7:0]gt_interface_sel;
  wire gt_slv_rd_done;
  wire gt_slv_rden;
  wire gt_slv_wren;
  wire \i_/i_/i__carry__0_n_0 ;
  wire \i_/i_/i__carry__0_n_1 ;
  wire \i_/i_/i__carry__0_n_2 ;
  wire \i_/i_/i__carry__0_n_3 ;
  wire \i_/i_/i__carry__0_n_4 ;
  wire \i_/i_/i__carry__0_n_5 ;
  wire \i_/i_/i__carry__0_n_6 ;
  wire \i_/i_/i__carry__0_n_7 ;
  wire \i_/i_/i__carry__1_n_0 ;
  wire \i_/i_/i__carry__1_n_1 ;
  wire \i_/i_/i__carry__1_n_2 ;
  wire \i_/i_/i__carry__1_n_3 ;
  wire \i_/i_/i__carry__1_n_4 ;
  wire \i_/i_/i__carry__1_n_5 ;
  wire \i_/i_/i__carry__1_n_6 ;
  wire \i_/i_/i__carry__1_n_7 ;
  wire \i_/i_/i__carry__2_n_7 ;
  wire \i_/i_/i__carry_n_0 ;
  wire \i_/i_/i__carry_n_1 ;
  wire \i_/i_/i__carry_n_2 ;
  wire \i_/i_/i__carry_n_3 ;
  wire \i_/i_/i__carry_n_4 ;
  wire \i_/i_/i__carry_n_5 ;
  wire \i_/i_/i__carry_n_6 ;
  wire \i_/i_/i__carry_n_7 ;
  wire p_0_in;
  wire rx_core_clk;
  wire rx_sys_reset;
  wire rx_sys_reset_axi;
  wire s_axi_aclk;
  wire [9:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [9:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [0:0]s_axi_bresp;
  wire s_axi_bvalid;
  wire [24:0]s_axi_rdata;
  wire s_axi_rready;
  wire [0:0]s_axi_rresp;
  wire s_axi_rvalid;
  wire [17:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire [6:2]slv_addr;
  wire [3:0]\slv_rdata_reg[3] ;
  wire slv_rden_r;
  wire slv_rden_r_0;
  wire slv_wren_clk2;
  wire slv_wren_done_pulse;
  wire slv_wren_done_pulse_1;
  wire timeout_enable;
  wire timeout_length;
  wire [11:0]timeout_value;
  wire tx_core_clk;
  wire tx_sys_reset;
  wire tx_sys_reset_axi;
  wire wait_for_drp;
  wire [3:0]\NLW_i_/i_/i__carry__2_CO_UNCONNECTED ;
  wire [3:1]\NLW_i_/i_/i__carry__2_O_UNCONNECTED ;

  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i
       (.D(D),
        .E(drp_int_addr),
        .Q(gt_interface_sel[0]),
        .access_type5_out(access_type5_out),
        .\axi_bresp_reg[1] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_29),
        .\axi_rdata_reg[0] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_47),
        .\axi_rdata_reg[0]_0 (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_48),
        .\axi_rdata_reg[10] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_24),
        .\axi_rdata_reg[11] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_3),
        .\axi_rdata_reg[15] (drp_read_data),
        .\axi_rdata_reg[1] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_46),
        .\axi_rdata_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_26),
        .\axi_rdata_reg[2]_0 (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_45),
        .\axi_rdata_reg[3] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_44),
        .\axi_rdata_reg[4] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_43),
        .\axi_rdata_reg[5] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_42),
        .\axi_rdata_reg[6] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_41),
        .\axi_rdata_reg[7] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_40),
        .\axi_rdata_reg[8] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_30),
        .\axi_rdata_reg[8]_0 ({Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_49,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_50,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_51,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_52,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_53,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_54,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_55}),
        .\axi_rdata_reg[9] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_25),
        .data_out(data_out),
        .data_sync_reg1(data_sync_reg1),
        .data_sync_reg1_0(Q),
        .data_sync_reg_gsr(data_sync_reg_gsr_0),
        .drp_reset(drp_reset),
        .drp_reset_reg_0(axi_register_if_i_n_27),
        .drpclk(drpclk),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drprdy_out(gt0_drprdy_out),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt_axi_map_wready_reg(axi_register_if_i_n_29),
        .gt_axi_map_wready_reg_0(drp_write_data),
        .gt_axi_map_wready_reg_1(timeout_length),
        .gt_slv_rd_done(gt_slv_rd_done),
        .gt_slv_rden(gt_slv_rden),
        .gt_slv_wren(gt_slv_wren),
        .p_0_in(p_0_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_wdata(s_axi_wdata),
        .\slv_addr_reg[4] (slv_addr[4:2]),
        .wait_for_drp(wait_for_drp));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i
       (.E(axi_register_if_i_n_58),
        .Q(gt_interface_sel),
        .\axi_rdata_reg[0] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_16),
        .\axi_rdata_reg[1] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_11),
        .\axi_rdata_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_12),
        .\axi_rdata_reg[2]_0 (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_13),
        .\axi_rdata_reg[7] ({Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_29,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_30,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_31,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_32,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_33,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_34,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_35}),
        .\loopback_0_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_15),
        .p_0_in(p_0_in),
        .\rx_pd_0_reg[1] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_14),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_wdata(s_axi_wdata[11:0]),
        .\slv_addr_reg[2] (axi_register_if_i_n_26),
        .\slv_addr_reg[2]_0 (axi_register_if_i_n_17),
        .\slv_addr_reg[2]_1 (axi_register_if_i_n_57),
        .\slv_addr_reg[5] (axi_register_if_i_n_66),
        .\slv_addr_reg[6] ({slv_addr[6:4],slv_addr[2]}),
        .\slv_rd_addr_reg[1] (axi_register_if_i_n_24),
        .timeout_enable(timeout_enable),
        .\timeout_timer_count_reg[11] (timeout_value),
        .\txpllclksel_reg[1] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1),
        .\txpllclksel_reg[1]_0 (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_10));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i
       (.E(axi_register_if_i_n_60),
        .Q(slv_addr[3:2]),
        .\arststages_ff_reg[4] (\arststages_ff_reg[4] ),
        .\axi_rdata_reg[0] (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_28),
        .\axi_rdata_reg[0]_0 (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_29),
        .\axi_rdata_reg[1] (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_12),
        .\axi_rdata_reg[1]_0 (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_27),
        .\axi_rdata_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_24),
        .\axi_rdata_reg[3] (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_13),
        .chan_async_axi_map_wready_reg(axi_register_if_i_n_30),
        .chan_async_axi_map_wready_reg_0(axi_register_if_i_n_40),
        .chan_async_axi_map_wready_reg_1(axi_register_if_i_n_41),
        .chan_async_axi_map_wready_reg_2(axi_register_if_i_n_42),
        .data_in(data_in),
        .data_sync_reg1(data_sync_reg1_1),
        .data_sync_reg1_0(data_sync_reg1_0),
        .gt0_cpllpd_in(gt0_cpllpd_in),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxpd_in(gt0_rxpd_in),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_in),
        .gt0_txsysclksel_in(gt0_txsysclksel_in),
        .p_0_in(p_0_in),
        .rx_sys_reset(rx_sys_reset),
        .rx_sys_reset_axi(rx_sys_reset_axi),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_wdata(s_axi_wdata[4:0]),
        .\slv_addr_reg[2] (axi_register_if_i_n_32),
        .\slv_addr_reg[2]_0 (axi_register_if_i_n_33),
        .\slv_addr_reg[2]_1 (axi_register_if_i_n_34),
        .\slv_addr_reg[2]_2 (axi_register_if_i_n_35),
        .\slv_addr_reg[2]_3 (axi_register_if_i_n_36),
        .\slv_addr_reg[2]_4 (axi_register_if_i_n_37),
        .\slv_addr_reg[2]_5 (axi_register_if_i_n_38),
        .\slv_addr_reg[2]_6 (axi_register_if_i_n_39),
        .\slv_addr_reg[4] (axi_register_if_i_n_67),
        .tx_sys_reset(tx_sys_reset),
        .tx_sys_reset_axi(tx_sys_reset_axi));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx_i
       (.Q(slv_addr[3:2]),
        .\axi_bresp_reg[1] (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx_i_n_6),
        .chan_rx_axi_map_wready(chan_rx_axi_map_wready),
        .chan_rx_slv_rdata(chan_rx_slv_rdata),
        .chan_rx_slv_rden(chan_rx_slv_rden),
        .data_sync_reg_gsr(data_sync_reg_gsr),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .\gt_interface_sel_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1),
        .p_0_in(p_0_in),
        .rx_core_clk(rx_core_clk),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_wdata(s_axi_wdata[0]),
        .s_axi_wvalid(s_axi_wvalid),
        .\slv_addr_reg[3] (axi_register_if_i_n_25),
        .\slv_addr_reg[3]_0 (axi_register_if_i_n_31),
        .\slv_addr_reg[3]_1 (axi_register_if_i_n_61),
        .slv_rden_r(slv_rden_r),
        .slv_rden_r_0(slv_rden_r_0),
        .slv_wren_done_pulse(slv_wren_done_pulse),
        .slv_wren_done_pulse_1(slv_wren_done_pulse_1));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_tx Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_tx_i
       (.D({axi_register_if_i_n_63,axi_register_if_i_n_64}),
        .E(axi_register_if_i_n_68),
        .Q(slv_addr[3:2]),
        .SR(SR),
        .\axi_rdata_reg[3] (chan_tx_slv_rdata),
        .chan_tx_axi_map_wready(chan_tx_axi_map_wready),
        .chan_tx_slv_rden(chan_tx_slv_rden),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txpd_in(gt0_txpd_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .\gt_interface_sel_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1),
        .p_0_in(p_0_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_wdata(s_axi_wdata[3:0]),
        .s_axi_wvalid(s_axi_wvalid),
        .\slv_addr_reg[2] (axi_register_if_i_n_62),
        .\slv_addr_reg[3] (axi_register_if_i_n_61),
        .\slv_addr_reg[3]_0 (axi_register_if_i_n_59),
        .\slv_addr_reg[3]_1 (axi_register_if_i_n_23),
        .\slv_addr_reg[4] (axi_register_if_i_n_65),
        .\slv_addr_reg[5] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_14),
        .\slv_rdata_reg[3]_0 (\slv_rdata_reg[3] ),
        .slv_rden_r(slv_rden_r_0),
        .slv_wren_clk2(slv_wren_clk2),
        .slv_wren_done_pulse(slv_wren_done_pulse_1),
        .tx_core_clk(tx_core_clk));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_axi axi_register_if_i
       (.D({axi_register_if_i_n_63,axi_register_if_i_n_64}),
        .DI(axi_register_if_i_n_52),
        .E(drp_int_addr),
        .O({\i_/i_/i__carry_n_4 ,\i_/i_/i__carry_n_5 ,\i_/i_/i__carry_n_6 ,\i_/i_/i__carry_n_7 }),
        .Q(slv_addr),
        .S({axi_register_if_i_n_13,axi_register_if_i_n_14,axi_register_if_i_n_15,axi_register_if_i_n_16}),
        .access_type5_out(access_type5_out),
        .\axi_rdata_reg[7]_0 (axi_register_if_i_n_66),
        .chan_async_axi_map_wready_reg_0(axi_register_if_i_n_24),
        .chan_rx_axi_map_wready(chan_rx_axi_map_wready),
        .chan_rx_slv_rdata(chan_rx_slv_rdata),
        .chan_rx_slv_rden(chan_rx_slv_rden),
        .chan_tx_axi_map_wready(chan_tx_axi_map_wready),
        .chan_tx_slv_rden(chan_tx_slv_rden),
        .clk1_ready_pulse_reg(Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_29),
        .\cmm_interface_sel_reg[0] (axi_register_if_i_n_17),
        .\cmm_interface_sel_reg[7] ({Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_29,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_30,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_31,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_32,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_33,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_34,Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_35}),
        .cpll_pd_0_reg(axi_register_if_i_n_30),
        .cpll_pd_0_reg_0(axi_register_if_i_n_31),
        .data_sync_reg_gsr(data_sync_reg_gsr_1),
        .data_sync_reg_gsr_0(data_sync_reg_gsr_2),
        .data_sync_reg_gsr_1(data_sync_reg_gsr_3),
        .\drp_int_addr_reg[1] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_46),
        .\drp_int_addr_reg[3] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_44),
        .\drp_int_addr_reg[4] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_43),
        .\drp_read_data_reg[0] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_47),
        .\drp_read_data_reg[15] (drp_read_data),
        .\drp_read_data_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_45),
        .\drp_read_data_reg[5] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_42),
        .\drp_read_data_reg[6] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_41),
        .\drp_read_data_reg[7] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_40),
        .\drp_read_data_reg[8] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_30),
        .drp_reset(drp_reset),
        .drp_reset_reg(axi_register_if_i_n_27),
        .\drp_write_data_reg[10] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_24),
        .\drp_write_data_reg[11] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_3),
        .\drp_write_data_reg[15] (drp_write_data),
        .\drp_write_data_reg[15]_0 (data_sync_reg1[15:12]),
        .gt0_cpllpd_in(gt0_cpllpd_in),
        .gt0_loopback_in(gt0_loopback_in),
        .gt0_rxpd_in(gt0_rxpd_in),
        .gt0_rxsysclksel_in(gt0_rxsysclksel_in),
        .gt0_txsysclksel_in(gt0_txsysclksel_in),
        .\gt_interface_sel_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_1),
        .\gt_interface_sel_reg[2]_0 (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_13),
        .\gt_interface_sel_reg[4] (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_10),
        .\gt_interface_sel_reg[7] (axi_register_if_i_n_57),
        .\gt_interface_sel_reg[7]_0 (gt_interface_sel[7:1]),
        .gt_slv_rd_done(gt_slv_rd_done),
        .gt_slv_rden(gt_slv_rden),
        .gt_slv_wren(gt_slv_wren),
        .\loopback_0_reg[0] (axi_register_if_i_n_42),
        .\loopback_0_reg[0]_0 (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_28),
        .\loopback_0_reg[1] (axi_register_if_i_n_41),
        .\loopback_0_reg[1]_0 (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_27),
        .\loopback_0_reg[2] (axi_register_if_i_n_40),
        .p_0_in(p_0_in),
        .\rx_pd_0_reg[0] (axi_register_if_i_n_35),
        .\rx_pd_0_reg[1] (axi_register_if_i_n_23),
        .\rx_pd_0_reg[1]_0 (axi_register_if_i_n_34),
        .\rx_pd_0_reg[1]_1 (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_12),
        .rx_sys_reset_axi(rx_sys_reset_axi),
        .rx_sys_reset_axi_reg(axi_register_if_i_n_33),
        .\rxpllclksel_reg[0] (axi_register_if_i_n_39),
        .\rxpllclksel_reg[1] (axi_register_if_i_n_38),
        .rxpolarity_0_reg(axi_register_if_i_n_25),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bresp(s_axi_bresp),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rresp(s_axi_rresp),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata({s_axi_wdata[17:16],s_axi_wdata[2:0]}),
        .s_axi_wready(s_axi_wready),
        .s_axi_wvalid(s_axi_wvalid),
        .\slv_addr_reg[5]_0 (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_14),
        .\slv_addr_reg[5]_1 (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_12),
        .\slv_addr_reg[6]_0 (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_15),
        .\slv_rd_addr_reg[1]_0 (Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_11),
        .\slv_rdata_reg[0] (axi_register_if_i_n_62),
        .\slv_rdata_reg[3] (chan_tx_slv_rdata),
        .slv_rden_r(slv_rden_r_0),
        .slv_rden_r_1(slv_rden_r),
        .slv_rden_r_reg(Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx_i_n_6),
        .\slv_wdata_r_internal_reg[0] (axi_register_if_i_n_68),
        .slv_wren_clk2(slv_wren_clk2),
        .slv_wren_done_pulse(slv_wren_done_pulse_1),
        .slv_wren_done_pulse_0(slv_wren_done_pulse),
        .timeout_enable(timeout_enable),
        .timeout_enable_reg(axi_register_if_i_n_26),
        .timeout_enable_reg_0(Jesd204_microblaze_jesd204_phy_0_0_phyAxiConfig_i_n_16),
        .\timeout_length_reg[11] (timeout_length),
        .\timeout_length_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_26),
        .\timeout_length_reg[8] ({Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_49,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_50,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_51,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_52,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_53,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_54,Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_55}),
        .\timeout_length_reg[9] (Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_25),
        .\timeout_timer_count_reg[11]_0 ({axi_register_if_i_n_44,axi_register_if_i_n_45,axi_register_if_i_n_46,axi_register_if_i_n_47}),
        .\timeout_timer_count_reg[11]_1 ({\i_/i_/i__carry__1_n_4 ,\i_/i_/i__carry__1_n_5 ,\i_/i_/i__carry__1_n_6 ,\i_/i_/i__carry__1_n_7 }),
        .\timeout_timer_count_reg[12]_0 (axi_register_if_i_n_43),
        .\timeout_timer_count_reg[12]_1 (\i_/i_/i__carry__2_n_7 ),
        .\timeout_timer_count_reg[7]_0 ({axi_register_if_i_n_48,axi_register_if_i_n_49,axi_register_if_i_n_50,axi_register_if_i_n_51}),
        .\timeout_value_reg[11] (axi_register_if_i_n_58),
        .\timeout_value_reg[11]_0 (timeout_value),
        .\timeout_value_reg[7] ({\i_/i_/i__carry__0_n_4 ,\i_/i_/i__carry__0_n_5 ,\i_/i_/i__carry__0_n_6 ,\i_/i_/i__carry__0_n_7 }),
        .tx_sys_reset_axi(tx_sys_reset_axi),
        .tx_sys_reset_axi_reg(axi_register_if_i_n_32),
        .\txdiffctrl_0_reg[3] (axi_register_if_i_n_65),
        .\txdiffctrl_0_reg[3]_0 (\slv_rdata_reg[3] [3:2]),
        .txinihibit_0_reg(axi_register_if_i_n_61),
        .\txpllclksel_reg[0] (axi_register_if_i_n_37),
        .\txpllclksel_reg[0]_0 (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_29),
        .\txpllclksel_reg[1] (axi_register_if_i_n_36),
        .txpolarity_0_reg(axi_register_if_i_n_59),
        .\txpostcursor_0_reg[2] (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_24),
        .\txpostcursor_0_reg[4] (axi_register_if_i_n_60),
        .\txpostcursor_0_reg[4]_0 (data_sync_reg1_0[4]),
        .\txprecursor_0_reg[3] (Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async_i_n_13),
        .\txprecursor_0_reg[4] (axi_register_if_i_n_67),
        .\txprecursor_0_reg[4]_0 (data_sync_reg1_1[4]),
        .wait_for_drp(wait_for_drp),
        .wait_for_drp_reg(axi_register_if_i_n_29),
        .wait_for_drp_reg_0(Jesd204_microblaze_jesd204_phy_0_0_drpChannelMailbox_i_n_48));
  CARRY4 \i_/i_/i__carry 
       (.CI(1'b0),
        .CO({\i_/i_/i__carry_n_0 ,\i_/i_/i__carry_n_1 ,\i_/i_/i__carry_n_2 ,\i_/i_/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,axi_register_if_i_n_52}),
        .O({\i_/i_/i__carry_n_4 ,\i_/i_/i__carry_n_5 ,\i_/i_/i__carry_n_6 ,\i_/i_/i__carry_n_7 }),
        .S({axi_register_if_i_n_13,axi_register_if_i_n_14,axi_register_if_i_n_15,axi_register_if_i_n_16}));
  CARRY4 \i_/i_/i__carry__0 
       (.CI(\i_/i_/i__carry_n_0 ),
        .CO({\i_/i_/i__carry__0_n_0 ,\i_/i_/i__carry__0_n_1 ,\i_/i_/i__carry__0_n_2 ,\i_/i_/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\i_/i_/i__carry__0_n_4 ,\i_/i_/i__carry__0_n_5 ,\i_/i_/i__carry__0_n_6 ,\i_/i_/i__carry__0_n_7 }),
        .S({axi_register_if_i_n_48,axi_register_if_i_n_49,axi_register_if_i_n_50,axi_register_if_i_n_51}));
  CARRY4 \i_/i_/i__carry__1 
       (.CI(\i_/i_/i__carry__0_n_0 ),
        .CO({\i_/i_/i__carry__1_n_0 ,\i_/i_/i__carry__1_n_1 ,\i_/i_/i__carry__1_n_2 ,\i_/i_/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\i_/i_/i__carry__1_n_4 ,\i_/i_/i__carry__1_n_5 ,\i_/i_/i__carry__1_n_6 ,\i_/i_/i__carry__1_n_7 }),
        .S({axi_register_if_i_n_44,axi_register_if_i_n_45,axi_register_if_i_n_46,axi_register_if_i_n_47}));
  CARRY4 \i_/i_/i__carry__2 
       (.CI(\i_/i_/i__carry__1_n_0 ),
        .CO(\NLW_i_/i_/i__carry__2_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_i_/i_/i__carry__2_O_UNCONNECTED [3:1],\i_/i_/i__carry__2_n_7 }),
        .S({1'b0,1'b0,1'b0,axi_register_if_i_n_43}));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_axi
   (chan_tx_axi_map_wready,
    p_0_in,
    chan_rx_axi_map_wready,
    s_axi_wready,
    s_axi_arready,
    gt_slv_rden,
    chan_tx_slv_rden,
    chan_rx_slv_rden,
    s_axi_rvalid,
    s_axi_bvalid,
    s_axi_awready,
    s_axi_bresp,
    s_axi_rresp,
    S,
    \cmm_interface_sel_reg[0] ,
    Q,
    \rx_pd_0_reg[1] ,
    chan_async_axi_map_wready_reg_0,
    rxpolarity_0_reg,
    timeout_enable_reg,
    drp_reset_reg,
    gt_slv_wren,
    wait_for_drp_reg,
    cpll_pd_0_reg,
    cpll_pd_0_reg_0,
    tx_sys_reset_axi_reg,
    rx_sys_reset_axi_reg,
    \rx_pd_0_reg[1]_0 ,
    \rx_pd_0_reg[0] ,
    \txpllclksel_reg[1] ,
    \txpllclksel_reg[0] ,
    \rxpllclksel_reg[1] ,
    \rxpllclksel_reg[0] ,
    \loopback_0_reg[2] ,
    \loopback_0_reg[1] ,
    \loopback_0_reg[0] ,
    \timeout_timer_count_reg[12]_0 ,
    \timeout_timer_count_reg[11]_0 ,
    \timeout_timer_count_reg[7]_0 ,
    DI,
    access_type5_out,
    E,
    \timeout_length_reg[11] ,
    \drp_write_data_reg[15] ,
    \gt_interface_sel_reg[7] ,
    \timeout_value_reg[11] ,
    txpolarity_0_reg,
    \txpostcursor_0_reg[4] ,
    txinihibit_0_reg,
    \slv_rdata_reg[0] ,
    D,
    \txdiffctrl_0_reg[3] ,
    \axi_rdata_reg[7]_0 ,
    \txprecursor_0_reg[4] ,
    \slv_wdata_r_internal_reg[0] ,
    s_axi_rdata,
    s_axi_aclk,
    O,
    \timeout_value_reg[7] ,
    \timeout_timer_count_reg[11]_1 ,
    \timeout_timer_count_reg[12]_1 ,
    \timeout_value_reg[11]_0 ,
    s_axi_arvalid,
    s_axi_aresetn,
    s_axi_wvalid,
    \drp_int_addr_reg[1] ,
    \timeout_length_reg[8] ,
    \rx_pd_0_reg[1]_1 ,
    \loopback_0_reg[1]_0 ,
    \cmm_interface_sel_reg[7] ,
    s_axi_rready,
    s_axi_bready,
    s_axi_awvalid,
    s_axi_wdata,
    timeout_enable,
    drp_reset,
    wait_for_drp,
    gt0_cpllpd_in,
    \slv_addr_reg[6]_0 ,
    tx_sys_reset_axi,
    rx_sys_reset_axi,
    \slv_addr_reg[5]_0 ,
    gt0_rxpd_in,
    \gt_interface_sel_reg[2] ,
    gt0_txsysclksel_in,
    gt0_rxsysclksel_in,
    gt0_loopback_in,
    s_axi_araddr,
    s_axi_awaddr,
    gt_slv_rd_done,
    slv_wren_done_pulse,
    slv_rden_r,
    \slv_rdata_reg[3] ,
    \slv_rd_addr_reg[1]_0 ,
    slv_wren_done_pulse_0,
    slv_rden_r_1,
    chan_rx_slv_rdata,
    wait_for_drp_reg_0,
    \drp_read_data_reg[0] ,
    \txpostcursor_0_reg[2] ,
    \slv_addr_reg[5]_1 ,
    slv_rden_r_reg,
    clk1_ready_pulse_reg,
    \drp_write_data_reg[10] ,
    \gt_interface_sel_reg[4] ,
    \gt_interface_sel_reg[2]_0 ,
    \gt_interface_sel_reg[7]_0 ,
    \timeout_length_reg[9] ,
    \drp_int_addr_reg[3] ,
    \timeout_length_reg[2] ,
    \drp_read_data_reg[2] ,
    \drp_write_data_reg[11] ,
    \drp_int_addr_reg[4] ,
    \drp_read_data_reg[7] ,
    \drp_read_data_reg[6] ,
    \drp_read_data_reg[5] ,
    timeout_enable_reg_0,
    data_sync_reg_gsr,
    \drp_write_data_reg[15]_0 ,
    \drp_read_data_reg[15] ,
    \txpllclksel_reg[0]_0 ,
    \loopback_0_reg[0]_0 ,
    \txprecursor_0_reg[4]_0 ,
    \txpostcursor_0_reg[4]_0 ,
    data_sync_reg_gsr_0,
    \txdiffctrl_0_reg[3]_0 ,
    slv_wren_clk2,
    \txprecursor_0_reg[3] ,
    \drp_read_data_reg[8] ,
    data_sync_reg_gsr_1);
  output chan_tx_axi_map_wready;
  output p_0_in;
  output chan_rx_axi_map_wready;
  output s_axi_wready;
  output s_axi_arready;
  output gt_slv_rden;
  output chan_tx_slv_rden;
  output chan_rx_slv_rden;
  output s_axi_rvalid;
  output s_axi_bvalid;
  output s_axi_awready;
  output [0:0]s_axi_bresp;
  output [0:0]s_axi_rresp;
  output [3:0]S;
  output [0:0]\cmm_interface_sel_reg[0] ;
  output [4:0]Q;
  output \rx_pd_0_reg[1] ;
  output [0:0]chan_async_axi_map_wready_reg_0;
  output rxpolarity_0_reg;
  output timeout_enable_reg;
  output drp_reset_reg;
  output gt_slv_wren;
  output wait_for_drp_reg;
  output cpll_pd_0_reg;
  output cpll_pd_0_reg_0;
  output tx_sys_reset_axi_reg;
  output rx_sys_reset_axi_reg;
  output \rx_pd_0_reg[1]_0 ;
  output \rx_pd_0_reg[0] ;
  output \txpllclksel_reg[1] ;
  output \txpllclksel_reg[0] ;
  output \rxpllclksel_reg[1] ;
  output \rxpllclksel_reg[0] ;
  output \loopback_0_reg[2] ;
  output \loopback_0_reg[1] ;
  output \loopback_0_reg[0] ;
  output [0:0]\timeout_timer_count_reg[12]_0 ;
  output [3:0]\timeout_timer_count_reg[11]_0 ;
  output [3:0]\timeout_timer_count_reg[7]_0 ;
  output [0:0]DI;
  output access_type5_out;
  output [0:0]E;
  output [0:0]\timeout_length_reg[11] ;
  output [0:0]\drp_write_data_reg[15] ;
  output [0:0]\gt_interface_sel_reg[7] ;
  output [0:0]\timeout_value_reg[11] ;
  output txpolarity_0_reg;
  output [0:0]\txpostcursor_0_reg[4] ;
  output txinihibit_0_reg;
  output \slv_rdata_reg[0] ;
  output [1:0]D;
  output [0:0]\txdiffctrl_0_reg[3] ;
  output \axi_rdata_reg[7]_0 ;
  output [0:0]\txprecursor_0_reg[4] ;
  output [0:0]\slv_wdata_r_internal_reg[0] ;
  output [24:0]s_axi_rdata;
  input s_axi_aclk;
  input [3:0]O;
  input [3:0]\timeout_value_reg[7] ;
  input [3:0]\timeout_timer_count_reg[11]_1 ;
  input [0:0]\timeout_timer_count_reg[12]_1 ;
  input [11:0]\timeout_value_reg[11]_0 ;
  input s_axi_arvalid;
  input s_axi_aresetn;
  input s_axi_wvalid;
  input \drp_int_addr_reg[1] ;
  input [6:0]\timeout_length_reg[8] ;
  input \rx_pd_0_reg[1]_1 ;
  input \loopback_0_reg[1]_0 ;
  input [6:0]\cmm_interface_sel_reg[7] ;
  input s_axi_rready;
  input s_axi_bready;
  input s_axi_awvalid;
  input [4:0]s_axi_wdata;
  input timeout_enable;
  input drp_reset;
  input wait_for_drp;
  input gt0_cpllpd_in;
  input \slv_addr_reg[6]_0 ;
  input tx_sys_reset_axi;
  input rx_sys_reset_axi;
  input \slv_addr_reg[5]_0 ;
  input [1:0]gt0_rxpd_in;
  input \gt_interface_sel_reg[2] ;
  input [1:0]gt0_txsysclksel_in;
  input [1:0]gt0_rxsysclksel_in;
  input [2:0]gt0_loopback_in;
  input [9:0]s_axi_araddr;
  input [9:0]s_axi_awaddr;
  input gt_slv_rd_done;
  input slv_wren_done_pulse;
  input slv_rden_r;
  input [3:0]\slv_rdata_reg[3] ;
  input \slv_rd_addr_reg[1]_0 ;
  input slv_wren_done_pulse_0;
  input slv_rden_r_1;
  input [0:0]chan_rx_slv_rdata;
  input wait_for_drp_reg_0;
  input \drp_read_data_reg[0] ;
  input \txpostcursor_0_reg[2] ;
  input \slv_addr_reg[5]_1 ;
  input slv_rden_r_reg;
  input clk1_ready_pulse_reg;
  input \drp_write_data_reg[10] ;
  input \gt_interface_sel_reg[4] ;
  input \gt_interface_sel_reg[2]_0 ;
  input [6:0]\gt_interface_sel_reg[7]_0 ;
  input \timeout_length_reg[9] ;
  input \drp_int_addr_reg[3] ;
  input \timeout_length_reg[2] ;
  input \drp_read_data_reg[2] ;
  input \drp_write_data_reg[11] ;
  input \drp_int_addr_reg[4] ;
  input \drp_read_data_reg[7] ;
  input \drp_read_data_reg[6] ;
  input \drp_read_data_reg[5] ;
  input timeout_enable_reg_0;
  input data_sync_reg_gsr;
  input [3:0]\drp_write_data_reg[15]_0 ;
  input [3:0]\drp_read_data_reg[15] ;
  input \txpllclksel_reg[0]_0 ;
  input \loopback_0_reg[0]_0 ;
  input [0:0]\txprecursor_0_reg[4]_0 ;
  input [0:0]\txpostcursor_0_reg[4]_0 ;
  input data_sync_reg_gsr_0;
  input [1:0]\txdiffctrl_0_reg[3]_0 ;
  input slv_wren_clk2;
  input \txprecursor_0_reg[3] ;
  input \drp_read_data_reg[8] ;
  input data_sync_reg_gsr_1;

  wire [1:0]D;
  wire [0:0]DI;
  wire [0:0]E;
  wire [3:0]O;
  wire [4:0]Q;
  wire [3:0]S;
  wire access_type5_out;
  wire axi_arready0;
  wire axi_awready0;
  wire axi_awready19_in;
  wire axi_awready_i_2_n_0;
  wire \axi_bresp[1]_i_1_n_0 ;
  wire \axi_bresp[1]_i_3_n_0 ;
  wire \axi_bresp[1]_i_6_n_0 ;
  wire \axi_bresp[1]_i_7_n_0 ;
  wire axi_bvalid0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_10_n_0 ;
  wire \axi_rdata[0]_i_13_n_0 ;
  wire \axi_rdata[0]_i_14_n_0 ;
  wire \axi_rdata[0]_i_16_n_0 ;
  wire \axi_rdata[0]_i_1_n_0 ;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[0]_i_5_n_0 ;
  wire \axi_rdata[0]_i_8_n_0 ;
  wire \axi_rdata[0]_i_9_n_0 ;
  wire \axi_rdata[10]_i_1_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[11]_i_1_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[11]_i_5_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[12]_i_1_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_1_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[13]_i_5_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[14]_i_1_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_1_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[15]_i_5_n_0 ;
  wire \axi_rdata[16]_i_1_n_0 ;
  wire \axi_rdata[17]_i_1_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_1_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_1_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_1_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_5_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[1]_i_8_n_0 ;
  wire \axi_rdata[1]_i_9_n_0 ;
  wire \axi_rdata[20]_i_1_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_1_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_1_n_0 ;
  wire \axi_rdata[24]_i_1_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_1_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[2]_i_10_n_0 ;
  wire \axi_rdata[2]_i_1_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[2]_i_8_n_0 ;
  wire \axi_rdata[2]_i_9_n_0 ;
  wire \axi_rdata[3]_i_10_n_0 ;
  wire \axi_rdata[3]_i_11_n_0 ;
  wire \axi_rdata[3]_i_12_n_0 ;
  wire \axi_rdata[3]_i_1_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[3]_i_5_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_8_n_0 ;
  wire \axi_rdata[3]_i_9_n_0 ;
  wire \axi_rdata[4]_i_12_n_0 ;
  wire \axi_rdata[4]_i_13_n_0 ;
  wire \axi_rdata[4]_i_14_n_0 ;
  wire \axi_rdata[4]_i_1_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[4]_i_5_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[4]_i_9_n_0 ;
  wire \axi_rdata[5]_i_1_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[5]_i_5_n_0 ;
  wire \axi_rdata[6]_i_1_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[6]_i_5_n_0 ;
  wire \axi_rdata[7]_i_1_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[7]_i_5_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[8]_i_1_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[9]_i_1_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire \axi_rdata_reg[7]_0 ;
  wire \axi_rresp[1]_i_1_n_0 ;
  wire \axi_rresp[1]_i_2_n_0 ;
  wire \axi_rresp[1]_i_3_n_0 ;
  wire \axi_rresp[1]_i_4_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wr_access0;
  wire chan_async_axi_map_wready;
  wire chan_async_axi_map_wready0;
  wire [0:0]chan_async_axi_map_wready_reg_0;
  wire chan_async_slv_rden;
  wire chan_async_slv_rden_i_1_n_0;
  wire chan_rx_axi_map_wready;
  wire chan_rx_axi_map_wready0;
  wire [0:0]chan_rx_slv_rdata;
  wire chan_rx_slv_rden;
  wire chan_rx_slv_rden_i_1_n_0;
  wire chan_tx_axi_map_wready;
  wire chan_tx_axi_map_wready0;
  wire chan_tx_axi_map_wready_i_2_n_0;
  wire chan_tx_slv_rden;
  wire chan_tx_slv_rden_i_1_n_0;
  wire clk1_ready_pulse_reg;
  wire \cmm_interface_sel[7]_i_2_n_0 ;
  wire [0:0]\cmm_interface_sel_reg[0] ;
  wire [6:0]\cmm_interface_sel_reg[7] ;
  wire cpll_pd_0_reg;
  wire cpll_pd_0_reg_0;
  wire data_sync_reg_gsr;
  wire data_sync_reg_gsr_0;
  wire data_sync_reg_gsr_1;
  wire drp_access_in_progress_i_4_n_0;
  wire \drp_int_addr_reg[1] ;
  wire \drp_int_addr_reg[3] ;
  wire \drp_int_addr_reg[4] ;
  wire \drp_read_data_reg[0] ;
  wire [3:0]\drp_read_data_reg[15] ;
  wire \drp_read_data_reg[2] ;
  wire \drp_read_data_reg[5] ;
  wire \drp_read_data_reg[6] ;
  wire \drp_read_data_reg[7] ;
  wire \drp_read_data_reg[8] ;
  wire drp_reset;
  wire drp_reset_i_2_n_0;
  wire drp_reset_reg;
  wire \drp_write_data_reg[10] ;
  wire \drp_write_data_reg[11] ;
  wire [0:0]\drp_write_data_reg[15] ;
  wire [3:0]\drp_write_data_reg[15]_0 ;
  wire gt0_cpllpd_in;
  wire [2:0]gt0_loopback_in;
  wire [1:0]gt0_rxpd_in;
  wire [1:0]gt0_rxsysclksel_in;
  wire [1:0]gt0_txsysclksel_in;
  wire gt_axi_map_wready;
  wire gt_axi_map_wready0;
  wire \gt_interface_sel_reg[2] ;
  wire \gt_interface_sel_reg[2]_0 ;
  wire \gt_interface_sel_reg[4] ;
  wire [0:0]\gt_interface_sel_reg[7] ;
  wire [6:0]\gt_interface_sel_reg[7]_0 ;
  wire gt_slv_rd_done;
  wire gt_slv_rden;
  wire gt_slv_rden_i_1_n_0;
  wire gt_slv_wren;
  wire load_timeout_timer0;
  wire \loopback_0[2]_i_2_n_0 ;
  wire \loopback_0_reg[0] ;
  wire \loopback_0_reg[0]_0 ;
  wire \loopback_0_reg[1] ;
  wire \loopback_0_reg[1]_0 ;
  wire \loopback_0_reg[2] ;
  wire p_0_in;
  wire phy1_axi_map_wready;
  wire phy1_axi_map_wready0;
  wire phy1_slv_rden;
  wire phy1_slv_rden_i_1_n_0;
  wire phy1_slv_rden_i_3_n_0;
  wire read_in_progress;
  wire read_in_progress_i_1_n_0;
  wire read_in_progress_i_2_n_0;
  wire \rx_pd_0[1]_i_3_n_0 ;
  wire \rx_pd_0_reg[0] ;
  wire \rx_pd_0_reg[1] ;
  wire \rx_pd_0_reg[1]_0 ;
  wire \rx_pd_0_reg[1]_1 ;
  wire rx_sys_reset_axi;
  wire rx_sys_reset_axi_reg;
  wire \rxpllclksel[1]_i_2_n_0 ;
  wire \rxpllclksel_reg[0] ;
  wire \rxpllclksel_reg[1] ;
  wire rxpolarity_0_reg;
  wire s_axi_aclk;
  wire [9:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [9:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [0:0]s_axi_bresp;
  wire s_axi_bvalid;
  wire [24:0]s_axi_rdata;
  wire s_axi_rready;
  wire [0:0]s_axi_rresp;
  wire s_axi_rvalid;
  wire [4:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire [7:7]slv_addr;
  wire \slv_addr[2]_i_1_n_0 ;
  wire \slv_addr[3]_i_1_n_0 ;
  wire \slv_addr[4]_i_1_n_0 ;
  wire \slv_addr[5]_i_1_n_0 ;
  wire \slv_addr[6]_i_1_n_0 ;
  wire \slv_addr[7]_i_1_n_0 ;
  wire \slv_addr_reg[5]_0 ;
  wire \slv_addr_reg[5]_1 ;
  wire \slv_addr_reg[6]_0 ;
  wire \slv_rd_addr[0]_i_1_n_0 ;
  wire \slv_rd_addr[1]_i_1_n_0 ;
  wire \slv_rd_addr[2]_i_1_n_0 ;
  wire \slv_rd_addr[3]_i_1_n_0 ;
  wire \slv_rd_addr[3]_i_2_n_0 ;
  wire \slv_rd_addr_reg[1]_0 ;
  wire \slv_rd_addr_reg_n_0_[0] ;
  wire \slv_rd_addr_reg_n_0_[2] ;
  wire \slv_rd_addr_reg_n_0_[3] ;
  wire \slv_rdata_reg[0] ;
  wire [3:0]\slv_rdata_reg[3] ;
  wire slv_rden_r;
  wire slv_rden_r_1;
  wire slv_rden_r_reg;
  wire slv_reg_rden0;
  wire slv_reg_rden_i_1_n_0;
  wire slv_reg_rden_reg_n_0;
  wire [0:0]\slv_wdata_r_internal_reg[0] ;
  wire slv_wren_clk2;
  wire slv_wren_done_pulse;
  wire slv_wren_done_pulse_0;
  wire timeout;
  wire timeout_enable;
  wire timeout_enable_i_2_n_0;
  wire timeout_enable_reg;
  wire timeout_enable_reg_0;
  wire [0:0]\timeout_length_reg[11] ;
  wire \timeout_length_reg[2] ;
  wire [6:0]\timeout_length_reg[8] ;
  wire \timeout_length_reg[9] ;
  wire \timeout_timer_count[0]_i_1_n_0 ;
  wire \timeout_timer_count[0]_i_2_n_0 ;
  wire \timeout_timer_count[0]_i_3_n_0 ;
  wire \timeout_timer_count[0]_i_4_n_0 ;
  wire \timeout_timer_count[0]_i_5_n_0 ;
  wire \timeout_timer_count[0]_i_6_n_0 ;
  wire [3:0]\timeout_timer_count_reg[11]_0 ;
  wire [3:0]\timeout_timer_count_reg[11]_1 ;
  wire [0:0]\timeout_timer_count_reg[12]_0 ;
  wire [0:0]\timeout_timer_count_reg[12]_1 ;
  wire [3:0]\timeout_timer_count_reg[7]_0 ;
  wire \timeout_timer_count_reg_n_0_[0] ;
  wire \timeout_timer_count_reg_n_0_[10] ;
  wire \timeout_timer_count_reg_n_0_[11] ;
  wire \timeout_timer_count_reg_n_0_[1] ;
  wire \timeout_timer_count_reg_n_0_[2] ;
  wire \timeout_timer_count_reg_n_0_[3] ;
  wire \timeout_timer_count_reg_n_0_[4] ;
  wire \timeout_timer_count_reg_n_0_[5] ;
  wire \timeout_timer_count_reg_n_0_[6] ;
  wire \timeout_timer_count_reg_n_0_[7] ;
  wire \timeout_timer_count_reg_n_0_[8] ;
  wire \timeout_timer_count_reg_n_0_[9] ;
  wire \timeout_value[11]_i_2_n_0 ;
  wire [0:0]\timeout_value_reg[11] ;
  wire [11:0]\timeout_value_reg[11]_0 ;
  wire [3:0]\timeout_value_reg[7] ;
  wire tx_sys_reset_axi;
  wire tx_sys_reset_axi_reg;
  wire [0:0]\txdiffctrl_0_reg[3] ;
  wire [1:0]\txdiffctrl_0_reg[3]_0 ;
  wire txinihibit_0_reg;
  wire \txpllclksel_reg[0] ;
  wire \txpllclksel_reg[0]_0 ;
  wire \txpllclksel_reg[1] ;
  wire txpolarity_0_reg;
  wire \txpostcursor_0_reg[2] ;
  wire [0:0]\txpostcursor_0_reg[4] ;
  wire [0:0]\txpostcursor_0_reg[4]_0 ;
  wire \txprecursor_0_reg[3] ;
  wire [0:0]\txprecursor_0_reg[4] ;
  wire [0:0]\txprecursor_0_reg[4]_0 ;
  wire valid_waddr_i_1_n_0;
  wire valid_waddr_i_2_n_0;
  wire valid_waddr_i_4_n_0;
  wire valid_waddr_reg_n_0;
  wire wait_for_drp;
  wire wait_for_drp_i_2_n_0;
  wire wait_for_drp_reg;
  wire wait_for_drp_reg_0;
  wire write_in_progress;
  wire write_in_progress_i_1_n_0;
  wire write_in_progress_i_2_n_0;

  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    axi_arready_i_1
       (.I0(read_in_progress),
        .I1(s_axi_arvalid),
        .I2(write_in_progress),
        .I3(s_axi_arready),
        .O(axi_arready0));
  FDRE #(
    .INIT(1'b0)) 
    axi_arready_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(s_axi_arready),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s_axi_aresetn),
        .O(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hFFF00002)) 
    axi_awready_i_2
       (.I0(s_axi_awvalid),
        .I1(valid_waddr_reg_n_0),
        .I2(read_in_progress),
        .I3(s_axi_arvalid),
        .I4(s_axi_awready),
        .O(axi_awready_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    axi_awready_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_awready_i_2_n_0),
        .Q(s_axi_awready),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h8BBB000088880000)) 
    \axi_bresp[1]_i_1 
       (.I0(timeout),
        .I1(axi_bvalid0),
        .I2(s_axi_bready),
        .I3(s_axi_bvalid),
        .I4(s_axi_aresetn),
        .I5(s_axi_bresp),
        .O(\axi_bresp[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF00FE)) 
    \axi_bresp[1]_i_2 
       (.I0(\axi_bresp[1]_i_3_n_0 ),
        .I1(slv_rden_r_reg),
        .I2(clk1_ready_pulse_reg),
        .I3(s_axi_bvalid),
        .I4(\axi_bresp[1]_i_6_n_0 ),
        .I5(\axi_bresp[1]_i_7_n_0 ),
        .O(axi_bvalid0));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hE0)) 
    \axi_bresp[1]_i_3 
       (.I0(phy1_axi_map_wready),
        .I1(chan_async_axi_map_wready),
        .I2(s_axi_wvalid),
        .O(\axi_bresp[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \axi_bresp[1]_i_6 
       (.I0(chan_rx_axi_map_wready),
        .I1(chan_tx_axi_map_wready),
        .I2(phy1_axi_map_wready),
        .I3(gt_axi_map_wready),
        .I4(chan_async_axi_map_wready),
        .I5(s_axi_wready),
        .O(\axi_bresp[1]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \axi_bresp[1]_i_7 
       (.I0(timeout),
        .I1(write_in_progress),
        .O(\axi_bresp[1]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \axi_bresp_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\axi_bresp[1]_i_1_n_0 ),
        .Q(s_axi_bresp),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hF2)) 
    axi_bvalid_i_1
       (.I0(s_axi_bvalid),
        .I1(s_axi_bready),
        .I2(axi_bvalid0),
        .O(axi_bvalid_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    axi_bvalid_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s_axi_bvalid),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hAAAAAAAAFFFFFFAB)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(\axi_rdata[0]_i_3_n_0 ),
        .I2(\axi_rdata[4]_i_4_n_0 ),
        .I3(\axi_rdata[0]_i_4_n_0 ),
        .I4(\axi_rdata[0]_i_5_n_0 ),
        .I5(\axi_rdata[4]_i_2_n_0 ),
        .O(\axi_rdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    \axi_rdata[0]_i_10 
       (.I0(\slv_rd_addr_reg_n_0_[2] ),
        .I1(chan_async_axi_map_wready_reg_0),
        .I2(slv_addr),
        .I3(Q[4]),
        .I4(Q[3]),
        .O(\axi_rdata[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0D00000000000000)) 
    \axi_rdata[0]_i_13 
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(slv_addr),
        .I5(Q[4]),
        .O(\axi_rdata[0]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h3BBB)) 
    \axi_rdata[0]_i_14 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\timeout_value_reg[11]_0 [0]),
        .O(\axi_rdata[0]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'hFFFEFEFF)) 
    \axi_rdata[0]_i_16 
       (.I0(Q[4]),
        .I1(slv_addr),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[3]),
        .O(\axi_rdata[0]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT5 #(
    .INIT(32'h00002000)) 
    \axi_rdata[0]_i_2 
       (.I0(\slv_rdata_reg[3] [0]),
        .I1(\slv_rd_addr_reg_n_0_[3] ),
        .I2(\slv_rd_addr_reg_n_0_[0] ),
        .I3(\slv_rd_addr_reg_n_0_[2] ),
        .I4(chan_async_axi_map_wready_reg_0),
        .O(\axi_rdata[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'h0000FF35)) 
    \axi_rdata[0]_i_3 
       (.I0(\txpllclksel_reg[0]_0 ),
        .I1(\loopback_0_reg[0]_0 ),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(\axi_rdata[0]_i_8_n_0 ),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8F888F8F8F888888)) 
    \axi_rdata[0]_i_4 
       (.I0(\axi_rdata[0]_i_9_n_0 ),
        .I1(chan_rx_slv_rdata),
        .I2(\axi_rdata[0]_i_10_n_0 ),
        .I3(wait_for_drp_reg_0),
        .I4(Q[2]),
        .I5(\drp_read_data_reg[0] ),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8A888A888A88AAAA)) 
    \axi_rdata[0]_i_5 
       (.I0(\axi_rdata[4]_i_7_n_0 ),
        .I1(\axi_rdata[0]_i_13_n_0 ),
        .I2(\axi_rdata[0]_i_14_n_0 ),
        .I3(\axi_rdata[11]_i_5_n_0 ),
        .I4(timeout_enable_reg_0),
        .I5(\axi_rdata[0]_i_16_n_0 ),
        .O(\axi_rdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h000000000B080000)) 
    \axi_rdata[0]_i_8 
       (.I0(rx_sys_reset_axi),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(tx_sys_reset_axi),
        .I4(Q[3]),
        .I5(Q[2]),
        .O(\axi_rdata[0]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \axi_rdata[0]_i_9 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(\slv_rd_addr_reg_n_0_[2] ),
        .O(\axi_rdata[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h00000080AAAAAAAA)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\drp_write_data_reg[10] ),
        .I2(chan_async_axi_map_wready_reg_0),
        .I3(\axi_rdata[25]_i_2_n_0 ),
        .I4(Q[3]),
        .I5(\axi_rdata[10]_i_3_n_0 ),
        .O(\axi_rdata[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCCFFDEFFFDFFDEFF)) 
    \axi_rdata[10]_i_3 
       (.I0(Q[0]),
        .I1(\axi_rdata[10]_i_4_n_0 ),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[3]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'hAAAAFFBF)) 
    \axi_rdata[10]_i_4 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(\timeout_value_reg[11]_0 [10]),
        .I2(Q[1]),
        .I3(Q[3]),
        .I4(slv_addr),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h08AA080808080808)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[11]_i_2_n_0 ),
        .I2(\axi_rdata[11]_i_3_n_0 ),
        .I3(\drp_write_data_reg[11] ),
        .I4(chan_async_axi_map_wready_reg_0),
        .I5(\axi_rdata[11]_i_5_n_0 ),
        .O(\axi_rdata[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h5051004040514051)) 
    \axi_rdata[11]_i_2 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0D0D0D0D0DFF0D0D)) 
    \axi_rdata[11]_i_3 
       (.I0(\timeout_value_reg[11]_0 [11]),
        .I1(\axi_rdata[11]_i_6_n_0 ),
        .I2(slv_addr),
        .I3(\axi_rdata[21]_i_2_n_0 ),
        .I4(\axi_rdata[11]_i_7_n_0 ),
        .I5(Q[4]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \axi_rdata[11]_i_5 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(slv_addr),
        .O(\axi_rdata[11]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \axi_rdata[11]_i_6 
       (.I0(Q[3]),
        .I1(Q[1]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \axi_rdata[11]_i_7 
       (.I0(Q[0]),
        .I1(Q[2]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h8A888A888888888A)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[12]_i_2_n_0 ),
        .I2(\axi_rdata[15]_i_3_n_0 ),
        .I3(Q[0]),
        .I4(Q[4]),
        .I5(Q[1]),
        .O(\axi_rdata[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFA8080000)) 
    \axi_rdata[12]_i_2 
       (.I0(Q[1]),
        .I1(\drp_write_data_reg[15]_0 [0]),
        .I2(Q[0]),
        .I3(\drp_read_data_reg[15] [0]),
        .I4(\axi_rdata[15]_i_5_n_0 ),
        .I5(\axi_rdata[12]_i_3_n_0 ),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \axi_rdata[12]_i_3 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(slv_addr),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(Q[1]),
        .I5(Q[2]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hA8AAA8A8A8AAA8AA)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[13]_i_2_n_0 ),
        .I2(\axi_rdata[13]_i_3_n_0 ),
        .I3(\axi_rdata[13]_i_4_n_0 ),
        .I4(Q[4]),
        .I5(\axi_rdata[13]_i_5_n_0 ),
        .O(\axi_rdata[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'h8A800000)) 
    \axi_rdata[13]_i_2 
       (.I0(\axi_rdata[15]_i_5_n_0 ),
        .I1(\drp_read_data_reg[15] [1]),
        .I2(Q[0]),
        .I3(\drp_write_data_reg[15]_0 [1]),
        .I4(Q[1]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000800000)) 
    \axi_rdata[13]_i_3 
       (.I0(Q[0]),
        .I1(Q[4]),
        .I2(\axi_rdata[13]_i_6_n_0 ),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hDFDDFFFFFFFFFFFF)) 
    \axi_rdata[13]_i_4 
       (.I0(slv_addr),
        .I1(chan_async_axi_map_wready_reg_0),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(Q[2]),
        .I5(rxpolarity_0_reg),
        .O(\axi_rdata[13]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \axi_rdata[13]_i_5 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\axi_rdata[13]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[13]_i_6 
       (.I0(slv_addr),
        .I1(chan_async_axi_map_wready_reg_0),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000A08AAAAAAAAA)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(Q[1]),
        .I2(Q[4]),
        .I3(Q[0]),
        .I4(\axi_rdata[15]_i_3_n_0 ),
        .I5(\axi_rdata[14]_i_2_n_0 ),
        .O(\axi_rdata[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'h7F777FFF)) 
    \axi_rdata[14]_i_2 
       (.I0(\axi_rdata[15]_i_5_n_0 ),
        .I1(Q[1]),
        .I2(\drp_read_data_reg[15] [2]),
        .I3(Q[0]),
        .I4(\drp_write_data_reg[15]_0 [2]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000A088AAAAAAAA)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(Q[4]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\axi_rdata[15]_i_3_n_0 ),
        .I5(\axi_rdata[15]_i_4_n_0 ),
        .O(\axi_rdata[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \axi_rdata[15]_i_2 
       (.I0(\slv_rd_addr_reg_n_0_[2] ),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .I2(\slv_rd_addr_reg_n_0_[3] ),
        .O(\axi_rdata[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'hFF5DFFFF)) 
    \axi_rdata[15]_i_3 
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(chan_async_axi_map_wready_reg_0),
        .I4(slv_addr),
        .O(\axi_rdata[15]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h757FFFFF)) 
    \axi_rdata[15]_i_4 
       (.I0(Q[1]),
        .I1(\drp_read_data_reg[15] [3]),
        .I2(Q[0]),
        .I3(\drp_write_data_reg[15]_0 [3]),
        .I4(\axi_rdata[15]_i_5_n_0 ),
        .O(\axi_rdata[15]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'h00000002)) 
    \axi_rdata[15]_i_5 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(slv_addr),
        .I4(Q[4]),
        .O(\axi_rdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8500000000000000)) 
    \axi_rdata[16]_i_1 
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(\axi_rdata[24]_i_2_n_0 ),
        .I4(Q[2]),
        .I5(slv_addr),
        .O(\axi_rdata[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \axi_rdata[17]_i_1 
       (.I0(\slv_rd_addr_reg_n_0_[3] ),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .I2(chan_async_axi_map_wready_reg_0),
        .I3(\slv_rd_addr_reg_n_0_[2] ),
        .I4(\axi_rdata[17]_i_2_n_0 ),
        .O(\axi_rdata[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2EEEFFFFFFFFFFFC)) 
    \axi_rdata[17]_i_2 
       (.I0(Q[0]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(slv_addr),
        .I5(Q[2]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000444000000)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(\axi_rdata[25]_i_3_n_0 ),
        .I2(Q[3]),
        .I3(slv_addr),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(\axi_rdata[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \axi_rdata[18]_i_2 
       (.I0(Q[1]),
        .I1(Q[4]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT4 #(
    .INIT(16'h000D)) 
    \axi_rdata[19]_i_1 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\axi_rdata[19]_i_2_n_0 ),
        .O(\axi_rdata[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'hBFBDFFFF)) 
    \axi_rdata[19]_i_2 
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(slv_addr),
        .I3(Q[3]),
        .I4(\axi_rdata[25]_i_3_n_0 ),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAABABBBBAAAAAAAA)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\axi_rdata[1]_i_3_n_0 ),
        .I2(\slv_rd_addr_reg[1]_0 ),
        .I3(\axi_rdata[1]_i_5_n_0 ),
        .I4(\slv_rd_addr_reg_n_0_[2] ),
        .I5(\axi_rdata[1]_i_6_n_0 ),
        .O(\axi_rdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT5 #(
    .INIT(32'h00002000)) 
    \axi_rdata[1]_i_2 
       (.I0(\slv_rdata_reg[3] [1]),
        .I1(\slv_rd_addr_reg_n_0_[3] ),
        .I2(\slv_rd_addr_reg_n_0_[0] ),
        .I3(\slv_rd_addr_reg_n_0_[2] ),
        .I4(chan_async_axi_map_wready_reg_0),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000800080008AAAA)) 
    \axi_rdata[1]_i_3 
       (.I0(\axi_rdata[4]_i_7_n_0 ),
        .I1(\axi_rdata[1]_i_7_n_0 ),
        .I2(\axi_rdata[1]_i_8_n_0 ),
        .I3(\axi_rdata[3]_i_11_n_0 ),
        .I4(\axi_rdata[3]_i_8_n_0 ),
        .I5(\axi_rdata[1]_i_9_n_0 ),
        .O(\axi_rdata[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'hABFB)) 
    \axi_rdata[1]_i_5 
       (.I0(Q[3]),
        .I1(\rx_pd_0_reg[1]_1 ),
        .I2(Q[2]),
        .I3(\loopback_0_reg[1]_0 ),
        .O(\axi_rdata[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h4454000055555555)) 
    \axi_rdata[1]_i_6 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\drp_int_addr_reg[1] ),
        .I2(\timeout_length_reg[8] [0]),
        .I3(\axi_rdata[4]_i_9_n_0 ),
        .I4(\axi_rdata[11]_i_5_n_0 ),
        .I5(chan_async_axi_map_wready_reg_0),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF7FFFFFFFF)) 
    \axi_rdata[1]_i_7 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(slv_addr),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(\timeout_value_reg[11]_0 [1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001010001)) 
    \axi_rdata[1]_i_8 
       (.I0(slv_addr),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(\gt_interface_sel_reg[7]_0 [0]),
        .I5(Q[1]),
        .O(\axi_rdata[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0404000000000400)) 
    \axi_rdata[1]_i_9 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\cmm_interface_sel_reg[7] [0]),
        .I4(Q[4]),
        .I5(slv_addr),
        .O(\axi_rdata[1]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \axi_rdata[20]_i_1 
       (.I0(\slv_rd_addr_reg_n_0_[3] ),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .I2(chan_async_axi_map_wready_reg_0),
        .I3(\slv_rd_addr_reg_n_0_[2] ),
        .I4(\axi_rdata[20]_i_2_n_0 ),
        .O(\axi_rdata[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE77F7FFF7)) 
    \axi_rdata[20]_i_2 
       (.I0(Q[2]),
        .I1(slv_addr),
        .I2(Q[1]),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(Q[0]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[25]_i_3_n_0 ),
        .I1(slv_addr),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(Q[4]),
        .I5(\axi_rdata[21]_i_2_n_0 ),
        .O(\axi_rdata[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \axi_rdata[21]_i_2 
       (.I0(Q[1]),
        .I1(Q[2]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \axi_rdata[22]_i_1 
       (.I0(slv_addr),
        .I1(Q[2]),
        .I2(\axi_rdata[25]_i_3_n_0 ),
        .I3(Q[0]),
        .I4(Q[4]),
        .I5(Q[1]),
        .O(\axi_rdata[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000002)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(slv_addr),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(Q[1]),
        .I5(Q[2]),
        .O(\axi_rdata[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \axi_rdata[24]_i_2 
       (.I0(\slv_rd_addr_reg_n_0_[3] ),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .I2(chan_async_axi_map_wready_reg_0),
        .I3(\slv_rd_addr_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \axi_rdata[25]_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(\axi_rdata[25]_i_2_n_0 ),
        .I4(Q[0]),
        .I5(\axi_rdata[25]_i_3_n_0 ),
        .O(\axi_rdata[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \axi_rdata[25]_i_2 
       (.I0(slv_addr),
        .I1(Q[4]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \axi_rdata[25]_i_3 
       (.I0(\slv_rd_addr_reg_n_0_[2] ),
        .I1(chan_async_axi_map_wready_reg_0),
        .I2(\slv_rd_addr_reg_n_0_[0] ),
        .I3(\slv_rd_addr_reg_n_0_[3] ),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000004440404)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(\axi_rdata[2]_i_3_n_0 ),
        .I2(\axi_rdata[2]_i_4_n_0 ),
        .I3(\txpostcursor_0_reg[2] ),
        .I4(\slv_addr_reg[5]_1 ),
        .I5(\axi_rdata[2]_i_7_n_0 ),
        .O(\axi_rdata[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \axi_rdata[2]_i_10 
       (.I0(\timeout_value_reg[11]_0 [2]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(slv_addr),
        .I4(Q[4]),
        .I5(Q[3]),
        .O(\axi_rdata[2]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'hFFFFF4CC)) 
    \axi_rdata[2]_i_2 
       (.I0(\slv_rdata_reg[3] [2]),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .I2(chan_async_axi_map_wready_reg_0),
        .I3(\slv_rd_addr_reg_n_0_[2] ),
        .I4(\slv_rd_addr_reg_n_0_[3] ),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF1F1F101FFFFFFFF)) 
    \axi_rdata[2]_i_3 
       (.I0(\axi_rdata[2]_i_8_n_0 ),
        .I1(\rx_pd_0_reg[1] ),
        .I2(Q[0]),
        .I3(\axi_rdata[2]_i_9_n_0 ),
        .I4(\axi_rdata[2]_i_10_n_0 ),
        .I5(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[2]_i_4 
       (.I0(\slv_rd_addr_reg_n_0_[2] ),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hA8AAA8A8A8AAAAAA)) 
    \axi_rdata[2]_i_7 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(\axi_rdata[25]_i_2_n_0 ),
        .I2(Q[3]),
        .I3(\timeout_length_reg[2] ),
        .I4(Q[2]),
        .I5(\drp_read_data_reg[2] ),
        .O(\axi_rdata[2]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'hFFCFFF77)) 
    \axi_rdata[2]_i_8 
       (.I0(\cmm_interface_sel_reg[7] [1]),
        .I1(Q[3]),
        .I2(data_sync_reg_gsr),
        .I3(Q[4]),
        .I4(slv_addr),
        .O(\axi_rdata[2]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000C1C000000000)) 
    \axi_rdata[2]_i_9 
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(slv_addr),
        .I3(\gt_interface_sel_reg[7]_0 [1]),
        .I4(Q[1]),
        .I5(Q[3]),
        .O(\axi_rdata[2]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAFEFEAAFE)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(\axi_rdata[15]_i_2_n_0 ),
        .I2(\axi_rdata[3]_i_3_n_0 ),
        .I3(\axi_rdata[4]_i_7_n_0 ),
        .I4(\axi_rdata[3]_i_4_n_0 ),
        .I5(\axi_rdata[3]_i_5_n_0 ),
        .O(\axi_rdata[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \axi_rdata[3]_i_10 
       (.I0(\gt_interface_sel_reg[7]_0 [2]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(slv_addr),
        .I5(Q[4]),
        .O(\axi_rdata[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h80000000FFFFFFFF)) 
    \axi_rdata[3]_i_11 
       (.I0(Q[4]),
        .I1(slv_addr),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(\axi_rdata[3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF7FFFFFFFF)) 
    \axi_rdata[3]_i_12 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(slv_addr),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(\timeout_value_reg[11]_0 [3]),
        .O(\axi_rdata[3]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT5 #(
    .INIT(32'h00002000)) 
    \axi_rdata[3]_i_2 
       (.I0(\slv_rdata_reg[3] [3]),
        .I1(\slv_rd_addr_reg_n_0_[3] ),
        .I2(\slv_rd_addr_reg_n_0_[0] ),
        .I3(\slv_rd_addr_reg_n_0_[2] ),
        .I4(chan_async_axi_map_wready_reg_0),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \axi_rdata[3]_i_3 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(\gt_interface_sel_reg[2]_0 ),
        .I3(\gt_interface_sel_reg[4] ),
        .I4(\axi_rdata[3]_i_6_n_0 ),
        .I5(\txprecursor_0_reg[3] ),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hABABAB00ABABABAB)) 
    \axi_rdata[3]_i_4 
       (.I0(\axi_rdata[3]_i_8_n_0 ),
        .I1(Q[1]),
        .I2(\axi_rdata[3]_i_9_n_0 ),
        .I3(\axi_rdata[3]_i_10_n_0 ),
        .I4(\axi_rdata[3]_i_11_n_0 ),
        .I5(\axi_rdata[3]_i_12_n_0 ),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h22222222A2AAAAAA)) 
    \axi_rdata[3]_i_5 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(\axi_rdata[11]_i_5_n_0 ),
        .I2(\axi_rdata[13]_i_5_n_0 ),
        .I3(Q[2]),
        .I4(\timeout_length_reg[8] [1]),
        .I5(\drp_int_addr_reg[3] ),
        .O(\axi_rdata[3]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \axi_rdata[3]_i_6 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .I2(\slv_rd_addr_reg_n_0_[3] ),
        .O(\axi_rdata[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hAEAAAAAA)) 
    \axi_rdata[3]_i_8 
       (.I0(Q[0]),
        .I1(slv_addr),
        .I2(Q[4]),
        .I3(Q[1]),
        .I4(Q[2]),
        .O(\axi_rdata[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0FF5F3FF)) 
    \axi_rdata[3]_i_9 
       (.I0(\cmm_interface_sel_reg[7] [2]),
        .I1(data_sync_reg_gsr_0),
        .I2(Q[4]),
        .I3(slv_addr),
        .I4(Q[3]),
        .I5(Q[2]),
        .O(\axi_rdata[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h4544555545444544)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\axi_rdata[4]_i_3_n_0 ),
        .I2(\axi_rdata[4]_i_4_n_0 ),
        .I3(\axi_rdata[4]_i_5_n_0 ),
        .I4(\axi_rdata[4]_i_6_n_0 ),
        .I5(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h66EEFF7F66FFFF7F)) 
    \axi_rdata[4]_i_12 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(\timeout_value_reg[11]_0 [4]),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(\gt_interface_sel_reg[7]_0 [3]),
        .O(\axi_rdata[4]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFBFFFFFFFF)) 
    \axi_rdata[4]_i_13 
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(slv_addr),
        .I4(Q[4]),
        .I5(\cmm_interface_sel_reg[7] [3]),
        .O(\axi_rdata[4]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h4444444400000040)) 
    \axi_rdata[4]_i_14 
       (.I0(Q[4]),
        .I1(slv_addr),
        .I2(data_sync_reg_gsr_1),
        .I3(Q[3]),
        .I4(Q[1]),
        .I5(Q[2]),
        .O(\axi_rdata[4]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \axi_rdata[4]_i_2 
       (.I0(\slv_rd_addr_reg_n_0_[3] ),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AE000000)) 
    \axi_rdata[4]_i_3 
       (.I0(\drp_int_addr_reg[4] ),
        .I1(\timeout_length_reg[8] [2]),
        .I2(\axi_rdata[4]_i_9_n_0 ),
        .I3(\axi_rdata[11]_i_5_n_0 ),
        .I4(chan_async_axi_map_wready_reg_0),
        .I5(\slv_rd_addr_reg_n_0_[2] ),
        .O(\axi_rdata[4]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    \axi_rdata[4]_i_4 
       (.I0(\slv_rd_addr_reg_n_0_[2] ),
        .I1(\gt_interface_sel_reg[4] ),
        .I2(\gt_interface_sel_reg[2]_0 ),
        .I3(chan_async_axi_map_wready_reg_0),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000064200000)) 
    \axi_rdata[4]_i_5 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\txprecursor_0_reg[4]_0 ),
        .I3(\txpostcursor_0_reg[4]_0 ),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\axi_rdata[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hF600F600F6FFF600)) 
    \axi_rdata[4]_i_6 
       (.I0(Q[4]),
        .I1(slv_addr),
        .I2(\axi_rdata[4]_i_12_n_0 ),
        .I3(Q[0]),
        .I4(\axi_rdata[4]_i_13_n_0 ),
        .I5(\axi_rdata[4]_i_14_n_0 ),
        .O(\axi_rdata[4]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \axi_rdata[4]_i_7 
       (.I0(chan_async_axi_map_wready_reg_0),
        .I1(\slv_rd_addr_reg_n_0_[2] ),
        .O(\axi_rdata[4]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \axi_rdata[4]_i_9 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(\axi_rdata[4]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h000002A2AAAA02A2)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[5]_i_2_n_0 ),
        .I2(Q[0]),
        .I3(\axi_rdata[5]_i_3_n_0 ),
        .I4(chan_async_axi_map_wready_reg_0),
        .I5(\axi_rdata[5]_i_4_n_0 ),
        .O(\axi_rdata[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h77FFFF67FFFFFFFF)) 
    \axi_rdata[5]_i_2 
       (.I0(Q[4]),
        .I1(slv_addr),
        .I2(\cmm_interface_sel_reg[7] [4]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA8AAAAAAAAAAAAAA)) 
    \axi_rdata[5]_i_3 
       (.I0(\axi_rdata[5]_i_5_n_0 ),
        .I1(Q[3]),
        .I2(\axi_rdata[25]_i_2_n_0 ),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\timeout_value_reg[11]_0 [5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF757F7F7F7F7F7F7)) 
    \axi_rdata[5]_i_4 
       (.I0(\axi_rdata[11]_i_5_n_0 ),
        .I1(\drp_read_data_reg[5] ),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(\timeout_length_reg[8] [3]),
        .I5(Q[1]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFBFFFFEFFFFF)) 
    \axi_rdata[5]_i_5 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(\gt_interface_sel_reg[7]_0 [4]),
        .I5(slv_addr),
        .O(\axi_rdata[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000101010001)) 
    \axi_rdata[6]_i_1 
       (.I0(\slv_rd_addr_reg_n_0_[3] ),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .I2(\slv_rd_addr_reg_n_0_[2] ),
        .I3(\axi_rdata[6]_i_2_n_0 ),
        .I4(chan_async_axi_map_wready_reg_0),
        .I5(\axi_rdata[6]_i_3_n_0 ),
        .O(\axi_rdata[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF00BBBBFFFFAFAF)) 
    \axi_rdata[6]_i_2 
       (.I0(\axi_rdata[6]_i_4_n_0 ),
        .I1(Q[0]),
        .I2(Q[3]),
        .I3(\axi_rdata[6]_i_5_n_0 ),
        .I4(Q[1]),
        .I5(Q[2]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF757F7F7F7F7F7F7)) 
    \axi_rdata[6]_i_3 
       (.I0(\axi_rdata[11]_i_5_n_0 ),
        .I1(\drp_read_data_reg[6] ),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(\timeout_length_reg[8] [4]),
        .I5(Q[1]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h3333EFEE3333EFFF)) 
    \axi_rdata[6]_i_4 
       (.I0(Q[2]),
        .I1(slv_addr),
        .I2(\gt_interface_sel_reg[7]_0 [5]),
        .I3(Q[0]),
        .I4(Q[4]),
        .I5(\cmm_interface_sel_reg[7] [5]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hF3F133F3)) 
    \axi_rdata[6]_i_5 
       (.I0(\timeout_value_reg[11]_0 [6]),
        .I1(slv_addr),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(Q[0]),
        .O(\axi_rdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000A8AAAAAAA8AA)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[7]_i_2_n_0 ),
        .I2(\axi_rdata[7]_i_3_n_0 ),
        .I3(\axi_rdata[7]_i_4_n_0 ),
        .I4(chan_async_axi_map_wready_reg_0),
        .I5(\axi_rdata[7]_i_5_n_0 ),
        .O(\axi_rdata[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \axi_rdata[7]_i_2 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(\gt_interface_sel_reg[7]_0 [6]),
        .I3(rxpolarity_0_reg),
        .I4(slv_addr),
        .I5(Q[4]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000C00505)) 
    \axi_rdata[7]_i_3 
       (.I0(\axi_rdata[7]_i_6_n_0 ),
        .I1(slv_addr),
        .I2(Q[4]),
        .I3(\axi_rdata_reg[7]_0 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7FFFFFFFFF)) 
    \axi_rdata[7]_i_4 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(\axi_rdata[25]_i_2_n_0 ),
        .I5(\timeout_value_reg[11]_0 [7]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hF757F7F7F7F7F7F7)) 
    \axi_rdata[7]_i_5 
       (.I0(\axi_rdata[11]_i_5_n_0 ),
        .I1(\drp_read_data_reg[7] ),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(\timeout_length_reg[8] [5]),
        .I5(Q[1]),
        .O(\axi_rdata[7]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    \axi_rdata[7]_i_6 
       (.I0(Q[3]),
        .I1(slv_addr),
        .I2(\cmm_interface_sel_reg[7] [6]),
        .I3(Q[2]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \axi_rdata[7]_i_7 
       (.I0(Q[3]),
        .I1(Q[2]),
        .O(\axi_rdata_reg[7]_0 ));
  LUT6 #(
    .INIT(64'hA0A088A8000088A8)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[8]_i_2_n_0 ),
        .I2(\axi_rdata[11]_i_5_n_0 ),
        .I3(\rx_pd_0_reg[1] ),
        .I4(chan_async_axi_map_wready_reg_0),
        .I5(\axi_rdata[8]_i_4_n_0 ),
        .O(\axi_rdata[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000040A0000000)) 
    \axi_rdata[8]_i_2 
       (.I0(slv_addr),
        .I1(\timeout_value_reg[11]_0 [8]),
        .I2(\axi_rdata[21]_i_2_n_0 ),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(Q[0]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \axi_rdata[8]_i_3 
       (.I0(Q[1]),
        .I1(Q[2]),
        .O(\rx_pd_0_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT5 #(
    .INIT(32'h08FF0800)) 
    \axi_rdata[8]_i_4 
       (.I0(Q[1]),
        .I1(\timeout_length_reg[8] [6]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(\drp_read_data_reg[8] ),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h008AAA8A008A008A)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[9]_i_2_n_0 ),
        .I2(\axi_rdata[9]_i_3_n_0 ),
        .I3(chan_async_axi_map_wready_reg_0),
        .I4(\timeout_length_reg[9] ),
        .I5(\axi_rdata[11]_i_5_n_0 ),
        .O(\axi_rdata[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0A0000015D000000)) 
    \axi_rdata[9]_i_2 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(slv_addr),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7FFFFFFFFF)) 
    \axi_rdata[9]_i_3 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(\axi_rdata[25]_i_2_n_0 ),
        .I5(\timeout_value_reg[11]_0 [9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[0] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[0]_i_1_n_0 ),
        .Q(s_axi_rdata[0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[10] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[10]_i_1_n_0 ),
        .Q(s_axi_rdata[10]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[11] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[11]_i_1_n_0 ),
        .Q(s_axi_rdata[11]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[12] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[12]_i_1_n_0 ),
        .Q(s_axi_rdata[12]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[13] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[13]_i_1_n_0 ),
        .Q(s_axi_rdata[13]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[14] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[14]_i_1_n_0 ),
        .Q(s_axi_rdata[14]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[15] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[15]_i_1_n_0 ),
        .Q(s_axi_rdata[15]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[16] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[16]_i_1_n_0 ),
        .Q(s_axi_rdata[16]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[17] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[17]_i_1_n_0 ),
        .Q(s_axi_rdata[17]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[18] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[18]_i_1_n_0 ),
        .Q(s_axi_rdata[18]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[19] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[19]_i_1_n_0 ),
        .Q(s_axi_rdata[19]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[1] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[1]_i_1_n_0 ),
        .Q(s_axi_rdata[1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[20] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[20]_i_1_n_0 ),
        .Q(s_axi_rdata[20]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[21] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[21]_i_1_n_0 ),
        .Q(s_axi_rdata[21]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[22] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[22]_i_1_n_0 ),
        .Q(s_axi_rdata[22]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[24] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[24]_i_1_n_0 ),
        .Q(s_axi_rdata[23]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[25] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[25]_i_1_n_0 ),
        .Q(s_axi_rdata[24]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[2] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[2]_i_1_n_0 ),
        .Q(s_axi_rdata[2]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[3] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[3]_i_1_n_0 ),
        .Q(s_axi_rdata[3]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[4] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[4]_i_1_n_0 ),
        .Q(s_axi_rdata[4]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[5] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[5]_i_1_n_0 ),
        .Q(s_axi_rdata[5]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[6] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[6]_i_1_n_0 ),
        .Q(s_axi_rdata[6]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[7] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[7]_i_1_n_0 ),
        .Q(s_axi_rdata[7]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[8] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[8]_i_1_n_0 ),
        .Q(s_axi_rdata[8]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rdata_reg[9] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden_reg_n_0),
        .D(\axi_rdata[9]_i_1_n_0 ),
        .Q(s_axi_rdata[9]),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'hFFDF00C0)) 
    \axi_rresp[1]_i_1 
       (.I0(\axi_rresp[1]_i_2_n_0 ),
        .I1(timeout),
        .I2(read_in_progress),
        .I3(s_axi_rvalid),
        .I4(s_axi_rresp),
        .O(\axi_rresp[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF55554450)) 
    \axi_rresp[1]_i_2 
       (.I0(\axi_rresp[1]_i_3_n_0 ),
        .I1(gt_slv_rd_done),
        .I2(phy1_slv_rden),
        .I3(chan_async_axi_map_wready_reg_0),
        .I4(\slv_rd_addr_reg_n_0_[2] ),
        .I5(\axi_rresp[1]_i_4_n_0 ),
        .O(\axi_rresp[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF04C4C4C4)) 
    \axi_rresp[1]_i_3 
       (.I0(chan_async_slv_rden),
        .I1(\slv_rd_addr_reg_n_0_[2] ),
        .I2(chan_async_axi_map_wready_reg_0),
        .I3(slv_wren_done_pulse_0),
        .I4(slv_rden_r_1),
        .I5(\slv_rd_addr_reg_n_0_[0] ),
        .O(\axi_rresp[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFEFEFAAAAAAAA)) 
    \axi_rresp[1]_i_4 
       (.I0(\slv_rd_addr_reg_n_0_[3] ),
        .I1(chan_async_axi_map_wready_reg_0),
        .I2(\slv_rd_addr_reg_n_0_[2] ),
        .I3(slv_wren_done_pulse),
        .I4(slv_rden_r),
        .I5(\slv_rd_addr_reg_n_0_[0] ),
        .O(\axi_rresp[1]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \axi_rresp_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\axi_rresp[1]_i_1_n_0 ),
        .Q(s_axi_rresp),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'h74747444)) 
    axi_rvalid_i_1
       (.I0(s_axi_rready),
        .I1(s_axi_rvalid),
        .I2(read_in_progress),
        .I3(timeout),
        .I4(\axi_rresp[1]_i_2_n_0 ),
        .O(axi_rvalid_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    axi_rvalid_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s_axi_rvalid),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_wr_access_i_1
       (.I0(s_axi_arvalid),
        .O(axi_awready19_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wr_access_i_2
       (.I0(s_axi_wvalid),
        .I1(valid_waddr_reg_n_0),
        .I2(s_axi_wready),
        .O(axi_wr_access0));
  FDRE #(
    .INIT(1'b0)) 
    axi_wr_access_reg
       (.C(s_axi_aclk),
        .CE(axi_awready19_in),
        .D(axi_wr_access0),
        .Q(s_axi_wready),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    chan_async_axi_map_wready_i_1
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(valid_waddr_reg_n_0),
        .I2(s_axi_wvalid),
        .I3(\slv_rd_addr_reg_n_0_[2] ),
        .I4(chan_async_axi_map_wready_reg_0),
        .I5(chan_async_axi_map_wready),
        .O(chan_async_axi_map_wready0));
  FDRE chan_async_axi_map_wready_reg
       (.C(s_axi_aclk),
        .CE(axi_awready19_in),
        .D(chan_async_axi_map_wready0),
        .Q(chan_async_axi_map_wready),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    chan_async_slv_rden_i_1
       (.I0(s_axi_araddr[7]),
        .I1(s_axi_araddr[9]),
        .I2(s_axi_araddr[8]),
        .I3(s_axi_araddr[6]),
        .O(chan_async_slv_rden_i_1_n_0));
  FDRE chan_async_slv_rden_reg
       (.C(s_axi_aclk),
        .CE(slv_reg_rden0),
        .D(chan_async_slv_rden_i_1_n_0),
        .Q(chan_async_slv_rden),
        .R(phy1_slv_rden_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    chan_rx_axi_map_wready_i_1
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(valid_waddr_reg_n_0),
        .I2(s_axi_wvalid),
        .I3(chan_async_axi_map_wready_reg_0),
        .I4(\slv_rd_addr_reg_n_0_[2] ),
        .I5(chan_rx_axi_map_wready),
        .O(chan_rx_axi_map_wready0));
  FDRE chan_rx_axi_map_wready_reg
       (.C(s_axi_aclk),
        .CE(axi_awready19_in),
        .D(chan_rx_axi_map_wready0),
        .Q(chan_rx_axi_map_wready),
        .R(p_0_in));
  LUT4 #(
    .INIT(16'h0040)) 
    chan_rx_slv_rden_i_1
       (.I0(s_axi_araddr[9]),
        .I1(s_axi_araddr[7]),
        .I2(s_axi_araddr[8]),
        .I3(s_axi_araddr[6]),
        .O(chan_rx_slv_rden_i_1_n_0));
  FDRE chan_rx_slv_rden_reg
       (.C(s_axi_aclk),
        .CE(slv_reg_rden0),
        .D(chan_rx_slv_rden_i_1_n_0),
        .Q(chan_rx_slv_rden),
        .R(phy1_slv_rden_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    chan_tx_axi_map_wready_i_1
       (.I0(\slv_rd_addr_reg_n_0_[3] ),
        .I1(\slv_rd_addr_reg_n_0_[0] ),
        .I2(\slv_rd_addr_reg_n_0_[2] ),
        .I3(chan_async_axi_map_wready_reg_0),
        .I4(chan_tx_axi_map_wready_i_2_n_0),
        .I5(chan_tx_axi_map_wready),
        .O(chan_tx_axi_map_wready0));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    chan_tx_axi_map_wready_i_2
       (.I0(valid_waddr_reg_n_0),
        .I1(s_axi_wvalid),
        .O(chan_tx_axi_map_wready_i_2_n_0));
  FDRE chan_tx_axi_map_wready_reg
       (.C(s_axi_aclk),
        .CE(axi_awready19_in),
        .D(chan_tx_axi_map_wready0),
        .Q(chan_tx_axi_map_wready),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    chan_tx_slv_rden_i_1
       (.I0(s_axi_araddr[7]),
        .I1(s_axi_araddr[9]),
        .I2(s_axi_araddr[6]),
        .I3(s_axi_araddr[8]),
        .O(chan_tx_slv_rden_i_1_n_0));
  FDRE chan_tx_slv_rden_reg
       (.C(s_axi_aclk),
        .CE(slv_reg_rden0),
        .D(chan_tx_slv_rden_i_1_n_0),
        .Q(chan_tx_slv_rden),
        .R(phy1_slv_rden_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \cmm_interface_sel[7]_i_1 
       (.I0(Q[0]),
        .I1(\cmm_interface_sel[7]_i_2_n_0 ),
        .I2(Q[4]),
        .I3(slv_addr),
        .I4(phy1_axi_map_wready),
        .I5(s_axi_wvalid),
        .O(\cmm_interface_sel_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \cmm_interface_sel[7]_i_2 
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[2]),
        .O(\cmm_interface_sel[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    cpll_pd_0_i_1
       (.I0(s_axi_wdata[0]),
        .I1(chan_async_axi_map_wready),
        .I2(s_axi_wvalid),
        .I3(cpll_pd_0_reg_0),
        .I4(gt0_cpllpd_in),
        .O(cpll_pd_0_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    cpll_pd_0_i_2
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\gt_interface_sel_reg[4] ),
        .I5(\gt_interface_sel_reg[2]_0 ),
        .O(cpll_pd_0_reg_0));
  LUT6 #(
    .INIT(64'h0000800080000000)) 
    drp_access_in_progress_i_2
       (.I0(drp_access_in_progress_i_4_n_0),
        .I1(gt_axi_map_wready),
        .I2(s_axi_wvalid),
        .I3(Q[0]),
        .I4(s_axi_wdata[4]),
        .I5(s_axi_wdata[3]),
        .O(access_type5_out));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    drp_access_in_progress_i_4
       (.I0(slv_addr),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .O(drp_access_in_progress_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \drp_int_addr[8]_i_1 
       (.I0(gt_axi_map_wready),
        .I1(s_axi_wvalid),
        .I2(Q[0]),
        .I3(\axi_rdata[11]_i_5_n_0 ),
        .I4(Q[1]),
        .I5(Q[2]),
        .O(E));
  LUT6 #(
    .INIT(64'hBA8A000000000000)) 
    drp_reset_i_1
       (.I0(drp_reset),
        .I1(drp_reset_i_2_n_0),
        .I2(\axi_rdata[11]_i_5_n_0 ),
        .I3(s_axi_wdata[0]),
        .I4(gt_slv_wren),
        .I5(s_axi_aresetn),
        .O(drp_reset_reg));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    drp_reset_i_2
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(Q[1]),
        .O(drp_reset_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \drp_write_data[15]_i_1 
       (.I0(\axi_rdata[13]_i_5_n_0 ),
        .I1(s_axi_wvalid),
        .I2(gt_axi_map_wready),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(\axi_rdata[25]_i_2_n_0 ),
        .O(\drp_write_data_reg[15] ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'h08000000)) 
    gt_axi_map_wready_i_1
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(chan_async_axi_map_wready_reg_0),
        .I2(gt_axi_map_wready),
        .I3(valid_waddr_reg_n_0),
        .I4(s_axi_wvalid),
        .O(gt_axi_map_wready0));
  FDRE gt_axi_map_wready_reg
       (.C(s_axi_aclk),
        .CE(axi_awready19_in),
        .D(gt_axi_map_wready0),
        .Q(gt_axi_map_wready),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    \gt_interface_sel[7]_i_1 
       (.I0(Q[0]),
        .I1(\cmm_interface_sel[7]_i_2_n_0 ),
        .I2(Q[4]),
        .I3(slv_addr),
        .I4(phy1_axi_map_wready),
        .I5(s_axi_wvalid),
        .O(\gt_interface_sel_reg[7] ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    gt_slv_rden_i_1
       (.I0(s_axi_araddr[6]),
        .I1(s_axi_araddr[8]),
        .I2(s_axi_araddr[9]),
        .I3(s_axi_araddr[7]),
        .O(gt_slv_rden_i_1_n_0));
  FDRE gt_slv_rden_reg
       (.C(s_axi_aclk),
        .CE(slv_reg_rden0),
        .D(gt_slv_rden_i_1_n_0),
        .Q(gt_slv_rden),
        .R(phy1_slv_rden_i_1_n_0));
  LUT3 #(
    .INIT(8'h74)) 
    i__carry__0_i_1
       (.I0(\timeout_value_reg[11]_0 [7]),
        .I1(\timeout_timer_count[0]_i_4_n_0 ),
        .I2(\timeout_timer_count_reg_n_0_[7] ),
        .O(\timeout_timer_count_reg[7]_0 [3]));
  LUT3 #(
    .INIT(8'h3A)) 
    i__carry__0_i_2
       (.I0(\timeout_timer_count_reg_n_0_[6] ),
        .I1(\timeout_value_reg[11]_0 [6]),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(\timeout_timer_count_reg[7]_0 [2]));
  LUT3 #(
    .INIT(8'h3A)) 
    i__carry__0_i_3
       (.I0(\timeout_timer_count_reg_n_0_[5] ),
        .I1(\timeout_value_reg[11]_0 [5]),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(\timeout_timer_count_reg[7]_0 [1]));
  LUT3 #(
    .INIT(8'h3A)) 
    i__carry__0_i_4
       (.I0(\timeout_timer_count_reg_n_0_[4] ),
        .I1(\timeout_value_reg[11]_0 [4]),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(\timeout_timer_count_reg[7]_0 [0]));
  LUT3 #(
    .INIT(8'h3A)) 
    i__carry__1_i_1
       (.I0(\timeout_timer_count_reg_n_0_[11] ),
        .I1(\timeout_value_reg[11]_0 [11]),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(\timeout_timer_count_reg[11]_0 [3]));
  LUT3 #(
    .INIT(8'h3A)) 
    i__carry__1_i_2
       (.I0(\timeout_timer_count_reg_n_0_[10] ),
        .I1(\timeout_value_reg[11]_0 [10]),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(\timeout_timer_count_reg[11]_0 [2]));
  LUT3 #(
    .INIT(8'h74)) 
    i__carry__1_i_3
       (.I0(\timeout_value_reg[11]_0 [9]),
        .I1(\timeout_timer_count[0]_i_4_n_0 ),
        .I2(\timeout_timer_count_reg_n_0_[9] ),
        .O(\timeout_timer_count_reg[11]_0 [1]));
  LUT3 #(
    .INIT(8'h3A)) 
    i__carry__1_i_4
       (.I0(\timeout_timer_count_reg_n_0_[8] ),
        .I1(\timeout_value_reg[11]_0 [8]),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(\timeout_timer_count_reg[11]_0 [0]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__2_i_1
       (.I0(timeout),
        .I1(\timeout_timer_count[0]_i_4_n_0 ),
        .O(\timeout_timer_count_reg[12]_0 ));
  LUT3 #(
    .INIT(8'h3A)) 
    i__carry_i_1
       (.I0(\timeout_timer_count_reg_n_0_[0] ),
        .I1(\timeout_value_reg[11]_0 [0]),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(DI));
  LUT3 #(
    .INIT(8'h74)) 
    i__carry_i_2
       (.I0(\timeout_value_reg[11]_0 [3]),
        .I1(\timeout_timer_count[0]_i_4_n_0 ),
        .I2(\timeout_timer_count_reg_n_0_[3] ),
        .O(S[3]));
  LUT3 #(
    .INIT(8'h3A)) 
    i__carry_i_3
       (.I0(\timeout_timer_count_reg_n_0_[2] ),
        .I1(\timeout_value_reg[11]_0 [2]),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(S[2]));
  LUT3 #(
    .INIT(8'h74)) 
    i__carry_i_4
       (.I0(\timeout_value_reg[11]_0 [1]),
        .I1(\timeout_timer_count[0]_i_4_n_0 ),
        .I2(\timeout_timer_count_reg_n_0_[1] ),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h53)) 
    i__carry_i_5
       (.I0(\timeout_value_reg[11]_0 [0]),
        .I1(\timeout_timer_count_reg_n_0_[0] ),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(S[0]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \loopback_0[0]_i_1 
       (.I0(s_axi_wdata[0]),
        .I1(\loopback_0[2]_i_2_n_0 ),
        .I2(s_axi_wvalid),
        .I3(chan_async_axi_map_wready),
        .I4(\slv_addr_reg[6]_0 ),
        .I5(gt0_loopback_in[0]),
        .O(\loopback_0_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \loopback_0[1]_i_1 
       (.I0(s_axi_wdata[1]),
        .I1(\loopback_0[2]_i_2_n_0 ),
        .I2(s_axi_wvalid),
        .I3(chan_async_axi_map_wready),
        .I4(\slv_addr_reg[6]_0 ),
        .I5(gt0_loopback_in[1]),
        .O(\loopback_0_reg[1] ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \loopback_0[2]_i_1 
       (.I0(s_axi_wdata[2]),
        .I1(\loopback_0[2]_i_2_n_0 ),
        .I2(s_axi_wvalid),
        .I3(chan_async_axi_map_wready),
        .I4(\slv_addr_reg[6]_0 ),
        .I5(gt0_loopback_in[2]),
        .O(\loopback_0_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \loopback_0[2]_i_2 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(\loopback_0[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    phy1_axi_map_wready_i_1
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(chan_async_axi_map_wready_reg_0),
        .I2(\slv_rd_addr_reg_n_0_[2] ),
        .I3(valid_waddr_reg_n_0),
        .I4(s_axi_wvalid),
        .I5(phy1_axi_map_wready),
        .O(phy1_axi_map_wready0));
  FDRE phy1_axi_map_wready_reg
       (.C(s_axi_aclk),
        .CE(axi_awready19_in),
        .D(phy1_axi_map_wready0),
        .Q(phy1_axi_map_wready),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'hF700FFFF)) 
    phy1_slv_rden_i_1
       (.I0(s_axi_arready),
        .I1(s_axi_arvalid),
        .I2(s_axi_rvalid),
        .I3(\axi_rresp[1]_i_2_n_0 ),
        .I4(s_axi_aresetn),
        .O(phy1_slv_rden_i_1_n_0));
  LUT3 #(
    .INIT(8'h08)) 
    phy1_slv_rden_i_2
       (.I0(s_axi_arready),
        .I1(s_axi_arvalid),
        .I2(s_axi_rvalid),
        .O(slv_reg_rden0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    phy1_slv_rden_i_3
       (.I0(s_axi_araddr[7]),
        .I1(s_axi_araddr[9]),
        .I2(s_axi_araddr[6]),
        .I3(s_axi_araddr[8]),
        .O(phy1_slv_rden_i_3_n_0));
  FDRE phy1_slv_rden_reg
       (.C(s_axi_aclk),
        .CE(slv_reg_rden0),
        .D(phy1_slv_rden_i_3_n_0),
        .Q(phy1_slv_rden),
        .R(phy1_slv_rden_i_1_n_0));
  LUT6 #(
    .INIT(64'h000000EA00EA00EA)) 
    read_in_progress_i_1
       (.I0(read_in_progress),
        .I1(s_axi_arvalid),
        .I2(s_axi_arready),
        .I3(read_in_progress_i_2_n_0),
        .I4(s_axi_rvalid),
        .I5(s_axi_rready),
        .O(read_in_progress_i_1_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    read_in_progress_i_2
       (.I0(timeout),
        .I1(s_axi_aresetn),
        .O(read_in_progress_i_2_n_0));
  FDRE read_in_progress_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(read_in_progress_i_1_n_0),
        .Q(read_in_progress),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \rx_pd_0[0]_i_1 
       (.I0(s_axi_wdata[0]),
        .I1(\slv_addr_reg[5]_0 ),
        .I2(Q[0]),
        .I3(\rx_pd_0[1]_i_3_n_0 ),
        .I4(\rx_pd_0_reg[1] ),
        .I5(gt0_rxpd_in[0]),
        .O(\rx_pd_0_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \rx_pd_0[1]_i_1 
       (.I0(s_axi_wdata[1]),
        .I1(\slv_addr_reg[5]_0 ),
        .I2(Q[0]),
        .I3(\rx_pd_0[1]_i_3_n_0 ),
        .I4(\rx_pd_0_reg[1] ),
        .I5(gt0_rxpd_in[1]),
        .O(\rx_pd_0_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rx_pd_0[1]_i_3 
       (.I0(s_axi_wvalid),
        .I1(chan_async_axi_map_wready),
        .O(\rx_pd_0[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFBFF00000800)) 
    rx_sys_reset_axi_i_1
       (.I0(s_axi_wdata[0]),
        .I1(Q[0]),
        .I2(\cmm_interface_sel[7]_i_2_n_0 ),
        .I3(\rx_pd_0[1]_i_3_n_0 ),
        .I4(\slv_addr_reg[6]_0 ),
        .I5(rx_sys_reset_axi),
        .O(rx_sys_reset_axi_reg));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \rxpllclksel[0]_i_1 
       (.I0(s_axi_wdata[0]),
        .I1(\rx_pd_0[1]_i_3_n_0 ),
        .I2(\slv_addr_reg[5]_0 ),
        .I3(Q[0]),
        .I4(\rxpllclksel[1]_i_2_n_0 ),
        .I5(gt0_rxsysclksel_in[0]),
        .O(\rxpllclksel_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \rxpllclksel[1]_i_1 
       (.I0(s_axi_wdata[1]),
        .I1(\rx_pd_0[1]_i_3_n_0 ),
        .I2(\slv_addr_reg[5]_0 ),
        .I3(Q[0]),
        .I4(\rxpllclksel[1]_i_2_n_0 ),
        .I5(gt0_rxsysclksel_in[1]),
        .O(\rxpllclksel_reg[1] ));
  LUT2 #(
    .INIT(4'hB)) 
    \rxpllclksel[1]_i_2 
       (.I0(Q[1]),
        .I1(Q[2]),
        .O(\rxpllclksel[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rxpolarity_0_i_2
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(rxpolarity_0_reg));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_addr[2]_i_1 
       (.I0(s_axi_araddr[0]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[0]),
        .O(\slv_addr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_addr[3]_i_1 
       (.I0(s_axi_araddr[1]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[1]),
        .O(\slv_addr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_addr[4]_i_1 
       (.I0(s_axi_araddr[2]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[2]),
        .O(\slv_addr[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_addr[5]_i_1 
       (.I0(s_axi_araddr[3]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[3]),
        .O(\slv_addr[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_addr[6]_i_1 
       (.I0(s_axi_araddr[4]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[4]),
        .O(\slv_addr[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_addr[7]_i_1 
       (.I0(s_axi_araddr[5]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[5]),
        .O(\slv_addr[7]_i_1_n_0 ));
  FDRE \slv_addr_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_addr[2]_i_1_n_0 ),
        .Q(Q[0]),
        .R(p_0_in));
  FDRE \slv_addr_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_addr[3]_i_1_n_0 ),
        .Q(Q[1]),
        .R(p_0_in));
  FDRE \slv_addr_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_addr[4]_i_1_n_0 ),
        .Q(Q[2]),
        .R(p_0_in));
  FDRE \slv_addr_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_addr[5]_i_1_n_0 ),
        .Q(Q[3]),
        .R(p_0_in));
  FDRE \slv_addr_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_addr[6]_i_1_n_0 ),
        .Q(Q[4]),
        .R(p_0_in));
  FDRE \slv_addr_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_addr[7]_i_1_n_0 ),
        .Q(slv_addr),
        .R(p_0_in));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_rd_addr[0]_i_1 
       (.I0(s_axi_araddr[6]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[6]),
        .O(\slv_rd_addr[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_rd_addr[1]_i_1 
       (.I0(s_axi_araddr[7]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[7]),
        .O(\slv_rd_addr[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_rd_addr[2]_i_1 
       (.I0(s_axi_araddr[8]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[8]),
        .O(\slv_rd_addr[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02020F02)) 
    \slv_rd_addr[3]_i_1 
       (.I0(s_axi_awvalid),
        .I1(s_axi_awready),
        .I2(valid_waddr_reg_n_0),
        .I3(s_axi_arvalid),
        .I4(read_in_progress),
        .O(\slv_rd_addr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \slv_rd_addr[3]_i_2 
       (.I0(s_axi_araddr[9]),
        .I1(s_axi_arvalid),
        .I2(read_in_progress),
        .I3(s_axi_awaddr[9]),
        .O(\slv_rd_addr[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_rd_addr_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_rd_addr[0]_i_1_n_0 ),
        .Q(\slv_rd_addr_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \slv_rd_addr_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_rd_addr[1]_i_1_n_0 ),
        .Q(chan_async_axi_map_wready_reg_0),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \slv_rd_addr_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_rd_addr[2]_i_1_n_0 ),
        .Q(\slv_rd_addr_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \slv_rd_addr_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_rd_addr[3]_i_1_n_0 ),
        .D(\slv_rd_addr[3]_i_2_n_0 ),
        .Q(\slv_rd_addr_reg_n_0_[3] ),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \slv_rdata[0]_i_2 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\slv_rdata_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \slv_rdata[0]_i_4 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\gt_interface_sel_reg[4] ),
        .I4(\gt_interface_sel_reg[2]_0 ),
        .I5(Q[3]),
        .O(txpolarity_0_reg));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_rdata[2]_i_1 
       (.I0(\txdiffctrl_0_reg[3]_0 [0]),
        .I1(\slv_addr_reg[6]_0 ),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_rdata[3]_i_1 
       (.I0(\txdiffctrl_0_reg[3]_0 [1]),
        .I1(\slv_addr_reg[6]_0 ),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT5 #(
    .INIT(32'h75553000)) 
    slv_reg_rden_i_1
       (.I0(\axi_rresp[1]_i_2_n_0 ),
        .I1(s_axi_rvalid),
        .I2(s_axi_arvalid),
        .I3(s_axi_arready),
        .I4(slv_reg_rden_reg_n_0),
        .O(slv_reg_rden_i_1_n_0));
  FDRE slv_reg_rden_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(slv_reg_rden_i_1_n_0),
        .Q(slv_reg_rden_reg_n_0),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT4 #(
    .INIT(16'hF444)) 
    \slv_wdata_r_internal[3]_i_1 
       (.I0(slv_rden_r),
        .I1(chan_tx_slv_rden),
        .I2(chan_tx_axi_map_wready),
        .I3(s_axi_wvalid),
        .O(\slv_wdata_r_internal_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    timeout_enable_i_1
       (.I0(s_axi_wdata[0]),
        .I1(\axi_rdata[11]_i_5_n_0 ),
        .I2(Q[0]),
        .I3(timeout_enable_i_2_n_0),
        .I4(\rxpllclksel[1]_i_2_n_0 ),
        .I5(timeout_enable),
        .O(timeout_enable_reg));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    timeout_enable_i_2
       (.I0(s_axi_wvalid),
        .I1(phy1_axi_map_wready),
        .O(timeout_enable_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    \timeout_length[11]_i_1 
       (.I0(\axi_rdata[13]_i_5_n_0 ),
        .I1(s_axi_wvalid),
        .I2(gt_axi_map_wready),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(\axi_rdata[25]_i_2_n_0 ),
        .O(\timeout_length_reg[11] ));
  LUT6 #(
    .INIT(64'hFFFFF8FFF8FFF8FF)) 
    \timeout_timer_count[0]_i_1 
       (.I0(s_axi_bvalid),
        .I1(s_axi_bready),
        .I2(timeout),
        .I3(s_axi_aresetn),
        .I4(s_axi_rvalid),
        .I5(s_axi_rready),
        .O(\timeout_timer_count[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hF4)) 
    \timeout_timer_count[0]_i_2 
       (.I0(\timeout_timer_count[0]_i_3_n_0 ),
        .I1(timeout_enable),
        .I2(\timeout_timer_count[0]_i_4_n_0 ),
        .O(\timeout_timer_count[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \timeout_timer_count[0]_i_3 
       (.I0(\timeout_timer_count[0]_i_5_n_0 ),
        .I1(\timeout_timer_count_reg_n_0_[1] ),
        .I2(\timeout_timer_count_reg_n_0_[0] ),
        .I3(\timeout_timer_count_reg_n_0_[3] ),
        .I4(\timeout_timer_count_reg_n_0_[2] ),
        .I5(\timeout_timer_count[0]_i_6_n_0 ),
        .O(\timeout_timer_count[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \timeout_timer_count[0]_i_4 
       (.I0(timeout_enable),
        .I1(load_timeout_timer0),
        .I2(s_axi_awready),
        .I3(s_axi_awvalid),
        .I4(s_axi_wready),
        .I5(s_axi_wvalid),
        .O(\timeout_timer_count[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \timeout_timer_count[0]_i_5 
       (.I0(\timeout_timer_count_reg_n_0_[5] ),
        .I1(\timeout_timer_count_reg_n_0_[4] ),
        .I2(\timeout_timer_count_reg_n_0_[7] ),
        .I3(\timeout_timer_count_reg_n_0_[6] ),
        .O(\timeout_timer_count[0]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \timeout_timer_count[0]_i_6 
       (.I0(\timeout_timer_count_reg_n_0_[11] ),
        .I1(\timeout_timer_count_reg_n_0_[8] ),
        .I2(\timeout_timer_count_reg_n_0_[9] ),
        .I3(timeout),
        .I4(\timeout_timer_count_reg_n_0_[10] ),
        .O(\timeout_timer_count[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \timeout_timer_count[0]_i_7 
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .O(load_timeout_timer0));
  FDRE \timeout_timer_count_reg[0] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(O[0]),
        .Q(\timeout_timer_count_reg_n_0_[0] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[10] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_timer_count_reg[11]_1 [2]),
        .Q(\timeout_timer_count_reg_n_0_[10] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[11] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_timer_count_reg[11]_1 [3]),
        .Q(\timeout_timer_count_reg_n_0_[11] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[12] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_timer_count_reg[12]_1 ),
        .Q(timeout),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[1] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(O[1]),
        .Q(\timeout_timer_count_reg_n_0_[1] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[2] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(O[2]),
        .Q(\timeout_timer_count_reg_n_0_[2] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[3] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(O[3]),
        .Q(\timeout_timer_count_reg_n_0_[3] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[4] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_value_reg[7] [0]),
        .Q(\timeout_timer_count_reg_n_0_[4] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[5] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_value_reg[7] [1]),
        .Q(\timeout_timer_count_reg_n_0_[5] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[6] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_value_reg[7] [2]),
        .Q(\timeout_timer_count_reg_n_0_[6] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[7] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_value_reg[7] [3]),
        .Q(\timeout_timer_count_reg_n_0_[7] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[8] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_timer_count_reg[11]_1 [0]),
        .Q(\timeout_timer_count_reg_n_0_[8] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  FDRE \timeout_timer_count_reg[9] 
       (.C(s_axi_aclk),
        .CE(\timeout_timer_count[0]_i_2_n_0 ),
        .D(\timeout_timer_count_reg[11]_1 [1]),
        .Q(\timeout_timer_count_reg_n_0_[9] ),
        .R(\timeout_timer_count[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \timeout_value[11]_i_1 
       (.I0(\timeout_value[11]_i_2_n_0 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(s_axi_wvalid),
        .I4(phy1_axi_map_wready),
        .I5(\axi_rdata[25]_i_2_n_0 ),
        .O(\timeout_value_reg[11] ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \timeout_value[11]_i_2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .O(\timeout_value[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    tx_sys_reset_axi_i_1
       (.I0(s_axi_wdata[0]),
        .I1(Q[0]),
        .I2(\cmm_interface_sel[7]_i_2_n_0 ),
        .I3(\rx_pd_0[1]_i_3_n_0 ),
        .I4(\slv_addr_reg[6]_0 ),
        .I5(tx_sys_reset_axi),
        .O(tx_sys_reset_axi_reg));
  LUT6 #(
    .INIT(64'h0000000000010000)) 
    \txdiffctrl_0[3]_i_1 
       (.I0(\slv_addr_reg[6]_0 ),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(\axi_rdata[13]_i_5_n_0 ),
        .I4(slv_wren_clk2),
        .I5(slv_rden_r),
        .O(\txdiffctrl_0_reg[3] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    txinihibit_0_i_2
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\gt_interface_sel_reg[4] ),
        .I5(\gt_interface_sel_reg[2]_0 ),
        .O(txinihibit_0_reg));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \txpllclksel[0]_i_1 
       (.I0(s_axi_wdata[0]),
        .I1(\rx_pd_0[1]_i_3_n_0 ),
        .I2(\gt_interface_sel_reg[2] ),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(gt0_txsysclksel_in[0]),
        .O(\txpllclksel_reg[0] ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \txpllclksel[1]_i_1 
       (.I0(s_axi_wdata[1]),
        .I1(\rx_pd_0[1]_i_3_n_0 ),
        .I2(\gt_interface_sel_reg[2] ),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(gt0_txsysclksel_in[1]),
        .O(\txpllclksel_reg[1] ));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    \txpostcursor_0[4]_i_1 
       (.I0(Q[0]),
        .I1(\rx_pd_0[1]_i_3_n_0 ),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[3]),
        .I5(\slv_addr_reg[6]_0 ),
        .O(\txpostcursor_0_reg[4] ));
  LUT6 #(
    .INIT(64'h0000000002000000)) 
    \txprecursor_0[4]_i_1 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(\slv_addr_reg[6]_0 ),
        .I3(s_axi_wvalid),
        .I4(chan_async_axi_map_wready),
        .I5(\axi_rdata[13]_i_5_n_0 ),
        .O(\txprecursor_0_reg[4] ));
  LUT6 #(
    .INIT(64'h00000A000000EA00)) 
    valid_waddr_i_1
       (.I0(valid_waddr_reg_n_0),
        .I1(valid_waddr_i_2_n_0),
        .I2(axi_awready0),
        .I3(s_axi_aresetn),
        .I4(timeout),
        .I5(valid_waddr_i_4_n_0),
        .O(valid_waddr_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    valid_waddr_i_2
       (.I0(s_axi_awvalid),
        .I1(s_axi_awready),
        .O(valid_waddr_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h1)) 
    valid_waddr_i_3
       (.I0(read_in_progress),
        .I1(s_axi_arvalid),
        .O(axi_awready0));
  LUT2 #(
    .INIT(4'h8)) 
    valid_waddr_i_4
       (.I0(s_axi_bready),
        .I1(s_axi_bvalid),
        .O(valid_waddr_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    valid_waddr_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(valid_waddr_i_1_n_0),
        .Q(valid_waddr_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    wait_for_drp_i_1
       (.I0(s_axi_wdata[0]),
        .I1(gt_axi_map_wready),
        .I2(s_axi_wvalid),
        .I3(wait_for_drp_i_2_n_0),
        .I4(wait_for_drp),
        .O(wait_for_drp_reg));
  LUT6 #(
    .INIT(64'hFEFFFFFFFFFFFFFF)) 
    wait_for_drp_i_2
       (.I0(Q[4]),
        .I1(slv_addr),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(wait_for_drp_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    wr_req_reg_i_1
       (.I0(s_axi_wvalid),
        .I1(gt_axi_map_wready),
        .O(gt_slv_wren));
  LUT6 #(
    .INIT(64'h00000000FFFFF888)) 
    write_in_progress_i_1
       (.I0(s_axi_awready),
        .I1(s_axi_awvalid),
        .I2(s_axi_wready),
        .I3(s_axi_wvalid),
        .I4(write_in_progress),
        .I5(write_in_progress_i_2_n_0),
        .O(write_in_progress_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'hFF8F)) 
    write_in_progress_i_2
       (.I0(s_axi_bvalid),
        .I1(s_axi_bready),
        .I2(s_axi_aresetn),
        .I3(timeout),
        .O(write_in_progress_i_2_n_0));
  FDRE write_in_progress_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(write_in_progress_i_1_n_0),
        .Q(write_in_progress),
        .R(1'b0));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen
   (slv_wren_done_pulse,
    clk2_ready_reg_0,
    txinihibit_0_reg,
    txpolarity_0_reg,
    slv_access_valid_hold_reg,
    \tx_pd_0_reg[1] ,
    \tx_pd_0_reg[0] ,
    p_0_in,
    s_axi_aclk,
    SR,
    tx_core_clk,
    Q,
    \slv_addr_reg[3] ,
    slv_rden_r_reg,
    gt0_txinhibit_in,
    \slv_addr_reg[3]_0 ,
    gt0_txpolarity_in,
    s_axi_wvalid,
    chan_tx_axi_map_wready,
    chan_tx_slv_rden,
    data_in,
    \slv_addr_reg[5] ,
    \slv_addr_reg[2] ,
    \slv_addr_reg[3]_1 ,
    gt0_txpd_in);
  output slv_wren_done_pulse;
  output clk2_ready_reg_0;
  output txinihibit_0_reg;
  output txpolarity_0_reg;
  output slv_access_valid_hold_reg;
  output \tx_pd_0_reg[1] ;
  output \tx_pd_0_reg[0] ;
  input p_0_in;
  input s_axi_aclk;
  input [0:0]SR;
  input tx_core_clk;
  input [1:0]Q;
  input \slv_addr_reg[3] ;
  input slv_rden_r_reg;
  input gt0_txinhibit_in;
  input \slv_addr_reg[3]_0 ;
  input gt0_txpolarity_in;
  input s_axi_wvalid;
  input chan_tx_axi_map_wready;
  input chan_tx_slv_rden;
  input data_in;
  input \slv_addr_reg[5] ;
  input [0:0]\slv_addr_reg[2] ;
  input \slv_addr_reg[3]_1 ;
  input [1:0]gt0_txpd_in;

  wire [1:0]Q;
  wire [0:0]SR;
  wire axi_2_drp_valid_i_n_0;
  wire axi_2_drp_valid_i_n_1;
  wire chan_tx_axi_map_wready;
  wire chan_tx_slv_rden;
  wire clk1_ready_pulse0;
  wire clk1_ready_sync_r;
  wire clk2_ready;
  wire clk2_ready_reg_0;
  wire clk2_valid_pulse0;
  wire clk2_valid_sync_r;
  wire data_in;
  wire gt0_txinhibit_in;
  wire [1:0]gt0_txpd_in;
  wire gt0_txpolarity_in;
  wire p_0_in;
  wire s_axi_aclk;
  wire s_axi_wvalid;
  wire slv_access_valid_hold_reg;
  wire [0:0]\slv_addr_reg[2] ;
  wire \slv_addr_reg[3] ;
  wire \slv_addr_reg[3]_0 ;
  wire \slv_addr_reg[3]_1 ;
  wire \slv_addr_reg[5] ;
  wire slv_rden_r_reg;
  wire slv_wren_clear;
  wire slv_wren_done_pulse;
  wire tx_core_clk;
  wire \tx_pd_0[1]_i_2_n_0 ;
  wire \tx_pd_0_reg[0] ;
  wire \tx_pd_0_reg[1] ;
  wire txinihibit_0_reg;
  wire txpolarity_0_reg;

  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__5 axi_2_drp_valid_i
       (.clk2_ready_reg(axi_2_drp_valid_i_n_0),
        .clk2_valid_pulse0(clk2_valid_pulse0),
        .clk2_valid_pulse_reg(clk2_ready_reg_0),
        .clk2_valid_sync_r(clk2_valid_sync_r),
        .data_in(clk2_ready),
        .data_out(axi_2_drp_valid_i_n_1),
        .slv_access_valid_hold_reg(data_in),
        .tx_core_clk(tx_core_clk));
  FDRE clk1_ready_pulse_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(clk1_ready_pulse0),
        .Q(slv_wren_done_pulse),
        .R(p_0_in));
  FDRE clk1_ready_sync_r_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(slv_wren_clear),
        .Q(clk1_ready_sync_r),
        .R(p_0_in));
  FDRE clk2_ready_reg
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(axi_2_drp_valid_i_n_0),
        .Q(clk2_ready),
        .R(SR));
  FDRE clk2_valid_pulse_reg
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(clk2_valid_pulse0),
        .Q(clk2_ready_reg_0),
        .R(SR));
  FDRE clk2_valid_sync_r_reg
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(axi_2_drp_valid_i_n_1),
        .Q(clk2_valid_sync_r),
        .R(SR));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__6 drp_2_axi_in_progress_i
       (.chan_tx_axi_map_wready(chan_tx_axi_map_wready),
        .chan_tx_slv_rden(chan_tx_slv_rden),
        .clk1_ready_pulse0(clk1_ready_pulse0),
        .clk1_ready_sync_r(clk1_ready_sync_r),
        .clk2_ready_reg(clk2_ready),
        .data_in(data_in),
        .data_out(slv_wren_clear),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_wvalid(s_axi_wvalid),
        .slv_access_valid_hold_reg(slv_access_valid_hold_reg),
        .slv_rden_r_reg(slv_rden_r_reg));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \tx_pd_0[0]_i_1 
       (.I0(Q[0]),
        .I1(\slv_addr_reg[5] ),
        .I2(\slv_addr_reg[2] ),
        .I3(\slv_addr_reg[3]_1 ),
        .I4(\tx_pd_0[1]_i_2_n_0 ),
        .I5(gt0_txpd_in[0]),
        .O(\tx_pd_0_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \tx_pd_0[1]_i_1 
       (.I0(Q[1]),
        .I1(\slv_addr_reg[5] ),
        .I2(\slv_addr_reg[2] ),
        .I3(\slv_addr_reg[3]_1 ),
        .I4(\tx_pd_0[1]_i_2_n_0 ),
        .I5(gt0_txpd_in[1]),
        .O(\tx_pd_0_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \tx_pd_0[1]_i_2 
       (.I0(slv_rden_r_reg),
        .I1(clk2_ready_reg_0),
        .O(\tx_pd_0[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    txinihibit_0_i_1
       (.I0(Q[0]),
        .I1(\slv_addr_reg[3] ),
        .I2(clk2_ready_reg_0),
        .I3(slv_rden_r_reg),
        .I4(gt0_txinhibit_in),
        .O(txinihibit_0_reg));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    txpolarity_0_i_1
       (.I0(Q[0]),
        .I1(\slv_addr_reg[3]_0 ),
        .I2(clk2_ready_reg_0),
        .I3(slv_rden_r_reg),
        .I4(gt0_txpolarity_in),
        .O(txpolarity_0_reg));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__1
   (drp_access_in_progress_reg,
    access_type_reg,
    drp_access_valid_reg,
    gt0_drpwe_in,
    gt0_drpen_in,
    \axi_bresp_reg[1] ,
    p_0_in,
    s_axi_aclk,
    drpclk,
    gt0_drprdy_out,
    drp_if_select,
    data_out,
    drp_access_in_progress_reg_0,
    access_type5_out,
    \s_axi_wdata[30] ,
    s_axi_aresetn,
    gt_slv_wren,
    access_type,
    s_axi_wdata,
    data_in,
    wait_for_drp,
    wr_req_reg);
  output drp_access_in_progress_reg;
  output access_type_reg;
  output drp_access_valid_reg;
  output gt0_drpwe_in;
  output gt0_drpen_in;
  output \axi_bresp_reg[1] ;
  input p_0_in;
  input s_axi_aclk;
  input drpclk;
  input gt0_drprdy_out;
  input drp_if_select;
  input data_out;
  input drp_access_in_progress_reg_0;
  input access_type5_out;
  input \s_axi_wdata[30] ;
  input s_axi_aresetn;
  input gt_slv_wren;
  input access_type;
  input [0:0]s_axi_wdata;
  input data_in;
  input wait_for_drp;
  input wr_req_reg;

  wire access_type;
  wire access_type5_out;
  wire access_type_reg;
  wire axi_2_drp_valid_i_n_0;
  wire axi_2_drp_valid_i_n_1;
  wire axi_2_drp_valid_i_n_2;
  wire \axi_bresp_reg[1] ;
  wire clk1_ready_pulse;
  wire clk1_ready_pulse0;
  wire clk1_ready_sync_r;
  wire clk2_ready;
  wire clk2_valid_pulse;
  wire clk2_valid_sync_r;
  wire data_in;
  wire data_out;
  wire drp_access_in_progress_reg;
  wire drp_access_in_progress_reg_0;
  wire drp_access_valid_reg;
  wire drp_if_select;
  wire drpclk;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire gt_slv_wren;
  wire p_0_in;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [0:0]s_axi_wdata;
  wire \s_axi_wdata[30] ;
  wire selected_rdy_axi;
  wire wait_for_drp;
  wire wr_req_reg;

  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h2)) 
    Jesd204_microblaze_jesd204_phy_0_0_gt_i_1
       (.I0(clk2_valid_pulse),
        .I1(drp_if_select),
        .O(gt0_drpen_in));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h20)) 
    Jesd204_microblaze_jesd204_phy_0_0_gt_i_2
       (.I0(access_type),
        .I1(drp_if_select),
        .I2(clk2_valid_pulse),
        .O(gt0_drpwe_in));
  LUT6 #(
    .INIT(64'hE200E2000000E200)) 
    access_type_i_1
       (.I0(access_type),
        .I1(access_type5_out),
        .I2(s_axi_wdata),
        .I3(s_axi_aresetn),
        .I4(clk1_ready_pulse),
        .I5(gt_slv_wren),
        .O(access_type_reg));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__1 axi_2_drp_valid_i
       (.clk2_ready_reg(axi_2_drp_valid_i_n_0),
        .clk2_valid_pulse_reg(axi_2_drp_valid_i_n_1),
        .clk2_valid_sync_r(clk2_valid_sync_r),
        .clk2_valid_sync_r_reg(axi_2_drp_valid_i_n_2),
        .data_in(clk2_ready),
        .data_out(data_out),
        .drp_access_valid_reg(data_in),
        .drp_if_select(drp_if_select),
        .drpclk(drpclk),
        .gt0_drprdy_out(gt0_drprdy_out));
  LUT4 #(
    .INIT(16'hBF80)) 
    \axi_bresp[1]_i_5 
       (.I0(clk1_ready_pulse),
        .I1(drp_access_in_progress_reg_0),
        .I2(wait_for_drp),
        .I3(wr_req_reg),
        .O(\axi_bresp_reg[1] ));
  FDRE clk1_ready_pulse_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(clk1_ready_pulse0),
        .Q(clk1_ready_pulse),
        .R(p_0_in));
  FDRE clk1_ready_sync_r_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(selected_rdy_axi),
        .Q(clk1_ready_sync_r),
        .R(p_0_in));
  FDRE clk2_ready_reg
       (.C(drpclk),
        .CE(1'b1),
        .D(axi_2_drp_valid_i_n_0),
        .Q(clk2_ready),
        .R(1'b0));
  FDRE clk2_valid_pulse_reg
       (.C(drpclk),
        .CE(1'b1),
        .D(axi_2_drp_valid_i_n_1),
        .Q(clk2_valid_pulse),
        .R(1'b0));
  FDRE clk2_valid_sync_r_reg
       (.C(drpclk),
        .CE(1'b1),
        .D(axi_2_drp_valid_i_n_2),
        .Q(clk2_valid_sync_r),
        .R(1'b0));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__2 drp_2_axi_in_progress_i
       (.access_type5_out(access_type5_out),
        .clk1_ready_pulse0(clk1_ready_pulse0),
        .clk1_ready_sync_r(clk1_ready_sync_r),
        .clk2_ready_reg(clk2_ready),
        .data_in(data_in),
        .data_out(selected_rdy_axi),
        .drp_access_valid_reg(drp_access_valid_reg),
        .gt_slv_wren(gt_slv_wren),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\s_axi_wdata[30] (\s_axi_wdata[30] ));
  LUT6 #(
    .INIT(64'hE200E2000000E200)) 
    drp_access_in_progress_i_1
       (.I0(drp_access_in_progress_reg_0),
        .I1(access_type5_out),
        .I2(\s_axi_wdata[30] ),
        .I3(s_axi_aresetn),
        .I4(clk1_ready_pulse),
        .I5(gt_slv_wren),
        .O(drp_access_in_progress_reg));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__2
   (slv_wren_done_pulse,
    rxpolarity_0_reg,
    rxlpmen_reg,
    rxdfelpmreset_reg,
    slv_access_valid_hold_reg,
    \axi_bresp_reg[1] ,
    p_0_in,
    s_axi_aclk,
    data_sync_reg_gsr,
    rx_core_clk,
    \slv_wdata_r_internal_reg[0] ,
    slv_rden_r_reg,
    \slv_addr_reg[3] ,
    \gt_interface_sel_reg[2] ,
    gt0_rxpolarity_in,
    \slv_addr_reg[3]_0 ,
    gt0_rxlpmen_in,
    \slv_addr_reg[3]_1 ,
    gt0_rxdfelpmreset_in,
    s_axi_wvalid,
    chan_rx_axi_map_wready,
    chan_rx_slv_rden,
    data_in,
    slv_rden_r_0,
    slv_wren_done_pulse_1);
  output slv_wren_done_pulse;
  output rxpolarity_0_reg;
  output rxlpmen_reg;
  output rxdfelpmreset_reg;
  output slv_access_valid_hold_reg;
  output \axi_bresp_reg[1] ;
  input p_0_in;
  input s_axi_aclk;
  input data_sync_reg_gsr;
  input rx_core_clk;
  input \slv_wdata_r_internal_reg[0] ;
  input slv_rden_r_reg;
  input \slv_addr_reg[3] ;
  input \gt_interface_sel_reg[2] ;
  input gt0_rxpolarity_in;
  input \slv_addr_reg[3]_0 ;
  input gt0_rxlpmen_in;
  input \slv_addr_reg[3]_1 ;
  input gt0_rxdfelpmreset_in;
  input s_axi_wvalid;
  input chan_rx_axi_map_wready;
  input chan_rx_slv_rden;
  input data_in;
  input slv_rden_r_0;
  input slv_wren_done_pulse_1;

  wire axi_2_drp_valid_i_n_0;
  wire axi_2_drp_valid_i_n_1;
  wire \axi_bresp_reg[1] ;
  wire chan_rx_axi_map_wready;
  wire chan_rx_slv_rden;
  wire clk1_ready_pulse0;
  wire clk1_ready_sync_r;
  wire clk2_ready;
  wire clk2_valid_pulse0;
  wire clk2_valid_sync_r;
  wire data_in;
  wire data_sync_reg_gsr;
  wire gt0_rxdfelpmreset_in;
  wire gt0_rxlpmen_in;
  wire gt0_rxpolarity_in;
  wire \gt_interface_sel_reg[2] ;
  wire p_0_in;
  wire rx_core_clk;
  wire rxdfelpmreset_reg;
  wire rxlpmen_reg;
  wire rxpolarity_0_reg;
  wire s_axi_aclk;
  wire s_axi_wvalid;
  wire slv_access_valid_hold_reg;
  wire \slv_addr_reg[3] ;
  wire \slv_addr_reg[3]_0 ;
  wire \slv_addr_reg[3]_1 ;
  wire slv_rden_r_0;
  wire slv_rden_r_reg;
  wire \slv_wdata_r_internal_reg[0] ;
  wire slv_wren_clear;
  wire slv_wren_clk2;
  wire slv_wren_done_pulse;
  wire slv_wren_done_pulse_1;

  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__3 axi_2_drp_valid_i
       (.clk2_ready_reg(axi_2_drp_valid_i_n_0),
        .clk2_valid_pulse0(clk2_valid_pulse0),
        .clk2_valid_sync_r(clk2_valid_sync_r),
        .data_in(clk2_ready),
        .data_out(axi_2_drp_valid_i_n_1),
        .rx_core_clk(rx_core_clk),
        .slv_access_valid_hold_reg(data_in),
        .slv_wren_clk2(slv_wren_clk2));
  LUT4 #(
    .INIT(16'h4F44)) 
    \axi_bresp[1]_i_4 
       (.I0(slv_rden_r_reg),
        .I1(slv_wren_done_pulse),
        .I2(slv_rden_r_0),
        .I3(slv_wren_done_pulse_1),
        .O(\axi_bresp_reg[1] ));
  FDRE clk1_ready_pulse_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(clk1_ready_pulse0),
        .Q(slv_wren_done_pulse),
        .R(p_0_in));
  FDRE clk1_ready_sync_r_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(slv_wren_clear),
        .Q(clk1_ready_sync_r),
        .R(p_0_in));
  FDRE clk2_ready_reg
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(axi_2_drp_valid_i_n_0),
        .Q(clk2_ready),
        .R(data_sync_reg_gsr));
  FDRE clk2_valid_pulse_reg
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(clk2_valid_pulse0),
        .Q(slv_wren_clk2),
        .R(data_sync_reg_gsr));
  FDRE clk2_valid_sync_r_reg
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(axi_2_drp_valid_i_n_1),
        .Q(clk2_valid_sync_r),
        .R(data_sync_reg_gsr));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__4 drp_2_axi_in_progress_i
       (.chan_rx_axi_map_wready(chan_rx_axi_map_wready),
        .chan_rx_slv_rden(chan_rx_slv_rden),
        .clk1_ready_pulse0(clk1_ready_pulse0),
        .clk1_ready_sync_r(clk1_ready_sync_r),
        .clk2_ready_reg(clk2_ready),
        .data_in(data_in),
        .data_out(slv_wren_clear),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_wvalid(s_axi_wvalid),
        .slv_access_valid_hold_reg(slv_access_valid_hold_reg),
        .slv_rden_r_reg(slv_rden_r_reg));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    rxdfelpmreset_i_1
       (.I0(\slv_wdata_r_internal_reg[0] ),
        .I1(\slv_addr_reg[3]_1 ),
        .I2(slv_wren_clk2),
        .I3(slv_rden_r_reg),
        .I4(gt0_rxdfelpmreset_in),
        .O(rxdfelpmreset_reg));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    rxlpmen_i_1
       (.I0(\slv_wdata_r_internal_reg[0] ),
        .I1(\slv_addr_reg[3]_0 ),
        .I2(slv_wren_clk2),
        .I3(slv_rden_r_reg),
        .I4(gt0_rxlpmen_in),
        .O(rxlpmen_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000008)) 
    rxpolarity_0_i_1
       (.I0(\slv_wdata_r_internal_reg[0] ),
        .I1(slv_wren_clk2),
        .I2(slv_rden_r_reg),
        .I3(\slv_addr_reg[3] ),
        .I4(\gt_interface_sel_reg[2] ),
        .I5(gt0_rxpolarity_in),
        .O(rxpolarity_0_reg));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync
   (data_out,
    tx_reset_done,
    s_axi_aclk);
  output data_out;
  input tx_reset_done;
  input s_axi_aclk;

  wire cdc_i_i_1_n_0;
  wire data_out;
  wire data_tmp;
  wire s_axi_aclk;
  wire tx_reset_done;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__23 cdc_i
       (.dest_clk(s_axi_aclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(cdc_i_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cdc_i_i_1
       (.I0(tx_reset_done),
        .O(cdc_i_i_1_n_0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__1
   (clk2_ready_reg,
    clk2_valid_pulse_reg,
    clk2_valid_sync_r_reg,
    data_in,
    gt0_drprdy_out,
    drp_if_select,
    data_out,
    clk2_valid_sync_r,
    drp_access_valid_reg,
    drpclk);
  output clk2_ready_reg;
  output clk2_valid_pulse_reg;
  output clk2_valid_sync_r_reg;
  input data_in;
  input gt0_drprdy_out;
  input drp_if_select;
  input data_out;
  input clk2_valid_sync_r;
  input drp_access_valid_reg;
  input drpclk;

  wire clk2_ready_reg;
  wire clk2_valid_pulse_reg;
  wire clk2_valid_sync_r;
  wire clk2_valid_sync_r_reg;
  wire data_in;
  wire data_out;
  wire data_sync_reg_gsr_n_0;
  wire data_tmp;
  wire drp_access_valid_reg;
  wire drp_if_select;
  wire drpclk;
  wire gt0_drprdy_out;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__28 cdc_i
       (.dest_clk(drpclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(drp_access_valid_reg));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h88F80000)) 
    clk2_ready_i_1
       (.I0(data_in),
        .I1(data_sync_reg_gsr_n_0),
        .I2(gt0_drprdy_out),
        .I3(drp_if_select),
        .I4(data_out),
        .O(clk2_ready_reg));
  LUT3 #(
    .INIT(8'h40)) 
    clk2_valid_pulse_i_1__1
       (.I0(clk2_valid_sync_r),
        .I1(data_sync_reg_gsr_n_0),
        .I2(data_out),
        .O(clk2_valid_pulse_reg));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    clk2_valid_sync_r_i_1
       (.I0(data_sync_reg_gsr_n_0),
        .I1(data_out),
        .O(clk2_valid_sync_r_reg));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_sync_reg_gsr_n_0),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__10
   (data_out,
    rx_reset_done,
    s_axi_aclk);
  output data_out;
  input rx_reset_done;
  input s_axi_aclk;

  wire cdc_i_i_1__0_n_0;
  wire data_out;
  wire data_tmp;
  wire rx_reset_done;
  wire s_axi_aclk;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__24 cdc_i
       (.dest_clk(s_axi_aclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(cdc_i_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cdc_i_i_1__0
       (.I0(rx_reset_done),
        .O(cdc_i_i_1__0_n_0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__11
   (SR,
    s_axi_aresetn,
    tx_core_clk);
  output [0:0]SR;
  input s_axi_aresetn;
  input tx_core_clk;

  wire [0:0]SR;
  wire data_tmp;
  wire s_axi_aresetn;
  wire tx_core_clk;
  wire tx_core_reset;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__26 cdc_i
       (.dest_clk(tx_core_clk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(s_axi_aresetn));
  LUT1 #(
    .INIT(2'h1)) 
    clk2_valid_sync_r_i_1__0
       (.I0(tx_core_reset),
        .O(SR));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(tx_core_reset),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__2
   (drp_access_valid_reg,
    data_out,
    clk1_ready_pulse0,
    data_in,
    access_type5_out,
    \s_axi_wdata[30] ,
    s_axi_aresetn,
    gt_slv_wren,
    clk1_ready_sync_r,
    clk2_ready_reg,
    s_axi_aclk);
  output drp_access_valid_reg;
  output data_out;
  output clk1_ready_pulse0;
  input data_in;
  input access_type5_out;
  input \s_axi_wdata[30] ;
  input s_axi_aresetn;
  input gt_slv_wren;
  input clk1_ready_sync_r;
  input clk2_ready_reg;
  input s_axi_aclk;

  wire access_type5_out;
  wire clk1_ready_pulse0;
  wire clk1_ready_sync_r;
  wire clk2_ready_reg;
  wire data_in;
  wire data_out;
  wire data_tmp;
  wire drp_access_valid_reg;
  wire gt_slv_wren;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire \s_axi_wdata[30] ;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__29 cdc_i
       (.dest_clk(s_axi_aclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(clk2_ready_reg));
  LUT2 #(
    .INIT(4'h2)) 
    clk1_ready_pulse_i_1
       (.I0(clk1_ready_sync_r),
        .I1(data_out),
        .O(clk1_ready_pulse0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hE200E2000000E200)) 
    drp_access_valid_i_1
       (.I0(data_in),
        .I1(access_type5_out),
        .I2(\s_axi_wdata[30] ),
        .I3(s_axi_aresetn),
        .I4(data_out),
        .I5(gt_slv_wren),
        .O(drp_access_valid_reg));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__3
   (clk2_ready_reg,
    data_out,
    clk2_valid_pulse0,
    slv_wren_clk2,
    data_in,
    clk2_valid_sync_r,
    slv_access_valid_hold_reg,
    rx_core_clk);
  output clk2_ready_reg;
  output data_out;
  output clk2_valid_pulse0;
  input slv_wren_clk2;
  input data_in;
  input clk2_valid_sync_r;
  input slv_access_valid_hold_reg;
  input rx_core_clk;

  wire clk2_ready_reg;
  wire clk2_valid_pulse0;
  wire clk2_valid_sync_r;
  wire data_in;
  wire data_out;
  wire data_tmp;
  wire rx_core_clk;
  wire slv_access_valid_hold_reg;
  wire slv_wren_clk2;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__32 cdc_i
       (.dest_clk(rx_core_clk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(slv_access_valid_hold_reg));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    clk2_ready_i_1__1
       (.I0(slv_wren_clk2),
        .I1(data_out),
        .I2(data_in),
        .O(clk2_ready_reg));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h2)) 
    clk2_valid_pulse_i_1__0
       (.I0(data_out),
        .I1(clk2_valid_sync_r),
        .O(clk2_valid_pulse0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__4
   (slv_access_valid_hold_reg,
    data_out,
    clk1_ready_pulse0,
    s_axi_wvalid,
    chan_rx_axi_map_wready,
    chan_rx_slv_rden,
    slv_rden_r_reg,
    data_in,
    clk1_ready_sync_r,
    clk2_ready_reg,
    s_axi_aclk);
  output slv_access_valid_hold_reg;
  output data_out;
  output clk1_ready_pulse0;
  input s_axi_wvalid;
  input chan_rx_axi_map_wready;
  input chan_rx_slv_rden;
  input slv_rden_r_reg;
  input data_in;
  input clk1_ready_sync_r;
  input clk2_ready_reg;
  input s_axi_aclk;

  wire chan_rx_axi_map_wready;
  wire chan_rx_slv_rden;
  wire clk1_ready_pulse0;
  wire clk1_ready_sync_r;
  wire clk2_ready_reg;
  wire data_in;
  wire data_out;
  wire data_tmp;
  wire s_axi_aclk;
  wire s_axi_wvalid;
  wire slv_access_valid_hold_reg;
  wire slv_rden_r_reg;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single cdc_i
       (.dest_clk(s_axi_aclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(clk2_ready_reg));
  LUT2 #(
    .INIT(4'h2)) 
    clk1_ready_pulse_i_1__1
       (.I0(clk1_ready_sync_r),
        .I1(data_out),
        .O(clk1_ready_pulse0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hD5D5FFD5C0C0FFC0)) 
    slv_access_valid_hold_i_1__0
       (.I0(data_out),
        .I1(s_axi_wvalid),
        .I2(chan_rx_axi_map_wready),
        .I3(chan_rx_slv_rden),
        .I4(slv_rden_r_reg),
        .I5(data_in),
        .O(slv_access_valid_hold_reg));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__5
   (clk2_ready_reg,
    data_out,
    clk2_valid_pulse0,
    clk2_valid_pulse_reg,
    data_in,
    clk2_valid_sync_r,
    slv_access_valid_hold_reg,
    tx_core_clk);
  output clk2_ready_reg;
  output data_out;
  output clk2_valid_pulse0;
  input clk2_valid_pulse_reg;
  input data_in;
  input clk2_valid_sync_r;
  input slv_access_valid_hold_reg;
  input tx_core_clk;

  wire clk2_ready_reg;
  wire clk2_valid_pulse0;
  wire clk2_valid_pulse_reg;
  wire clk2_valid_sync_r;
  wire data_in;
  wire data_out;
  wire data_tmp;
  wire slv_access_valid_hold_reg;
  wire tx_core_clk;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__30 cdc_i
       (.dest_clk(tx_core_clk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(slv_access_valid_hold_reg));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    clk2_ready_i_1__0
       (.I0(clk2_valid_pulse_reg),
        .I1(data_out),
        .I2(data_in),
        .O(clk2_ready_reg));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h2)) 
    clk2_valid_pulse_i_1
       (.I0(data_out),
        .I1(clk2_valid_sync_r),
        .O(clk2_valid_pulse0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__6
   (slv_access_valid_hold_reg,
    data_out,
    clk1_ready_pulse0,
    s_axi_wvalid,
    chan_tx_axi_map_wready,
    chan_tx_slv_rden,
    slv_rden_r_reg,
    data_in,
    clk1_ready_sync_r,
    clk2_ready_reg,
    s_axi_aclk);
  output slv_access_valid_hold_reg;
  output data_out;
  output clk1_ready_pulse0;
  input s_axi_wvalid;
  input chan_tx_axi_map_wready;
  input chan_tx_slv_rden;
  input slv_rden_r_reg;
  input data_in;
  input clk1_ready_sync_r;
  input clk2_ready_reg;
  input s_axi_aclk;

  wire chan_tx_axi_map_wready;
  wire chan_tx_slv_rden;
  wire clk1_ready_pulse0;
  wire clk1_ready_sync_r;
  wire clk2_ready_reg;
  wire data_in;
  wire data_out;
  wire data_tmp;
  wire s_axi_aclk;
  wire s_axi_wvalid;
  wire slv_access_valid_hold_reg;
  wire slv_rden_r_reg;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__31 cdc_i
       (.dest_clk(s_axi_aclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(clk2_ready_reg));
  LUT2 #(
    .INIT(4'h2)) 
    clk1_ready_pulse_i_1__0
       (.I0(clk1_ready_sync_r),
        .I1(data_out),
        .O(clk1_ready_pulse0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hD5D5FFD5C0C0FFC0)) 
    slv_access_valid_hold_i_1
       (.I0(data_out),
        .I1(s_axi_wvalid),
        .I2(chan_tx_axi_map_wready),
        .I3(chan_tx_slv_rden),
        .I4(slv_rden_r_reg),
        .I5(data_in),
        .O(slv_access_valid_hold_reg));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__7
   (data_out,
    gt0_cplllock_out,
    s_axi_aclk);
  output data_out;
  input gt0_cplllock_out;
  input s_axi_aclk;

  wire cdc_i_i_1__1_n_0;
  wire data_out;
  wire data_tmp;
  wire gt0_cplllock_out;
  wire s_axi_aclk;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__22 cdc_i
       (.dest_clk(s_axi_aclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(cdc_i_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cdc_i_i_1__1
       (.I0(gt0_cplllock_out),
        .O(cdc_i_i_1__1_n_0));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__8
   (\drp_read_data_reg[0] ,
    data_out,
    s_axi_aresetn,
    drpclk);
  output [0:0]\drp_read_data_reg[0] ;
  output data_out;
  input s_axi_aresetn;
  input drpclk;

  wire data_out;
  wire data_tmp;
  wire [0:0]\drp_read_data_reg[0] ;
  wire drpclk;
  wire s_axi_aresetn;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__25 cdc_i
       (.dest_clk(drpclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(s_axi_aresetn));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \drp_read_data[15]_i_1 
       (.I0(data_out),
        .O(\drp_read_data_reg[0] ));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_sync__xdcDup__9
   (clk2_ready_reg,
    s_axi_aresetn,
    rx_core_clk);
  output clk2_ready_reg;
  input s_axi_aresetn;
  input rx_core_clk;

  wire clk2_ready_reg;
  wire data_tmp;
  wire rx_core_clk;
  wire rx_core_reset;
  wire s_axi_aresetn;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__27 cdc_i
       (.dest_clk(rx_core_clk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(s_axi_aresetn));
  LUT1 #(
    .INIT(2'h1)) 
    clk2_valid_sync_r_i_1__1
       (.I0(rx_core_reset),
        .O(clk2_ready_reg));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(rx_core_reset),
        .R(1'b0));
endmodule

(* DowngradeIPIdentifiedWarnings = "yes" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_support
   (s_axi_aclk,
    s_axi_aresetn,
    s_axi_awaddr,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready,
    gt0_txresetdone_out,
    gt0_rxresetdone_out,
    gt0_cplllock_out,
    gt0_eyescandataerror_out,
    gt0_eyescantrigger_in,
    gt0_eyescanreset_in,
    gt0_txprbsforceerr_in,
    gt0_txpcsreset_in,
    gt0_txpmareset_in,
    gt0_txbufstatus_out,
    gt0_rxcdrhold_in,
    gt0_rxprbserr_out,
    gt0_rxprbssel_in,
    gt0_rxprbscntreset_in,
    gt0_rxbufreset_in,
    gt0_rxbufstatus_out,
    gt0_rxstatus_out,
    gt0_rxbyteisaligned_out,
    gt0_rxbyterealign_out,
    gt0_rxcommadet_out,
    gt0_dmonitorout_out,
    gt0_rxpcsreset_in,
    gt0_rxpmareset_in,
    gt0_rxmonitorout_out,
    gt0_rxmonitorsel_in,
    tx_sys_reset,
    rx_sys_reset,
    tx_reset_gt,
    rx_reset_gt,
    tx_reset_done,
    rx_reset_done,
    cpll_refclk,
    rxencommaalign,
    tx_core_clk,
    txoutclk,
    rx_core_clk,
    rxoutclk,
    drpclk,
    gt_prbssel,
    gt0_txdata,
    gt0_txcharisk,
    gt0_rxdata,
    gt0_rxcharisk,
    gt0_rxdisperr,
    gt0_rxnotintable,
    rxn_in,
    rxp_in,
    txn_out,
    txp_out);
  input s_axi_aclk;
  input s_axi_aresetn;
  input [11:0]s_axi_awaddr;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input s_axi_wvalid;
  output s_axi_wready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [11:0]s_axi_araddr;
  input s_axi_arvalid;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input s_axi_rready;
  output gt0_txresetdone_out;
  output gt0_rxresetdone_out;
  output gt0_cplllock_out;
  output gt0_eyescandataerror_out;
  input gt0_eyescantrigger_in;
  input gt0_eyescanreset_in;
  input gt0_txprbsforceerr_in;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  output [1:0]gt0_txbufstatus_out;
  input gt0_rxcdrhold_in;
  output gt0_rxprbserr_out;
  input [2:0]gt0_rxprbssel_in;
  input gt0_rxprbscntreset_in;
  input gt0_rxbufreset_in;
  output [2:0]gt0_rxbufstatus_out;
  output [2:0]gt0_rxstatus_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  output [14:0]gt0_dmonitorout_out;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  output [6:0]gt0_rxmonitorout_out;
  input [1:0]gt0_rxmonitorsel_in;
  input tx_sys_reset;
  input rx_sys_reset;
  input tx_reset_gt;
  input rx_reset_gt;
  output tx_reset_done;
  output rx_reset_done;
  input cpll_refclk;
  input rxencommaalign;
  input tx_core_clk;
  output txoutclk;
  input rx_core_clk;
  output rxoutclk;
  input drpclk;
  input [2:0]gt_prbssel;
  input [31:0]gt0_txdata;
  input [3:0]gt0_txcharisk;
  output [31:0]gt0_rxdata;
  output [3:0]gt0_rxcharisk;
  output [3:0]gt0_rxdisperr;
  output [3:0]gt0_rxnotintable;
  input [0:0]rxn_in;
  input [0:0]rxp_in;
  output [0:0]txn_out;
  output [0:0]txp_out;

  wire \<const0> ;
  wire common0_qpll_clk_i;
  wire common0_qpll_refclk_i;
  wire cpll_refclk;
  wire drpclk;
  wire gt0_cplllock_out;
  wire [14:0]gt0_dmonitorout_out;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_rxbufreset_in;
  wire [2:0]gt0_rxbufstatus_out;
  wire gt0_rxbyteisaligned_out;
  wire gt0_rxbyterealign_out;
  wire gt0_rxcdrhold_in;
  wire [3:0]gt0_rxcharisk;
  wire gt0_rxcommadet_out;
  wire [31:0]gt0_rxdata;
  wire [3:0]gt0_rxdisperr;
  wire [6:0]gt0_rxmonitorout_out;
  wire [1:0]gt0_rxmonitorsel_in;
  wire [3:0]gt0_rxnotintable;
  wire gt0_rxpcsreset_in;
  wire gt0_rxpmareset_in;
  wire gt0_rxprbscntreset_in;
  wire gt0_rxprbserr_out;
  wire [2:0]gt0_rxprbssel_in;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_txbufstatus_out;
  wire [3:0]gt0_txcharisk;
  wire [31:0]gt0_txdata;
  wire gt0_txpcsreset_in;
  wire gt0_txpmareset_in;
  wire gt0_txprbsforceerr_in;
  wire gt0_txresetdone_out;
  wire [2:0]gt_prbssel;
  wire rx_core_clk;
  wire rx_reset_done;
  wire rx_reset_gt;
  wire rx_sys_reset;
  wire rxencommaalign;
  wire [0:0]rxn_in;
  wire rxoutclk;
  wire [0:0]rxp_in;
  wire s_axi_aclk;
  wire [11:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [11:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [1:1]\^s_axi_bresp ;
  wire s_axi_bvalid;
  wire [25:0]\^s_axi_rdata ;
  wire s_axi_rready;
  wire [1:1]\^s_axi_rresp ;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire tx_core_clk;
  wire tx_reset_done;
  wire tx_reset_gt;
  wire tx_sys_reset;
  wire [0:0]txn_out;
  wire txoutclk;
  wire [0:0]txp_out;

  assign s_axi_bresp[1] = \^s_axi_bresp [1];
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_rdata[31] = \<const0> ;
  assign s_axi_rdata[30] = \<const0> ;
  assign s_axi_rdata[29] = \<const0> ;
  assign s_axi_rdata[28] = \<const0> ;
  assign s_axi_rdata[27] = \<const0> ;
  assign s_axi_rdata[26] = \<const0> ;
  assign s_axi_rdata[25:24] = \^s_axi_rdata [25:24];
  assign s_axi_rdata[23] = \<const0> ;
  assign s_axi_rdata[22:0] = \^s_axi_rdata [22:0];
  assign s_axi_rresp[1] = \^s_axi_rresp [1];
  assign s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_block jesd204_phy_block_i
       (.common0_qpll_clk_in(common0_qpll_clk_i),
        .common0_qpll_refclk_in(common0_qpll_refclk_i),
        .cpll_refclk(cpll_refclk),
        .drpclk(drpclk),
        .gt0_cplllock_out(gt0_cplllock_out),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_rxbufreset_in(gt0_rxbufreset_in),
        .gt0_rxbufstatus_out(gt0_rxbufstatus_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxbyterealign_out(gt0_rxbyterealign_out),
        .gt0_rxcdrhold_in(gt0_rxcdrhold_in),
        .gt0_rxcharisk(gt0_rxcharisk),
        .gt0_rxcommadet_out(gt0_rxcommadet_out),
        .gt0_rxdata(gt0_rxdata),
        .gt0_rxdisperr(gt0_rxdisperr),
        .gt0_rxmonitorout_out(gt0_rxmonitorout_out),
        .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),
        .gt0_rxnotintable(gt0_rxnotintable),
        .gt0_rxpcsreset_in(gt0_rxpcsreset_in),
        .gt0_rxpmareset_in(gt0_rxpmareset_in),
        .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),
        .gt0_rxprbserr_out(gt0_rxprbserr_out),
        .gt0_rxprbssel_in(gt0_rxprbssel_in),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_txbufstatus_out(gt0_txbufstatus_out),
        .gt0_txcharisk(gt0_txcharisk),
        .gt0_txdata(gt0_txdata),
        .gt0_txpcsreset_in(gt0_txpcsreset_in),
        .gt0_txpmareset_in(gt0_txpmareset_in),
        .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),
        .gt0_txresetdone_out(gt0_txresetdone_out),
        .gt_prbssel(gt_prbssel),
        .rx_core_clk(rx_core_clk),
        .rx_reset_done(rx_reset_done),
        .rx_reset_gt(rx_reset_gt),
        .rx_sys_reset(rx_sys_reset),
        .rxencommaalign(rxencommaalign),
        .rxn_in(rxn_in),
        .rxoutclk(rxoutclk),
        .rxp_in(rxp_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr[11:2]),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr[11:2]),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bresp(\^s_axi_bresp ),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata({\^s_axi_rdata [25:24],\^s_axi_rdata [22:0]}),
        .s_axi_rready(s_axi_rready),
        .s_axi_rresp(\^s_axi_rresp ),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata({s_axi_wdata[31:30],s_axi_wdata[15:0]}),
        .s_axi_wready(s_axi_wready),
        .s_axi_wvalid(s_axi_wvalid),
        .tx_core_clk(tx_core_clk),
        .tx_reset_done(tx_reset_done),
        .tx_reset_gt(tx_reset_gt),
        .tx_sys_reset(tx_sys_reset),
        .txn_out(txn_out),
        .txoutclk(txoutclk),
        .txp_out(txp_out));
  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_gt_common_wrapper jesd204_phy_gt_common_i
       (.common0_qpll_clk_in(common0_qpll_clk_i),
        .common0_qpll_refclk_in(common0_qpll_refclk_i),
        .cpll_refclk(cpll_refclk));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block
   (data_out,
    tx_reset_gt,
    drpclk);
  output data_out;
  input tx_reset_gt;
  input drpclk;

  wire data_out;
  wire data_tmp;
  wire drpclk;
  wire tx_reset_gt;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
  (* DEF_VAL = "1'b1" *) 
  (* DEST_SYNC_FF = "5" *) 
  (* INIT = "1" *) 
  (* RST_ACTIVE_HIGH = "1" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_async_rst__4 xpm_cdc_async_rst_inst
       (.dest_arst(data_tmp),
        .dest_clk(drpclk),
        .src_arst(tx_reset_gt));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized0
   (data_out,
    O54,
    s_axi_aclk);
  output data_out;
  input O54;
  input s_axi_aclk;

  wire O54;
  wire data_out;
  wire data_tmp;
  wire s_axi_aclk;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__17 cdc_i
       (.dest_clk(s_axi_aclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(O54));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized1
   (data_out,
    data_in,
    drpclk);
  output data_out;
  input data_in;
  input drpclk;

  wire data_in;
  wire data_out;
  wire data_tmp;
  wire drpclk;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__18 cdc_i
       (.dest_clk(drpclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(data_in));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized2
   (data_out,
    data_in,
    drpclk);
  output data_out;
  input data_in;
  input drpclk;

  wire data_in;
  wire data_out;
  wire data_tmp;
  wire drpclk;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__19 cdc_i
       (.dest_clk(drpclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(data_in));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized3
   (tx_reset_done_r0,
    GT_TX_FSM_RESET_DONE_OUT,
    data_in,
    drpclk);
  output tx_reset_done_r0;
  input GT_TX_FSM_RESET_DONE_OUT;
  input data_in;
  input drpclk;

  wire GT_TX_FSM_RESET_DONE_OUT;
  wire data_in;
  wire data_tmp;
  wire drpclk;
  wire tx_chan_rst_done_sync;
  wire tx_reset_done_r0;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__20 cdc_i
       (.dest_clk(drpclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(data_in));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(tx_chan_rst_done_sync),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    tx_reset_done_r_i_1
       (.I0(GT_TX_FSM_RESET_DONE_OUT),
        .I1(tx_chan_rst_done_sync),
        .O(tx_reset_done_r0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__parameterized4
   (rx_reset_done_r0,
    GT_RX_FSM_RESET_DONE_OUT,
    data_in,
    drpclk);
  output rx_reset_done_r0;
  input GT_RX_FSM_RESET_DONE_OUT;
  input data_in;
  input drpclk;

  wire GT_RX_FSM_RESET_DONE_OUT;
  wire data_in;
  wire data_tmp;
  wire drpclk;
  wire rx_chan_rst_done_sync;
  wire rx_reset_done_r0;

  (* DEST_SYNC_FF = "4" *) 
  (* SIM_ASSERT_CHK = "0" *) 
  (* SRC_INPUT_REG = "0" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "SINGLE" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__21 cdc_i
       (.dest_clk(drpclk),
        .dest_out(data_tmp),
        .src_clk(1'b0),
        .src_in(data_in));
  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(rx_chan_rst_done_sync),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    rx_reset_done_r_i_1
       (.I0(GT_RX_FSM_RESET_DONE_OUT),
        .I1(rx_chan_rst_done_sync),
        .O(rx_reset_done_r0));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__1
   (gt_rxreset0,
    data_out,
    GT_RX_FSM_RESET_DONE_OUT,
    data_in,
    drpclk);
  output gt_rxreset0;
  input data_out;
  input GT_RX_FSM_RESET_DONE_OUT;
  input data_in;
  input drpclk;

  wire GT_RX_FSM_RESET_DONE_OUT;
  wire data_in;
  wire data_out;
  wire data_tmp;
  wire drpclk;
  wire gt_rxreset0;
  wire rx_rst_all_sync;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(rx_rst_all_sync),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hBA)) 
    gt_rxreset_i_1
       (.I0(rx_rst_all_sync),
        .I1(data_out),
        .I2(GT_RX_FSM_RESET_DONE_OUT),
        .O(gt_rxreset0));
  (* DEF_VAL = "1'b1" *) 
  (* DEST_SYNC_FF = "5" *) 
  (* INIT = "1" *) 
  (* RST_ACTIVE_HIGH = "1" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_async_rst xpm_cdc_async_rst_inst
       (.dest_arst(data_tmp),
        .dest_clk(drpclk),
        .src_arst(data_in));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__2
   (data_out,
    rx_reset_gt,
    drpclk);
  output data_out;
  input rx_reset_gt;
  input drpclk;

  wire data_out;
  wire data_tmp;
  wire drpclk;
  wire rx_reset_gt;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(data_out),
        .R(1'b0));
  (* DEF_VAL = "1'b1" *) 
  (* DEST_SYNC_FF = "5" *) 
  (* INIT = "1" *) 
  (* RST_ACTIVE_HIGH = "1" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_async_rst__5 xpm_cdc_async_rst_inst
       (.dest_arst(data_tmp),
        .dest_clk(drpclk),
        .src_arst(rx_reset_gt));
endmodule

(* ORIG_REF_NAME = "Jesd204_microblaze_jesd204_phy_0_0_sync_block" *) 
module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_sync_block__xdcDup__3
   (gt_txreset0,
    data_out,
    GT_TX_FSM_RESET_DONE_OUT,
    data_in,
    drpclk);
  output gt_txreset0;
  input data_out;
  input GT_TX_FSM_RESET_DONE_OUT;
  input data_in;
  input drpclk;

  wire GT_TX_FSM_RESET_DONE_OUT;
  wire data_in;
  wire data_out;
  wire data_tmp;
  wire drpclk;
  wire gt_txreset0;
  wire tx_rst_all_sync;

  (* ASYNC_REG *) 
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg_gsr
       (.C(drpclk),
        .CE(1'b1),
        .D(data_tmp),
        .Q(tx_rst_all_sync),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hBA)) 
    gt_txreset_i_1
       (.I0(tx_rst_all_sync),
        .I1(data_out),
        .I2(GT_TX_FSM_RESET_DONE_OUT),
        .O(gt_txreset0));
  (* DEF_VAL = "1'b1" *) 
  (* DEST_SYNC_FF = "5" *) 
  (* INIT = "1" *) 
  (* RST_ACTIVE_HIGH = "1" *) 
  (* VERSION = "0" *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  (* XPM_MODULE = "TRUE" *) 
  Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_async_rst__6 xpm_cdc_async_rst_inst
       (.dest_arst(data_tmp),
        .dest_clk(drpclk),
        .src_arst(data_in));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_async
   (gt0_cpllpd_in,
    tx_sys_reset_axi,
    rx_sys_reset_axi,
    gt0_rxpd_in,
    gt0_txsysclksel_in,
    gt0_rxsysclksel_in,
    gt0_loopback_in,
    \axi_rdata_reg[1] ,
    \axi_rdata_reg[3] ,
    data_sync_reg1,
    data_sync_reg1_0,
    \axi_rdata_reg[2] ,
    data_in,
    \arststages_ff_reg[4] ,
    \axi_rdata_reg[1]_0 ,
    \axi_rdata_reg[0] ,
    \axi_rdata_reg[0]_0 ,
    p_0_in,
    chan_async_axi_map_wready_reg,
    s_axi_aclk,
    \slv_addr_reg[2] ,
    \slv_addr_reg[2]_0 ,
    \slv_addr_reg[2]_1 ,
    \slv_addr_reg[2]_2 ,
    \slv_addr_reg[2]_3 ,
    \slv_addr_reg[2]_4 ,
    \slv_addr_reg[2]_5 ,
    \slv_addr_reg[2]_6 ,
    chan_async_axi_map_wready_reg_0,
    chan_async_axi_map_wready_reg_1,
    chan_async_axi_map_wready_reg_2,
    Q,
    tx_sys_reset,
    rx_sys_reset,
    E,
    s_axi_wdata,
    \slv_addr_reg[4] );
  output gt0_cpllpd_in;
  output tx_sys_reset_axi;
  output rx_sys_reset_axi;
  output [1:0]gt0_rxpd_in;
  output [1:0]gt0_txsysclksel_in;
  output [1:0]gt0_rxsysclksel_in;
  output [2:0]gt0_loopback_in;
  output \axi_rdata_reg[1] ;
  output \axi_rdata_reg[3] ;
  output [4:0]data_sync_reg1;
  output [4:0]data_sync_reg1_0;
  output \axi_rdata_reg[2] ;
  output data_in;
  output \arststages_ff_reg[4] ;
  output \axi_rdata_reg[1]_0 ;
  output \axi_rdata_reg[0] ;
  output \axi_rdata_reg[0]_0 ;
  input p_0_in;
  input chan_async_axi_map_wready_reg;
  input s_axi_aclk;
  input \slv_addr_reg[2] ;
  input \slv_addr_reg[2]_0 ;
  input \slv_addr_reg[2]_1 ;
  input \slv_addr_reg[2]_2 ;
  input \slv_addr_reg[2]_3 ;
  input \slv_addr_reg[2]_4 ;
  input \slv_addr_reg[2]_5 ;
  input \slv_addr_reg[2]_6 ;
  input chan_async_axi_map_wready_reg_0;
  input chan_async_axi_map_wready_reg_1;
  input chan_async_axi_map_wready_reg_2;
  input [1:0]Q;
  input tx_sys_reset;
  input rx_sys_reset;
  input [0:0]E;
  input [4:0]s_axi_wdata;
  input [0:0]\slv_addr_reg[4] ;

  wire [0:0]E;
  wire [1:0]Q;
  wire \arststages_ff_reg[4] ;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire \axi_rdata_reg[1] ;
  wire \axi_rdata_reg[1]_0 ;
  wire \axi_rdata_reg[2] ;
  wire \axi_rdata_reg[3] ;
  wire chan_async_axi_map_wready_reg;
  wire chan_async_axi_map_wready_reg_0;
  wire chan_async_axi_map_wready_reg_1;
  wire chan_async_axi_map_wready_reg_2;
  wire data_in;
  wire [4:0]data_sync_reg1;
  wire [4:0]data_sync_reg1_0;
  wire gt0_cpllpd_in;
  wire [2:0]gt0_loopback_in;
  wire [1:0]gt0_rxpd_in;
  wire [1:0]gt0_rxsysclksel_in;
  wire [1:0]gt0_txsysclksel_in;
  wire p_0_in;
  wire rx_sys_reset;
  wire rx_sys_reset_axi;
  wire s_axi_aclk;
  wire [4:0]s_axi_wdata;
  wire \slv_addr_reg[2] ;
  wire \slv_addr_reg[2]_0 ;
  wire \slv_addr_reg[2]_1 ;
  wire \slv_addr_reg[2]_2 ;
  wire \slv_addr_reg[2]_3 ;
  wire \slv_addr_reg[2]_4 ;
  wire \slv_addr_reg[2]_5 ;
  wire \slv_addr_reg[2]_6 ;
  wire [0:0]\slv_addr_reg[4] ;
  wire tx_sys_reset;
  wire tx_sys_reset_axi;

  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(gt0_txsysclksel_in[0]),
        .I1(gt0_cpllpd_in),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(gt0_rxpd_in[0]),
        .O(\axi_rdata_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_7 
       (.I0(gt0_loopback_in[0]),
        .I1(data_sync_reg1[0]),
        .I2(Q[1]),
        .I3(data_sync_reg1_0[0]),
        .I4(Q[0]),
        .I5(gt0_rxsysclksel_in[0]),
        .O(\axi_rdata_reg[0] ));
  LUT4 #(
    .INIT(16'hC0A0)) 
    \axi_rdata[1]_i_10 
       (.I0(gt0_rxpd_in[1]),
        .I1(gt0_txsysclksel_in[1]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\axi_rdata_reg[1] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_11 
       (.I0(gt0_loopback_in[1]),
        .I1(data_sync_reg1[1]),
        .I2(Q[1]),
        .I3(data_sync_reg1_0[1]),
        .I4(Q[0]),
        .I5(gt0_rxsysclksel_in[1]),
        .O(\axi_rdata_reg[1]_0 ));
  LUT5 #(
    .INIT(32'h1D331DFF)) 
    \axi_rdata[2]_i_5 
       (.I0(data_sync_reg1_0[2]),
        .I1(Q[1]),
        .I2(gt0_loopback_in[2]),
        .I3(Q[0]),
        .I4(data_sync_reg1[2]),
        .O(\axi_rdata_reg[2] ));
  LUT4 #(
    .INIT(16'hC7F7)) 
    \axi_rdata[3]_i_7 
       (.I0(data_sync_reg1[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(data_sync_reg1_0[3]),
        .O(\axi_rdata_reg[3] ));
  FDRE #(
    .INIT(1'b0)) 
    cpll_pd_0_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(chan_async_axi_map_wready_reg),
        .Q(gt0_cpllpd_in),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \loopback_0_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(chan_async_axi_map_wready_reg_2),
        .Q(gt0_loopback_in[0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \loopback_0_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(chan_async_axi_map_wready_reg_1),
        .Q(gt0_loopback_in[1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \loopback_0_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(chan_async_axi_map_wready_reg_0),
        .Q(gt0_loopback_in[2]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \rx_pd_0_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2]_2 ),
        .Q(gt0_rxpd_in[0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \rx_pd_0_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2]_1 ),
        .Q(gt0_rxpd_in[1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    rx_sys_reset_axi_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2]_0 ),
        .Q(rx_sys_reset_axi),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \rxpllclksel_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2]_6 ),
        .Q(gt0_rxsysclksel_in[0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \rxpllclksel_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2]_5 ),
        .Q(gt0_rxsysclksel_in[1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    tx_sys_reset_axi_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2] ),
        .Q(tx_sys_reset_axi),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txpllclksel_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2]_4 ),
        .Q(gt0_txsysclksel_in[0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txpllclksel_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_addr_reg[2]_3 ),
        .Q(gt0_txsysclksel_in[1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txpostcursor_0_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[0]),
        .Q(data_sync_reg1_0[0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txpostcursor_0_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[1]),
        .Q(data_sync_reg1_0[1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txpostcursor_0_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[2]),
        .Q(data_sync_reg1_0[2]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txpostcursor_0_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[3]),
        .Q(data_sync_reg1_0[3]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txpostcursor_0_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[4]),
        .Q(data_sync_reg1_0[4]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txprecursor_0_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[4] ),
        .D(s_axi_wdata[0]),
        .Q(data_sync_reg1[0]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txprecursor_0_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[4] ),
        .D(s_axi_wdata[1]),
        .Q(data_sync_reg1[1]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txprecursor_0_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[4] ),
        .D(s_axi_wdata[2]),
        .Q(data_sync_reg1[2]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txprecursor_0_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[4] ),
        .D(s_axi_wdata[3]),
        .Q(data_sync_reg1[3]),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \txprecursor_0_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_addr_reg[4] ),
        .D(s_axi_wdata[4]),
        .Q(data_sync_reg1[4]),
        .R(p_0_in));
  LUT2 #(
    .INIT(4'hE)) 
    xpm_cdc_async_rst_inst_i_1
       (.I0(tx_sys_reset_axi),
        .I1(tx_sys_reset),
        .O(data_in));
  LUT2 #(
    .INIT(4'hE)) 
    xpm_cdc_async_rst_inst_i_1__0
       (.I0(rx_sys_reset_axi),
        .I1(rx_sys_reset),
        .O(\arststages_ff_reg[4] ));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_rx
   (slv_wren_done_pulse,
    slv_rden_r,
    chan_rx_slv_rdata,
    gt0_rxpolarity_in,
    gt0_rxlpmen_in,
    gt0_rxdfelpmreset_in,
    \axi_bresp_reg[1] ,
    p_0_in,
    s_axi_aclk,
    data_sync_reg_gsr,
    rx_core_clk,
    chan_rx_slv_rden,
    s_axi_aresetn,
    s_axi_wdata,
    chan_rx_axi_map_wready,
    s_axi_wvalid,
    \slv_addr_reg[3] ,
    \gt_interface_sel_reg[2] ,
    \slv_addr_reg[3]_0 ,
    \slv_addr_reg[3]_1 ,
    Q,
    slv_rden_r_0,
    slv_wren_done_pulse_1);
  output slv_wren_done_pulse;
  output slv_rden_r;
  output [0:0]chan_rx_slv_rdata;
  output gt0_rxpolarity_in;
  output gt0_rxlpmen_in;
  output gt0_rxdfelpmreset_in;
  output \axi_bresp_reg[1] ;
  input p_0_in;
  input s_axi_aclk;
  input data_sync_reg_gsr;
  input rx_core_clk;
  input chan_rx_slv_rden;
  input s_axi_aresetn;
  input [0:0]s_axi_wdata;
  input chan_rx_axi_map_wready;
  input s_axi_wvalid;
  input \slv_addr_reg[3] ;
  input \gt_interface_sel_reg[2] ;
  input \slv_addr_reg[3]_0 ;
  input \slv_addr_reg[3]_1 ;
  input [1:0]Q;
  input slv_rden_r_0;
  input slv_wren_done_pulse_1;

  wire [1:0]Q;
  wire \axi_bresp_reg[1] ;
  wire chan_rx_axi_map_wready;
  wire [0:0]chan_rx_slv_rdata;
  wire chan_rx_slv_rden;
  wire clk2clk_handshake_pulse_gen_i_n_1;
  wire clk2clk_handshake_pulse_gen_i_n_2;
  wire clk2clk_handshake_pulse_gen_i_n_3;
  wire clk2clk_handshake_pulse_gen_i_n_4;
  wire data_sync_reg_gsr;
  wire gt0_rxdfelpmreset_in;
  wire gt0_rxlpmen_in;
  wire gt0_rxpolarity_in;
  wire \gt_interface_sel_reg[2] ;
  wire p_0_in;
  wire rx_core_clk;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [0:0]s_axi_wdata;
  wire s_axi_wvalid;
  wire slv_access_valid_hold;
  wire \slv_addr_reg[3] ;
  wire \slv_addr_reg[3]_0 ;
  wire \slv_addr_reg[3]_1 ;
  wire \slv_rdata[0]_i_1__0_n_0 ;
  wire slv_rden_r;
  wire slv_rden_r_0;
  wire \slv_wdata_r_internal[0]_i_1_n_0 ;
  wire \slv_wdata_r_internal_reg_n_0_[0] ;
  wire slv_wren_done_pulse;
  wire slv_wren_done_pulse_1;

  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen__xdcDup__2 clk2clk_handshake_pulse_gen_i
       (.\axi_bresp_reg[1] (\axi_bresp_reg[1] ),
        .chan_rx_axi_map_wready(chan_rx_axi_map_wready),
        .chan_rx_slv_rden(chan_rx_slv_rden),
        .data_in(slv_access_valid_hold),
        .data_sync_reg_gsr(data_sync_reg_gsr),
        .gt0_rxdfelpmreset_in(gt0_rxdfelpmreset_in),
        .gt0_rxlpmen_in(gt0_rxlpmen_in),
        .gt0_rxpolarity_in(gt0_rxpolarity_in),
        .\gt_interface_sel_reg[2] (\gt_interface_sel_reg[2] ),
        .p_0_in(p_0_in),
        .rx_core_clk(rx_core_clk),
        .rxdfelpmreset_reg(clk2clk_handshake_pulse_gen_i_n_3),
        .rxlpmen_reg(clk2clk_handshake_pulse_gen_i_n_2),
        .rxpolarity_0_reg(clk2clk_handshake_pulse_gen_i_n_1),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_wvalid(s_axi_wvalid),
        .slv_access_valid_hold_reg(clk2clk_handshake_pulse_gen_i_n_4),
        .\slv_addr_reg[3] (\slv_addr_reg[3] ),
        .\slv_addr_reg[3]_0 (\slv_addr_reg[3]_0 ),
        .\slv_addr_reg[3]_1 (\slv_addr_reg[3]_1 ),
        .slv_rden_r_0(slv_rden_r_0),
        .slv_rden_r_reg(slv_rden_r),
        .\slv_wdata_r_internal_reg[0] (\slv_wdata_r_internal_reg_n_0_[0] ),
        .slv_wren_done_pulse(slv_wren_done_pulse),
        .slv_wren_done_pulse_1(slv_wren_done_pulse_1));
  FDRE #(
    .INIT(1'b0)) 
    rxdfelpmreset_reg
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_3),
        .Q(gt0_rxdfelpmreset_in),
        .R(data_sync_reg_gsr));
  FDSE #(
    .INIT(1'b1)) 
    rxlpmen_reg
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_2),
        .Q(gt0_rxlpmen_in),
        .S(data_sync_reg_gsr));
  FDRE #(
    .INIT(1'b0)) 
    rxpolarity_0_reg
       (.C(rx_core_clk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_1),
        .Q(gt0_rxpolarity_in),
        .R(data_sync_reg_gsr));
  FDRE slv_access_valid_hold_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_4),
        .Q(slv_access_valid_hold),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000FAC000000AC0)) 
    \slv_rdata[0]_i_1__0 
       (.I0(gt0_rxpolarity_in),
        .I1(gt0_rxlpmen_in),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\gt_interface_sel_reg[2] ),
        .I5(gt0_rxdfelpmreset_in),
        .O(\slv_rdata[0]_i_1__0_n_0 ));
  FDRE \slv_rdata_reg[0] 
       (.C(s_axi_aclk),
        .CE(s_axi_aresetn),
        .D(\slv_rdata[0]_i_1__0_n_0 ),
        .Q(chan_rx_slv_rdata),
        .R(1'b0));
  FDRE slv_rden_r_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(chan_rx_slv_rden),
        .Q(slv_rden_r),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hAAEFEFEFAA202020)) 
    \slv_wdata_r_internal[0]_i_1 
       (.I0(s_axi_wdata),
        .I1(slv_rden_r),
        .I2(chan_rx_slv_rden),
        .I3(chan_rx_axi_map_wready),
        .I4(s_axi_wvalid),
        .I5(\slv_wdata_r_internal_reg_n_0_[0] ),
        .O(\slv_wdata_r_internal[0]_i_1_n_0 ));
  FDRE \slv_wdata_r_internal_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_wdata_r_internal[0]_i_1_n_0 ),
        .Q(\slv_wdata_r_internal_reg_n_0_[0] ),
        .R(p_0_in));
endmodule

module Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_transDbgCtrl_tx
   (slv_wren_done_pulse,
    slv_wren_clk2,
    slv_rden_r,
    gt0_txinhibit_in,
    gt0_txpolarity_in,
    gt0_txpd_in,
    \slv_rdata_reg[3]_0 ,
    \axi_rdata_reg[3] ,
    p_0_in,
    s_axi_aclk,
    SR,
    tx_core_clk,
    chan_tx_slv_rden,
    \slv_addr_reg[3] ,
    \slv_addr_reg[3]_0 ,
    s_axi_wvalid,
    chan_tx_axi_map_wready,
    \slv_addr_reg[2] ,
    \gt_interface_sel_reg[2] ,
    Q,
    E,
    s_axi_wdata,
    \slv_addr_reg[5] ,
    \slv_addr_reg[3]_1 ,
    \slv_addr_reg[4] ,
    s_axi_aresetn,
    D);
  output slv_wren_done_pulse;
  output slv_wren_clk2;
  output slv_rden_r;
  output gt0_txinhibit_in;
  output gt0_txpolarity_in;
  output [1:0]gt0_txpd_in;
  output [3:0]\slv_rdata_reg[3]_0 ;
  output [3:0]\axi_rdata_reg[3] ;
  input p_0_in;
  input s_axi_aclk;
  input [0:0]SR;
  input tx_core_clk;
  input chan_tx_slv_rden;
  input \slv_addr_reg[3] ;
  input \slv_addr_reg[3]_0 ;
  input s_axi_wvalid;
  input chan_tx_axi_map_wready;
  input \slv_addr_reg[2] ;
  input \gt_interface_sel_reg[2] ;
  input [1:0]Q;
  input [0:0]E;
  input [3:0]s_axi_wdata;
  input \slv_addr_reg[5] ;
  input \slv_addr_reg[3]_1 ;
  input [0:0]\slv_addr_reg[4] ;
  input s_axi_aresetn;
  input [1:0]D;

  wire [1:0]D;
  wire [0:0]E;
  wire [1:0]Q;
  wire [0:0]SR;
  wire [3:0]\axi_rdata_reg[3] ;
  wire chan_tx_axi_map_wready;
  wire chan_tx_slv_rden;
  wire clk2clk_handshake_pulse_gen_i_n_2;
  wire clk2clk_handshake_pulse_gen_i_n_3;
  wire clk2clk_handshake_pulse_gen_i_n_4;
  wire clk2clk_handshake_pulse_gen_i_n_5;
  wire clk2clk_handshake_pulse_gen_i_n_6;
  wire gt0_txinhibit_in;
  wire [1:0]gt0_txpd_in;
  wire gt0_txpolarity_in;
  wire \gt_interface_sel_reg[2] ;
  wire p_0_in;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [3:0]s_axi_wdata;
  wire s_axi_wvalid;
  wire slv_access_valid_hold;
  wire \slv_addr_reg[2] ;
  wire \slv_addr_reg[3] ;
  wire \slv_addr_reg[3]_0 ;
  wire \slv_addr_reg[3]_1 ;
  wire [0:0]\slv_addr_reg[4] ;
  wire \slv_addr_reg[5] ;
  wire \slv_rdata[0]_i_1_n_0 ;
  wire \slv_rdata[0]_i_3_n_0 ;
  wire \slv_rdata[1]_i_1_n_0 ;
  wire [3:0]\slv_rdata_reg[3]_0 ;
  wire slv_rden_r;
  wire \slv_wdata_r_internal_reg_n_0_[0] ;
  wire \slv_wdata_r_internal_reg_n_0_[1] ;
  wire \slv_wdata_r_internal_reg_n_0_[2] ;
  wire \slv_wdata_r_internal_reg_n_0_[3] ;
  wire slv_wren_clk2;
  wire slv_wren_done_pulse;
  wire tx_core_clk;

  Jesd204_microblaze_jesd204_phy_0_0_Jesd204_microblaze_jesd204_phy_0_0_phyCoreCtrlInterface_hshk_pls_gen clk2clk_handshake_pulse_gen_i
       (.Q({\slv_wdata_r_internal_reg_n_0_[1] ,\slv_wdata_r_internal_reg_n_0_[0] }),
        .SR(SR),
        .chan_tx_axi_map_wready(chan_tx_axi_map_wready),
        .chan_tx_slv_rden(chan_tx_slv_rden),
        .clk2_ready_reg_0(slv_wren_clk2),
        .data_in(slv_access_valid_hold),
        .gt0_txinhibit_in(gt0_txinhibit_in),
        .gt0_txpd_in(gt0_txpd_in),
        .gt0_txpolarity_in(gt0_txpolarity_in),
        .p_0_in(p_0_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_wvalid(s_axi_wvalid),
        .slv_access_valid_hold_reg(clk2clk_handshake_pulse_gen_i_n_4),
        .\slv_addr_reg[2] (Q[0]),
        .\slv_addr_reg[3] (\slv_addr_reg[3] ),
        .\slv_addr_reg[3]_0 (\slv_addr_reg[3]_0 ),
        .\slv_addr_reg[3]_1 (\slv_addr_reg[3]_1 ),
        .\slv_addr_reg[5] (\slv_addr_reg[5] ),
        .slv_rden_r_reg(slv_rden_r),
        .slv_wren_done_pulse(slv_wren_done_pulse),
        .tx_core_clk(tx_core_clk),
        .\tx_pd_0_reg[0] (clk2clk_handshake_pulse_gen_i_n_6),
        .\tx_pd_0_reg[1] (clk2clk_handshake_pulse_gen_i_n_5),
        .txinihibit_0_reg(clk2clk_handshake_pulse_gen_i_n_2),
        .txpolarity_0_reg(clk2clk_handshake_pulse_gen_i_n_3));
  FDRE slv_access_valid_hold_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_4),
        .Q(slv_access_valid_hold),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h040F040FFFFF040F)) 
    \slv_rdata[0]_i_1 
       (.I0(\slv_addr_reg[2] ),
        .I1(gt0_txinhibit_in),
        .I2(\gt_interface_sel_reg[2] ),
        .I3(\slv_rdata[0]_i_3_n_0 ),
        .I4(gt0_txpolarity_in),
        .I5(\slv_addr_reg[3]_0 ),
        .O(\slv_rdata[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hC7F7)) 
    \slv_rdata[0]_i_3 
       (.I0(gt0_txpd_in[0]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\slv_rdata_reg[3]_0 [0]),
        .O(\slv_rdata[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h002C0020)) 
    \slv_rdata[1]_i_1 
       (.I0(gt0_txpd_in[1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\gt_interface_sel_reg[2] ),
        .I4(\slv_rdata_reg[3]_0 [1]),
        .O(\slv_rdata[1]_i_1_n_0 ));
  FDRE \slv_rdata_reg[0] 
       (.C(s_axi_aclk),
        .CE(s_axi_aresetn),
        .D(\slv_rdata[0]_i_1_n_0 ),
        .Q(\axi_rdata_reg[3] [0]),
        .R(1'b0));
  FDRE \slv_rdata_reg[1] 
       (.C(s_axi_aclk),
        .CE(s_axi_aresetn),
        .D(\slv_rdata[1]_i_1_n_0 ),
        .Q(\axi_rdata_reg[3] [1]),
        .R(1'b0));
  FDRE \slv_rdata_reg[2] 
       (.C(s_axi_aclk),
        .CE(s_axi_aresetn),
        .D(D[0]),
        .Q(\axi_rdata_reg[3] [2]),
        .R(1'b0));
  FDRE \slv_rdata_reg[3] 
       (.C(s_axi_aclk),
        .CE(s_axi_aresetn),
        .D(D[1]),
        .Q(\axi_rdata_reg[3] [3]),
        .R(1'b0));
  FDRE slv_rden_r_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(chan_tx_slv_rden),
        .Q(slv_rden_r),
        .R(p_0_in));
  FDRE \slv_wdata_r_internal_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[0]),
        .Q(\slv_wdata_r_internal_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \slv_wdata_r_internal_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[1]),
        .Q(\slv_wdata_r_internal_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE \slv_wdata_r_internal_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[2]),
        .Q(\slv_wdata_r_internal_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE \slv_wdata_r_internal_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[3]),
        .Q(\slv_wdata_r_internal_reg_n_0_[3] ),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \tx_pd_0_reg[0] 
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_6),
        .Q(gt0_txpd_in[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tx_pd_0_reg[1] 
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_5),
        .Q(gt0_txpd_in[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \txdiffctrl_0_reg[0] 
       (.C(tx_core_clk),
        .CE(\slv_addr_reg[4] ),
        .D(\slv_wdata_r_internal_reg_n_0_[0] ),
        .Q(\slv_rdata_reg[3]_0 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \txdiffctrl_0_reg[1] 
       (.C(tx_core_clk),
        .CE(\slv_addr_reg[4] ),
        .D(\slv_wdata_r_internal_reg_n_0_[1] ),
        .Q(\slv_rdata_reg[3]_0 [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \txdiffctrl_0_reg[2] 
       (.C(tx_core_clk),
        .CE(\slv_addr_reg[4] ),
        .D(\slv_wdata_r_internal_reg_n_0_[2] ),
        .Q(\slv_rdata_reg[3]_0 [2]),
        .R(SR));
  FDSE #(
    .INIT(1'b1)) 
    \txdiffctrl_0_reg[3] 
       (.C(tx_core_clk),
        .CE(\slv_addr_reg[4] ),
        .D(\slv_wdata_r_internal_reg_n_0_[3] ),
        .Q(\slv_rdata_reg[3]_0 [3]),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    txinihibit_0_reg
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_2),
        .Q(gt0_txinhibit_in),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    txpolarity_0_reg
       (.C(tx_core_clk),
        .CE(1'b1),
        .D(clk2clk_handshake_pulse_gen_i_n_3),
        .Q(gt0_txpolarity_in),
        .R(SR));
endmodule

(* DEF_VAL = "1'b1" *) (* DEST_SYNC_FF = "5" *) (* INIT = "1" *) 
(* RST_ACTIVE_HIGH = "1" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "ASYNC_RST" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_async_rst
   (src_arst,
    dest_clk,
    dest_arst);
  input src_arst;
  input dest_clk;
  output dest_arst;

  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "ASYNC_RST" *) wire [4:0]arststages_ff;
  wire dest_arst;
  wire dest_clk;
  wire src_arst;

  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(1'b1),
        .Q(arststages_ff[0]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[0]),
        .Q(arststages_ff[1]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[1]),
        .Q(arststages_ff[2]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[2]),
        .Q(arststages_ff[3]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[4] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[3]),
        .Q(arststages_ff[4]));
  LUT1 #(
    .INIT(2'h1)) 
    dest_arst_INST_0
       (.I0(arststages_ff[4]),
        .O(dest_arst));
endmodule

(* DEF_VAL = "1'b1" *) (* DEST_SYNC_FF = "5" *) (* INIT = "1" *) 
(* ORIG_REF_NAME = "xpm_cdc_async_rst" *) (* RST_ACTIVE_HIGH = "1" *) (* VERSION = "0" *) 
(* XPM_MODULE = "TRUE" *) (* xpm_cdc = "ASYNC_RST" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_async_rst__4
   (src_arst,
    dest_clk,
    dest_arst);
  input src_arst;
  input dest_clk;
  output dest_arst;

  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "ASYNC_RST" *) wire [4:0]arststages_ff;
  wire dest_arst;
  wire dest_clk;
  wire src_arst;

  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(1'b1),
        .Q(arststages_ff[0]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[0]),
        .Q(arststages_ff[1]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[1]),
        .Q(arststages_ff[2]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[2]),
        .Q(arststages_ff[3]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[4] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[3]),
        .Q(arststages_ff[4]));
  LUT1 #(
    .INIT(2'h1)) 
    dest_arst_INST_0
       (.I0(arststages_ff[4]),
        .O(dest_arst));
endmodule

(* DEF_VAL = "1'b1" *) (* DEST_SYNC_FF = "5" *) (* INIT = "1" *) 
(* ORIG_REF_NAME = "xpm_cdc_async_rst" *) (* RST_ACTIVE_HIGH = "1" *) (* VERSION = "0" *) 
(* XPM_MODULE = "TRUE" *) (* xpm_cdc = "ASYNC_RST" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_async_rst__5
   (src_arst,
    dest_clk,
    dest_arst);
  input src_arst;
  input dest_clk;
  output dest_arst;

  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "ASYNC_RST" *) wire [4:0]arststages_ff;
  wire dest_arst;
  wire dest_clk;
  wire src_arst;

  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(1'b1),
        .Q(arststages_ff[0]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[0]),
        .Q(arststages_ff[1]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[1]),
        .Q(arststages_ff[2]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[2]),
        .Q(arststages_ff[3]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[4] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[3]),
        .Q(arststages_ff[4]));
  LUT1 #(
    .INIT(2'h1)) 
    dest_arst_INST_0
       (.I0(arststages_ff[4]),
        .O(dest_arst));
endmodule

(* DEF_VAL = "1'b1" *) (* DEST_SYNC_FF = "5" *) (* INIT = "1" *) 
(* ORIG_REF_NAME = "xpm_cdc_async_rst" *) (* RST_ACTIVE_HIGH = "1" *) (* VERSION = "0" *) 
(* XPM_MODULE = "TRUE" *) (* xpm_cdc = "ASYNC_RST" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_async_rst__6
   (src_arst,
    dest_clk,
    dest_arst);
  input src_arst;
  input dest_clk;
  output dest_arst;

  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "ASYNC_RST" *) wire [4:0]arststages_ff;
  wire dest_arst;
  wire dest_clk;
  wire src_arst;

  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(1'b1),
        .Q(arststages_ff[0]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[0]),
        .Q(arststages_ff[1]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[1]),
        .Q(arststages_ff[2]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[2]),
        .Q(arststages_ff[3]));
  (* ASYNC_REG *) 
  (* XPM_CDC = "ASYNC_RST" *) 
  FDCE #(
    .INIT(1'b1)) 
    \arststages_ff_reg[4] 
       (.C(dest_clk),
        .CE(1'b1),
        .CLR(src_arst),
        .D(arststages_ff[3]),
        .Q(arststages_ff[4]));
  LUT1 #(
    .INIT(2'h1)) 
    dest_arst_INST_0
       (.I0(arststages_ff[4]),
        .O(dest_arst));
endmodule

(* DEST_SYNC_FF = "4" *) (* SIM_ASSERT_CHK = "0" *) (* SRC_INPUT_REG = "0" *) 
(* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) (* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__17
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__18
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__19
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__20
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__21
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__22
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__23
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__24
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__25
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__26
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__27
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__28
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__29
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__30
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__31
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule

(* DEST_SYNC_FF = "4" *) (* ORIG_REF_NAME = "xpm_cdc_single" *) (* SIM_ASSERT_CHK = "0" *) 
(* SRC_INPUT_REG = "0" *) (* VERSION = "0" *) (* XPM_MODULE = "TRUE" *) 
(* xpm_cdc = "SINGLE" *) 
module Jesd204_microblaze_jesd204_phy_0_0_xpm_cdc_single__32
   (src_clk,
    src_in,
    dest_clk,
    dest_out);
  input src_clk;
  input src_in;
  input dest_clk;
  output dest_out;

  wire dest_clk;
  wire src_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* xpm_cdc = "SINGLE" *) wire [3:0]syncstages_ff;

  assign dest_out = syncstages_ff[3];
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[0] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(src_in),
        .Q(syncstages_ff[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[1] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[0]),
        .Q(syncstages_ff[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[2] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[1]),
        .Q(syncstages_ff[2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XPM_CDC = "SINGLE" *) 
  FDRE \syncstages_ff_reg[3] 
       (.C(dest_clk),
        .CE(1'b1),
        .D(syncstages_ff[2]),
        .Q(syncstages_ff[3]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
