#----------------------------------------------------------------------
# Title      : External Clock Constraints for JESD204
# Project    : jesd204_phy_v3_2_1
#----------------------------------------------------------------------
# File       : Jesd204_microblaze_jesd204_phy_0_0_clocks.xdc
# Author     : Xilinx
#----------------------------------------------------------------------
# Description: Xilinx Constraint file for JESD204 PHY core
#---------------------------------------------------------------------
# (c) Copyright 2004-2013 Xilinx, Inc. All rights reserved.

# Set DRP Clock
set drpclk [get_clocks -of_objects [get_ports drpclk]]

# Set AXI Clock
set axiclk [get_clocks -of_objects [get_ports s_axi_aclk]]

# 7 Series GT(X/H) False paths - CHANNEL
set_false_path -from [get_cells -hier -filter {name =~ *jesd204_phy_block_i/cpll_lock_r_reg* && IS_SEQUENTIAL}]      -to [get_clocks -of_objects [get_ports s_axi_aclk]]

set_false_path -from [get_cells -hier -filter {name =~ *jesd204_phy_block_i/*x_pll_lock_i_reg* && IS_SEQUENTIAL}]    -to [get_clocks -of_objects [get_ports drpclk]]

set_false_path -from [get_cells -hier -filter {name =~ *drp*Mailbox_i/drp_write_data_reg* && IS_SEQUENTIAL}]       -to [get_clocks -of_objects [get_ports drpclk]]
set_false_path -from [get_cells -hier -filter {name =~ *drp*Mailbox_i/drp_int_addr_reg* && IS_SEQUENTIAL}]         -to [get_clocks -of_objects [get_ports drpclk]]
set_false_path -from [get_cells -hier -filter {name =~ *drp*Mailbox_i/drp_if_select_reg* && IS_SEQUENTIAL}]        -to [get_clocks -of_objects [get_ports drpclk]]
set_false_path -from [get_cells -hier -filter {name =~ *drp*Mailbox_i/access_type_reg* && IS_SEQUENTIAL}]          -to [get_clocks -of_objects [get_ports drpclk]]
set_false_path -from [get_cells -hier -filter {name =~ *drp*Mailbox_i/drp_int_addr_reg* && IS_SEQUENTIAL}]         -to [get_clocks -of_objects [get_ports drpclk]]

set_false_path -from [get_cells -hier -filter {name =~ *_transDbgCtrl_async_i/txpllclksel_reg* && IS_SEQUENTIAL}]  -to [get_clocks -of_objects [get_ports drpclk]]
set_false_path -from [get_cells -hier -filter {name =~ *_transDbgCtrl_async_i/rxpllclksel_reg* && IS_SEQUENTIAL}]  -to [get_clocks -of_objects [get_ports drpclk]]

