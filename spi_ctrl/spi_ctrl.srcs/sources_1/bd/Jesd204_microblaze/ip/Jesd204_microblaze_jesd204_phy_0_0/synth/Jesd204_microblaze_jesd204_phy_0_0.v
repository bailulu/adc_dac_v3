//----------------------------------------------------------------------------
// Title : JESD204 PHY Wrapper
// Project : JESD204 PHY
//----------------------------------------------------------------------------
// File : Jesd204_microblaze_jesd204_phy_0_0.v
//----------------------------------------------------------------------------
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES. 

`timescale 1ns / 1ps

(* CORE_GENERATION_INFO = "Jesd204_microblaze_jesd204_phy_0_0,jesd204_phy_v3_2_1,{x_ipProduct=Vivado 2016.4,x_ipVendor=xilinx.com,x_ipLibrary=ip,x_ipName=jesd204_phy,x_ipVersion=3.2,x_ipCoreRevision=1,x_ipLanguage=VHDL,x_ipSimLanguage=VHDL,C_COMPONENT_NAME=Jesd204_microblaze_jesd204_phy_0_0,C_FAMILY=virtex7,C_SILICON_REVISION=,C_LANES=1,C_SPEEDGRADE=-3,C_SupportLevel=1,C_TransceiverControl=true,c_sub_core_name=Jesd204_microblaze_jesd204_phy_0_0_gt,C_GT_Line_Rate=6.25,C_GT_REFCLK_FREQ=156.250,C_DRPCLK_FREQ=100.0,C_PLL_SELECTION=0,C_RX_GT_Line_Rate=6.25,C_RX_GT_REFCLK_FREQ=156.250,C_RX_PLL_SELECTION=0,C_QPLL_FBDIV=40,C_QPLL_REFCLKDIV=1,C_PLL0_FBDIV=1,C_PLL0_FBDIV_45=4,C_PLL0_REFCLKDIV=1,C_PLL1_FBDIV=1,C_PLL1_FBDIV_45=4,C_PLL1_REFCLKDIV=1,C_Axi_Lite=true,C_AXICLK_FREQ=100.0,C_Transceiver=GTHE2,C_GT_Loc=X0Y0,C_gt_val_extended_timeout=false,C_Tx_use_64b=0,C_Rx_use_64b=0,C_CHANNEL_POS=0,C_QUADS=0,C_Equalization_Mode=0,C_Rx_MasterChan=1,C_Tx_MasterChan=1,C_Ins_Loss=12,C_Config_Type=1,C_Min_Line_Rate=6.25,C_Max_Line_Rate=12.5}" *)
(* X_CORE_INFO = "jesd204_phy_v3_2_1,Vivado 2016.4" *)

//***********************************Entity Declaration************************
(* DowngradeIPIdentifiedWarnings="yes" *)
module Jesd204_microblaze_jesd204_phy_0_0 (
  //---------------------------------------------------------------------------
  // AXI Lite IO
  //---------------------------------------------------------------------------
  input          s_axi_aclk,
  input          s_axi_aresetn,

  input  [11:0]  s_axi_awaddr,
  input          s_axi_awvalid,
  output         s_axi_awready,
  input  [31:0]  s_axi_wdata,
  input          s_axi_wvalid,
  output         s_axi_wready,
  output [1:0]   s_axi_bresp,
  output         s_axi_bvalid,
  input          s_axi_bready,
  input  [11:0]  s_axi_araddr,
  input          s_axi_arvalid,
  output         s_axi_arready,
  output [31:0]  s_axi_rdata,
  output [1:0]   s_axi_rresp,
  output         s_axi_rvalid,
  input          s_axi_rready,

  // Additional GT signals for debug
  // GT Reset Done Outputs
  output         gt0_txresetdone_out,
  output         gt0_rxresetdone_out,

  output         gt0_cplllock_out,

  // RX Margin Analysis Ports
  output         gt0_eyescandataerror_out,
  input          gt0_eyescantrigger_in,
  input          gt0_eyescanreset_in,

  // TX Pattern Checker ports
  input          gt0_txprbsforceerr_in,

  // TX Initialization
  input          gt0_txpcsreset_in,
  input          gt0_txpmareset_in,

  // TX Buffer Ports
  output [1:0]   gt0_txbufstatus_out,

  // Rx CDR Ports
  input          gt0_rxcdrhold_in,

  // RX Pattern Checker ports
  output         gt0_rxprbserr_out,
  input  [2:0]   gt0_rxprbssel_in,
  input          gt0_rxprbscntreset_in,

  // RX Buffer Bypass Ports
  input          gt0_rxbufreset_in,
  output [2:0]   gt0_rxbufstatus_out,
  output [2:0]   gt0_rxstatus_out,

  // RX Byte and Word Alignment Ports
  output         gt0_rxbyteisaligned_out,
  output         gt0_rxbyterealign_out,
  output         gt0_rxcommadet_out,

  // Digital Monitor Ports
  output [14:0]   gt0_dmonitorout_out,


  // RX Initialization and Reset Ports
  input          gt0_rxpcsreset_in,
  input          gt0_rxpmareset_in,

  // Receive Ports - RX Equalizer Ports
  output [6:0]   gt0_rxmonitorout_out,
  input  [1:0]   gt0_rxmonitorsel_in,

  // System Reset Inputs for each direction
  input          tx_sys_reset,
  input          rx_sys_reset,

  // Reset Inputs for each direction
  input          tx_reset_gt,
  input          rx_reset_gt,

  // Reset Done for each direction
  output         tx_reset_done,
  output         rx_reset_done,

  input          cpll_refclk,
  input          rxencommaalign,

  // Clocks
  input          tx_core_clk,
  output         txoutclk,
  input          rx_core_clk,
  output         rxoutclk,
  input          drpclk,

  // PRBS mode
  input  [2:0]   gt_prbssel,
  // Tx Ports
  // Lane 0
  input  [31:0]  gt0_txdata,
  input  [3:0]   gt0_txcharisk,

  // Rx Ports
  // Lane 0
  output [31:0]  gt0_rxdata,
  output [3:0]   gt0_rxcharisk,
  output [3:0]   gt0_rxdisperr,
  output [3:0]   gt0_rxnotintable,

  // Serial ports
  input  [0:0]   rxn_in,
  input  [0:0]   rxp_in,
  output [0:0]   txn_out,
  output [0:0]   txp_out
);

//------------------------------------------------------------
// Instantiate the JESD204 PHY core
//------------------------------------------------------------
Jesd204_microblaze_jesd204_phy_0_0_support
inst(
  .s_axi_aclk                          (s_axi_aclk                    ),
  .s_axi_aresetn                       (s_axi_aresetn                 ),

  .s_axi_awaddr                        (s_axi_awaddr                  ),
  .s_axi_awvalid                       (s_axi_awvalid                 ),
  .s_axi_awready                       (s_axi_awready                 ),

  .s_axi_wdata                         (s_axi_wdata                   ),
  .s_axi_wvalid                        (s_axi_wvalid                  ),
  .s_axi_wready                        (s_axi_wready                  ),

  .s_axi_bresp                         (s_axi_bresp                   ),
  .s_axi_bvalid                        (s_axi_bvalid                  ),
  .s_axi_bready                        (s_axi_bready                  ),

  .s_axi_araddr                        (s_axi_araddr                  ),
  .s_axi_arvalid                       (s_axi_arvalid                 ),
  .s_axi_arready                       (s_axi_arready                 ),

  .s_axi_rdata                         (s_axi_rdata                   ),
  .s_axi_rresp                         (s_axi_rresp                   ),
  .s_axi_rvalid                        (s_axi_rvalid                  ),
  .s_axi_rready                        (s_axi_rready                  ),

  // GT Reset Done Outputs
  .gt0_txresetdone_out                 (gt0_txresetdone_out           ),
  .gt0_rxresetdone_out                 (gt0_rxresetdone_out           ),

  .gt0_cplllock_out                    (gt0_cplllock_out              ),

  // RX Margin Analysis Ports
  .gt0_eyescandataerror_out            (gt0_eyescandataerror_out      ),
  .gt0_eyescantrigger_in               (gt0_eyescantrigger_in         ),
  .gt0_eyescanreset_in                 (gt0_eyescanreset_in           ),

  // TX Pattern Checker ports
  .gt0_txprbsforceerr_in               (gt0_txprbsforceerr_in         ),

  // TX Initialization
  .gt0_txpcsreset_in                   (gt0_txpcsreset_in             ),
  .gt0_txpmareset_in                   (gt0_txpmareset_in             ),

  // TX Buffer Ports
  .gt0_txbufstatus_out                 (gt0_txbufstatus_out           ),

  // Rx CDR Ports
  .gt0_rxcdrhold_in                    (gt0_rxcdrhold_in              ),

  // Receive Ports - Pattern Checker ports
  .gt0_rxprbserr_out                   (gt0_rxprbserr_out             ),
  .gt0_rxprbssel_in                    (gt0_rxprbssel_in              ),
  .gt0_rxprbscntreset_in               (gt0_rxprbscntreset_in         ),

  // RX Buffer Bypass Ports
  .gt0_rxbufreset_in                   (gt0_rxbufreset_in             ),
  .gt0_rxbufstatus_out                 (gt0_rxbufstatus_out           ),
  .gt0_rxstatus_out                    (gt0_rxstatus_out              ),

  // RX Byte and Word Alignment Ports
  .gt0_rxbyteisaligned_out             (gt0_rxbyteisaligned_out       ),
  .gt0_rxbyterealign_out               (gt0_rxbyterealign_out         ),
  .gt0_rxcommadet_out                  (gt0_rxcommadet_out            ),

  // Digital Monitor Ports
  .gt0_dmonitorout_out                 (gt0_dmonitorout_out           ),


  // RX Initialization and Reset Ports
  .gt0_rxpcsreset_in                   (gt0_rxpcsreset_in             ),
  .gt0_rxpmareset_in                   (gt0_rxpmareset_in             ),

  // Receive Ports - RX Equalizer Ports
  .gt0_rxmonitorout_out                (gt0_rxmonitorout_out          ),
  .gt0_rxmonitorsel_in                 (gt0_rxmonitorsel_in           ),

  // System Reset Inputs for each direction
  .tx_sys_reset                        (tx_sys_reset                  ),
  .rx_sys_reset                        (rx_sys_reset                  ),

  // Reset Inputs for each direction
  .tx_reset_gt                         (tx_reset_gt                   ),
  .rx_reset_gt                         (rx_reset_gt                   ),

  // Reset Done for each direction
  .tx_reset_done                       (tx_reset_done                 ),
  .rx_reset_done                       (rx_reset_done                 ),

  .cpll_refclk                         (cpll_refclk                   ),
  .rxencommaalign                      (rxencommaalign                ),

  // Clocks
  .tx_core_clk                         (tx_core_clk                   ),
  .txoutclk                            (txoutclk                      ),
  .rx_core_clk                         (rx_core_clk                   ),
  .rxoutclk                            (rxoutclk                      ),
  .drpclk                              (drpclk                        ),

  .gt_prbssel                          (gt_prbssel                    ),

  // Tx Ports
  // Lane 0
  .gt0_txdata                          (gt0_txdata                    ),
  .gt0_txcharisk                       (gt0_txcharisk                 ),

  // Rx Ports
  // Lane 0
  .gt0_rxdata                          (gt0_rxdata                    ),
  .gt0_rxcharisk                       (gt0_rxcharisk                 ),
  .gt0_rxdisperr                       (gt0_rxdisperr                 ),
  .gt0_rxnotintable                    (gt0_rxnotintable              ),

  // Serial ports
  .rxn_in                              (rxn_in                        ),
  .rxp_in                              (rxp_in                        ),
  .txn_out                             (txn_out                       ),
  .txp_out                             (txp_out                       )
);

endmodule
