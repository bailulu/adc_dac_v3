// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
// Date        : Wed Mar 21 19:59:53 2018
// Host        : lu running 64-bit Ubuntu 14.04.3 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top Jesd204_microblaze_jesd204_phy_0_0 -prefix
//               Jesd204_microblaze_jesd204_phy_0_0_ Jesd204_microblaze_jesd204_phy_0_0_stub.v
// Design      : Jesd204_microblaze_jesd204_phy_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7vx690tffg1761-3
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "jesd204_phy_v3_2_1,Vivado 2016.4" *)
module Jesd204_microblaze_jesd204_phy_0_0(s_axi_aclk, s_axi_aresetn, s_axi_awaddr, 
  s_axi_awvalid, s_axi_awready, s_axi_wdata, s_axi_wvalid, s_axi_wready, s_axi_bresp, 
  s_axi_bvalid, s_axi_bready, s_axi_araddr, s_axi_arvalid, s_axi_arready, s_axi_rdata, 
  s_axi_rresp, s_axi_rvalid, s_axi_rready, gt0_txresetdone_out, gt0_rxresetdone_out, 
  gt0_cplllock_out, gt0_eyescandataerror_out, gt0_eyescantrigger_in, gt0_eyescanreset_in, 
  gt0_txprbsforceerr_in, gt0_txpcsreset_in, gt0_txpmareset_in, gt0_txbufstatus_out, 
  gt0_rxcdrhold_in, gt0_rxprbserr_out, gt0_rxprbssel_in, gt0_rxprbscntreset_in, 
  gt0_rxbufreset_in, gt0_rxbufstatus_out, gt0_rxstatus_out, gt0_rxbyteisaligned_out, 
  gt0_rxbyterealign_out, gt0_rxcommadet_out, gt0_dmonitorout_out, gt0_rxpcsreset_in, 
  gt0_rxpmareset_in, gt0_rxmonitorout_out, gt0_rxmonitorsel_in, tx_sys_reset, rx_sys_reset, 
  tx_reset_gt, rx_reset_gt, tx_reset_done, rx_reset_done, cpll_refclk, rxencommaalign, 
  tx_core_clk, txoutclk, rx_core_clk, rxoutclk, drpclk, gt_prbssel, gt0_txdata, gt0_txcharisk, 
  gt0_rxdata, gt0_rxcharisk, gt0_rxdisperr, gt0_rxnotintable, rxn_in, rxp_in, txn_out, txp_out)
/* synthesis syn_black_box black_box_pad_pin="s_axi_aclk,s_axi_aresetn,s_axi_awaddr[11:0],s_axi_awvalid,s_axi_awready,s_axi_wdata[31:0],s_axi_wvalid,s_axi_wready,s_axi_bresp[1:0],s_axi_bvalid,s_axi_bready,s_axi_araddr[11:0],s_axi_arvalid,s_axi_arready,s_axi_rdata[31:0],s_axi_rresp[1:0],s_axi_rvalid,s_axi_rready,gt0_txresetdone_out,gt0_rxresetdone_out,gt0_cplllock_out,gt0_eyescandataerror_out,gt0_eyescantrigger_in,gt0_eyescanreset_in,gt0_txprbsforceerr_in,gt0_txpcsreset_in,gt0_txpmareset_in,gt0_txbufstatus_out[1:0],gt0_rxcdrhold_in,gt0_rxprbserr_out,gt0_rxprbssel_in[2:0],gt0_rxprbscntreset_in,gt0_rxbufreset_in,gt0_rxbufstatus_out[2:0],gt0_rxstatus_out[2:0],gt0_rxbyteisaligned_out,gt0_rxbyterealign_out,gt0_rxcommadet_out,gt0_dmonitorout_out[14:0],gt0_rxpcsreset_in,gt0_rxpmareset_in,gt0_rxmonitorout_out[6:0],gt0_rxmonitorsel_in[1:0],tx_sys_reset,rx_sys_reset,tx_reset_gt,rx_reset_gt,tx_reset_done,rx_reset_done,cpll_refclk,rxencommaalign,tx_core_clk,txoutclk,rx_core_clk,rxoutclk,drpclk,gt_prbssel[2:0],gt0_txdata[31:0],gt0_txcharisk[3:0],gt0_rxdata[31:0],gt0_rxcharisk[3:0],gt0_rxdisperr[3:0],gt0_rxnotintable[3:0],rxn_in[0:0],rxp_in[0:0],txn_out[0:0],txp_out[0:0]" */;
  input s_axi_aclk;
  input s_axi_aresetn;
  input [11:0]s_axi_awaddr;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input s_axi_wvalid;
  output s_axi_wready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [11:0]s_axi_araddr;
  input s_axi_arvalid;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input s_axi_rready;
  output gt0_txresetdone_out;
  output gt0_rxresetdone_out;
  output gt0_cplllock_out;
  output gt0_eyescandataerror_out;
  input gt0_eyescantrigger_in;
  input gt0_eyescanreset_in;
  input gt0_txprbsforceerr_in;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  output [1:0]gt0_txbufstatus_out;
  input gt0_rxcdrhold_in;
  output gt0_rxprbserr_out;
  input [2:0]gt0_rxprbssel_in;
  input gt0_rxprbscntreset_in;
  input gt0_rxbufreset_in;
  output [2:0]gt0_rxbufstatus_out;
  output [2:0]gt0_rxstatus_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  output [14:0]gt0_dmonitorout_out;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  output [6:0]gt0_rxmonitorout_out;
  input [1:0]gt0_rxmonitorsel_in;
  input tx_sys_reset;
  input rx_sys_reset;
  input tx_reset_gt;
  input rx_reset_gt;
  output tx_reset_done;
  output rx_reset_done;
  input cpll_refclk;
  input rxencommaalign;
  input tx_core_clk;
  output txoutclk;
  input rx_core_clk;
  output rxoutclk;
  input drpclk;
  input [2:0]gt_prbssel;
  input [31:0]gt0_txdata;
  input [3:0]gt0_txcharisk;
  output [31:0]gt0_rxdata;
  output [3:0]gt0_rxcharisk;
  output [3:0]gt0_rxdisperr;
  output [3:0]gt0_rxnotintable;
  input [0:0]rxn_in;
  input [0:0]rxp_in;
  output [0:0]txn_out;
  output [0:0]txp_out;
endmodule
