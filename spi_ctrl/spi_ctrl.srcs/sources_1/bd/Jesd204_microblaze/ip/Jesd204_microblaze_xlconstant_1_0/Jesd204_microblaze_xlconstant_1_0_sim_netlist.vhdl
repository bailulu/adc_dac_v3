-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
-- Date        : Wed Mar 21 20:01:24 2018
-- Host        : lu running 64-bit Ubuntu 14.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top Jesd204_microblaze_xlconstant_1_0 -prefix
--               Jesd204_microblaze_xlconstant_1_0_ Jesd204_microblaze_xlconstant_1_0_sim_netlist.vhdl
-- Design      : Jesd204_microblaze_xlconstant_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7vx690tffg1761-3
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Jesd204_microblaze_xlconstant_1_0 is
  port (
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of Jesd204_microblaze_xlconstant_1_0 : entity is true;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Jesd204_microblaze_xlconstant_1_0 : entity is "yes";
end Jesd204_microblaze_xlconstant_1_0;

architecture STRUCTURE of Jesd204_microblaze_xlconstant_1_0 is
  signal \<const0>\ : STD_LOGIC;
begin
  dout(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
end STRUCTURE;
