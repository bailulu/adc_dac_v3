// (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: xilinx.com:ip:jesd204_phy:3.2
// IP Revision: 1

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
jesd204_phy_0 your_instance_name (
  .cpll_refclk(cpll_refclk),                            // input wire cpll_refclk
  .drpclk(drpclk),                                      // input wire drpclk
  .tx_reset_gt(tx_reset_gt),                            // input wire tx_reset_gt
  .rx_reset_gt(rx_reset_gt),                            // input wire rx_reset_gt
  .tx_sys_reset(tx_sys_reset),                          // input wire tx_sys_reset
  .rx_sys_reset(rx_sys_reset),                          // input wire rx_sys_reset
  .txp_out(txp_out),                                    // output wire [1 : 0] txp_out
  .txn_out(txn_out),                                    // output wire [1 : 0] txn_out
  .rxp_in(rxp_in),                                      // input wire [1 : 0] rxp_in
  .rxn_in(rxn_in),                                      // input wire [1 : 0] rxn_in
  .tx_core_clk(tx_core_clk),                            // input wire tx_core_clk
  .rx_core_clk(rx_core_clk),                            // input wire rx_core_clk
  .txoutclk(txoutclk),                                  // output wire txoutclk
  .rxoutclk(rxoutclk),                                  // output wire rxoutclk
  .gt_prbssel(gt_prbssel),                              // input wire [2 : 0] gt_prbssel
  .gt0_txdata(gt0_txdata),                              // input wire [31 : 0] gt0_txdata
  .gt0_txcharisk(gt0_txcharisk),                        // input wire [3 : 0] gt0_txcharisk
  .gt1_txdata(gt1_txdata),                              // input wire [31 : 0] gt1_txdata
  .gt1_txcharisk(gt1_txcharisk),                        // input wire [3 : 0] gt1_txcharisk
  .tx_reset_done(tx_reset_done),                        // output wire tx_reset_done
  .gt0_rxdata(gt0_rxdata),                              // output wire [31 : 0] gt0_rxdata
  .gt0_rxcharisk(gt0_rxcharisk),                        // output wire [3 : 0] gt0_rxcharisk
  .gt0_rxdisperr(gt0_rxdisperr),                        // output wire [3 : 0] gt0_rxdisperr
  .gt0_rxnotintable(gt0_rxnotintable),                  // output wire [3 : 0] gt0_rxnotintable
  .gt1_rxdata(gt1_rxdata),                              // output wire [31 : 0] gt1_rxdata
  .gt1_rxcharisk(gt1_rxcharisk),                        // output wire [3 : 0] gt1_rxcharisk
  .gt1_rxdisperr(gt1_rxdisperr),                        // output wire [3 : 0] gt1_rxdisperr
  .gt1_rxnotintable(gt1_rxnotintable),                  // output wire [3 : 0] gt1_rxnotintable
  .rx_reset_done(rx_reset_done),                        // output wire rx_reset_done
  .rxencommaalign(rxencommaalign),                      // input wire rxencommaalign
  .gt0_txpmareset_in(gt0_txpmareset_in),                // input wire gt0_txpmareset_in
  .gt0_txpcsreset_in(gt0_txpcsreset_in),                // input wire gt0_txpcsreset_in
  .gt0_rxpmareset_in(gt0_rxpmareset_in),                // input wire gt0_rxpmareset_in
  .gt0_rxpcsreset_in(gt0_rxpcsreset_in),                // input wire gt0_rxpcsreset_in
  .gt0_eyescantrigger_in(gt0_eyescantrigger_in),        // input wire gt0_eyescantrigger_in
  .gt0_eyescanreset_in(gt0_eyescanreset_in),            // input wire gt0_eyescanreset_in
  .gt0_rxprbscntreset_in(gt0_rxprbscntreset_in),        // input wire gt0_rxprbscntreset_in
  .gt0_txprbsforceerr_in(gt0_txprbsforceerr_in),        // input wire gt0_txprbsforceerr_in
  .gt0_rxcdrhold_in(gt0_rxcdrhold_in),                  // input wire gt0_rxcdrhold_in
  .gt0_txresetdone_out(gt0_txresetdone_out),            // output wire gt0_txresetdone_out
  .gt0_rxresetdone_out(gt0_rxresetdone_out),            // output wire gt0_rxresetdone_out
  .gt0_eyescandataerror_out(gt0_eyescandataerror_out),  // output wire gt0_eyescandataerror_out
  .gt0_rxprbserr_out(gt0_rxprbserr_out),                // output wire gt0_rxprbserr_out
  .gt0_rxcommadet_out(gt0_rxcommadet_out),              // output wire gt0_rxcommadet_out
  .gt0_rxbufstatus_out(gt0_rxbufstatus_out),            // output wire [2 : 0] gt0_rxbufstatus_out
  .gt0_txbufstatus_out(gt0_txbufstatus_out),            // output wire [1 : 0] gt0_txbufstatus_out
  .gt0_cplllock_out(gt0_cplllock_out),                  // output wire gt0_cplllock_out
  .gt0_rxmonitorout_out(gt0_rxmonitorout_out),          // output wire [6 : 0] gt0_rxmonitorout_out
  .gt0_rxprbssel_in(gt0_rxprbssel_in),                  // input wire [2 : 0] gt0_rxprbssel_in
  .gt0_rxmonitorsel_in(gt0_rxmonitorsel_in),            // input wire [1 : 0] gt0_rxmonitorsel_in
  .gt0_dmonitorout_out(gt0_dmonitorout_out),            // output wire [14 : 0] gt0_dmonitorout_out
  .gt0_rxstatus_out(gt0_rxstatus_out),                  // output wire [2 : 0] gt0_rxstatus_out
  .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),    // output wire gt0_rxbyteisaligned_out
  .gt0_rxbyterealign_out(gt0_rxbyterealign_out),        // output wire gt0_rxbyterealign_out
  .gt0_rxbufreset_in(gt0_rxbufreset_in),                // input wire gt0_rxbufreset_in
  .gt1_txpmareset_in(gt1_txpmareset_in),                // input wire gt1_txpmareset_in
  .gt1_txpcsreset_in(gt1_txpcsreset_in),                // input wire gt1_txpcsreset_in
  .gt1_rxpmareset_in(gt1_rxpmareset_in),                // input wire gt1_rxpmareset_in
  .gt1_rxpcsreset_in(gt1_rxpcsreset_in),                // input wire gt1_rxpcsreset_in
  .gt1_eyescantrigger_in(gt1_eyescantrigger_in),        // input wire gt1_eyescantrigger_in
  .gt1_eyescanreset_in(gt1_eyescanreset_in),            // input wire gt1_eyescanreset_in
  .gt1_rxprbscntreset_in(gt1_rxprbscntreset_in),        // input wire gt1_rxprbscntreset_in
  .gt1_txprbsforceerr_in(gt1_txprbsforceerr_in),        // input wire gt1_txprbsforceerr_in
  .gt1_rxcdrhold_in(gt1_rxcdrhold_in),                  // input wire gt1_rxcdrhold_in
  .gt1_txresetdone_out(gt1_txresetdone_out),            // output wire gt1_txresetdone_out
  .gt1_rxresetdone_out(gt1_rxresetdone_out),            // output wire gt1_rxresetdone_out
  .gt1_eyescandataerror_out(gt1_eyescandataerror_out),  // output wire gt1_eyescandataerror_out
  .gt1_rxprbserr_out(gt1_rxprbserr_out),                // output wire gt1_rxprbserr_out
  .gt1_rxcommadet_out(gt1_rxcommadet_out),              // output wire gt1_rxcommadet_out
  .gt1_rxbufstatus_out(gt1_rxbufstatus_out),            // output wire [2 : 0] gt1_rxbufstatus_out
  .gt1_txbufstatus_out(gt1_txbufstatus_out),            // output wire [1 : 0] gt1_txbufstatus_out
  .gt1_cplllock_out(gt1_cplllock_out),                  // output wire gt1_cplllock_out
  .gt1_rxmonitorout_out(gt1_rxmonitorout_out),          // output wire [6 : 0] gt1_rxmonitorout_out
  .gt1_rxprbssel_in(gt1_rxprbssel_in),                  // input wire [2 : 0] gt1_rxprbssel_in
  .gt1_rxmonitorsel_in(gt1_rxmonitorsel_in),            // input wire [1 : 0] gt1_rxmonitorsel_in
  .gt1_dmonitorout_out(gt1_dmonitorout_out),            // output wire [14 : 0] gt1_dmonitorout_out
  .gt1_rxstatus_out(gt1_rxstatus_out),                  // output wire [2 : 0] gt1_rxstatus_out
  .gt1_rxbyteisaligned_out(gt1_rxbyteisaligned_out),    // output wire gt1_rxbyteisaligned_out
  .gt1_rxbyterealign_out(gt1_rxbyterealign_out),        // output wire gt1_rxbyterealign_out
  .gt1_rxbufreset_in(gt1_rxbufreset_in),                // input wire gt1_rxbufreset_in
  .s_axi_aclk(s_axi_aclk),                              // input wire s_axi_aclk
  .s_axi_aresetn(s_axi_aresetn),                        // input wire s_axi_aresetn
  .s_axi_awaddr(s_axi_awaddr),                          // input wire [11 : 0] s_axi_awaddr
  .s_axi_awvalid(s_axi_awvalid),                        // input wire s_axi_awvalid
  .s_axi_awready(s_axi_awready),                        // output wire s_axi_awready
  .s_axi_wdata(s_axi_wdata),                            // input wire [31 : 0] s_axi_wdata
  .s_axi_wvalid(s_axi_wvalid),                          // input wire s_axi_wvalid
  .s_axi_wready(s_axi_wready),                          // output wire s_axi_wready
  .s_axi_bresp(s_axi_bresp),                            // output wire [1 : 0] s_axi_bresp
  .s_axi_bvalid(s_axi_bvalid),                          // output wire s_axi_bvalid
  .s_axi_bready(s_axi_bready),                          // input wire s_axi_bready
  .s_axi_araddr(s_axi_araddr),                          // input wire [11 : 0] s_axi_araddr
  .s_axi_arvalid(s_axi_arvalid),                        // input wire s_axi_arvalid
  .s_axi_arready(s_axi_arready),                        // output wire s_axi_arready
  .s_axi_rdata(s_axi_rdata),                            // output wire [31 : 0] s_axi_rdata
  .s_axi_rresp(s_axi_rresp),                            // output wire [1 : 0] s_axi_rresp
  .s_axi_rvalid(s_axi_rvalid),                          // output wire s_axi_rvalid
  .s_axi_rready(s_axi_rready)                          // input wire s_axi_rready
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

// You must compile the wrapper file jesd204_phy_0.v when simulating
// the core, jesd204_phy_0. When compiling the wrapper file, be sure to
// reference the Verilog simulation library.

