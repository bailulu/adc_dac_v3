vlib work
vlib msim

vlib msim/xil_defaultlib
vlib msim/xpm

vmap xil_defaultlib msim/xil_defaultlib
vmap xpm msim/xpm

vlog -work xil_defaultlib -64 -sv \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../../spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/ip_0/jesd204_phy_0_gt/example_design/jesd204_phy_0_gt_tx_startup_fsm.vhd" \
"../../../../spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/ip_0/jesd204_phy_0_gt/example_design/jesd204_phy_0_gt_rx_startup_fsm.vhd" \
"../../../../spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/ip_0/jesd204_phy_0_gt_init.vhd" \
"../../../../spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/ip_0/jesd204_phy_0_gt_cpll_railing.vhd" \
"../../../../spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/ip_0/jesd204_phy_0_gt_gt.vhd" \
"../../../../spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/ip_0/jesd204_phy_0_gt_multi_gt.vhd" \
"../../../../spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/ip_0/jesd204_phy_0_gt/example_design/jesd204_phy_0_gt_sync_block.vhd" \
"../../../../spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/ip_0/jesd204_phy_0_gt.vhd" \
"/home/lu/Desktop/adc_dac/adc_dac_all/spi_ctrl/spi_ctrl.srcs/sources_1/ip/jesd204_phy_0_1/jesd204_phy_0_sim_netlist.vhdl" \

vlog -work xil_defaultlib "glbl.v"

