library ieee;
    use ieee.std_logic_1164.all; 
    --use ieee.numeric_std.all; 
    -- non-IEEE 
    use ieee.std_logic_unsigned.all;
    use ieee.std_logic_misc.all;
    use ieee.std_logic_arith.all; 

library unisim;
  use unisim.vcomponents.all;
-------------------------------------------------------------------------------------
-- ENTITY
-------------------------------------------------------------------------------------
entity adc_ctrol_dataread is
generic
(
  START_ADDR             : std_logic_vector(14 downto 0) := b"000000000000000";--x"0005400";
  STOP_ADDR              : std_logic_vector(14 downto 0) := b"111111111111111"--x"00073FF"
);
port (
 
   reset            : in  std_logic;
   clk              : in  std_logic;
  -- sclk             : in  std_logic;
   clk_reg          : in  std_logic;
 --  ena              : in  std_logic;     -- external enable signal        
  
   out_cmd_val            : out   std_logic;
   out_cmd                : out   std_logic_vector(27-1 downto 0);  
  
   trig_n_cs              : out   std_logic;
   trig_sclk              : out   std_logic;
   trig_sdo               : out   std_logic;
   trig_clksel0           : in    std_logic
);
end adc_ctrol_dataread;

-------------------------------------------------------------------------------------
-- ARCHITECTURE
-------------------------------------------------------------------------------------
architecture Behavioral of adc_ctrol_dataread is

signal out_data_read_reg: std_logic_vector(27-1 downto 0);
signal in_cmd_val: std_logic;
signal o_tx_end  : std_logic;

--***********************************************************************************
begin
--***********************************************************************************

adc_ctrl_ini: entity work.adc_ctrl
generic map (
  START_ADDR      => START_ADDR,
  STOP_ADDR       => STOP_ADDR, 
  
  COM_ALL      => 27,
  COM_NUM      => 4,
  ADDR_NUM     => 15,
  DATA_NUM     => 8,
  CLK_DIV      => 5 
)
port map(
     rst               => reset,
  --   clk               => sclk,

     clk_cmd           => clk,   
     in_cmd_val        => in_cmd_val,    
     in_cmd            => out_data_read_reg,     
     out_cmd_val       => out_cmd_val,        
     out_cmd           => out_cmd ,         
     
     clk_reg           => clk_reg,

     trig_n_cs         =>  trig_n_cs,   
     trig_sclk         =>  trig_sclk,       
     trig_sdo          =>  trig_sdo,   
     trig_clksel0      =>  trig_clksel0,
     o_tx_end          =>  o_tx_end  
 );
  
 data_read_ini: entity work.data_read
 generic map (  
   COM_ALL      => 27
 )
 port map(
    reset            => reset,
    clk              => clk,
    data_end         => o_tx_end,
   -- ena              => ena,       
    out_data_read    => out_data_read_reg,
    out_data_valid   => in_cmd_val
   --out_data_valid   => open
 ); 
  
--***********************************************************************************
end architecture Behavioral;
--***********************************************************************************

