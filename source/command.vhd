
--------------------------------------------------------------------------------
-- Specify libraries
--------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_unsigned.all;
  use ieee.std_logic_misc.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_1164.all;

--------------------------------------------------------------------------------
-- Entity declaration
--------------------------------------------------------------------------------

entity command is
generic (
  -- START_ADDR   : std_logic_vector(27 downto 0) := x"0000000";
  -- STOP_ADDR    : std_logic_vector(27 downto 0) := x"0000010";
   START_ADDR   : std_logic_vector;
   STOP_ADDR    : std_logic_vector;
   COM_ALL      : integer;
   COM_NUM      : integer;
   ADDR_NUM     : integer;
   DATA_NUM     : integer
);
port (
   reset            : in  std_logic;
   -- Command interface
   clk_cmd          : in  std_logic;                    --cmd_in and cmd_out are synchronous to this clock;
   out_cmd          : out std_logic_vector(COM_ALL-1 downto 0);
   out_cmd_val      : out std_logic;
   in_cmd           : in  std_logic_vector(COM_ALL-1 downto 0);
   in_cmd_val       : in  std_logic;
   -- Register interface
   clk_reg          : in  std_logic;                    --register interface is synchronous to this clock
   wr_reg_data     : out std_logic_vector(DATA_NUM-1 downto 0);--caries the data which will be written in the adc register.
   wr_reg_val      : out std_logic;                    -- there is valid data on the wr_reg_data(pulse)
   wr_reg_val_ack  : out std_logic;                    --the wr_reg_data has valid data and expects and acknowledge back  (pulse)
   wr_reg_addr     : out std_logic_vector(ADDR_NUM-1 downto 0);--caries the register address which will be written.
   rd_reg_data      : in  std_logic_vector(DATA_NUM-1 downto 0);-- register data is read and placed on this bus
   rd_reg_val       : in  std_logic;                    --pulse to indicate requested register data is valid
   rd_reg_req       : out std_logic;                    --pulse to request to read data
   rd_reg_addr      : out std_logic_vector(ADDR_NUM-1 downto 0);--requested address
   --write acknowledge interface
   wr_ack           : in  std_logic;                      --pulse to indicate write is done
   -- Mailbox interface
   mbx_in_reg       : in  std_logic_vector(DATA_NUM-1 downto 0);--value of the mailbox to send
   mbx_in_val       : in  std_logic                     --pulse to indicate mailbox is valid
);
end entity command;

--------------------------------------------------------------------------------
-- Architecture declaration
--------------------------------------------------------------------------------

architecture arch_spi_cmd of command is

-----------------------------------------------------------------------------------
-- Constant declarations
-----------------------------------------------------------------------------------
constant CMD_WR              : std_logic_vector(3 downto 0) := x"0";
constant CMD_RD              : std_logic_vector(3 downto 0) := x"2";
constant CMD_RD_ACK          : std_logic_vector(3 downto 0) := x"4";
constant CMD_WR_ACK          : std_logic_vector(3 downto 0) := x"5";
constant CMD_WR_EXPECTS_ACK  : std_logic_vector(3 downto 0) := x"6";

-----------------------------------------------------------------------------------
-- Dignal declarations
-----------------------------------------------------------------------------------
signal register_wr     : std_logic;
signal register_wr_ack : std_logic;
signal register_rd     : std_logic;
signal out_cmd_val_sig : std_logic;
signal rd_reg_addr_sig : std_logic_vector(ADDR_NUM-1 downto 0);
signal wr_reg_addr_sig     : std_logic_vector(ADDR_NUM-1 downto 0);

signal mbx_in_val_sig  : std_logic;
signal mbx_received    : std_logic;

signal wr_ack_sig      : std_logic;


-----------------------------------------------------------------------------------
-- Begin
-----------------------------------------------------------------------------------
begin

-----------------------------------------------------------------------------------
-- Synchronous processes
-----------------------------------------------------------------------------------
read_reg_proc: process (reset, clk_cmd)
begin
   if (reset = '1') then
      rd_reg_addr_sig <= (others => '0');
      register_rd     <= '0';
      mbx_received    <= '0';
      out_cmd         <= (others => '0');
      out_cmd_val     <= '0';

   elsif (clk_cmd'event and clk_cmd = '1') then

      --register the requested address when the address is in the modules range
      --if (in_cmd_val = '1' and in_cmd(63 downto 60) = CMD_RD and in_cmd(59 downto 32) >= start_addr and in_cmd(59 downto 32) <= stop_addr) then
      --   rd_reg_addr_sig <= in_cmd(59 downto 32)-start_addr;
      --end if;
      if (in_cmd_val = '1' and in_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) = CMD_RD and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) >= start_addr and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) <= stop_addr) then
         rd_reg_addr_sig <= in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM)-start_addr;
      end if;
      --generate the read req pulse when the address is in the modules range
      --if (in_cmd_val = '1' and in_cmd(63 downto 60) = CMD_RD and in_cmd(59 downto 32) >= start_addr and in_cmd(59 downto 32) <= stop_addr) then
      --   register_rd <= '1';
      --else
      --   register_rd <= '0';
      -- end if; 
      if (in_cmd_val = '1' and in_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) = CMD_RD and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) >= start_addr and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) <= stop_addr) then
           register_rd <= '1';
      else
           register_rd <= '0';
      end if;
      --mailbox has less priority then command acknowledge
      --create the output packet
      if (out_cmd_val_sig = '1' and mbx_in_val_sig = '1') then
         mbx_received <= '1';
      elsif( mbx_received = '1' and out_cmd_val_sig = '0') then
         mbx_received <= '0';
      end if;

      if (out_cmd_val_sig = '1') then
         out_cmd(DATA_NUM-1 downto 0)  <= rd_reg_data;
         out_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) <= rd_reg_addr_sig+start_addr;
         out_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) <= CMD_RD_ACK;
      elsif (mbx_in_val_sig = '1' or mbx_received = '1') then
         out_cmd(DATA_NUM-1 downto 0)  <= mbx_in_reg;
         out_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) <= start_addr;
         out_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) <= (others=>'0');
      elsif (wr_ack_sig = '1' ) then
         out_cmd(DATA_NUM-1 downto 0)  <= mbx_in_reg;
         out_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) <= wr_reg_addr_sig+start_addr;
         out_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) <= CMD_WR_ACK;
      else
         out_cmd(COM_ALL-1 downto 0)  <= (others=>'0');
      end if;

      if (out_cmd_val_sig = '1') then
         out_cmd_val <= '1';
      elsif (mbx_in_val_sig = '1' or mbx_received = '1') then
         out_cmd_val <= '1';
      elsif (wr_ack_sig = '1') then
         out_cmd_val <= '1';
      else
         out_cmd_val <= '0';
      end if;

   end if;
end process;

write_reg_proc: process(reset, clk_cmd)
begin
   if (reset = '1') then
      wr_reg_addr_sig  <= (others => '0');
      wr_reg_data      <= (others => '0');
      register_wr       <= '0';
      register_wr_ack   <= '0';

   elsif(clk_cmd'event and clk_cmd = '1') then

      --register the requested address when the address is in the modules range
      if (in_cmd_val = '1' and (in_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) = CMD_WR or in_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) = CMD_WR_EXPECTS_ACK)
          and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) >= start_addr and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) <= stop_addr) then
         wr_reg_addr_sig <= in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) - start_addr;
         wr_reg_data      <= in_cmd(DATA_NUM-1 downto 0);
      end if;

      --generate the write req pulse when the address is in the modules range
      if (in_cmd_val = '1' and (in_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) = CMD_WR or in_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) = CMD_WR_EXPECTS_ACK)
          and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) >= start_addr and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) <= stop_addr) then
         register_wr <= '1';
      else
         register_wr <= '0';
      end if;
      
      --generate the write requests ack pulse when the address is in the modules range and command is write that expects an ack
      if (in_cmd_val = '1' and in_cmd(COM_ALL-1 downto COM_ALL-COM_NUM) = CMD_WR_EXPECTS_ACK and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) >= start_addr and in_cmd(ADDR_NUM+DATA_NUM-1 downto DATA_NUM) <= stop_addr) then
         register_wr_ack <= '1';
      else
         register_wr_ack <= '0';
      end if;
      

   end if;
end process;

-----------------------------------------------------------------------------------
-- Asynchronous mapping
-----------------------------------------------------------------------------------
rd_reg_addr <= rd_reg_addr_sig;

wr_reg_addr <= wr_reg_addr_sig;

wr_reg_val  <= register_wr;

rd_reg_req  <= register_rd;

out_cmd_val_sig <= rd_reg_val;

mbx_in_val_sig <= mbx_in_val;

wr_ack_sig <= wr_ack;

wr_reg_val_ack <= register_wr_ack;
-----------------------------------------------------------------------------------
-- End
-----------------------------------------------------------------------------------

end architecture arch_spi_cmd;

